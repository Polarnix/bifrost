# Name:          install-gmdss-metarea-software.yml
# Purpose:       Install Bifrost GMDSS METAREA tools to the client.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.- name: install packages from apt
---
- name: create directory structure for ice charting
  file: path=/home/bifrostanalyst/{{ item }} owner=bifrostanalyst group=bifrost state=directory
  with_items:
    - Python
    - Python/GMDSS
    - Include
    - Include/GMDSS
    - Ancillary
    - Ancillary/GMDSS
    - Settings
    - Settings/GMDSS
    - Static
    - Static/METAREA
    - Static/METAREA/Arctic
    - Static/METAREA/Antarctic

- name: copy input Arctic METAREA Shapefiles
  copy: src=roles/common/files/metarea/{{ item }}
        dest=/home/bifrostanalyst/Static/METAREA/Arctic
        owner=bifrostanalyst group=bifrost mode="u+rwx,g+rx,o+rx"
  with_items:
    - arctic/01-uk/metarea-01.shp
    - arctic/01-uk/metarea-01.shx
    - arctic/01-uk/metarea-01.dbf
    - arctic/01-uk/metarea-01.prj
    - arctic/19-norway/metarea-19.shp
    - arctic/19-norway/metarea-19.shx
    - arctic/19-norway/metarea-19.dbf
    - arctic/19-norway/metarea-19.prj
    - arctic/20-russia/metarea-20.shp
    - arctic/20-russia/metarea-20.shx
    - arctic/20-russia/metarea-20.dbf
    - arctic/20-russia/metarea-20.prj
    - arctic/21-russia/metarea-21.shp
    - arctic/21-russia/metarea-21.shx
    - arctic/21-russia/metarea-21.dbf
    - arctic/21-russia/metarea-21.prj
    
- name: copy input Antarctic METAREA Shapefiles
  copy: src=roles/common/files/metarea/{{ item }}
        dest=/home/bifrostanalyst/Static/METAREA/Antarctic
        owner=bifrostanalyst group=bifrost mode="u+rwx,g+rx,o+rx"
  with_items:
    - antarctic/06-argentina/metarea-06.shp
    - antarctic/06-argentina/metarea-06.shx
    - antarctic/06-argentina/metarea-06.dbf
    - antarctic/06-argentina/metarea-06.prj
    - antarctic/07-south-africa/metarea-07.shp
    - antarctic/07-south-africa/metarea-07.shx
    - antarctic/07-south-africa/metarea-07.dbf
    - antarctic/07-south-africa/metarea-07.prj
    - antarctic/10-australia/metarea-10.shp
    - antarctic/10-australia/metarea-10.shx
    - antarctic/10-australia/metarea-10.dbf
    - antarctic/10-australia/metarea-10.prj
    - antarctic/14-new-zealand/metarea-14.shp
    - antarctic/14-new-zealand/metarea-14.shx
    - antarctic/14-new-zealand/metarea-14.dbf
    - antarctic/14-new-zealand/metarea-14.prj
    - antarctic/15-chile/metarea-15.shp
    - antarctic/15-chile/metarea-15.shx
    - antarctic/15-chile/metarea-15.dbf
    - antarctic/15-chile/metarea-15.prj

- name: copy static files
  copy: src=roles/client/files/static/gmdss/{{ item }}
        dest=/home/bifrostanalyst/Include/GMDSS
        owner=bifrostanalyst group=bifrost mode="u+rwx,g+rx,o+rx"
  with_items:
    - GSHHS_i_landmask.shp
    - GSHHS_i_landmask.shx
    - GSHHS_i_landmask.dbf
    - GSHHS_i_landmask.prj
    - cover_mask_pstereo.shp
    - cover_mask_pstereo.shx
    - cover_mask_pstereo.dbf
    - cover_mask_pstereo.prj
    - islands.csv

- name: copy static files
  copy: src=roles/client/files/static/metarea/Antarctic/{{ item }}
        dest=/home/bifrostanalyst/Static/METAREA
        owner=bifrostanalyst group=bifrost mode="u+rwx,g+rx,o+rx"
  with_items:
    - antarctic_metarea.shp
    - antarctic_metarea.shx
    - antarctic_metarea.dbf
    - antarctic_metarea.prj
        
- name: copy GMDSS Antarctic python scripts
  copy: src=roles/client/files/gmdss-antarctic-reporting/{{ item }}
        dest=/home/bifrostanalyst/Python/GMDSS
        owner=bifrostanalyst group=bifrost mode="u+rwx,g+rx,o+rx"
  with_items:
    - increase_subarea_nodes.py
    - extract_ice_edge.py
    - get_SIC_raster.py
    - icecover.py
    - metarea_processing.py
    - iceedge_report.py
    - README.txt