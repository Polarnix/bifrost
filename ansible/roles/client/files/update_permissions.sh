#!/bin/bash

# Name:          update_permissions.sh
# Purpose:       Shell script to change file permissions and make the current
#                working files read-write by everyone (allows multiple analysts to
#                work on files together).
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

TODAY=`date +'%Y%m%d'`
chmod -R a+rwx "/disk1/Istjenesten/New_Production_System/ICS/${TODAY}"_???
chmod -R a+rwx /disk1/Istjenesten/New_Production_System/tempdata
chmod -R a+rwx /disk1/Istjenesten/New_Production_System/LiveData
chmod -R a+rwx /disk1/Istjenesten/New_Production_System/tmp
exit
