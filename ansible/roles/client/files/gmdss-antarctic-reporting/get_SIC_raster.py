#!/usr/bin/python

# Name:          get_SIC_raster.py
# Purpose:       Get PMW SIC information from OSI SAF or University of Bremen (AMSR2)..
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


import sys, os
from ftplib import FTP
import pycurl
import gzip, shutil

import numpy as N

from Scientific.IO.NetCDF import NetCDFFile

import osgeo.gdal as gdal
from osgeo.gdalconst import *
import osgeo.osr as osr

TMPDIR = '/home/bifrostanalyst/tmp'

mthstr = [ 'jan', 'feb', 'mar', 'apr', 'may', 'jun', \
           'jul', 'aug', 'sep', 'oct', 'nov', 'dec' ]

# Download file using FTP
def ftp_download( urlstr, ftpdir, ftpfn ):

    # Open FTP session
    ftp = FTP( urlstr )
    ftp.login()
    ftp.cwd( ftpdir )

    # Handle data being downloaded
    def handleDownload(block):
        fout.write(block)

    # Download file to temporary directory
    localfname = ("%s/%s" % (TMPDIR,ftpfn))
    fout = open( localfname, 'wb' )
    ftp.retrbinary( ("RETR %s" % ftpfn), handleDownload )
    fout.close()

    # Close ftp session
    ftp.close()

    return localfname


# Download file using HTTP
def http_download( urlstr, httpdir, httpfn ):

    # Download file to temporary directory
    localfname = ("%s/%s" % (TMPDIR,httpfn))
    fp = open(localfname, "wb")
    curl = pycurl.Curl()
    curl.setopt(pycurl.URL, urlstr+httpdir+httpfn)
    curl.setopt(pycurl.WRITEDATA, fp)
    curl.perform()
    curl.close()
    fp.close()

    return localfname


# Read SIC data and map parameters from netCDF
def netcdf_sic( netcdffn ):
    # Open netCDF file
    fin = NetCDFFile( netcdffn, 'r' )

    # Get the SIC variable
    sicvar = fin.variables['ice_conc']
    sic = N.array(sicvar.getValue())

    # Get map projection data
    mapvar = fin.variables['Polar_Stereographic_Grid']
    proj4str = mapvar.proj4_string
    xvar = fin.variables['xc']
    yvar = fin.variables['yc']
    ulx = xvar[0] * 1000.0
    uly = yvar[0] * 1000.0
    xres = (xvar[1] - xvar[0]) * 1000.0
    yres = (yvar[1] - yvar[0]) * 1000.0
    maptrans = [ ulx, xres, 0.0, uly, 0.0, yres ]

    # Close netCDF file
    fin.close()

    # Squeeze array to remove spare dimension
    sic = sic.squeeze()

    # Convert to normal range of values
    sic = sic / 100.0
    sic[N.nonzero(sic<0)] = 255

    return [ sic, proj4str, maptrans ]


# Read SIC data and map parameters from GeoTIFF
def geotiff_sic( tiffn ):

    # Open GeoTIFF file
    driver = gdal.GetDriverByName('GTiff')
    driver.Register()
    inds = gdal.Open( tiffn, GA_ReadOnly )
    if inds is None:
        print 'Could not open ' + tiffn
        sys.exit()

    # Get map projection and transformation
    maptrans = inds.GetGeoTransform()
    proj = inds.GetProjection()
    src_srs = osr.SpatialReference()
    src_srs.ImportFromWkt(proj)
    proj4str = src_srs.ExportToProj4()

    # Get SIC data
    inband = inds.GetRasterBand(1)
    sic = (inband.ReadAsArray()).astype(N.float32)
    idx = N.nonzero(sic<250)
    sic[idx] = sic[idx] / 2.0
    # Blank missing values
    idx = N.nonzero(sic==255)
    sic[idx] = 0.0

    # Close file
    inband = None
    inds = None

    return [ sic, proj4str, maptrans ]


# Create a classified GeoTIFF image
# Converts SIC to NIS ice classes
def geotiff_class( sic, proj4str, maptrans, sicdate ):

    # Get shape of SIC array
    [ny,nx] = sic.shape
    # print ny,nx

    # Blank array for output
    sicclass = N.zeros( (ny,nx), dtype=N.uint8 )

    # Define NIS ice classes
    # lower = [ 10.0, 40.0, 70.0, 90.0, 100.0 ]
    # value = [ 25, 55, 80, 95, 100 ]
    lower = [ 10.0 ]
    value = [ 25 ]

    # Loop through classes and create classified image
    for (low,val) in zip(lower,value):
        # print low, val
        idx = N.nonzero(sic>=low)
        sicclass[idx] = val
    # print N.unique(sicclass)

    # Create a GeoTIFF file containing sicclass data
    driver = gdal.GetDriverByName('GTiff')
    driver.Register()
    outfn = ("%s/sic_class_%4d%02d%02d.tif" % \
        (TMPDIR,sicdate.year,sicdate.month,sicdate.day))
    outds = driver.Create(outfn,nx,ny,1,GDT_Byte,["COMPRESS=LZW"])
    outband = outds.GetRasterBand(1)
    outband.WriteArray(sicclass,0,0)
    outband.FlushCache()
    outds.SetGeoTransform(maptrans)
    trg_srs = osr.SpatialReference()
    trg_srs.ImportFromProj4(proj4str)
    outds.SetProjection(trg_srs.ExportToWkt())
    outband = None
    outds = None

    return outfn


# OSI SAF Reprocessing SIC data file
def reprocessed( safdate ):
    ftpdir = ("reprocessed/ice/conc/v1p1/%4d/%02d/" % \
        (safdate.year,safdate.month))
    ftpfn = ("ice_conc_sh_polstere-100_reproc_%4d%02d%02d1200.nc.gz" % \
        (safdate.year,safdate.month,safdate.day))

    # Download compressed netCDF file from FTP
    localfn = ftp_download('osisaf.met.no',ftpdir,ftpfn)

    # Reprocessed dataset files are compressed, so uncompress
    ncfn = localfn[:-3]
    with open(ncfn, "wb") as tmp:
        shutil.copyfileobj(gzip.open(localfn), tmp)
    os.remove( localfn )

    # Read data from netCDF 
    [ sic, proj4str, maptrans ] = netcdf_sic( ncfn )

    # Write classified GeoTIFF
    gtiffn = geotiff_class( sic, proj4str, maptrans, safdate )

    # Delete the netCDF file
    os.remove( ncfn )

    return gtiffn


# Get OSI SAF SIC data file
def osisaf( safdate ):
    ftpdir = ("archive/ice/conc/%4d/%02d/" % \
        (safdate.year,safdate.month))
    ftpfn = ("ice_conc_sh_polstere-100_multi_%4d%02d%02d1200.nc" % \
        (safdate.year,safdate.month,safdate.day))

    # Download netCDF file from FTP
    ncfn = ftp_download('osisaf.met.no',ftpdir,ftpfn)

    # Read data from netCDF 
    [ sic, proj4str, maptrans ] = netcdf_sic( ncfn )

    # Write classified GeoTIFF
    gtiffn = geotiff_class( sic, proj4str, maptrans, safdate )

    # Delete the netCDF file
    os.remove( ncfn )

    return gtiffn


# Get Bremen AMSR-E and AMSR2 data file
def amsr( bredate, satstr ):
    httpdir = ("%sdata/asi_daygrid_swath/s6250/%4d/%3s/" % \
        (satstr,bredate.year,mthstr[bredate.month-1]))
    if satstr == 'amsre':
        httpfn = ("asi-s6250-%4d%02d%02d-v5.tif" % \
            (bredate.year,bredate.month,bredate.day))
    else:
        httpfn = ("asi-AMSR2-s6250-%4d%02d%02d-v5.tif" % \
            (bredate.year,bredate.month,bredate.day))

    # Download GeoTIFF file from HTTP
    tiffn = http_download('http://iup.physik.uni-bremen.de:8084/',httpdir,httpfn)

    # Read data from GEoTIFF
    [ sic, proj4str, maptrans ] = geotiff_sic( tiffn )

    # Write classified GeoTIFF
    gtiffn = geotiff_class( sic, proj4str, maptrans, bredate )

    # Delete the netCDF file
    os.remove( tiffn )

    return gtiffn

