#!/usr/bin/python

# Name:          icecover.py
# Purpose:       Remap ice cover prior to extracting ice edge.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import sys, os

import osgeo.gdal as gdal
import osgeo.ogr as ogr
from osgeo.gdalconst import *
import osgeo.osr as osr

TMPDIR = '/home/bifrostanalyst/tmp'
STATICDIR = '/home/bifrostanalyst/Static'
INCDIR = '/home/bifrostanalyst/Include/GMDSS'

# Create and ice cover mask by:

# 1. Coverting a SIC raster to polygons (using gdal_polygonize)
def icecover( sicfn, sicdate ):

    # Open the source raster file
    rast_drv = gdal.GetDriverByName('GTiff')
    rast_drv.Register()
    inds = gdal.Open( sicfn, GA_ReadOnly )
    if inds is None:
        print 'Could not open ' + tiffn
        sys.exit()

    # Get the source band (and set mask band to none)
    inband = inds.GetRasterBand(1)
    maskband = None

    # Open the destination Shapefile
    vect_drv = ogr.GetDriverByName('ESRI Shapefile')
    outfn = ("%s/sic_class_%4d%02d%02d_poly.shp" % \
        (TMPDIR,sicdate.year,sicdate.month,sicdate.day))
    if os.path.exists(outfn):
        vect_drv.DeleteDataSource(outfn)
    outds = vect_drv.CreateDataSource(outfn)

    # Create output spatial reference
    out_srs = osr.SpatialReference()
    out_srs.ImportFromWkt( inds.GetProjectionRef() )

    # Create output layer
    layer_name = ("sic_class_%4d%02d%02d_poly" % \
        (sicdate.year,sicdate.month,sicdate.day))
    outlay = outds.CreateLayer(layer_name, \
        srs=out_srs)

    # Create fields for SIC value
    sic_fdef = ogr.FieldDefn('sic', ogr.OFTInteger)
    outlay.CreateField(sic_fdef)
    dst_field = 0

    # Invoke GDAL Polygonize algorithm
    options = []
    # prog_func = gdal.TermProgress
    prog_func = None
    result = gdal.Polygonize( inband, maskband, \
        outlay, dst_field, options, callback = prog_func )

    # Close files
    inband = None
    inds = None
    outds = None

    return outfn


# 2. Union of SIC polygons with filtering
def filterunion( inpfn, sicdate ):

    # Get Shapefile driver from OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Open input file
    inds = driver.Open( inpfn, 0)

    # Get input layer
    inlay = inds.GetLayer()

    # Get input projection
    in_srs = inlay.GetSpatialRef()

    # Open mask file and get polygons
    mskfn = ("%s/cover_mask_pstereo.shp" % INCDIR)
    mskds = driver.Open( mskfn, 0 )
    msklay = mskds.GetLayer()
    msk_srs = msklay.GetSpatialRef()
    # Coordinate transform to SIC image spatial reference
    coordtrans = osr.CoordinateTransformation( msk_srs, in_srs )
    mask_list = []
    nmsk = msklay.GetFeatureCount()
    for i in range(nmsk):
        mskfeat = msklay.GetFeature(i)
        msk_poly = mskfeat.GetGeometryRef()
        msk_poly.Transform(coordtrans)
        mask_list.append( msk_poly.Clone() )
        mskfeat.Destroy()
    mskds.Destroy()

    # Open islands file and get centre points
    islfn = ("%s/islands.csv" % INCDIR)
    isl = open( islfn, 'r' )
    islands = []
    firstflg = 1
    for tline in isl:
        if firstflg == 1:
            proj4str = tline
            isl_srs = osr.SpatialReference()
            isl_srs.ImportFromProj4(proj4str)
            coordtrans = osr.CoordinateTransformation( isl_srs, in_srs )
            firstflg = 0
        else:
            bits = tline.split(',')
            isl_lon = float(bits[0])
            isl_lat = float(bits[1])
            isl_geom = ogr.Geometry(ogr.wkbPoint)
            isl_geom.AddPoint( isl_lon, isl_lat )
            isl_geom.Transform(coordtrans)
            # print isl_geom
            islands.append( isl_geom.Clone() )
    isl.close()
    nisl = len(islands)

    # Loop through features and filter/union
    npoly = inlay.GetFeatureCount()
    # print npoly
    percent_scale = 100.0 / float(npoly)
    percent_counter = 0.0
    poly_area = 0.0
    cover_area = 0.0
    firstflg = 1
    for i in range(npoly):
        # Get feature
        infeat = inlay.GetFeature(i)

        # Get SIC value
        in_sic = infeat.GetFieldAsString('sic')
        in_sic = int(in_sic)

        # Get feature geometry
        in_poly = infeat.GetGeometryRef()

        # Do not do polygons with zero SIC
        if in_sic > 0:

            # More filtering and union
            mskflg = 0

            # Filter for areas
            for j in range(nmsk):
                # print i, j, in_poly.Within( mask_list[j] )
                # print in_poly.ExportToWkt()
                # print mask_list[j].ExportToWkt()
                if in_poly.Within( mask_list[j] ):
                    mskflg = mskflg + 1

            # Filter islands
            if mskflg == 0:
                isl_limit = 25.0
                in_centre = in_poly.Centroid()
                for j in range(nisl):
                    distance = in_centre.Distance( islands[j] ) / 1000.0
                    # print j, distance
                    if distance < isl_limit:
                        mskflg = mskflg + 1

            # Add polygon to output if the mask flag has not been set
            if mskflg == 0:

                if firstflg == 1:
                    # Create initial geometry by cloning
                    cover = in_poly.Clone()
                    firstflg = 0
                else:
                    cover = cover.Union( in_poly )

                poly_area = in_poly.Area() / (1000.0*1000.0)
                cover_area = cover.Area() / (1000.0*1000.0)

        progress = float(i) * percent_scale
        if progress >= percent_counter or i == (npoly-1):
            # print ("%5.1f %5d %3d %12.0f %12.0f" % \
            #     (progress,i,in_sic,poly_area,cover_area))
            percent_counter = percent_counter + 10.0

        # Destroy input feature
        infeat.Destroy()

    # Create an output Shapefile
    outfn = ("%s/sic_class_%4d%02d%02d_union.shp" % \
        (TMPDIR,sicdate.year,sicdate.month,sicdate.day))
    if os.path.exists(outfn):
        driver.DeleteDataSource(outfn)
    outds = driver.CreateDataSource(outfn)

    # Create output layer
    layer_name = ("sic_class_%4d%02d%02d_union" % \
        (sicdate.year,sicdate.month,sicdate.day))
    outlay = outds.CreateLayer(layer_name, \
        geom_type=ogr.wkbPolygon, srs=in_srs)
    # Create an output feature and set attributes
    featureDefn = outlay.GetLayerDefn()
    outfeat = ogr.Feature( featureDefn )
    outfeat.SetGeometry(cover)

    # Write the feature to the layer
    outlay.CreateFeature(outfeat)

    # Destroy output feature
    outfeat.Destroy()

    # Close input and output files
    inds.Destroy()
    outds.Destroy()

    # Create a projection file
    prjfn = ("%s.prj" % outfn[:-4])
    # print prjfn
    prj = open(prjfn, 'w')
    prj.write(in_srs.ExportToWkt())
    prj.close()

    return outfn


# 3. Buffer ice
def bufferice( inpfn, bufdist, sicdate ):

    # Get Shapefile driver from OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Open input file
    inds = driver.Open( inpfn, 0)

    # Get input layer
    inlay = inds.GetLayer()

    # Get input projection
    in_srs = inlay.GetSpatialRef()

    # Expecting one feature to apply the buffer to
    npoly = inlay.GetFeatureCount()
    if npoly > 1:
        print ("Too many (%d) polygons. Something went wrong with union." % npoly)

    # Get feature
    infeat = inlay.GetFeature(0)

    # Get feature geometry
    in_poly = infeat.GetGeometryRef()

    # Add the buffer
    buf_geom = in_poly.Buffer( (bufdist*1000.0) )

    # Destroy input feature
    infeat.Destroy()

    # Create an output Shapefile
    outfn = ("%s/sic_class_%4d%02d%02d_buffer.shp" % \
        (TMPDIR,sicdate.year,sicdate.month,sicdate.day))
    if os.path.exists(outfn):
        driver.DeleteDataSource(outfn)
    outds = driver.CreateDataSource(outfn)

    # Create output layer
    layer_name = ("sic_class_%4d%02d%02d_buffer" % \
        (sicdate.year,sicdate.month,sicdate.day))
    outlay = outds.CreateLayer(layer_name, \
        geom_type=ogr.wkbPolygon, srs=in_srs)
    # Create an output feature and set attributes
    featureDefn = outlay.GetLayerDefn()
    outfeat = ogr.Feature( featureDefn )
    outfeat.SetGeometry(buf_geom)

    # Write the feature to the layer
    outlay.CreateFeature(outfeat)

    # Destroy output feature
    outfeat.Destroy()

    # Close input and output files
    inds.Destroy()
    outds.Destroy()

    # Create a projection file
    prjfn = ("%s.prj" % outfn[:-4])
    # print prjfn
    prj = open(prjfn, 'w')
    prj.write(in_srs.ExportToWkt())
    prj.close()

    return outfn

