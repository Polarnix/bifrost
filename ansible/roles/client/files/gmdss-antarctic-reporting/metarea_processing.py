#!/usr/bin/python

# Name:          metarea_processing.py
# Purpose:       Processing of data for METAREAs.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import sys, os

import numpy as N

import osgeo.ogr as ogr
import osgeo.osr as osr

OUTDIR = '/home/bifrostanalyst/Static/METAREA'
TMPDIR = '/home/bifrostanalyst/tmp'
STATICDIR = '/home/bifrostanalyst/Static/METAREA'

def savepolygon( in_srs, poly_geom ):
    # Get Shapefile driver from OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Create an output Shapefile
    outfn = ("%s/test_polygon.shp" % TMPDIR)
    if os.path.exists(outfn):
        driver.DeleteDataSource(outfn)
    outds = driver.CreateDataSource(outfn)

    # Create output layer
    layer_name = "test"
    outlay = outds.CreateLayer(layer_name, \
        geom_type=ogr.wkbPolygon, srs=in_srs)
    # Create an output feature and set attributes
    featureDefn = outlay.GetLayerDefn()
    outfeat = ogr.Feature( featureDefn )
    outfeat.SetGeometry(poly_geom)

    # Write the feature to the layer
    outlay.CreateFeature(outfeat)

    # Destroy output feature
    outfeat.Destroy()

    # Close input and output files
    outds.Destroy()

    # Create a projection file
    prjfn = ("%s.prj" % outfn[:-4])
    # print prjfn
    prj = open(prjfn, 'w')
    prj.write(in_srs.ExportToWkt())
    prj.close()


def saveline( areano, pdate, in_srs, line_geom ):
    # Get Shapefile driver from OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Create an output Shapefile
    outfn = ("%s/ice%02d_%s_23.shp" % (OUTDIR,areano,pdate.strftime("%Y%m%d")))
    if os.path.exists(outfn):
        driver.DeleteDataSource(outfn)
    outds = driver.CreateDataSource(outfn)

    # Create output layer
    layer_name = ("ice%02d_%s" % (areano,pdate.strftime("%Y%m%d")))
    outlay = outds.CreateLayer(layer_name, \
        geom_type=ogr.wkbLineString, srs=in_srs)
    # Create an output feature and set attributes
    featureDefn = outlay.GetLayerDefn()
    outfeat = ogr.Feature( featureDefn )
    outfeat.SetGeometry(line_geom)

    # Write the feature to the layer
    outlay.CreateFeature(outfeat)

    # Destroy output feature
    outfeat.Destroy()

    # Close input and output files
    outds.Destroy()

    # Create a projection file
    prjfn = ("%s.prj" % outfn[:-4])
    # print prjfn
    prj = open(prjfn, 'w')
    prj.write(in_srs.ExportToWkt())
    prj.close()


def get_metarea( areano ):

    # Get Shapefile driver from OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Open source file
    # srcfn = ("%s/Antarctic-METAREA.shp" % OUTDIR)
    srcfn = ("%s/antarctic_metarea.shp" % OUTDIR)
    # print srcfn

    # Open input file
    inds = driver.Open( srcfn, 0)

    # Get input layer
    inlay = inds.GetLayer()

    # Get input projection
    in_srs = inlay.GetSpatialRef()

    # Create lists for METAREA data
    id_list = []
    name_list = []
    geom_list = []

    # Loop through METAREA polygons
    npoly = inlay.GetFeatureCount()
    for i in range(npoly):
        # Get feature
        infeat = inlay.GetFeature(i)

        # Get feature attributes
        in_metarea = infeat.GetFieldAsString('metarea')
        in_metarea = int(in_metarea)
        in_country = infeat.GetFieldAsString('country')
        in_id = infeat.GetFieldAsString('id')
        if len(in_id) > 0:
            in_id = int(in_id)
        else:
            in_id = 0
        in_name = infeat.GetFieldAsString('name')

        # Get feature geometry
        in_poly = infeat.GetGeometryRef()

        # If the right METAREA, add data to lists
        if in_metarea == areano:
            id_list.append( in_id )
            name_list.append( in_name )
            geom_list.append( in_poly.Clone() )

        # Destroy input feature
        infeat.Destroy()

    # Close input file
    inds.Destroy()

    return [ id_list, name_list, in_srs, geom_list ]


def clipmetarea( met_srs, subareas, icefn ):

    # Create union of METAREA components
    nsub = len( subareas )
    for i in range(nsub):
        if i == 0:
            metarea = subareas[i].Clone()
        else:
            metarea = metarea.Union( subareas[i] )
    # Remove holes
    nbits = metarea.GetGeometryCount()
    if nbits > 1:
        sglmet = ogr.Geometry(ogr.wkbPolygon)
        sglmet.AddGeometry( metarea.GetGeometryRef(0) )
    else:
        sglmet = metarea.Clone()
    del metarea

    # Get Shapefile driver from OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Open input file
    inds = driver.Open( icefn, 0)

    # Get input layer
    inlay = inds.GetLayer()

    # Get input projection
    in_srs = inlay.GetSpatialRef()

    # Expecting one feature to apply the buffer to
    npoly = inlay.GetFeatureCount()
    if npoly > 1:
        print ("Too many (%d) polygons. Something wrong." % npoly)

    # Get feature
    infeat = inlay.GetFeature(0)

    # Get feature geometry
    ice_poly = infeat.GetGeometryRef()

    # Coordinate transform, ice to METAREA
    coordtrans = osr.CoordinateTransformation( in_srs, met_srs )

    # Transfrom ice polygon coordinates
    ice_poly.Transform( coordtrans )

    # Calculate the intersection
    intersect = sglmet.Intersection( ice_poly )

    # Remove holes
    nbits = intersect.GetGeometryCount()
    if nbits > 1:
        # newpoly = ogr.Geometry(ogr.wkbPolygon)
        # newpoly.AddGeometry( intersect.GetGeometryRef(0) )
        newpoly = (intersect.GetGeometryRef(0)).Clone()
    else:
        newpoly = intersect.Clone()
    del intersect

    # Test validity of polygon
    # Sometimes we get a linear ring (Type=2), in which case it needs to be converted
    # to a polygon
    if newpoly.GetGeometryType() == 2:
        tmpring = newpoly.Clone()
        newpoly.Destroy()
        newpoly = ogr.Geometry(ogr.wkbPolygon)
        newpoly.AddGeometry( tmpring )        
        tmpring.Destroy()

    # Save result to file (for testing)
    # savepolygon( met_srs, newpoly )

    # Load landmask (not needed, easier to clip lines without it)
    # lndmskfn = ("%s/GSHHS_i_landmask.shp" % STATICDIR)
    # lndds = driver.Open( lndmskfn, 0)
    # lndlay = lndds.GetLayer()
    # lnd_srs = lndlay.GetSpatialRef()
    # Get feature
    # lndfeat = lndlay.GetFeature(0)
    # Get feature geometry
    # land_poly = lndfeat.GetGeometryRef()

    # Remove land areas from ice polygon
    # masked_poly = newpoly.Difference( land_poly )

    # Destroy input feature
    infeat.Destroy()
    # lndfeat.Destroy()

    # Close input files
    inds.Destroy()
    # lndds.Destroy()

    return [ sglmet, newpoly ]


def polytolines( srs, metpoly, icepoly ):

    # Get METAREA outline
    metline = metpoly.GetGeometryRef(0)
    # print metline.ExportToWkt()

    # Create list of points
    nmetp = metline.GetPointCount()
    # print nmetp
    metx = N.zeros((nmetp),dtype=N.float64)
    mety = N.zeros((nmetp),dtype=N.float64)
    for i in range(nmetp):
        metx[i] = metline.GetX(i)
        mety[i] = metline.GetY(i)

    # Get ice polygon outline
    iceline = icepoly.GetGeometryRef(0)
    # print iceline.ExportToWkt()

    # Create list of points
    nicep = iceline.GetPointCount()
    # print nicep
    icex = N.zeros((nicep),dtype=N.float64)
    icey = N.zeros((nicep),dtype=N.float64)
    for i in range(nicep):
        icex[i] = iceline.GetX(i)
        icey[i] = iceline.GetY(i)

    # Loop through ice line points and look for corresponding METAREA line points
    # This relies on us having created the ice polygon using the METAREA polygon,
    # so that they share the same point cooridnates
    newi = []
    newx = []
    newy = []
    firstflg = 1
    wrapflg = 0
    idx = 0
    for i in range(nicep):
        dx = metx - icex[i]
        dy = mety - icey[i]
        dist = N.sqrt( (dx*dx) + (dy*dy) )
        idxlist = N.nonzero(dist<10.0)
        if len(idxlist[0]) == 0:
            if firstflg == 1:
                oldi = i
                firstflg = 0 
            if (i-oldi) > 1:
                wrapflg = wrapflg + 1
                wrapidx = idx
            # print idx,icex[idx],icey[idx],dist[idx]
            newi.append(i)
            newx.append(icex[i])
            newy.append(icey[i])
            oldi = i
            idx = idx + 1

    # If we detect a wrap-around, we have to detangle
    np = len(newx)
    if wrapflg == 1:
        # print "Wrap-around detected."
        # print np, wrapidx
        # print newi[0],newx[0]
        # print newi[wrapidx-1],newx[wrapidx-1]
        # print newi[wrapidx],newx[wrapidx]
        # print newi[-1],newx[-1]
        finalx = []
        finaly = []
        for i in range(wrapidx,np):
            finalx.append(newx[i])
            finaly.append(newy[i])
        for i in range(wrapidx):
            finalx.append(newx[i])
            finaly.append(newy[i])
    elif wrapflg > 1:
        print "Really tangled!"
        sys.exit()
    else:
        finalx = newx
        finaly = newy
    del newx, newy

    # Create a linestring geometry
    icelimit = ogr.Geometry(ogr.wkbLineString)
    for i in range(len(finalx)):
        icelimit.AddPoint(finalx[i],finaly[i])
    # print icelimit.ExportToWkt()

    # Save geometry to Shapefile (for testing)
    # savelinestring( srs, icelimit )

    return icelimit


def simplify( srs, line, target ):

    np = line.GetPointCount()
    i = 1
    while np > target:
        dist = i * 100.0
        newline = line.Simplify( dist )
        np = newline.GetPointCount()
        # print i, dist, np
        i = i + 1

    # Save geometry to Shapefile (for testing)
    # savelinestring( srs, newline )

    return newline
