#!/usr/bin/python

# Name:          increase_subarea_nodes.py
# Purpose:       Increase the number of ice edge nodes within METAREA subareas.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import numpy as N

import osgeo.ogr as ogr
import osgeo.osr as osr

INPDIR='/home/nicholsh/Documents/OS_Production/QGIS'
OUTDIR='/home/nicholsh/Documents/OS_Production/Python/Outputs'

SHPLIST=[ 'METAREA-06_Argentina/metarea-06.shp', \
          'METAREA-07_South_Africa/metarea-07.shp', \
          'METAREA-10_Australia/metarea-10.shp', \
          'METAREA-14_New_Zealand/metarea-14.shp', \
          'METAREA-15_Chile/metarea-15.shp' ]

# Get Shapefile driver from OGR
driver = ogr.GetDriverByName('ESRI Shapefile')

# Create output Shapefile
outfn = ("%s/output_metarea.shp" % OUTDIR)
if os.path.exists(outfn):
    driver.DeleteDataSource(outfn)
outds = driver.CreateDataSource(outfn)

# Create output spatial reference
out_srs = osr.SpatialReference()
out_srs.ImportFromEPSG( 4326 )

# Create output layer
outlay = outds.CreateLayer('output_metarea', \
    geom_type=ogr.wkbPolygon)

# Create output field definitions
area_fdef = ogr.FieldDefn('metarea', ogr.OFTInteger)
outlay.CreateField(area_fdef)
country_fdef = ogr.FieldDefn('country', ogr.OFTString)
country_fdef.SetWidth(80)
outlay.CreateField(country_fdef)
id_fdef = ogr.FieldDefn('id', ogr.OFTInteger)
outlay.CreateField(id_fdef)
name_fdef = ogr.FieldDefn('name', ogr.OFTString)
name_fdef.SetWidth(80)
outlay.CreateField(name_fdef)

# Loop through input Shapefiles
for shpfn in SHPLIST:
    inpfn = ("%s/%s" % (INPDIR,shpfn))
    # print inpfn

    areano = int(shpfn[8:10])
    country = shpfn[11:-15].replace('_',' ')

    # Open input file
    inds = driver.Open( inpfn, 0)

    # Get input layer
    inlay = inds.GetLayer()

    # Get input projection and create transform to
    # longitude/latitude EPSG:4326
    in_srs = inlay.GetSpatialRef()
    coordtrans = osr.CoordinateTransformation( in_srs, out_srs )

    # Loop through features
    npoly = inlay.GetFeatureCount()
    print areano, country, npoly
    for i in range(npoly):
        infeat = inlay.GetFeature(i)

        # Get the feature attributes 'id' and 'name'
        in_id = infeat.GetFieldAsString('id')
        in_name = infeat.GetFieldAsString('name')
        # print in_id, in_name

        # Create an output feature and set attributes
        featureDefn = outlay.GetLayerDefn()
        outfeat = ogr.Feature( featureDefn )
        outfeat.SetField('metarea',areano)
        outfeat.SetField('country',country)
        if len(in_id) > 0:
            outfeat.SetField('id',int(in_id))
        outfeat.SetField('name',in_name)

        # Create output polygon ring geometry
        outring = ogr.Geometry(ogr.wkbLinearRing)

        # Get feature geometry
        in_geom = infeat.GetGeometryRef()
        nring = in_geom.GetGeometryCount()
        inring = in_geom.GetGeometryRef(0)
        nnode = inring.GetPointCount()

        # Loop through polygon line segments
        for j in range(1,nnode):

            # Get segment start end end coordinates.
            # Convert to longitude/latitude
            x1 = inring.GetX(j-1)
            y1 = inring.GetY(j-1)
            point1 = ogr.Geometry(ogr.wkbPoint)
            point1.AddPoint( x1, y1 )
            point1.Transform( coordtrans )
            x1 = point1.GetX()
            y1 = point1.GetY()
            x2 = inring.GetX(j)
            y2 = inring.GetY(j)
            point2 = ogr.Geometry(ogr.wkbPoint)
            point2.AddPoint( x2, y2 )
            point2.Transform( coordtrans )
            x2 = point2.GetX()
            y2 = point2.GetY()
            # print x1, y1, x2, y2

            # Calculate segment length in km
            x1rad = N.radians(x1)
            y1rad = N.radians(y1)
            x2rad = N.radians(x2)
            y2rad = N.radians(y2)
            # print x1rad, y1rad, x2rad, y2rad
            # Haversince formula from 
            # http://williams.best.vwh.net/avform.htm#Dist
            erad = 6366.71
            d = 2.0 * N.arcsin( N.sqrt( \
                N.power( N.sin((y1rad-y2rad)/2.0), 2.0) + \
                N.cos(y1rad) * N.cos(y2rad) * \
                N.power( N.sin((x1rad-x2rad)/2.0), 2.0) ))
            d = d * erad

            # Add extra nodes about every 10 km
            if d > 0.001:
                nextra = int(d/10.0)
                dstep = d / float(nextra)
                # print d, nextra, dstep
                deltax = (x2 - x1) / d
                deltay = (y2 - y1) / d
                for k in range(nextra):
                    incd = float(k) * dstep
                    newx = x1 + (incd * deltax)
                    newy = y1 + (incd * deltay)
                    # print ("%3d %8.3f %7.3f" % (k,newx,newy))
                    outring.AddPoint(newx,newy)

        # Close the ring
        outring.CloseRings()

        # Create the output polygon and add to feature
        outpoly = ogr.Geometry(ogr.wkbPolygon)
        outpoly.AddGeometry(outring)
        outfeat.SetGeometry(outpoly)

        # Write the feature to the layer
        outlay.CreateFeature(outfeat)

        # Destroy input and output features
        infeat.Destroy()
        outfeat.Destroy()

    # Close input file
    inds.Destroy()

# Close output file
outds.Destroy()

# Create a projection file
prjfn = ("%s.prj" % outfn[:-4])
# print prjfn
prj = open(prjfn, 'w')
prj.write(out_srs.ExportToWkt())
prj.close()


