GMDSS reporting for Antarctic METAREAs

1. increase_subarea_nodes.py
Add more nodes to subarea polygons and creates a combined file - Antarctic-METAREA.shp

2. extract_ice_edge.py
Extract an ice edge line from Bremen AMSRE/AMSR2 or OSISAF SIC data. 