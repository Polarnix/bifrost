#!/usr/bin/python

# Name:          iceedge_report.py
# Purpose:       Generate text report of ice edge for GMDSS METAREAs.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Usage:
# python iceedge.py iceedge.shp [metarea number]

import sys
import string
from datetime import date
from ftplib import FTP
import zipfile

import osgeo.ogr as ogr
import osgeo.osr as osr

OUTDIR = '/home/bifrostanalyst/Documents'
TMPDIR = '/home/bifrostanalyst/tmp'
STATICDIR = '/home/bifrostanalyst/Static/GMDSS'


def get_authority( areano ):
    authorities = {  6: 'THE ARGENTINE NAVY METEOROLOGICAL SERVICE', \
                     7: 'THE SOUTH AFRICAN WEATHER SERVICE', \
                    10: 'THE BUREAU OF METEOROLOGY TASMANIA', \
                    14: 'THE METEOROLOGICAL SERVICE OF NEW ZEALAND', \
                    15: 'THE CHILEAN NAVY WEATHER SERVICE', \
                    17: 'THE CANADIAN ICE SERVICE', \
                    18: 'THE CANADIAN ICE SERVICE', \
                    19: 'THE NORWEGIAN ICE SERVICE', \
                    20: 'THE ARCTIC AND ANTARCTIC RESEARCH INSTITUTE', \
                    21: 'THE ARCTIC AND ANTARCTIC RESEARCH INSTITUTE' }
    authority = authorities[areano]

    contacts = {  6: 'HIELO.MARINO@GMAIL.COM', \
                  7: 'johan.stander@weathersa.co.za', \
                 10: 'N.Moodie@bom.gov.au', \
                 14: 'Steve.Ready@metservice.com', \
                 15: 'gespinosa@directemar.cl', \
                 17: 'CIS-SCG-CLIENT@EC.GC.CA', \
                 18: 'CIS-SCG-CLIENT@EC.GC.CA', \
                 19: 'ISTJENESTEN@MET.NO', \
                 20: 'SERVICE@AARI.NW.RU', \
                 21: 'SERVICE@AARI.NW.RU' }
    email = contacts[areano]

    return [ authority, email ]
    

def report_ant( areano, pdate, met_srs, idlist, namelist, polywkblist, iceline, ftpflg ):

    # Convert geometries to longitude/latitude coordinates
    ll_srs = osr.SpatialReference()
    ll_srs.ImportFromProj4('+proj=longlat +ellps=WGS84')
    coordtrans = osr.CoordinateTransformation( met_srs, ll_srs )
    iceline.Transform( coordtrans )
    # print iceline.ExportToWkt()
    for i in range(len(polywkblist)):
        polywkblist[i].Transform( coordtrans )
        # print polywkblist[i].ExportToWkt()

    # Extract line point data
    edgex = []
    edgey = []
    geom = iceline.Clone()
    np = geom.GetPointCount()
    for i in range(np):
        # print "%10.5f, %10.5f" % ( geom.GetX(i), geom.GetY(i) )
        edgex = edgex + [ geom.GetX(i) ]
        edgey = edgey + [ geom.GetY(i) ]
    # print edgex
    # print edgey

    # Open output file
    rephr = 23
    foutname = ('%s/ice%02d_%s_%02d.txt' % (OUTDIR,areano,pdate.strftime("%Y%m%d"),rephr) )
    fout = open(foutname, 'w')

    # Header information
    [ authority, email ] = get_authority( areano )
    fout.write(("ICE BULLETIN FOR METAREA %d ISSUED BY %s\n" % (areano,authority)))
    fout.write("(%s) AT %02d UTC %d %s\n" % ( email,rephr,pdate.day,string.upper(pdate.strftime("%b")) ))

    # print len(idlist)
    # print len(namelist)
    # print len(polywkblist)

    npoly = len(idlist)
    polyxlist = []
    polyylist = []
    for i in range(npoly):    
        tmpx = []
        tmpy = []
        ring = polywkblist[i].GetGeometryRef(0)
        np = ring.GetPointCount()
        # print np
        for j in range(np):
            # print "%10.5f, %10.5f" % ( geom.GetX(j), geom.GetY(j) )
            lon, lat, z = ring.GetPoint(j)
            tmpx = tmpx + [ lon ]
            tmpy = tmpy + [ lat ]
        # print tmpx
        # print tmpy
        polyxlist = polyxlist + [ tmpx ]
        polyylist = polyylist + [ tmpy ]
    # print polyxlist
    # print polyylist

    # sortedlist = sorted(idlist)
    # print sortedlist

    # Loop through polygons
    # for i in sorted(idlist):
    for i in range(len(idlist)):

        # Get position of polygon in list (sorting by ID)
        idx = -9999
        # for j in range(npoly):
        #     if idlist[j] == i:
        #         idx = j
        idx = i
        # print idx, idlist[idx], namelist[idx]

        # Create polygon geometry
        ring = ogr.Geometry(ogr.wkbLinearRing)
        polyx = polyxlist[idx]
        polyy = polyylist[idx]
        for j in range(len(polyx)):
            # print polyx[j], polyy[j]
            ring.AddPoint(polyx[j], polyy[j])
        polygon = ogr.Geometry(ogr.wkbPolygon)
        polygon.AddGeometry(ring)

        # print polygon.ExportToWkt()

        # Loop through points in line, and see if they fall within polygon
        within = 0
        firstflg = 1
        segmentn = 0
        listn = 0
        segstr = []
        xlist = []
        ylist = []
        for j in range(len(edgex)):
            point = ogr.Geometry(ogr.wkbPoint)
            point.SetPoint_2D(0,edgex[j], edgey[j])
            within = int(point.Within(polygon))
            # print point, point.Within(polygon), firstflg, within
            if firstflg == 1:
                firstflg = 0
                if within == 1:
                    xlist = xlist + [ edgex[j] ]
                    ylist = ylist + [ edgey[j] ]
                    segmentn = segmentn + 1
                    segstr = segstr + [ listn ]
                    listn = listn + 1
            else:
                line = ogr.Geometry(ogr.wkbLineString)
                line.AddPoint(edgex[j-1], edgey[j-1])
                line.AddPoint(edgex[j], edgey[j])
                if oldwithin == 0 and within == 1:
                    # print "intersect entering"
                    intersect = line.Intersection(polygon)
                    # print line
                    # print intersect
                    # print intersect.GetX(0), intersect.GetY(0)
                    xlist = xlist + [ intersect.GetX(0) ]
                    ylist = ylist + [ intersect.GetY(0) ]
                    xlist = xlist + [ intersect.GetX(1) ]
                    ylist = ylist + [ intersect.GetY(1) ]
                    segmentn = segmentn + 1
                    segstr = segstr + [ listn ]
                    listn = listn + 2
                elif oldwithin == 1 and within == 1:
                    # print "within"
                    xlist = xlist + [ edgex[j] ]
                    ylist = ylist + [ edgey[j] ]
                    listn = listn + 1
                elif oldwithin == 1 and within == 0:
                    # print "intersect exiting"
                    intersect = line.Intersection(polygon)
                    # print line
                    # print intersect
                    xlist = xlist + [ intersect.GetX(1) ]
                    ylist = ylist + [ intersect.GetY(1) ]
                    listn = listn + 1
                line.Destroy()
            point.Destroy()


            oldwithin = within

        # if segmentn > 0:
        #     print 'Segments = ', segmentn
        #     print segstr
        #     print listn
 
        polygon.Destroy()

        # print ' '
        # print xlist
        # print ylist

        # If point in sub-area, print report
        if len(xlist) > 0:

            fout.write(("\n%s\n" % namelist[idx].upper()))

            first = 1
            for segment in range(segmentn):
                if first == 1:
                    coordline = 'ICE S OF '
                    first = 0
                    limit = 3
                else:
                    coordline = ''
                    limit = 4
                nlin = 0
                start = segstr[segment]
                if (segment+1) >= segmentn:
                    end = len(xlist)
                else:
                    end = segstr[segment+1]
                # print start, end
                for j in range(start,end):
                    lat = ylist[j]
                    latdeg = abs(int(lat))
                    latmin = int((lat % 1) * 60)
                    # print lat, latdeg, latmin
                    if lat < 0:
                        latstr = ('%02d%02dS' % (latdeg,latmin))
                    else:
                        latstr = ('%02d%02dN' % (latdeg,latmin))
                    lon = xlist[j]
                    londeg = abs(int(lon))
                    lonmin = int((lon % 1) * 60)
                    if lon < 0:
                        lonstr = ('%03d%02dW' % (londeg,lonmin))
                    else:
                        lonstr = ('%03d%02dE' % (londeg,lonmin))
                    coordline = coordline + latstr + ' ' + lonstr
                    if j < (end-1):
                        coordline = coordline + ', '
                    nlin = nlin + 1
                    if nlin == limit:
                        fout.write(("%s\n" % coordline))
                        coordline = ''
                        nlin = 0
                if nlin > 0:
                    fout.write(("%s\n" % coordline))
                if segment < (segmentn-1):
                    fout.write("ICE EDGE EXITS AREA AND RE-ENTERS AT\n")

    fout.write("\nICE EDGE NOT FOR NAVIGATIONAL PURPOSES\n")
    fout.close()

    # sys.exit()
    
    return foutname


def send_to_aari( areano, foutname ):

    # Send results to AARI GMDSS site
    if metarea == 1 or metarea == 19:
        # Create a zip file containing the Shapefile
        zipfname = ('%s/ice%02d_%s_23.zip' % (DATADIR,metarea,today.strftime("%Y%m%d")) )
        shpfname = lineshp
        dbffname = shpfname[:-4]+'.dbf'
        shxfname = shpfname[:-4]+'.shx'
        # print zipfname
        edgezip = zipfile.ZipFile(zipfname,'w')
        edgezip.write(dbffname)
        edgezip.write(shpfname)
        edgezip.write(shxfname)
        edgezip.close()

        # Transfer files
        username = ('metarea%02d_ice' % metarea)
        password = ('metarea%02dgmdss' % metarea)
        # print username, password
        try:
            aari = FTP(host='gmdss.aari.ru',user=username,passwd=password)
        except:
            print 'Problem with FTP transfer to AARI'
        else:
            status = aari.cwd(('bull/%02d' % metarea))
            targdir=today.strftime("%Y%m%d")
            entries = aari.nlst()
            present = 0
            for fname in entries:
                if targdir == fname:
                    present = 1
            # print present
            if present == 0:
                status = aari.mkd(targdir)
                # print status
            status = aari.cwd(targdir)
            # Send the text file
            fin = open(foutname,'r')
            storname = ('ice%02d_%s_23.txt' % (metarea,today.strftime("%Y%m%d")) )
            cmd = ("STOR %s" % storname)
            # print cmd
            status = aari.storlines(cmd,fin)
            fin.close()
            # Send the Shapefile file zipped
            fin = open(zipfname,'r')
            storname = ('ice%02d_%s_23.zip' % (metarea,today.strftime("%Y%m%d")) )
            cmd = ("STOR %s" % storname)
            # print cmd
            status = aari.storbinary(cmd,fin)
            fin.close()
            aari.quit()
