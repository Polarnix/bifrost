#/usr/bin/python

# Name:          extract_ice_edge.py
# Purpose:       Extract ice edge information from PMW data for GMDSS METAREA reporting.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import sys, os
from datetime import date

import osgeo.ogr as ogr

from get_SIC_raster import reprocessed,osisaf,amsr
from icecover import icecover, filterunion, bufferice
from metarea_processing import get_metarea, clipmetarea, polytolines, simplify, saveline
from iceedge_report import report_ant

# Get input parameters
if len(sys.argv) >= 3:
    datestr = sys.argv[1]
    srcstr = sys.argv[2]
    if len(sys.argv) == 4:
        areastr = sys.argv[3]
    else:
        areastr = 'all'
else:
    print "Usage: python extract_ice_edge.py yyyymmdd amsr/osisaf [areas]"
    sys.exit()

# Check inputs are valid
# 1. Date
year = int(datestr[0:4])
month = int(datestr[4:6])
day = int(datestr[6:8])
pdate = date(year,month,day)
if pdate >= date.today():
    print "Date must be before today."
    sys.exit()
# 2. Source
if srcstr == 'amsr':
    if pdate >= date(2002,6,19) and pdate <= date(2011,10,4):
        satstr = 'amsre'
    elif pdate > date(2012,7,24):
        satstr = 'amsr2'
    else:
        print ("Date %4d-%02d-%02d is outside the range for AMSR-E or AMSR2" \
            % (year,month,day))
        sys.exit()
elif srcstr == 'osisaf':
    if pdate >= date(1978,10,25) and pdate <= date(2009,10,31):
        satstr = 'reprocessed'
    elif pdate > date(2009,11,1):
        satstr = 'osisaf'
    else:
        print ("Date %4d-%02d-%02d is outside the range for OSI SAF" \
            % (year,month,day))
        sys.exit()
else:
    print ("Unrecognised source: %s" % srcstr)
    sys.exit()
# 3. Areas
if areastr == 'all':
    arealist = [ 6, 7, 10, 14, 15 ]
else:
    checklist = [ 6, 7, 10, 14, 15 ]
    tmplist = areastr.split(',')
    arealist = []
    for numstr in tmplist:
        areano = int(numstr)
        check = 0
        for checkno in checklist:
            if areano == checkno:
                check = areano
        if check != 0:
            arealist.append(areano)
        else:
            print ("METAREA-%02d not valid for this analysis" % areano)
            sys.exit()
    arealist = sorted(arealist)

# Part 1. Download SIC data (Bremen/OSI SAF)
#        Reduce raster to limited number of ice classes, e.g. NIS SIC
#        Limits results to ice greater than 10%
if satstr == 'amsre':
    sicfn = amsr( pdate, satstr )
elif satstr == 'amsr2':
    sicfn = amsr( pdate, satstr )
elif satstr == 'reprocessed':
    sicfn = reprocessed( pdate )
elif satstr == 'osisaf':
    sicfn = osisaf( pdate )
# print sicfn

# Part 2. Create ice mask polygon ('gdal_polygonize')
# Convert SIC raster to polygons
icecovfn = icecover( sicfn, pdate )
# Merge SIC polygons into one, and filter by excluding
#    obvious land areas such as South America and islands.
icemapfn = filterunion( icecovfn, pdate )
# Buffer by 50 km
icebuffn = bufferice( icemapfn, 50.0, pdate )
# Remove temporary files
driver = ogr.GetDriverByName('ESRI Shapefile')
if os.path.exists(icecovfn):
    driver.DeleteDataSource(icecovfn)
if os.path.exists(icemapfn):
    driver.DeleteDataSource(icemapfn)

# Loop through METAREAs
for areano in arealist:
    # print areano

    # Part 3. Get METAREA data
    [ ids, names, metsrs, geoms ] = get_metarea( areano )
    # print len(ids)
    # print names

    # Part 4. Clip to METAREA
    # Get single polygons for the METAREA and ice coverage
    [ metpoly, icepoly ] = clipmetarea( metsrs, geoms, icebuffn )

    # Part 5. Calculate % of sub-areas covered

    # Part 6. Get outer line and simplify to 40 coordinates
    # Convert to lines and difference ice using METAREA
    iceline = polytolines( metsrs, metpoly, icepoly )
    iceline40 = simplify( metsrs, iceline, 40 )
    shpfname = saveline( areano, pdate, metsrs, iceline40 )

    # Part 7. Generate text for sub-areas.
    report_ant( areano, pdate, metsrs, ids, names, geoms, iceline40, 0 )

    # sys.exit()
    
    del ids, names, geoms, iceline40




