Ice charting Python scripts

01_startup.py
Creates daily ice chart analysis files and adds entry to production database. Uses copy_ice.

copy_ice.py
Copies ice chart data for new ice chart.

exclusion_list.txt
List of public holidays in Norway. Used to regulate routine weekday production.

02_livedata.py
Copies satellite data to LiveData folder to increase speed (i.e. not having to access over network)

03_validate.py
Check ice chart for errors. Uses geoemtry_functions.py, validity.py and prepair.

geometry_functions.py
Functions to work with ice chart geometries.

validity.py
Check polygon validity.

prepair
Try to identify errors in polygons that are missed by OGR and QGIS.

04_finish.py
Upload ice chart to database.

05_prodcheck.py
Check on ice chart production status.

06_clearcache.py
Archive analyst files older than 1 month and delete from client.
