#!/usr/bin/python

# Name:          copy_ice.py
# Purpose:       Routine to copy overall ice extent and fast ice extent from a previous ice chart.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import sys, os

from datetime import datetime

import osgeo.ogr as ogr
import osgeo.osr as osr

import pg

# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)
TMPDIR='/disk1/Istjenesten/New_Production_System/tmp'

# Polygon buffer distance (1 mm)
BUFFER = 0.001

# Create an extent polgon
def create_extent_poly(EXTENT):
    # Define number of x and y points
    res = 10000.0
    nx = int( (EXTENT[2] - EXTENT[0]) / res )
    ny = int( (EXTENT[1] - EXTENT[3]) / res )

    # Create an outer ring for the polygon
    outring = ogr.Geometry(ogr.wkbLinearRing)
    ypos = EXTENT[1]
    for i in range(nx):
        xpos = float( EXTENT[0] + (i * res) )
        outring.AddPoint( xpos, ypos )
    xpos = EXTENT[2]
    for i in range(ny):
        ypos = float( EXTENT[1] - (i * res) )
        outring.AddPoint( xpos, ypos )
    ypos = EXTENT[3]
    for i in range(nx):
        xpos = float( EXTENT[2] - (i * res) )
        outring.AddPoint( xpos, ypos )
    xpos = EXTENT[0]
    for i in range(ny):
        ypos = float( EXTENT[3] + (i * res) )
        outring.AddPoint( xpos, ypos )
    outring.CloseRings()

    # Create the polygon
    polygon = ogr.Geometry( ogr.wkbPolygon )
    polygon.AddGeometry( outring )

    return polygon


def copy_ice( proddir, regionstr, input_dt, output_dt, output_hr ):

    # Validity check
    if input_dt >= output_dt:
        print 'copy_ice: Input date later than output date. Stopping.'
        sys.exit()

    # Region specific
    if regionstr == 'ARC':
        regsql = 'ST_Y(ST_Centroid(polygon)) > 0.0'
        proj4str = '+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84'
        regcode = 'ARC'
        # Full extent of ice chart as ULLR coordinates
        EXTENT = [ -2600000.0, 400000.0, 2621073.0, -4700000.0 ]
    elif regionstr == 'ANT':
        regsql = 'ST_Y(ST_Centroid(polygon)) < 0.0'
        proj4str = '+proj=stere +lat_0=90s +lon_0=0e +lat_ts=90s +ellps=WGS84 +datum=WGS84'
        regcode = 'ANT'
        # Full extent of ice chart as ULLR coordinates
        EXTENT = [ -4296642.0, 3500000.0, 1000000.0, -100000.0 ]
    else:
        print 'copy_ice: Unknown region. Stopping.'
        sys.exit()

    # Get Bifrost server credentials from file
    bifrostfn = ("%s/bifrost_credentials.txt" % SETDIR)
    credentials = get_bifrost_credentials( bifrostfn )   

    # Connect to ice charts database
    dbcon = pg.connect(dbname='Icecharts', host=credentials['BIFROST_SERVER_IP'], user='bifrostadmin', passwd='bifrostadmin')

    # Get ice chart polygons
    sqltxt = "SELECT ST_AsText(polygon),norway_iceclass,si3ct,si3ca,si3sa,si3fa,si3cb,si3sb,si3fb,si3fp,si3fs FROM icechart_polygons"
    sqltxt = ("%s WHERE datetime = \'%s\'" % (sqltxt,input_dt.strftime('%Y-%m-%d %H:%M:%S')))
    sqltxt = ("%s AND %s" % (sqltxt,regsql))
    sqltxt = ("%s ORDER BY id;" % sqltxt)
    print sqltxt
    queryres = dbcon.query(sqltxt)
    icedata = queryres.getresult()
    npoly = len(icedata)
    # print icedata
    print npoly

    # Close database connection
    dbcon.close()

    # Setup map projections and transforms
    ll_srs = osr.SpatialReference()
    ll_srs.ImportFromEPSG(4326)
    output_srs = osr.SpatialReference()
    output_srs.ImportFromProj4(proj4str)
    ll2output = osr.CoordinateTransformation(ll_srs,output_srs)
    # output2ll = osr.CoordinateTransformation(output_srs,ll_srs)

    # Set up full ice chart extent
    extent_poly = create_extent_poly(EXTENT)
    extent_copy = extent_poly.Clone()

    # Loop through ice chart polygons and 
    # 1) Substract from extent polygon
    # 2) Add to overall ice polygon
    firstflg = 1
    for i in range(npoly):
        # Unpack database record
        [ polywkt, norwaystr, si3ct, si3ca, si3sa, si3sa, si3cb, si3sb, si3fb, si3fp, si3fs ] \
            = icedata[i]

        # Create geometry from WKT, and transform to output map projection
        # NB: Add buffer to prevent slivers in outputs
        icepoly = ogr.CreateGeometryFromWkt(polywkt)
        icepoly.Transform( ll2output )
        icepoly.Buffer( BUFFER )

        # Subtract any ice cover from extent, ignoring any Ice Free polygons
        if norwaystr != 'Ice Free' and si3ct != 0:
            # Construct ice free area by subtracting ice cover
            extent_poly = extent_poly.Difference( icepoly )

            # Construct overall ice cover, also ignore any Fast Ice polygons
            if norwaystr != 'Fast Ice' and si3fp != 8:
                # If first run just copy geometry to a multipolygon, otherwise append
                if firstflg == 1:
                    overall = ogr.Geometry(ogr.wkbMultiPolygon)
                    firstflg = 0
                overall.AddGeometry( icepoly.Clone() )
                # print i, norwaystr, si3ct, si3ca, si3sa, si3fb, si3fp, si3fs, \
                #     int(icepoly.Area()/(1000.0*1000.0)), int(overall.Area()/(1000.0*1000.0))

        # Destroy input geometry
        icepoly.Destroy()


    # Union overall multipolygon against itself to dissolve
    # overall = overall.Union(overall)
    for i in range(overall.GetGeometryCount()):
        polybit = overall.GetGeometryRef(i)
        if i == 0:
            tmpoverall = polybit.Clone()
        else:
            tmpoverall = tmpoverall.Union( polybit )
    # print tmpoverall
    # Reduce so that overall polygon is within original extent
    overall = tmpoverall.Intersection(extent_copy)

    # Create new ice polygons Shapefile

    # Define Shapefile as the format using OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Create new polygons shapefile
    datestr = output_dt.strftime('%Y%m%d')
    polyfn = ("%s/ice_%s_%02d_polygons_%s.shp" % (proddir,datestr,output_hr,regcode))
    if os.path.exists( polyfn ):
        driver.DeleteDataSource( polyfn )
    poly_ds = driver.CreateDataSource( polyfn )
    poly_lay = poly_ds.CreateLayer( datestr, srs=output_srs, \
        geom_type=ogr.wkbPolygon )
    totalfld = ogr.FieldDefn( 'iceconc', ogr.OFTInteger )
    poly_lay.CreateField( totalfld )
    conc1fld = ogr.FieldDefn( 'iceconc1', ogr.OFTInteger )
    poly_lay.CreateField( conc1fld )
    type1fld = ogr.FieldDefn( 'icetype1', ogr.OFTInteger )
    poly_lay.CreateField( type1fld )
    conc2fld = ogr.FieldDefn( 'iceconc2', ogr.OFTInteger )
    poly_lay.CreateField( conc2fld )
    type2fld = ogr.FieldDefn( 'icetype2', ogr.OFTInteger )
    poly_lay.CreateField( type2fld )
    featdefn = poly_lay.GetLayerDefn()

    # Create an ice free extent features
    nextent = extent_poly.GetGeometryCount()
    revcount = 0
    for i in range(nextent):
        polybit = extent_poly.GetGeometryRef(i)
        # print polybit.ExportToWkt()
        # print polybit.Area()
        if polybit.Area() > 1.0:
            ext_feat = ogr.Feature( featdefn )
            ext_feat.SetField( 'iceconc', 0 )
            ext_feat.SetField( 'iceconc1', 0 )
            ext_feat.SetField( 'icetype1', 0 )
            ext_feat.SetField( 'iceconc2', 0 )
            ext_feat.SetField( 'icetype2', 0 )
            ext_feat.SetGeometry( polybit )
            poly_lay.CreateFeature( ext_feat )
            ext_feat.Destroy()
            revcount = revcount + 1
    # print nextent, revcount

    # Create an ice covered area feature
    if overall.GetGeometryName() == 'POLYGON':
        # print overall.ExportToWkt()
        # print overall.Area()
        cvr_feat = ogr.Feature( featdefn )
        cvr_feat.SetField( 'iceconc', 0 )
        cvr_feat.SetField( 'iceconc1', 0 )
        cvr_feat.SetField( 'icetype1', 0 )
        cvr_feat.SetField( 'iceconc2', 0 )
        cvr_feat.SetField( 'icetype2', 0 )
        cvr_feat.SetGeometry( overall )
        poly_lay.CreateFeature( cvr_feat )
        cvr_feat.Destroy()
    elif overall.GetGeometryName() == 'MULTIPOLYGON':
        ncover = overall.GetGeometryCount()
        # print ncover
        for i in range(ncover):
            polybit = overall.GetGeometryRef(i)
            # print polybit.ExportToWkt()
            # print polybit.Area()
            cvr_feat = ogr.Feature( featdefn )
            cvr_feat.SetField( 'iceconc', 0 )
            cvr_feat.SetField( 'iceconc1', 0 )
            cvr_feat.SetField( 'icetype1', 0 )
            cvr_feat.SetField( 'iceconc2', 0 )
            cvr_feat.SetField( 'icetype2', 0 )
            cvr_feat.SetGeometry( polybit )
            poly_lay.CreateFeature( cvr_feat )
            cvr_feat.Destroy()
        # print ncover
    elif overall.GetGeometryName() == 'GEOMETRYCOLLECTION':
        ncover = overall.GetGeometryCount()
        # print ncover
        for i in range(ncover):
            polybit = overall.GetGeometryRef(i)
            if polybit.GetGeometryName() == 'POLYGON':
                # print polybit.ExportToWkt()
                # print polybit.Area()
                cvr_feat = ogr.Feature( featdefn )
                cvr_feat.SetField( 'iceconc', 0 )
                cvr_feat.SetField( 'iceconc1', 0 )
                cvr_feat.SetField( 'icetype1', 0 )
                cvr_feat.SetField( 'iceconc2', 0 )
                cvr_feat.SetField( 'icetype2', 0 )
                cvr_feat.SetGeometry( polybit )
                poly_lay.CreateFeature( cvr_feat )
                cvr_feat.Destroy()
        # print ncover

    # Add Fast Ice polygons
    nfast = 0
    for i in range(npoly):
        # Unpack database record
        [ polywkt, norwaystr, si3ct, si3ca, si3sa, si3sa, si3cb, si3sb, si3fb, si3fp, si3fs ] \
            = icedata[i]

        # Construct Fast Ice polygons
        if norwaystr == 'Fast Ice' or si3fp == 8:
            # Create geometry from WKT, and transform to output map projection
            icepoly = ogr.CreateGeometryFromWkt(polywkt)
            icepoly.Transform( ll2output )
            icepoly.Difference( overall )

            # Create a Fast Ice feature
            fast_feat = ogr.Feature( featdefn )
            fast_feat.SetField( 'iceconc', 6 )
            fast_feat.SetField( 'iceconc1', 10 )
            fast_feat.SetField( 'icetype1', 17 )
            fast_feat.SetField( 'iceconc2', 0 )
            fast_feat.SetField( 'icetype2', 0 )
            fast_feat.SetGeometry( icepoly )
            poly_lay.CreateFeature( fast_feat )
            fast_feat.Destroy()

            # Destroy input geometry
            icepoly.Destroy()

            nfast = nfast + 1
    # print nfast

    # Close Shapefile
    poly_ds.Destroy()

    return polyfn


# Main function
if __name__ == "__main__":

    regionstr = 'ANT'
    yesterday = datetime(2015,11,23,15,0,0)
    today = datetime(2015,11,30,15,0,0)

    polyfn = copy_ice( TMPDIR, regionstr, yesterday, today )
