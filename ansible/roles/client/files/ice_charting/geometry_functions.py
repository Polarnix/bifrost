#!/usr/bin/python

# Name:          geometry_functions.py
# Purpose:       Geometry validity tests.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Dependencies:  Now uses prepair (Polygon Repair) to help detect bad polygons.
#                prepair generates a MULTIPOLYGON, which then fails normal OGR checks.
#                See Ledoux, H., Arroyo Ohori, K., and Meijers, M. (2014). A triangulation-based approach to 
#                automatically repair GIS polygons. Computers & Geosciences 66:121-131.

import os, sys
from subprocess import Popen, PIPE
# import subprocess
import shlex
from datetime import datetime
import copy

import osgeo.ogr as ogr
import osgeo.osr as osr

import shapely.wkt
# from shapely.geometry import Polygon
from shapely.validation import explain_validity

import numpy as N

from get_map_projections import get_map_projections


# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)
PYDIR = ("%s/Python/Bifrost" % BASEDIR)
TMPDIR = ("%s/tmp" % BASEDIR)

# Count geometry features
def featcount( layer, layer_list ):

    sitlut = [ 'Ice Free', 'Open Water', 'Very Open Drift Ice', 'Open Drift Ice', \
        'Close Drift Ice', 'Very Close Drift Ice', 'Fast Ice' ]

    # Ice type list
    icetype = []
    feature = []
    hole = []
    icex = []
    icey = []

    layer.ResetReading()
    for lyr in layer_list:
        count = 0
        points = 0
        feat = lyr.GetNextFeature()

        sitidx = feat.GetFieldIndex('icetype')
        # print 'sitidx = ', sitidx
        concidx = feat.GetFieldIndex('iceconc')
        # print 'concidx = ', concidx

        while feat is not None:

            geom = feat.GetGeometryRef()
          
            # gtype = feat.GetFieldAsString(3)
            # print gtype 
            # print geom

            gtype = ''
            if geom != None:
                geomstr = geom.ExportToWkt()
                gtype = geomstr.split(' ')[0]
                # print gtype

            if gtype == 'POLYGON':
                count = count + 1
                feature.append(gtype)
                if concidx == -1:
                    if sitidx != -1:
                        icetype.append(feat.GetFieldAsString(sitidx))
                    else:
                        icetype.append('Unknown')
                else:
                    typeidx = feat.GetFieldAsInteger(concidx)
                    # print typeidx, feat.GetFieldAsString(concidx)
                    icetype.append(sitlut[typeidx])
                coord = geomstr
                bak_coord = coord
                coord = coord.split("((")
                coord = coord[1].split("))")[0]
            
                coord = coord.split("(")
                np = len(coord)
                if np == 1:
                    hole = hole + [0]
                    coord = coord[0].split(",")
                    xtmp = []
                    ytmp = []
                    # print coord
                    for i in range(len(coord)):
                        xy = coord[i].split(" ")
                        # print xy
                        xtmp.append(float(xy[0]))
                        ytmp.append(float(xy[1]))
                        points = points + 1
                    icex = icex + [xtmp]
                    icey = icey + [ytmp]        
                else:
                    hole = hole + [np]
                    parts = coord
                    partx = []
                    party = []
                    for p in range(np):
                        coord = parts[p].split(")")[0]
                        coord = coord.split(",")
                        xtmp = []
                        ytmp = []
                        for i in range(len(coord)):
                            xy = coord[i].split(" ")
                            xtmp.append(float(xy[0]))
                            ytmp.append(float(xy[1]))
                        points = points + 1
                        partx = partx + [xtmp]
                        party = party + [ytmp]        
                    icex = icex + [partx]
                    icey = icey + [party]
            elif gtype == 'MULTIPOLYGON':
                count = count + 1
                feature.append(gtype)
                if concidx == -1:
                    if sitidx != -1:
                        icetype.append(feat.GetFieldAsString(sitidx))
                    else:
                        icetype.append('Unknown')
                else:
                    typeidx = feat.GetFieldAsInteger(concidx)
                    icetype.append(sitlut[typeidx])
                coord = geomstr
                coord = coord.split("(((")
                coord = coord[1].split(")))")[0]
                polys = coord.split("((")
                # print 'polys\n', polys
                polyx = []
                polyy = []
                tmphole = []
                for p in range(len(polys)):
                    coord = polys[p].split("))")[0]
                    coord = coord.split("(")
                    np = len(coord)
                    # print 'multipolygon part', p, np
                    if np == 1:
                        tmphole = tmphole + [0]            
                        coord = coord[0].split(",")
                        xtmp = []
                        ytmp = []
                        # print coord
                        for i in range(len(coord)):
                            xy = coord[i].split(" ")
                            # print xy
                            xtmp.append(float(xy[0]))
                            ytmp.append(float(xy[1]))
                            points = points + 1
                        polyx = polyx + [xtmp]
                        polyy = polyy + [ytmp]        
                    else:
                        tmphole = tmphole + [np]            
                        parts = coord
                        partx = []
                        party = []
                        for p in range(np):
                            coord = parts[p].split(")")[0]
                            coord = coord.split(",")
                            xtmp = []
                            ytmp = []
                            for i in range(len(coord)):
                                xy = coord[i].split(" ")
                                xtmp.append(float(xy[0]))
                                ytmp.append(float(xy[1]))
                            partx = partx + [xtmp]
                            party = party + [ytmp]        
                            points = points + 1
                        polyx = polyx + [partx]
                        polyy = polyy + [party]
                hole = hole + [tmphole]
                icex = icex + [polyx]
                icey = icey + [polyy]
            elif gtype == '':
                pass
            else:
                print gtype
                print geom
                sys.exit()
            
            feat = lyr.GetNextFeature()
        
    print count, ' features'
    print points, ' points'
    # print len(icex)

    return [ count, points, icetype, feature, hole, icex, icey ]


# Get length of line feature
def GetLength( boundary ):
    pperi = 0
    peri_np = boundary.GetPointCount()
    if peri_np > 0:
        oldx = boundary.GetX(0)
        oldy = boundary.GetY(0)
        for i in range(peri_np):
            dx = boundary.GetX(i) - oldx
            dy = boundary.GetY(i) - oldy
            segdist = N.sqrt( (dx*dx) + (dy*dy) )
            pperi = pperi + segdist
            oldx = boundary.GetX(i)
            oldy = boundary.GetY(i)
    pperi = pperi / 1000.0

    return pperi


# Reproject features
def reproject( feat, srs, lon, lat, hole, direction, regionstr ):

    # Region specific settings, map projection and extent
    mapfn = ("%s/map_projections.txt" % SETDIR)
    map_projections = get_map_projections( mapfn )

    if regionstr == 'arctic':
        SRC_PROJ  = map_projections['SRS_ARCTIC_PROJ4']
    elif regionstr == 'antarctic':
        SRC_PROJ  = map_projections['SRS_ANTARCTIC_PROJ4']
    else:
        print ("geometry_functions-reproject: Unknown region %s" % regionstr)
        sys.exit()
    # SRC_PROJ = srs.ExportToProj4()
    NEW_PROJ = '+proj=longlat +ellps=WGS84'

    source = osr.SpatialReference()
    target = osr.SpatialReference()

    if direction == 0:
        source.ImportFromProj4(SRC_PROJ)
        target.ImportFromProj4(NEW_PROJ)
    else:
        source.ImportFromProj4(NEW_PROJ)
        target.ImportFromProj4(SRC_PROJ)

    s_wkt = source.ExportToWkt()
    # print 'Source:\n', s_wkt
    t_wkt = target.ExportToWkt()
    # print 'Target:\n', t_wkt
    
    transform = osr.CoordinateTransformation(source,target)

    x = copy.deepcopy(lon)
    y = copy.deepcopy(lat)

    for p in range(len(feat)):
        if feat[p] == 'POLYGON':
            # print 'test polygon', hole[p]
            if hole[p] == 0:
                for i in range(len(lon[p])):
                    ( x[p][i], y[p][i], alt ) = transform.TransformPoint(lon[p][i],lat[p][i],0)                
            else:
                for j in range(hole[p]):
                    for i in range(len(lon[p][j])):
                        ( x[p][j][i], y[p][j][i], alt ) = transform.TransformPoint(lon[p][j][i],lat[p][j][i],0)
        elif feat[p] == 'MULTIPOLYGON':
            npoly = len(lon[p])
            # print npoly
            for poly in range(npoly):
                # print 'test multipolygon', poly, hole[p]
                holelist = hole[p]
                if holelist[poly] == 0:
                    for i in range(len(lon[p][poly])):
                        ( x[p][poly][i], y[p][poly][i], alt ) = transform.TransformPoint(lon[p][poly][i],lat[p][poly][i],0)
                else:
                    for j in range(holelist[poly]):
                        for i in range(len(lon[p][poly][j])):
                            ( x[p][poly][j][i], y[p][poly][j][i], alt ) = transform.TransformPoint(lon[p][poly][j][i],lat[p][poly][j][i],0)

    return [ x, y ]


# Validation of polygon using Shapely
def shapelyValidation( polywkt ):

    # Construct a polygon object
    polygon = shapely.wkt.loads( polywkt )

    # Use Shapely explain_validity check to test polygon
    status = 0
    if explain_validity( polygon ) != 'Valid Geometry':
        # print polygon.wkt
        print explain_validity( polygon )
        status = 1

    return status


# Validation of polygon using prepair
def prepairValidation( polywkt ):

    # Use prepair to try to fix polygon
    # This generates a MULTIPOLYGON that should then fail the OGR check.
    cmd = ("/usr/local/bin/prepair")
    part1 = (" --wkt \'%s\'" % polywkt)
    # reswkt = os.system( fullcmd )
    # works! reswkt = subprocess.call(cmd + part1, shell=True)
    try:
        p1 = Popen(cmd+part1,stdout=PIPE,stderr=PIPE,shell=True)
        [reswkt,errors] = p1.communicate()
    except:
        # Popen will fail if the WKT is too long.
        # Work-around is to write WKT to a text file
        # print 'Work-around'
        txtfn = ("%s/prepair_wkt.txt" % TMPDIR)
        txt = open( txtfn, 'w' )
        txt.write( ("%s\n" % polywkt) )
        txt.close()
        part1 = (" -f \'%s\'" % txtfn)
        p1 = Popen(cmd+part1,stdout=PIPE,stderr=PIPE,shell=True)
        [reswkt,errors] = p1.communicate()
        os.remove(txtfn)
    # print reswkt

    # Now test the MULTIPOLYGON
    status = 0
    multipoly = ogr.CreateGeometryFromWkt( reswkt )
    if multipoly != None:
        if multipoly.IsValid() == False:
            status = 1
        else:
            mpoly2 = shapely.wkt.loads( reswkt )
            if explain_validity( mpoly2 ) != 'Valid Geometry':
                # print polygon.wkt
                print explain_validity( mpoly2 )
                status = 1

    else:
        print 'Geometry is empty'
        status = 1
    # print status

    return status

