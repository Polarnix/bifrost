#!/usr/bin/python

# Name:          get_bifrost_credentials.py
# Purpose:       Loads Bifrost credentials from settings and status files.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Notes:         Needs a plain text file containing the following 1 line:
#                BIFROST_SERVER_FN=
#                The routine then gets server IP address BIFROST_SERVER_IP from the file.

import os, sys
from datetime import datetime

def get_bifrost_credentials( bifrost_fn ):

    # Empty dictionary for values
    credentials = {}

    # Try to load file data, otherwise give an error and halt
    try:
        infile = open( bifrost_fn, 'r' )
    except:
        print "Bifrost login credentials are required!"
        print ("Unable to load from file %s" % bifrost_fn)
        sys.exit()
    else:
        for txtln in infile:
            bits = (txtln.strip()).split('=')
            keytxt = bits[0]
            keyval = bits[1]
            credentials[keytxt] = keyval
        infile.close()

    # Try to load further server information from the BIFROST_SERVER_FN file provided.
    try:
        statfile = open( credentials['BIFROST_SERVER_FN'], 'r' )
    except:
        print ("Bifrost server status file (%s) is not available." % credentials['BIFROST_SERVER_FN'])
        print "Unable to determine where the Bifrost server is located."
        sys.exit()
    else:
        txtln = statfile.readline()
        statfile.close()

        # Parse text line to get server information
        txtbits = (txtln.strip()).split(',')
        server_no = int(txtbits[0])
        server_ip = txtbits[1]
        file_dt = datetime.strptime( txtbits[2], '%Y-%m-%d %H:%M:%S.%f' )
        
        # Add information to credentials
        credentials['BIFROST_SERVER_NO'] = server_no
        credentials['BIFROST_SERVER_IP'] = server_ip

    return credentials


# Main routine for testing
if __name__ == '__main__':
    
    inpfn = '/home/bifrostanalyst/Settings/Bifrost/bifrost_credentials.txt'
    credentials = get_bifrost_credentials( inpfn )
    print credentials
    
