#!/usr/bin/python

# Name:          04_finish.py
# Purpose:       Finishes ice chart by injecting polygons into database. Sets flag to initiate production procedures.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, platform
from datetime import datetime
import copy
from shutil import copyfile
import time
import argparse

import pg

import osgeo.ogr as ogr
import osgeo.osr as osr

import numpy as N

from geometry_functions import featcount, reproject, GetLength
from get_bifrost_credentials import get_bifrost_credentials
from get_ip_address import get_ip_address
from get_map_projections import get_map_projections
from get_analyst_and_country import get_analyst_and_country


# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
INCDIR = ("%s/Include/Bifrost" % BASEDIR)
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)
ICEDIR = ("%s/ICS" % BASEDIR)

# Active flag. Set this to zero for debugging
active_flg = 1
# Ice chart flag. Set to zero so that we can only upload METAREA line
iceact_flg = 1
# METAREA line flag. Set to zero so that we can only upload ice chart data
metact_flg = 1


# Symbols dictionary
symbol_dict =  [ 'Unknown', \
                 'Belts', \
                 'Compacting', \
                 'Crack Area', \
                 'Crack Location', \
                 'Diverging', \
                 'Drift Direction', \
                 'Ice Displacement', \
                 'Ice Free', \
                 'Lead', \
                 'New Ice', \
                 'Rafting', \
                 'Ridges', \
                 'Ridges and Rubble', \
                 'Ridges-Hummocks', \
                 'Snow Cover', \
                 'Stage of Melting', \
                 'Stamukha', \
                 'Shearing', \
                 'Drift', \
                 'Growler', \
                 'Bergy Bit', \
                 'Iceberg (Unspecified)', \
                 'Iceberg (Small)', \
                 'Iceberg (Medium)', \
                 'Iceberg (Large)', \
                 'Iceberg (Very Large)', \
                 'Ice_Island', \
                 'Floeberg', \
                 'Suspect Berg', \
                 'Many Growlers', \
                 'Many Bergy Bits', \
                 'Many Icebergs (Unspecified)', \
                 'Many Icebergs (Small)', \
                 'Many Icebergs (Medium)', \
                 'Many Icebergs (Large)', \
                 'Many Icebergs (Very Large)', \
                 'Tabular Iceberg (Unspecified)', \
                 'Tabular Iceberg (Small)', \
                 'Tabular Iceberg (Medium)', \
                 'Tabular Iceberg (Large)', \
                 'Tabular Iceberg (Very Large)', \
                 'Many Tabular Icebergs (Unspecified)', \
                 'Many Tabular Icebergs (Small)', \
                 'Many Tabular Icebergs (Medium)', \
                 'Many Tabular Icebergs (Large)', \
                 'Many Tabular Icebergs (Very Large)' ]


# Date reformatting
def mkdate(datestr):
    return time.strptime(datestr,'%Y%m%d')


# Main function
if __name__ == "__main__":

    # Get today's date
    today = time.localtime()
    todaystr = time.strftime('%Y-%m-%d',today)
    # print todaystr

    # Get input parameters using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument( "region", help="Region for ice chart [arctic/antarctic]", type=str, \
        choices=['arctic','antarctic'])
    parser.add_argument("-d","--date",help="Date for ice chart [yyyymmdd]",type=mkdate,default=today)
    parser.add_argument("-t","--time",help="Valid hour for ice chart [hh]",type=int,default=-1, \
        choices=xrange(0,24))
    args = parser.parse_args()
    regionstr = args.region
    year = (args.date).tm_year
    month = (args.date).tm_mon
    day = (args.date).tm_mday
    if args.time == -1:
       hour = 15
    else:
        hour = args.time
    datestr = ("%4d%02d%02d" % (year,month,day))
    dtstamp = datetime( year, month, day, hour, 0, 0 )
    dow = dtstamp.isoweekday()
    if args.time == -1:
        autoflg = True
    else:
        autoflg = False

    # Set computer name
    hoststr = platform.node()

    # Get computer IP address
    ipstr = get_ip_address()

    # Get Bifrost server credentials from file
    bifrostfn = ("%s/bifrost_credentials.txt" % SETDIR)
    credentials = get_bifrost_credentials( bifrostfn )   
    
    # Set up database connection
    dbcon = pg.connect(dbname='Icecharts', host=credentials['BIFROST_SERVER_IP'], user='bifrostadmin', passwd='bifrostadmin')

    # Check current status of database
    sqltxt = "SELECT new_flg,valid_flg,finish_flg,metline_flg,id,valid_hour,charttype FROM production WHERE"
    sqltxt = ("%s icechart_date = \'%s\' AND computer = \'%s\' AND region = \'%s\'" \
        % (sqltxt,datestr,ipstr,regionstr))
    checkres = dbcon.query(sqltxt)
    current = checkres.ntuples()
    if current == 1:
        new_flg = checkres.getresult()[0][0]
        valid_flg = checkres.getresult()[0][1]
        finish_flg = checkres.getresult()[0][2]
        metline_flg = checkres.getresult()[0][3]
        icechart_id = checkres.getresult()[0][4]
        valid_hour = int( checkres.getresult()[0][5] )
        chart_type = checkres.getresult()[0][6]
    else:
        if current == 0:
            print "04_finish: No production database entry for this date. Stopping."
        else:
            print ("04_finish: Too many (%d) database entries for this date. Stopping." \
                % current)
        sys.exit()
    # print current, new_flg, finish_flg, icechart_id
    # Check new_flg is set and finish_flg is unset for this date
    # If finish_flg is set, find out how many polygons are already in database
    dtstr = dtstamp.strftime('%Y-%m-%d %H:%M:%S')
    if finish_flg > 0:
        # Get number of polygons in database
        if regionstr == 'arctic':
            checkpoly = dbcon.query(("SELECT id FROM icechart_polygons WHERE datetime = \'%s\' AND ST_Y(ST_Centroid(polygon)) > 0.0;" \
                % dtstr ))
        else:
            checkpoly = dbcon.query(("SELECT id FROM icechart_polygons WHERE datetime = \'%s\' AND ST_Y(ST_Centroid(polygon)) < 0.0;" \
                % dtstr ))
        npoly = checkpoly.ntuples()
        print ("04_finish: There are already %d polygons in the database. These should be deleted. Stopping." % npoly)
        sys.exit()
    else:
        if new_flg == 0:
            print "04_finish: Somehow there is a database entry, but new_flg is not set. Stopping."
            sys.exit()
    if valid_flg != 1:
        print "04_finish: Ice chart polygons have not passed validation. Stopping."
        sys.exit()

    # If the program reaches here, then we are good to inject polygons into the database

    # Get maximum value of ID
    maxidres = dbcon.query('SELECT MAX(ID) FROM icechart_polygons;')
    maxid = maxidres.getresult()[0][0]
    if not maxid:
        maxid = -1
    # print maxid

    # Counters for polygons and lines, also act as flags
    newcount = 0
    npoints = 0

    # Set regional values
    if regionstr == 'arctic':
        regcode = 'ARC'
    elif regionstr == 'antarctic':
        regcode = 'ANT'

    # Region specific settings, map projection and extent
    mapfn = ("%s/map_projections.txt" % SETDIR)
    map_projections = get_map_projections( mapfn )

    # Only process if iceact_flg is not zero
    if iceact_flg > 0:

        # Ice chart filename
        icefn = ("%s/%s_%02d_%s/ice_%s_%02d_polygons_%s.shp" \
            % (ICEDIR,datestr,valid_hour,regcode,datestr,valid_hour,regcode))
        print icefn

        # Create backup copy of Shapefile in case tidying up (REPACK) step causes problems
        shpbits = [ 'shp', 'shx', 'dbf', 'shx' ]
        rootfn = icefn[:-4]
        for bit in shpbits:
            oldfn = ("%s.%s" % (rootfn,bit))
            newfn = ("%s_bak.%s" % (rootfn,bit))
            copyfile( oldfn, newfn )

        # Ensure all deleted features are properly deleted from Shapefile
        icechart = ogr.Open( icefn, 1 )
        layer = icechart.GetLayer(0)
        icechart.ExecuteSQL('REPACK ' + layer.GetName())
        icechart.Destroy()
    
        # Open ice chart file
        geocfield='geocode'
        toler = 100.0
        debug = 1    
        icechart = ogr.Open(icefn)
        layer = icechart.GetLayer(0)
        # Get map projection information
        iceproj = layer.GetSpatialRef()
    
        # Number of layers
        nlay = icechart.GetLayerCount()
        layer_list = [icechart.GetLayer(i) for i in xrange(nlay)]
    
        # Get list of layers
        layer_namelist = [icechart.GetLayer(i).GetName() for i in xrange(nlay)]
    
        # Count features
        [count,points,icetype,feat,hole,icex,icey] = \
            featcount( layer, layer_list )
   
        # Convert from met.no Polar Stereographic to standard WGS84 long-lat
        [ wgs84x, wgs84y ] = reproject( feat, iceproj,icex, icey, hole, 0, regionstr )
    
        # nextid is next points_id value set from maxid determined from database
        nextid = maxid + 1
        print ('nextid=%d' % nextid)
    
        # NB: Date format is "2003-06-05 10:49:16.724039" and should be UTC
        sqldatestr = ("%4d-%02d-%02d %02d:00:00" % (year,month,day,valid_hour))
        print sqldatestr
    
        OLD_PROJ = '+proj=longlat +ellps=WGS84'
        if regionstr == 'arctic':
            NEW_PROJ = map_projections['SRS_ARCTIC_PROJ4']
        elif regionstr == 'antarctic':
            NEW_PROJ = map_projections['SRS_ANTARCTIC_PROJ4']
    
        source = osr.SpatialReference()
        target = osr.SpatialReference()
    
        source.ImportFromProj4(OLD_PROJ)
        target.ImportFromProj4(NEW_PROJ)
    
        s_wkt = source.ExportToWkt()
        t_wkt = target.ExportToWkt()
        
        transform = osr.CoordinateTransformation(source,target)

        # Get ice analyst information
        identity = get_analyst_and_country( ipstr )
        print identity

        # Insert polygons into database
        for p in range(count):
    
            if icetype[p] == 'Open Water':
                itype = 1
                concint = 1
                iceform = 98
            elif icetype[p] == 'Very Open Drift Ice':
                itype = 2
                concint = 14
                iceform = 98
            elif icetype[p] == 'Open Drift Ice':
                itype = 3
                concint = 47
                iceform = 98
            elif icetype[p] == 'Close Drift Ice':
                itype = 4
                concint = 79
                iceform = 98
            elif icetype[p] == 'Very Close Drift Ice':
                itype = 5
                concint = 91
                iceform = 98
            elif icetype[p] == 'Fast Ice':
                itype = 6
                concint = 92
                iceform = 8
            elif icetype[p] == 'Ice Free':
                itype = 0
                concint = 0
                iceform = 0
            elif icetype[p] == 'Unknown' or icetype[p] == '':
                itype = 99
                concint = 99
                iceform = 99
            else:
                print 'Unknown ice type: '+('%s' % icetype[p])
                print icex[p]
                print icey[p]
                sys.exit()
            # print icetype[p], itype
            flg=0
            if feat[p] == 'POLYGON':

                insertstr = "INSERT INTO icechart_polygons ( id, datetime, service, analyst, charttype, norway_iceclass, srcimgtype, srcimgid, " + \
                "SI3AREA, SI3PERIMETER, SI3CT, SI3CA, SI3SA, SI3FA, SI3CB, SI3SB, SI3FB, SI3CC, SI3SC, SI3FC, SI3CN, SI3CD, SI3FP, SI3FS, SI3poly_type, " + \
                "SI3SO, SI3SD, SI3FD, SI3SE, SI3FE, " + \
                "SI3DP, SI3DD, SI3DR, SI3DO, SI3WF, SI3WN, SI3WD, SI3WW, SI3WO, SI3RN, SI3RA, SI3RD, SI3RC, SI3RF, SI3RH, SI3RO, SI3RX, " + \
                "SI3EM, SI3EX, SI3EI, SI3EO, SI3AV, SI3AK, SI3AM, SI3AT, SI3SCsnow, SI3SNsnow, SI3SDsnow, SI3SMsnow, SI3SAsnow, SI3SOsnow, " + \
                "SI3BL, SI3BD, SI3BE, SI3BN, SI3BY, SI3BO, polygon) VALUES ("
    
                if hole[p] == 0:
                    polystr = 'POLYGON(('
                    for i in range(len(wgs84x[p])):
                        polystr=polystr+('%.15f ' % wgs84x[p][i])+('%.15f' % wgs84y[p][i])
                        if i < (len(wgs84x[p])-1):
                            polystr=polystr+','
                    # polystr=polystr+'))\', 4326))'
                    polystr=polystr+'))'
                else:
                    polystr='POLYGON('
                    # print hole[p]
                    for j in range(hole[p]):
                        polystr=polystr+'('
                        for i in range(len(wgs84x[p][j])):
                            polystr=polystr+('%.15f ' % wgs84x[p][j][i])+('%.15f' % wgs84y[p][j][i])
                            if i < (len(wgs84x[p][j])-1):
                                polystr=polystr+','
                        polystr=polystr+')'
                        if j < (hole[p]-1):
                            polystr=polystr+','
                    # polystr=polystr+')\', 4326)'
                    polystr=polystr+')'
    
                polygon = ogr.CreateGeometryFromWkt(polystr)
                polygon.Transform(transform)

                parea = polygon.GetArea() / ( 1000.0 * 1000.0 )
                # Expecting 1798.747 km2, 2011-07-04
                boundary = polygon.GetBoundary()
                # This sometimes produces a MULTIPOLYGON (i.e. holes), so only take the outer edge
                if boundary.GetGeometryName() == 'MULTILINESTRING':
                    newbound = boundary.GetGeometryRef(0)
                    boundary = newbound.Clone()
                # NB Length only available in newer versions of OGR
                try:
                    pperi = boundary.get_Length() / 1000.0
                except:
                    pperi = GetLength( boundary )
                    # print 'here'
                # print parea, pperi    
                insertstr = insertstr + ("nextval(\'icechart_polygons_serial\'), \'%s\', \'%s\', \'%s\', \'%s\', " \
                    % (sqldatestr,identity['BIFROST_COUNTRY'],identity['BIFROST_ANALYST'],chart_type)) + \
                    ("\'%s\', \'UNKN\', -9999, %f, %f, %d, -9, -9, -9, " % (icetype[p], parea, pperi, concint)) + \
                    ("-9, -9, -9, -9, -9, -9, -9, -9, %d, -9, \'I\', " % (iceform,)) + \
                    "-9, -9, -9, -9, -9, " + \
                    "-9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, " + \
                    "-9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, " + \
                    ("ST_GeomFromText(\'%s\',4326));" % polystr)
                if active_flg == 1:
                    dbcon.query(insertstr)
                else:
                    print insertstr
                nextid = nextid + 1
                newcount = newcount + 1
                polygon = None
    
            elif feat[p] == 'MULTIPOLYGON':
                # insertstr=insertstr+'MULTIPOLYGON('
                npoly = len(wgs84x[p])
                # print 'npoly=',npoly
                for poly in range(npoly):
    
                    insertstr = "INSERT INTO icechart_polygons ( id, datetime, service, analyst, charttype, norway_iceclass, srcimgtype, srcimgid, " + \
                    "SI3AREA, SI3PERIMETER, SI3CT, SI3CA, SI3SA, SI3FA, SI3CB, SI3SB, SI3FB, SI3CC, SI3SC, SI3FC, SI3CN, SI3CD, SI3FP, SI3FS, SI3poly_type, " + \
                    "SI3SO, SI3SD, SI3FD, SI3SE, SI3FE, " + \
                    "SI3DP, SI3DD, SI3DR, SI3DO, SI3WF, SI3WN, SI3WD, SI3WW, SI3WO, SI3RN, SI3RA, SI3RD, SI3RC, SI3RF, SI3RH, SI3RO, SI3RX, " + \
                    "SI3EM, SI3EX, SI3EI, SI3EO, SI3AV, SI3AK, SI3AM, SI3AT, SI3SCsnow, SI3SNsnow, SI3SDsnow, SI3SMsnow, SI3SAsnow, SI3SOsnow, " + \
                    "SI3BL, SI3BD, SI3BE, SI3BN, SI3BY, SI3BO, polygon) VALUES ("
    
                    polystr='POLYGON('
                    if hole[p][poly] == 0:
                        polystr=polystr+'('
                        for i in range(len(wgs84x[p][poly])):
                            polystr=polystr+('%.15f ' % wgs84x[p][poly][i])+('%.15f' % wgs84y[p][poly][i])
                            if i < (len(wgs84x[p][poly])-1):
                                polystr=polystr+','
                        polystr=polystr+')'
                    else:
                        for j in range(hole[p][poly]):
                            polystr=polystr+'('
                            for i in range(len(wgs84x[p][poly][j])):
                                polystr=polystr+('%.15f ' % wgs84x[p][poly][j][i])+('%.15f' % wgs84y[p][poly][j][i])
                                if i < (len(wgs84x[p][poly][j])-1):
                                    polystr=polystr+','
                            polystr=polystr+')'
                            if j < (hole[p][poly]-1):
                                polystr=polystr+','
                        # print 'Found one!'
                        flg=1
                    polystr=polystr+')'
    
                    # print polystr
                    polygon = ogr.CreateGeometryFromWkt(polystr)
                    polygon.Transform(transform)

                    parea = polygon.GetArea() / ( 1000.0 * 1000.0 )
                    # Expecting 1798.747 km2, 2011-07-04
                    boundary = polygon.GetBoundary()
                    # This sometimes produces a MULTIPOLYGON (i.e. holes), so only take the outer edge
                    if boundary.GetGeometryName() == 'MULTILINESTRING':
                        newbound = boundary.GetGeometryRef(0)
                        boundary = newbound.Clone()
                    try:
                        pperi = boundary.Length() / 1000.0
                    except:
                        pperi = GetLength( boundary )
                    # print parea, pperi
                    insertstr = insertstr + ("nextval(\'icechart_polygons_serial\'), \'%s\', \'%s\', \'%s\', \'%s\', " \
                        % (sqldatestr,identity['BIFROST_COUNTRY'],identity['BIFROST_ANALYST'],chart_type)) + \
                        ("\'%s\', \'UNKN\', -9999, %f, %f, %d, -9, -9, -9, " % (icetype[p], parea, pperi, concint)) + \
                        ("-9, -9, -9, -9, -9, -9, -9, -9, %d, -9, \'I\', " % (iceform,)) + \
                        "-9, -9, -9, -9, -9, " + \
                        "-9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, " + \
                        "-9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, " + \
                        ("ST_GeomFromText(\'%s\',4326));" % polystr)
                    if active_flg == 1:
                        dbcon.query(insertstr)
                    else:
                       print insertstr
                    nextid = nextid + 1
                    newcount = newcount + 1
                    polygon = None

        # Close ice chart file
        icechart.Destroy()
    
        print ("Finished polygon upload cleanly!\n")


        # Upload ice chart symbols
        symbfn = ("%s/%s_%02d_%s/ice_%s_%02d_symbols_%s.shp" \
            % (ICEDIR,datestr,valid_hour,regcode,datestr,valid_hour,regcode))
        print symbfn

        # Create backup copy of Shapefile in case tidying up (REPACK) step causes problems
        shpbits = [ 'shp', 'shx', 'dbf', 'shx' ]
        rootfn = symbfn[:-4]
        for bit in shpbits:
            oldfn = ("%s.%s" % (rootfn,bit))
            newfn = ("%s_bak.%s" % (rootfn,bit))
            copyfile( oldfn, newfn )

        # Ensure all deleted features are properly deleted from Shapefile
        icesymb = ogr.Open( symbfn, 1 )
        layer = icesymb.GetLayer(0)
        icesymb.ExecuteSQL('REPACK ' + layer.GetName())
        icesymb.Destroy()
    
        # Open ice chart symbols file
        icesymb = ogr.Open(symbfn)
        layer = icesymb.GetLayer(0)
        srs = layer.GetSpatialRef()
        s_wkt = srs.ExportToWkt()
        # print s_wkt

        # Longitude/latitude (EPSG:4326) information
        target = osr.SpatialReference()    
        target.ImportFromEPSG(4326)
        t_wkt = target.ExportToWkt()
        # print t_wkt
        
        # Coordinate transform
        to_longlat = osr.CoordinateTransformation(srs,target)

        # Count features
        nfeat = layer.GetFeatureCount()

        # Loop through features and add to database
        for i in range(nfeat):
            symbol = layer.GetFeature(i)
            type_field = symbol.GetField("TYPE")
            if type_field != None:
                type_field = int(type_field)
            else:
                type_field = 0
            rot_field = symbol.GetField("ROTATION")
            if rot_field != None:
                rot_field = int(rot_field)
            else:
                rot_field = 0
            value_field = symbol.GetField("VALUE")
            location = symbol.GetGeometryRef()
            location.Transform(to_longlat)
            # print type_field, rot_field, value_field, location
            insertstr = "INSERT INTO icechart_symbols (id,type,name,rotation,value,datetime,service,analyst,charttype,location) VALUES"
            insertstr = ("%s (nextval(\'icechart_symbols_serial\')" % insertstr)
            insertstr = ("%s, %d" % (insertstr,type_field))
            insertstr = ("%s, \'%s\'" % (insertstr,symbol_dict[type_field]))
            insertstr = ("%s, %d" % (insertstr,rot_field))
            insertstr = ("%s, \'%s\'" % (insertstr,value_field))
            insertstr = ("%s, \'%s\'" % (insertstr,sqldatestr))
            insertstr = ("%s, \'%s\'" % (insertstr,identity['BIFROST_COUNTRY']))
            insertstr = ("%s, \'%s\'" % (insertstr,identity['BIFROST_ANALYST']))
            insertstr = ("%s, \'%s\'" % (insertstr,chart_type))
            insertstr = ("%s,ST_GeomFromText(\'%s\',4326));" % (insertstr,location.ExportToWkt()))
            if active_flg == 1:
                dbcon.query(insertstr)
            else:
                print insertstr

        # Close symbols file
        icesymb.Destroy()

        print ("Finished symbols upload cleanly!\n")

        # If Antarctic chart, upload ice shelves and icebergs data
        if regionstr == 'antarctic':
            # Ice shelves
            shelffn = ("%s/%s_%02d_%s/ice_%s_%02d_iceshelf_%s.shp" \
                % (ICEDIR,datestr,valid_hour,regcode,datestr,valid_hour,regcode))
            print shelffn
    
            # Create backup copy of Shapefile in case tidying up (REPACK) step causes problems
            shpbits = [ 'shp', 'shx', 'dbf', 'shx' ]
            rootfn = symbfn[:-4]
            for bit in shpbits:
                oldfn = ("%s.%s" % (rootfn,bit))
                newfn = ("%s_bak.%s" % (rootfn,bit))
                copyfile( oldfn, newfn )
    
            # Ensure all deleted features are properly deleted from Shapefile
            iceshelf = ogr.Open( shelffn, 1 )
            layer = iceshelf.GetLayer(0)
            iceshelf.ExecuteSQL('REPACK ' + layer.GetName())
            iceshelf.Destroy()
        
            # Open Antarctic ice chart ice shelves file
            iceshelf = ogr.Open(shelffn)
            layer = iceshelf.GetLayer(0)
            srs = layer.GetSpatialRef()
            s_wkt = srs.ExportToWkt()
            # print s_wkt
    
            # Longitude/latitude (EPSG:4326) information
            target = osr.SpatialReference()    
            target.ImportFromEPSG(4326)
            t_wkt = target.ExportToWkt()
            # print t_wkt
            
            # Coordinate transform
            to_longlat = osr.CoordinateTransformation(srs,target)
    
            # Count features
            nfeat = layer.GetFeatureCount()

            # Loop through features and add to database
            for i in range(nfeat):
                shelf = layer.GetFeature(i)
                sat_field = shelf.GetField("SATELLITE")
                if sat_field == None:
                    sat_field = 'Unknown'
                datestr_field = shelf.GetField("DATESTR")
                if datestr_field != None:
                    datestr_field = int(datestr_field)
                else:
                    datestr_field = 0
                dateend_field = shelf.GetField("DATEEND")
                if dateend_field != None:
                    dateend_field = int(dateend_field)
                else:
                    dateend_field = 0
                polygon = shelf.GetGeometryRef()
                polygon.Transform(to_longlat)
                insertstr = "INSERT INTO iceshelf (id,satellite,datestr,dateend,datetime,service,analyst,charttype,polygon) VALUES"
                insertstr = ("%s (nextval(\'iceshelf_serial\')" % insertstr)
                insertstr = ("%s, \'%s\'" % (insertstr,sat_field))
                insertstr = ("%s, %d" % (insertstr,datestr_field))
                insertstr = ("%s, %d" % (insertstr,dateend_field))
                insertstr = ("%s, \'%s\'" % (insertstr,sqldatestr))
                insertstr = ("%s, \'%s\'" % (insertstr,identity['BIFROST_COUNTRY']))
                insertstr = ("%s, \'%s\'" % (insertstr,identity['BIFROST_ANALYST']))
                insertstr = ("%s, \'%s\'" % (insertstr,chart_type))
                insertstr = ("%s,ST_GeomFromText(\'%s\',4326));" % (insertstr,polygon.ExportToWkt()))
                if active_flg == 1:
                    dbcon.query(insertstr)
                else:
                    print insertstr
    
            # Close ice shelves file
            iceshelf.Destroy()
    
            print ("Finished Antarctic ice shelf data upload cleanly!\n")


            # Icebergs
            bergsfn = ("%s/%s_%02d_%s/ice_%s_%02d_icebergs_%s.shp" % (ICEDIR,datestr,valid_hour,regcode,datestr,valid_hour,regcode))
            print bergsfn
    
            # Create backup copy of Shapefile in case tidying up (REPACK) step causes problems
            shpbits = [ 'shp', 'shx', 'dbf', 'shx' ]
            rootfn = symbfn[:-4]
            for bit in shpbits:
                oldfn = ("%s.%s" % (rootfn,bit))
                newfn = ("%s_bak.%s" % (rootfn,bit))
                copyfile( oldfn, newfn )
    
            # Ensure all deleted features are properly deleted from Shapefile
            icebergs = ogr.Open( bergsfn, 1 )
            layer = icebergs.GetLayer(0)
            icebergs.ExecuteSQL('REPACK ' + layer.GetName())
            icebergs.Destroy()
        
            # Open Antarctic ice chart icebergs file
            icebergs = ogr.Open(bergsfn)
            layer = icebergs.GetLayer(0)
            srs = layer.GetSpatialRef()
            s_wkt = srs.ExportToWkt()
            # print s_wkt
    
            # Longitude/latitude (EPSG:4326) information
            target = osr.SpatialReference()    
            target.ImportFromEPSG(4326)
            t_wkt = target.ExportToWkt()
            # print t_wkt
            
            # Coordinate transform
            to_longlat = osr.CoordinateTransformation(srs,target)
    
            # Count features
            nfeat = layer.GetFeatureCount()

            # Loop through features and add to database
            for i in range(nfeat):
                berg = layer.GetFeature(i)
                name_field = berg.GetField("NAME")
                if name_field == None:
                    name_field = 'Unknown'
                sat_field = berg.GetField("SATELLITE")
                if sat_field == None:
                    sat_field = 'Unknown'
                date_field = berg.GetField("DATE")
                if date_field != None:
                    date_field = int(date_field)
                else:
                    date_field = 0
                time_field = berg.GetField("TIME")
                if time_field != None:
                    time_field = int(time_field)
                else:
                    time_field = 0
                polygon = berg.GetGeometryRef()
                polygon.Transform(to_longlat)
                insertstr = "INSERT INTO icebergs (id,name,satellite,date,time,datetime,service,analyst,charttype,polygon) VALUES"
                insertstr = ("%s (nextval(\'icebergs_serial\')" % insertstr)
                insertstr = ("%s, \'%s\'" % (insertstr,name_field))
                insertstr = ("%s, \'%s\'" % (insertstr,sat_field))
                insertstr = ("%s, %d" % (insertstr,date_field))
                insertstr = ("%s, %d" % (insertstr,time_field))
                insertstr = ("%s, \'%s\'" % (insertstr,sqldatestr))
                insertstr = ("%s, \'%s\'" % (insertstr,identity['BIFROST_COUNTRY']))
                insertstr = ("%s, \'%s\'" % (insertstr,identity['BIFROST_ANALYST']))
                insertstr = ("%s, \'%s\'" % (insertstr,chart_type))
                insertstr = ("%s,ST_GeomFromText(\'%s\',4326));" % (insertstr,polygon.ExportToWkt()))
                if active_flg == 1:
                    dbcon.query(insertstr)
                else:
                    print insertstr

            # Close icebergs file
            icebergs.Destroy()

            print ("Finished Antarctic icebergs data upload cleanly!\n")


    # Upload METAREA line if active and Arctic
    if metact_flg > 0 and regionstr == 'arctic':

        # Open METAREA line file
        metfn = ("%s/%s_%s/ice_%s_metarea_line_%s.shp" % (ICEDIR,datestr,regcode,datestr,regcode))
        print metfn

        # Create backup copy of Shapefile in case tidying up (REPACK) step causes problems
        shpbits = [ 'shp', 'shx', 'dbf', 'shx' ]
        rootfn = metfn[:-4]
        for bit in shpbits:
            oldfn = ("%s.%s" % (rootfn,bit))
            newfn = ("%s_bak.%s" % (rootfn,bit))
            copyfile( oldfn, newfn )

        # Ensure all features are properly deleted from Shapefile
        metline = ogr.Open( metfn, 1 )
        metlay = metline.GetLayer(0)
        metline.ExecuteSQL('REPACK ' + metlay.GetName())
        metline.Destroy()

        # Now open for reading
        metline = ogr.Open(metfn)

        # Get default layer and projection
        metlay = metline.GetLayer()
        metsrs = metlay.GetSpatialRef()
        # print metsrs.ExportToWkt()

        # Create conversion to longitude/latitude
        ll_proj4 = '+proj=longlat +ellps=WGS84'
        ll_srs = osr.SpatialReference()
        ll_srs.ImportFromProj4(ll_proj4)
        ll_wkt = ll_srs.ExportToWkt()
        mettoll = osr.CoordinateTransformation(metsrs,ll_srs)

        # Get number of features
        procflg = 0
        nline = metlay.GetFeatureCount()
        if nline == 0:
            print "04_finish: No METAREA line data to upload."
        elif nline > 0:
            print "04_finish: Uploading METAREA line data."
            procflg = 1
        else:
            print "04_finish: Too many lines in METAREA line file.  Not uploading."
        # print nline, procflg

        # NB: Date format is "2003-06-05 10:49:16.724039" and should be UTC
        sqldatestr = ('%4d-' % year) + ('%02d-' % month) + ('%02d' % day) + ' 15:00:00'
        # print sqldatestr

        # Upload if procflg = 1
        if procflg == 1:
            line_feat = metlay.GetFeature(0)
            line_geom = line_feat.GetGeometryRef()
            line_geom.Transform(mettoll)
            # Get the number of points in the line
            npoints = line_geom.GetPointCount()

            # Copy to database
            insertstr = "INSERT INTO metline ( id, datetime, service, analyst, charttype, line) VALUES"
            insertstr = ("%s (nextval(\'metline_serial\'), \'%s\',ST_GeomFromText(\'%s\',4326));" \
                % (insertstr,sqldatestr,identity['BIFROST_COUNTRY'],identity['BIFROST_ANALYST'],chart_type,line_geom.ExportToWkt()))
            if active_flg == 1:
                dbcon.query(insertstr)
            else:
                print insertstr

        # Close METAREA line file
        metline.Destroy()

    # Update production database
    timenow = datetime.now()
    dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
    # print newcount, dtstr, icechart_id
    if iceact_flg > 0 and metact_flg > 0:
        sqltxt = ("UPDATE production SET (finish_flg,finish_dt,metline_flg,metline_dt) = (%d,\'%s\',%d,\'%s\') WHERE id = %d;" \
            % (newcount,dtstr,npoints,dtstr,icechart_id))
    else:
        if iceact_flg > 0:
            sqltxt = ("UPDATE production SET (finish_flg,finish_dt) = (%d,\'%s\') WHERE id = %d;" \
                % (newcount,dtstr,icechart_id))
        if metact_flg > 0:
            sqltxt = ("UPDATE production SET (metline_flg,metline_dt) = (%d,\'%s\') WHERE id = %d;" \
                % (npoints,dtstr,icechart_id))
    # print sqltxt
    if active_flg == 1:
        dbcon.query(sqltxt)
    else:
        print sqltxt

    # Close database connection
    dbcon.close()

