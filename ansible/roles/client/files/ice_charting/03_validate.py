#!/usr/bin/python

# Name:          03_validate.py
# Purpose:       Crunch tests for Ice Charts files to check for errors since QGIS error checking is missing some.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Dependencies:  Uses prepair (Polygon Repair) to help detect bad polygons.
#                prepair generates a MULTIPOLYGON, which then fails normal OGR checks.
#                See Ledoux, H., Arroyo Ohori, K., and Meijers, M. (2014). A triangulation-based approach to 
#                automatically repair GIS polygons. Computers & Geosciences 66:121-131.

import os, sys, platform
from datetime import datetime
import time
import copy
import argparse

import pg

import osgeo.ogr as ogr
import osgeo.osr as osr

import shapely

import numpy as N

from geometry_functions import featcount, reproject, GetLength, shapelyValidation, prepairValidation
from validity import polytest, pointcounter
from get_bifrost_credentials import get_bifrost_credentials
from get_ip_address import get_ip_address
from get_map_projections import get_map_projections


# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
INCDIR = ("%s/Include/Bifrost" % BASEDIR)
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)
ICEDIR = ("%s/ICS" % BASEDIR)

# Active flag. Set this to zero for debugging, one to update the database
active_flg = 1
# Ice chart flag. Set to zero so that we can only upload METAREA line
iceact_flg = 1
# METAREA line flag. Set to zero so that we can only upload ice chart data
metact_flg = 1

# Size threshold for too small polygons
sizethres = 0.09

# Date reformatting
def mkdate(datestr):
    return time.strptime(datestr,'%Y%m%d')


# Check polygon file
def check_polygons(icefn,regionstr):
    # Counters for polygons and lines, also act as flags
    newcount = 0
    npoints = 0

    # Open ice chart file
    geocfield='geocode'
    toler = 100.0
    debug = 1    
    icechart = ogr.Open(icefn)
    layer = icechart.GetLayer(0)
    # Get map projection information
    iceproj = layer.GetSpatialRef()

    # Check for empty Shapefile
    nfeat = layer.GetFeatureCount()
    if nfeat == 0:
        print "03_validate: No features in file."
        invalid = 0
        return invalid

    # Number of layers
    nlay = icechart.GetLayerCount()
    layer_list = [icechart.GetLayer(i) for i in xrange(nlay)]

    # Get list of layers
    layer_namelist = [icechart.GetLayer(i).GetName() for i in xrange(nlay)]

    # Count features
    [count,points,icetype,feat,hole,icex,icey] = \
        featcount( layer, layer_list )

    # Check to see if only one polygon is present
    if count == 1 and icefn.find('polygons') > -1:
        print '03_validate: Only one polygon. I am guessing you have edited a previous ice chart and I am only seeing the default blank chart for today. Stopping.'
        sys.exit()
   
    # Convert from input map projection to standard WGS84 long-lat
    [ wgs84x, wgs84y ] = reproject( feat, iceproj, icex, icey, hole, 0, regionstr )
    
    # Region specific settings, map projection and extent
    mapfn = ("%s/map_projections.txt" % SETDIR)
    map_projections = get_map_projections( mapfn )

    # Create coordinate transformations
    OLD_PROJ = '+proj=longlat +ellps=WGS84'
    if regionstr == 'arctic':
        NEW_PROJ = map_projections['SRS_ARCTIC_PROJ4']
    elif regionstr == 'antarctic':
        NEW_PROJ = map_projections['SRS_ANTARCTIC_PROJ4']

    source = osr.SpatialReference()
    target = osr.SpatialReference()

    source.ImportFromProj4(OLD_PROJ)
    target.ImportFromProj4(NEW_PROJ)

    s_wkt = source.ExportToWkt()
    t_wkt = target.ExportToWkt()
    
    transform = osr.CoordinateTransformation(source,target)

    # Counter for invalid features
    invalid = 0

    # Loop through polygons
    for p in range(count):

        flg=0
        if feat[p] == 'POLYGON':
            # print 'POLYGON'

            if hole[p] == 0:
                polystr = 'POLYGON(('
                for i in range(len(wgs84x[p])):
                    polystr=polystr+('%.15f ' % wgs84x[p][i])+('%.15f' % wgs84y[p][i])
                    if i < (len(wgs84x[p])-1):
                        polystr=polystr+','
                # polystr=polystr+'))\', 4326))'
                polystr=polystr+'))'
            else:
                polystr='POLYGON('
                # print hole[p]
                for j in range(hole[p]):
                    polystr=polystr+'('
                    for i in range(len(wgs84x[p][j])):
                        polystr=polystr+('%.15f ' % wgs84x[p][j][i])+('%.15f' % wgs84y[p][j][i])
                        if i < (len(wgs84x[p][j])-1):
                            polystr=polystr+','
                    polystr=polystr+')'
                    if j < (hole[p]-1):
                        polystr=polystr+','
                # polystr=polystr+')\', 4326)'
                polystr=polystr+')'
            
            polygon = ogr.CreateGeometryFromWkt(polystr)
            polygon.Transform(transform)

            # Temporary flag for multiple checks
            tmpflg = 0

            # Calculate area and perimeter length
            parea = polygon.GetArea() / ( 1000.0 * 1000.0 )
            boundary = polygon.GetBoundary()
            # This sometimes produces a MULTILINESTRING (i.e. holes), so only take the outer edge
            if boundary != None:
                if boundary.GetGeometryName() == 'MULTILINESTRING':
                    newbound = boundary.GetGeometryRef(0)
                    boundary = newbound.Clone()
                # NB Length only available in newer versions of OGR
                try:
                    pperi = boundary.get_Length() / 1000.0
                except:
                    pperi = GetLength( boundary )
                # print 'here'
                # print ("%16.4f %12.3f" % (parea, pperi))
                if parea < sizethres:
                    centre_poly = polygon.Centroid()
                    cenx = centre_poly.GetX()
                    ceny = centre_poly.GetY()
                    print ("Area check: Polygon #%d (Centre %.2f,%.2f) area too small at %.4f. Limit is %.4f." \
                        % (p,cenx,ceny,parea,sizethres))
                    tmpflg = tmpflg + 1
            else:
                tmpflg = tmpflg + 1

            # Check geometry validity using OGR
            if polygon.IsValid() == False:
                print ("OGR: Invalid POLYGON #%d" % p)
                # print polygon.ExportToWkt()
                tmpflg = tmpflg + 1
            if tmpflg == 0:
                shapely_status = shapelyValidation( polygon.ExportToWkt() )
                if shapely_status == 1:
                    print ("Shapely: Invalid POLYGON #%d" % p)
                    tmpflg = tmpflg + 1
            if tmpflg == 0:
                prepair_status = prepairValidation( polygon.ExportToWkt() )
                if prepair_status == 1:
                    print ("prepair: Invalid POLYGON #%d" % p)
                    tmpflg = tmpflg + 1
            if tmpflg == 0:
                add_status = pointcounter( polygon.ExportToWkt() )
                if add_status == 1:
                    print ("pointcounter: Invalid POLYGON #%d" % p)
                    tmpflg = tmpflg + 1
                    # sys.exit()

            # If tmpflg has been set, increment the invalid polygons counter
            if tmpflg > 0:
                invalid = invalid + 1                

            newcount = newcount + 1
            polygon = None

        elif feat[p] == 'MULTIPOLYGON':
            # print 'MULTIPOLYGON'

            npoly = len(wgs84x[p])
            # print 'npoly=',npoly
            for poly in range(npoly):

                polystr='POLYGON('
                if hole[p][poly] == 0:
                    polystr=polystr+'('
                    for i in range(len(wgs84x[p][poly])):
                        polystr=polystr+('%.15f ' % wgs84x[p][poly][i])+('%.15f' % wgs84y[p][poly][i])
                        if i < (len(wgs84x[p][poly])-1):
                            polystr=polystr+','
                    polystr=polystr+')'
                else:
                    for j in range(hole[p][poly]):
                        polystr=polystr+'('
                        for i in range(len(wgs84x[p][poly][j])):
                            polystr=polystr+('%.15f ' % wgs84x[p][poly][j][i])+('%.15f' % wgs84y[p][poly][j][i])
                            if i < (len(wgs84x[p][poly][j])-1):
                                polystr=polystr+','
                        polystr=polystr+')'
                        if j < (hole[p][poly]-1):
                            polystr=polystr+','
                    # print 'Found one!'
                    flg=1
                polystr=polystr+')'

                # print polystr
                polygon = ogr.CreateGeometryFromWkt(polystr)
                polygon.Transform(transform)

                # Temporary flag for multiple checks
                tmpflg = 0

                parea = polygon.GetArea() / ( 1000.0 * 1000.0 )
                # Expecting 1798.747 km2, 2011-07-04
                boundary = polygon.GetBoundary()
                # This sometimes produces a MULTIPOLYGON (i.e. holes), so only take the outer edge
                if boundary != None:
                    if boundary.GetGeometryName() == 'MULTILINESTRING':
                        newbound = boundary.GetGeometryRef(0)
                        boundary = newbound.Clone()
                    try:
                        pperi = boundary.Length() / 1000.0
                    except:
                        pperi = GetLength( boundary )
                    # print ("%16.4f %12.3f" % (parea, pperi))
                    if parea < sizethres:
                        centre_poly = polygon.Centroid()
                        cenx = centre_poly.GetX()
                        ceny = centre_poly.GetY()
                        print ("Area check: Polygon #%d (Centre %.2f,%.2f) area too small at %.4f. Limit is %.4f." \
                            % (p,cenx,ceny,parea,sizethres))
                        tmpflg = tmpflg + 1
                else:
                    tmpflg = tmpflg + 1

                # Check geometry validity using OGR
                if polygon.IsValid() == False:
                    print ("OGR: Invalid POLYGON #%d" % p)
                    # print polygon.ExportToWkt()
                    tmpflg = tmpflg + 1
                if tmpflg == 0:
                    shapely_status = shapelyValidation( polygon.ExportToWkt() )
                    if shapely_status == 1:
                        print ("Shapely: Invalid POLYGON #%d" % p)
                        tmpflg = tmpflg + 1
                if tmpflg == 0:
                    prepair_status = prepairValidation( polygon.ExportToWkt() )
                    if prepair_status == 1:
                        print ("prepair: Invalid POLYGON #%d" % p)
                        tmpflg = tmpflg + 1
                if tmpflg == 0:
                    add_status = pointcounter( polygon.ExportToWkt() )
                    if add_status == 1:
                        print ("pointcounter: Invalid POLYGON #%d" % p)
                        tmpflg = tmpflg + 1

                # If tmpflg has been set, increment the invalid polygons counter
                if tmpflg > 0:
                    invalid = invalid + 1                

                newcount = newcount + 1
                polygon = None

    # Close ice chart file
    icechart.Destroy()

    return invalid


# Main function
if __name__ == "__main__":

    # Get today's date
    today = time.localtime()
    todaystr = time.strftime('%Y-%m-%d',today)
    # print todaystr

    # Get input parameters using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument( "region", help="Region for ice chart [arctic/antarctic]", type=str, \
        choices=['arctic','antarctic'])
    parser.add_argument("-d","--date",help="Date for ice chart [yyyymmdd]",type=mkdate,default=today)
    parser.add_argument("-t","--time",help="Valid hour for ice chart [hh]",type=int,default=-1, \
        choices=xrange(0,24))
    args = parser.parse_args()
    regionstr = args.region
    year = (args.date).tm_year
    month = (args.date).tm_mon
    day = (args.date).tm_mday
    if args.time == -1:
       hour = 15
    else:
        hour = args.time
    datestr = ("%4d%02d%02d" % (year,month,day))
    dtstamp = datetime( year, month, day, hour, 0, 0 )
    dow = dtstamp.isoweekday()
    if args.time == -1:
        autoflg = True
    else:
        autoflg = False

    # Get computer name
    hoststr = platform.node()

    # Get computer IP address
    ipstr = get_ip_address()

    # Get Bifrost server credentials from file
    bifrostfn = ("%s/bifrost_credentials.txt" % SETDIR)
    credentials = get_bifrost_credentials( bifrostfn )   

    # Set up database connection
    dbcon = pg.connect(dbname='Icecharts', host=credentials['BIFROST_SERVER_IP'], user='bifrostadmin', passwd='bifrostadmin')

    # Check current status of database
    sqltxt = "SELECT new_flg,finish_flg,metline_flg,id,valid_hour FROM production WHERE"
    sqltxt = ("%s icechart_date = \'%s\' AND computer = \'%s\' AND region = \'%s\'" \
        % (sqltxt,datestr,ipstr,regionstr))
    checkres = dbcon.query(sqltxt)
    current = checkres.ntuples()
    if current == 1:
        new_flg = checkres.getresult()[0][0]
        finish_flg = checkres.getresult()[0][1]
        metline_flg = checkres.getresult()[0][2]
        icechart_id = checkres.getresult()[0][3]
        valid_hour = int( checkres.getresult()[0][4] )
    else:
        if current == 0:
            print "03_validate: No production database entry for this region on this computer on this date. Stopping."
        else:
            print ("03_validate: Too many (%d) database entries for this date. Stopping." \
                % current)
        sys.exit()
    # print current, new_flg, finish_flg, icechart_id
    # Check new_flg is set and finish_flg is unset for this date
    # If finish_flg is set, find out how many polygons are already in database
    dtstr = dtstamp.strftime('%Y-%m-%d %H:%M:%S')
    if finish_flg > 0:
        # Get number of polygons in database
        if regionstr == 'arctic':
            checkpoly = dbcon.query(("SELECT id FROM icechart_polygons WHERE datetime = \'%s\' AND ST_Y(ST_Centroid(polygon)) > 0.0;" \
                % dtstr ))
        elif regionstr == 'antarctic':
            checkpoly = dbcon.query(("SELECT id FROM icechart_polygons WHERE datetime = \'%s\' AND ST_Y(ST_Centroid(polygon)) < 0.0;" \
                % dtstr ))
        npoly = checkpoly.ntuples()
        print ("03_validate: There are already %d polygons in the database. These should be deleted" % npoly)
        # sys.exit()
    else:
        if new_flg == 0:
            print "03_validate: Somehow there is a database entry, but new_flg is not set. Stopping."
            sys.exit()

    # Only process if iceact_flg is not zero
    if iceact_flg > 0:

        # Set regional values
        if regionstr == 'arctic':
            regcode = 'ARC'
        elif regionstr == 'antarctic':
            regcode = 'ANT'

        print 'Ice Chart ID=',icechart_id

        # Check ice chart polygons file
        icefn = ("%s/%s_%02d_%s/ice_%s_%02d_polygons_%s.shp" \
            % (ICEDIR,datestr,valid_hour,regcode,datestr,valid_hour,regcode))
        # icefn = "/disk1/nicholsh/QGIS/ice_20140724_polygons_edit.shp"
        print ("\n%s" % icefn)
        invalid = check_polygons( icefn, regionstr )

        # If Antarctic, aso check the ice shelf and icebergs files as well
        if regionstr == 'antarctic':

            # Ice shelves
            shelffn = ("%s/%s_%02d_%s/ice_%s_%02d_iceshelf_%s.shp" \
                 % (ICEDIR,datestr,valid_hour,regcode,datestr,valid_hour,regcode))
            print ("\n%s" % shelffn)
            invalid = invalid + check_polygons( shelffn, regionstr )

            # Icebergs
            bergfn = ("%s/%s_%02d_%s/ice_%s_%02d_icebergs_%s.shp" % \
                (ICEDIR,datestr,valid_hour,regcode,datestr,valid_hour,regcode))
            print ("\n%s" % bergfn)
            invalid = invalid + check_polygons( bergfn, regionstr )


    if invalid > 0:
        print ("\n***** %d invalid polygons. *****" % invalid)
        # Update production database with 0 flag to say NOT validated
        sqltxt = ("UPDATE production SET (valid_flg,valid_dt) = (0,\'%s\') WHERE id = %d;" \
            % (dtstr,icechart_id))
        if active_flg == 1:
            dbcon.query(sqltxt)
        else:
            print sqltxt
    else:
        # Update the production database
        timenow = datetime.now()
        dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
        print "\nCongratulations! No invalid polygons."
        sqltxt = ("UPDATE production SET (valid_flg,valid_dt) = (1,\'%s\') WHERE id = %d;" \
            % (dtstr,icechart_id))
        if active_flg == 1:
            dbcon.query(sqltxt)
        else:
            print sqltxt

    print 'Finished!'


    # Close database connection
    dbcon.close()

