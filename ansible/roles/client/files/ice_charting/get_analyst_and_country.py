#!/usr/bin/python

# Name:          get_analyst_and_country.py
# Purpose:       Get information on ice service country and analyst.
#                Either automatically by IP address (MET Norway) or by user input.
#                This is done once only, and results stored in 
#                /home/bifrostanalyst/Settings/Bifrost/identity.txt
#                BIFROST_COUNTRY=[2 char string] ISO-3166-1
#                BIFROST_ANALYST=[2 char string]
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, platform
import pycountry
import re

from get_ip_address import get_ip_address


# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
INCDIR = ("%s/Include/Bifrost" % BASEDIR)
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)


# Dictionary of probable countries
country_dict = { 'AR': 'argentina,republica argentina', \
                 'AU': 'australia,commonwealth of australia', \
                 'CL': 'chile,republica de chile', \
                 'DE': 'germany,deutschland,frg', \
                 'DK': 'denmark,danmark', \
                 'FI': 'finland,suomi', \
                 'GB': 'uk,united kingdom,great britain', \
                 'NO': 'norway,norge', \
                 'NZ': 'new zealand,aotearoa', \
                 'RU': 'russia,rossiya,russian federation', \
                 'US': 'usa,united states of america', \
                 'ZA': 'south africa' }


# Get analyst and country information
def get_analyst_and_country( ipaddr ):

    # Filename for storing information
    identfn = ("%s/identity.txt" % SETDIR)

    # Empty dictionary for values
    identity = {}

    # Try to open file for reading
    try:
        infile = open( identfn, 'r' )
    except:

        # See if IP address is MET Norway
        if ipaddr.find('157.249.48.') > -1:
            identity['BIFROST_COUNTRY'] = 'NO'
            username = os.getlogin()
            analyst_dict = { 'nicholsh'  : 'NH', \
                             'trondr'    : 'TR', \
                             'signea'    : 'SA', \
                             'havardl'   : 'HL', \
                             'oddio'     : 'OI', \
                             'penelopew' : 'PW', \
                             'vebjornk'  : 'VK' }
            analyst = analyst_dict[username]
            identity['BIFROST_ANALYST'] = analyst

        else:

            # Set up dictionary of country codes using pycountry
            # countries = {}
            # for country in pycountry.countries:
            #     countries[(country.name).lower()] = country.alpha2
            # code = countries.get('new zealand')
            # print code

            # Get analyst input for country
            valid_country_flg = 0
            while valid_country_flg == 0:
                raw_country = ( raw_input("Enter the name of the ice service country: ") ).lower()
                # Try to decode the country name
                for country in country_dict.items():
                    name_alternatives = country[1].split(',')
                    for name in name_alternatives:
                        if name.find(raw_country) > -1:
                            country_code = country[0]
                            valid_country_flg = 1
                if valid_country_flg == 0:
                    print "  Invalid or unknown country name, try again!"
            identity['BIFROST_COUNTRY'] = country_code

            # Get analyst name
            valid_analyst_flg = 0
            while valid_analyst_flg == 0:
                raw_analyst = ( raw_input("Enter the initials of the first (given) and second (family) name of the analyst, e.g. Nick Hughes = NH: ") ).upper()
                # print re.match('^[\w-]+$', raw_analyst)
                punctuation = re.match('^[\w-]+$', raw_analyst)
                if len(raw_analyst) == 2 and punctuation is not None:
                    analyst = raw_analyst
                    valid_analyst_flg = 1
                else:
                    if len(raw_analyst) < 2 or len(raw_analyst) > 2:
                        print "  Input must be 2 initial letters, try again."
                    elif punctuation is None:
                        print "  No spaces or punctuation symbols allowed, try again."
            identity['BIFROST_ANALYST'] = analyst

        # Write input to file
        outfile = open( identfn, 'w' )
        for item in identity.items():
            outfile.write( ("%s=%s\n" % (item[0],item[1])) )
        outfile.close()

    else:
        for txtln in infile:
            bits = (txtln.strip()).split('=')
            keytxt = bits[0]
            keyval = bits[1]
            identity[keytxt] = keyval
        infile.close()

    return identity


# Main routine for testing
if __name__ == '__main__':

    # Get computer name
    hoststr = platform.node()

    # Get computer IP address
    ipstr = get_ip_address()

    # Call subroutine
    identity = get_analyst_and_country( ipstr )
    print identity

