#!/usr/bin/python

# Name:          06_clearcache.py
# Purpose:       Python script to archive, then delete, older production files from client directories.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os,sys
from os.path import join
import shutil
from datetime import datetime, timedelta

import pg

import zipfile

import paramiko

from get_bifrost_credentials import get_bifrost_credentials

# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
INCDIR = ("%s/Include/Bifrost" % BASEDIR)
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)
ICEDIR = ("%s/ICS" % BASEDIR)
TMPDIR = ("%s/tmp" % BASEDIR)
# And on the Bifrost server, the production archive location
PRODARCH='/home/bifrostadmin/Production_Archive'

# Set the size of the cache in days
CACHESIZE = 31

# New system flag, enables database manipulation
NEWSYS = 1

# Active flag, do file transfer and delete operations if set
activeflg = 1

# Create a zipfile of the specified directory
def createzip( dirname ):

    # Specify the zip-file name
    zipfn = ("%s/ice_%s_production_backup.zip" % (TMPDIR,dirname))
    print zipfn

    # Open zip file for writing
    icezip = zipfile.ZipFile(zipfn,'w',zipfile.ZIP_DEFLATED)

    # Walk through directory and get names of files to compress
    fullpath = ("%s/%s" % (ICEDIR,dirname))
    for root, dirs, files in os.walk(fullpath):
        # print root, dirs, files
        for name in files:
            fullfn = join(root, name) 
            shortfn = fullfn.replace(("%s/" % ICEDIR),'')
            # print shortfn
            icezip.write(fullfn,shortfn)

    # Close zipfile
    icezip.close()

    return zipfn


# Main function
if __name__ == "__main__":

    # Get current date
    autoflg = True
    dtstamp = datetime.now()
    year = dtstamp.year
    month = dtstamp.month
    day = dtstamp.day
    datestr = ("%4d%02d%02d" % (year,month,day))
    dtstamp = datetime( year, month, day, 15, 0, 0 )
    dow = dtstamp.isoweekday()

    # Set the earliest date/time allowed in cache
    cachemin = dtstamp - timedelta(days=CACHESIZE)
    # print cachemin
    
    # Find ICS folders older than 31 days
    icedirs = os.listdir( ICEDIR )
    cleardirs = []
    for dirname in sorted(icedirs):
        fullpath = ("%s/%s" % (ICEDIR,dirname))
        stat = os.stat( fullpath )
        dirdt = datetime.fromtimestamp( stat.st_mtime )
        if dirdt < cachemin and fullpath.find('Coastline') == -1:
            cleardirs.append( dirname )
    # print cleardirs

    # Get Bifrost server credentials from file
    bifrostfn = ("%s/bifrost_credentials.txt" % SETDIR)
    credentials = get_bifrost_credentials( bifrostfn )   

    # Open SSH connection to Bifrost server
    #   NB: Should rely on on publickey athentication to avoid login password in script issue.
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.connect(credentials['BIFROST_SERVER_IP'], username='bifrostadmin', password='bifrostadmin')
    # Create an SFTP connection
    ftp = ssh.open_sftp()

    # Open connection to production database
    if NEWSYS == 1:
        # Connect to ice charts database
        dbcon = pg.connect(dbname='Icecharts', host=credentials['BIFROST_SERVER_IP'], user='bifrostadmin', passwd='bifrostadmin')

    # Loop through folders
    for dirname in cleardirs:

        #   Zip folder
        zipfn = createzip( dirname )

        # Check to see if destination folder exists on Istjenesten server
        archyr = dirname[0:4]
        cmd = ("cd \"%s/%s\"" % (PRODARCH,archyr))
        # print cmd
        stdin, stdout, stderr = ssh.exec_command(cmd)
        errors = stderr.readlines()
        if len(errors) > 0:
            restxt = errors[0]
            if restxt.find('No such file or directory') != -1:
                print ("Creating directory for year %s" % archyr)
                if activeflg == 1:
                    cmd = ("mkdir \"%s/%s\"" % (PRODARCH,archyr))
                    # print cmd
                    stdin, stdout, stderr = ssh.exec_command(cmd)
                    errors = stderr.readlines()
                    if len(errors) > 0:
                        restxt = errors[0]
                        print 'Unable to create directory'
                        print restxt
                        sys.exit()
                else:
                    print cmd
            else:
                print 'Another type of error:'
                print restxt
                sys.exit()

        #   Copy zip file to Istjenesten server
        cmd = ("/usr/bin/scp %s" % zipfn )
        remotefn = ("%s/%s/ice_%s_production_backup.zip" % (PRODARCH,archyr,dirname))
        # print remotefn
        if activeflg == 1:
            ftp.put(zipfn,remotefn )
        else:
            print remotefn
        # Adjust file permission to make it read-only
        cmd = ("chmod -w \"%s\"" % remotefn)
        if activeflg == 1:
            stdin, stdout, stderr = ssh.exec_command(cmd)
        else:
            print cmd

        #   Delete folder
        fullpath = ("%s/%s" % (ICEDIR,dirname))
        print fullpath
        if activeflg == 1:
            shutil.rmtree(fullpath)
        else:
            print ("Deleting %s" % fullpath)

        #   Delete temporary zip file
        os.remove( zipfn )

        #   Set cacheclear_flg and cacheclear_dt
        if NEWSYS == 1:
            # Find the database entry
            chkyr = int(archyr)
            chkmh = int(dirname[4:6])
            chkdy = int(dirname[6:8])
            chkreg = dirname[-3:]
            # print chkyr,chkmh,chkdy,chkreg
            if chkreg == 'ARC':
                regionstr = 'arctic'
            elif chkreg == 'ANT':
                regionstr = 'antarctic'
            checkres = dbcon.query(("SELECT id FROM production WHERE icechart_date = \'%4d-%02d-%02d\' AND region = \'%s\'" \
                % (chkyr,chkmh,chkdy,regionstr)))
            current = checkres.ntuples()
            if current == 0:
                print ("No entry in database for date %4d-%02d-%02d" % (chkyr,chkmh,chkdy))
            elif current > 1:
                print ("Too many (%d) database entries found" % current)
                sys.exit()
            else:
                itemid = checkres.getresult()[0][0]
                print ("Database entry is #%d" % itemid)

                # Update the production database
                timenow = datetime.now()
                curdtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
                sqltxt = ("UPDATE production SET cacheclear_flg = 1, cacheclear_dt = \'%s\' WHERE id = %d" \
                    % (curdtstr,itemid))
                print sqltxt
                if activeflg == 1:
                    checkres = dbcon.query(sqltxt)

    # Close SFTP and ssh connections
    ftp.close()
    ssh.close()

    # Close database connection
    if NEWSYS == 1:
        dbcon.close()
