#!/usr/bin/python

# Name:          check_coastline.py
# Purpose:       Check for coastline subset files in ICE/Coastline directory.
#                If files are not present, create them from database.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import subprocess

import pg

import osgeo.ogr as ogr
import osgeo.osr as osr

from get_bifrost_credentials import get_bifrost_credentials
from get_map_projections import get_map_projections
from copy_ice import create_extent_poly
from progress_bar import printProgress


# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)
PREPAIR = '/usr/local/bin/prepair'

# Bufferr size for number of geometries to load simultaneously
BUFSIZE = 1000

# Filter for Antarctic Digital Database area
ADDWKT = 'POLYGON ((-3118455.3489247309044 670718.665725806728005,-2799676.025403224863112 3515211.090994623489678,314552.750537635758519 4397984.602284945547581,4402244.986755767837167 1245352.014540192205459,1116820.6398641820997 -4909541.07806733623147,-3118455.3489247309044 670718.665725806728005))'

# Fix polygon by closing any open rings
def fix_polygon( inpgeom ):

    # Get number of sub-geometries
    npoly = inpgeom.GetGeometryCount()
    # print npoly

    # Create output multipolygon
    outgeom = ogr.Geometry(ogr.wkbMultiPolygon)

    for i in range(npoly):
        subpoly = inpgeom.GetGeometryRef(i)
        # print subpoly.ExportToWkt()
        nrings = subpoly.GetGeometryCount()
        # print nrings

        # Create new subpoly
        poly1 = ogr.Geometry(ogr.wkbPolygon)

        for j in range(nrings):
            ring = subpoly.GetGeometryRef(j)
            # print ring.ExportToWkt()
            # print dir(ring)
            ring.CloseRings()
            # print ring.ExportToWkt()

            # Add ring to polygon
            poly1.AddGeometry(ring)

        # Add polygon to multipolygon
        outgeom.AddGeometry(poly1)
 
    #print outgeom.ExportToWkt()
    return outgeom


# Generate GSHHS files
def create_GSHHS_files( datadir, regcode ):

    # Change error reporting level
    ogr.UseExceptions()
    # ogr.PushErrorHandler('CPLQuietErrorHandler')

    # GSHHS levels
    levels = [ 'c', 'l', 'i', 'h', 'f' ]

    # Region names
    regnames = { 'ARC': 'ARCTIC', 'ANT': 'ANTARCTIC' }

    # Get Bifrost server credentials from file
    bifrostfn = ("%s/bifrost_credentials.txt" % SETDIR)
    credentials = get_bifrost_credentials( bifrostfn )   

    # Region specific settings, map projection and extent
    mapfn = ("%s/map_projections.txt" % SETDIR)
    map_projections = get_map_projections( mapfn )

    # Set up spatial reference systems and transforms
    keystr = ("SRS_%s_WKT" % regnames[regcode])
    inp_srs = osr.SpatialReference()
    inp_srs.ImportFromEPSG(4326)
    out_srs = osr.SpatialReference()
    out_srs.ImportFromWkt(map_projections[keystr])
    inp2out = osr.CoordinateTransformation( inp_srs, out_srs )    

    # Create an extent polygon
    if regcode == 'ARC':
        ullr = [ -5000000, 5000000, 5000000, -5000000 ]
    elif regcode == 'ANT':
        ullr = [ -7000000, 7000000, 7000000, -7000000 ]
    else:
        print ("Unknown region \'%s\'." % regcode)
        sys.exit()
    ext_poly = create_extent_poly(ullr)

    # Create filter polygon for Antarctic Digital Database
    add_area = ogr.CreateGeometryFromWkt(ADDWKT)
    
    # Connect to Ocean database
    dbcon = pg.connect(dbname='Ocean', host=credentials['BIFROST_SERVER_IP'], user='bifrostadmin', passwd='bifrostadmin')

    # Initialise OGR Shapefile driver
    shpdrv = ogr.GetDriverByName("ESRI Shapefile")

    # Loop through levels
    for level in levels:
        # Get list of feature gid
        # ST_Extent is xmin ymin, xmax ymax
        if regcode.find('ARC') > -1:
            latlimit = "> 0.0"
        elif regcode.find('ANT') > -1:
            latlimit = "< 0.0"
        sql = ("SELECT gid FROM gshhs_%s WHERE ST_Y(ST_Centroid(ST_Envelope(geom))) %s ORDER BY gid;" \
            % (level,latlimit))
        gidlist = (dbcon.query(sql)).getresult()
        nfeat = len(gidlist)
        print ("  GSHHS level = %s   %8d features" % (level,nfeat))

        # Open an output file
        shpfn = ("%s/Coastline/GSHHS_%s_%s.shp" % (datadir,level,regcode))
        if os.path.exists(shpfn):
            shpdrv.DeleteDataSource(shpfn)
        out_ds = shpdrv.CreateDataSource(shpfn)
        out_lay = out_ds.CreateLayer( ("GSHHS_%s_%s" % (level,regcode)), out_srs, \
            ogr.wkbMultiPolygon)
        gid_field = ogr.FieldDefn("gid", ogr.OFTInteger)
        out_lay.CreateField(gid_field)

        # Start progress bar
        printProgress(0, nfeat, prefix = 'Progress:', suffix = 'Complete', barLength = 50)

        # Loop through geometries in blocks to avoid stressing memory
        stridx = 0
        endidx = stridx + BUFSIZE
        if endidx > nfeat:
            endidx = nfeat
        while stridx < nfeat:
            # Limit and offset for SQL query
            limit = endidx - stridx
            offset = stridx
            # print limit, offset

            # Get geometries
            sql = ("SELECT gid,ST_AsText(geom) FROM gshhs_%s WHERE ST_Y(ST_Centroid(ST_Envelope(geom))) %s" \
                % (level,latlimit))
            sql = ("%s ORDER BY gid LIMIT %d OFFSET %d;" % (sql,limit,offset))
            # print sql
            geomlist = (dbcon.query(sql)).getresult()
            # print gidlist[endidx-1]
            # print geomlist[-1]

            # Loop throuugh geometries and do further filtering/clipping
            for geomrec in geomlist:
                geom = ogr.CreateGeometryFromWkt( geomrec[1] )
                # print geom.ExportToWkt()
                geom.Transform( inp2out )
                # print geom.ExportToWkt()

                # Check geometry validity
                try:
                    if geom.IsValid() == False:
                        print 'Error with geometry'
                except:
                    # print geom.ExportToWkt()
                    newgeom = fix_polygon( geom )
                    # print newgeom.ExportToWkt()

                add_flg = 0
                if geom.IsValid():

                    # Check for reasons to add geometry to file
                    if geom.Within(ext_poly):
                        # Geometry is within target extent, therefore add to file
                        add_flg = 1
                        outgeom = geom.Clone()
                    elif geom.Intersects(ext_poly):
                        # Geometry intersects the target extent, therefore add the intersection geometry to file
                        add_flg = 1
                        outgeom = geom.Intersection(ext_poly)

                    # If Antarctic, check for reason not to add geometry to file
                    # i.e. the geometry lies within the region covered by ADD
                    if regcode == 'ANT':
                        if outgeom.Within(add_area):
                            add_flg = 0
                       
                # Add geometry to file
                if add_flg == 1:
                    feature = ogr.Feature(out_lay.GetLayerDefn())
                    feature.SetField( "gid", int(geomrec[0]) )
                    feature.SetGeometry(outgeom)
                    out_lay.CreateFeature(feature)
                    feature.Destroy()

            # Increment indexes
            stridx = endidx
            endidx = stridx + BUFSIZE
            if endidx > nfeat:
                endidx = nfeat

            # Update progress bar
            printProgress(stridx, nfeat, prefix = 'Progress:', suffix = 'Complete', barLength = 50)

        # Close the output Shapefile
        out_ds.Destroy()

    status = 1
    return status


# Generate ADD files
def create_ADD_files( datadir, prepair_flg ):

    # Change error reporting level
    ogr.UseExceptions()
    # ogr.PushErrorHandler('CPLQuietErrorHandler')

    # ADD levels
    levels = [ 'l', 'i', 'h' ]

    # Get Bifrost server credentials from file
    bifrostfn = ("%s/bifrost_credentials.txt" % SETDIR)
    credentials = get_bifrost_credentials( bifrostfn )   

    # Region specific settings, map projection and extent
    mapfn = ("%s/map_projections.txt" % SETDIR)
    map_projections = get_map_projections( mapfn )

    # Set up spatial reference systems and transforms
    keystr = "SRS_ANTARCTIC_WKT"
    inp_srs = osr.SpatialReference()
    inp_srs.ImportFromEPSG(3031)
    out_srs = osr.SpatialReference()
    out_srs.ImportFromWkt(map_projections[keystr])
    inp2out = osr.CoordinateTransformation( inp_srs, out_srs )    

    # Create filter polygon for Antarctic Digital Database
    add_area = ogr.CreateGeometryFromWkt(ADDWKT)

    # Antarctic coastline file areas
    add_types = [ 'ant', 'subant' ]
    
    # Connect to Ocean database
    dbcon = pg.connect(dbname='Ocean', host=credentials['BIFROST_SERVER_IP'], user='bifrostadmin', passwd='bifrostadmin')

    # Initialise OGR Shapefile driver
    shpdrv = ogr.GetDriverByName("ESRI Shapefile")

    # Loop through levels
    for level in levels:
        # Open an output file
        shpfn = ("%s/Coastline/ADD_%s_ANT.shp" % (datadir,level))
        if os.path.exists(shpfn):
            shpdrv.DeleteDataSource(shpfn)
        out_ds = shpdrv.CreateDataSource(shpfn)
        out_lay = out_ds.CreateLayer( ("ADD_%s_ANT" % level), out_srs, \
            ogr.wkbMultiPolygon)
        gid_field = ogr.FieldDefn("gid", ogr.OFTInteger)
        out_lay.CreateField(gid_field)
        surf_field = ogr.FieldDefn("surface", ogr.OFTString)
        surf_field.SetWidth(16)
        out_lay.CreateField(surf_field)

        # Loop through coastline file types
        for add_type in add_types:

            # Get list of feature gid
            # ST_Extent is xmin ymin, xmax ymax
            sql = ("SELECT gid FROM add_%s_coastline_%s ORDER BY gid;" \
                % (add_type,level))
            gidlist = (dbcon.query(sql)).getresult()
            nfeat = len(gidlist)
            if add_type == 'ant':
                print ("  ADD Antarctic Coastline level = %s   %8d features" % (level,nfeat))
            elif add_type == 'subant':
                print ("  ADD Sub-Antarctic Coastline level = %s   %8d features" % (level,nfeat))
            else:
                print ("Unknown ADD type \'%s\'" %  add_type)
                sys.exit()

            # Start progress bar
            printProgress(0, nfeat, prefix = 'Progress:', suffix = 'Complete', barLength = 50)

            # Loop through geometries in blocks to avoid stressing memory
            stridx = 0
            endidx = stridx + BUFSIZE
            if endidx > nfeat:
                endidx = nfeat
            while stridx < nfeat:
                # Limit and offset for SQL query
                limit = endidx - stridx
                offset = stridx
                # print limit, offset

                # Get geometries
                if add_type == 'ant':
                    sql = ("SELECT gid,ST_AsText(geom),surface FROM add_ant_coastline_%s" % level)
                elif add_type == 'subant':
                    sql = ("SELECT gid,ST_AsText(geom) FROM add_subant_coastline_%s" % level)
                sql = ("%s ORDER BY gid LIMIT %d OFFSET %d;" % (sql,limit,offset))
                # print sql
                geomlist = (dbcon.query(sql)).getresult()
                # print gidlist[endidx-1]
                # print geomlist[-1]

                # Loop throuugh geometries and do further filtering/clipping
                for geomrec in geomlist:
                    geom = ogr.CreateGeometryFromWkt( geomrec[1] )
                    # print geom.ExportToWkt()
                    geom.Transform( inp2out )
                    # print geom.ExportToWkt()

                    # Check geometry validity
                    if prepair_flg == 1:
                        if geom.IsValid() == False:
                            # Use prepair to fix polygon
                            geomwkt = geom.ExportToWkt()
                            wktfn = '/home/bifrostanalyst/tmp/geometry_wkt.txt'
                            fout = open( wktfn, 'w' )
                            fout.write( ("%s\n" % geomwkt) )
                            fout.close()
                            cmd = ("%s -f \'%s\'" % (PREPAIR,wktfn))
                            newwkt = subprocess.check_output(cmd,shell=True)
                            geom = ogr.CreateGeometryFromWkt( newwkt )
                            # print geom.IsValid()
                            os.remove(wktfn)

                    add_flg = 0

                    # Check for reasons to add geometry to file
                    if geom.Within(add_area):
                        # Geometry is within target extent, therefore add to file
                        add_flg = 1
                        outgeom = geom.Clone()

                    # Add geometry to file
                    if add_flg == 1:
                        feature = ogr.Feature(out_lay.GetLayerDefn())
                        feature.SetField( "gid", int(geomrec[0]) )
                        if add_type == 'ant':
                            feature.SetField( "surface", geomrec[2].strip() )
                        elif add_type == 'subant':
                            feature.SetField( "surface", 'land' )
                        feature.SetGeometry(outgeom)
                        out_lay.CreateFeature(feature)
                        feature.Destroy()

                # Increment indexes
                stridx = endidx
                endidx = stridx + BUFSIZE
                if endidx > nfeat:
                    endidx = nfeat

                # Update progress bar
                printProgress(stridx, nfeat, prefix = 'Progress:', suffix = 'Complete', barLength = 50)

        # Close the output Shapefile
        out_ds.Destroy()
        # sys.exit()

    status = 1
    return status


# Check to see if we need to create coastline data
def check_coastline( regcode, datadir, prepair_flg ):

    # See if GSHHS_h file for region exists
    testfn = ("%s/Coastline/GSHHS_h_%s.shp" % (datadir,regcode))
    if os.path.isfile( testfn ) == False:
        print "Creating local coastline Shapefiles:"

        # Get GSHHS data
        status = create_GSHHS_files( datadir, regcode )

        # Add Antarctic Digital Database (ADD) data if Antarctic
        if regcode.find('ANT') > -1:
            status = create_ADD_files( datadir, prepair_flg )

        status = 1
    else:
        status = 0

    return status


if __name__ == '__main__':

    status = check_coastline( 'ANT', '/home/bifrostanalyst/ICS', 0 )
    print status
