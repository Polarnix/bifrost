#!/usr/bin/python

# Name:          02_livedata.py
# Purpose:       Python script to copy satellite image files from Bifrost server to the LiveData directory.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-13a - Add SMOS copy capability.
#                2017-ix-13b - Add ASCAT copy capability. 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, platform, shutil
from subprocess import call
import time
from datetime import datetime, timedelta
import argparse

import pg

from get_bifrost_credentials import get_bifrost_credentials
from get_ip_address import get_ip_address


# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
ANCDIR = ("%s/Ancillary/Bifrost" % BASEDIR)
INCDIR = ("%s/Include/Bifrost" % BASEDIR)
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)
LIVEDIR = ("%s/LiveData" % BASEDIR)
ICEDATA = ("/mnt/bifrostsat/Data")


# Get Sentinel-1 files from Bifrost server
def get_S1_files( dbcon, year, month, day, hour, minute, regstr, livelist ):

    # Check database server to see list of file dates/times
    if regstr == 'arctic':
        procstr = 'procflg = 1'
        intstr = '15 hours'
        regdir = 'Arctic'
    elif regstr == 'antarctic':
        procstr = 'procflg = -1'
        intstr = '72 hours'
        regdir = 'Antarctic'
    else:
        print ("02_livedata.py: Unknown region \'%s\'" % regstr)
        sys.exit()
    sqltxt = "SELECT id,datetime FROM s1_files WHERE"
    sqltxt = ("%s datetime > timestamp \'%4d-%02d-%02d %02d:%02d:00\' - interval \'%s\'" \
        % (sqltxt,year,month,day,hour,minute,intstr))
    sqltxt = ("%s AND %s ORDER BY datetime;" % (sqltxt,procstr))
    # print sqltxt
    checkres = dbcon.query(sqltxt)
    numS1 = checkres.ntuples()
    idlist = []
    S1list = []
    if numS1 > 0:
        for i in range(numS1):
            idlist.append( checkres.getresult()[i][0] )
            S1list.append( checkres.getresult()[i][1] )
    # print len(S1list)

    # Create a list of processed Sentinel-1 files (excluding incidence angle images) on the server
    S1datadir = ("%s/S1/%s/GeoTIFF" % (ICEDATA,regdir))
    rawlist = os.listdir( S1datadir )
    S1files = []
    for i in range(numS1):
        S1dt = datetime.strptime(S1list[i][0:19],"%Y-%m-%d %H:%M:%S")
        searchstr = S1dt.strftime("_%Y%m%dT%H%M%S_%Y")
        # print searchstr
        for fname in rawlist:
            if fname.find(searchstr) > -1 and fname.find('incidence') == -1:
                # print ("   %s" % fname)
                S1files.append( fname )

    # Compare lists and only copy those files that are not already present
    count = 0
    for S1fn in S1files:
        copyflg = 1
        for livefn in livelist:
            if S1fn == livefn:
                copyflg = 0
        # print S1fn, copyflg
        if copyflg == 1:
            srcfn = ("%s/%s" % (S1datadir,S1fn))
            dstfn = ("%s/%s" % (LIVEDIR,S1fn))
            shutil.copyfile( srcfn, dstfn )
            count = count + 1

    print ("%d S1 files copied for %s." % (count,regdir))


# Get RS2 files from Bifrost server
def get_rs2_files( dbcon, year, month, day, regstr, livelist ):

    # Check database server to see list of file dates/times
    if regstr == 'arctic':
        procstr = 'procflg = 1'
        intstr = '15 hours'
        regdir = 'Arctic'
    elif regstr == 'antarctic':
        procstr = 'procflg = -1'
        intstr = '72 hours'
        regdir = 'Antarctic'
    else:
        print ("02_livedata.py: Unknown region \'%s\'" % regstr)
        sys.exit()
    sqltxt = "SELECT id,datetime FROM rs2_files WHERE"
    sqltxt = ("%s datetime > timestamp \'%4d-%02d-%02d 15:00:00\' - interval \'%s\'" \
        % (sqltxt,year,month,day,intstr))
    sqltxt = ("%s AND %s ORDER BY datetime;" % (sqltxt,procstr))
    # print sqltxt
    checkres = dbcon.query(sqltxt)
    numrs2 = checkres.ntuples()
    idlist = []
    rs2list = []
    if numrs2 > 0:
        for i in range(numrs2):
            idlist.append( checkres.getresult()[i][0] )
            rs2list.append( checkres.getresult()[i][1] )
    # print rs2list

    # Create a list of processed RS2 files (excluding incidence angle images) on the server
    rs2datadir = ("%s/RS2/%s/GeoTIFF" % (ICEDATA,regdir))
    rawlist = os.listdir( rs2datadir )
    rs2files = []
    for i in range(numrs2):
        rs2dt = datetime.strptime(rs2list[i],"%Y-%m-%d %H:%M:%S")
        searchstr = rs2dt.strftime("_%Y%m%d_%H%M%S_")
        # print searchstr
        for fname in rawlist:
            if fname.find(searchstr) > -1 and fname.find('incidence') == -1:
                # print ("   %s" % fname)
                rs2files.append( fname )

    # Compare lists and only copy those files that are not already present
    count = 0
    for rs2fn in rs2files:
        copyflg = 1
        for livefn in livelist:
            if rs2fn == livefn:
                copyflg = 0
        # print rs2fn, copyflg
        if copyflg == 1:
            srcfn = ("%s/%s" % (rs2datadir,rs2fn))
            dstfn = ("%s/%s" % (LIVEDIR,rs2fn))
            shutil.copyfile( srcfn, dstfn )
            count = count + 1

    print ("%d RS2 files copied for %s." % (count,regdir))


# Get COMSO SkyMed files from Bifrost server
def get_csks_files( dbcon, year, month, day, regstr, livelist ):

    # Check database server to see list of file dates/times
    if regstr == 'arctic':
        procstr = 'procflg = 1'
        intstr = '24 hours'
        regdir = 'Arctic'
    elif regstr == 'antarctic':
        procstr = 'procflg = -1'
        intstr = '72 hours'
        regdir = 'Antarctic'
    else:
        print ("02_livedata.py: Unknown region \'%s\'" % regstr)
        sys.exit()
    sqltxt = "SELECT id,datetime FROM csks_files WHERE"
    sqltxt = ("%s datetime > timestamp \'%4d-%02d-%02d 15:00:00\' - interval \'%s\'" \
        % (sqltxt,year,month,day,intstr))
    sqltxt = ("%s AND %s ORDER BY datetime;" % (sqltxt,procstr))
    # print sqltxt
    checkres = dbcon.query(sqltxt)
    numcsks = checkres.ntuples()
    idlist = []
    cskslist = []
    if numcsks > 0:
        for i in range(numcsks):
            idlist.append( checkres.getresult()[i][0] )
            cskslist.append( checkres.getresult()[i][1] )
    # print cskslist

    # Create a list of processed COSMO SkyMed files (excluding incidence angle images) on the server
    csksdatadir = ("%s/CSKS/%s/GeoTIFF" % (ICEDATA,regdir))
    rawlist = os.listdir( csksdatadir )
    csksfiles = []
    for i in range(numcsks):
        csksdt = datetime.strptime(cskslist[i],"%Y-%m-%d %H:%M:%S")
        searchstr = csksdt.strftime("_%Y%m%d%H%M%S_")
        # print searchstr
        for fname in rawlist:
            if fname.find(searchstr) > -1 and fname.find('incidence') == -1:
                # print ("   %s" % fname)
                csksfiles.append( fname )

    # Compare lists and only copy those files that are not already present
    count = 0
    for csksfn in csksfiles:
        copyflg = 1
        for livefn in livelist:
            if csksfn == livefn:
                copyflg = 0
        # print csksfn, copyflg
        if copyflg == 1:
            srcfn = ("%s/%s" % (csksdatadir,csksfn))
            dstfn = ("%s/%s" % (LIVEDIR,csksfn))
            shutil.copyfile( srcfn, dstfn )
            count = count + 1

    print ("%d COMSO SkyMed files copied for %s." % (count,regdir))


# Get AMSR2 files from Bifrost server
def get_amsr2_files( year, month, day, regstr, livelist ):

    # Create a list of processed AMSR2 files on the server
    if regstr == 'arctic':
        procstr = 'procflg = 1'
        intstr = '24 hours'
        regdir = 'Arctic'
    elif regstr == 'antarctic':
        procstr = 'procflg = -1'
        intstr = '72 hours'
        regdir = 'Antarctic'
    else:
        print ("02_livedata.py: Unknown region \'%s\'" % regstr)
        sys.exit()
    amsr2datadir = ("%s/AMSR2/%s/GeoTIFF" % (ICEDATA,regdir))
    rawlist = os.listdir( amsr2datadir )
    amsr2files = []
    amsr2dt = datetime(year,month,day,15,0,0) - timedelta(days=1)
    searchstr = amsr2dt.strftime("%Y%m%d")
    # print searchstr
    for fname in rawlist:
        if fname.find(searchstr) > -1:
            # print ("   %s" % fname)
            amsr2files.append( fname )

    # Compare lists and only copy those files that are not already present
    count = 0
    for amsr2fn in amsr2files:
        copyflg = 1
        for livefn in livelist:
            if amsr2fn == livefn:
                copyflg = 0
        # print amsr2fn, copyflg
        if copyflg == 1:
            srcfn = ("%s/%s" % (amsr2datadir,amsr2fn))
            dstfn = ("%s/%s" % (LIVEDIR,amsr2fn))
            shutil.copyfile( srcfn, dstfn )
            count = count + 1

    print ("%d AMSR2 files copied for %s." % (count,regdir))


# Get SMOS files from Bifrost server
def get_smos_files( year, month, day, regstr, livelist ):

    # Create a list of processed SMOS files on the server
    if regstr == 'arctic':
        procstr = 'procflg = 1'
        intstr = '24 hours'
        regdir = 'Arctic'
    elif regstr == 'antarctic':
        procstr = 'procflg = -1'
        intstr = '72 hours'
        regdir = 'Antarctic'
    else:
        print ("02_livedata.py: Unknown region \'%s\'" % regstr)
        sys.exit()
    smosdatadir = ("%s/SMOS/%s/GeoTIFF" % (ICEDATA,regdir))
    rawlist = os.listdir( smosdatadir )
    smosfiles = []
    smosdt = datetime(year,month,day,15,0,0) - timedelta(days=1)
    searchstr = smosdt.strftime("%Y%m%d")
    # print searchstr
    for fname in rawlist:
        if fname.find(searchstr) > -1:
            # print ("   %s" % fname)
            smosfiles.append( fname )

    # Compare lists and only copy those files that are not already present
    count = 0
    for smosfn in smosfiles:
        copyflg = 1
        for livefn in livelist:
            if smosfn == livefn:
                copyflg = 0
        # print smosfn, copyflg
        if copyflg == 1:
            srcfn = ("%s/%s" % (smosdatadir,smosfn))
            dstfn = ("%s/%s" % (LIVEDIR,smosfn))
            shutil.copyfile( srcfn, dstfn )
            count = count + 1

    print ("%d SMOS files copied for %s." % (count,regdir))


# Get ASCAT files from Bifrost server
def get_ascat_files( year, month, day, regstr, livelist ):

    # Create a list of processed ASCAT files on the server
    if regstr == 'arctic':
        procstr = 'procflg = 1'
        intstr = '24 hours'
        regdir = 'Arctic'
    elif regstr == 'antarctic':
        procstr = 'procflg = -1'
        intstr = '72 hours'
        regdir = 'Antarctic'
    else:
        print ("02_livedata.py: Unknown region \'%s\'" % regstr)
        sys.exit()
    ascatdatadir = ("%s/ASCAT/%s/GeoTIFF" % (ICEDATA,regdir))
    rawlist = os.listdir( ascatdatadir )
    ascatfiles = []
    ascatdt = datetime(year,month,day,15,0,0) - timedelta(days=1)
    searchstr = ascatdt.strftime("%Y%m%d")
    # print searchstr
    for fname in rawlist:
        if fname.find(searchstr) > -1:
            # print ("   %s" % fname)
            ascatfiles.append( fname )

    # Compare lists and only copy those files that are not already present
    count = 0
    for ascatfn in ascatfiles:
        copyflg = 1
        for livefn in livelist:
            if ascatfn == livefn:
                copyflg = 0
        # print ascatfn, copyflg
        if copyflg == 1:
            srcfn = ("%s/%s" % (ascatdatadir,ascatfn))
            dstfn = ("%s/%s" % (LIVEDIR,ascatfn))
            shutil.copyfile( srcfn, dstfn )
            count = count + 1

    print ("%d ASCAT files copied for %s." % (count,regdir))


# Get MODIS files from Bifrost server
def get_modis_files( dbcon, year, month, day, hour, minute, regstr, livelist ):

    # Check database server to see list of file dates/times
    if regstr == 'arctic':
        intstr = '15 hours'
        regdir = 'Arctic'
    elif regstr == 'antarctic':
        intstr = '72 hours'
        regdir = 'Antarctic'
    else:
        print ("02_livedata.py: Unknown region \'%s\'" % regstr)
        sys.exit()
    sqltxt = "SELECT id,datetime FROM modis_files WHERE"
    sqltxt = ("%s datetime > timestamp \'%4d-%02d-%02d %02d:%02d:00\' - interval \'%s\'" \
        % (sqltxt,year,month,day,hour,minute,intstr))
    sqltxt = ("%s AND procflg = 1 ORDER BY datetime;" % (sqltxt))
    # print sqltxt
    checkres = dbcon.query(sqltxt)
    nummodis = checkres.ntuples()
    idlist = []
    modislist = []
    if nummodis > 0:
        for i in range(nummodis):
            idlist.append( checkres.getresult()[i][0] )
            modislist.append( checkres.getresult()[i][1] )
    # print modislist

    # Create a list of processed MODIS files on the server
    #    Exclude files from nsr region ('nsr_')
    modisdatadir = ("%s/MODIS/%s/GeoTIFF" % (ICEDATA,regdir))
    rawlist = os.listdir( modisdatadir )
    modisfiles = []
    for i in range(nummodis):
        modisdt = datetime.strptime(modislist[i],"%Y-%m-%d %H:%M:%S")
        searchstr = modisdt.strftime("_%Y%m%d_%H%M_")
        # print searchstr
        for fname in rawlist:
            if fname.find(searchstr) > -1 and fname.find('nsr_') == -1:
                # print ("   %s" % fname)
                modisfiles.append( fname )

    # Compare lists and only copy those files that are not already present
    count = 0
    for modisfn in modisfiles:
        copyflg = 1
        for livefn in livelist:
            if modisfn == livefn:
                copyflg = 0
        # print modisfn, copyflg
        if copyflg == 1:
            srcfn = ("%s/%s" % (modisdatadir,modisfn))
            dstfn = ("%s/%s" % (LIVEDIR,modisfn))
            shutil.copyfile( srcfn, dstfn )
            count = count + 1

    print ("%d MODIS files copied for %s." % (count,regdir))


# Get NOAA files from Bifrost server
def get_noaa_files( year, month, day, livelist ):

    # Create a list of processed NOAA files on the server
    noaadatadir = ("%s/NOAA/ArcView" % ICEDATA)
    rawlist = os.listdir( noaadatadir )
    noaafiles = []
    reslist = []      # List of resolutions to reprocess images to
    minimumdt = datetime(year,month,day,4,0,0)
    for fname in rawlist:
        if fname.find('msv') == -1 and fname.find('noaa20') > -1:
            noaadtstr = fname[4:16]
            noaadt = datetime.strptime(noaadtstr,"%Y%m%d%H%M")
            if noaadt > minimumdt and fname.find('.xml') == -1:
                # print ("   %s" % fname)
                noaafiles.append( fname )
                reslist.append( 500 )
        elif fname.find('msv') > -1 and fname.find('noaa20') > -1:
            noaadtstr = fname[4:16]
            noaadt = datetime.strptime(noaadtstr,"%Y%m%d%H%M")
            if noaadt > minimumdt and fname.find('.xml') == -1:
                # print ("   %s" % fname)
                noaafiles.append( fname )
                reslist.append( 1000 )
        elif fname.find('noaa_mosaic') > -1:
            noaadtstr = fname[12:20]+fname[21:25]
            noaadt = datetime.strptime(noaadtstr,"%Y%m%d%H%M")
            if noaadt > minimumdt and fname.find('.xml') == -1:
                # print ("   %s" % fname)
                noaafiles.append( fname )
                reslist.append( 1000 )
    # print noaafiles

    # Compare lists and only copy those files that are not already present
    count = 0
    for noaafn,targres in zip(noaafiles,reslist):
        copyflg = 1
        for livefn in livelist:
            # noaafn[0:-1] is to correct for Frodeism of naming files '.tiff' rather than '.tif'
            if noaafn[0:-1] == livefn:
                copyflg = 0
        # print noaafn, copyflg
        if copyflg == 1:
            srcfn = ("%s/%s" % (noaadatadir,noaafn))
            dstfn = ("%s/%s" % (LIVEDIR,noaafn[0:-1]))

            # Cannot do a straight copy as these files have the old ArcView projection and need warping
            cmd = "/usr/bin/gdalwarp -of \"GTiff\" -ot Byte -co \"COMPRESS=LZW\" -co \"BIGTIFF=YES\""
            cmd = ("%s -s_srs \"+proj=stere +lat_0=90 +lat_ts=90 +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs\"" % cmd)
            cmd = ("%s -s_srs \"+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84\"" % cmd)
            cmd = ("%s -tr %d %d -r bilinear -dstnodata 0 -q" % (cmd,targres,targres))
            cmd = ("%s \"%s\" \"%s\"" % (cmd,srcfn,dstfn))
            # print cmd
            try:
                retcode = call(cmd, shell=True)
                if retcode < 0:
                    print >>sys.stderr, "Child was terminated by signal", -retcode
                else:
                    # print >>sys.stderr, "Child returned", retcode
                    pass
            except OSError, e:
                print >>sys.stderr, "Execution failed:", e

            count = count + 1

    print ("%d NOAA files copied." % count)


# Date reformatting
def mkdate(datestr):
    return time.strptime(datestr,'%Y%m%d')


# Main function
if __name__ == "__main__":

    # Get today's date
    today = time.localtime()
    todaystr = time.strftime('%Y-%m-%d',today)
    # print todaystr

    # Get input parameters using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d","--date",help="Date for data retrieval [yyyymmdd]",type=mkdate,default=today)
    parser.add_argument("-t","--time",help="Time for data retrieval [hhmm]",default='0800')
    args = parser.parse_args()
    year = (args.date).tm_year
    month = (args.date).tm_mon
    day = (args.date).tm_mday
    timestr = args.time
    hour = int( timestr[0:2] )
    minute = int( timestr[2:4] )
    datestr = ("%4d%02d%02d" % (year,month,day))
    dtstamp = datetime( year, month, day, hour, minute, 0 )
    dow = dtstamp.isoweekday()
    # print datestr, timestr, len(sys.argv)
    if len(sys.argv) == 1:
        autoflg = True
    elif len(sys.argv) == 3 or len(sys.argv) == 5:
        autoflg = False
    else:
        print 'Unable to set autoflg', len(sys.argv)
        sys.exit()

    print ("02_livedata.py at %4d-%02d-%02d %02d:%02d" % (year,month,day,hour,minute))
    
    # Open exclusion list (Public Holidays) file
    excfn = ("%s/exclusion_list.txt" % INCDIR)
    excf = open( excfn, 'r' )

    # Get computer name
    hoststr = platform.node()

    # Get computer IP address
    ipstr = get_ip_address()
    
    # Check for reasons not to continue
    if autoflg == True:
        if dow > 5:  
            # 1. Automatic running on a weekend
            print "02_livedata: Trying to run automatically on a weekend. Stopping."
            sys.exit()
        else:
            # 2. Automatic running on a weekday, check exclusion list (Public Holidays) list
            excflg = False
            for excdate in excf:
                # print excdate, len(excdate)
                if excdate.strip() == datestr:
                    print"02_livedata: Trying to run automatically on a public holiday. Stopping."
                    sys.exit()
    else:
        if dow > 5:
            # 3. Manual running on a weekend - just warn but do not stop
            print "02_livedata: You are running an ice chart on a weekend?"
        else:
            # 4. Manual running on a weekday, check exclusion list (Public Holidays) list
            #    Just warn, but do not stop
            excflg = False
            for excdate in excf:
                # print excdate, len(excdate)
                if excdate.strip() == datestr:
                    print"02_livedata: You are running an ice chart on a public holiday?"
                    excflg = True
    
    # Close exclusion list file
    excf.close()

    # Get Bifrost server credentials from file
    bifrostfn = ("%s/bifrost_credentials.txt" % SETDIR)
    credentials = get_bifrost_credentials( bifrostfn )   

    # Connect to ice charts database
    dbcon = pg.connect(dbname='Icecharts', host=credentials['BIFROST_SERVER_IP'], user='bifrostadmin', passwd='bifrostadmin')
    # Connect to satellite data processing database (Sentinel-1)
    satdbcon = pg.connect(dbname='Satellite', host=credentials['BIFROST_SERVER_IP'], user='bifrostsat', passwd='bifrostsat')

    # Check current status of database for ice charts on this computer
    sqltxt = ("SELECT new_flg,finish_flg,id,region FROM production WHERE icechart_date = \'%s\' AND computer = \'%s\';" \
        % (dtstamp.strftime('%Y-%m-%d'),ipstr) )
    checkres = dbcon.query(sqltxt)
    current = checkres.ntuples()
    new_flg = []
    finish_flg = []
    icechart_id = []
    regions = []
    if current > 0 and current <= 2:
        for i in range(current):
            new_flg.append( checkres.getresult()[i][0] )
            finish_flg.append( checkres.getresult()[i][1] )
            icechart_id.append( checkres.getresult()[i][2] )
            regions.append( (checkres.getresult()[i][3]).strip() )
    else:
        if current == 0:
            print "02_livedata: No production database entry for this computer on this date. Stopping."
        # else:
        #     print ("02_livedata: Too many (%d) database entries for this date. Stopping." \
        #         % current)
        sys.exit()

    # Create a lock file
    tmpt=time.gmtime()
    tout=( '*** 02_livedata.py - ' + time.asctime(tmpt) + ' ***\n' )
    lockfn= ("%s/02_livedata.lock" % ANCDIR)
    try:
        fin = open(lockfn, 'r')
        fin.close()
        raise NameError, 'Lock Exists!'
    except IOError:
        # No lock file exists so create one
        fout = open(lockfn, 'w')
        fout.write(tout)
        fout.close()
        pass
    except NameError:
        # If we get to here then there is a lock file in existance so we should exit
        print 'Lock exists!'
        sys.exit()

    # Get list of files currently in the LiveData directory
    livelist = os.listdir( LIVEDIR )
    # print livelist

    # If hour is not 23, copy files from server.
    # Otherwise, do a late night deletion
    # print hour
    if hour != 23:

        # Loop through regions
        for region in regions:

            # Check availability and copy Sentinel-1 files if required
            get_S1_files( satdbcon, year, month, day, hour, minute, region, livelist )

            # Check availability and copy RS2 files if required
            # get_rs2_files( satdbcon, year, month, day, region, livelist )

            # Check availability and copy COSMO SkyMed files if required
            # get_csks_files( satdbcon, year, month, day, region, livelist )

            # Check availability and copy AMSR2 files if required
            get_amsr2_files( year, month, day, region, livelist )

            # Check availability and copy SMOS files if required
            get_smos_files( year, month, day, region, livelist )

            # Check availability and copy ASCAT files if required
            get_ascat_files( year, month, day, region, livelist )

            # Check availability and copy MODIS files if required
            get_modis_files( satdbcon, year, month, day, hour, minute, region, livelist )

            # Check availability and copy NOAA files if required (Arctic only)
            # if region == 'arctic':
            #     get_noaa_files( year, month, day, livelist )

    else:
        # Clear the LiveData directory
        for fname in livelist:
            os.remove( ("%s/%s" % (LIVEDIR,fname)) )

    # Remove the lock file
    os.remove(lockfn)

