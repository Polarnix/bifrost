#!/usr/bin/python

# Name:          get_ip_address.py
# Purpose:       Get computer IP address as a string.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import netifaces as ni

def get_ip_address():
    interfaces = ni.interfaces()
    ipstr = 'void'
    for interface in interfaces:
        ni.ifaddresses( interface )
        tmpipstr = str( ni.ifaddresses( interface )[2][0]['addr'] )
        if tmpipstr.find('10.168.33.') or tmpipstr.find('192.168.1.'):
            ipstr = tmpipstr
    return ipstr

if __name__ == '__main__':
    ipstr = get_ip_address()
    print ipstr
