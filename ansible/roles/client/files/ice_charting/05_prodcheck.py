#!/usr/bin/python

# Name:          05_prodcheck.py
# Purpose:       Routine to check automated ice chart production status.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, platform
from datetime import datetime
import time
import argparse

import pg

from get_bifrost_credentials import get_bifrost_credentials
from get_ip_address import get_ip_address

# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)


# Date reformatting
def mkdate(datestr):
    return time.strptime(datestr,'%Y%m%d')


# Main function
if __name__ == "__main__":

    # Get today's date
    today = time.localtime()
    todaystr = time.strftime('%Y-%m-%d',today)
    # print todaystr

    # Get input parameters using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument( "region", help="Region for ice chart [arctic/antarctic]", type=str, \
        choices=['arctic','antarctic'])
    parser.add_argument("-d","--date",help="Date for ice chart [yyyymmdd]",type=mkdate,default=today)
    parser.add_argument("-t","--time",help="Valid hour for ice chart [hh]",type=int,default=-1, \
        choices=xrange(0,24))
    args = parser.parse_args()
    regionstr = args.region
    year = (args.date).tm_year
    month = (args.date).tm_mon
    day = (args.date).tm_mday
    if args.time == -1:
       hour = 15
    else:
        hour = args.time
    datestr = ("%4d%02d%02d" % (year,month,day))
    dtstamp = datetime( year, month, day, 15, 0, 0 )
    dow = dtstamp.isoweekday()
    if args.time == -1:
        autoflg = True
    else:
        autoflg = False

    # Set computer name
    hoststr = platform.node()

    # Get computer IP address
    ipstr = get_ip_address()

    # Get Bifrost server credentials from file
    bifrostfn = ("%s/bifrost_credentials.txt" % SETDIR)
    credentials = get_bifrost_credentials( bifrostfn )   

    # Day-of-week and month strings
    dow_str = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' , 'Sunday' ]
    mth_str = [ 'January', 'February', 'March', 'April', 'May', 'June', \
        'July', 'August', 'September', 'October', 'November', 'December' ]

    # Set up database connection
    dbcon = pg.connect(dbname='Icecharts', host=credentials['BIFROST_SERVER_IP'], user='bifrostadmin', passwd='bifrostadmin')

    # Check current status of database
    # checkres = dbcon.query(("SELECT new_flg,finish_flg,metline_flg,id FROM production WHERE icechart_date = \'%4d-%02d-%02d\'" % (year,month,day)))
    sqltxt = "SELECT new_flg,finish_flg,metline_flg,id FROM production WHERE"
    sqltxt = ("%s icechart_date = \'%s\' AND valid_hour = %d AND computer = \'%s\' AND region = \'%s\'" \
        % (sqltxt,datestr,hour,ipstr,regionstr))
    checkres = dbcon.query(sqltxt)
    current = checkres.ntuples()
    if current == 1:
        new_flg = checkres.getresult()[0][0]
        finish_flg = checkres.getresult()[0][1]
        metline_flg = checkres.getresult()[0][2]
        icechart_id = checkres.getresult()[0][3]
    else:
        if current == 0:
            print "05_prodcheck: No production database entry for this date. Stopping."
        else:
            print ("05_prodcheck: Too many (%d) database entries for this date. Stopping." \
                % current)
        sys.exit()
    dtstr = dtstamp.strftime('%Y-%m-%d %H:%M:%S')

    # Define production database parameters to list
    if regionstr == 'arctic':
        if dow <= 5:
            prodpar = [ [ 'new_flg',         'new_dt',         'Ice chart setup' ], \
                        [ 'valid_flg',       'valid_dt',       'Ice chart validated' ], \
                        [ 'finish_flg',      'finish_dt',      'Ice chart finished' ], \
                        [ 'metline_flg',     'metline_dt',     'METAREA line finished' ], \
                        [ 'outputs_flg',     'outputs_dt',     'Output PNG, JPG and PDF files generated' ], \
                        [ 'telexpts_flg',    'telexpts_dt',    'Telex point report generated' ], \
                        [ 'archive_flg',     'archive_dt',     'Ice chart Shapefile and PNG files archived' ], \
                        [ 'print_flg',       'print_dt',       'Printed' ], \
                        [ 'disseminate_flg', 'disseminate_dt', 'Distributed by e-mail' ], \
                        [ 'metdiss_flg',     'metdiss_dt',     'METAREA line distributed' ], \
                        [ 'pvupload_flg',    'pvupload_dt',    'Uploaded to Polar View server' ], \
                        [ 'istupload_flg',   'istupload_dt',   'Uploaded to iceservice.met.no' ], \
                        [ 'ftpupload_flg',   'ftpupload_dt',   'Uploaded to FTP server' ], \
                        [ 'gridded_flg',     'gridded_dt',     'MyOcean grid file generated' ], \
                        [ 'gridupload_flg',  'gridupload_dt',  'MyOcean grid file uploaded to Oslo' ], \
                        [ 'safvalid_flg',    'safvalid_dt',    'SAF validation grid file generated' ], \
                        [ 'safupload_flg',   'safupload_dt',   'SAF validation grid file uploaded to Oslo' ], \
                        [ 'routine_flg',     'routine_dt',     'Ice chart files uploaded to Oslo Routine' ], \
                        [ 'twitter_flg',     'twitter_dt',     'Generated Twitter message' ] ]
            #           [ 'sigrid_flg',      'sigrid_dt',      'SIGRID3.3 file generated' ], \
            #           [ 'cacheclear_flg',  'cacheclear_dt',  'Cleared from production machine cache' ] ]
        else:
            prodpar = [ [ 'new_flg',         'new_dt',         'Ice chart setup' ], \
                        [ 'valid_flg',       'valid_dt',       'Ice chart validated' ], \
                        [ 'finish_flg',      'finish_dt',      'Ice chart finished' ], \
                        [ 'outputs_flg',     'outputs_dt',     'Output PNG, JPG and PDF files generated' ], \
                        [ 'print_flg',       'print_dt',       'Printed' ], \
                        [ 'disseminate_flg', 'disseminate_dt', 'Distributed by e-mail' ], \
                        [ 'pvupload_flg',    'pvupload_dt',    'Uploaded to Polar View server' ], \
                        [ 'ftpupload_flg',   'ftpupload_dt',   'Uploaded to FTP server' ] ]
            #           [ 'istupload_flg',   'istupload_dt',   'Uploaded to iceservice.met.no' ], \
            #           [ 'cacheclear_flg',  'cacheclear_dt',  'Cleared from production machine cache' ] ]
    elif regionstr == 'antarctic':
        prodpar = [ [ 'new_flg',         'new_dt',         'Ice chart setup' ], \
                    [ 'valid_flg',       'valid_dt',       'Ice chart validated' ], \
                    [ 'finish_flg',      'finish_dt',      'Ice chart finished' ], \
                    [ 'outputs_flg',     'outputs_dt',     'Output PNG, JPG and PDF files generated' ], \
                    [ 'archive_flg',     'archive_dt',     'Ice chart Shapefile and PNG files archived' ], \
                    [ 'print_flg',       'print_dt',       'Printed' ], \
                    [ 'disseminate_flg', 'disseminate_dt', 'Distributed by e-mail' ], \
                    [ 'pvupload_flg',    'pvupload_dt',    'Uploaded to Polar View server' ], \
                    [ 'istupload_flg',   'istupload_dt',   'Uploaded to iceservice.met.no' ], \
                    [ 'ftpupload_flg',   'ftpupload_dt',   'Uploaded to FTP server' ] ]
        #           [ 'sigrid_flg',      'sigrid_dt',      'SIGRID3.3 file generated' ], \
        #           [ 'cacheclear_flg',  'cacheclear_dt',  'Cleared from production machine cache' ] ]
    # print prodpar

    # Generate SQL query string for database
    firstflg = 1
    for parameter in prodpar:
        if firstflg == 1:
            sqltxt = ("SELECT %s,%s" % (parameter[0],parameter[1]))
            firstflg = 0
        else:
            sqltxt = ("%s,%s,%s" % (sqltxt,parameter[0],parameter[1]))
    # sqltxt = ("%s FROM production WHERE icechart_date = \'%4d-%02d-%02d\';" % (sqltxt,year,month,day))
    sqltxt = ("%s FROM production WHERE icechart_date = \'%s\' AND valid_hour = %d AND computer = \'%s\' AND region = \'%s\'" \
        % (sqltxt,datestr,hour,ipstr,regionstr))
    # print sqltxt

    # Query database
    queryres = dbcon.query(sqltxt)
    # print queryres
    prodres = queryres.getresult()[0]
    current = checkres.ntuples()
    if current == 1:

        # Output status
        if day % 10 == 1:
            suffix = 'st'
        elif day % 10 == 2:
            suffix = 'nd'
        elif day % 10 == 3:
            suffix = 'rd'
        else:
            suffix = 'th'
        print ("Ice chart status for %s %d%s %s %d -\n" % (dow_str[dow-1],day,suffix,mth_str[month-1],year))
        print "Production Item:                           Date/Time:          Status:"
        #      123456789012345678901234567890123456789012 1234567890123456789 1234567
        for i in range(len(prodpar)):
            parameter = prodpar[i]
            idx = i * 2
            resval = prodres[idx]
            if resval != 0:
                resdt = datetime.strptime( prodres[idx+1][0:19], '%Y-%m-%d %H:%M:%S' )
                resdtstr = resdt.strftime('%Y-%m-%d %H:%M:%S')
            else:
                resdtstr = ' '
            if parameter[0].find('new_flg') > -1:
                if resval == 1:
                    resstr = 'With blank extent.'
                else:
                    resstr = 'Using yesterdays polygons.'
                print ("%-42s %19s %s" % (parameter[2],resdtstr, resstr))
            elif parameter[0].find('finish_flg') > -1:
                resstr = ("%d polygon(s)." % resval)
                print ("%-42s %19s %s" % (parameter[2],resdtstr, resstr))
            elif parameter[0].find('metline_flg') > -1:
                resstr = ("%d point(s)." % resval)
                print ("%-42s %19s %s" % (parameter[2],resdtstr, resstr))
            else:
                if resval == 0:
                    resstr = '*** No'
                else:
                    resstr = 'Yes'
                print ("%-42s %19s %-7s" % (parameter[2],resdtstr, resstr))
    print ' '

    # Close database connection
    dbcon.close()

