#!/usr/bin/python

# Name:          01_startup.py
# Purpose:       Python script to generate a new ice chart set of files on the correct day.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-13  - Correct to allow server IP address to be sent to routines. 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, platform, shutil
from datetime import date, datetime, timedelta
import time
import argparse
import ast

import pg

import osgeo.ogr as ogr
import osgeo.osr as osr

from copy_ice import copy_ice, create_extent_poly
from get_bifrost_credentials import get_bifrost_credentials
from get_ip_address import get_ip_address
from get_map_projections import get_map_projections
from check_coastline import check_coastline

# Setup production system directories
BASEDIR = "/home/bifrostanalyst"
INCDIR = ("%s/Include/Bifrost" % BASEDIR)
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)
ICEDIR = ("%s/ICS" % BASEDIR)
QGISDIR = ("%s/QGIS" % BASEDIR)


# Create new ice polygon Shapefile
def new_ice( proddir, datestr, valid_hour, PROJSTR, EXTENT, regcode ):
    # Define Shapefile as the format using OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Spatial reference system for shapefiles
    ice_srs = osr.SpatialReference()
    ice_srs.ImportFromProj4( PROJSTR )

    # Create an ice chart extent polygon
    ext_poly = create_extent_poly(EXTENT)

    # Create new polygons shapefile
    polyfn = ("%s/ice_%s_%02d_polygons_%s.shp" % (proddir,datestr,valid_hour,regcode))
    if os.path.exists( polyfn ):
        driver.DeleteDataSource( polyfn )
    poly_ds = driver.CreateDataSource( polyfn )
    layer_name = ("%s_%02d" % (datestr,valid_hour))
    poly_lay = poly_ds.CreateLayer( layer_name, srs=ice_srs, \
        geom_type=ogr.wkbPolygon )
    concfld = ogr.FieldDefn( 'iceconc', ogr.OFTInteger )    # SIGRID 3.3 CT
    poly_lay.CreateField( concfld )
    conc1fld = ogr.FieldDefn( 'iceconc1', ogr.OFTInteger )  # SIGRID 3.3 CA
    poly_lay.CreateField( conc1fld )
    type1fld = ogr.FieldDefn( 'icetype1', ogr.OFTInteger )  # SIGRID 3.3 SA
    poly_lay.CreateField( type1fld )
    conc2fld = ogr.FieldDefn( 'iceconc2', ogr.OFTInteger )  # SIGRID 3.3 CB
    poly_lay.CreateField( conc2fld )
    type2fld = ogr.FieldDefn( 'icetype2', ogr.OFTInteger )  # SIGRID 3.3 SB
    poly_lay.CreateField( type2fld )
    featdefn = poly_lay.GetLayerDefn()
    # Create an ice chart extent polygon
    ext_feat = ogr.Feature( featdefn )
    ext_feat.SetField( 'iceconc', 0 )
    ext_feat.SetField( 'iceconc1', 0 )
    ext_feat.SetField( 'icetype1', 0 )
    ext_feat.SetField( 'iceconc2', 0 )
    ext_feat.SetField( 'icetype2', 0 )
    ext_feat.SetGeometry( ext_poly )
    poly_lay.CreateFeature( ext_feat )
    ext_feat.Destroy()
    poly_ds.Destroy()

    return polyfn


# Create new ice chart function
def create_new( proddir, datestr, valid_hour, PROJSTR, EXTENT, regcode, server_str ):
    # Define Shapefile as the format using OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Spatial reference system for shapefiles
    ice_srs = osr.SpatialReference()
    ice_srs.ImportFromProj4( PROJSTR )

    # Create new polygons shapefile
    polyfn = new_ice( proddir, datestr, valid_hour, PROJSTR, EXTENT, regcode )

    # Create new ice shelf shapefile (Antarctic only)
    if regcode == 'ANT':
        shelffn = ("%s/ice_%s_%02d_iceshelf_%s.shp" % (proddir,datestr,valid_hour,regcode))
        if os.path.exists( shelffn ):
            driver.DeleteDataSource( shelffn )
        shelf_ds = driver.CreateDataSource( shelffn )
        layer_name = ("%s_%02d_iceshelf" % (datestr,valid_hour))
        shelf_lay = shelf_ds.CreateLayer( layer_name, srs=ice_srs, \
            geom_type=ogr.wkbPolygon )
        srcfld = ogr.FieldDefn( 'satellite', ogr.OFTString )
        srcfld.SetWidth( 16 )
        shelf_lay.CreateField( srcfld )
        startdfld = ogr.FieldDefn( 'datestr', ogr.OFTInteger )
        shelf_lay.CreateField( startdfld )
        enddfld = ogr.FieldDefn( 'dateend', ogr.OFTInteger )
        shelf_lay.CreateField( enddfld )
        shelf_ds.Destroy()

    # Create new iceberg polygon shapefile (Antarctic only)
    if regcode == 'ANT':
        bergfn = ("%s/ice_%s_%02d_icebergs_%s.shp" % (proddir,datestr,valid_hour,regcode))
        if os.path.exists( bergfn ):
            driver.DeleteDataSource( bergfn )
        berg_ds = driver.CreateDataSource( bergfn )
        layer_name = ("%s_%02d_icebergs" % (datestr,valid_hour))
        berg_lay = shelf_ds.CreateLayer( layer_name, srs=ice_srs, \
            geom_type=ogr.wkbPolygon )
        namefld = ogr.FieldDefn( 'name', ogr.OFTString )
        namefld.SetWidth( 16 )
        berg_lay.CreateField( namefld )
        srcfld = ogr.FieldDefn( 'satellite', ogr.OFTString )
        srcfld.SetWidth( 16 )
        berg_lay.CreateField( srcfld )
        datefld = ogr.FieldDefn( 'date', ogr.OFTInteger )
        berg_lay.CreateField( datefld )
        timefld = ogr.FieldDefn( 'time', ogr.OFTInteger )
        timefld.SetWidth( 6 )
        berg_lay.CreateField( timefld )
        berg_ds.Destroy()

    # Create new symbols shapefile
    symbfn = ("%s/ice_%s_%02d_symbols_%s.shp" % (proddir,datestr,valid_hour,regcode))
    if os.path.exists( symbfn ):
        driver.DeleteDataSource( symbfn )
    symb_ds = driver.CreateDataSource( symbfn )
    layer_name = ("%s_%02d_symbols" % (datestr,valid_hour))
    symb_lay = symb_ds.CreateLayer( layer_name, srs=ice_srs, \
        geom_type=ogr.wkbPoint )
    typefld = ogr.FieldDefn( 'type', ogr.OFTInteger )
    symb_lay.CreateField( typefld )
    rotfld = ogr.FieldDefn( 'rotation', ogr.OFTInteger )
    symb_lay.CreateField( rotfld )
    valfld = ogr.FieldDefn( 'value', ogr.OFTString )
    valfld.SetWidth( 16 )
    symb_lay.CreateField( valfld )
    symb_ds.Destroy()

    # Create new METAREA line shapefile
    linefn = ("%s/ice_%s_%02d_metarea_line_%s.shp" % (proddir,datestr,valid_hour,regcode))
    if os.path.exists( linefn ):
        driver.DeleteDataSource( linefn )
    line_ds = driver.CreateDataSource( linefn )
    layer_name = ("%s_%02d_metarea_line" % (datestr,valid_hour))
    line_lay = line_ds.CreateLayer( layer_name, srs=ice_srs, \
        geom_type=ogr.wkbLineString )
    idfld = ogr.FieldDefn( 'id', ogr.OFTInteger )
    line_lay.CreateField( idfld )
    line_ds.Destroy()

    # Create new QGIS project
    templfn = ("%s/Project_Templates/template_%s.qgs" % (QGISDIR,regcode))
    qgisfn = ("%s/ice_chart_%s_%02d_%s.qgs" % (proddir,datestr,valid_hour,regcode))
    template = open( templfn, 'r' )
    qgisfile = open( qgisfn, 'w' )
    for tline in template:
        if tline.find('YYYYMMDD') > -1:
            tline = tline.replace('YYYYMMDD',datestr)
        if tline.find('VALIDHR') > -1:
            tline = tline.replace('VALIDHR',("%02d" % valid_hour))
        if tline.find('SERVER-IPADDR') > -1:
            tline = tline.replace('SERVER-IPADDR',("%s" % server_str))
            # print tline
        qgisfile.write( tline )
    template.close()
    qgisfile.close()


# Copy previous ice chart function
def copy_previous( proddir, datestr, valid_hour, previous_str, previous_hour, PROJSTR, regcode, server_str ):
    # Define Shapefile as the format using OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Spatial reference system for shapefiles
    ice_srs = osr.SpatialReference()
    ice_srs.ImportFromProj4( PROJSTR )

    # Unpack date strings
    dateyr = int(datestr[0:4])
    datemh = int(datestr[4:6])
    datedy = int(datestr[6:8])
    output_dt = datetime( dateyr, datemh, datedy, valid_hour, 0, 0 )
    prevyr = int(previous_str[0:4])
    prevmh = int(previous_str[4:6])
    prevdy = int(previous_str[6:8])
    input_dt = datetime( prevyr, prevmh, prevdy, previous_hour, 0, 0 )

    # Copy previous ice charts polygons
    polyfn = copy_ice( proddir, regcode, input_dt, output_dt, valid_hour )

    # Copy previous ice shelf shapefile
    if regcode == 'ANT':
        shelffn = ("%s/ice_%s_%02d_iceshelf_%s.shp" % (proddir,datestr,valid_hour,regcode))
        if os.path.exists( shelffn ):
            driver.DeleteDataSource( shelffn )
        shelf_ds = driver.CreateDataSource( shelffn )
        layer_name = ("%s_%02d_iceshelf" % (datestr,valid_hour))
        shelf_lay = shelf_ds.CreateLayer( layer_name, srs=ice_srs, \
            geom_type=ogr.wkbPolygon )
        srcfld = ogr.FieldDefn( 'satellite', ogr.OFTString )
        srcfld.SetWidth( 16 )
        shelf_lay.CreateField( srcfld )
        startdfld = ogr.FieldDefn( 'datestr', ogr.OFTInteger )
        shelf_lay.CreateField( startdfld )
        enddfld = ogr.FieldDefn( 'dateend', ogr.OFTInteger )
        shelf_lay.CreateField( enddfld )
        featdefn = shelf_lay.GetLayerDefn()
        # Open previous file
        inpfn = ("%s/%s_%02d_%s/ice_%s_%02d_iceshelf_%s.shp" 
            % (ICEDIR,previous_str,previous_hour,regcode,previous_str,previous_hour,regcode))
        # print inpfn
        # Ensure all features are properly deleted from Shapefile
        in_ds = driver.Open( inpfn, 1 )
        in_lay = in_ds.GetLayer(0)
        in_ds.ExecuteSQL('REPACK ' + in_lay.GetName())
        in_ds.Destroy()
        # Now open file for reading  
        in_ds = driver.Open( inpfn, 0 )
        in_lay = in_ds.GetLayer(0)
        nfeat = in_lay.GetFeatureCount()
        # Copy ice chart symbols
        for i in range(nfeat):
            # Read input feature
            in_feat = in_lay.GetFeature(i)
            shelfsat = in_feat.GetField( 'satellite' )
            shelfstr = in_feat.GetField( 'datestr' )
            shelfend = in_feat.GetField( 'dateend' )
            icegeom = in_feat.GetGeometryRef()
            # Create output feature
            ext_feat = ogr.Feature( featdefn )
            ext_feat.SetField( 'satellite', shelfsat )
            ext_feat.SetField( 'datestr', shelfstr )
            ext_feat.SetField( 'dateend', shelfend )
            ext_feat.SetGeometry( icegeom )
            shelf_lay.CreateFeature( ext_feat )
            # Destroy features
            in_feat.Destroy()
            ext_feat.Destroy()
        # Close files
        in_ds.Destroy()
        shelf_ds.Destroy()

    # Copy previous icebergs shapefile
    if regcode == 'ANT':
        bergfn = ("%s/ice_%s_%02d_icebergs_%s.shp" % (proddir,datestr,valid_hour,regcode))
        if os.path.exists( bergfn ):
            driver.DeleteDataSource( bergfn )
        berg_ds = driver.CreateDataSource( bergfn )
        layer_name = ("%s_%02d_icebergs" % (datestr,valid_hour))
        berg_lay = berg_ds.CreateLayer( layer_name, srs=ice_srs, \
            geom_type=ogr.wkbPolygon )
        namefld = ogr.FieldDefn( 'name', ogr.OFTString )
        namefld.SetWidth( 16 )
        berg_lay.CreateField( namefld )
        srcfld = ogr.FieldDefn( 'satellite', ogr.OFTString )
        srcfld.SetWidth( 16 )
        berg_lay.CreateField( srcfld )
        datefld = ogr.FieldDefn( 'date', ogr.OFTInteger )
        berg_lay.CreateField( datefld )
        timefld = ogr.FieldDefn( 'time', ogr.OFTInteger )
        timefld.SetWidth( 6 )
        berg_lay.CreateField( timefld )
        featdefn = berg_lay.GetLayerDefn()
        # Open previous file
        inpfn = ("%s/%s_%02d_%s/ice_%s_%02d_icebergs_%s.shp" \
            % (ICEDIR,previous_str,previous_hour,regcode,previous_str,previous_hour,regcode))
        # Ensure all features are properly deleted from Shapefile
        in_ds = driver.Open( inpfn, 1 )
        in_lay = in_ds.GetLayer(0)
        in_ds.ExecuteSQL('REPACK ' + in_lay.GetName())
        in_ds.Destroy()
        # Now open file for reading  
        in_ds = driver.Open( inpfn, 0 )
        in_lay = in_ds.GetLayer(0)
        nfeat = in_lay.GetFeatureCount()
        # Copy ice chart symbols
        for i in range(nfeat):
            # Read input feature
            in_feat = in_lay.GetFeature(i)
            bergname = in_feat.GetField( 'name' )
            bergsat = in_feat.GetField( 'satellite' )
            bergdate = in_feat.GetField( 'date' )
            bergtime = in_feat.GetField( 'time' )
            icegeom = in_feat.GetGeometryRef()
            # Create output feature
            ext_feat = ogr.Feature( featdefn )
            ext_feat.SetField( 'name', bergname )
            ext_feat.SetField( 'satellite', bergsat )
            ext_feat.SetField( 'date', bergdate )
            ext_feat.SetField( 'time', bergtime )
            ext_feat.SetGeometry( icegeom )
            berg_lay.CreateFeature( ext_feat )
            # Destroy features
            in_feat.Destroy()
            ext_feat.Destroy()
        # Close files
        in_ds.Destroy()
        berg_ds.Destroy()

    # Copy previous symbols shapefile
    symbfn = ("%s/ice_%s_%02d_symbols_%s.shp" % (proddir,datestr,valid_hour,regcode))
    if os.path.exists( symbfn ):
        driver.DeleteDataSource( symbfn )
    symb_ds = driver.CreateDataSource( symbfn )
    layer_name = ("%s_%02d_symbols" % (datestr,valid_hour))
    symb_lay = symb_ds.CreateLayer( layer_name, srs=ice_srs, \
        geom_type=ogr.wkbPoint )
    typefld = ogr.FieldDefn( 'type', ogr.OFTInteger )
    symb_lay.CreateField( typefld )
    rotfld = ogr.FieldDefn( 'rotation', ogr.OFTInteger )
    symb_lay.CreateField( rotfld )
    valfld = ogr.FieldDefn( 'value', ogr.OFTString )
    valfld.SetWidth( 16 )
    symb_lay.CreateField( valfld )
    featdefn = symb_lay.GetLayerDefn()
    # Open yesterday's file
    inpfn = ("%s/%s_%02d_%s/ice_%s_%02d_symbols_%s.shp" \
        % (ICEDIR,previous_str,previous_hour,regcode,previous_str,previous_hour,regcode))
    # print inpfn
    # Ensure all features are properly deleted from Shapefile
    in_ds = driver.Open( inpfn, 1 )
    in_lay = in_ds.GetLayer(0)
    in_ds.ExecuteSQL('REPACK ' + in_lay.GetName())
    in_ds.Destroy()
    # Now open file for reading  
    in_ds = driver.Open( inpfn, 0 )
    in_lay = in_ds.GetLayer(0)
    nfeat = in_lay.GetFeatureCount()
    # Copy ice chart symbols
    for i in range(nfeat):
        # Read input feature
        in_feat = in_lay.GetFeature(i)
        symbtype = in_feat.GetField( 'type' )
        symbrot = in_feat.GetField( 'rotation' )
        symbval = in_feat.GetField( 'value' )
        icegeom = in_feat.GetGeometryRef()
        # Create output feature
        ext_feat = ogr.Feature( featdefn )
        ext_feat.SetField( 'type', symbtype )
        ext_feat.SetField( 'rotation', symbrot )
        ext_feat.SetField( 'value', symbval )
        ext_feat.SetGeometry( icegeom )
        symb_lay.CreateFeature( ext_feat )
        # Destroy features
        in_feat.Destroy()
        ext_feat.Destroy()
    # Close files
    in_ds.Destroy()
    symb_ds.Destroy()

    # Copy previous METAREA line shapefile
    linefn = ("%s/ice_%s_%02_metarea_line_%s.shp" % (proddir,datestr,valid_hour,regcode))
    if os.path.exists( linefn ):
        driver.DeleteDataSource( linefn )
    line_ds = driver.CreateDataSource( linefn )
    layer_name = ("%s_%02d_metarea_line" % (datestr,valid_hour))
    line_lay = line_ds.CreateLayer( layer_name, srs=ice_srs, \
        geom_type=ogr.wkbLineString )
    idfld = ogr.FieldDefn( 'id', ogr.OFTInteger )
    line_lay.CreateField( idfld )
    featdefn = line_lay.GetLayerDefn()
    # Open yesterday's file
    inpfn = ("%s/%s/ice_%s_%02d_metarea_line_%s.shp" % (ICEDIR,previous_str,previous_str,regcode))
    # print inpfn
    in_ds = driver.Open( inpfn, 0 )
    in_lay = in_ds.GetLayer(0)
    nfeat = in_lay.GetFeatureCount()
    # Copy ice chart symbols
    for i in range(nfeat):
        # Read input feature
        in_feat = in_lay.GetFeature(i)
        line_id = in_feat.GetField( 'id' )
        icegeom = in_feat.GetGeometryRef()
        # Create output feature
        ext_feat = ogr.Feature( featdefn )
        ext_feat.SetField( 'id', line_id )
        ext_feat.SetGeometry( icegeom )
        line_lay.CreateFeature( ext_feat )
        # Destroy features
        in_feat.Destroy()
        ext_feat.Destroy()
    # Close files
    in_ds.Destroy()
    line_ds.Destroy()

    # Create new QGIS project
    # NB: This ensures all layer styles are reset
    templfn = ("%s/template_%s.qgs" % (QGISDIR,regcode))
    qgisfn = ("%s/ice_chart_%s_02d_%s.qgs" % (proddir,datestr,valid_hour,regcode))
    template = open( templfn, 'r' )
    qgisfile = open( qgisfn, 'w' )
    for tline in template:
        if tline.find('YYYYMMDD') > -1:
            tline = tline.replace('YYYYMMDD',datestr)
        if tline.find('VALIDHR') > -1:
            tline = tline.replace('VALIDHR',("%02d" % valid_hour))
        if tline.find('SERVER-IPADDR') > -1:
            tline = tline.replace('SERVER-IPADDR',("%s" % server_str))
            # print tline
        qgisfile.write( tline )
    template.close()
    qgisfile.close()


# Check database for previous ice charts
def check_previous( dbcon, regionstr, dtstamp, ndays ):
    previous = dtstamp - timedelta(days=ndays)
    previous_str = previous.strftime('%Y%m%d')
    checkres = dbcon.query(("SELECT finish_flg,valid_hour FROM production WHERE icechart_date = \'%s\' AND region = \'%s\'" \
        % (previous_str,regionstr)))
    previous = checkres.ntuples()
    if previous == 0:
        previous_flg = 0
        previous_hour = 15
    else:
        previous_flg = checkres.getresult()[0][0]
        previous_hour = int( checkres.getresult()[0][1] )
    return [ previous_flg, previous_str, previous_hour ]


# Date reformatting
def mkdate(datestr):
    return time.strptime(datestr,'%Y%m%d')


# Main function
if __name__ == "__main__":

    # Get today's date
    today = time.localtime()
    todaystr = time.strftime('%Y-%m-%d',today)
    # print todaystr

    # Get input parameters using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument( "region", help="Region for ice chart [arctic/antarctic]", type=str.lower, \
        choices=['arctic','antarctic'])
    parser.add_argument( "chart_type", help="Chart type[Production/Training/Collaborative]", type=str.lower, \
        choices=['p', 't', 'c'])
    parser.add_argument("-d","--date",help="Date for ice chart [yyyymmdd]",type=mkdate,default=today)
    parser.add_argument("-t","--time",help="Valid hour for ice chart [hh]",type=int,default=-1, \
        choices=xrange(0,24))
    args = parser.parse_args()
    regionstr = args.region
    chart_type = (args.chart_type).upper()
    year = (args.date).tm_year
    month = (args.date).tm_mon
    day = (args.date).tm_mday
    if args.time == -1:
       hour = 15
    else:
        hour = args.time
    datestr = ("%4d%02d%02d" % (year,month,day))
    dtstamp = datetime( year, month, day, hour, 0, 0 )
    dow = dtstamp.isoweekday()
    if args.time == -1:
        autoflg = True
    else:
        autoflg = False
    # print args.time, autoflg

    # Get computer name
    hoststr = platform.node()

    # Get computer IP address
    ipstr = get_ip_address()

    # Open exclusion list (Public Holidays) file
    excfn = ("%s/exclusion_list.txt" % INCDIR)
    excf = open( excfn, 'r' )
    
    # Check for reasons not to continue
    if autoflg == True:
        if dow > 5:  
            # 1. Automatic running on a weekend
            print "01_startup: Trying to run automatically on a weekend. Stopping."
            sys.exit()
        else:
            # 2. Automatic running on a weekday, check exclusion list (Public Holidays) list
            excflg = False
            for excdate in excf:
                # print excdate, len(excdate)
                if excdate.strip() == datestr:
                    print"01_startup: Trying to run automatically on a public holiday. Stopping."
                    sys.exit()
    else:
        if dow > 5:
            # 3. Manual running on a weekend - just warn but do not stop
            print "01_startup: You are running an ice chart on a weekend? Continuing."
        else:
            # 4. Manual running on a weekday, check exclusion list (Public Holidays) list
            #    Just warn, but do not stop
            excflg = False
            for excdate in excf:
                # print excdate, len(excdate)
                if excdate.strip() == datestr:
                    print"01_startup: You are running an ice chart on a public holiday? Continuing."
                    excflg = True
    
    # Close exclusion list file
    excf.close()

    # Define a region code
    if regionstr == 'arctic':
       regcode = 'ARC'
    elif regionstr == 'antarctic':
       regcode = 'ANT'

    # Manually set values for debugging
    # dtstamp = datetime( 2014, 5, 13, 15, 0 ,0 )
    # datestr = ("%4d%02d%02d" % (dtstamp.year,dtstamp.month,dtstamp.day))
    # print dtstamp, dow
    
    # Get Bifrost server credentials from file
    bifrostfn = ("%s/bifrost_credentials.txt" % SETDIR)
    credentials = get_bifrost_credentials( bifrostfn )   
    
    # Connect to ice charts database
    dbcon = pg.connect(dbname='Icecharts', host=credentials['BIFROST_SERVER_IP'], user='bifrostadmin', passwd='bifrostadmin')
    
    # See if there is an ice chart for:
    #   - yesterday
    [ previous_flg, previous_str, previous_hour ] = check_previous( dbcon, regionstr, dtstamp, 1 )

    # Target ice chart analyst files directory
    proddir = ("%s/%s_%02d_%s" % (ICEDIR,datestr,hour,regcode))

    # See if there is already a database entry for a production ice chart today. If so, it implies we are overwriting
    # files, or that we have training/collaboration
    sql = "SELECT new_flg,id,charttype,computer FROM production WHERE"
    sql = ("%s icechart_date = \'%s\' AND valid_hour = %d AND region = \'%s\'" \
        % (sql,datestr,hour,regionstr))
    checkres = dbcon.query(sql)
    current = checkres.ntuples()

    # If no results, then this run sets the initial database entry.
    overwrite_flg = 0

    # Checks to see exactly what ice chart is already being done if we have results from the query
    if current > 0:

        if chart_type == 'P':
            # Look for P=Production entries
            for i in range(current):
                dbrec = checkres.getresult()[i]
                dbflg = int(dbrec[0])
                dbid = int(dbrec[1])
                dbtype = dbrec[2]
                dbcomp = dbrec[3]
                if dbtype.find('P') > -1:
                    if dbcomp.find(ipstr) == -1:
                        print ("01_startup: Production ice chart already being generated on %s. Stopping." % dbcomp)
                        sys.exit()
                    else:
                        overwrite_flg = dbflg
                        overwrite_id = dbid

        elif chart_type == 'T':
            # Look for T=Training entries
            for i in range(current):
                dbrec = checkres.getresult()[i]
                dbflg = int(dbrec[0])
                dbid = int(dbrec[1])
                dbtype = dbrec[2]
                dbcomp = dbrec[3]
                if dbtype.find('T') > -1 and dbcomp.find(ipstr) > -1:
                    overwrite_flg = dbflg
                    overwrite_id = dbid

        elif chart_type == 'C':
            # Look for C=Collaborative entries
            for i in range(current):
                dbrec = checkres.getresult()[i]
                dbflg = int(dbrec[0])
                dbid = int(dbrec[1])
                dbtype = dbrec[2]
                dbcomp = dbrec[3]
                if dbtype.find('P') > -1:
                    print ("01_startup: Production ice chart already being generated on \'%s\', so unable to start a collaborative chart. Stopping." % dbcomp)
                    sys.exit()
                elif dbtype.find('C') > -1:
                    if dbcomp.find(ipstr) > -1:
                        overwrite_flg = dbflg
                        overwrite_id = dbid
                    else:
                        print ("01_startup: Warning, collaborative chart already in progress on \'%s\'." % dbcomp)

        else:
            # If we get here,something is wrong with our user input
            print ("01_startup: Unknown chart type \'%s\'. Stopping." % chart_type)
            sys.exit()

    # print 'overwrite_flg', overwrite_flg
        
    # Handle positive overwrite flags
    if overwrite_flg > 0:
        if autoflg == True:
            print "01_startup: Trying to automatically overwrite existing files. This can only be done manually. Stopping."
            sys.exit()
        else:
            # Get user confirmation that they want to do this
            print "01_startup: Do you want to overwrite existing files [N/y]?"
            userinput = sys.stdin.readline()
            if (userinput.upper()).strip() != 'Y':
                sys.exit()
            else:
                shutil.rmtree(proddir)

    # If we reach this section, then create folder for ice chart if it does not already exist
    # print proddir
    if not os.path.exists(proddir):
        os.makedirs(proddir)

    # Region specific settings, map projection and extent
    mapfn = ("%s/map_projections.txt" % SETDIR)
    map_projections = get_map_projections( mapfn )
    
    if regionstr == 'arctic':
        PROJSTR = map_projections['SRS_ARCTIC_PROJ4']
        # Full extent of ice chart as ULLR coordinates
        EXTENT = ast.literal_eval( map_projections['SRS_ARCTIC_ULLR'] )
    elif regionstr == 'antarctic':
        PROJSTR = map_projections['SRS_ANTARCTIC_PROJ4']
        # Full extent of ice chart as ULLR coordinates
        EXTENT = ast.literal_eval( map_projections['SRS_ANTARCTIC_ULLR'] )
    else:
        print ("Undefined region: %s" % regionstr)
        sys.exit()

    # Check to see if coastline data exists if not then generate files in ICS/Coastline
    coast_status = check_coastline( regcode, ICEDIR, 0 )

    # If previous_flg is zero, create new ice chart files, otherwise copy from yesterday
    if previous_flg == 0:
        create_new( proddir, datestr, hour, PROJSTR, EXTENT, regcode, credentials['BIFROST_SERVER_IP'] )
        status_flg = 1
    else:
        copy_previous( proddir, datestr, hour, previous_str, previous_hour, PROJSTR, regcode, credentials['BIFROST_SERVER_IP'] )
        status_flg = 2

    # Create database entry, or update existing one
    fldnames = [ 'icechart_date', 'valid_hour', 'region', 'computer', 'charttype', \
        'new_flg', 'new_dt', 'valid_flg', 'valid_dt', \
        'finish_flg', 'finish_dt', 'sigrid_flg', 'sigrid_dt', \
        'gridded_flg', 'gridded_dt', 'outputs_flg', 'outputs_dt', \
        'telexpts_flg', 'telexpts_dt', 'pvupload_flg', 'pvupload_dt', \
        'istupload_flg', 'istupload_dt', 'ftpupload_flg', 'ftpupload_dt', \
        'disseminate_flg', 'disseminate_dt', 'archive_flg', 'archive_dt', \
        'cacheclear_flg', 'cacheclear_dt', 'routine_flg', 'routine_dt', \
        'gridupload_flg', 'gridupload_dt', 'safvalid_flg', 'safvalid_dt', \
        'safupload_flg', 'safupload_dt', 'metline_flg', 'metline_dt', \
        'metdiss_flg', 'metdiss_dt', 'print_flg', 'print_dt', \
        'twitter_flg', 'twitter_dt' ]
    fldtypes = [ 'date', 'smallint', 'char', 'char', 'char', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz', \
        'smallint', 'timestamptz', 'smallint', 'timestamptz' ]
    # Current time
    timenow = datetime.now()
    if overwrite_flg == 0:
        sqltxt1 = ("INSERT INTO production (")
        sqltxt2 = ("VALUES (")
    else:
        sqltxt1 = ("UPDATE production SET (")
        sqltxt2 = ("(")
    firstflg = 1
    for fldname,fldtype in zip(fldnames,fldtypes):
        # print fldname, fldtype
        # Handle first item in list not having a comma in front
        if firstflg == 0:
            namestr = ','
            valuestr = ','
        else:
            namestr = ''
            valuestr = ''
            firstflg = 0
        # Set values by type
        namestr = ("%s%s" % (namestr,fldname))
        if fldtype == 'date':
            valuestr = ("%s\'%s\'" % (valuestr,datestr))
        elif fldtype == 'smallint':
            if fldname == 'new_flg':
                valuestr = ("%s%d" % (valuestr,status_flg))
            elif fldname == 'valid_hour':
                valuestr = ("%s%d" % (valuestr,hour))
            else:
                valuestr = ("%s%d" % (valuestr,0))
        elif fldtype == 'timestamptz':
            dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
            valuestr = ("%s\'%s\'" % (valuestr,dtstr))
        elif fldtype == 'char':
            if fldname == 'region':
                valuestr = ("%s\'%s\'" % (valuestr,regionstr))
            if fldname == 'computer':
                valuestr = ("%s\'%s\'" % (valuestr,ipstr))
            if fldname == 'charttype':
                valuestr = ("%s\'%s\'" % (valuestr,chart_type))
        sqltxt1 = ("%s%s" % (sqltxt1,namestr))
        sqltxt2 = ("%s%s" % (sqltxt2,valuestr))
    if overwrite_flg == 0:
        sqltxt = ("%s) %s);" % (sqltxt1,sqltxt2))
    else:
        sqltxt = ("%s) = %s) WHERE id = %d;" % (sqltxt1,sqltxt2,overwrite_id))
    print sqltxt
    checkres = dbcon.query(sqltxt)
                
    # Close database connection
    dbcon.close()


