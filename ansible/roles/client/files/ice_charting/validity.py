#!/usr/bin/python

# Name:          validity.py
# Purpose:       Polygon geometry validity tests.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys

import osgeo.ogr as ogr
import osgeo.osr as osr

import shapely.wkt
from shapely.validation import explain_validity

import numpy as N

# See http://stackoverflow.com/questions/5278801/validity-of-algorithm-for-creation-of-a-non-self-intersecting-polygon
def polytest( polywkt ):
    status = 0
    eps = 0.000001

    # Create polygon geometry
    poly = ogr.CreateGeometryFromWkt(polywkt)

    # Loop through the rings
    nring = poly.GetGeometryCount()
    for k in range(nring):
        ring = poly.GetGeometryRef(k)

        # Get the vertices
        nnode = ring.GetPointCount()
        x = []
        y = []
        for l in range(nnode):
            x.append(ring.GetX(l))
            y.append(ring.GetY(l))
        # print x
        # print y

        # Now test for intersections
        for i in range(nnode-1):
            x1 = x[i]
            x2 = x[i+1]
            y1 = y[i]
            y2 = y[i+1]
            for j in range(i+2,nnode-1):
                x3 = x[j]
                x4 = x[j+1]
                y3 = y[j]
                y4 = y[j+1]
                x43 = x4 - x3
                x21 = x2 - x1
                y21 = y2 - y1
                y43 = y4 - y3
                y31 = y3 - y1
                # Check for parallel line segments that can never cross
                para_flg = 0
                if x21 < eps and x43 < eps:
                    para_flg = para_flg + 1
                if y21 < eps and y43 < eps:
                    para_flg = para_flg + 1
                # print para_flg
                divider = ( x43*y21 - x21*y43 )

                if para_flg == 0 and divider != 0:
                    inx = ( x43*y21*x1 - x21*y43*x3 + y31*x21*x43 ) / divider
                    if x1 < x2:
                        minx12 = x1
                        maxx12 = x2
                    else:
                        minx12 = x2
                        maxx12 = x1
                    if x3 < x4:
                        minx34 = x3
                        maxx34 = x4
                    else:
                        minx34 = x4
                        maxx34 = x3
                    if minx12 < inx and inx < maxx12 and minx34 < inx and inx < maxx34:
                        ctxt1 = ("LINESTRING(%f %f,%f %f)" % (x1,y1,x2,y2)) 
                        ctxt2 = ("LINESTRING(%f %f,%f %f)" % (x3,y3,x4,y4)) 
                        print ("polytest: Intersect between %s and %s" % (ctxt1,ctxt2))
                        status = 1

    return status


# var ps = vectors.features[0].geometry.getVertices(), i, j, inx, x1, x2, x3, x4, y1, y2, y3, y4, x43, x21, y21, y43, y31, maxx12, maxx34, minx12, minx34;
# ps.push(ps[0]);
# for(i = 0; i < ps.length -1 ; i++ ) {
#   x1 = ps[i].x; x2 = ps[i+1].x;
#   y1 = ps[i].y; y2 = ps[i+1].y;
#   for(j = i + 2; j < ps.length -1 ; j++ ) {
#     x3 = ps[j].x; x4 = ps[j+1].x;
#     y3 = ps[j].y; y4 = ps[j+1].y;
#     x43 = x4 - x3; x21 = x2 - x1; y21 = y2 - y1; y43 = y4 - y3; y31 = y3 - y1;
#     inx = ( x43*y21*x1 - x21*y43*x3 + y31*x21*x43 )/( x43*y21 - x21*y43 );
#     if( x1 < x2 ){
#       minx12 = x1; maxx12 = x2;
#     } else {
#       minx12 = x2; maxx12 = x1;
#     }
#     if( x3 < x4 ){
#       minx34 = x3; maxx34 = x4;
#     } else {
#       minx34 = x4; maxx34 = x3;
#     }
#     if (minx12 < inx && inx < maxx12 && minx34 < inx && inx < maxx34 ){
#       console.log('intersected!'+' ,x1: '+x1+' ,x2: '+x2+' ,inx: '+inx+' ,i: '+i+' ,j: '+j);
# 
#       return;
#     }
#   }
# }


def find_slope(x1, y1, x2, y2):
    # the points (x1, y1) and (x2, y2) define this line segment
    return ((y2 - y1) / (x2 - x1))

def find_intercept(m, x, y):
    # m is the slope of the segment, and (x,y) is one of the endpoints
    return (y - m * x)

def find_intersection(m1, b1, m2, b2): 
    # m, b describe the slope and intersection of each line segment (y = m*x + b)
    x = ((b2-b1) / (m1-m2)) # solve for x
    y = m1 * x + b1 # solve for y
    return [x,y]

# Check for multiple points of the same type
def pointcounter( polywkt ):
    # Overall error count
    errors = 0

    # Tolerance for duplicate point check
    toler = 5.0

    # Create polygon geometry
    poly = ogr.CreateGeometryFromWkt(polywkt)

    # Loop through the rings
    nring = poly.GetGeometryCount()
    for i in range(nring):
        ring = poly.GetGeometryRef(i)

        # Get the vertices
        nnode = ring.GetPointCount()
        x = []
        y = []
        for j in range(nnode):
            x.append(ring.GetX(j))
            y.append(ring.GetY(j))
        # print x
        # print y

        # Beginning and end points will be the same, so ignore the end point
        px = []
        py = []
        pcount = []
        for j in range(nnode-1):
            present = 0
            for k in range(len(px)):
               diffx = abs(x[j] - px[k])
               diffy = abs(y[j] - py[k])
               if diffx < toler and diffy < toler:
                   present = 1
            if present == 0:
                # print j
                px.append(x[j])
                py.append(y[j])
                count = 1
                for k in range(j+1,nnode-1):
                    # print '   ', k
                    diffx = abs(x[j] - x[k])
                    diffy = abs(y[j] - y[k])
                    if diffx < toler and diffy < toler:
                        count = count + 1
                pcount.append(count)

        # See if pcount array has any values greater than
        px = N.array(px,N.float64)
        py = N.array(py,N.float64)
        pcount = N.array(pcount,N.uint16)
        maxcount = N.max(pcount)
        if maxcount > 1:
            maxidx = N.nonzero(pcount>1)[0]
            # print maxcount, maxidx
            # print pcount[maxidx],maxidx
            # print px[12]
            # print py[12]
            for idx in maxidx:
                print ("pointcounter: Duplication of %d points at %.2f,%.2f (Vertex %d)" % (pcount[idx],px[idx],py[idx],idx))
                errors = errors + 1
            # sys.exit()

    status = 0
    if errors > 0:
        status = 1

    # sys.exit()

    return status
