#!/usr/bin/python

# Name:          bifrost-clean-status.py
# Purpose:       Reset status of server-client link.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import sys, os
from datetime import datetime
from time import sleep

# Status filename for checking
bifrostfn = '/tmp/bifrost_status.txt'

# Check for age of file (seconds)
age = 60.0

while 1:
    try:
        fin = open( bifrostfn, 'r' )
    except:
        pass
    else:
        txtln = fin.readline()
        fin.close()
        
        # See if datetime is more than age old
        txtbits = (txtln.strip()).split(',')
        file_dt = datetime.strptime( txtbits[2], '%Y-%m-%d %H:%M:%S.%f' )
        deltat = (datetime.now() - file_dt).total_seconds()
        
        # If older than age, delete the file
        if deltat > age:
            print 'Removing stale file.'
            os.remove( bifrostfn )
        else:
            print ("File is %d seconds old." % deltat)
        
    # Wait 10 seconds before trying again
    sleep(10)
