<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.4.0-Chugiak" minimumScale="-4.65661e-10" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <pipe>
    <rasterrenderer opacity="1" alphaBand="0" classificationMax="10000" classificationMinMaxOrigin="User" band="1" classificationMin="0" type="singlebandpseudocolor">
      <rasterTransparency>
        <singleValuePixelList>
          <pixelListEntry min="0" max="0" percentTransparent="100"/>
        </singleValuePixelList>
      </rasterTransparency>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" clip="0">
          <item alpha="255" value="0" label="0.000000" color="#d7191c"/>
          <item alpha="255" value="1111.11" label="1111.111111" color="#e75b3a"/>
          <item alpha="255" value="2222.22" label="2222.222222" color="#f89d59"/>
          <item alpha="255" value="3333.33" label="3333.333333" color="#fdc980"/>
          <item alpha="255" value="4444.44" label="4444.444444" color="#feedaa"/>
          <item alpha="255" value="5555.56" label="5555.555556" color="#ecf7b9"/>
          <item alpha="255" value="6666.67" label="6666.666667" color="#c7e8ad"/>
          <item alpha="255" value="7777.78" label="7777.777778" color="#9cd3a6"/>
          <item alpha="255" value="8888.89" label="8888.888889" color="#63abb0"/>
          <item alpha="255" value="10000" label="10000.000000" color="#2b83ba"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeBlue="128" grayscaleMode="0" saturation="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
