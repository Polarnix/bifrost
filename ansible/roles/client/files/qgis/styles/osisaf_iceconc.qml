<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.4.0-Chugiak" minimumScale="-4.65661e-10" maximumScale="1e+08" hasScaleBasedVisibilityFlag="0">
  <pipe>
    <rasterrenderer opacity="1" alphaBand="0" classificationMax="100" classificationMinMaxOrigin="User" band="1" classificationMin="0" type="singlebandpseudocolor">
      <rasterTransparency>
        <singleValuePixelList>
          <pixelListEntry min="-999" max="-999" percentTransparent="100"/>
          <pixelListEntry min="0" max="0" percentTransparent="100"/>
        </singleValuePixelList>
      </rasterTransparency>
      <rastershader>
        <colorrampshader colorRampType="INTERPOLATED" clip="0">
          <item alpha="255" value="0" label="0.000000" color="#2b83ba"/>
          <item alpha="255" value="11.1111" label="11.111111" color="#63abb0"/>
          <item alpha="255" value="22.2222" label="22.222222" color="#9cd3a6"/>
          <item alpha="255" value="33.3333" label="33.333333" color="#c7e8ad"/>
          <item alpha="255" value="44.4444" label="44.444444" color="#ecf7b9"/>
          <item alpha="255" value="55.5556" label="55.555556" color="#feedaa"/>
          <item alpha="255" value="66.6667" label="66.666667" color="#fdc980"/>
          <item alpha="255" value="77.7778" label="77.777778" color="#f89d59"/>
          <item alpha="255" value="88.8889" label="88.888889" color="#e75b3a"/>
          <item alpha="255" value="100" label="100.000000" color="#d7191c"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation colorizeGreen="128" colorizeOn="0" colorizeRed="255" colorizeBlue="128" grayscaleMode="0" saturation="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
