<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="1.8.0-Lisboa" minimumScale="0" maximumScale="1e+08" minLabelScale="0" maxLabelScale="1e+08" hasScaleBasedVisibilityFlag="0" scaleBasedLabelVisibilityFlag="0">
  <transparencyLevelInt>255</transparencyLevelInt>
  <classificationattribute>shiptype</classificationattribute>
  <uniquevalue>
    <classificationfield>shiptype</classificationfield>
    <symbol>
      <lowervalue null="1"></lowervalue>
      <uppervalue null="1"></uppervalue>
      <label>default</label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="161" blue="203" green="102"/>
      <fillpattern>NoBrush</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Cargo ship</lowervalue>
      <uppervalue>Cargo ship</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Fishing Vessel</lowervalue>
      <uppervalue>Fishing Vessel</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="255" blue="0" green="0"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>HSC</lowervalue>
      <uppervalue>HSC</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Law enforcement vessel</lowervalue>
      <uppervalue>Law enforcement vessel</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Local assigned 2</lowervalue>
      <uppervalue>Local assigned 2</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Medical transport</lowervalue>
      <uppervalue>Medical transport</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="255" blue="0" green="170"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Other</lowervalue>
      <uppervalue>Other</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="255" blue="255" green="0"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Passenger ship</lowervalue>
      <uppervalue>Passenger ship</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Pilot vessel</lowervalue>
      <uppervalue>Pilot vessel</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Pleasure craft</lowervalue>
      <uppervalue>Pleasure craft</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="0" blue="255" green="0"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Port tender</lowervalue>
      <uppervalue>Port tender</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>SAR Aircraft</lowervalue>
      <uppervalue>SAR Aircraft</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>SAR vessel</lowervalue>
      <uppervalue>SAR vessel</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Sailing Vessel</lowervalue>
      <uppervalue>Sailing Vessel</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="0" blue="0" green="255"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Tanker</lowervalue>
      <uppervalue>Tanker</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Tug boat</lowervalue>
      <uppervalue>Tug boat</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Unused</lowervalue>
      <uppervalue>Unused</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Vessel</lowervalue>
      <uppervalue>Vessel</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="170" blue="255" green="0"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>WIG</lowervalue>
      <uppervalue>WIG</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="210" blue="210" green="210"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
  </uniquevalue>
  <customproperties/>
  <displayfield>shipname</displayfield>
  <label>0</label>
  <labelfield>shipname</labelfield>
  <labelattributes>
    <label fieldname="shipname" text="Label"/>
    <family fieldname="" name="Liberation Sans Narrow"/>
    <size fieldname="" units="pt" value="8"/>
    <bold fieldname="" on="0"/>
    <italic fieldname="" on="0"/>
    <underline fieldname="" on="0"/>
    <strikeout fieldname="" on="0"/>
    <color fieldname="" red="0" blue="0" green="0"/>
    <x fieldname=""/>
    <y fieldname=""/>
    <offset x="0" y="0" units="pt" yfieldname="" xfieldname=""/>
    <angle fieldname="" value="0" auto="0"/>
    <alignment fieldname="" value="center"/>
    <buffercolor fieldname="" red="255" blue="255" green="255"/>
    <buffersize fieldname="" units="pt" value="1"/>
    <bufferenabled fieldname="" on=""/>
    <multilineenabled fieldname="" on=""/>
    <selectedonly on=""/>
  </labelattributes>
  <edittypes>
    <edittype type="0" name="imo"/>
    <edittype type="0" name="shipname"/>
    <edittype type="0" name="shiptype"/>
  </edittypes>
  <editform></editform>
  <editforminit></editforminit>
  <annotationform></annotationform>
  <attributeactions/>
</qgis>
