<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.2.0-Valmiera" minimumScale="0" maximumScale="1e+08" simplifyDrawingHints="1" minLabelScale="0" maxLabelScale="1e+08" simplifyDrawingTol="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyLocal="1" scaleBasedLabelVisibilityFlag="0">
  <renderer-v2 symbollevels="0" type="singleSymbol">
    <symbols>
      <symbol alpha="1" type="fill" name="0">
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="border_width_unit" v="MM"/>
          <prop k="color" v="255,85,0,255"/>
          <prop k="color_border" v="255,85,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="style" v="no"/>
          <prop k="style_border" v="solid"/>
          <prop k="width_border" v="0.66"/>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale scalemethod="area"/>
  </renderer-v2>
  <customproperties>
    <property key="labeling" value="pal"/>
    <property key="labeling/addDirectionSymbol" value="false"/>
    <property key="labeling/angleOffset" value="0"/>
    <property key="labeling/blendMode" value="0"/>
    <property key="labeling/bufferBlendMode" value="0"/>
    <property key="labeling/bufferColorA" value="255"/>
    <property key="labeling/bufferColorB" value="255"/>
    <property key="labeling/bufferColorG" value="255"/>
    <property key="labeling/bufferColorR" value="255"/>
    <property key="labeling/bufferDraw" value="false"/>
    <property key="labeling/bufferJoinStyle" value="64"/>
    <property key="labeling/bufferNoFill" value="false"/>
    <property key="labeling/bufferSize" value="1"/>
    <property key="labeling/bufferSizeInMapUnits" value="false"/>
    <property key="labeling/bufferTransp" value="0"/>
    <property key="labeling/centroidWhole" value="false"/>
    <property key="labeling/decimals" value="3"/>
    <property key="labeling/displayAll" value="false"/>
    <property key="labeling/dist" value="0"/>
    <property key="labeling/distInMapUnits" value="false"/>
    <property key="labeling/enabled" value="true"/>
    <property key="labeling/fieldName" value="datetime"/>
    <property key="labeling/fontBold" value="false"/>
    <property key="labeling/fontCapitals" value="0"/>
    <property key="labeling/fontFamily" value="Liberation Sans Narrow"/>
    <property key="labeling/fontItalic" value="false"/>
    <property key="labeling/fontLetterSpacing" value="0"/>
    <property key="labeling/fontLimitPixelSize" value="false"/>
    <property key="labeling/fontMaxPixelSize" value="10000"/>
    <property key="labeling/fontMinPixelSize" value="3"/>
    <property key="labeling/fontSize" value="8"/>
    <property key="labeling/fontSizeInMapUnits" value="false"/>
    <property key="labeling/fontStrikeout" value="false"/>
    <property key="labeling/fontUnderline" value="false"/>
    <property key="labeling/fontWeight" value="50"/>
    <property key="labeling/fontWordSpacing" value="0"/>
    <property key="labeling/formatNumbers" value="false"/>
    <property key="labeling/isExpression" value="false"/>
    <property key="labeling/labelOffsetInMapUnits" value="false"/>
    <property key="labeling/labelPerPart" value="false"/>
    <property key="labeling/leftDirectionSymbol" value="&lt;"/>
    <property key="labeling/limitNumLabels" value="false"/>
    <property key="labeling/maxCurvedCharAngleIn" value="20"/>
    <property key="labeling/maxCurvedCharAngleOut" value="-20"/>
    <property key="labeling/maxNumLabels" value="2000"/>
    <property key="labeling/mergeLines" value="false"/>
    <property key="labeling/minFeatureSize" value="0"/>
    <property key="labeling/multilineAlign" value="0"/>
    <property key="labeling/multilineHeight" value="1"/>
    <property key="labeling/namedStyle" value="Regular"/>
    <property key="labeling/obstacle" value="true"/>
    <property key="labeling/placeDirectionSymbol" value="0"/>
    <property key="labeling/placement" value="1"/>
    <property key="labeling/placementFlags" value="0"/>
    <property key="labeling/plussign" value="false"/>
    <property key="labeling/preserveRotation" value="true"/>
    <property key="labeling/previewBkgrdColor" value="#ffffff"/>
    <property key="labeling/priority" value="5"/>
    <property key="labeling/quadOffset" value="4"/>
    <property key="labeling/reverseDirectionSymbol" value="false"/>
    <property key="labeling/rightDirectionSymbol" value=">"/>
    <property key="labeling/scaleMax" value="10000000"/>
    <property key="labeling/scaleMin" value="1"/>
    <property key="labeling/scaleVisibility" value="false"/>
    <property key="labeling/shadowBlendMode" value="6"/>
    <property key="labeling/shadowColorB" value="0"/>
    <property key="labeling/shadowColorG" value="0"/>
    <property key="labeling/shadowColorR" value="0"/>
    <property key="labeling/shadowDraw" value="false"/>
    <property key="labeling/shadowOffsetAngle" value="135"/>
    <property key="labeling/shadowOffsetDist" value="1"/>
    <property key="labeling/shadowOffsetGlobal" value="true"/>
    <property key="labeling/shadowOffsetUnits" value="1"/>
    <property key="labeling/shadowRadius" value="1.5"/>
    <property key="labeling/shadowRadiusAlphaOnly" value="false"/>
    <property key="labeling/shadowRadiusUnits" value="1"/>
    <property key="labeling/shadowScale" value="100"/>
    <property key="labeling/shadowTransparency" value="30"/>
    <property key="labeling/shadowUnder" value="0"/>
    <property key="labeling/shapeBlendMode" value="0"/>
    <property key="labeling/shapeBorderColorA" value="255"/>
    <property key="labeling/shapeBorderColorB" value="128"/>
    <property key="labeling/shapeBorderColorG" value="128"/>
    <property key="labeling/shapeBorderColorR" value="128"/>
    <property key="labeling/shapeBorderWidth" value="0"/>
    <property key="labeling/shapeBorderWidthUnits" value="1"/>
    <property key="labeling/shapeDraw" value="false"/>
    <property key="labeling/shapeFillColorA" value="255"/>
    <property key="labeling/shapeFillColorB" value="255"/>
    <property key="labeling/shapeFillColorG" value="255"/>
    <property key="labeling/shapeFillColorR" value="255"/>
    <property key="labeling/shapeJoinStyle" value="64"/>
    <property key="labeling/shapeOffsetUnits" value="1"/>
    <property key="labeling/shapeOffsetX" value="0"/>
    <property key="labeling/shapeOffsetY" value="0"/>
    <property key="labeling/shapeRadiiUnits" value="1"/>
    <property key="labeling/shapeRadiiX" value="0"/>
    <property key="labeling/shapeRadiiY" value="0"/>
    <property key="labeling/shapeRotation" value="0"/>
    <property key="labeling/shapeRotationType" value="0"/>
    <property key="labeling/shapeSVGFile" value=""/>
    <property key="labeling/shapeSizeType" value="0"/>
    <property key="labeling/shapeSizeUnits" value="1"/>
    <property key="labeling/shapeSizeX" value="0"/>
    <property key="labeling/shapeSizeY" value="0"/>
    <property key="labeling/shapeTransparency" value="0"/>
    <property key="labeling/shapeType" value="0"/>
    <property key="labeling/textColorA" value="255"/>
    <property key="labeling/textColorB" value="0"/>
    <property key="labeling/textColorG" value="0"/>
    <property key="labeling/textColorR" value="0"/>
    <property key="labeling/textTransp" value="0"/>
    <property key="labeling/upsidedownLabels" value="0"/>
    <property key="labeling/wrapChar" value=""/>
    <property key="labeling/xOffset" value="0"/>
    <property key="labeling/yOffset" value="0"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerTransparency>0</layerTransparency>
  <displayfield>filename</displayfield>
  <label>0</label>
  <labelattributes>
    <label fieldname="" text="Label"/>
    <family fieldname="" name="Ubuntu"/>
    <size fieldname="" units="pt" value="12"/>
    <bold fieldname="" on="0"/>
    <italic fieldname="" on="0"/>
    <underline fieldname="" on="0"/>
    <strikeout fieldname="" on="0"/>
    <color fieldname="" red="0" blue="0" green="0"/>
    <x fieldname=""/>
    <y fieldname=""/>
    <offset x="0" y="0" units="pt" yfieldname="" xfieldname=""/>
    <angle fieldname="" value="0" auto="0"/>
    <alignment fieldname="" value="center"/>
    <buffercolor fieldname="" red="255" blue="255" green="255"/>
    <buffersize fieldname="" units="pt" value="1"/>
    <bufferenabled fieldname="" on=""/>
    <multilineenabled fieldname="" on=""/>
    <selectedonly on=""/>
  </labelattributes>
  <edittypes>
    <edittype labelontop="0" editable="1" type="0" name="apeninsula_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="apeninsula_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="arcvflg"/>
    <edittype labelontop="0" editable="1" type="0" name="arcviewdt"/>
    <edittype labelontop="0" editable="1" type="0" name="areaflg"/>
    <edittype labelontop="0" editable="1" type="0" name="baltic_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="baltic_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="barents_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="barents_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="bellingh_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="bellingh_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="bellsund_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="bellsund_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="bothnia_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="bothnia_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="bouvet_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="bouvet_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="caspian_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="caspian_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="chelyuskin_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="chelyuskin_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="chukchi_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="chukchi_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="cldflg"/>
    <edittype labelontop="0" editable="1" type="0" name="clouddt"/>
    <edittype labelontop="0" editable="1" type="0" name="datetime"/>
    <edittype labelontop="0" editable="1" type="0" name="deletedt"/>
    <edittype labelontop="0" editable="1" type="0" name="delflg"/>
    <edittype labelontop="0" editable="1" type="0" name="delprocessdt"/>
    <edittype labelontop="0" editable="1" type="0" name="delprocflg"/>
    <edittype labelontop="0" editable="1" type="0" name="denmarks_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="denmarks_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="diaflg"/>
    <edittype labelontop="0" editable="1" type="0" name="dianadt"/>
    <edittype labelontop="0" editable="1" type="0" name="egreenlnd_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="egreenlnd_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="esiberian_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="esiberian_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="eweddell_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="eweddell_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="filename"/>
    <edittype labelontop="0" editable="1" type="0" name="fimbul_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="fimbul_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="finland_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="finland_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="higharc_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="higharc_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="hinlopen_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="hinlopen_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="hkm"/>
    <edittype labelontop="0" editable="1" type="0" name="id"/>
    <edittype labelontop="0" editable="1" type="0" name="isfjord_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="isfjord_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="kara_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="kara_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="karansr_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="karansr_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="km1"/>
    <edittype labelontop="0" editable="1" type="0" name="kongsf_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="kongsf_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="laptev_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="laptev_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="nbering_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="nbering_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="negreenlnd_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="negreenlnd_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="nnorway_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="nnorway_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="oslof_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="oslof_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="processdt"/>
    <edittype labelontop="0" editable="1" type="0" name="procflg"/>
    <edittype labelontop="0" editable="1" type="0" name="qkm"/>
    <edittype labelontop="0" editable="1" type="0" name="rijpf_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="rijpf_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="satellite"/>
    <edittype labelontop="0" editable="1" type="0" name="serverdt"/>
    <edittype labelontop="0" editable="1" type="0" name="sgeorgia_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="sgeorgia_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="size"/>
    <edittype labelontop="0" editable="1" type="0" name="sorkney_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="sorkney_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="storf_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="storf_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="svalbard_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="svalbard_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="threshold"/>
    <edittype labelontop="0" editable="1" type="0" name="white_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="white_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="widjef_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="widjef_cover"/>
    <edittype labelontop="0" editable="1" type="0" name="wweddell_cloud"/>
    <edittype labelontop="0" editable="1" type="0" name="wweddell_cover"/>
  </edittypes>
  <editform></editform>
  <editforminit></editforminit>
  <featformsuppress>0</featformsuppress>
  <annotationform></annotationform>
  <editorlayout>generatedlayout</editorlayout>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <attributeactions/>
</qgis>
