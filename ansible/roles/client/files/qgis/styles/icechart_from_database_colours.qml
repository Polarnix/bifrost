<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="2.2.0-Valmiera" minimumScale="-4.65661e-10" maximumScale="1e+08" simplifyDrawingHints="0" minLabelScale="0" maxLabelScale="1e+08" simplifyDrawingTol="1" simplifyMaxScale="1" hasScaleBasedVisibilityFlag="0" simplifyLocal="1" scaleBasedLabelVisibilityFlag="0">
  <renderer-v2 attr="norway_iceclass" symbollevels="0" type="categorizedSymbol">
    <categories>
      <category symbol="0" value="Close Drift Ice" label="Close Drift Ice"/>
      <category symbol="1" value="Fast Ice" label="Fast Ice"/>
      <category symbol="2" value="Open Drift Ice" label="Open Drift Ice"/>
      <category symbol="3" value="Open Water" label="Open Water"/>
      <category symbol="4" value="Very Close Drift Ice" label="Very Close Drift Ice"/>
      <category symbol="5" value="Very Open Drift Ice" label="Very Open Drift Ice"/>
    </categories>
    <symbols>
      <symbol alpha="1" type="fill" name="0">
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="border_width_unit" v="MM"/>
          <prop k="color" v="255,127,7,255"/>
          <prop k="color_border" v="0,0,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <prop k="style_border" v="solid"/>
          <prop k="width_border" v="0.26"/>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="1">
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="border_width_unit" v="MM"/>
          <prop k="color" v="150,150,150,255"/>
          <prop k="color_border" v="0,0,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <prop k="style_border" v="solid"/>
          <prop k="width_border" v="0.26"/>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="2">
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="border_width_unit" v="MM"/>
          <prop k="color" v="255,255,0,255"/>
          <prop k="color_border" v="0,0,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <prop k="style_border" v="solid"/>
          <prop k="width_border" v="0.26"/>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="3">
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="border_width_unit" v="MM"/>
          <prop k="color" v="150,200,255,255"/>
          <prop k="color_border" v="0,0,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <prop k="style_border" v="solid"/>
          <prop k="width_border" v="0.26"/>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="4">
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="border_width_unit" v="MM"/>
          <prop k="color" v="255,0,0,255"/>
          <prop k="color_border" v="0,0,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <prop k="style_border" v="solid"/>
          <prop k="width_border" v="0.26"/>
        </layer>
      </symbol>
      <symbol alpha="1" type="fill" name="5">
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="border_width_unit" v="MM"/>
          <prop k="color" v="140,255,160,255"/>
          <prop k="color_border" v="0,0,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <prop k="style_border" v="solid"/>
          <prop k="width_border" v="0.26"/>
        </layer>
      </symbol>
    </symbols>
    <source-symbol>
      <symbol alpha="1" type="fill" name="0">
        <layer pass="0" class="SimpleFill" locked="0">
          <prop k="border_width_unit" v="MM"/>
          <prop k="color" v="205,84,161,255"/>
          <prop k="color_border" v="0,0,0,255"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <prop k="style_border" v="solid"/>
          <prop k="width_border" v="0.26"/>
        </layer>
      </symbol>
    </source-symbol>
    <rotation/>
    <sizescale scalemethod="area"/>
  </renderer-v2>
  <customproperties>
    <property key="labeling" value="pal"/>
    <property key="labeling/addDirectionSymbol" value="false"/>
    <property key="labeling/angleOffset" value="0"/>
    <property key="labeling/blendMode" value="0"/>
    <property key="labeling/bufferBlendMode" value="0"/>
    <property key="labeling/bufferColorA" value="255"/>
    <property key="labeling/bufferColorB" value="255"/>
    <property key="labeling/bufferColorG" value="255"/>
    <property key="labeling/bufferColorR" value="255"/>
    <property key="labeling/bufferDraw" value="false"/>
    <property key="labeling/bufferJoinStyle" value="64"/>
    <property key="labeling/bufferNoFill" value="false"/>
    <property key="labeling/bufferSize" value="1"/>
    <property key="labeling/bufferSizeInMapUnits" value="false"/>
    <property key="labeling/bufferTransp" value="0"/>
    <property key="labeling/centroidWhole" value="false"/>
    <property key="labeling/decimals" value="3"/>
    <property key="labeling/displayAll" value="false"/>
    <property key="labeling/dist" value="0"/>
    <property key="labeling/distInMapUnits" value="false"/>
    <property key="labeling/enabled" value="false"/>
    <property key="labeling/fieldName" value=""/>
    <property key="labeling/fontBold" value="true"/>
    <property key="labeling/fontCapitals" value="0"/>
    <property key="labeling/fontFamily" value="Ubuntu"/>
    <property key="labeling/fontItalic" value="true"/>
    <property key="labeling/fontLetterSpacing" value="0"/>
    <property key="labeling/fontLimitPixelSize" value="false"/>
    <property key="labeling/fontMaxPixelSize" value="10000"/>
    <property key="labeling/fontMinPixelSize" value="3"/>
    <property key="labeling/fontSize" value="11"/>
    <property key="labeling/fontSizeInMapUnits" value="false"/>
    <property key="labeling/fontStrikeout" value="false"/>
    <property key="labeling/fontUnderline" value="false"/>
    <property key="labeling/fontWeight" value="75"/>
    <property key="labeling/fontWordSpacing" value="0"/>
    <property key="labeling/formatNumbers" value="false"/>
    <property key="labeling/isExpression" value="false"/>
    <property key="labeling/labelOffsetInMapUnits" value="true"/>
    <property key="labeling/labelPerPart" value="false"/>
    <property key="labeling/leftDirectionSymbol" value="&lt;"/>
    <property key="labeling/limitNumLabels" value="false"/>
    <property key="labeling/maxCurvedCharAngleIn" value="20"/>
    <property key="labeling/maxCurvedCharAngleOut" value="-20"/>
    <property key="labeling/maxNumLabels" value="2000"/>
    <property key="labeling/mergeLines" value="false"/>
    <property key="labeling/minFeatureSize" value="0"/>
    <property key="labeling/multilineAlign" value="0"/>
    <property key="labeling/multilineHeight" value="1"/>
    <property key="labeling/namedStyle" value="Bold Italic"/>
    <property key="labeling/obstacle" value="true"/>
    <property key="labeling/placeDirectionSymbol" value="0"/>
    <property key="labeling/placement" value="0"/>
    <property key="labeling/placementFlags" value="0"/>
    <property key="labeling/plussign" value="false"/>
    <property key="labeling/preserveRotation" value="true"/>
    <property key="labeling/previewBkgrdColor" value="#ffffff"/>
    <property key="labeling/priority" value="5"/>
    <property key="labeling/quadOffset" value="4"/>
    <property key="labeling/reverseDirectionSymbol" value="false"/>
    <property key="labeling/rightDirectionSymbol" value=">"/>
    <property key="labeling/scaleMax" value="10000000"/>
    <property key="labeling/scaleMin" value="1"/>
    <property key="labeling/scaleVisibility" value="false"/>
    <property key="labeling/shadowBlendMode" value="6"/>
    <property key="labeling/shadowColorB" value="0"/>
    <property key="labeling/shadowColorG" value="0"/>
    <property key="labeling/shadowColorR" value="0"/>
    <property key="labeling/shadowDraw" value="false"/>
    <property key="labeling/shadowOffsetAngle" value="135"/>
    <property key="labeling/shadowOffsetDist" value="1"/>
    <property key="labeling/shadowOffsetGlobal" value="true"/>
    <property key="labeling/shadowOffsetUnits" value="1"/>
    <property key="labeling/shadowRadius" value="1.5"/>
    <property key="labeling/shadowRadiusAlphaOnly" value="false"/>
    <property key="labeling/shadowRadiusUnits" value="1"/>
    <property key="labeling/shadowScale" value="100"/>
    <property key="labeling/shadowTransparency" value="30"/>
    <property key="labeling/shadowUnder" value="0"/>
    <property key="labeling/shapeBlendMode" value="0"/>
    <property key="labeling/shapeBorderColorA" value="255"/>
    <property key="labeling/shapeBorderColorB" value="128"/>
    <property key="labeling/shapeBorderColorG" value="128"/>
    <property key="labeling/shapeBorderColorR" value="128"/>
    <property key="labeling/shapeBorderWidth" value="0"/>
    <property key="labeling/shapeBorderWidthUnits" value="1"/>
    <property key="labeling/shapeDraw" value="false"/>
    <property key="labeling/shapeFillColorA" value="255"/>
    <property key="labeling/shapeFillColorB" value="255"/>
    <property key="labeling/shapeFillColorG" value="255"/>
    <property key="labeling/shapeFillColorR" value="255"/>
    <property key="labeling/shapeJoinStyle" value="64"/>
    <property key="labeling/shapeOffsetUnits" value="1"/>
    <property key="labeling/shapeOffsetX" value="0"/>
    <property key="labeling/shapeOffsetY" value="0"/>
    <property key="labeling/shapeRadiiUnits" value="1"/>
    <property key="labeling/shapeRadiiX" value="0"/>
    <property key="labeling/shapeRadiiY" value="0"/>
    <property key="labeling/shapeRotation" value="0"/>
    <property key="labeling/shapeRotationType" value="0"/>
    <property key="labeling/shapeSVGFile" value=""/>
    <property key="labeling/shapeSizeType" value="0"/>
    <property key="labeling/shapeSizeUnits" value="1"/>
    <property key="labeling/shapeSizeX" value="0"/>
    <property key="labeling/shapeSizeY" value="0"/>
    <property key="labeling/shapeTransparency" value="0"/>
    <property key="labeling/shapeType" value="0"/>
    <property key="labeling/textColorA" value="255"/>
    <property key="labeling/textColorB" value="0"/>
    <property key="labeling/textColorG" value="0"/>
    <property key="labeling/textColorR" value="0"/>
    <property key="labeling/textTransp" value="0"/>
    <property key="labeling/upsidedownLabels" value="0"/>
    <property key="labeling/wrapChar" value=""/>
    <property key="labeling/xOffset" value="0"/>
    <property key="labeling/yOffset" value="0"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerTransparency>30</layerTransparency>
  <displayfield>id</displayfield>
  <label>0</label>
  <labelattributes>
    <label fieldname="" text="Label"/>
    <family fieldname="" name="Ubuntu"/>
    <size fieldname="" units="pt" value="12"/>
    <bold fieldname="" on="0"/>
    <italic fieldname="" on="0"/>
    <underline fieldname="" on="0"/>
    <strikeout fieldname="" on="0"/>
    <color fieldname="" red="0" blue="0" green="0"/>
    <x fieldname=""/>
    <y fieldname=""/>
    <offset x="0" y="0" units="pt" yfieldname="" xfieldname=""/>
    <angle fieldname="" value="0" auto="0"/>
    <alignment fieldname="" value="center"/>
    <buffercolor fieldname="" red="255" blue="255" green="255"/>
    <buffersize fieldname="" units="pt" value="1"/>
    <bufferenabled fieldname="" on=""/>
    <multilineenabled fieldname="" on=""/>
    <selectedonly on=""/>
  </labelattributes>
  <edittypes>
    <edittype labelontop="0" editable="1" type="0" name="analyst"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhmax"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhmean"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhmedi"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhmedian"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhmin"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhmode"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhnull"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhnulls"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhstde"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhstdev"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshhsum"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvmax"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvmean"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvmedi"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvmedian"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvmin"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvmode"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvnull"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvnulls"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvstde"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvstdev"/>
    <edittype labelontop="0" editable="1" type="0" name="cskshvsum"/>
    <edittype labelontop="0" editable="1" type="0" name="csksid"/>
    <edittype labelontop="0" editable="1" type="0" name="csksinca_1"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincama"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincamax"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincame"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincamean"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincamedian"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincami"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincamin"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincamo"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincamode"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincanu"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincanulls"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincast"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincastdev"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincasu"/>
    <edittype labelontop="0" editable="1" type="0" name="csksincasum"/>
    <edittype labelontop="0" editable="1" type="0" name="csksnp"/>
    <edittype labelontop="0" editable="1" type="0" name="csksscenes"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhmax"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhmean"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhmedi"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhmedian"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhmin"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhmode"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhnull"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhnulls"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhstde"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhstdev"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvhsum"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvmax"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvmean"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvmedi"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvmedian"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvmin"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvmode"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvnull"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvnulls"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvstde"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvstdev"/>
    <edittype labelontop="0" editable="1" type="0" name="csksvvsum"/>
    <edittype labelontop="0" editable="1" type="0" name="datetime"/>
    <edittype labelontop="0" editable="1" type="0" name="iceberg_id"/>
    <edittype labelontop="0" editable="1" type="0" name="id"/>
    <edittype labelontop="0" editable="1" type="0" name="norway_ice"/>
    <edittype labelontop="0" editable="1" type="0" name="norway_iceclass"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hhmax"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hhmean"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hhmedia"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hhmedian"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hhmin"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hhmode"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hhnulls"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hhstdev"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hhsum"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hvmax"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hvmean"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hvmedia"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hvmedian"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hvmin"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hvmode"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hvnulls"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hvstdev"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2hvsum"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2id"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incamax"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incamea"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incamean"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incamed"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incamedian"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incamin"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incamod"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incamode"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incanul"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incanulls"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incastd"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incastdev"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2incasum"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2np"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2scenes"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vhmax"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vhmean"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vhmedia"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vhmedian"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vhmin"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vhmode"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vhnulls"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vhstdev"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vhsum"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vvmax"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vvmean"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vvmedia"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vvmedian"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vvmin"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vvmode"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vvnulls"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vvstdev"/>
    <edittype labelontop="0" editable="1" type="0" name="rs2vvsum"/>
    <edittype labelontop="0" editable="1" type="0" name="service"/>
    <edittype labelontop="0" editable="1" type="0" name="si3ak"/>
    <edittype labelontop="0" editable="1" type="0" name="si3am"/>
    <edittype labelontop="0" editable="1" type="0" name="si3area"/>
    <edittype labelontop="0" editable="1" type="0" name="si3at"/>
    <edittype labelontop="0" editable="1" type="0" name="si3av"/>
    <edittype labelontop="0" editable="1" type="0" name="si3bd"/>
    <edittype labelontop="0" editable="1" type="0" name="si3be"/>
    <edittype labelontop="0" editable="1" type="0" name="si3bl"/>
    <edittype labelontop="0" editable="1" type="0" name="si3bn"/>
    <edittype labelontop="0" editable="1" type="0" name="si3bo"/>
    <edittype labelontop="0" editable="1" type="0" name="si3by"/>
    <edittype labelontop="0" editable="1" type="0" name="si3ca"/>
    <edittype labelontop="0" editable="1" type="0" name="si3cb"/>
    <edittype labelontop="0" editable="1" type="0" name="si3cc"/>
    <edittype labelontop="0" editable="1" type="0" name="si3cd"/>
    <edittype labelontop="0" editable="1" type="0" name="si3cn"/>
    <edittype labelontop="0" editable="1" type="0" name="si3ct"/>
    <edittype labelontop="0" editable="1" type="0" name="si3dd"/>
    <edittype labelontop="0" editable="1" type="0" name="si3do"/>
    <edittype labelontop="0" editable="1" type="0" name="si3dp"/>
    <edittype labelontop="0" editable="1" type="0" name="si3dr"/>
    <edittype labelontop="0" editable="1" type="0" name="si3ei"/>
    <edittype labelontop="0" editable="1" type="0" name="si3em"/>
    <edittype labelontop="0" editable="1" type="0" name="si3eo"/>
    <edittype labelontop="0" editable="1" type="0" name="si3ex"/>
    <edittype labelontop="0" editable="1" type="0" name="si3fa"/>
    <edittype labelontop="0" editable="1" type="0" name="si3fb"/>
    <edittype labelontop="0" editable="1" type="0" name="si3fc"/>
    <edittype labelontop="0" editable="1" type="0" name="si3fd"/>
    <edittype labelontop="0" editable="1" type="0" name="si3fe"/>
    <edittype labelontop="0" editable="1" type="0" name="si3fp"/>
    <edittype labelontop="0" editable="1" type="0" name="si3fs"/>
    <edittype labelontop="0" editable="1" type="0" name="si3perimet"/>
    <edittype labelontop="0" editable="1" type="0" name="si3perimeter"/>
    <edittype labelontop="0" editable="1" type="0" name="si3poly_ty"/>
    <edittype labelontop="0" editable="1" type="0" name="si3poly_type"/>
    <edittype labelontop="0" editable="1" type="0" name="si3ra"/>
    <edittype labelontop="0" editable="1" type="0" name="si3rc"/>
    <edittype labelontop="0" editable="1" type="0" name="si3rd"/>
    <edittype labelontop="0" editable="1" type="0" name="si3rf"/>
    <edittype labelontop="0" editable="1" type="0" name="si3rh"/>
    <edittype labelontop="0" editable="1" type="0" name="si3rn"/>
    <edittype labelontop="0" editable="1" type="0" name="si3ro"/>
    <edittype labelontop="0" editable="1" type="0" name="si3rx"/>
    <edittype labelontop="0" editable="1" type="0" name="si3sa"/>
    <edittype labelontop="0" editable="1" type="0" name="si3sasnow"/>
    <edittype labelontop="0" editable="1" type="0" name="si3sb"/>
    <edittype labelontop="0" editable="1" type="0" name="si3sc"/>
    <edittype labelontop="0" editable="1" type="0" name="si3scsnow"/>
    <edittype labelontop="0" editable="1" type="0" name="si3sd"/>
    <edittype labelontop="0" editable="1" type="0" name="si3sdsnow"/>
    <edittype labelontop="0" editable="1" type="0" name="si3se"/>
    <edittype labelontop="0" editable="1" type="0" name="si3smsnow"/>
    <edittype labelontop="0" editable="1" type="0" name="si3snsnow"/>
    <edittype labelontop="0" editable="1" type="0" name="si3so"/>
    <edittype labelontop="0" editable="1" type="0" name="si3sosnow"/>
    <edittype labelontop="0" editable="1" type="0" name="si3wd"/>
    <edittype labelontop="0" editable="1" type="0" name="si3wf"/>
    <edittype labelontop="0" editable="1" type="0" name="si3wn"/>
    <edittype labelontop="0" editable="1" type="0" name="si3wo"/>
    <edittype labelontop="0" editable="1" type="0" name="si3ww"/>
    <edittype labelontop="0" editable="1" type="0" name="srcimgid"/>
    <edittype labelontop="0" editable="1" type="0" name="srcimgtype"/>
  </edittypes>
  <editform></editform>
  <editforminit></editforminit>
  <featformsuppress>0</featformsuppress>
  <annotationform>.</annotationform>
  <editorlayout>generatedlayout</editorlayout>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <attributeactions/>
</qgis>
