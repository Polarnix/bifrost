#!/usr/bin/python

# Name:          bifrost-receiver.py
# Purpose:       Receive status of Bifrost server, run as a daemon.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import socket
import select
import sys
from datetime import datetime

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind( ('', 50100) )

# Filename for output
bifrostfn = '/tmp/bifrost_status.txt'

while 1:
    (rfd,wfd,efd)=select.select([s],[],[])
    if s in rfd:
        (string, address) = s.recvfrom(100)
        print 'string: %s' % string
        print 'from: %s' % str(address)
        print 'when: %s' % datetime.now()
        print '--------------------------------'
        
        # Get server number
        strbits = string.split('_')
        server_no = int(strbits[-1])
        
        # Get server IP address
        server_ip = address[0].replace("\'","")
        
        # Write Bifrost status to file
        fout = open( bifrostfn, 'w', 0)
        fout.write( ("%d,%s,%s\n" % (server_no,server_ip,datetime.now())) )
        fout.close()