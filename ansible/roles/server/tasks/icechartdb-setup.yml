# Name:          icechartdb-setup.yml
# Purpose:       Create the ice charts database.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
---
- name: create ice charts database
  postgresql_db: name=Icecharts
                 state=present
                 port=5432
                 owner=bifrostadmin
                 encoding='UTF-8'
                 template='template_postgis'
                 
- name: add bifrost admin user role
  postgresql_user: db=Icecharts name=bifrostadmin password=bifrostadmin priv=ALL
- name: add bifrost analyst user role
  postgresql_user: db=Icecharts name=bifrostanalyst password=bifrostanalyst priv=ALL
- name: add bifrost satellite user role
  postgresql_user: db=Icecharts name=bifrostsat password=bifrostsat priv=ALL
  
- name: remove bifrostanalyst unnecessary privileges
  postgresql_user: name=bifrostanalyst role_attr_flags=NOSUPERUSER,NOCREATEDB
- name: remove bifrostsat unnecessary privileges
  postgresql_user: name=bifrostsat role_attr_flags=NOSUPERUSER,NOCREATEDB

- name: create icecharts database tables
  include: roles/server/tasks/run-sql-script.yml sqlfile={{ item }} dbname=Icecharts
  with_items:
    - icechartdb-icechart-polygons-setup.sql
    - icechartdb-icechart-lines-setup.sql
    - icechartdb-icechart-symbols-setup.sql
    - icechartdb-iceshelf-setup.sql
    - icechartdb-icebergs-setup.sql
    - icechartdb-metarea-line-setup.sql
    - icechartdb-nic-icebergs-setup.sql
    - icechartdb-customer-setup.sql
    - icechartdb-production-setup.sql
    - icechartdb-external-production-setup.sql  
