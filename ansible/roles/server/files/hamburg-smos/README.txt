Directory for University of Hamburg SMOS processing scripts.

For data files, see https://icdc.zmaw.de/thredds/catalog/ftpthredds/smos_sea_ice_thickness/catalog.html
Data is available October to ~April, Arctic only. NetCDF format.
