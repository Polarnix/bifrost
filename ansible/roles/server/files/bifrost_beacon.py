#!/usr/bin/python

# Name:          bifrost_beacon.py
# Purpose:       Broadcasts a beacon signal to the local subnet for helping client machines to
#                locate the server.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import sys
import time
import socket
import ipaddr
import netifaces

# eth1 = Used by Virtualbox for private network
# eth0 = Used by Virtual box for host adapator - communicates outside the box if
#        port forwarding enabled.

ext_netadapt = 'enp0s3'    # 'eth0'
netifaces.ifaddresses(ext_netadapt)
ext_ip = netifaces.ifaddresses(ext_netadapt)[2][0]['addr']
priv_netadapt = 'enp0s8'   # 'eth1'
netifaces.ifaddresses(priv_netadapt)
priv_ip = netifaces.ifaddresses(priv_netadapt)[2][0]['addr']
print ext_ip, priv_ip

# Get the netmask (see https://docs.python.org/3/library/ipaddress.html#ipaddress.IPv4Network)
ext_mask = ipaddr.IPv4Network( ("%s/24" % ext_ip ) )
ext_netmask = str( ext_mask.broadcast )
priv_mask = ipaddr.IPv4Network( ("%s/24" % priv_ip ) )
priv_netmask = str( priv_mask.broadcast )
print ext_netmask, priv_netmask

s = socket.socket (socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
s.setsockopt (socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
while 1:
    s.sendto('bifrost_ext_1\n', (ext_netmask, 50100))
    s.sendto('bifrost_priv_1\n', (priv_netmask, 50100))
    time.sleep(5)