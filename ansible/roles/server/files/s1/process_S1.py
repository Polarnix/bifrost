#!/usr/bin/python

# Name:          process_S1.py
# Purpose:       Processes Sentinel-1 image files, either from download or Dropbox.
#                Interacts with PostgreSQL+PostGIS 'Satellite' database and 's1_files'
#                table.
#
#                procstatus codes:
#                    0 = Not processed
#                -9001 = File was not a .zip
#                -9002 = Unable to determine resolution of image type
#                -9003 = Unable to determine what bands are in the file
#                -9004 = Unable to find input file
#                -9005 = Empty file problem
#                -9006 = RAW,SLC, or OCN file
#
#                -9002 = Unable to gdalwarp the image tile.
#                -9003 = Unable to gdal_translate to the final image format.
#                -9004 = Unable to find input file
#                -9005 = Empty file problem
#                -9900 = Some other problem in processing.
#                -9999 = Image not in the Arctic or Antarctic.
#                   -1 = Successful, Antarctic.
#                    1 = Successful, Arctic.
#
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


import os, sys, platform

import string, time

import shlex, shutil, subprocess
import string

import struct

from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo import gdal_array
from osgeo.gdalconst import *

import numpy as N

import zipfile

import pg


# System dependent settings
BASEDIR = '/home/bifrostsat/Data/S1'
DATADIR = ("%s/Raw" % BASEDIR)
DROPBOX = "/home/bifrostsat/Dropbox"
ANCDIR = '/home/bifrostsat/Ancillary/S1'
TMPDIR = ("%s/tmp" % BASEDIR)
GDALHOME = '/usr'

ARCPROJ='+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84'
ANTPROJ='+proj=stere +lat_0=90s +lon_0=0e +lat_ts=90s +ellps=WGS84'
MIDPROJ='+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs'

# Number of files to process before  stopping
PROCLIMIT = 10

# Output the start time
tmpt=time.gmtime()
tout=( '*** process_S1.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file for slow computers
lockfn= ANCDIR + '/process_S1.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    fout = open(lockfn, 'w')
    fout.write(tout)
    fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()
    
# Interrogate 'icecharts' database table 'envisatfiles' to see what files are
# available for processing.
con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')
queryresult = con1.query('SELECT id,filename,datetime,location FROM s1_files WHERE procflg = 0 ORDER BY datetime DESC;')
proclist = queryresult.getresult()
# print proclist

proccount = 0
for item in proclist:
    fid = item[0]
    fname = item[1]
    acqdtstr = item[2]
    location = item[3]
    print "Processing ", fname

    if location == 'Dropbox':
        fullfname = ("%s/%s" % (DROPBOX,fname))
    else:
        fullfname = ("%s/%s/%s" % (BASEDIR,location,fname))

    procstatus = 0
    outres = 0
    try:
        acqdt = time.strptime(acqdtstr,"%Y-%m-%d %H:%M:%S")
    except:
        acqdt = time.strptime(acqdtstr,"%Y-%m-%d %H:%M:%S.%f")
    # Create temporary directory for data extraction
    extractdir = ("%s/%s" % (TMPDIR,fname[:-4]))
    # extractdir = ("%s" % (TMPDIR))

    # If extractdir exists, delete it
    if os.path.isdir(extractdir):
        shutil.rmtree(extractdir)

    # Try to unpack the file
    if fname[-4:] != '.zip':
        print 'Expecting a ZIP archive file!'
        procstatus = -9001
    else:
        # print fullfname
        try:
            # print extractdir
            os.mkdir(extractdir)
            s1zip = zipfile.ZipFile(fullfname, 'r')
            s1zip.extractall(extractdir)
            s1zip.close()
            pass
        except:
            print 'Problem opening file.'
            procstatus = -9010
        else:
            pass
        pass

    # Determine rootfname, since it seems to differ from that of the zip-file 
    # Look for the date and time
    # NB: Not needed for ESA zip-files
    # searchstr = time.strftime("_%Y%m%dT%H%M%S_",acqdt)
    # filelist=os.listdir(TMPDIR)
    # for fname in filelist:
    #     if fname.find(searchstr) != -1:
    #         rootfname = fname
    rootfname = fname[:-4]
    # print procstatus

    if procstatus > -9000:

        # Sentinel-1 is not yet supported in GDAL, therefore we have to use the uncalibrated
        # images.
        srcdir = ("%s/%s/%s/measurement" % (TMPDIR,rootfname,rootfname))
        # scrdir updated to handle new ESA zipped-file structure
        #srcdir = ("%s/%s/data/FTP/S1/%4d/%02d/%02d/%s/measurement" \
        #    % (TMPDIR,rootfname,acqdt.tm_year,acqdt.tm_mon,acqdt.tm_mday,rootfname))
        # print srcdir
        srcfilelist = os.listdir(srcdir)
        
        # Determine channels that are present
        hhvvhvvh_flg = [ 0, 0, 0, 0 ]
        chnfn = [ 'None', 'None', 'None', 'None' ]
        nbands = 0
        warpfn = [ 'None', 'None', 'None', 'None' ]
        for srcfn in srcfilelist:
            if srcfn.find('-hh-') != -1:
                hhvvhvvh_flg[0] = 1
                chnfn[0] = ("%s/%s" % (srcdir,srcfn))
                nbands = nbands + 1
            if srcfn.find('-vv-') != -1:
                hhvvhvvh_flg[1] = 1
                chnfn[1] = ("%s/%s" % (srcdir,srcfn))
                nbands = nbands + 1
            if srcfn.find('-hv-') != -1:
                hhvvhvvh_flg[2] = 1
                chnfn[2] = ("%s/%s" % (srcdir,srcfn))
                nbands = nbands + 1
            if srcfn.find('-vh-') != -1:
                hhvvhvvh_flg[3] = 1
                chnfn[3] = ("%s/%s" % (srcdir,srcfn))
                nbands = nbands + 1
        # print hhvvhvvh_flg
        # print chnfn
        print 'Bands = ', nbands

        # Assign a channel for testing
        if hhvvhvvh_flg[0] == 1:
            testchn = 0
        elif hhvvhvvh_flg[1] == 1:
            testchn = 1
        elif hhvvhvvh_flg[2] == 1:
            testchn = 2
        elif hhvvhvvh_flg[3] == 1:
            testchn = 3
        # print testchn

        # Construct a text string containing the bands
        bandstr = ''
        if hhvvhvvh_flg[0] == 1:
            bandstr = ("%sHH" % bandstr)
        if hhvvhvvh_flg[1] == 1:
            bandstr = ("%sVV" % bandstr)
        if hhvvhvvh_flg[2] == 1:
            bandstr = ("%sHV" % bandstr)
        if hhvvhvvh_flg[3] == 1:
            bandstr = ("%sVH" % bandstr)
        # print bandstr

        # Check for Date Line (180E/180W)
        dateline = 0
        firstgcp = 1
        # print chnfn[testchn]
        inds = gdal.Open(chnfn[testchn])
        # n_gcp = inds.GetGCPCount()
        # print n_gcp
        gcps = inds.GetGCPs()
        inds = None
        for gcp in gcps:
            # print gcp.GCPPixel, gcp.GCPLine, gcp.GCPX, gcp.GCPY, gcp.GCPZ
            if firstgcp == 1:
                minlon = gcp.GCPX
                maxlon = gcp.GCPX
                firstgcp = 0
            else:
                if gcp.GCPX > maxlon:
                    maxlon = gcp.GCPX
                if gcp.GCPX < minlon:
                    minlon = gcp.GCPX
        # print minlon, maxlon
        if minlon < -90 and maxlon > 90:
            dateline = 1
        # print dateline

        # Define outres according to filename
        imagemodes = [ 'EW',  'IW'  ]
        imagetypes = [ 'GRD', 'GRD' ]
        imageres =   [ 50,    50    ]
        i=0
        outres=0
        for imgmode in imagemodes:
            teststr=('_%s_' % imgmode)
            if string.find(rootfname,teststr) > -1:
                outres=imageres[i]
            i=i+1
        if outres==0:
            print 'Unable to determine input image type from filename', rootfname
            procstatus = -9002        
        else:
            print ('Output resolution = %d metres' % outres)

    # Determine area (Arctic/Antarctic/Mid-latitudes) using GCPs
    if procstatus > -9000:
        ngcp=len(gcps)
        gcplon=N.zeros((ngcp,1),dtype=N.float32)
        gcplat=N.zeros((ngcp,1),dtype=N.float32)
        gcpx=N.zeros((ngcp,1),dtype=N.float32)
        gcpy=N.zeros((ngcp,1),dtype=N.float32)
    
        narc=0
        nant=0
        nmid=0
        i=0
        for gcp in gcps:
            gcplon[i] = gcp.GCPX
            gcplat[i] = gcp.GCPY
            if gcplat[i] > 50.0:
                narc = narc + 1
            elif gcplat[i] < -50.0:
                nant = nant + 1
            else:
                nmid = nmid + 1
            gcpx[i] = gcp.GCPPixel
            gcpy[i] = gcp.GCPLine
            # print gcp.GCPPixel, gcp.GCPLine, gcp.GCPX, gcp.GCPY
            i=i+1

        nx = 1
        while gcpx[nx] > gcpx[nx-1]:
            nx = nx + 1
            ny = ngcp / nx
            # print nx, ny

        gcpx = gcpx.reshape(ny,nx).copy()
        gcpy = gcpy.reshape(ny,nx).copy()
        gcplon = gcplon.reshape(ny,nx).copy()
        gcplat = gcplat.reshape(ny,nx).copy()

        area = 'Unknown'
        # Check to see if Arctic
        if narc > 0:
            area = 'Arctic'
            procstatus = 1
            lat0 = 90.0
            lat0str = '90n'
            targproj=ARCPROJ

        # Check to see if Antarctic
        if nant > 0:
           area = 'Antarctic'
           procstatus = -1
           lat0 = -90.0
           lat0str = '90s'
           targproj=ANTPROJ

        # Check to see if mid-latitude
        if nmid > 0:
            area = 'Mid-latitudes'
            procstatus = 2
            targproj=MIDPROJ

        print 'Area = ', area

    # Check for SLC file
    if rootfname.find('SLC') > -1 or rootfname.find('RAW') > -1 \
        or rootfname.find('OCN') > -1:
        print 'SLC file ', rootfname
        procstatus = -9006

    #  Only do more processing of image is in an area of interest
    if procstatus > -9000:

        # If dateline, then gdal_translate to apply "corrected" GCPs
        newgcp_list = []
        for i in range(4):
            if hhvvhvvh_flg[i] == 1:
                inpfname = chnfn[i]
                # print inpfname
                # Construct a channel root filename
                inpfnbits = inpfname.split('/')
                chnrootfn = inpfnbits[-1][:-5]

                newgcpfname = ("%s/%s_newgcps.tif" % (TMPDIR,chnrootfn))
                newgcp_list.append(newgcpfname)

                # Use gdal_translate to construct files with corrected GCPs
                cmd = GDALHOME+"/bin/gdal_translate -of GTiff -co \"COMPRESS=LZW\" -co \"BIGTIFF=YES\" " + \
                    "-a_srs \"+proj=longlat +ellps=WGS84\" "
                if dateline == 1:
                    for gcp in gcps:
                        tmpgcplon = gcp.GCPX
                        if tmpgcplon < 0:
                            tmpgcplon = tmpgcplon + 360.0
                            gcp.GCPX = tmpgcplon
                        cmd = cmd + "-gcp " + \
                            ("%f " % gcp.GCPPixel) + ("%f " % gcp.GCPLine) + \
                            ("%f " % tmpgcplon) + ("%f " % gcp.GCPY) + ("%f " % gcp.GCPZ)
                else:
                    for gcp in gcps:
                        tmpgcplon = gcp.GCPX
                        cmd = cmd + "-gcp " + \
                            ("%f " % gcp.GCPPixel) + ("%f " % gcp.GCPLine) + \
                            ("%f " % tmpgcplon) + ("%f " % gcp.GCPY) + ("%f " % gcp.GCPZ)
                cmd = cmd + "-quiet " + inpfname + " " + newgcpfname
                # print cmd
                retcode = 0
                try:
                    retcode = subprocess.call(cmd, shell=True)
                    if retcode < 0:
                        print >>sys.stderr, "Child was terminated by signal", -retcode
                    # else:
                    #     print >>sys.stderr, "Child returned", retcode
                except OSError, e:
                    print >>sys.stderr, "Execution failed:", e
    
                # Create incidence angle image
                # Not known yet for Sentinel-1
    
                # Remove incidence angle slope
                # Not known yet for Sentinel-1

                # gdalwarp Sentinel-1 image
                corrfname = TMPDIR + "/" + rootfname + "_corrected.tif"
                if dateline == 0:
                    cmd = GDALHOME+"/bin/gdalwarp -of GTiff -co \"BIGTIFF=YES\" "
                else:
                    cmd = GDALHOME+"/bin/gdalwarp --config CENTER_LONG 180 -of GTiff -co \"BIGTIFF=YES\" "
                outfname = ("%s/%s.tif" % (TMPDIR,chnrootfn))
                warpfn[i] = outfname
                # print 'corrfname = ', corrfname
                # print 'outfname = ', outfname
                cmd = cmd + "-order 3 " + \
                    ("-t_srs \"%s\" " % targproj) + \
                    ("-tr %d %d -r bilinear -q -overwrite" % (outres,outres))
                cmd = ("%s %s %s" % (cmd,newgcpfname,outfname))
                # print cmd
                # Other options: --debug on to see if there are problems
                #                -order 3 seems to work best, -order 2 lots of work
                #                -q for quiet
                #                -overwrite to overwrite older files
                retcode = 0
                try:
                    retcode = subprocess.call(cmd, shell=True)
                    if retcode < 0:
                        print >>sys.stderr, "Child was terminated by signal", -retcode
                    # else:
                    #     print >>sys.stderr, "Child returned", retcode
                except OSError, e:
                    print >>sys.stderr, "Execution failed:", e

        # Open warped images to get statistics
        coflg = 0
        if hhvvhvvh_flg[0] == 1:
            co_inds = gdal.Open(warpfn[0])
            coflg = 1
            # print warpfn[0]
        elif hhvvhvvh_flg[1] == 1:
            co_inds = gdal.Open(warpfn[1])
            # print warpfn[1]
            coflg = 1
        else:
            print 'No co-polarised channel available.'
            # sys.exit()
        xflg = 0
        if hhvvhvvh_flg[2] == 1:
            x_inds = gdal.Open(warpfn[2])
            xflg = 1
        elif hhvvhvvh_flg[3] == 1:
            x_inds = gdal.Open(warpfn[3])
            xflg = 1
        else:
            print 'No cross-polarised channel available.'
            # sys.exit()
       
        # Get co-polarised band
        if coflg == 1:
            band_copol = co_inds.GetRasterBand(1)
            stats = co_inds.GetRasterBand(1).GetStatistics(0,1)
            # print 'Band HH Type=',gdal.GetDataTypeName(band_copol.DataType)
            min_copol = band_copol.GetMinimum()
            max_copol = band_copol.GetMaximum()
            if min_copol is None or max_copol is None:
                (min_copol,max_copol) = band_copol.ComputeRasterMinMax(1)
            print 'Min=%.3f, Max=%.3f' % (min_copol,max_copol)
            [avg_copol,std_copol] = band_copol.ComputeBandStats()
            # print avg_copol, std_copol
        
        # Get cross-polarised band
        if xflg == 1:
            band_xpol = x_inds.GetRasterBand(1)
            stats = x_inds.GetRasterBand(1).GetStatistics(0,1)
            # print 'Band HV Type=',gdal.GetDataTypeName(band_xpol.DataType)
            min_xpol = band_xpol.GetMinimum()
            max_xpol = band_xpol.GetMaximum()
            if min_xpol is None or max_xpol is None:
                (min_xpol,max_xpol) = band_xpol.ComputeRasterMinMax(1)
            print 'Min=%.3f, Max=%.3f' % (min_xpol,max_xpol)
            [avg_xpol,std_xpol] = band_xpol.ComputeBandStats()
            # print avg_xpol, std_xpol
        
        # Input projection string and geotransform
        projstr = co_inds.GetProjection()
        transform = co_inds.GetGeoTransform()
        
        # Calculate new colour scale limits (average + 2 stddev)
        if coflg == 1:
            min_copol = 0.0
            max_copol = avg_copol + (2.0 * std_copol)
        if xflg == 1:
            min_xpol = 0.0
            max_xpol = avg_xpol + (2.0 * std_xpol)
            min_co_x = -(2.0 * std_xpol)
            max_co_x = avg_copol + (2.0 * std_copol)
        
        if coflg == 1:
            a = N.zeros((band_copol.YSize,band_copol.XSize,3),N.uint8)
        elif xflg == 1:
            a = N.zeros((band_xpol.YSize,band_xpol.XSize,3),N.uint8)
        # f4b = N.zeros((band_copol.YSize,band_copol.XSize,1),N.float32)
        minb = 0.0
        maxb = 0.0
        for i in range(band_copol.YSize):
            if coflg == 1:
                # if i % 1000 == 0:
                #     print i
                sl_copol = band_copol.ReadRaster( 0, i, band_copol.XSize, 1, \
                           band_copol.XSize, 1, GDT_UInt16 )
                tof_copol = struct.unpack('H' * band_copol.XSize, sl_copol)
                vals_copol=N.array(tof_copol)
            if xflg == 1:
                sl_xpol = band_xpol.ReadRaster( 0, i, band_xpol.XSize, 1, \
                       band_xpol.XSize, 1, GDT_UInt16 )
                tof_xpol = struct.unpack('H' * band_xpol.XSize, sl_xpol)
                vals_xpol=N.array(tof_xpol)
            if nbands == 2:
                b=vals_copol-vals_xpol
            if coflg == 1:
                vals_copol[vals_copol>max_copol] = max_copol
                a[i,:,0]=(vals_copol-min_copol)*(256.0/(max_copol-min_copol))
            if xflg == 1:
                vals_xpol[vals_xpol>max_xpol] = max_xpol
                a[i,:,1]=(vals_xpol-min_xpol)*(256.0/(max_xpol-min_xpol))
            if nbands == 2:
                # f4b[i,:,0]=b
                tmpminb = N.min(b)
                tmpmaxb = N.max(b)
                if tmpminb < minb or i == 0:
                    minb = tmpminb
                if tmpmaxb > maxb or i == 0:
                    maxb = tmpmaxb
        
        # Create HH-HV band
        if nbands == 2:
            firstflg = 1
            for i in range(band_copol.YSize):
                # if i % 1000 == 0:
                #     print i
                sl_copol = band_copol.ReadRaster( 0, i, band_copol.XSize, 1, \
                       band_copol.XSize, 1, GDT_UInt16 )
                tof_copol = struct.unpack('H' * band_copol.XSize, sl_copol)
                vals_copol=N.array(tof_copol)
                sl_copol = band_copol.ReadRaster( 0, i, band_copol.XSize, 1, \
                       band_copol.XSize, 1, GDT_UInt16 )
                sl_xpol = band_xpol.ReadRaster( 0, i, band_xpol.XSize, 1, \
                       band_xpol.XSize, 1, GDT_UInt16 )
                tof_xpol = struct.unpack('H' * band_xpol.XSize, sl_xpol)
                vals_xpol=N.array(tof_xpol)
                b=vals_copol-vals_xpol
                if firstflg == 1:
                    testmin = N.min(b)
                    testmax = N.max(b)
                    firstflg = 0
                else:
                    if N.min(b) < testmin:
                        testmin = N.min(b)
                    if N.max(b) > testmax:
                        testmax = N.max(b)
                # b[b>max_co_x] = max_co_x
                # b[b<min_co_x] = min_co_x
                a[i,:,2] = (b-min_co_x)*(256.0/(max_co_x-min_co_x))
        
            a[:,:,2][a[:,:,0]==0] = 0

        # print testmin, testmax
        # print min_co_x, max_co_x

        format = "GTiff"
        driver = gdal.GetDriverByName( format )
        metadata = driver.GetMetadata()
        if nbands == 1:
            dst_filename = ("%s/%s/GeoTIFF/%s.tif" % (BASEDIR,area,rootfname))
            if coflg == 1:
                dst_ds = driver.Create( dst_filename, band_copol.XSize, band_copol.YSize, 1, 
                    gdal.GDT_Byte, [ 'COMPRESS=JPEG' ] )
            elif xflg == 1:
                dst_ds = driver.Create( dst_filename, band_xpol.XSize, band_xpol.YSize, 1, 
                    gdal.GDT_Byte, [ 'COMPRESS=JPEG' ] )
        elif nbands == 2:
            dst_filename = ("%s/%s/GeoTIFF/%s_rgb.tif" % (BASEDIR,area,rootfname))
            dst_ds = driver.Create( dst_filename, band_copol.XSize, band_copol.YSize, 3, 
                gdal.GDT_Byte, [ 'COMPRESS=JPEG' ] )
        print dst_filename
        
        dst_ds.SetGeoTransform( transform )    
        dst_ds.SetProjection( projstr )
        
        dst_ds.GetRasterBand(1).WriteArray( a[:,:,0] )
        if nbands == 2:
            dst_ds.GetRasterBand(2).WriteArray( a[:,:,1] )
            dst_ds.GetRasterBand(3).WriteArray( a[:,:,2] )

        # Once we're done, close properly the dataset
        dst_ds = None

        # Remove a array
        del a        

        # Write individual channel files
        # Warped images and bands are already open
        # Set up co-pol file
        # print 'Writing channel files'
        if coflg == 1:
            if nbands == 1:
                dst_copol_filename = ("%s/%s/GeoTIFF/%s_%s.tif" % (BASEDIR,area,rootfname,bandstr))
            elif nbands == 2:
                dst_copol_filename = ("%s/%s/GeoTIFF/%s_%s.tif" % (BASEDIR,area,rootfname,bandstr[0:2]))
            # print dst_copol_filename
            dst_co_ds = driver.Create( dst_copol_filename, band_copol.XSize, band_copol.YSize, 1, 
                gdal.GDT_Float32, [ 'COMPRESS=LZW' ] )
            # 'TILED=YES', 
            dst_co_ds.SetGeoTransform( transform )    
            dst_co_ds.SetProjection( projstr )
            f4b = N.zeros((band_copol.YSize,band_copol.XSize,1),N.float32)
            # Loop through values
            for i in range(band_copol.YSize):
                # if i % 1000 == 0:
                #     print i
                sl_copol = band_copol.ReadRaster( 0, i, band_copol.XSize, 1, \
                           band_copol.XSize, 1, GDT_UInt16 )
                tof_copol = struct.unpack('H' * band_copol.XSize, sl_copol)
                vals_copol=N.array(tof_copol)
                # print vals_copol.shape
                f4b[i,:,0]=vals_copol
            dst_co_ds.GetRasterBand(1).WriteArray( f4b[:,:,0] )
            # Close destination dataset
            dst_co_ds = None

        # Set up x-pol file
        if xflg == 1:
            dst_xpol_filename = ("%s/%s/GeoTIFF/%s_%s.tif" % (BASEDIR,area,rootfname,bandstr[2:4]))
            # print dst_xpol_filename
            dst_x_ds = driver.Create( dst_xpol_filename, band_xpol.XSize, band_xpol.YSize, 1, 
                gdal.GDT_Float32, [ 'COMPRESS=LZW' ] )
            # 'TILED=YES', 
            dst_x_ds.SetGeoTransform( transform )    
            dst_x_ds.SetProjection( projstr )
            f4b = N.zeros((band_copol.YSize,band_copol.XSize,1),N.float32)
            # Loop through values
            for i in range(band_copol.YSize):
                # if i % 1000 == 0:
                #     print i
                sl_xpol = band_xpol.ReadRaster( 0, i, band_xpol.XSize, 1, \
                       band_xpol.XSize, 1, GDT_UInt16 )
                tof_xpol = struct.unpack('H' * band_xpol.XSize, sl_xpol)
                vals_xpol=N.array(tof_xpol)
                f4b[i,:,0]=vals_xpol
            dst_x_ds.GetRasterBand(1).WriteArray( f4b[:,:,0] )
            # Close destination dataset
            dst_x_ds = None
        
        # Once we're done, close properly the input datasets
        if coflg == 1:
            co_inds = None
        if xflg == 1:
            x_inds = None

        # Remove f4b array
        del f4b

        # Delete temporary GeoTIFFs
        for fname in newgcp_list:
            # print fname
            os.remove(fname)
        for fname in warpfn:
            if fname != 'None':
                # print fname
                os.remove(fname)
                os.remove( ("%s.aux.xml" % fname) )

        # Generate pyramid layers
        inds = gdal.Open(dst_filename)
        s1band=inds.GetRasterBand(1)
        imagexsz = s1band.XSize
        imageysz = s1band.YSize
        inds = None
        xsize = imagexsz
        ysize = imageysz
        cmd = GDALHOME+"/bin/gdaladdo --config COMPRESS_OVERVIEW JPEG -r gauss -ro "+dst_filename+" 2 4 8 16 32 64 128 256 > /dev/null"
        # print cmd
        try:
            retcode = subprocess.call(cmd, shell=True)
            if retcode < 0:
                print >>sys.stderr, "Child was terminated by signal", -retcode
            # else:
            #     print >>sys.stderr, "Child returned", retcode
        except OSError, e:
            print >>sys.stderr, "Execution failed:", e

        # Generate pyramid layers for single channel images
        if coflg == 1:
            cmd = ("%s/bin/gdaladdo --config COMPRESS_OVERVIEW LZW -r gauss -ro %s 2 4 8 16 32 64 128 256 > /dev/null" \
                % (GDALHOME,dst_copol_filename))
            try:
                retcode = subprocess.call(cmd, shell=True)
            except OSError, e:
                print >>sys.stderr, "Execution failed:", e
        if xflg == 1:
            cmd = ("%s/bin/gdaladdo --config COMPRESS_OVERVIEW LZW -r gauss -ro %s 2 4 8 16 32 64 128 256 > /dev/null" \
                % (GDALHOME,dst_xpol_filename))
            try:
                retcode = subprocess.call(cmd, shell=True)
            except OSError, e:
                print >>sys.stderr, "Execution failed:", e


        # Generate Polygon WKT from GCP points
        wkt = 'POLYGON(('
        for i in range(nx):
            wkt = wkt + ('%f %f,' % (gcplon[0,i],gcplat[0,i]))
        for i in range(1,ny-1):
            wkt = wkt + ('%f %f,' % (gcplon[i,nx-1],gcplat[i,nx-1]))
        for i in range(nx-1,-1,-1):
            wkt = wkt + ('%f %f,' % (gcplon[ny-1,i],gcplat[ny-1,i]))
        for i in range(ny-2,0,-1):
            wkt = wkt + ('%f %f,' % (gcplon[i,0],gcplat[i,0]))
        wkt = wkt + ('%f %f))' % (gcplon[0,0],gcplat[0,0]))
        # print wkt

    # Delete the unpacked files
    if procstatus > -9010:
        # shutil.rmtree( ("%s/%s" % (extractdir,rootfname)) )
        shutil.rmtree( extractdir )

    # If we get here then we must have some form of procstatus value
    # Update database entry with new values
    tmpt=time.gmtime()
    procdtstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)	
    if procstatus > -9000:
        updatefstr = ("UPDATE s1_files SET processdt = \'%s\', " % procdtstr) + \
            ("procflg = %d, " % procstatus) + \
            ("coverage = ST_GeomFromText(\'%s\', 4326) " % wkt) + \
            ("WHERE id = %d;" % fid)
        proccount = proccount + 1
    else:
        updatefstr = ("UPDATE s1_files SET processdt = \'%s\', " % procdtstr) + \
            ("procflg = %d " % procstatus) + \
            ("WHERE id = %d;" % fid)
    print updatefstr
    queryres = con1.query(updatefstr)
    # print queryres

    # Break out of loop if proccount is equal or greater to PROCLIMIT
    if proccount >= PROCLIMIT:
        break

# Remove the lock file
os.remove( lockfn )

