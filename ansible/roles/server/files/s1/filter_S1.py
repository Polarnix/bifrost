#!/usr/bin/python

# Name:          filter_S1.py
# Purpose:       Filter out S1 files in a directory, removing duplicates with preference to Fast-24h (over NRT-3h)
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
from datetime import datetime

import pg

# Active flag turns deleting on/off
activeflg = 1

# Get command line input of directory to process
if len(sys.argv) == 2:
    procdir = sys.argv[1]
    # print procdir
    # Check directory exists
    if not os.path.exists(procdir):
        print ("Directory %s does not exist" % procdir)
        sys.exit()
else:
    sys.exit()

# Get list of files in directory
rawflist = os.listdir( procdir )

# Connect to satellite information database
dbcon = pg.connect(dbname='Satellite', host='localhost', user='istjenesten', passwd='svalbard')

# Basic filter, only process those files with a delivery timeliness in the database
validfn = []
validdt = []
validfid = []
validdelivery = []
for rawfn in rawflist:
    ftype = rawfn[-3:]
    # Only look at zip-files
    if ftype == 'zip':
        # Check information in database
        sql = ("SELECT id,datetime,delivery FROM s1files WHERE filename = \'%s\';" % rawfn)
        dbres = dbcon.query(sql).getresult()
        # print dbres
        if len(dbres) > 1:
            print ("Too many database records for %s" % rawfn)
            sys.exit()
        elif len(dbres) == 0:
            print ("No database record for %s" % rawfn)
            sys.exit()
        else:
            dbres = dbres[0]
            fid = int(dbres[0])
            dtstr = dbres[1]
            # print len(dtstr)
            if len(dtstr) > 19:
                dt = datetime.strptime(dtstr,'%Y-%m-%d %H:%M:%S.%f')
            else:
                dt = datetime.strptime(dtstr,'%Y-%m-%d %H:%M:%S')
            # print dt
            delivery = dbres[2]

        # Only process if delivery is Fast-24h or NRT-3h
        if delivery == 'Fast-24h' or delivery == 'NRT-3h':
            # print rawfn, delivery
            validfn.append( rawfn )
            validdt.append( dt )
            validfid.append(fid)
            validdelivery.append( delivery )

# Only process further if we have more than one file on the list
# print len(validfn)
if len(validfn) > 1:

    # Sort list on datetime
    # print validdt
    sortidx = sorted(range(len(validdt)), key=lambda k: validdt[k])
    # print sortidx

    # Loop through sorted list to create list of files to remove
    dellist = []
    for i in range(len(sortidx)):
        idx = sortidx[i]
        if i > 0:
            # Check to see if record is the same time as previous
            rawdelta = (validdt[idx] - olddt)
            # print dir(rawdelta)
            # print rawdelta.microseconds
            if rawdelta.microseconds == 0:
                delta = (rawdelta.days * 86400) + rawdelta.seconds
            else:
                delta = (rawdelta.days * 86400) + rawdelta.seconds + (1.0 / rawdelta.microseconds)
            # print ("%12.5f" % delta)

            # If delta is less than 5 seconds, see which file is NRT-3h
            if delta < 5.0:
                # print ("%12.5f" % delta), idx, oldidx
                if validdelivery[idx] == 'NRT-3h' and olddelivery == 'Fast-24h':
                    dellist.append( [validfn[idx], validdt[idx], validfid[idx], validdelivery[idx] ] )
                elif olddelivery == 'NRT-3h' and validdelivery[idx] == 'Fast-24h':
                    dellist.append( [oldfn, olddt, oldfid, olddelivery] )

        # Copy data to old
        oldfn = validfn[idx]
        olddt = validdt[idx]
        oldfid = validfid[idx]
        olddelivery = validdelivery[idx]
        oldidx = idx

    # Go through list and delete files
    for rec in dellist:
        fullfn = ("%s/%s" % (procdir,rec[0]))
        print fullfn
        if activeflg == 1:
            try:
                os.remove( fullfn )
            except:
                print ("Unable to remove %s" % fullfn)

