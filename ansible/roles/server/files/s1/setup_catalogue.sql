CREATE TABLE s1catalogue(
    id            int4,
    filename      varchar(80),
    datetime      timestamp,
    size          int,
    serverdt      timestamp,
    downloaddt    timestamp,
    downloadflg   smallint);
SELECT AddGeometryColumn('', 's1catalogue', 'coverage', 4326, 'POLYGON', 2);
CREATE INDEX sidx_s1catcover ON s1catalogue USING GIST (coverage);
ALTER TABLE s1catalogue ADD PRIMARY KEY (id);
GRANT ALL ON s1catalogue TO istjenesten;
CREATE SEQUENCE s1_refno START 1 CYCLE;
GRANT ALL ON s1_refno TO istjenesten;
