#!/usr/bin/python

# Name:          update_S1_planning.py
# Purpose:       Add Sentinel-1 planning information to database.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
from urllib2 import urlopen, URLError, HTTPError
from datetime import datetime

import pg
from BeautifulSoup import BeautifulSoup

import osgeo.ogr as ogr
import osgeo.osr as osr

import numpy as N

TMPDIR='/home/bifrostsat/tmp'


# Add more points to a polygon along the sides
# Formulae from Aviation Formulary (http://williams.best.vwh.net/avform.htm)
def addpolypoints( srs, polygeom ):
    # Extract outer ring points from polygon geometry
    ring = polygeom.GetGeometryRef(0)
    np = ring.GetPointCount()
    x = []
    y = []
    for i in range(np):
        x.append( N.radians(-ring.GetX(i)) )
        y.append( N.radians(ring.GetY(i)) )

    # Earth radius km
    erad = 6366.71

    # Numpy EPS
    eps = N.finfo(N.float).eps

    # Step in kilometres
    stepkm = 25.0

    # Create new ring geometry
    newring = ogr.Geometry(ogr.wkbLinearRing)
    for i in range(np-1):
        # Calculate distance
        d = 2.0 * N.arcsin( N.sqrt( N.power(N.sin((y[i]-y[i+1])/2.0), 2.0) + \
            N.cos(y[i]) * N.cos(y[i+1]) * N.power(N.sin((x[i]-x[i+1])/2.0), 2.0) ))
        dkm = d*erad
        # Calculate course between points
        if N.cos(y[i]) < eps:
            if y[i] > 0:
                tc1 = N.pi
            else:
                tc1 = 2.0*N.pi
        else:
            if N.sin(x[i+1]-x[i]) < 0:
                tc1 = N.arccos( (N.sin(y[i+1]) - N.sin(y[i]) * N.cos(d)) / (N.sin(d) * N.cos(y[i])) )    
            else:
                tc1 = 2.0 * N.pi - N.arccos( (N.sin(y[i+1]) - N.sin(y[i]) * N.cos(d)) \
                    / (N.sin(d) * N.cos(y[i])) )

        # print d, dkm, N.degrees(tc1)

        # New lon/lat given radial and distance
        newp = int(dkm/stepkm)
        for j in range(newp):
            newd = (float(j) * stepkm) / erad
            lat = N.arcsin( N.sin(y[i]) * N.cos(newd) + N.cos(y[i]) * N.sin(newd) * N.cos(tc1) )
            if N.cos(lat) ==0:
                lon = x[i]
            else:
                lon = N.mod( x[i] - N.arcsin( N.sin(tc1) * N.sin(newd) / N.cos(lat) ) + N.pi, \
                    2.0*N.pi) - N.pi
            # print '   ', newd, newd*erad, N.degrees(lon), N.degrees(lat)

            # Add points to ring
            newring.AddPoint( -N.degrees(lon), N.degrees(lat) )

    # Close ring
    newring.CloseRings()

    # Add ring to a new polygon geometry
    newpoly = ogr.Geometry(ogr.wkbPolygon)
    newpoly.AddGeometry(newring)

    return newpoly


# Read ESA S1 KML
def read_kml( kmlfn ):

    # Define a root filename
    rootfn = (kmlfn.split('/')[-1])[:-4]
    
    # Set up OGR drivers
    kmldrv = ogr.GetDriverByName("LIBKML")
    
    # Open KML file and get basic info
    inds = kmldrv.Open(kmlfn,0)
    # print dir(inds)
    nlayers = inds.GetLayerCount()

    # Lists for output data
    acqdt = []
    # timeliness = []
    mode = []
    coverage = []
    polarisation = []
    
    # Loop through layers and get layer and feature data
    for i in range(nlayers):
        inlay = inds.GetLayerByIndex(i)
        # print dir(inlay)
        inlay_name = inlay.GetName()
        inlay_nfeat = inlay.GetFeatureCount()
        inlay_srs = inlay.GetSpatialRef()
        # print inlay_name, inlay_nfeat, inlay_srs.ExportToProj4()
        infeatdefn = inlay.GetLayerDefn()
        # print dir(infeatdefn)

        # Get field definitions
        infield_names = []
        infield_types = []
        infield_widths = []
        infield_precs = []
        nfields = infeatdefn.GetFieldCount()
        for j in range(nfields):
            fielddefn = infeatdefn.GetFieldDefn(j)
            # print dir(fielddefn)
            infield_names.append( fielddefn.GetName() )
            infield_types.append( fielddefn.GetTypeName() )
            infield_widths.append( fielddefn.GetWidth() )
            infield_precs.append( fielddefn.GetPrecision() )
        # print infield_names
    
        # Loop through features and get geometry and field data
        if inlay_nfeat > 0:
    
            for j in range(inlay_nfeat):
                infeat = inlay.GetFeature(j+1)
                fieldvals = []
                for k in range(len(infield_names)):
                    tmpval = infeat.GetField(infield_names[k])
                    if tmpval != None:
                        if infield_types[k] == 'Integer':
                            fieldvals.append( int(tmpval) )
                        if infield_types[k] == 'String':
                            fieldvals.append( tmpval )
                        elif infield_types[k] == 'DateTime':
                            if tmpval.find('T') == -1:
                                fieldvals.append( datetime.strptime(tmpval,"%Y/%m/%d %H:%M:%S") )
                            else:
                                fieldvals.append( datetime.strptime(tmpval,"%Y-%m-%dT%H:%M:%S") )
                        else:
                            fieldvals.append( tmpval )
                    else:
                        fieldvals.append('None')
                # print fieldvals
    
                # Get geometry. This is a LINEARRING, so make this into a POLYGON
                geometry = infeat.GetGeometryRef()
                polygon = ogr.Geometry(ogr.wkbPolygon)
                polygon.AddGeometry(geometry)
                # print polygon.ExportToWkt()

                # Polygon points are rather sparse, so add more
                newpoly = addpolypoints( inlay_srs, polygon )
                newpoly.FlattenTo2D()
    
                # Add data to lists
                acqdt.append( fieldvals[3] )
                # timeliness.append( fieldvals[17] )
                mode.append( fieldvals[16] )
                coverage.append( newpoly )
                polarisation.append( fieldvals[18] )
                # print acqdt, mode, coverage, polarisation
    
            # print acqdt
            # print timeliness
            # print mode
            # print coverage
    
    # Close input file
    inds.Destroy()

    # print len(acqdt), len(timeliness), len(mode), len(coverage)
    # print len(acqdt), len(mode), len(coverage), len(polarisation)

    return [ acqdt, mode, polarisation, coverage ]


# Main program routine
if __name__ == '__main__':

    # Open database connection
    dbcon = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')

    # Get list of files already processed
    sql = 'SELECT DISTINCT(filename) FROM s1planning;'
    res = dbcon.query(sql).getresult()
    proclist = []
    for rec in res:
        proclist.append(rec[0])

    # Try to open HTTP connection to ESA server that hosts the files
    url1 = 'https://sentinel.esa.int/web/sentinel/missions/sentinel-1/observation-scenario/acquisition-segments'
    # url1 = 'https://sentinel.esa.int/web/sentinel/missions/sentinel-1/observation-scenario/acquisition-segments/archive'
    try:
        html_page = urlopen(url1)
    except URLError as e:
        print e.reason
        sys.exit()
    soup = BeautifulSoup(html_page)
    linklist = []
    for link in soup.findAll('a'):
        linkstr = str(link.get('href'))
        if linkstr.find('/documents/') > -1 and \
            (linkstr.find('Sentinel-1A_MP') > -1 or linkstr.find('Sentinel-1B_MP') > -1):
            # print linkstr
            linklist.append(linkstr)

    # Loop through list and get the files that have not been added to database
    for linkstr in linklist:

        # KML filename
        linkbits = linkstr.split('/')
        kmlfn = ("%s_TIMELINESS.kml" % linkbits[-1])

        # See if file is processed
        procflg = 0
        for testfn in proclist:
            if kmlfn == testfn:
                procflg = 1
        # print procflg

        # Only process if procflg = 0
        if procflg == 0:
            print kmlfn

            # Download file
            url2 = ("https://sentinel.esa.int%s" % linkstr)
            localfn = ("%s/%s" % (TMPDIR,kmlfn))
            try:
                inp = urlopen(url2)
                with open( localfn, "wb" ) as local_file:
                    local_file.write(inp.read())
            except HTTPError, e:
                print "HTTP Error:", e.code, url
                sys.exit()
            except URLError, e:
                print "URL Error:", e.reason, url
                sys.exit()

            # Decode file contents
            [ acqdt, mode, polarisation, coverage ] = read_kml( localfn )

            # Remove the KML-file
            os.remove(localfn)

            # Add records to database
            nrec = len(acqdt)
            for i in range(nrec):
                sql = "INSERT INTO s1planning (filename,datetime,mode,polarisation,coverage) VALUES"
                sql = ("%s (\'%s\',\'%s\',\'%s\',\'%s\',ST_GeomFromText(\'%s\',4326));" \
                    % (sql,kmlfn,acqdt[i].strftime('%Y-%m-%d %H:%M:%S'),mode[i],polarisation[i], \
                    coverage[i].ExportToWkt()))
                # print sql
                res = dbcon.query(sql)

