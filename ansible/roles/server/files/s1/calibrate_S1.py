#!/usr/bin/python

# Name:          calibrate_S1.py
# Purpose:       Produce GeoTIFFs of calibrated values from Sentinel-1 data.
#                NB: Dateline correction does not work with GDAL 1.10.1
#                GDAL 2.0.0 installation on pc4533 requires the following -
#                  export LD_LIBRARY_PATH=/usr/local/gdal-2.0.0/lib:$LD_LIBRARY_PATH
#                  export PYTHONPATH=/usr/local/lib/python2.7/dist-packages
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


import os, sys
import shutil
import argparse

import bz2, zipfile

import subprocess
import xml.etree.ElementTree as ET

import osgeo.gdal as gdal
import osgeo.gdal_array as gdal_array
import osgeo.ogr as ogr
import osgeo.osr as osr

import numpy as N
import scipy.interpolate as interp


TMPDIR = '/disk2/istjenesten_projects/IAW5_Satellite_Data/tmp'
GDALHOME = '/usr'
# GDALHOME = '/usr/local/gdal-2.0.0'

# Determine product type (product timeliness = NRT-3h or Fast-24h)
def get_type( xmlroot ):
    # Default value of timeliness is None
    timeliness = None

    # Search XML-tree for timeliness value
    for metadataobj in xmlroot.iter(tag='metadataObject'):
        if metadataobj.attrib['ID'] == 'generalProductInformation':
            for elem in metadataobj.iter():
                if (elem.tag).find('productTimelinessCategory') != -1:
                    timeliness = elem.text

    return timeliness


# Determine received channels in the file
def get_channels( xmlroot ):
    # Default value of channels is None
    channels = []

    # Search XML-tree for transmitterReceiverPolarisation values
    for metadataobj in xmlroot.iter(tag='metadataObject'):
        if metadataobj.attrib['ID'] == 'generalProductInformation':
            for elem in metadataobj.iter():
                if (elem.tag).find('transmitterReceiverPolarisation') != -1:
                    channels.append( elem.text )

    channels = sorted(channels)

    return channels


# Get filenames associated with the product
def get_filelist( xmlroot, idname ):

    # Blank list
    filelist = []

    # Search XML-tree for relevant ID name
    for dataobj in xmlroot.iter(tag='dataObject'):
        if dataobj.attrib['repID'] == idname:
            for elem in dataobj.iter(tag='fileLocation'):
                filelist.append( elem.attrib['href'] )

    return filelist


# Get data for calibration type ('sigmaNought', 'betaNought', 'gamma', 'dn')
def get_calibration( xmlroot, caltype ):

    # Lists for data
    lines = []
    pixels = []
    sigma0 = []

    for calvect in xmlroot.iter(tag='calibrationVector'):
        lines.append( int((calvect.find('line')).text) )
        pixtext = calvect.find('pixel').text
        tmppix = pixtext.split(' ')
        tmppix = map(int,tmppix)
        pixels.append( tmppix )
        sig0text = calvect.find(caltype).text
        tmpsig0 = sig0text.split(' ')
        tmpsig0 = map(float,tmpsig0)
        sigma0.append( tmpsig0 )

    # Bilinear interpolation by scipy
    x = N.array(pixels[0], dtype=N.float32)
    y = N.array(lines, dtype=N.float32)
    z = (N.array(sigma0, dtype=N.float32)).T
    # print x.shape,y.shape,z.shape
    bifunc = interp.RectBivariateSpline(x,y,z)

    return [ lines, pixels, sigma0, bifunc ]


# Get data for de-noise type ('noiseLut')
def get_denoise( xmlroot ):

    # Lists for data
    lines = []
    pixels = []
    sigma0 = []

    for calvect in xmlroot.iter(tag='noiseVector'):
        lines.append( int((calvect.find('line')).text) )
        pixtext = calvect.find('pixel').text
        tmppix = pixtext.split(' ')
        tmppix = map(int,tmppix)
        pixels.append( tmppix )
        sig0text = calvect.find('noiseLut').text
        tmpsig0 = sig0text.split(' ')
        tmpsig0 = map(float,tmpsig0)
        sigma0.append( tmpsig0 )

    # Bilinear interpolation by scipy
    nlines = len(lines)
    xlist = []
    ylist = []
    zlist = []
    for i in range(nlines):
        nx = len(pixels[i])
        xlist.extend(pixels[i])
        ytmp = [lines[i]] * nx
        ylist.extend(ytmp)
        zlist.extend(sigma0[i])
    # print len(xlist), len(ylist), len(zlist)

    x = N.array(xlist, dtype=N.float32)
    y = N.array(ylist, dtype=N.float32)
    z = N.array(zlist, dtype=N.float32)
    # print x.shape,y.shape,z.shape
    # print "Calculating spline..."
    # See http://docs.scipy.org/doc/scipy-0.17.0/reference/generated/scipy.interpolate.SmoothBivariateSpline.html#scipy.interpolate.SmoothBivariateSpline
    bifunc = interp.SmoothBivariateSpline(x,y,z)
    # print "  Done."

    return [ lines, pixels, sigma0, bifunc ]


# Check channel filenames are consistent
def filename_consistency( channel, calxmlfn, geoxmlfn, tiffn ):
    chtxt = channel.lower()
    chlist = [ (calxmlfn.split('-'))[4], \
               (geoxmlfn.split('-'))[3], \
               (tiffn.split('-'))[3] ]
    # print chtxt, chlist

    count = 0
    for testch in chlist:
        if chtxt == testch:
            count = count +1 

    if count == 3:
        status = True
    else:
        status = False

    return status


# Calibrate image using supplied calibration function
def calibrate_image( inparch, inpfn, lines, bifunc, dbflg ):
    # Construct root file name
    rootfn = (inpfn.split('/')[-1])[:-5]

    # Extract zip-file content to temporary tif-file
    tmpfn = ("%s/%s.tiff" % (TMPDIR,rootfn))
    source = inparch.open( inpfn )
    target = file(tmpfn,"wb")
    shutil.copyfileobj( source, target )
    target.close()
    source.close()

    # GDAL GeoTIFF driver
    driver = gdal.GetDriverByName('GTiff')

    # Get input image parameters
    inds = gdal.Open(tmpfn)
    cols = inds.RasterXSize
    rows = inds.RasterYSize
    # print cols, rows
    inband = inds.GetRasterBand(1)

    # Output image
    fulloutfn = ("%s/%s_cal.tif" % (TMPDIR,rootfn))
    outds = driver.Create(fulloutfn, cols, rows, 1, gdal.GDT_Float32)
    gdal_array.CopyDatasetInfo( inds, outds, xoff=0, yoff=0 )
    outband = outds.GetRasterBand(1)

    # Check for Date Line (180E/180W)
    gcps = inds.GetGCPs()
    firstgcp = 1
    dateline = 0
    for gcp in gcps:
        if firstgcp == 1:
            minlon = gcp.GCPX
            maxlon = gcp.GCPX
        else:
            if gcp.GCPX > maxlon:
                maxlon = gcp.GCPX
            if gcp.GCPX < minlon:
                minlon = gcp.GCPX
        firstgcp = 0
    if minlon < -90 and maxlon > 90:
        dateline = 1
    # Correct longitude/latitudes for date line
    if dateline == 1:
        new_gcps = []
        for gcp in gcps:
            ngcp = gdal.GCP()
            if gcp.GCPX < 0:
                ngcp.GCPX = gcp.GCPX + 360.0
            else:
                ngcp.GCPX = gcp.GCPX
            ngcp.GCPY = gcp.GCPY
            ngcp.GCPZ = gcp.GCPZ
            ngcp.GCPPixel = gcp.GCPPixel
            ngcp.GCPLine = gcp.GCPLine
            new_gcps.append(ngcp)
        outds.SetGCPs( new_gcps, inds.GetGCPProjection() )

    # Copy image data and calibrate
    xi = N.arange(0,cols)
    for i in range(len(lines)-1):
        yoff = lines[i]
        if yoff < rows:
            ycount = lines[i+1] - lines[i]
            if lines[i+1] > rows:
                ycount = (rows - lines[i])
            # print yoff, ycount, yoff + ycount
            data = inband.ReadAsArray(0,yoff,cols,ycount).astype(N.float32)

            yi = N.arange(0,ycount)+yoff
            xmesh,ymesh = N.meshgrid(xi,yi)
            # print xmesh.shape, ymesh.shape

            calval = bifunc(xi,yi)
            # print sigval.shape, data.shape
            data = (data * data) / (calval.T * calval.T)

            idx = N.nonzero( data != 0.0 )

            # Next line converts values to dB
            if dbflg == 1:
                data[idx] = 10.0 * N.log10(data[idx])

            y1 = yoff
            y2 = yoff + ycount
            # print y1, y2, ycount, data.shape, N.min(data), N.max(data)
            outband.WriteArray(data,0,yoff)
            outband.FlushCache()

    # Close image
    inds = None
    outds = None

    # Remove extracted file
    os.remove(tmpfn)

    return fulloutfn


# Calibrate image using supplied calibration function
def calibrate_denoise_image( inparch, inpfn, lines, bifunc, noisebifunc, dbflg ):
    # Construct root file name
    rootfn = (inpfn.split('/')[-1])[:-5]

    # Extract zip-file content to temporary tif-file
    tmpfn = ("%s/%s.tiff" % (TMPDIR,rootfn))
    source = inparch.open( inpfn )
    target = file(tmpfn,"wb")
    shutil.copyfileobj( source, target )
    target.close()
    source.close()

    # GDAL GeoTIFF driver
    driver = gdal.GetDriverByName('GTiff')

    # Get input image parameters
    inds = gdal.Open(tmpfn)
    cols = inds.RasterXSize
    rows = inds.RasterYSize
    # print cols, rows
    inband = inds.GetRasterBand(1)

    # Output image
    fulloutfn = ("%s/%s_cal.tif" % (TMPDIR,rootfn))
    outds = driver.Create(fulloutfn, cols, rows, 1, gdal.GDT_Float32, \
        options=["BIGTIFF=YES"])
    gdal_array.CopyDatasetInfo( inds, outds, xoff=0, yoff=0 )
    outband = outds.GetRasterBand(1)

    # Check for Date Line (180E/180W)
    gcps = inds.GetGCPs()
    firstgcp = 1
    dateline = 0
    for gcp in gcps:
        if firstgcp == 1:
            minlon = gcp.GCPX
            maxlon = gcp.GCPX
        else:
            if gcp.GCPX > maxlon:
                maxlon = gcp.GCPX
            if gcp.GCPX < minlon:
                minlon = gcp.GCPX
        firstgcp = 0
    if minlon < -90 and maxlon > 90:
        dateline = 1
    # Correct longitude/latitudes for date line
    if dateline == 1:
        new_gcps = []
        for gcp in gcps:
            ngcp = gdal.GCP()
            if gcp.GCPX < 0:
                ngcp.GCPX = gcp.GCPX + 360.0
            else:
                ngcp.GCPX = gcp.GCPX
            ngcp.GCPY = gcp.GCPY
            ngcp.GCPZ = gcp.GCPZ
            ngcp.GCPPixel = gcp.GCPPixel
            ngcp.GCPLine = gcp.GCPLine
            new_gcps.append(ngcp)
        outds.SetGCPs( new_gcps, inds.GetGCPProjection() )

    # Copy image data and calibrate
    xi = N.arange(0,cols)
    for i in range(len(lines)-1):
        yoff = lines[i]
        if yoff < rows:
            ycount = lines[i+1] - lines[i]
            if lines[i+1] > rows:
                ycount = (rows - lines[i])
            # print yoff, ycount, yoff + ycount
            data = inband.ReadAsArray(0,yoff,cols,ycount).astype(N.float32)

            yi = N.arange(0,ycount)+yoff
            xmesh,ymesh = N.meshgrid(xi,yi)
            # print xmesh.shape, ymesh.shape

            calval = bifunc(xi,yi)
            noiseval = noisebifunc(xi,yi)
            # print calval.shape, data.shape, noiseval.shape
            # print ("calval   %12.6f %8.2f" % (N.min(calval), N.max(calval)))
            # print ("noiseval %12.6f %8.2f" % (N.min(noiseval), N.max(noiseval)))
            data = ((data * data)-noiseval.T) / (calval.T * calval.T)
            # print ("data     %12.6f %8.2f" % (N.min(data), N.max(data)))

            idx = N.nonzero( data != 0.0 )

            # Next line converts values to dB
            if dbflg == 1:
                data[idx] = 10.0 * N.log10(data[idx])
            # print ("data db  %12.6f %8.2f\n" % (N.min(data), N.max(data)))

            y1 = yoff
            y2 = yoff + ycount
            # print y1, y2, ycount, data.shape, N.min(data), N.max(data)
            outband.WriteArray(data,0,yoff)
            outband.FlushCache()

    # Close image
    inds = None
    outds = None

    # Remove extracted file
    os.remove(tmpfn)

    return fulloutfn


# Warp to polar stereographic
def warp_to_pstereo( inpfn, rootfn, timeliness, channel ):
    # Construct output file name
    psfn = ("%s_%s_%s_pstereo.tif" % (rootfn[:-5],timeliness,channel))
    # print psfn

    # Resolution dependent on mode (EW=50, IW=20)
    imgmode = rootfn.split('_')[1]
    if imgmode == 'EW':
        imgres = 40
    elif imgmode == 'IW':
        imgres = 10
    else:
        print ("Unknown imaging mode: %s" % imgmode)
        imgres = 200
    # print imgres

    # Check to see if north or south
    # print inpfn
    inds = gdal.Open(inpfn)
    ngcp = inds.GetGCPCount()
    gcps = inds.GetGCPs()
    # print len(gcps)
    # for gcp in gcps:
    #     print gcp.GCPX, gcp.GCPY
    latitude = gcps[0].GCPY
    # print latitude
    inds = None
    # Check to see if dateline correction has been applied to GCPs
    dateline = 0
    firstgcp = 1
    for gcp in gcps:
        if firstgcp == 1:
            minlon = gcp.GCPX
            maxlon = gcp.GCPX
        else:
            if gcp.GCPX < minlon:
                minlon = gcp.GCPX
            if gcp.GCPX > maxlon:
                maxlon = gcp.GCPX
        firstgcp = 0
    # if minlon < -90 and maxlon > 90:
    #     dateline = 1
    if maxlon > 180.0:
        dateline = 1
    # print maxlon, dateline

    # Set Proj4 string
    if latitude >= 0:
        projstr = '+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84'
    else:
        projstr = '+proj=stere +lat_0=90s +lon_0=0e +lat_ts=90s +ellps=WGS84 +datum=WGS84'

    # Use gdalwarp to reproject the image
    if dateline == 0:
        cmd = ("%s/bin/gdalwarp -of GTiff -co \"BIGTIFF=YES\"" % GDALHOME)
    elif dateline == 1:
        cmd = ("%s/bin/gdalwarp --config CENTER_LONG 180 -of GTiff" % GDALHOME)
    cmd = ("%s -t_srs \"%s\"" % (cmd,projstr))
    cmd = ("%s -srcnodata 0.0 -dstnodata -1.0" % cmd)
    cmd = ("%s -tr %d %d -order 3 -r bilinear -overwrite -q" % (cmd,imgres,imgres))
    cmd = ("%s %s %s" % (cmd,inpfn,psfn))
    # print cmd
    try:
        retcode = subprocess.call(cmd, shell=True)
        if retcode < 0:
            print >>sys.stderr, "Child was terminated by signal", -retcode
    except OSError, e:
        print >>sys.stderr, "Execution failed:", e

    return psfn


# Open Sentinel-1 product archive file
def open_archive( inpfn ):
    # Open bzipped-file zip-file object
    if inpfn[-3:] == 'bz2':
        inparch = zipfile.ZipFile( bz2.BZ2File(inpfn,'r'), 'r')
    elif inpfn[-3:] == 'zip':
        inparch = zipfile.ZipFile( inpfn, 'r')
    else:
        print 'Unrecognised archive format. Stopping.'
        sys.exit()
    flist = inparch.namelist()
    # print flist
    for fname in flist:
        if fname.find('manifest.safe') > -1:
            manifest = fname
    rootfn = (manifest.split('/'))[0]

    return [ inparch, rootfn, manifest ]


# Get geolocation data
def get_geoinfo( xmlroot ):

    # Lists for data
    az_times = []
    rng_times = []
    lines = []
    pixels = []
    latitudes = []
    longitudes = []
    heights = []
    incangs = []
    elevangs = []

    # Get image size
    for imginfo in xmlroot.iter(tag='imageInformation'):
        npixels = int(imginfo.find('numberOfSamples').text)
        nlines = int(imginfo.find('numberOfLines').text)
    # print nlines, npixels

    # Get incidence angle data
    for geovect in xmlroot.iter(tag='geolocationGridPointList'):
        nval = int(geovect.attrib['count'])
        count = 0
        for geogp in geovect.iter(tag='geolocationGridPoint'):
            az_times.append( geogp.find('azimuthTime').text )
            rng_times.append( geogp.find('slantRangeTime').text )

            lines.append( int(geogp.find('line').text) )
            pixels.append( int(geogp.find('pixel').text) )

            latitudes.append( float(geogp.find('latitude').text) )
            longitudes.append( float(geogp.find('longitude').text) )
            heights.append( float(geogp.find('height').text) )

            incangs.append( float(geogp.find('incidenceAngle').text) )
            elevangs.append( float(geogp.find('elevationAngle').text) )

            count = count + 1
        # print nval, count

    # Check for Date Line (180E/180W)
    dateline = 0
    for i in range(count):
        if i == 0:
            minlon = longitudes[i]
            maxlon = longitudes[i]
        else:
            if longitudes[i] > maxlon:
                maxlon = longitudes[i]
            if longitudes[i] < minlon:
                minlon = longitudes[i]
    # print minlon, maxlon
    if minlon < -90 and maxlon > 90:
        dateline = 1
    # print dateline
    # Correct longitude/latitudes for date line
    if dateline == 1:
        for i in range(count):
            if longitudes[i] < 0:
                longitudes[i] = longitudes[i] + 360.0
    # print N.min(longitudes), N.max(longitudes)

    # Get list of lines and pixels, and reshape incidence angle to array
    y = N.unique(lines)
    x = N.unique(pixels)
    ny = len(y)
    nx = len(x)
    z = N.reshape(incangs,(ny,nx)).T

    # Bilinear interpolation by scipy
    # print x.shape,y.shape,z.shape
    bifunc = interp.RectBivariateSpline(x,y,z)

    return [ nlines, npixels, x, y, z, bifunc, [ lines, pixels, longitudes, latitudes ] ]


# Generate an incidence angle image
def incidence_angle_image( rootfn, timeliness, rows, cols, geolines, incabifunc, gcpdata ):

    # Unpack gcp data
    gcpy = gcpdata[0]
    gcpx = gcpdata[1]
    gcplong = gcpdata[2]
    gcplat = gcpdata[3]

    # GDAL GeoTIFF driver
    driver = gdal.GetDriverByName('GTiff')

    # Output image
    incafn = ("%s/%s_incidence.tif" % (TMPDIR,rootfn[:-5]))
    # print incafn
    outds = driver.Create(incafn, cols, rows, 1, gdal.GDT_Float32)
    outband = outds.GetRasterBand(1)

    # print rows, cols
    # print geopixels

    # Add incidence angle data
    xi = N.arange(0,cols)
    for i in range(len(geolines)-1):
        yoff = geolines[i]
        if yoff < rows:
            ycount = geolines[i+1] - geolines[i]
            if geolines[i+1] > rows:
                ycount = (rows - geolines[i])
            # print yoff, ycount, yoff + ycount

            yi = N.arange(0,ycount)+yoff
            xmesh,ymesh = N.meshgrid(xi,yi)
            # print xmesh.shape, ymesh.shape

            incaval = incabifunc(xi,yi).T
            # print incaval.shape

            y1 = yoff
            y2 = yoff + ycount
            # print y1, y2, ycount, incaval.shape, N.min(incaval), N.max(incaval)
            outband.WriteArray(incaval,0,yoff)
            outband.FlushCache()

    # Set projection
    outsrs = osr.SpatialReference()
    outsrs.ImportFromEPSG(4326)
    outds.SetProjection( outsrs.ExportToWkt() )

    # Create GCP points
    new_gcps = []
    for x,y,lon,lat in zip(gcpx,gcpy,gcplong,gcplat):
        ngcp = gdal.GCP()
        ngcp.GCPX = lon
        ngcp.GCPY = lat
        ngcp.GCPZ = 0.0
        ngcp.GCPPixel = x
        ngcp.GCPLine = y
        new_gcps.append( ngcp )
    outds.SetGCPs( new_gcps, outsrs.ExportToWkt() )

    # Close image
    inds = None
    outds = None

    return incafn


# Get the image footprint as a WKT string
def get_footprint( xmlroot ):

    # Search XML-tree for timeliness value
    for xmldataobj in xmlroot.iter(tag='xmlData'):
        for elem in xmldataobj.iter():
             if (elem.tag).find('coordinates') != -1:
                 coordtxt = elem.text
    # print coordtxt

    # Construct ring
    ring = ogr.Geometry(ogr.wkbLinearRing)
    yxpairs = coordtxt.split(' ')
    for yxpair in yxpairs:
        yxvals = yxpair.split(',')
        ring.AddPoint( float(yxvals[1]), float(yxvals[0]) )
    ring.CloseRings()

    # Add ring to the polygon
    polygon = ogr.Geometry(ogr.wkbPolygon)
    polygon.AddGeometry(ring)

    # Generate WKT string got footprint
    footprint = polygon.ExportToWkt()
    # print footprint

    return footprint


# Generate a calibrated image if supplied with file name and dbflg
def calibrate_S1( inpfn, dbflg=0 ):

    # Open bzipped-file zip-file object
    [ inparch, rootfn, manifest ] = open_archive( inpfn )

    # Parse manifest.safe file and get xml-file and data file names
    maniroot = ET.parse( inparch.open(manifest,'r') ).getroot()
    # manifest = 'image/S1A_EW_GRDM_1SDH_20150720T065604_20150720T065708_006891_0094EB_6550.SAFE/manifest.safe'
    # maniroot = ET.parse( manifest ).getroot()

    timeliness = get_type( maniroot )
    channels = get_channels( maniroot )
    calxmllist = get_filelist( maniroot, 's1Level1CalibrationSchema' )
    noisexmllist = get_filelist( maniroot, 's1Level1NoiseSchema' )
    geoxmllist = get_filelist( maniroot, 's1Level1ProductSchema' )
    datalist = get_filelist( maniroot, 's1Level1MeasurementSchema' )
    # Get polygon coverage as WKT
    # footprint = get_footprint( maniroot )
    # print footprint

    # print rootfn
    # print timeliness
    # print channels

    # Loop through dataset channels
    firstflg = 1
    for channel, calxmlfn, noisexmlfn, geoxmlfn, tiffn in zip(channels,calxmllist,noisexmllist,geoxmllist,datalist):
        if filename_consistency( channel, calxmlfn, geoxmlfn, tiffn ) == True:

            # On first run through, generate an incidence angle image
            if firstflg == 1:

                # Get geo-positioning information
                fullgeofn = ("%s/%s" % (rootfn,geoxmlfn[2:]))
                # print fullgeofn
                georoot = ET.parse( inparch.open(fullgeofn,'r') ).getroot()
                [ nlines, npixels, geopixels, geolines, incang, incabifunc, gcpdata ] = get_geoinfo( georoot )

                # Generate incidence angle image
                tmpincafn = incidence_angle_image( rootfn, timeliness, nlines, npixels, geolines, incabifunc, gcpdata )

                # Warp to polar stereographic
                incafn = warp_to_pstereo( tmpincafn, rootfn, timeliness, 'incidence' )

                # Remove temporary GeoTIFF
                os.remove(tmpincafn)

                # Now set firstflg to zero
                firstflg = 0

            # Get calibration
            fullcalfn = ("%s/%s" % (rootfn,calxmlfn[2:]))
            calroot = ET.parse( inparch.open(fullcalfn,'r') ).getroot()
            [ lines, pixels, sigma0, s0bifunc ] = get_calibration( calroot, 'sigmaNought' )

            # Get de-noise
            fullnoisefn = ("%s/%s" % (rootfn,noisexmlfn[2:]))
            print fullnoisefn
            noiseroot = ET.parse( inparch.open(fullnoisefn,'r') ).getroot()
            [ lines, pixels, sigma0, noisebifunc ] = get_denoise( noiseroot )

            # Calibrate image
            # fulltiffn = ("%s/%s" % (rootfn,tiffn[2:]))
            # tmpfn = calibrate_image( inparch, fulltiffn, lines, s0bifunc, dbflg )

            # Calibrate and denoise image
            fulltiffn = ("%s/%s" % (rootfn,tiffn[2:]))
            tmpfn = calibrate_denoise_image( inparch, fulltiffn, lines, s0bifunc, noisebifunc, dbflg )

            # Warp to polar stereographic
            outfn = warp_to_pstereo( tmpfn, rootfn, timeliness, channel )

            # Remove temporary GeoTIFF
            os.remove(tmpfn)


# Main program function
if __name__ == "__main__":

    # Get inputs using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="Sentinel-1 filename (.bz2 or .zip)", type=str)
    parser.add_argument("-d" "--dbflg", help="Ouput dB values [0=no,1=yes]", type=int, \
        choices=[0,1], default=0)
    args = parser.parse_args()
    inpfn = args.filename
    dbflg = args.d__dbflg

    # Call up the calibration routine
    calibrate_S1( inpfn, dbflg )


