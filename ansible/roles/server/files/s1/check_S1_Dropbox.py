#!/usr/bin/python

# Name:          check_S1_Dropbox.py
# Purpose:       Script to check Dropbox folder for Sentinel-1 files added by the analysts.
#                Interacts with PostgreSQL+PostGIS 'Satellite' database and 's1files'
#                table.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, string
import time, math
from subprocess import *

import pg

# System dependent settings
BASEDIR='/home/bifrostsat/Data/S1'
DATADIR=BASEDIR+'/Raw'
ANCDIR = '/home/bifrostsat/Ancillary/S1'
DROPBOX = '/home/bifrostsat/Dropbox'
INVALIDDIR=DROPBOX+'/invalid'

# Active flag allows processing
active=1

# File size limiter in Mb - set low (e.g. 100 for very slow computers,
#    large > 1024 if confident computer can handle everything
limit = 1000.0

# Image type
imgtypelist = [ 'S1A', 'S1B' ]
extntype = 'zip'

# Output the start time
tmpt=time.gmtime()
tout=( '*** check_S1_Dropbox.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file for slow computers
lockfn= ANCDIR + '/check_S1_Dropbox.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    fout = open(lockfn, 'w')
    fout.write(tout)
    fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Connect to 'Satellite' database for determining file status from 's1files' table
con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')

# Get list of files and times/sizes in Dropbox
filelist=os.listdir(DROPBOX)
filelist=sorted(filelist)
fsize=[]
fctime=[]
for fname in filelist:
    fpath=('%s/%s' % (DROPBOX,fname))
    fctime=fctime+[os.path.getctime(fpath)]
    fsize=fsize+[os.path.getsize(fpath)]
# print filelist
# print fctime
# print fsize
tmpt=time.gmtime(fctime[0])
tstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)
# print tstr

# Pause 5 seconds, allow for filesize checking
# time.sleep(5)

idx=0
count=0
disk=0
for fname in filelist:
    # Split the filename
    fnbits = fname.split('_')
    # print fnbits

    # Get filename type and extension
    typestr = fnbits[0]
    extnstr = fname[-3:]

    # Loop through potential image types and test
    for imgtype in imgtypelist:

        # Acquisition time (taken from file name)
        if (typestr == imgtype and extnstr == extntype):
            dtstr = fnbits[4]
            filedt=time.strptime(dtstr,"%Y%m%dT%H%M%S")
            acqt=time.mktime(filedt)
            # print dtstr, extnstr

            # print idx, acqt, fctime[idx]
            count=count+1
            disk = disk + string.atof(fsize[idx])

            # Check to see if image is already listed in the database
            # It will only be present in the database if it has already been
            # downloaded.
            index=0
            present=0
            inarea=0
            processed=0
            querytxt = ("SELECT id FROM s1_files WHERE filename = \'%s\';" % fname)
            # print querytxt
            queryres = con1.query(querytxt)
            try:
                fileid = queryres.getresult()[0][0]
            except:
                # Now query for next available id
                maxfileidres = con1.query('SELECT MAX(id) FROM s1_files;')
                maxfileid = maxfileidres.getresult()[0][0]
                if not maxfileid:
                    maxfileid = 0
                nextfileid = maxfileid + 1
                present = 0
            else:
                nextfileid = -1
                present = 1
            # print 'nextfileid = ', nextfileid
            # print 'Present? ', present

            # If the file is not found in the database, add the information to enable processing
            if present == 0:
                mbsize = string.atof(fsize[idx]) / 1024.0 / 1024.0
                tmpt=time.gmtime(fctime[idx])
                filetstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)
                tout = ( ' %5d ' % idx ) + ( '[%5.1fMb] ' % mbsize ) + filetstr
                print tout
                tmpt=time.gmtime()
                stime=time.mktime(tmpt)
        
                # Processing if activated and size check for very slow computer
                fstat = 0
                fpath=('%s/%s' % (DROPBOX,fname))
        
                # Check that the size is the same
                chksize=os.path.getsize(fpath)
                if fstat == 0 and fsize[idx] == chksize:
                    # Date/time strings
                    tmpt=time.gmtime()
                    tstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)			
                    filedtstr = time.strftime("%Y-%m-%d %H:%M:%S", filedt)

                # Insert file information into database
                insertfstr = ("INSERT INTO s1_files " + \
                    "(id, filename, location, datetime, size, serverdt, downloaddt, " + \
                    "processdt, procflg, " + \
                    "archivedt, archflg, deletedt, delflg, " + \
                    "polyanalysedt, polyflg, classifydt, classflg) " + \
                    "VALUES(nextval(\'s1_serial\')," + \
                    "\'%s\','Dropbox',\'%s\',%d,\'%s\',\'%s\',\'%s\',0,\'%s\',0,\'%s\',0,\'%s\',0,\'%s\',0);" % \
                    (fname,filedtstr,int(fsize[idx]),filetstr, \
                    filetstr,tstr,tstr,tstr,tstr,tstr))
                print insertfstr
                queryres = con1.query(insertfstr)
                # print queryres

    idx = idx + 1
                    
disk = disk / 1024 / 1024 / 1024
print ( '%3d recent files, ' % count ) + ( '%6.2fGb of space required.' % disk )

print ' '

# Remove the lock file
os.remove(lockfn)
