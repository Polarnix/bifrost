#!/usr/bin/python

# Name:          S1_archive.py
# Purpose:       Watchdog program to restart Sentinel-1 processing if it should stall for 
#                any reason
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

from stat import S_ISREG, ST_CTIME, ST_MODE
import os, sys
import time, smtplib
import signal

from datetime import datetime, date

LIMIT = 6

ANCDIR = '/home/bifrostsat/Ancillary/S1'

# Get list of lock files
extension = '.lock'
entries = (os.path.join(ANCDIR, fn) for fn in os.listdir(ANCDIR))
entries = ((os.stat(path), path) for path in entries)
entries = ((stat[ST_CTIME], path)
    for stat, path in entries if S_ISREG(stat[ST_MODE]))

# Get lists of processes
procinfo = []
for line in os.popen("ps xa"):
    procinfo = procinfo + [ line ]
# print procinfo

# Set up e-mail notification
cntrladdr='istjenesten@met.no'
recipients = [ 'nick.hughes@met.no', 'istjenesten@met.no' ]
# destaddr='nick.hughes@met.no, istjenesten@met.no'
HeaderText='From: '+cntrladdr+'\r\n'
HeaderText+='To: '+(", ".join(recipients))+'\r\n'
HeaderText+='Subject: Sentinel-1 Watchdog reports timeout\r\n\r\n'
MessageText='The Sentinel-1 watchdog has barked, and reports a timeout on the following:'
messflg = 0

curdt = datetime.utcnow()
print 'Watchdog: ', curdt

for cdate, path in sorted(entries):
    # dtstr = datetime.strftime('%Y%m%d%H%M%S',dt)
    # print curdt - dt
    fname = os.path.basename(path)
    
    if fname.endswith(extension):

        # Get file time
        dt = datetime.fromtimestamp(cdate)
        # print dt

        difference = curdt - dt
        hours = difference.seconds / 3600.0
        print ("%s %s %5.2f hours" % (dt,fname,hours))

        # If file is process_S1.lock, etc.
        proclist = [ 'process_S1', 'download_S1', 'check_S1_Dropbox', 'check_S1_rolling_archive', \
            'S1_archive' ]
        for procname in proclist:
            if fname.startswith(procname):

                # Count number of processes featuring procname
                nproc = 0
                for info in procinfo:
                    # print info
                    if procname in info:
                        nproc = nproc + 1

                # If nproc > 0, good!  Otherwise...
                # print 'here', nproc
                if nproc == 0:
                    print ("%s lock file, but no processes running" % procname)
                    MessageText+=("\n%s lock file, but no processes running" % procname)
 
                    # If processMO processes do not exist,
                    #     delete the lock file
                    fname= ANCDIR + '/' + fname
                    print fname
                    MessageText+=("\nDeleted the lock file, %s" % fname)
                    messflg = messflg + 1
                    status = os.remove(fname)
                    # But what to do about 'bad' files?
                
                
        # If file is check_S1_Dropbox lock file
        procnamelist = [ 'check_S1_Dropbox' ]
        for procname in procnamelist:
            if fname.startswith(procname):

                # If lock file is more than 3.0 hours old, assume some kind of stall
                limit = 3.0
                if hours > limit:
                    print fname

                    # Find processes with that name and terminate
                    for info in procinfo:
                        if procname in info:
                            fields = info.split()
                            pid = fields[0]
                            # Kill the process
                            print info
                            print 'Killed ', pid, procname
                            os.kill(int(pid), signal.SIGKILL)
                            
                    # Remove lock file
                    MessageText+=("\nTimeout on %s. Killed any processes and deleted the lock file %s" % (procname,fname))
                    messflg = messflg + 1
                    fname= ANCDIR + '/' + fname
                    print fname
                    status = os.remove(fname)

        # fin = open(fname,'r')



        # fin.close()

# If we barked, send an e-mail
if messflg > 0:
    email=smtplib.SMTP('smtp.troms.dnmi.no')
    email.sendmail(cntrladdr,recipients,HeaderText+MessageText)
    del email
