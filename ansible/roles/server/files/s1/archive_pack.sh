#!/bin/bash

# Name:          archive_pack.sh
# Purpose:       Shell script to pack (and remove NRT-3h duplicates) in Sentinel-1 archive folder.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

YESTERDAY=$(date --date="yesterday" +"%d")
MONTH=$(date --date="yesterday" +"%m")
YEAR=$(date --date="yesterday" +"%Y")
# echo $YESTERDAY $MONTH $YEAR

ARCHDIR='/vol/data/Sentinel-1/Archive'

# Create an archive folder
cd "${ARCHDIR}"
mkdir "${YESTERDAY}"

# Move files to folder
mv *_${YEAR}${MONTH}${YESTERDAY}T??????_${YEAR}* "${YESTERDAY}"

# Filter out duplicate files, with preference to Fast-24h
# echo "${ARCHDIR}/${YESTERDAY}"
/usr/bin/python filter_S1.py "${ARCHDIR}/${YESTERDAY}"

# Compress files
bzip2 ${YESTERDAY}/*.zip

exit

