#!/bin/bash

# Name:          hourly_update.sh
# Purpose:       Shell script to run Python Sentinel-1 processing scripts.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Script to run Sentinel-1 processing hourly, noted that ESA only updating the server once per hour
# Nick Hughes, 2014-x-4

WORKDIR='/home/istjenesten/software/Sentinel-1/Routine'
cd "${WORKDIR}"

# Maintain catalogue of S1 scenes available on ESA server
/usr/bin/python "${WORKDIR}/check_S1_rolling_archive.py" 1>> "${WORKDIR}/Logs/check_S1_rolling_archive.log" 2>> /dev/null

# Download files from ESA server, based on catalogue entries
/usr/bin/python "${WORKDIR}/download_S1.py" 1>> "${WORKDIR}/Logs/download_S1.log" 2>> /dev/null

# Process images to high quality GeoTIFF
/usr/bin/python "${WORKDIR}/process_S1.py" 1>> "${WORKDIR}/Logs/process_S1.log" 2>> /dev/null

exit

