#!/usr/bin/python

# Name:          S1_archive.py
# Purpose:       Move Sentinel-1 raw (.zip) files to archive once they have been processed.
#
#                delstatus codes:
#                    0 = Not processed
#                -9001 = Unable to compress raw file
#                -9002 = Unable to move compressed raw file to archive
#                    1 = Successful archiving of files.
#
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, shutil
import string, time

import numpy

import pg

# System dependent settings
BASEDIR = '/home/bifrostsat/Data/S1'
DATADIR = ("%s/Raw" % BASEDIR)
DROPBOX = '/home/bifrostsat/Dropbox'
ANCDIR = '/home/bifrostsat/Ancillary/S1'
ARCHIVEDIR = ("%s/Archive" % BASEDIR)

# Output the start time
tmpt=time.gmtime()
tout=( '*** S1_archive.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file for slow computers
lockfn = ANCDIR + '/S1_archive.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    fout = open(lockfn, 'w')
    fout.write(tout)
    fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Connect to database table containing Sentinel-1 file information
con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')
    
# Interrogate 'Satellite' database table 's1_files' to determine what 
# files need deleting.
queryresult = con1.query('SELECT id,filename,location,procflg FROM s1_files WHERE procflg > -9000 AND procflg != 0 AND archflg = 0;')
proclist = queryresult.getresult()
# print proclist

for item in proclist:
    fid = item[0]
    rootfname = item[1]
    location = item[2]
    procflg = item[3]
    print "Processing ", rootfname

    archstatus = -9999

    # Move the raw file to the archive directory
    if location == 'Dropbox':
        oldfname = ("%s/%s" % (DROPBOX,rootfname))
    else:
        oldfname = ("%s/%s/%s" % (BASEDIR,location,rootfname))
    newfname = ("%s/%s" % (ARCHIVEDIR,rootfname))
    # print oldfname
    # print newfname
    try:
        shutil.move(oldfname,newfname)
        archstatus = 1
    except:
        archstatus = -9002
        print "Execution failed:", archstatus

    # If we get here then we must have some form of archstatus value
    # Update database entry with new values
    tmpt=time.gmtime()
    archdtstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)	
    updatefstr = ("UPDATE s1_files SET archivedt = \'%s\', " % archdtstr) + \
        ("archflg = %d " % archstatus) + \
        ("WHERE id = %d;" % fid)
    print updatefstr
    queryres = con1.query(updatefstr)
    # print queryres

# Remove the lock file
os.remove(lockfn)
