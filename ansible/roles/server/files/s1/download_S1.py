#!/usr/bin/python

# Name:          download_S1.py
# Purpose:       Check Sentinel-1 catalogue in "Satellite" database, "s1_catalogue" table for available files 
#                and if these fall within the processing zones (see s1_arctic.shp and s1_antarctic.shp in the
#                include folder) download the data from the Rolling Archive at ESA
#
#                procflg results:
#                -9001 = Unable to connect to FTP server.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


import os, sys, shutil
import time
from datetime import date, datetime, timedelta

import pg

from ftplib import FTP
from zipfile import ZipFile, ZIP_DEFLATED
import bz2

import xml.etree.ElementTree as ET

import osgeo.ogr as ogr
import osgeo.osr as osr

from get_esa_cmems_credentials import get_esa_cmems_credentials
from check_data_storage import check_data_storage


# System dependent settings
BASEDIR = '/home/bifrostsat/Data/S1'
DATADIR = ("%s/Raw" % BASEDIR)
ANCDIR = '/home/bifrostsat/Ancillary/S1'
SETDIR='/home/bifrostsat/Settings'
INCDIR = '/home/bifrostsat/Include/S1'
INVALIDDIR = ("%s/Raw/invalid" % BASEDIR)
TMPDIR = ("%s/tmp" % BASEDIR)

# Active flag allows processing
activeflg = 1

# Latest string determines how far back in time to look
lateststr = '6 hours'
# lateststr = '5 days'

# Threshold (%) for processing if scene partially overlaps processing region
threshold = 25.0

# Size of file archive (Gb)
target_size = 10.0

# Output the start time
tmpt=time.gmtime()
tout=( '*** download_S1.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file for slow computers
lockfn= ANCDIR + '/download_S1.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    fout = open(lockfn, 'w')
    fout.write(tout)
    fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()


# Open Sentinel-1 product archive file
def open_archive( inpfn ):
    # Open bzipped-file zip-file object
    if inpfn[-3:] == 'bz2':
        inparch = ZipFile( bz2.BZ2File(inpfn,'r'), 'r')
    elif inpfn[-3:] == 'zip':
        inparch = ZipFile( inpfn, 'r')
    else:
        print 'Unrecognised archive format. Stopping.'
        sys.exit()
    flist = inparch.namelist()
    # print flist
    for fname in flist:
        if fname.find('manifest.safe') > -1:
            manifest = fname
    rootfn = (manifest.split('/'))[0]

    return [ inparch, rootfn, manifest ]


# Determine product type (product timeliness = NRT-3h or Fast-24h)
def get_type( xmlroot ):
    # Default value of timeliness is None
    timeliness = None

    # Search XML-tree for timeliness value
    # NB: getiterator rather than iter in Python 2.5
    for metadataobj in xmlroot.getiterator(tag='metadataObject'):
        if metadataobj.attrib['ID'] == 'generalProductInformation':
            for elem in metadataobj.getiterator():
                if (elem.tag).find('productTimelinessCategory') != -1:
                    timeliness = elem.text

    return timeliness


# Download a zipped S1 scene
def download_zip_S1( s1fn, s1dt ):

    # Try to get ESA CMEMS credentials for FTP server
    esa_cmems_fn = ("%s/esa_cmems_credentials.txt" % SETDIR)
    credentials = get_esa_cmems_credentials( esa_cmems_fn )

    # Active flag allows processing
    activeflg = 1

    # Processing status flag
    procflg = 0

    # Try to open connection to ESA FTP server
    if activeflg == 1:
        try:
            esaftp = FTP(host=credentials['ESA_PRIMARY_HOST'], \
                         user=credentials['ESA_PRIMARY_USER'], \
                         passwd=credentials['ESA_PRIMARY_PASSWD'], \
                         timeout=15)
        except:
            print 'Unable to connect to primary FTP server.'

            # Primary FTP server has failed, now try the backup
            try:
                esaftp = FTP(host=credentials['ESA_SECONDARY_HOST'], \
                             user=credentials['ESA_SECONDARY_USER'], \
                             passwd=credentials['ESA_SECONDARY_PASSWD'], \
                             timeout=60)

            except:
                print 'Unable to connect to secondary FTP server.'
                procflg = 9001

    # Only do further processing if procflg is zero
    if procflg < 9000:

        # Define base directory on FTP
        basepath = ("%4d/%02d/%02d" % (s1dt.year,s1dt.month,s1dt.day))
        # print basepath

        shortfn = ("%s.zip" % s1fn)
        print shortfn
        ftpfn = ("%s/%s" % (basepath,shortfn))
        # print ftpfn
        zipfn = ("%s/%s" % (DATADIR,shortfn))
        # print zipfn
        esaftp.retrbinary(("RETR %s" % ftpfn), open(zipfn, 'wb').write)

        # Close the ftp connection
        esaftp.quit()

        # Set status flag
        procflg = 1

    return [procflg, zipfn]


# Download a raw, unpacked S1 scene and create a zip-file package
def download_raw_S1( s1fn, s1dt ):

    # Try to get ESA CMEMS credentials for FTP server
    esa_cmems_fn = ("%s/esa_cmems_credentials.txt" % SETDIR)
    credentials = get_esa_cmems_credentials( esa_cmems_fn )

    # Subfolders to check in FTP server
    subfolders = [ '', 'annotation', 'annotation/calibration', 'measurement', \
        'preview', 'preview/icons', 'support' ]

    # Active flag allows processing
    activeflg = 1

    # Processing status flag
    procflg = 0

    # Try to open connection to ESA FTP server
    if activeflg == 1:
        try:
            esaftp = FTP(host=credentials['ESA_PRIMARY_HOST'], \
                         user=credentials['ESA_PRIMARY_USER'], \
                         passwd=credentials['ESA_PRIMARY_PASSWD'], \
                         timeout=15)
        except:
            print 'Unable to connect to primary FTP server.'

            # Primary FTP server has failed, now try the backup
            try:
                esaftp = FTP(host=credentials['ESA_SECONDARY_HOST'], \
                             user=credentials['ESA_SECONDARY_USER'], \
                             passwd=credentials['ESA_SECONDARY_PASSWD'], \
                             timeout=60)
            except:
                print 'Unable to connect to secondary FTP server.'
                procflg = 9001

    # Only do further processing if procflg is zero
    if procflg < 9000:

        # Create folder in temporary file area
        filedir = ("%s/%s" % (TMPDIR,s1fn))
        if os.path.isdir(filedir) == False:
            os.mkdir(filedir)

        # Define base directory on FTP
        basepath = ("%4d/%02d/%02d/%s" % (s1dt.year,s1dt.month,s1dt.day,s1fn))
        # print basepath

        # Loop through subfolders
        for folder in subfolders:
            # Create a path for the FTP server retrieval
            if len(folder) > 1:
                ftppath = ("%s/%s" % (basepath,folder))
                # Create a local folder if required
                if os.path.isdir( ("%s/%s" % (filedir,folder)) ) == False:
                    os.mkdir( ("%s/%s" % (filedir,folder)) )
            else:
                ftppath = basepath
            filelist = sorted( esaftp.nlst(ftppath) )
            # Loop through files and retrieve
            for i in range(len(filelist)):
                shortfn = filelist[i].replace(("%s/" % basepath),'')
                # print shortfn
                folderflg = 0
                for j in range(len(subfolders)):
                    if shortfn == subfolders[j]:
                        folderflg = 1
                if folderflg == 0:
                    localfn = ("%s/%s" % (filedir,shortfn))
                    # print localfn
                    esaftp.retrbinary(("RETR %s" % filelist[i]), open(localfn, 'wb').write)

        # Close the ftp connection
        esaftp.quit()

        # Create a zip-file
        s1fnbits = s1fn.split('.')
        # print s1fnbits
        zipfn = ("%s/%s.zip" % (DATADIR,s1fnbits[0]))
        print zipfn
        s1zip = ZipFile(zipfn,'w')
        for root, dirs, files in os.walk(filedir):
            for fname in files:
                localfn = ("%s/%s" % (root,fname))
                # print localfn
                archivefn = ("%s/%s" % (root.replace(("%s/" % TMPDIR),''),fname))
                # print archivefn
                s1zip.write(localfn,archivefn,ZIP_DEFLATED)
        s1zip.close()

        # Remove the temporary file tree
        shutil.rmtree(filedir)

        # Set status flag
        procflg = 1

    return [procflg, zipfn]


# Connect to 'Satellite' database for updating the 's1_catalogue' and 's1_files' tables
con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')

# Processing status flag
procflg = 0

# Load geographical processing regions
projstr = [ '+proj=stere +lat_0=90n +lat_ts=90n +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84', \
    '+proj=stere +lat_0=90s +lat_ts=90s +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84' ]
area = [ 's1_arctic', 's1_antarctic' ]
hem = [ 0, 1 ]
regions = []
regproj = []
reghems = []
shpdrv = ogr.GetDriverByName('ESRI Shapefile')
for i in range(len(area)):
    areafn = ("%s/%s.shp" % (INCDIR,area[i]))
    # print areafn
    areads = shpdrv.Open(areafn, 0)
    if areads is None:
        print 'Could not open ' + areafn
        sys.exit()
    layer = areads.GetLayer(0)
    # Get layer SRS and create conversion to standard projection
    areasrs = layer.GetSpatialRef()
    targsrs = osr.SpatialReference()
    targsrs.ImportFromProj4(projstr[i])
    coordtrans = osr.CoordinateTransformation(areasrs,targsrs)
    # Loop through features
    numfeat = layer.GetFeatureCount()
    # print numfeat
    for j in range(numfeat):
        feat = layer.GetFeature(j)
        geom = feat.GetGeometryRef()
        geom.Transform(coordtrans)
        # print geom.ExportToWkt()
        # Add geometries to list
        regions.append( geom.ExportToWkt() )
        regproj.append( projstr[i] )
        reghems.append( hem[i] )
        # Destroy feature and geometry
        feat.Destroy()
        # geom.Destroy()
    # Close processing region file
    areads.Destroy()
# print regions
# print regproj
nregion = len(regions)

# Get list of available scenes to check
sqltxt = "SELECT id,filename,datetime,ST_AsText(coverage),serverdt FROM s1_catalogue WHERE"
sqltxt = ("%s datetime > current_timestamp - interval \'%s\' AND downloadflg = 0" % (sqltxt,lateststr))
sqltxt = ("%s ORDER BY datetime DESC;" % sqltxt)
# print sqltxt
res = con1.query(sqltxt)
reslist = res.getresult()
# print reslist

# Set scene footprint SRS
scenesrs = osr.SpatialReference()
scenesrs.ImportFromEPSG(4326)

# Folders to ignore while calculating storage volume
ignorelist = [ '/tmp' ]

# Main Loop - Loop through scenes and check geographical coverage
nscene = len(reslist)
print ("%d scenes:" % nscene)
for i in range(nscene):

    # Only download if we have disk space
    if check_data_storage( BASEDIR, ignorelist ) < target_size:

        result = reslist[i]
        sceneid = result[0]
        scenefn = result[1]
        scenedt = datetime.strptime(result[2], '%Y-%m-%d %H:%M:%S.%f')
        scenewkt = result[3]
        serverdt = result[4]
        print ' '
        print sceneid, scenefn
        # Create coverage geometry
        scenegeom = ogr.CreateGeometryFromWkt( scenewkt )
        # Get scene centre latitude
        centroid = scenegeom.Centroid()
        scenelat = centroid.GetY()

        # Loop through processing regions and see if scene is within one
        for j in range(nregion):
            # Only process further if scene is in the same hemisphere
            if (scenelat > 30.0 and reghems[j] == 0) or \
                (scenelat < -30.0 and reghems[j] == 1):
                # Create region geometry
                regiongeom = ogr.CreateGeometryFromWkt( regions[j] )
                # Create a coordinate transformation
                targsrs = osr.SpatialReference()
                targsrs.ImportFromProj4(regproj[j])
                coordtrans = osr.CoordinateTransformation(scenesrs,targsrs)
                # Create working geometry
                workinggeom = scenegeom.Clone()
                workinggeom.Transform(coordtrans)
                # See if geometry intersects
                if workinggeom.Intersects(regiongeom) or workinggeom.Within(regiongeom):
                    intergeom = workinggeom.Intersection(regiongeom)
                    overlap = (100.0 / workinggeom.Area()) * intergeom.Area()
                else:
                    overlap = 0.0

                # If the overlap is greater or equal to the threshold, and it is an EW file, download the file
                if overlap >= threshold:   # and scenefn.find('S1A_EW_') > -1:
                    print ("Overlap %5.1f" % overlap)
                    # [zipprocflg,zipfn] = download_raw_S1( scenefn, scenedt )
                    [zipprocflg,zipfn] = download_zip_S1( scenefn, scenedt )

                    # Open bzipped-file zip-file object
                    [ inparch, ziprootfn, manifest ] = open_archive( zipfn )
                    # Parse manifest.safe file and get xml-file and data file names
                    maniroot = ET.parse( inparch.open(manifest,'r') ).getroot()

                    # Get the delivery timeliness
                    timeliness = get_type( maniroot )
                    # print timeliness

                    # Close archive file
                    maniroot = None
                    inparch.close()

                    # Get the file size
                    scenesz = (os.stat(zipfn)).st_size
                    # print scenesz
                    # Get the current time to set downloaddt
                    curdt = datetime.now()
                    curdtstr = curdt.strftime('%Y-%m-%d %H:%M:%S')
                    # print curdtstr
                    # Create a short zip file name for 's1_files' table
                    zipfnbits = zipfn.split('/')
                    shortzip = zipfnbits[-1]

                    # Update 's1_catalogue' table with size, downloaddt, downloadflg
                    sqltxt = "UPDATE s1_catalogue SET"
                    sqltxt = ("%s size = %d" % (sqltxt,scenesz))
                    sqltxt = ("%s, downloaddt = \'%s\'" % (sqltxt,curdtstr))
                    sqltxt = ("%s, downloadflg = %d" % (sqltxt,zipprocflg))
                    sqltxt = ("%s WHERE id = %d;" % (sqltxt,sceneid))
                    print sqltxt
                    queryres = con1.query(sqltxt)

                    # Insert file information into 's1_files' database
                    # delivery = 'Fast-24h' or 'NRT-3h'
                    insertfstr = ("INSERT INTO s1_files " + \
                        "(id, filename, location, datetime, size, serverdt, downloaddt, " + \
                        "processdt, procflg, " + \
                        "archivedt, archflg, deletedt, delflg, " + \
                        "polyanalysedt, polyflg, classifydt, classflg, delivery) " + \
                        "VALUES(nextval(\'s1_serial\')," + \
                        "\'%s\','Raw',\'%s\',%d,\'%s\',\'%s\',\'%s\',0,\'%s\',0,\'%s\',0,\'%s\',0,\'%s\',0,\'%s\');" % \
                        (shortzip,scenedt,scenesz,serverdt, \
                        curdtstr,curdtstr,curdtstr,curdtstr,curdtstr,curdtstr,timeliness))
                    print insertfstr
                    queryres = con1.query(insertfstr)

            else:
                pass

    else:
        print ("Data storage above %d GB limit." % target_size) 
        break       

# Remove the lock file
os.remove(lockfn)
print '\n'

