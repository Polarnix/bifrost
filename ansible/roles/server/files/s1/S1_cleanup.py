#!/usr/bin/python

# Name:          S1_cleanuo.py
# Purpose:       Reduces space taken by Archive storage to a set level.
#                1. Get list of files where procflg and archflg are set to 1, 
#                   order by processing age
#                2. Check data storage, if more than 100 GB (or input option) then proceed.
#                3. Delete archive, processed, and geotiff files for oldest files
#
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import time
from datetime import datetime
import argparse

import pg

from check_data_storage import check_data_storage

# System dependent settings
BASEDIR = '/home/bifrostsat/Data/S1'
ARCHIVEDIR = ("%s/Archive" % BASEDIR)
ANCDIR = '/home/bifrostsat/Ancillary/S1'

# Size of file archive (Gb)
ARCHIVE_SIZE = 5.0

# Active flag allows debugging
activeflg = 1

# Output the start time
tmpt=time.gmtime()
tout=( '*** S1_cleanup.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file for slow computers
lockfn= ("%s/S1_cleanup.lock" % ANCDIR)
if os.path.isfile(lockfn) == True:
    print 'Lock exists!'
    sys.exit()
else:
    # No lock file exists so create one
    fout = open(lockfn, 'w')
    fout.write(tout)
    fout.close()
    pass

# Get optional input of archive size in Gb
parser = argparse.ArgumentParser()
parser.add_argument("-s", "--size", help="Archive size in Gb.", type=float, default=ARCHIVE_SIZE)
args = parser.parse_args()

# Extract arguments from input
target_size = args.size

# Connect to database table containing Envisat file information
con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')
    
# Get list of archive data files that can be safely deleted
sqltxt = "SELECT id,filename,datetime,procflg FROM s1_files WHERE"
sqltxt = ("%s procflg != 0 AND archflg = 1" % sqltxt)
sqltxt = ("%s AND delflg = 0" % sqltxt)
sqltxt = ("%s ORDER BY processdt" % sqltxt)
# print sqltxt
queryresult = con1.query(sqltxt)
proclist = queryresult.getresult()

# Folders to ignore while calculating storage volume
ignorelist = [ '/tmp' ]

# While loop to reduce storage below the set level
idx = 0
while check_data_storage( BASEDIR, ignorelist ) > target_size and idx < len(proclist):

    # Get the record for the item to be removed
    item = proclist[idx]
    fid = item[0]
    rawfn = item[1]
    print item[2]
    if len(item[2]) == 19:
        s1dt = datetime.strptime(item[2],'%Y-%m-%d %H:%M:%S')
    else:
        s1dt = datetime.strptime(item[2],'%Y-%m-%d %H:%M:%S.%f')
    areaflg = int(item[3])
    print "Processing ", rawfn

    # Set area string
    if areaflg == 1:
        areastr = 'Arctic'
    elif areaflg == -1:
        areastr = 'Antarctic'
    else:
        print ("Unknown area: %d" % areaflg)
        sys.exit()
    # print satstr, areastr

    # Set initial delete status
    delstatus = -9999
    deltifstatus = 1

    # Remove the raw file
    rawfname = ("%s/%s" % (ARCHIVEDIR,rawfn))
    delstatus = 1
    if activeflg == 1:
        if os.path.isfile(rawfname):
            os.remove(rawfname)
            delstatus = 1
        elif  os.path.isfile( ("%s.bz2" % rawfname) ):
            os.remove(rawfname)
            delstatus = 1
        else:
            delstatus = -9002
    else:
        print rawfname

    print delstatus

    # If we get here then we must have some form of status values
    # Update database entry with new values (not needed for removig S1 Archive files)
    tmpt=time.gmtime()
    deldtstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)	
    updatefstr = ("UPDATE s1_files SET deletedt = \'%s\'," % deldtstr)
    updatefstr = ("%s delflg = %d" % (updatefstr,delstatus))
    updatefstr = ("%s WHERE id = %d;" % (updatefstr,fid))
    # print updatefstr
    # if activeflg == 1:
    #     queryres = con1.query(updatefstr)
    # print queryres

    # Break out of loop if debugging
    if activeflg == 0:
        break

    idx = idx + 1

# Remove the lock file
os.remove(lockfn)
