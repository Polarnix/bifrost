#!/usr/bin/python

# Name:          get_esa_cmems_credentials.py
# Purpose:       Load ESA CMEMS login credentials for S1 data.
#                Needs a plain text file containing the following 6 lines:
#                  ESA_PRIMARY_HOST=
#                  ESA_PRIMARY_USER=
#                  ESA_PRIMARY_PASSWD=
#                  ESA_SECONDARY_HOST=
#                  ESA_SECONDARY_USER=
#                  ESA_SECONDARY_PASSWD=
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


import os, sys

def get_esa_cmems_credentials( esa_cmems_fn ):

    # Empty dictionary for values
    credentials = {}

    # Try to load file data, otherwise give an error and halt
    try:
        infile = open( esa_cmems_fn, 'r' )
    except:
        print "ESA CMEMS login credentials are required!"
        print ("Unable to load from file %s" % esa_cmems_fn)
        sys.exit()
    else:
        for txtln in infile:
            bits = (txtln.strip()).split('=')
            keytxt = bits[0]
            keyval = bits[1]
            credentials[keytxt] = keyval
        infile.close()

    return credentials
