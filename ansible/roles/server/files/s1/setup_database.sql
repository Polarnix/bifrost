CREATE TABLE s1files(
    id            int4,
    filename      varchar(80),
    location      varchar(8),
    datetime      timestamp,
    size          int,
    delivery      varchar(8),
    serverdt      timestamp,
    downloaddt    timestamp,
    processdt     timestamp,
    procflg       smallint,
    arcviewdt     timestamp,
    arcvflg       smallint,
    dianadt       timestamp,
    diaflg        smallint,
    archivedt     timestamp,
    archflg       smallint,
    deletedt      timestamp,
    delflg        smallint,
    polyanalysedt timestamp,
    polyflg       smallint,
    classifydt    timestamp,
    classflg      smallint,
    concdt        timestamp,
    concflg       smallint);
SELECT AddGeometryColumn('', 's1files', 'coverage', 4326, 'POLYGON', 2);
CREATE INDEX sidx_s1coverage ON s1files USING GIST (coverage
    GIST_GEOMETRY_OPS);
ALTER TABLE s1files ADD PRIMARY KEY (id);
GRANT ALL ON s1files TO istjenesten;
CREATE SEQUENCE s1_serial START 1 CYCLE;
GRANT ALL ON s1_serial TO istjenesten;

-- ALTER TABLE icecharts ADD COLUMN s1scenes integer; 
-- ALTER TABLE icecharts ADD COLUMN s1id integer[]; 
-- ALTER TABLE icecharts ADD COLUMN s1np integer[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HHmean real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HHstdev real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HHmode real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HHmedian real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HHmin real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HHmax real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HHsum real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HHnulls real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HVmean real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HVstdev real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HVmode real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HVmedian real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HVmin real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HVmax real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HVsum real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1HVnulls real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VVmean real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VVstdev real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VVmode real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VVmedian real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VVmin real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VVmax real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VVsum real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VVnulls real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VHmean real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VHstdev real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VHmode real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VHmedian real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VHmin real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VHmax real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VHsum real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1VHnulls real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1IncAmean real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1IncAstdev real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1IncAmode real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1IncAmedian real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1IncAmin real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1IncAmax real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1IncAsum real[]; 
-- ALTER TABLE icecharts ADD COLUMN s1IncAnulls real[];
