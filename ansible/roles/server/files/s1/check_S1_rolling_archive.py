#!/usr/bin/python

# Name:          check_S1_rolling_archive.py
# Purpose:       Checking Sentinel-1 Rolling Archive at ESA for new files and adds them to the local
#                catalogue in "Satellite" database, "s1catalogue" table.
#
#                procflg results:
#                -9001 = Unable to connect to FTP server.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


import os, sys
import time
from datetime import date, datetime, timedelta

import pg

from ftplib import FTP

import osgeo.ogr as ogr

from get_esa_cmems_credentials import get_esa_cmems_credentials

# System dependent settings
BASEDIR='/home/bifrostsat/Data/S1'
DATADIR=BASEDIR+'/Raw'
ANCDIR='/home/bifrostsat/Ancillary/S1'
SETDIR='/home/bifrostsat/Settings'
INVALIDDIR='/home/bifrostsat/Include/S1'
TMPDIR=BASEDIR+'/tmp'

# Active flag allows processing
activeflg = 1

# Latest flag restricts search to last 'searchdays' days of data
latestflg = 1
searchdays = 2

# Output the start time
tmpt=time.gmtime()
tout=( '*** check_S1_rolling_archive.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Try to get ESA CMEMS credentials for FTP server
esa_cmems_fn = ("%s/esa_cmems_credentials.txt" % SETDIR)
credentials = get_esa_cmems_credentials( esa_cmems_fn )

# Create a lock file for slow computers
lockfn= ANCDIR + '/check_S1_rolling_archive.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    fout = open(lockfn, 'w')
    fout.write(tout)
    fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Parse FTP server listing line
def parse_dir_line(line):
    # print line
    words = line.split()
    filename = words[8].replace('.manifest','')
    size = int(words[4])
    t = words[7].split(':')
    # print t, len(t)
    if len(t) == 2:
        ts = words[5] + '-' + words[6] + '-' + datetime.now().strftime('%Y') + ' ' + t[0] + ':' + t[1]
    else:
        ts = words[5] + '-' + words[6] + '-' + datetime.now().strftime('%Y') + ' 00:00'
    # print ts
    timestamp = datetime.strptime(ts, '%b-%d-%Y %H:%M')
    return [filename,size,timestamp]

# Connect to 'Satellite' database for updating the 's1catalogue' table
con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')

# Processing status flag
procflg = 0

# Start date
if latestflg == 1:
    startdt = date.today() - timedelta(days=searchdays)
else:
    startdt = date(2014,9,17)
print ("Start date: %s" % startdt)

# Attempt to open connection to ESA FTP server
if activeflg == 1:
    try:
        # raise ValueError('An error for testing.')    # Raise an error to test secondary server
        esaftp = FTP(host=credentials['ESA_PRIMARY_HOST'], \
                     user=credentials['ESA_PRIMARY_USER'], \
                     passwd=credentials['ESA_PRIMARY_PASSWD'], \
                     timeout=15)
    except:
        print 'Unable to connect to primary FTP server.'

        # Primary FTP server has failed, now try the backup
        try:
            esaftp = FTP(host=credentials['ESA_SECONDARY_HOST'], \
                         user=credentials['ESA_SECONDARY_USER'], \
                         passwd=credentials['ESA_SECONDARY_PASSWD'], \
                         timeout=60)
        except:
            print 'Unable to connect to secondary FTP server.'
            procflg = 9001

# Only do further processing if we have a connection.
if procflg < 9000:

    # Get list of year level folders
    if activeflg == 1:
        tmplist = sorted( esaftp.nlst() )
    else:
        tmplist = ['2015']
    yearlist = []
    for yrstr in tmplist:
        if len(yrstr) == 4 and yrstr[0:1] == '2':
            yearlist.append( yrstr )

    # Loop through year list
    for i in range(len(yearlist)):
        year = int(yearlist[i])

        # If year is within range, list that sub-folder
        if year >= startdt.year:
            # print year
            if activeflg == 1:
                ftppath = ("%4d" % year)
                monthlist = sorted( esaftp.nlst(ftppath) )
            else:
                monthlist = ['2014/09']
            # print monthlist

            # Loop through month list
            for j in range(len(monthlist)):
                pathbits = monthlist[j].split('/')
                month = int(pathbits[-1])

                # If month is within range, list that sub-folder
                if date(year,month,1) >= date(startdt.year,startdt.month,1):
                    # print month
                    if activeflg == 1:
                        ftppath = ("%4d/%02d" % (year,month))
                        daylist = sorted( esaftp.nlst(ftppath) )
                    else:
                        daylist = []
                    # print daylist

                    # Loop through day list
                    for k in range(len(daylist)):
                        pathbits = daylist[k].split('/')
                        day = int(pathbits[-1])

                        # If day is within range, list that sub-folder
                        if date(year,month,day) >= startdt:
                            if activeflg == 1:
                                ftppath = ("%4d/%02d/%02d" % (year,month,day))
                                # scenelist = sorted( esaftp.nlst(ftppath) )
                                tmpscnlst = []
                                esaftp.dir(ftppath,tmpscnlst.append)
                                tmpscnlst = sorted(tmpscnlst)
                                # print tmpscnlst[0:5]
                            else:
                                tmpscnlst = []

                            scenelist = []
                            for fname in tmpscnlst:
                                if fname.find('manifest') > -1:
                                    scenelist.append(fname)
                            print ("%4d-%02d-%02d %d files" % (year,month,day,len(scenelist)))
                            # print scenelist[0:1]

                            # Loop through scene list, and check to see if it is already in catalogue
                            for l in range(len(scenelist)):
                                # pathbits = scenelist[l].split('/')
                                # scenefn = pathbits[-1]
                                [scenefn,scenesz,createdt] = parse_dir_line( scenelist[l] )
                                # print len(scenefn), scenefn

                                # Construct SQL query, and get result
                                sqltxt = ("SELECT id FROM s1_catalogue WHERE filename = \'%s\';" % scenefn)
                                # print sqltxt
                                res = con1.query(sqltxt)
                                reslist = res.getresult()

                                # Length of result is zero, then we need to download the manifest and process it
                                # print len(reslist)
                                if len(reslist) == 0:
                                    # Retrieve the manifest.safe file
                                    manifestpath = ("%4d/%02d/%02d/%s.manifest" % \
                                        (year,month,day,scenefn))
                                    print manifestpath
                                    if activeflg == 1:
                                        esaftp.retrbinary(("RETR %s" % manifestpath), \
                                            open(('%s/manifest.safe' % TMPDIR), 'wb').write)
                                    # Open manifest.safe file and get file details
                                    fin = open(('%s/manifest.safe' % TMPDIR), 'r')

                                    # Metadata files sometimes fail to contain basic information, such as date/time
                                    # Flag value to be aware of this
                                    scenedtflg = 0
                                    coordflg = 0

                                    for tline in fin:
                                        # Image start time
                                        if tline.find('<safe:startTime>') != -1:
                                            idxstr = tline.find('<safe:startTime>') + 16
                                            idxend = tline.find('</safe:startTime>')
                                            # print tline
                                            # print tline[idxstr:idxend]
					    scenedt = datetime.strptime(tline[idxstr:idxend], \
                                                '%Y-%m-%dT%H:%M:%S.%f')
                                            # print scenedt
                                            scenedtflg = 1
                                        # Scene footprint coordinates
                                        if tline.find('<gml:coordinates>') != -1:
                                            idxstr = tline.find('<gml:coordinates>') + 17
                                            idxend = tline.find('</gml:coordinates>')
                                            # print tline
                                            # print tline[idxstr:idxend]
                                            coordlist = tline[idxstr:idxend].split(' ')
                                            # print coordlist
                                            ring = ogr.Geometry(ogr.wkbLinearRing)
                                            for coord in coordlist:
                                                [ystr,xstr] = coord.split(',')
                                                ring.AddPoint(float(xstr),float(ystr))
                                            ring.CloseRings()
                                            scenefp = ogr.Geometry(ogr.wkbPolygon)
                                            scenefp.AddGeometry(ring)
                                            scenefp.FlattenTo2D()
                                            # print scenefp.ExportToWkt()
                                            coordflg = 1
                                    fin.close()
                                    
                                    # Remove the manifest.safe file
                                    os.remove( ('%s/manifest.safe' % TMPDIR) )

                                    # Add details to catalogue
                                    if scenedtflg == 1 and coordflg == 1:
                                        sqltxt = "INSERT INTO s1_catalogue"
                                        sqltxt = ("%s (id,filename,datetime,size,serverdt,downloadflg,coverage)" % sqltxt)
                                        sqltxt = ("%s VALUES (nextval(\'s1_refno\'),\'%s\'" % (sqltxt,scenefn))
                                        sqltxt = ("%s,\'%s\',-9999" % (sqltxt,scenedt))
                                        sqltxt = ("%s,\'%s\',0" % (sqltxt,createdt))
                                        sqltxt = ("%s,ST_GeomFromText(\'%s\',4326));" % (sqltxt,scenefp))
                                        print sqltxt
                                        res = con1.query(sqltxt)
                                    else:
                                        print '  *** Incomplete metadata ***'

    # Close the FTP server connection
    if activeflg == 1:
        esaftp.quit()

# Remove the lock file
os.remove(lockfn)
print '\n'
