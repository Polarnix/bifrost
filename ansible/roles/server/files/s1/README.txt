Processing files for Sentinel-1

calibrate_S1.py
Produces calibrated (sigma_0) GeoTIFF files from a downloaded S1 SAFE.zip file.

update_S1_planning.py
Update database with planned acquisitions taken from the ESA web site.

s1_arctic.shp,.shx,.dbf,.prj and s1_antarctic.shp,.shx,.dbf,.prj
Processing areas for S1 data. 

archive_pack.sh
Reduce size of S1 archive directory.

check_S1_Dropbox.py
Check Dropbox folder for new files.

check_S1_rolling_archive.py
Check ESA CMEMS rolling archive for new files.

download_S1.py
Download S1 data files from ESA server.

filter_S1.py
Removes duplicate files from archive directory with preference to Fast24h files (not NRT3h).

hourly_update.sh
Runs S1 processing hourly.

process_S1.py
Process S1 files to GeoTIFF (uncalibrated).

S1_archive.py
Move processed files into the archive directory.

S1_delete.py
Deletes processed S1 files.

S1_cleanup.py
Reduces S1 data stirage below a set size by deleting files from archive.

S1_watchdog.py
Restarts processing if we have a problem.