SELECT 
  filename,
  datetime,
  serverdt,
  downloaddt 
FROM
  s1files
WHERE
  datetime > '2014-11-01 00:00:00'
ORDER BY
  datetime;

SELECT 
  filename,
  datetime,
  serverdt,
  downloaddt 
FROM
  s1files
WHERE
  datetime > '2014-11-01 00:00:00' AND
  date_part('hour',datetime) >= 4 AND
  date_part('hour',datetime) <= 8
ORDER BY
  datetime;
