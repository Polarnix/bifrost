#!/usr/bin/python

# Name:          S1_delete.py
# Purpose:       Deletes Sentinel-1 files when they are not needed or get too old.
#                Interacts with PostgreSQL+PostGIS 'Satellite' database and 's1_files'
#                table.
#
#                delstatus codes:
#                    0 = Not processed
#                -9001 = Unable to delete raw file
#                -9002 = Unable to delete raw or archive file.
#                -9003 = Unable to find GeoTIFF image tile
#                -9005 = Unable to find DIANA image tile
#                    1 = Successful clearance of files.
#
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, subprocess
import string, time

import numpy

import pg

# System dependent settings
BASEDIR='/home/bifrostsat/Data/S1'
DATADIR = ("%s/Raw" % BASEDIR)
ANCDIR = '/home/bifrostsat/Ancillary/S1'

# Length of file archive as string (i.e. number plus minutes, hours or days)
ARCHIVE_LENGTH = '6 hours'

# Active flag to enable deletion and database update
activeflg = 1

# Output the start time
tmpt=time.gmtime()
tout=( '*** S1_delete.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file for slow computers
lockfn= ANCDIR + '/S1_delete.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    fout = open(lockfn, 'w')
    fout.write(tout)
    fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Connect to database table containing Sentinel-1 file information
con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')

# Part 1: Remove files older than ARCHIVE_LENGTH days
querytxt = ("SELECT id,filename,procflg,archflg,polyflg,classflg FROM s1_files WHERE procflg > -9000 AND procflg != 0 AND delflg = 0 AND processdt < (CURRENT_TIMESTAMP - interval \'%s\');" % ARCHIVE_LENGTH)
print querytxt
queryresult = con1.query(querytxt)
proclist = queryresult.getresult()
# print proclist

for item in proclist:
    fid = item[0]
    rootfname = item[1][:-4]
    procflg = item[2]
    archflg = item[3]
    classflg = item[5]
    print "Processing ", rootfname

    delstatus = -9999

    # Define area
    area = 'Not Known'
    if procflg == 1:
        area = 'Arctic'
    elif procflg == -1:
        area = 'Antarctic'
    
    if area != 'Not Known':

        # Delete GeoTIFF tiles
        archlist = os.listdir( ("%s/%s/GeoTIFF" % (BASEDIR,area)) )
        # print archlist
        for fname in archlist:
            if fname.find(rootfname) > -1:
                fullfn = ("%s/%s/GeoTIFF/%s" % (BASEDIR,area,fname))
                if activeflg == 1:
                    try:
                        os.remove(fullfn)
                        delstatus = 1
                    except:
                        delstatus = -9003
                else:
                    # print fullfn
                    delstatus = 1
  

    # If we get here then we must have some form of delstatus value
    # Update database entry with new values
    tmpt=time.gmtime()
    deldtstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)	
    updatefstr = ("UPDATE s1_files SET deletedt = \'%s\', " % deldtstr) + \
        ("delflg = %d " % delstatus) + \
        ("WHERE id = %d;" % fid)
    print updatefstr
    if activeflg == 1:
        queryres = con1.query(updatefstr)
    # print queryres

# Remove the lock file
os.remove(lockfn)

