#!/bin/bash

# Name:          setup_processing.sh
# Purpose:       Shell script to set up directory structure for Sentinel-1 data processing..
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Setup data directories
DATAROOT='/vol/data/Sentinel-1'
# mkdir "${DATAROOT}"

# Main sub-directories
mkdir "${DATAROOT}/tmp"
mkdir "${DATAROOT}/Raw"
mkdir "${DATAROOT}/Archive"
mkdir "${DATAROOT}/Arctic"
mkdir "${DATAROOT}/Arctic/GeoTIFF"
mkdir "${DATAROOT}/Arctic/Diana"
mkdir "${DATAROOT}/Antarctic"
mkdir "${DATAROOT}/Antarctic/GeoTIFF"
mkdir "${DATAROOT}/Antarctic/Diana"
mkdir "${DATAROOT}/Mid-latitudes"
mkdir "${DATAROOT}/Mid-latitudes/GeoTIFF"
mkdir "${DATAROOT}/Mid-latitudes/Diana"

# Create a Dropbox
mkdir "${DATAROOT}/Dropbox"
chmod a+rwx "${DATAROOT}/Dropbox"
mkdir "${DATAROOT}/Dropbox/invalid"

# Create 'Satellite' database (as vnnis)
#   createdb Satellite
#   createlang plpgsql Satellite
#   psql -d Satellite -f /usr/share/postgresql/8.4/contrib/postgis-2.0/postgis.sql
#   psql -d Satellite -f /usr/share/postgresql/8.4/contrib/postgis-2.0/spatial_ref_sys.sql
#   psql -d Satellite -f /usr/share/postgresql/8.4/contrib/postgis-2.0/postgis_comments.sql

# Add database table for Sentinel-1
su - vnnis
psql -d Satellite -f setup_database.sql
psql -d Satellite -f setup_catalogue.sql

exit

