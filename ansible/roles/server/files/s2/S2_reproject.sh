#!/bin/bash

# Name:          S2_reproject.sh
# Purpose:       Shell script to reproject S2 granule data to an RGB GeoTIFF..
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

RAWDIR='/disk2/istjenesten_projects/IAW5_Satellite_Data/S2/Raw'
TMPDIR='/disk2/istjenesten_projects/IAW5_Satellite_Data/S2/tmp'
OUTDIR='/disk2/istjenesten_projects/IAW5_Satellite_Data/S2/GeoTIFF'

PROJSTR='+proj=stere +lat_0=90s +lon_0=0e +lat_ts=90s +ellps=WGS84 +datum=WGS84'
RES=10

cd "${RAWDIR}"
shopt -s nullglob
for fn in *.zip
do
  echo $fn

  # Unpack the file
  cd "${TMPDIR}"
  unzip -o "${RAWDIR}/${fn}" > /dev/null

  SAFEDIR=`ls -d *.SAFE`

  # echo "${TMPDIR}/${SAFEDIR}/GRANULE"
  GRANULES=`ls ${TMPDIR}/${SAFEDIR}/GRANULE`
  # echo $GRANULES
  for granule in $GRANULES
  do
    granbits=(${granule//_/ })
    dtstr=${granbits[6]}
    dtbits=(${dtstr//T/ })
    datestr=${dtbits[0]}
    timestr=${dtbits[1]}
    granule_id=${granbits[8]}
    substring=${granule: -7}
    # echo $substring
    short_granule=${granule%%$substring}
    # echo $short_granule
    granule_fn="S2A_${datestr}_${timestr}_${granule_id}"
    echo $datestr $timestr $granule_id

    # Process the R, G, and B high resolution channels
    declare -a channels=( "B04" "B03" "B02" )
    for chn in "${channels[@]}"
    do
      # Warp to polar stereographic
      gdalwarp --config GDAL_CACHEMAX 500 -wm 500 -of GTiff -ot Int32 -co "BIGTIFF=YES" \
        -t_srs "${PROJSTR}" -tr $RES $RES -r bilinear -multi -overwrite -q \
        -srcnodata 0.0 -dstnodata -1.0 \
        "${TMPDIR}/${SAFEDIR}/GRANULE/${granule}/IMG_DATA/${short_granule}_${chn}.jp2" \
        "${TMPDIR}/components/${granule_fn}_${chn}.tif"
      # S1 uses -srcnodata 0.0 -dstnodata -1.0
      # Translate to 8bit
      gdal_translate -of GTiff -ot Byte -co "COMPRESS=LZW" -co "BIGTIFF=YES" \
        -scale 0 9500 1 255 -a_nodata 0 -q \
        "${TMPDIR}/components/${granule_fn}_${chn}.tif" \
        "${TMPDIR}/components/${granule_fn}_${chn}_8bit.tif"
    done

    # Merge to true colour composite
    merged_fn="${TMPDIR}/merged/${granule_fn}_rgb_8bit.tif"
    gdal_merge.py -o "${merged_fn}" \
      -of GTiff -ot Byte -co "BIGTIFF=YES" -co "COMPRESS=LZW" \
      -co "TILED=YES" -co "BLOCKXSIZE=1024" -co "BLOCKYSIZE=1024" \
      -separate -n 0 -a_nodata 0 -q \
      "${TMPDIR}/components/${granule_fn}_B04_8bit.tif" \
      "${TMPDIR}/components/${granule_fn}_B03_8bit.tif" \
      "${TMPDIR}/components/${granule_fn}_B02_8bit.tif"

    # Create overview pyramids
    gdaladdo -q -r average --config COMPRESS_OVERVIEW JPEG --config BIGTIFF_OVERVIEW \
      -ro "${merged_fn}" 2 4 8 16 32 64 128 256

    # Generate JPG-compressed GeoTIFF
    gdal_translate -of GTiff -ot Byte -co "BIGTIFF=YES" \
      -co "COMPRESS=JPEG" -co "JPEG_QUALITY=75" \
      -co "TILED=YES" -co "BLOCKXSIZE=1024" -co "BLOCKYSIZE=1024" -q \
      "${TMPDIR}/merged/${granule_fn}_rgb_8bit.tif" \
      "${OUTDIR}/${granule_fn}_rgb_8bit.tif"

    # Create overview pyramids
    gdaladdo -q -r average --config COMPRESS_OVERVIEW JPEG --config BIGTIFF_OVERVIEW \
      -ro "${OUTDIR}/${granule_fn}_rgb_8bit.tif" 2 4 8 16 32 64 128 256

    rm "${TMPDIR}/components/"*_B0?.tif
    rm "${TMPDIR}/components/"*_B0?_8bit.tif

  done

  rm -r ${TMPDIR}/${SAFEDIR}

  # Merge tiles into one mega-S2 scene
  # gdal_merge.py -o "${OUTDIR}/S2A_${datestr}_${timestr}_rgb_8bit.tif" \
  #   -of GTiff -ot Byte -co "BIGTIFF=YES" \
  #   -co "TILED=YES" -co "BLOCKXSIZE=2048" -co "BLOCKYSIZE=2048" \
  #   -n 0 -a_nodata 0 \
  #   "${TMPDIR}/merged"/*_rgb_8bit.tif
    
  cd "${RAWDIR}"
done

exit


# from S1
#   /usr/bin/gdal_translate -of GTiff -ot Byte -co "COMPRESS=LZW" -co "BIGTIFF=YES" -co "TILED=YES" -co "BLOCKXSIZE=1024" -co "BLOCKYSIZE=1024" \
#     -scale 0.00 0.50 1 255 -a_nodata 0 -q \
#     "${WORKDIR}/${FNAME}.tif" "${WORKDIR}/8bit/${FNAME}_8bit.tif"
#   /usr/bin/gdaladdo -q -r average --config COMPRESS_OVERVIEW LZW --config BIGTIFF_OVERVIEW \
#     -ro "${WORKDIR}/8bit/${FNAME}_8bit.tif" 2 4 8 16 32 64 128 256

