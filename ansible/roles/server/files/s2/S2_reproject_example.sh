#!/bin/bash

# Name:          S2_reproject_example.sh
# Purpose:       Shell script example of reprojecting Sentinel-2 granule GeoTIFF images using gdalwarp.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

ROOTFN='S2A_OPER_PRD_MSIL1C_PDMC_20160404T100937_R082_V20160403T144934_20160403T144934.SAFE'
TILEFN='S2A_OPER_MSI_L1C_TL_MTI__20160403T213737_A004083_T28XEN'

PROJSTR='+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84'
RES=10

gdalwarp --config GDAL_CACHEMAX 500 -wm 500 -of GTiff -ot UInt16 -co "BIGTIFF=YES" -co "COMPRESS=LZW" \
  -t_srs "${PROJSTR}" -tr $RES $RES -r bilinear -multi -overwrite -q \
  "./${ROOTFN}/GRANULE/${TILEFN}_N02.01/IMG_DATA/${TILEFN}_B04.jp2" "${TILEFN}_B04.tif"

gdalwarp --config GDAL_CACHEMAX 500 -wm 500 -of GTiff -ot UInt16 -co "BIGTIFF=YES" -co "COMPRESS=LZW" \
  -t_srs "${PROJSTR}" -tr $RES $RES -r bilinear -multi -overwrite -q \
  "./${ROOTFN}/GRANULE/${TILEFN}_N02.01/IMG_DATA/${TILEFN}_B03.jp2" "${TILEFN}_B03.tif"

gdalwarp --config GDAL_CACHEMAX 500 -wm 500 -of GTiff -ot UInt16 -co "BIGTIFF=YES" -co "COMPRESS=LZW" \
  -t_srs "${PROJSTR}" -tr $RES $RES -r bilinear -multi -overwrite -q \
  "./${ROOTFN}/GRANULE/${TILEFN}_N02.01/IMG_DATA/${TILEFN}_B02.jp2" "${TILEFN}_B02.tif"

gdal_merge.py -o "${TILEFN}_rgb.tif" -of GTiff -co "BIGTIFF=YES" -co "COMPRESS=LZW" \
  -separate -n 0 \
  "${TILEFN}_B04.tif" "${TILEFN}_B03.tif" "${TILEFN}_B02.tif"

rm *_B0?.tif

