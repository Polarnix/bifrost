#!/usr/bin/python

# Name:          check_gmdss.py
# Purpose:       Check AARI GMDSS server for updated files.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, platform
import types
from datetime import datetime
import time
import urllib2
from copy import deepcopy

import pg

import osgeo.ogr as ogr
import osgeo.osr as osr

from file_utils import testoutdir
from icechart_utils import getproj, get_aari_dict, get_nic_dict, get_nicberg_dict

GMDSS_URL = 'http://ice.aari.aq'
GMDSS_DIR = '/home/bifrostadmin/Data/GMDSS/Antarctic'

# Output the start time
tmpt=time.gmtime()
tout=( '*** check_gmdss.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout
# Get date from start time
curyr=tmpt.tm_year
curmh=tmpt.tm_mon
curdy=tmpt.tm_mday

# Active flag allows writing to database
active_flg = 1


# Read Shapefile data from AARI and add to database
def read_aari( dbcon, shpfn ):

    # AARI fields and NIS database equivalents
    aari_dict = get_aari_dict()

    # Open Shapefile and get map projection information
    shpdrv = ogr.GetDriverByName('ESRI Shapefile')
    inds = shpdrv.Open(shpfn, 0)
    if inds is None:
        print ("Could not open %s." % shpfn)
        sys.exit()
    inlay = inds.GetLayer()
    in_srs = inlay.GetSpatialRef()

    # Set up projection transformation
    out_srs = osr.SpatialReference()
    out_srs.ImportFromEPSG(4326)
    to_longlat = osr.CoordinateTransformation(in_srs,out_srs)

    # Lists for data
    attributes = []
    values = []

    # Loop through polygons and import data
    npoly = inlay.GetFeatureCount()
    for i in range(npoly):
        infeat = inlay.GetFeature(i)
        fieldlist = []
        si3list = []
        vallist = []
        for keyname in aari_dict.keys():
            [ si3code, shptype, shpwidth ] = aari_dict[keyname]
            # print si3code, type(si3code)
            if si3code != 'None' and type(si3code) is types.StringType:
                tmpval = infeat.GetFieldAsString(keyname)
                if len(tmpval) >= 1:
                    if shptype == 'real':
                        value = float(tmpval)
                    elif shptype == 'string':
                        if shpwidth >= 2:
                            value = int(tmpval)
                        if shpwidth == 1:
                            value = tmpval
                    else:
                        # Capture unhandled parameters
                        print shptype, shpwidth
                        sys.exit()
                    fieldlist.append(keyname)
                    si3list.append(si3code)
                    vallist.append(value)
            elif si3code != 'None' and type(si3code) is types.ListType:
                tmpval = infeat.GetFieldAsString(keyname)
                # print tmpval
                if len(tmpval[0:2]) >= 2:
                    value1 = int(tmpval[0:2])
                    fieldlist.append(keyname)
                    si3list.append(si3code[0])
                    vallist.append(value1)
                if len(tmpval[2:4]) >= 2:
                    value2 = int(tmpval[2:4])
                    fieldlist.append(keyname)
                    si3list.append(si3code[1])
                    vallist.append(value2)
        # Get feature geometry
        polygeom = infeat.GetGeometryRef()
        polygeom.Transform(to_longlat)
        if polygeom.GetGeometryName() == 'POLYGON':

            fieldlist.append('GEOMETRY')
            si3list.append('polygon')
            # vallist.append( ("ST_GeomFromText(\'%s\', 4326)" \
            #     % polygeom.ExportToWkt()) )
            vallist.append( polygeom.ExportToWkt() )

            # print fieldlist
            # print si3list
            # print vallist
            # print len(fieldlist), len(si3list), len(vallist)
            attributes.append(si3list)
            values.append(vallist)
            # print attributes
            # print values

        elif polygeom.GetGeometryName() == 'MULTIPOLYGON':
            # Get the number of geometries present
            ngeom = polygeom.GetGeometryCount()
            for j in range(ngeom):
                geombit = polygeom.GetGeometryRef(j)
                bitname = geombit.GetGeometryName()
                if bitname == 'POLYGON':
                    bitfields = deepcopy( fieldlist )
                    bitfields.append('GEOMETRY')
                    bits_si3 = deepcopy( si3list )
                    bits_si3.append('polygon')
                    bits_vals = deepcopy( vallist )
                    bits_vals.append( geombit.ExportToWkt() )
                    attributes.append(bits_si3)
                    values.append(bits_vals)
                else:
                    print ("Unhandled geometry type \'%s\'." % polygeom.GetGeometryName())
                    sys.exit()

        else:
            print ("Unhandled geometry type \'%s\'." % polygeom.GetGeometryName())
            sys.exit()

        # Destroy input feature
        infeat.Destroy()

    # Close input file
    inds.Destroy()

    # Check polygon geometries
    error_count = 0
    for i in range(npoly):
        nattr = len(attributes[i])
        for j in range(nattr):
            if attributes[i][j] == 'polygon':
                # print values
                polywkt = values[i][j]
                # print polywkt
                polygon = ogr.CreateGeometryFromWkt(polywkt)
                if polygon.IsValid():
                    # print i, 'yes'
                    pass
                else:
                    # print i, 'no'
                    error_count = error_count + 1
                # Other checks?
                polygon.Destroy()
    print ("Warning - %d errors in polygons." % error_count)

    # Add data to database
    status = 0
    for i in range(npoly):
        nattr = len(attributes[i])
        # print nattr, len(values)
        sqltxt1 = "INSERT INTO icechart_polygons (id,datetime,service"
        sqltxt2 = ("VALUES (nextval(\'icechart_polygons_serial\'),\'%s\',\'RU\'" \
            % icedt.strftime('%Y-%m-%d %H:%M:%S'))
        for j in range(nattr):
            sqltxt1 = ("%s,%s" % (sqltxt1,attributes[i][j]))
            if attributes[i][j] != 'polygon':
                if type(values[i][j]) is types.IntType:
                    sqltxt2 = ("%s,%d" % (sqltxt2,values[i][j]))
                elif type(values[i][j]) is types.StringType:
                    sqltxt2 = ("%s,\'%s\'" % (sqltxt2,values[i][j]))
                elif type(values[i][j]) is types.FloatType:
                    sqltxt2 = ("%s,%f" % (sqltxt2,values[i][j]))                
            else:
                sqltxt2 = ("%s,ST_GeomFromText(\'%s\', 4326)" \
                    % (sqltxt2,values[i][j]))
        sqltxt = ("%s) %s);" % (sqltxt1,sqltxt2))
        # print sqltxt
        if active_flg == 1:
            # try:
            res = dbcon.query(sqltxt)
            # except:
            #     print 'Error:'
            #     print sqltxt
            #     sys.exit()
            status = status + 1
        else:
            print sqltxt

    return status


# Read Shapefile data from NIC and add to database
def read_nic( dbcon, shpfn ):

    # NIC fields and NIS database equivalents
    nic_dict = get_nic_dict()

    # Open Shapefile and get map projection information
    shpdrv = ogr.GetDriverByName('ESRI Shapefile')
    inds = shpdrv.Open(shpfn, 0)
    if inds is None:
        print ("Could not open %s." % shpfn)
        sys.exit()
    inlay = inds.GetLayer()
    in_srs = inlay.GetSpatialRef()

    # Set up projection transformation
    out_srs = osr.SpatialReference()
    out_srs.ImportFromEPSG(4326)
    to_longlat = osr.CoordinateTransformation(in_srs,out_srs)

    # Lists for data
    attributes = []
    values = []

    # Loop through polygons and import data
    npoly = inlay.GetFeatureCount()
    for i in range(npoly):
        infeat = inlay.GetFeature(i)
        fieldlist = []
        si3list = []
        vallist = []
        for keyname in nic_dict.keys():
            [ si3code, shptype, shpwidth ] = nic_dict[keyname]
            # print si3code, type(si3code)
            if si3code != 'None' and type(si3code) is types.StringType:
                tmpval = infeat.GetFieldAsString(keyname)
                if len(tmpval) >= 1:
                    if shptype == 'real':
                        value = float(tmpval)
                    elif shptype == 'string':
                        if keyname != 'POLY_TYPE':
                            value = int(tmpval)
                        elif keyname == 'POLY_TYPE':
                            value = tmpval
                    else:
                        # Capture unhandled parameters
                        print shptype, shpwidth
                        sys.exit()
                    fieldlist.append(keyname)
                    si3list.append(si3code)
                    vallist.append(value)
        # Get feature geometry
        polygeom = infeat.GetGeometryRef()
        polygeom.Transform(to_longlat)
        if polygeom.GetGeometryName() == 'POLYGON':

            fieldlist.append('GEOMETRY')
            si3list.append('polygon')
            # vallist.append( ("ST_GeomFromText(\'%s\', 4326)" \
            #     % polygeom.ExportToWkt()) )
            vallist.append( polygeom.ExportToWkt() )

            # print fieldlist
            # print si3list
            # print vallist
            # print len(fieldlist), len(si3list), len(vallist)
            attributes.append(si3list)
            values.append(vallist)
            # print attributes
            # print values

        elif polygeom.GetGeometryName() == 'MULTIPOLYGON':
            # Get the number of geometries present
            ngeom = polygeom.GetGeometryCount()
            for j in range(ngeom):
                geombit = polygeom.GetGeometryRef(j)
                bitname = geombit.GetGeometryName()
                if bitname == 'POLYGON':
                    bitfields = deepcopy( fieldlist )
                    bitfields.append('GEOMETRY')
                    bits_si3 = deepcopy( si3list )
                    bits_si3.append('polygon')
                    bits_vals = deepcopy( vallist )
                    bits_vals.append( geombit.ExportToWkt() )
                    attributes.append(bits_si3)
                    values.append(bits_vals)
                else:
                    print ("Unhandled geometry type \'%s\'." % polygeom.GetGeometryName())
                    sys.exit()

        else:
            print ("Unhandled geometry type \'%s\'." % polygeom.GetGeometryName())
            sys.exit()

        # Destroy input feature
        infeat.Destroy()

    # Close input file
    inds.Destroy()

    # Check polygon geometries
    error_count = 0
    for i in range(npoly):
        nattr = len(attributes[i])
        for j in range(nattr):
            if attributes[i][j] == 'polygon':
                # print values
                polywkt = values[i][j]
                # print polywkt
                polygon = ogr.CreateGeometryFromWkt(polywkt)
                if polygon.IsValid():
                    # print i, 'yes'
                    pass
                else:
                    # print i, 'no'
                    error_count = error_count + 1
                # Other checks?
                polygon.Destroy()
    print ("Warning - %d errors in polygons." % error_count)

    # Add data to database
    status = 0
    for i in range(npoly):
        nattr = len(attributes[i])
        # print nattr, len(values)
        sqltxt1 = "INSERT INTO icechart_polygons (id,datetime,service"
        sqltxt2 = ("VALUES (nextval(\'icechart_polygons_serial\'),\'%s\',\'US\'" \
            % icedt.strftime('%Y-%m-%d %H:%M:%S'))
        for j in range(nattr):
            sqltxt1 = ("%s,%s" % (sqltxt1,attributes[i][j]))
            if attributes[i][j] != 'polygon':
                if type(values[i][j]) is types.IntType:
                    sqltxt2 = ("%s,%d" % (sqltxt2,values[i][j]))
                elif type(values[i][j]) is types.StringType:
                    sqltxt2 = ("%s,\'%s\'" % (sqltxt2,values[i][j]))
                elif type(values[i][j]) is types.FloatType:
                    sqltxt2 = ("%s,%f" % (sqltxt2,values[i][j]))                
            else:
                sqltxt2 = ("%s,ST_GeomFromText(\'%s\', 4326)" \
                    % (sqltxt2,values[i][j]))
        sqltxt = ("%s) %s);" % (sqltxt1,sqltxt2))
        # print sqltxt
        if active_flg == 1:
            # try:
            res = dbcon.query(sqltxt)
            # except:
            #     print 'Error:'
            #     print sqltxt
            #     sys.exit()
            status = status + 1
        else:
            print sqltxt

    return status


# Read Shapefile data from NIC icebergs file and add to database
def read_nicberg( dbcon, shpfn, bergdt ):

    # NIC iceberg fields and NIS database equivalents
    nicberg_dict = get_nicberg_dict()

    # Open Shapefile and get map projection information
    shpdrv = ogr.GetDriverByName('ESRI Shapefile')
    inds = shpdrv.Open(shpfn, 0)
    if inds is None:
        print ("Could not open %s." % shpfn)
        sys.exit()
    inlay = inds.GetLayer()
    in_srs = inlay.GetSpatialRef()

    # Set up projection transformation
    out_srs = osr.SpatialReference()
    out_srs.ImportFromEPSG(4326)
    to_longlat = osr.CoordinateTransformation(in_srs,out_srs)

    # Lists for data
    attributes = []
    values = []

    # Loop through points and import data
    npoly = inlay.GetFeatureCount()
    badcount = 0
    for i in range(npoly):
        infeat = inlay.GetFeature(i)
        fieldlist = []
        si3list = []
        vallist = []
        for keyname in nicberg_dict.keys():
            [ si3code, si3type, shptype, shpwidth ] = nicberg_dict[keyname]
            # print si3code, type(si3code)
            if si3code != 'None' and type(si3code) is types.StringType:
                tmpval = infeat.GetFieldAsString(keyname)
                if len(tmpval) >= 1:
                    if si3type == 'int':
                        value = int(tmpval)
                    elif si3type == 'float':
                        value = float(tmpval)
                    elif si3type == 'string':
                        value = deepcopy( tmpval )
                    elif si3type == 'date':
                        datebits = tmpval.split('/')
                        bergmh = int(datebits[0])
                        bergdy = int(datebits[1])
                        bergyr = 2000 + int(datebits[2])
                        value = datetime( bergyr, bergmh, bergdy, 0, 0, 0 )
                    else:
                        # Capture unhandled parameters
                        print si3type, shptype, shpwidth
                        sys.exit()
                    fieldlist.append(keyname)
                    si3list.append(si3code)
                    vallist.append(value)
        # Get feature geometry
        pointgeom = infeat.GetGeometryRef()
        pointgeom.Transform(to_longlat)
        if pointgeom.GetGeometryName() == 'POINT':

            fieldlist.append('GEOMETRY')
            si3list.append('location')
            vallist.append( pointgeom.ExportToWkt() )

            # Check for points too far north - used as comments by NIC
            if pointgeom.GetY() < -10.0:
                attributes.append(si3list)
                values.append(vallist)
            else:
                badcount = badcount + 1

        else:
            print ("Unhandled geometry type \'%s\'." % pointgeom.GetGeometryName())
            sys.exit()

        # Destroy input feature
        infeat.Destroy()

    # Close input file
    inds.Destroy()

    # Adjust number of features
    npoly = npoly - badcount

    # Add data to database
    status = 0
    for i in range(npoly):
        nattr = len(attributes[i])
        # print nattr, len(values)
        sqltxt1 = "INSERT INTO nic_icebergs (id,filedate"
        sqltxt2 = ("VALUES (nextval(\'nic_icebergs_serial\'),\'%s\'" % (bergdt.strftime('%Y-%m-%d')))
        for j in range(nattr):
            sqltxt1 = ("%s,%s" % (sqltxt1,attributes[i][j]))
            if attributes[i][j] != 'location':
                if type(values[i][j]) is types.IntType:
                    sqltxt2 = ("%s,%d" % (sqltxt2,values[i][j]))
                elif type(values[i][j]) is types.FloatType:
                    sqltxt2 = ("%s,%f" % (sqltxt2,values[i][j]))                
                elif type(values[i][j]) is types.StringType:
                    sqltxt2 = ("%s,\'%s\'" % (sqltxt2,values[i][j]))
                elif str(type(values[i][j])) == '<type \'datetime.datetime\'>':
                    sqltxt2 = ("%s,\'%s\'" % (sqltxt2, \
                        values[i][j].strftime('%Y-%m-%d')))
            else:
                sqltxt2 = ("%s,ST_GeomFromText(\'%s\', 4326)" \
                    % (sqltxt2,values[i][j]))
        sqltxt = ("%s) %s);" % (sqltxt1,sqltxt2))
        # print sqltxt
        if active_flg == 1:
            # try:
            res = dbcon.query(sqltxt)
            # except:
            #     print 'Error:'
            #     print sqltxt
            #     sys.exit()
            status = status + 1
        else:
            print sqltxt

    return status


# Dictionary of recognised data providers
provdict = { 'aari' : 'RU', \
             'nic'  : 'US' }

# Connect to ice charts database
dbcon = pg.connect(dbname='Icecharts', host='localhost', \
    user='bifrostadmin', passwd='bifrostadmin')

# Define the host string
hoststr = platform.node()

# 1. Get listing of base directory
dirlist = []
# Loop through years and months
loopyr = 2014
loopmh = 12
loopflg = 1
lastmhflg = 0
while loopflg == 1:
    suburl = ("%s/antice/%d/%02d" % (GMDSS_URL,loopyr,loopmh))
    # print suburl
    try:
        aari = urllib2.urlopen(suburl).read().splitlines()
    except:
        print ("Problem opening connection to AARI - %s" % suburl)
        procflg = 1
    else:
        procflg = 0
        for txtline in aari:
            bits = txtline.split('>')
            for i in range(len(bits)):
                # Get strings fitting the pattern '2???????_*'
                if bits[i][0:1] == '2' and bits[i][8:9] == '_':
                    dirstr = bits[i].split('/')[0]
                    # print dirstr
                    dirlist.append( dirstr )
    # If lastmhflg has been set, quit the loop
    if lastmhflg == 1:
        loopflg = 0
    # Increment loopyr and loopmh
    loopmh = loopmh + 1
    if loopmh == 13:
        loopyr = loopyr + 1
        loopmh = 1
    if loopmh == curmh and loopyr == curyr:
        lastmhflg = 1
# print dirlist

# 2. Loop through directory names
if procflg == 0:
    for dirstr in dirlist:
        # Extract date
        datestr = dirstr[0:8]
        year = int(datestr[0:4])
        month = int(datestr[4:6])
        day = int(datestr[6:8])
        icedt = datetime( year, month, day, 0, 0, 0 )
        # Extract provider
        provider = dirstr[9:]

        # 3. Check to see if database includes ice chart polygons for
        #    that date and either NIC or AARI
        if provider != 'nis':
            print icedt, provider
            sqltxt = ("SELECT COUNT(*) FROM icechart_polygons WHERE datetime = \'%s\' AND service = \'%s\';" \
                % (icedt.strftime('%Y-%m-%d %H:%M:%S'),provdict[provider]))
            # print sqltxt
            npoly = (dbcon.query(sqltxt)).getresult()[0][0]
            # print npoly
            if npoly > 0:
                print ("There are already %d polygons present from %s for %s." \
                    % (npoly,provider.upper(),icedt.strftime('%Y-%m-%d')))
                procflg = 2
        else:
            # NIS is provider, so do not download
            procflg = 3

        # 4. Get subdirectory listing
        if procflg == 0:
            subdirlist = []
            suburl = ("%s/antice/%4d/%02d/%s" % (GMDSS_URL,year,month,dirstr))
            # print suburl
            try:
                aari = urllib2.urlopen(suburl).read().splitlines()
            except:
                print 'Problem opening connection to AARI.'
                procflg = 1
            else:
                procflg = 0
                for txtline in aari:
                    bits = txtline.split('>')
                    for i in range(len(bits)):
                        # Get strings fitting the pattern 'href=' and provider name
                        if bits[i].find('href=') > -1 and bits[i].find(provider) > -1:
                            if bits[i].find('antice') > -1 or bits[i].find('antarc') > -1:
                                fname = bits[i].replace('<a href=','')
                                fname = fname.replace('\"','')
                                # print fname
                                subdirlist.append( fname )
                # print subdirlist

        # 5. Copy original files to local folder
        if procflg == 0:
            # Create local folder if required
            localdir = ("%s/antice/%s" % (GMDSS_DIR,dirstr))
            testoutdir(localdir)
            # Loop through filenames, retrieve from server and save locally
            shplist = []
            for fname in subdirlist:
                furl = ("%s/antice/%4d/%02d/%s/%s" % (GMDSS_URL,year,month,dirstr,fname))
                # print furl
                aarifile = urllib2.urlopen(furl)
                localfn = ("%s/%s" % (localdir,fname))
                localfile = open(localfn,'wb')
                localfile.write(aarifile.read())
                localfile.close()
                aarifile.close()
                # Add Shapefile names to their own list
                if fname.find('.shp') > -1:
                    shplist.append(localfn)
            # print shplist

        # 6. In subdirectory listing, open Shapefile and import ice chart contents into database
        #    Subroutine: read_aari or read_nic
        # print provider
        if procflg == 0:
            if provider == 'aari':
                print shplist[0]
                status = read_aari(dbcon,shplist[0])
                print ("%d polygons imported." % status)
                # Add entry to external production database
                sqltxt = "INSERT INTO external (id, icechart_date, region, computer, service)"
                sqltxt = ("%s VALUES(nextval(\'external_serial\'),\'%s\',\'antarctic\',\'%s\',\'RU\');" \
                    % (sqltxt,icedt.strftime('%Y-%m-%d'),hoststr))
                print sqltxt
                if active_flg == 1:
                    res = dbcon.query(sqltxt)

            elif provider == 'nic':
                for shpfn in shplist:
                    print shpfn
                    if shpfn.find('antarc') > -1:
                        print shpfn
                        status = read_nic(dbcon,shpfn)
                        print ("%d polygons imported." % status)
                        # Add entry to external production database
                        sqltxt = "INSERT INTO external (id, icechart_date, region, computer, service)"
                        sqltxt = ("%s VALUES(nextval(\'external_serial\'),\'%s\',\'antarctic\',\'%s\',\'US\');" \
                            % (sqltxt,icedt.strftime('%Y-%m-%d'),hoststr))
                        print sqltxt
                        if active_flg == 1:
                            res = dbcon.query(sqltxt)
                    elif shpfn.find('antbrg') > -1:
                        status = read_nicberg(dbcon,shpfn)
                        print ("%d iceberg points imported." % status)
            else:
                print ("Unknown data provider %s." % provider)
                sys.exit()

        # 7. Copy AARI satellite data subdirectory to local folder
        if procflg == 0 and provider == 'aari':
            subdirlist = []
            suburl = ("%s/antice/%4d/%02d/%s/satellite" % (GMDSS_URL,year,month,dirstr))
            try:
                aari = urllib2.urlopen(suburl).read().splitlines()
            except:
                print 'Problem opening connection to AARI.'
                procflg = 1
            else:
                procflg = 0
                for txtline in aari:
                    bits = txtline.split('>')
                    for i in range(len(bits)):
                        # Get strings fitting the pattern 'href=' and year
                        if bits[i].find('href=') > -1 and \
                            bits[i].find(("%d" % year)) > -1 and \
                            bits[i].find('/antice/') == -1:
                            fname = bits[i].replace('<a href=','')
                            fname = fname.replace('\"','')
                            # print fname
                            subdirlist.append( fname )
                # print subdirlist

            # Now copy satellite image files to local directory
            localdir = ("%s/antice/%s/satellite" % (GMDSS_DIR,dirstr))
            testoutdir(localdir)
            # Loop through filenames, retrieve from server and save locally
            for fname in subdirlist:
                furl = ("%s/antice/%4d/%02d/%s/satellite/%s" % (GMDSS_URL,year,month,dirstr,fname))
                # print furl
                aarifile = urllib2.urlopen(furl)
                localfn = ("%s/%s" % (localdir,fname))
                localfile = open(localfn,'wb')
                localfile.write(aarifile.read())
                localfile.close()
                aarifile.close()

        # Reset the procflg
        procflg = 0

# Reset the procflg
procflg = 0

# 8. Get listing of base directory
dirlist = []
# Loop through years and months
loopyr = 2015
loopmh = 1
loopflg = 1
lastmhflg = 0
while loopflg == 1:
    suburl = ("%s/antbrg/%d/%02d" % (GMDSS_URL,loopyr,loopmh))
    # print suburl
    try:
        aari = urllib2.urlopen(suburl).read().splitlines()
    except:
        print ("Problem opening connection to AARI - %s" % suburl)
        procflg = 1
    else:
        procflg = 0
        for txtline in aari:
            bits = txtline.split('>')
            for i in range(len(bits)):
                # Get strings fitting the pattern '2???????_*'
                if bits[i][0:1] == '2' and bits[i][8:9] == '_':
                    dirstr = bits[i].split('/')[0]
                    # print dirstr
                    dirlist.append( dirstr )
    # If lastmhflg has been set, quit the loop
    if lastmhflg == 1:
        loopflg = 0
    # Increment loopyr and loopmh
    loopmh = loopmh + 1
    if loopmh == 13:
        loopyr = loopyr + 1
        loopmh = 1
    if loopmh == curmh and loopyr == curyr:
        lastmhflg = 1
# print dirlist

# 9. Loop through directory names
if procflg == 0:
    for dirstr in dirlist:
        # Extract date
        datestr = dirstr[0:8]
        year = int(datestr[0:4])
        month = int(datestr[4:6])
        day = int(datestr[6:8])
        icedt = datetime( year, month, day, 0, 0, 0 )
        # Extract provider
        provider = dirstr[9:]

        # 10. Check to see if database includes icebergs for NIC
        if provider != 'nis':
            print icedt, provider
            sqltxt = ("SELECT COUNT(*) FROM nic_icebergs WHERE filedate = \'%s\';" \
                % (icedt.strftime('%Y-%m-%d %H:%M:%S')))
            # print sqltxt
            nberg = (dbcon.query(sqltxt)).getresult()[0][0]
            # print nberg
            if nberg > 0:
                print ("There are already %d icebergs present for %s." \
                    % (nberg,icedt.strftime('%Y-%m-%d')))
                procflg = 2
        else:
            # NIS is provider, so do not download
            procflg = 3

        # 11. Get subdirectory listing
        if procflg == 0:
            subdirlist = []
            suburl = ("%s/antbrg/%4d/%02d/%s" % (GMDSS_URL,year,month,dirstr))
            # print suburl
            try:
                aari = urllib2.urlopen(suburl).read().splitlines()
            except:
                print 'Problem opening connection to AARI.'
                procflg = 1
            else:
                procflg = 0
                for txtline in aari:
                    bits = txtline.split('>')
                    for i in range(len(bits)):
                        # Get strings fitting the pattern 'href=' and provider name
                        if bits[i].find('href=') > -1 and bits[i].find(provider) > -1 and \
                            bits[i].find('antbrg') > -1:
                            fname = bits[i].replace('<a href=','')
                            fname = fname.replace('\"','')
                            # print fname
                            subdirlist.append( fname )
                # print subdirlist

        # 12. Copy original files to local folder
        if procflg == 0:
            # Create local folder if required
            localdir = ("%s/antbrg/%s" % (GMDSS_DIR,dirstr))
            testoutdir(localdir)
            # Loop through filenames, retrieve from server and save locally
            shplist = []
            nicshplist = []
            for fname in subdirlist:
                furl = ("%s/antbrg/%4d/%02d/%s/%s" % (GMDSS_URL,year,month,dirstr,fname))
                # print furl
                aarifile = urllib2.urlopen(furl)
                localfn = ("%s/%s" % (localdir,fname))
                localfile = open(localfn,'wb')
                localfile.write(aarifile.read())
                localfile.close()
                aarifile.close()
                # Add Shapefile names to their own list
                if fname.find('.shp') > -1:
                    shplist.append(localfn)
                    if fname.find('nic') > -1:
                        nicshplist.append(localfn)
            # print shplist
            # print nicshplist

            # 13. Add NIC iceberg data into database
            for shpfn in nicshplist:
                print shpfn
                status = read_nicberg(dbcon,shpfn,icedt)
                print ("%d iceberg points imported." % status)

        # Reset the procflg
        procflg = 0

        # sys.exit()
    # sys.exit()


# Output the end time
tmpt=time.gmtime()
tout=( '\n*** check_gmdss.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout
