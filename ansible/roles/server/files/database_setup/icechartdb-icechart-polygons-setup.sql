CREATE TABLE icechart_polygons(
    id               int4,
    datetime         timestamp,
    service          varchar(2),
    analyst          varchar(2),
    charttype        varchar(1),
    norway_iceclass  varchar(20),
    srcimgtype       varchar(4),
    srcimgid         int4,
    SI3AREA          float8,
    SI3PERIMETER     float8,
    SI3CT            int2,
    SI3CA            int2,
    SI3SA            int2,
    SI3FA            int2,
    SI3CB            int2,
    SI3SB            int2,
    SI3FB            int2,
    SI3CC            int2,
    SI3SC            int2,
    SI3FC            int2,
    SI3CN            int2,
    SI3CD            int2,
    SI3FP            int2,
    SI3FS            int2,
    SI3poly_type     varchar(1),
    SI3SO            int2,
    SI3SD            int2,
    SI3FD            int2,
    SI3SE            int2,
    SI3FE            int2,
    SI3DP            int2,
    SI3DD            int2,
    SI3DR            int2,
    SI3DO            int2,
    SI3WF            int2,
    SI3WN            int2,
    SI3WD            int2,
    SI3WW            int2,
    SI3WO            int2,
    SI3RN            int2,
    SI3RA            int2,
    SI3RD            int2,
    SI3RC            int2,
    SI3RF            int2,
    SI3RH            int2,
    SI3RO            int2,
    SI3RX            int2,
    SI3EM            int2,
    SI3EX            int2,
    SI3EI            int2,
    SI3EO            int2,
    SI3AV            int2,
    SI3AK            int2,
    SI3AM            int2,
    SI3AT            int2,
    SI3SCsnow        int2,
    SI3SNsnow        int2,
    SI3SDsnow        int2,
    SI3SMsnow        int2,
    SI3SAsnow        int2,
    SI3SOsnow        int2,
    SI3BL            int2,
    SI3BD            int2,
    SI3BE            int2,
    SI3BN            int2,
    SI3BY            int2,
    SI3BO            int2,
    iceberg_id       varchar(6) DEFAULT '');
SELECT AddGeometryColumn('', 'icechart_polygons', 'polygon', 4326, 'POLYGON', 2); 
CREATE INDEX sidx_icepoly ON icechart_polygons USING GIST (polygon GIST_GEOMETRY_OPS); 
ALTER TABLE icechart_polygons ADD PRIMARY KEY (id);

CREATE SEQUENCE icechart_polygons_serial START 1 CYCLE;

GRANT ALL ON TABLE icechart_polygons TO bifrostanalyst;
GRANT ALL ON SEQUENCE icechart_polygons_serial TO bifrostanalyst;