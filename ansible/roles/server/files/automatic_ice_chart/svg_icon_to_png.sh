#!/bin/bash

# Name:          svg_icon_to_png.sh
# Purpose:       Shell script example of how to convert an SVG-format icon graphic to a PNG.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

inkscape \
    --export-png=tmp.png --export-dpi=300 \
    --export-background-opacity=0 --without-gui $FNAME.svg
convert tmp.png -colorspace Gray -level 50%,100% $FNAME.png

