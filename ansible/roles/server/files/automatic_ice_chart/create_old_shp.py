#!/usr/bin/python

# Name:          create_old.py
# Purpose:       Create a Shapefile to the old Norwegian Ice Service (pre-2014) standard.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import string, time
from datetime import datetime

import pg

import osgeo.ogr as ogr
import osgeo.osr as osr

def create_old_shp( shpfn, icedt ):

    # Connect to database
    con1 = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')

    # Get list of ice chart polygons from database
    querystr = ("SELECT AsText(polygon),norway_iceclass FROM icecharts WHERE datetime = \'%s\' AND ST_Y(ST_Centroid(polygon)) > 0.0;" \
        %  icedt )
    queryres = con1.query( querystr )
    # print queryres
    iceres = queryres.getresult()
    npoly = len(iceres)

    # If no polygons, abort
    if npoly == 0:
        print "No polygons on this date.  Stopping."
        sys.exit()

    # Define layer name
    fnbits = shpfn.split('/')
    rootfn = fnbits[-1][:-4]
    # print rootfn

    # Define ice chart map projection
    ice_srs = osr.SpatialReference()
    ice_srs.ImportFromEPSG(4326)

    # Define Shapefile as the format using OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Create Shapefile and layer attributes
    if os.path.exists( shpfn ):
        driver.DeleteDataSource( shpfn )
    poly_ds = driver.CreateDataSource( shpfn )
    poly_lay = poly_ds.CreateLayer( rootfn, srs=ice_srs, \
        geom_type=ogr.wkbPolygon )
    idfld = ogr.FieldDefn( 'ID', ogr.OFTInteger )
    idfld.SetWidth(8)
    poly_lay.CreateField( idfld )
    typefld = ogr.FieldDefn( 'ICE_TYPE', ogr.OFTString )
    typefld.SetWidth(20)
    poly_lay.CreateField( typefld )
    featdefn = poly_lay.GetLayerDefn()

    # Write polygons to Shapefile
    for i in range(npoly):
        icedef = iceres[i]
        icewkt = icedef[0]
        ice_geom = ogr.CreateGeometryFromWkt( icewkt )
        icetype = icedef[1]
        # print icetype
        ext_feat = ogr.Feature( featdefn )
        ext_feat.SetField( 'ID', i )
        ext_feat.SetField( 'ICE_TYPE', icetype )
        ext_feat.SetGeometry( ice_geom )
        poly_lay.CreateFeature( ext_feat )
        ext_feat.Destroy()

    # Close the Shapefile
    poly_ds.Destroy()

