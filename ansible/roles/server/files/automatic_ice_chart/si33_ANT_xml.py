#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

# Name:          si33_ANT_xml.py
# Purpose:       Create SIGRID-3.3 XML header file for Antarctic products.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import string, datetime

import xml.dom.minidom
import re

TMPDIR = '/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart/tmp'

def si33_ANT_xml(year,month,day,areastr,prodname,limits,fieldname,fielddef):

    # Unpack limits values
    minlon = limits[0]
    maxlat = limits[1]
    maxlon = limits[2]
    minlat = limits[3]
    minpsx = limits[4]
    maxpsy = limits[5]
    maxpsx = limits[6]
    minpsy = limits[7]

    # Satellite source information
    sat_origin = [ 'NOAA (National Oceanic and Atmospheric Administration)', \
        'NOAA (National Oceanic and Atmospheric Administration)', \
        'NOAA (National Oceanic and Atmospheric Administration)', \
        'NOAA (National Oceanic and Atmospheric Administration)', \
        'NASA (National Aeronautics and Space Administration)', \
        'NASA (National Aeronautics and Space Administration)', \
        'NASA (National Aeronautics and Space Administration)', \
        'CSA (Canadian Space Agency)', \
        'NOAA (National Oceanic and Atmospheric Administration) / EUMETSAT (European Organisation for the Exploitation of Meteorological Satellites)', \
        'ESA (European Space Agency)', \
        'MDA (MacDonald, Dettwiler and Associates Ltd.)' ]
    sat_title = [ 'TIROS (Television Infrared Observation Satellite)', \
        'ITOS (Improved TIROS Operational System)', \
        'AVHRR (Advanced Very High Resolution Radiometer)', \
        'SSM/I (Special Sensor Microwave/Imager)', \
        'QuikSCAT', \
        'MODIS (Moderate-resolution Imaging Spectroradiometer)', \
        'AMSR-E', \
        'RADARSAT', \
        'AVHRR (Advanced Very High Resolution Radiometer)', \
        'Envisat ASAR (Advanced Synthetic Aperture Radar)', \
        'RADARSAT-2' ]
    sat_type = [ 'Optical (infrared) satellite sensor', \
        'Optical (infrared) satellite sensor', \
        'Optical (visual/infrared) satellite sensor', \
        'Passive microwave radiometer satellite sensor', \
        'Active microwave scatterometer satellite sensor', \
        'Optical (visual/infrared) satellite sensor', \
        'Passive microwave radiometer satellite sensor', \
        'Active microwave SAR (Synthetic Aperture Radar) satellite sensor', \
        'Optical (visual/infrared) satellite sensor', \
        'Active microwave SAR (Synthetic Aperture Radar) satellite sensor', \
        'Active microwave SAR (Synthetic Aperture Radar) satellite sensor' ]
    dt_today = datetime.date(year,month,day)
    delta_day = datetime.timedelta(days=2)
    dt_yesterday = dt_today - delta_day
    # print dt_today, dt_yesterday
    sat_begdate = [ dt_yesterday, dt_yesterday, dt_yesterday, dt_yesterday, dt_yesterday, dt_yesterday, \
        dt_yesterday, dt_today, dt_yesterday, dt_yesterday, dt_today ]
    sat_enddate = [ dt_today, dt_today, dt_today, dt_today, dt_today, dt_today, dt_today, dt_today, \
        dt_today, dt_today, dt_today ]
    sat_procdesc = [ 'Numerous data sources generally within the range of dates for the chart are used.  The chart is then drawn by hand.', \
        'Numerous data sources generally within the range of dates for the chart are used.  The chart is then drawn by hand.', \
        'Numerous data sources generally within the range of dates for the chart are used.  The chart then is manually generated in ArcInfo.', \
        'Numerous data sources generally within the range of dates for the chart are used.  Processing of raw data to ice parameters is provided by OSI SAF.  The chart then is manually generated in ArcInfo.', \
        'Numerous data sources generally within the range of dates for the chart are used.  The chart then is manually generated in ArcInfo.', \
        'Numerous data sources generally within the range of dates for the chart are used.  The chart then is manually generated in ArcInfo.', \
        'Numerous data sources generally within the range of Processing of raw data to sea ice concentration is provided by University of Bremen.  Dates for the chart are used.  The chart then is manually generated in ArcInfo.', \
        'Numerous data sources generally within the range of dates for the chart are used.  Data downlinked and provided by KSAT.  The chart then is manually generated in ArcInfo.', \
        'Numerous data sources generally within the range of dates for the chart are used.  The chart then is manually generated in ArcInfo.', \
        'Numerous data sources generally within the range of dates for the chart are used.  The chart then is manually generated in ArcInfo.', \
        'Numerous data sources generally within the range of dates for the chart are used.  Data downlinked and provided by KSAT.  The chart then is manually generated in ArcInfo.' ]
    satname = [ 'TIROS', 'ITOS', 'AVHRR', 'SSM/I', 'QuikSCAT', 'MODIS', 'AMSR-E', 'RADARSAT', 'AVHRR', \
        'Envisat ASAR', 'RADARSAT-2' ]
    satstart = [ '19670101', '19701211', '19781019', '19970601', '19990619', '20000101', '20050901', \
        '20070601', '20070620', '20080301', '20081101' ]
    satstop = [ '19701210', '19781018', '20070619', '20991231', '20081231', '20991231', '20991231', \
        '20081031', '20991231', '20991231', '20991231' ]
    i = 0
    abssattext = ''
    for satellite,startstr, stopstr in map(None,satname,satstart,satstop):
        stryr = int(startstr[0:4])
        strmh = int(startstr[4:6])
        strdy = int(startstr[6:8])
        dt_start = datetime.date(stryr,strmh,strdy)
        stpyr = int(stopstr[0:4])
        stpmh = int(stopstr[4:6])
        stpdy = int(stopstr[6:8])
        dt_stop = datetime.date(stpyr,stpmh,stpdy)
        # print satellite, dt_start, dt_stop
        if dt_today >= dt_start and dt_yesterday <= dt_stop:
            if i > 0:
                abssattext = abssattext + ', '
            abssattext = abssattext + satellite
            lastsat = satellite
            i = i + 1
    # print lastsat
    fix = ("and %s" % lastsat)
    abssattext = string.replace(abssattext,lastsat,fix)
    # print abssattext
    
    # Create XML DOM data
    xmlfname = ("%s/NIS_%s_%4d%02d%02d_pl_a_%s.xml" % (TMPDIR,areastr,year,month,day,prodname))
    DOM = xml.dom.minidom.Document()
    metadataElement = DOM.createElement("metadata")
    DOM.appendChild(metadataElement)
    idinfoElement = DOM.createElement("idinfo")
    metadataElement.appendChild(idinfoElement)
    
    # "citation" data
    citationElement = DOM.createElement("citation")
    idinfoElement.appendChild(citationElement)
    citeinfoElement = DOM.createElement("citeinfo")
    citationElement.appendChild(citeinfoElement)
    
    originElement = DOM.createElement("origin")
    citeinfoElement.appendChild(originElement)
    originText = DOM.createTextNode("Norwegian Ice Service")
    originElement.appendChild(originText)
    
    pubdateElement = DOM.createElement("pubdate")
    citeinfoElement.appendChild(pubdateElement)
    pubdateText = DOM.createTextNode(("%4d%02d%02d" % (year,month,day)))
    pubdateElement.appendChild(pubdateText)
    
    titleElement = DOM.createElement("title")
    citeinfoElement.appendChild(titleElement)
    titleText = DOM.createTextNode(("%s%4d%02d%02d" % (areastr,year,month,day)))
    titleElement.appendChild(titleText)
    
    # "decrip" data
    descriptElement = DOM.createElement("descript")
    idinfoElement.appendChild(descriptElement)
    
    abstractstr = ("The files contained in this data set were produced at the Norwegian Ice Service and derived from remote sensed data, and observations.  %s sensors comprised the majority of the sensors used in the analysis.  The charts contain information pertaining to the concentration of sea ice in Weddell Sea and Peninsual sectors of the Antarctic." % abssattext)
    abstractElement = DOM.createElement("abstract")
    descriptElement.appendChild(abstractElement)
    abstractText = DOM.createTextNode(abstractstr)
    abstractElement.appendChild(abstractText)
    
    purposeElement = DOM.createElement("purpose")
    descriptElement.appendChild(purposeElement)
    purposeText = DOM.createTextNode("")
    purposeElement.appendChild(purposeText)
    
    supplinfElement = DOM.createElement("supplinf")
    descriptElement.appendChild(supplinfElement)
    supplinfText = DOM.createTextNode("")
    supplinfElement.appendChild(supplinfText)
    
    # "timeperd" data
    timeperdElement = DOM.createElement("timeperd")
    idinfoElement.appendChild(timeperdElement)
    timeinfoElement = DOM.createElement("timeinfo")
    timeperdElement.appendChild(timeinfoElement)
    rngdatesElement = DOM.createElement("rngdates")
    timeinfoElement.appendChild(rngdatesElement)
    
    begdateElement = DOM.createElement("begdate")
    rngdatesElement.appendChild(begdateElement)
    begdateText = DOM.createTextNode(dt_yesterday.strftime('%Y%m%d'))
    begdateElement.appendChild(begdateText)
    
    enddateElement = DOM.createElement("enddate")
    rngdatesElement.appendChild(enddateElement)
    enddateText = DOM.createTextNode(dt_today.strftime('%Y%m%d'))
    enddateElement.appendChild(enddateText)
    
    currentElement = DOM.createElement("current")
    timeperdElement.appendChild(currentElement)
    currentText = DOM.createTextNode("Current as of end date.")
    currentElement.appendChild(currentText)
    
    # "status" data
    statusElement = DOM.createElement("status")
    idinfoElement.appendChild(statusElement)
    
    progressElement = DOM.createElement("progress")
    statusElement.appendChild(progressElement)
    progressText = DOM.createTextNode("Dataset is complete.")
    progressElement.appendChild(progressText)
    
    updateElement = DOM.createElement("update")
    statusElement.appendChild(updateElement)
    updateText = DOM.createTextNode("No changes or additions are made to the dataset after the data set is distributed.")
    updateElement.appendChild(updateText)
    
    # "spdom" data
    spdomElement = DOM.createElement("spdom")
    idinfoElement.appendChild(spdomElement)
    
    boundingElement = DOM.createElement("bounding")
    spdomElement.appendChild(boundingElement)
    
    westbcElement = DOM.createElement("westbc")
    boundingElement.appendChild(westbcElement)
    westbcText = DOM.createTextNode(("%f" % minlon))
    westbcElement.appendChild(westbcText)
    
    eastbcElement = DOM.createElement("eastbc")
    boundingElement.appendChild(eastbcElement)
    eastbcText = DOM.createTextNode(("%f" % maxlon))
    eastbcElement.appendChild(eastbcText)
    
    northbcElement = DOM.createElement("northbc")
    boundingElement.appendChild(northbcElement)
    northbcText = DOM.createTextNode(("%f" % maxlat))
    northbcElement.appendChild(northbcText)
    
    southbcElement = DOM.createElement("southbc")
    boundingElement.appendChild(southbcElement)
    southbcText = DOM.createTextNode(("%f" % minlat))
    southbcElement.appendChild(southbcText)
    
    lboundngElement = DOM.createElement("lboundng")
    spdomElement.appendChild(lboundngElement)
    
    leftbcElement = DOM.createElement("leftbc")
    lboundngElement.appendChild(leftbcElement)
    leftbcText = DOM.createTextNode(("%f" % minpsx))
    leftbcElement.appendChild(leftbcText)
    
    rightbcElement = DOM.createElement("rightbc")
    lboundngElement.appendChild(rightbcElement)
    rightbcText = DOM.createTextNode(("%f" % maxpsx))
    rightbcElement.appendChild(rightbcText)
    
    bottombcElement = DOM.createElement("bottombc")
    lboundngElement.appendChild(bottombcElement)
    bottombcText = DOM.createTextNode(("%f" % minpsy))
    bottombcElement.appendChild(bottombcText)
    
    topbcElement = DOM.createElement("topbc")
    lboundngElement.appendChild(topbcElement)
    topbcText = DOM.createTextNode(("%f" % maxpsy))
    topbcElement.appendChild(topbcText)
    
    # "keywords" data
    keywordsElement = DOM.createElement("keywords")
    idinfoElement.appendChild(keywordsElement)
    themeElement = DOM.createElement("theme")
    keywordsElement.appendChild(themeElement)
    placeElement = DOM.createElement("place")
    keywordsElement.appendChild(placeElement)
    
    themektElement = DOM.createElement("themekt")
    themeElement.appendChild(themektElement)
    themektText = DOM.createTextNode("None")
    themektElement.appendChild(themektText)
    
    theme = [ 'Sea Ice', 'Sea Ice Maps', 'Pack Ice', 'Fast Ice', 'Marginal Ice Zone', \
        'Ice Concentrations', 'Ice Extent', 'Polynya', 'Icebergs' ]
    for thstr in theme:
        themekeyElement = DOM.createElement("themekey")
        themeElement.appendChild(themekeyElement)
        themekeyText = DOM.createTextNode(thstr)
        themekeyElement.appendChild(themekeyText)
    
    placektElement = DOM.createElement("placekt")
    placeElement.appendChild(placektElement)
    placektText = DOM.createTextNode("None")
    placektElement.appendChild(placektText)
    
    place = [ 'Southern Hemisphere', 'Weddell Sea', 'Bellinghausen Sea', \
        'Antarctic Peninsula', 'Bransfield Strait' ]
    for plstr in place:
        placekeyElement = DOM.createElement("placekey")
        placeElement.appendChild(placekeyElement)
        placekeyText = DOM.createTextNode(plstr)
        placekeyElement.appendChild(placekeyText)
    
    # "accconst" data
    accconstElement = DOM.createElement("accconst")
    idinfoElement.appendChild(accconstElement)
    accconstText = DOM.createTextNode("Unclassified, unlimited distribution")
    accconstElement.appendChild(accconstText)
    
    # "useconst" data
    userconstElement = DOM.createElement("userconst")
    idinfoElement.appendChild(userconstElement)
    userconstText = DOM.createTextNode("Unclassified, unlimited distribution")
    userconstElement.appendChild(userconstText)
    
    
    
    # "dataqual" section
    dataqualElement = DOM.createElement("dataqual")
    metadataElement.appendChild(dataqualElement)
    
    logicElement = DOM.createElement("logic")
    dataqualElement.appendChild(logicElement)
    logicText = DOM.createTextNode("SIGRID-3 values are validated for consistancy with WMO standards for reporting ice conditions.  The chart is manually quality controlled.")
    logicElement.appendChild(logicText)
    
    completeElement = DOM.createElement("complete")
    dataqualElement.appendChild(completeElement)
    completeText = DOM.createTextNode(("%4d%02d%02d" % (year,month,day)))
    completeElement.appendChild(completeText)
    
    for origintxt,titletxt,typetxt,dt_begdate,dt_enddate,procdesctxt,startstr, stopstr in map(None,sat_origin,sat_title,sat_type,sat_begdate,sat_enddate,sat_procdesc,satstart,satstop):
        stryr = int(startstr[0:4])
        strmh = int(startstr[4:6])
        strdy = int(startstr[6:8])
        dt_start = datetime.date(stryr,strmh,strdy)
        stpyr = int(stopstr[0:4])
        stpmh = int(stopstr[4:6])
        stpdy = int(stopstr[6:8])
        dt_stop = datetime.date(stpyr,stpmh,stpdy)
        # print satellite, dt_start, dt_stop
        if dt_today >= dt_start and dt_yesterday <= dt_stop:
            lineageElement = DOM.createElement("lineage")
            dataqualElement.appendChild(lineageElement)
            srcinfoElement = DOM.createElement("srcinfo")
            lineageElement.appendChild(srcinfoElement)
    
            srcciteElement = DOM.createElement("srccite")
            srcinfoElement.appendChild(srcciteElement)
            citeinfoElement = DOM.createElement("citeinfo")
            srcciteElement.appendChild(citeinfoElement)
    
            originElement = DOM.createElement("origin")
            citeinfoElement.appendChild(originElement)
            originText = DOM.createTextNode(origintxt)
            originElement.appendChild(originText)
    
            pubdateElement = DOM.createElement("pubdate")
            citeinfoElement.appendChild(pubdateElement)
            pubdateText = DOM.createTextNode("various")
            pubdateElement.appendChild(pubdateText)
    
            titleElement = DOM.createElement("title")
            citeinfoElement.appendChild(titleElement)
            titleText = DOM.createTextNode(titletxt)
            titleElement.appendChild(titleText)
    
            typesrcElement = DOM.createElement("typesrc")
            srcinfoElement.appendChild(typesrcElement)
            typesrcText = DOM.createTextNode(typetxt)
            typesrcElement.appendChild(typesrcText)
    
            srctimeElement = DOM.createElement("srctime")
            srcinfoElement.appendChild(srctimeElement)
            timeinfoElement = DOM.createElement("timeinfo")
            srctimeElement.appendChild(timeinfoElement)
            rngdatesElement = DOM.createElement("rngdates")
            timeinfoElement.appendChild(rngdatesElement)
    
            begdateElement = DOM.createElement("begdate")
            rngdatesElement.appendChild(begdateElement)
            begdateText = DOM.createTextNode(dt_begdate.strftime('%Y%m%d'))
            begdateElement.appendChild(begdateText)
    
            enddateElement = DOM.createElement("enddate")
            rngdatesElement.appendChild(enddateElement)
            enddateText = DOM.createTextNode(dt_enddate.strftime('%Y%m%d'))
            enddateElement.appendChild(enddateText)
    
            srccurrElement = DOM.createElement("srccurr")
            srctimeElement.appendChild(srccurrElement)
            srccurrText = DOM.createTextNode("Ground conditions at time of acquisition.")
            srccurrElement.appendChild(srccurrText)
    
            procstepElement = DOM.createElement("procstep")
            lineageElement.appendChild(procstepElement)
    
            procdescElement = DOM.createElement("procdesc")
            procstepElement.appendChild(procdescElement)
            procdescText = DOM.createTextNode(procdesctxt)
            procdescElement.appendChild(procdescText)
    
            procdateElement = DOM.createElement("procdate")
            procstepElement.appendChild(procdateElement)
            procdateText = DOM.createTextNode("various")
            procdateElement.appendChild(procdateText)
    
    # "eoinfo" section
    eainfoElement = DOM.createElement("eainfo")
    metadataElement.appendChild(eainfoElement)
    
    detailedElement = DOM.createElement("detailed")
    eainfoElement.appendChild(detailedElement)
    
    i = 0
    for attrlabtxt,attrdeftxt in map(None,fieldname,fielddef):
        # print i, attrlabtxt
    
        if i == 0:
            enttypElement = DOM.createElement("enttyp")
            detailedElement.appendChild(enttypElement)
    
            enttyplElement = DOM.createElement("enttypl")
            enttypElement.appendChild(enttyplElement)
            enttyplText = DOM.createTextNode("Mandatory Fields")
            enttyplElement.appendChild(enttyplText)
    
            enttypdElement = DOM.createElement("enttypd")
            enttypElement.appendChild(enttypdElement)
            enttypdText = DOM.createTextNode("These are the mandatory fields in the dBase file")
            enttypdElement.appendChild(enttypdText)
    
            enttypdsElement = DOM.createElement("enttypds")
            enttypElement.appendChild(enttypdsElement)
            enttypdsText = DOM.createTextNode("WMO JCOMM ETSI")
            enttypdsElement.appendChild(enttypdsText)
        if attrlabtxt == 17:
            enttypElement = DOM.createElement("enttyp")
            detailedElement.appendChild(enttypElement)
    
            enttyplElement = DOM.createElement("enttypl")
            enttypElement.appendChild(enttyplElement)
            enttyplText = DOM.createTextNode("Supplementary Fields")
            enttyplElement.appendChild(enttyplText)
    
            enttypdElement = DOM.createElement("enttypd")
            enttypElement.appendChild(enttypdElement)
            enttypdText = DOM.createTextNode("These are the supplementary fields in the dBase file")
            enttypdElement.appendChild(enttypdText)
    
            enttypdsElement = DOM.createElement("enttypds")
            enttypElement.appendChild(enttypdsElement)
            enttypdsText = DOM.createTextNode("NIS")
            enttypdsElement.appendChild(enttypdsText)
    
        attrElement = DOM.createElement("attr")
        detailedElement.appendChild(attrElement)
    
        attrlablElement = DOM.createElement("attrlabl")
        attrElement.appendChild(attrlablElement)
        attrlablText = DOM.createTextNode(attrlabtxt)
        attrlablElement.appendChild(attrlablText)
    
        attrdefElement = DOM.createElement("attrdef")
        attrElement.appendChild(attrdefElement)
        attrdefText = DOM.createTextNode(attrdeftxt)
        attrdefElement.appendChild(attrdefText)
    
        attrdefsElement = DOM.createElement("attrdefs")
        attrElement.appendChild(attrdefsElement)
        attrdefsText = DOM.createTextNode("WMO JCOMM ETSI")
        attrdefsElement.appendChild(attrdefsText)
    
        attrdomvElement = DOM.createElement("attrdomv")
        attrElement.appendChild(attrdomvElement)
        codesetdElement = DOM.createElement("codesetd")
        attrdomvElement.appendChild(codesetdElement)
    
        codesetnElement = DOM.createElement("codesetn")
        codesetdElement.appendChild(codesetnElement)
        codesetnText = DOM.createTextNode("SIGRID-3")
        codesetnElement.appendChild(codesetnText)
    
        codesetsElement = DOM.createElement("codesets")
        codesetdElement.appendChild(codesetsElement)
        codesetsText = DOM.createTextNode("WMO JCOMM ETSI")
        codesetsElement.appendChild(codesetsText)
    
        i = i +1
    
    # "spref" section
    sprefElement = DOM.createElement("spref")
    metadataElement.appendChild(sprefElement)
    horizsysElement = DOM.createElement("horizsys")
    sprefElement.appendChild(horizsysElement)
    
    planarElement = DOM.createElement("planar")
    horizsysElement.appendChild(planarElement)
    mapprojElement = DOM.createElement("mapproj")
    planarElement.appendChild(mapprojElement)
    
    mapprojnElement = DOM.createElement("mapprojn")
    mapprojElement.appendChild(mapprojnElement)
    mapprojnText = DOM.createTextNode("EPSG:4326 WGS84")
    mapprojnElement.appendChild(mapprojnText)
    
    planciElement = DOM.createElement("planci")
    planarElement.appendChild(planciElement)
    planciText = DOM.createTextNode("degrees")
    planciElement.appendChild(planciText)
    
    geodeticElement = DOM.createElement("geodetic")
    horizsysElement.appendChild(geodeticElement)
    
    horizdnElement = DOM.createElement("horizdn")
    geodeticElement.appendChild(horizdnElement)
    horizdnText = DOM.createTextNode("WGS 1984")
    horizdnElement.appendChild(horizdnText)
    
    ellipsElement = DOM.createElement("ellips")
    geodeticElement.appendChild(ellipsElement)
    ellipsText = DOM.createTextNode("WGS 1984")
    ellipsElement.appendChild(ellipsText)
    
    # "metainfo" section
    metainfoElement = DOM.createElement("metainfo")
    metadataElement.appendChild(metainfoElement)
    
    metdElement = DOM.createElement("metd")
    metainfoElement.appendChild(metdElement)
    metdText = DOM.createTextNode("20110722")
    metdElement.appendChild(metdText)
    
    metcElement = DOM.createElement("metc")
    metainfoElement.appendChild(metcElement)
    cntinfoElement = DOM.createElement("cntinfo")
    metcElement.appendChild(cntinfoElement)
    
    cntorgpElement = DOM.createElement("cntorgp")
    cntinfoElement.appendChild(cntorgpElement)
    cntorgElement = DOM.createElement("cntorg")
    cntorgpElement.appendChild(cntorgElement)
    cntorgText = DOM.createTextNode("Norwegian Ice Service")
    cntorgElement.appendChild(cntorgText)
    
    cntaddrElement = DOM.createElement("cntaddr")
    cntinfoElement.appendChild(cntaddrElement)
    
    addrtypeElement = DOM.createElement("addrtype")
    cntaddrElement.appendChild(addrtypeElement)
    addrtypeText = DOM.createTextNode("Mailing address:")
    addrtypeElement.appendChild(addrtypeText)
    addrtypeElement = DOM.createElement("addrtype")
    cntaddrElement.appendChild(addrtypeElement)
    addrtypeText = DOM.createTextNode("Forecasting Division for Northern Norway")
    addrtypeElement.appendChild(addrtypeText)
    addrtypeElement = DOM.createElement("addrtype")
    cntaddrElement.appendChild(addrtypeElement)
    addrtypeText = DOM.createTextNode("P.O. Box 6314")
    addrtypeElement.appendChild(addrtypeText)
    
    postalElement = DOM.createElement("postal")
    cntaddrElement.appendChild(postalElement)
    postalText = DOM.createTextNode("NO-9293")
    postalElement.appendChild(postalText)
    
    cityElement = DOM.createElement("city")
    cntaddrElement.appendChild(cityElement)
    cityText = DOM.createTextNode("Tromsø")
    cityElement.appendChild(cityText)
    
    cntvoiceElement = DOM.createElement("cntvoice")
    cntinfoElement.appendChild(cntvoiceElement)
    cntvoiceText = DOM.createTextNode("+47 77 62 14 62")
    cntvoiceElement.appendChild(cntvoiceText)
    
    cntemailElement = DOM.createElement("cntemail")
    cntinfoElement.appendChild(cntemailElement)
    cntemailText = DOM.createTextNode("istjenesten@met.no")
    cntemailElement.appendChild(cntemailText)
    
    
    
    # Generate XML text
    xmltext = DOM.toprettyxml()
    fix = re.compile(r'((?<=>)(\n[\t]*)(?=[^<\t]))|(?<=[^>\t])(\n[\t]*)(?=<)')
    fixed_output = re.sub(fix, '', xmltext)
    # print fixed_output
    
    # Write .xml file
    fxml = open( xmlfname, 'w' )
    xmlstr = ("%s\n" % fixed_output)
    fxml.write( xmlstr )
    fxml.close()

    return xmlfname

