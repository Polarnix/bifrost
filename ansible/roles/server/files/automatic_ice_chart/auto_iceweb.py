#!/usr/bin/python

# Name:          auto_iceweb.py
# Purpose:       Uploads products to Istejenesten test web server. Shows how to upload
#                using WebDAV protocol, for example to a server that uses Plone CMS.
# Usage:         No command line options = Use current date and do not overwrite
#                python auto_iceweb [yyyymmdd] [force_flg]
#                  yyyymmdd = Date to upload
#                  force_flg = 1 to overwrite existing files on server
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import string, time
from datetime import datetime

import pg

from WebDAV import client

# from create_old_shp import create_old_shp

# General directories
BASEDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Python/include" % BASEDIR)
ANCDIR=("%s/Python/Ancillary" % BASEDIR)
OUTDIR=("%s/Outputs" % BASEDIR)

# New system flag
NEWSYS = 1

# Active flag - to send files to FTP
activeflg = 1

# Output the start time
tmpt=time.gmtime()
tout=( '*** auto_iceweb.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/auto_iceweb.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Define date of ice chart
if len(sys.argv) >= 2:
    datestr = sys.argv[1]
    iceyr = int(datestr[0:4])
    icemh = int(datestr[4:6])
    icedy = int(datestr[6:8])
else:
    now = datetime.now()
    iceyr = now.year
    icemh = now.month
    icedy = now.day
icedt = datetime( iceyr, icemh, icedy, 15, 0, 0)
dtstr = icedt.strftime('%Y%m%d')
# print icedt, dtstr
force_flg = 0
if len(sys.argv) == 3:
    force_flg =  int(sys.argv[2])

# Database checking on new system
proc_flg = 0
if NEWSYS == 1:

    # Connect to database
    dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')

    # Check status in database
    sqltxt = ("SELECT id,finish_flg,outputs_flg,istupload_flg FROM production WHERE icechart_date = \'%4d-%02d-%02d\' AND region =\'arctic\';" \
        % (iceyr,icemh,icedy))
    queryres = dbcon.query( sqltxt )
    nrec = queryres.ntuples()
    if nrec == 0:
        print "auto_istupload: No database record for this date. Stopping."
    elif nrec == 1:
        icechart_id = queryres.getresult()[0][0]
        finish_flg = queryres.getresult()[0][1]
        outputs_flg = queryres.getresult()[0][2]
        istupload_flg = queryres.getresult()[0][3]
    else:
        print "auto_istupload: Too many database records for this date. Stopping."
    if finish_flg <= 0:
        print "auto_istupload: Ice chart not finished on this date. Stopping."
    else:
        if outputs_flg <= 0:
            print "auto_istupload: No output graphics on this date. Stopping."
        else:
            if istupload_flg == 1 and force_flg != 1:
                print "auto_istupload: Ice chart already uploaded to iceservice.met.no server today. Stopping."
                proc_flg = 0
            else:
                proc_flg = 1

# Only process if proc_flg = 1 or NEWSYS = 0
if proc_flg == 1 or NEWSYS == 0:

    # Set ice chart area data
    ic_names = [ 'general', 'baltic', 'fram_strait', 'barents', 'denmark_strait', 'svalbard' ]
    # print ic_names
    ic_types = [ '', '_old' ]
    ic_formats = [ 'png', 'jpg', 'pdf' ]

    # Loop through ice charts and upload to server using WebDAV
    for chart_name in ic_names:
        for chart_type in ic_types:
            for chart_format in ic_formats:

                # Open WebDAV connection
                davurl = ("http://iceservice.met.no:8080/www/ice-charts/icechart_graphics/%s%s_latest.%s" % \
                    (chart_name,chart_type,chart_format))
                # print davurl
                res = client.Resource(davurl, username='istjenesten', password='Rudi@Hopen94')

                # Upload binary file
                localfn = ("%s/%d/%s/%s_%s%s.%s" % \
                    (OUTDIR,iceyr,dtstr,chart_name,dtstr,chart_type,chart_format))
                print localfn

                # Upload file and get response
                if activeflg == 1:
                    response = res.put(localfn)
                    # print response.get_status()
                else:
                    print davurl
                    print localfn

                # Close connection
                res = None

    # On new system, enter database status and close connection
    if NEWSYS == 1:
        # Database status update
        timenow = datetime.now()
        dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
        sqltxt = ("UPDATE production SET (istupload_flg,istupload_dt) = (1,\'%s\') WHERE id = %d;"
            % (dtstr,icechart_id))
        print sqltxt
        if activeflg == 1:
            dbcon.query( sqltxt )
    
        # Close datebase connection
        dbcon.close()

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** auto_iceweb.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

