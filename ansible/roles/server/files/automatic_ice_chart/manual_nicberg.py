#!/usr/bin/python

# Name:          manual_nicberg.py
# Purpose:       Example script to extract NIC iceberg data from database.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import sys, os
from datetime import datetime

import pg

from check_gmdss import read_nicberg

year = 2015
month = 11
day = 6
icedt = datetime( year, month, day, 0, 0, 0 )

DATADIR = '/disk1/Istjenesten/New_Production_System/GMDSS/Antarctic/antbrg'
# 20151106_nic, 20151113_nic, 20151120_nic, 20151127_nic

shpfn = ("%s/%s_nic/nic_antbrg_%s.shp" % \
    (DATADIR,icedt.stpftime('%Y%m%d'),icedt.stpftime('%Y%m%d')))

# Connect to ice charts database
dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')

print shpfn
sys.exit()
status = read_nicberg(dbcon,shpfn,icedt)
print ("%d iceberg points imported." % status)
