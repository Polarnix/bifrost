#!/usr/bin/python

# Name:          auto_ftpupload.py
# Purpose:       Automatically uploads product and Shapefiles to FTP server.
# Usage:         No command line options = Use current date and do not overwrite
#                python auto_ftpupload [yyyymmdd] [force_flg]
#                  yyyymmdd = Date to upload
#                  force_flg = 1 to overwrite existing files on server
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import string, time
from datetime import datetime

import pg

from ftplib import FTP

from create_old_shp import create_old_shp

BASEDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Python/include" % BASEDIR)
ANCDIR=("%s/Python/Ancillary" % BASEDIR)
OUTDIR=("%s/Outputs" % BASEDIR)

# New system flag
NEWSYS = 1

# Active flag - to send files to FTP
activeflg = 1

# Output the start time
tmpt=time.gmtime()
tout=( '*** auto_ftpupload.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/auto_ftpupload.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Define date of ice chart
if len(sys.argv) >= 2:
    datestr = sys.argv[1]
    iceyr = int(datestr[0:4])
    icemh = int(datestr[4:6])
    icedy = int(datestr[6:8])
else:
    now = datetime.now()
    iceyr = now.year
    icemh = now.month
    icedy = now.day
icedt = datetime( iceyr, icemh, icedy, 15, 0, 0)
dtstr = icedt.strftime('%Y%m%d')
# print icedt, dtstr
force_flg = 0
if len(sys.argv) == 3:
    force_flg =  int(sys.argv[2])

# Database checking on new system
if NEWSYS == 1:

    # Connect to database
    dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')

    # Check status in database
    sqltxt = ("SELECT id,finish_flg,outputs_flg,ftpupload_flg,region FROM production WHERE icechart_date = \'%4d-%02d-%02d\';" \
        % (iceyr,icemh,icedy))
    queryres = dbcon.query( sqltxt )
    nrec = queryres.ntuples()
    if nrec == 0:
        print "auto_ftpupload: No database record for this date. Stopping."
    elif nrec >= 1:
        icechart_id = []
        finish_flg = []
        outputs_flg = []
        ftpupload_flg = []
        regions = []
        proc_flg = []
        for i in range(nrec):
            icechart_id.append( queryres.getresult()[i][0] )
            finish_flg.append( queryres.getresult()[i][1] )
            outputs_flg.append( queryres.getresult()[i][2] )
            ftpupload_flg.append( queryres.getresult()[i][3] )
            regions.append( (queryres.getresult()[i][4]).strip() )

            # Further checks
            if finish_flg[i] <= 0:
                print ("auto_ftpupload: Ice chart not finished for %s on this date. Stopping." % regions[i])
                proc_flg.append( 0 )
            else:
                if outputs_flg[i] <= 0:
                    print ("auto_ftpupload: No output graphics for %s on this date. Stopping." % regions[i])
                    proc_flg.append( 0 )
                else:
                    if ftpupload_flg[i] == 1 and force_flg != 1:
                        print "auto_ftpupload: Ice chart already uploaded to ftp server today. Stopping."
                        proc_flg.append( 0 )
                    else:
                        proc_flg.append( 1 )

# Only process if proc_flg = 1 or NEWSYS = 0
for i in range(len(proc_flg)):

    if proc_flg[i] == 1 or NEWSYS == 0:

        if regions[i] == 'arctic':
            # Create temporary Shapefile in old format
            oldshpfn = ("%s/chart_ice.shp" % TMPDIR)
            create_old_shp( oldshpfn, icedt )
    
        # Set ice chart area data
        ic_names = [ 'general', 'baltic', 'fram_strait', 'barents', 'denmark_strait', 'svalbard', 'oslofjord' ]
        ic_old_names = [ 'c_map1', 'c_map2', 'c_map3', 'c_map6', 'c_map7', 'sarmap2', 'oslofjord' ]
        ic_old_bw_names = [ 'map1', 'map2', 'map3', 'map6', 'map7', 'sarmap1', 'oslofjord' ]
        # print ic_names
        ic_types = [ '', '_old' ]
        ic_formats = [ 'png', 'jpg', 'pdf' ]
        # Names for Antarctic
        ic_ant_names = [ 'antarctic', 'peninsula', 'weddell_east', 'bransfield_strait', 'adelaide_island' ]

        # Update datetime string
        dtstr = icedt.strftime('%Y%m%d')
    
        # Open FTP connection
        ftp = FTP ('ftpvm.met.no','istjenesten','Rudi@Hopen94')
        ftpdir = "/projects/icecharts"
        ftp.cwd( ftpdir )
    
        # Arctic ice charts
        if regions[i] == 'arctic':
            # Index value
            idx = 0
    
            # Get list of existing files
            flist = ftp.nlst()
            pnglist = []
            for fname in flist:
                if fname.find('.png') > -1 and fname.find('antarctic') == -1 and \
                    fname.find('sarmap') == -1:
                    pnglist.append( fname )
            # print pnglist
            jpglist = []
            for fname in flist:
                if fname.find('.jpg') > -1 and fname.find('map') == -1 and \
                    fname.find('metno') == -1 and fname.find('antarctic') == -1:
                    jpglist.append( fname )
            # print jpglist
            pdflist = []
            for fname in flist:
                if fname.find('.pdf') > -1 and fname.find('map') == -1:
                    pdflist.append( fname )
            # print pdflist
    
            # Loop through ice charts and upload to FTP
            for chart_name in ic_names:
                for chart_type in ic_types:
                    for chart_format in ic_formats:
                        # Upload binary file
                        localfn = ("%s/%d/%s/%s_%s%s.%s" % \
                            (OUTDIR,iceyr,dtstr,chart_name,dtstr,chart_type,chart_format))
                        ftpcmd = ("STOR %s_%s%s.%s" % (chart_name,dtstr,chart_type,chart_format))
                        if activeflg == 1:
                            status = ftp.storbinary( ftpcmd, open( localfn, 'rb' ) )
                        # print localfn
                        # print ftpcmd
                        # Copy to old filenames (necessary for Kilden)
                        if chart_format == 'jpg':
                            if chart_type == '':
                                ftpcmd2 = ("STOR %s.jpg" % ic_old_names[idx])
                            elif chart_type == '_old':
                                ftpcmd2 = ("STOR %s.jpg" % ic_old_bw_names[idx])
                            else:
                                print 'Unknown chart type', chart_type
                                sys.exit()
                            # print '*', localfn
                            # print '*', ftpcmd2
                            if activeflg == 1:
                                status = ftp.storbinary( ftpcmd2, open( localfn, 'rb' ) )
                idx = idx + 1

            # Upload Shapefile
            shp_bits = [ 'chart_ice.shp', 'chart_ice.shx', 'chart_ice.dbf', 'chart_ice.prj' ]
            for fname in shp_bits:
                localfn = ("%s/%s" % (TMPDIR,fname))
                ftpcmd = ("STOR %s" % fname)
                if activeflg == 1:
                    status = ftp.storbinary( ftpcmd, open( localfn, 'rb' ) )
            # Delete temporary Shapefile
            for fname in shp_bits:
                os.remove( ("%s/%s" % (TMPDIR,fname)) )
    
            # Delete old ice charts on ftp server
            if force_flg != 1:
                for fname in pnglist:
                    # print fname
                    if activeflg == 1:
                        status = ftp.delete( fname )
                for fname in jpglist:
                    # print fname
                    if activeflg == 1:
                        status = ftp.delete( fname )
                for fname in pdflist:
                    # print fname
                    if activeflg == 1:
                        status = ftp.delete( fname )

        elif regions[i] == 'antarctic':
            # Try to change directory to Antarctic subdirectory
            antsubdir = ("antarctic_%d" % iceyr)
            try:
                ftp.cwd( antsubdir )
            except:
                ftp.mkd( antsubdir )
                ftp.cwd( antsubdir )
    
            # Loop through Antarctic ice charts and upload to FTP
            for chart_name in ic_ant_names:
                for chart_type in ic_types:
                    for chart_format in ic_formats:
                        # Upload binary file
                        localfn = ("%s/%d/%s/%s_%s%s.%s" % \
                            (OUTDIR,iceyr,dtstr,chart_name,dtstr,chart_type,chart_format))
                        ftpcmd = ("STOR %s_%s%s.%s" % (chart_name,dtstr,chart_type,chart_format))
                        if activeflg == 1:
                            status = ftp.storbinary( ftpcmd, open( localfn, 'rb' ) )

            # Change directory downward again
            ftp.cwd('..')
        
        # Close FTP connection
        ftp.close()
    
        # On new system, enter database status and close connection
        if NEWSYS == 1:
            # Database status update
            timenow = datetime.now()
            dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
            sqltxt = ("UPDATE production SET (ftpupload_flg,ftpupload_dt) = (1,\'%s\') WHERE id = %d;"
                % (dtstr,icechart_id[i]))
            print sqltxt
            dbcon.query( sqltxt )

if NEWSYS == 1:        
    # Close datebase connection
    dbcon.close()

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** auto_ftpupload.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

