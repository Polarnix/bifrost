#!/usr/bin/python

# Name:          auto_disseminate.py
# Purpose:       Sends out ice charts by e-mail.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import time
from datetime import datetime

import pg

# Email handling libraries
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

import zipfile

BASEDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Python/include" % BASEDIR)
ANCDIR=("%s/Python/Ancillary" % BASEDIR)
OUTDIR=("%s/Outputs" % BASEDIR)

ARCHBASE='/vol/istjenesten/archive'
SHPARCHDIR=("%s/iceShapeArchive" % ARCHBASE)

# New system flag
NEWSYS = 1

# Active flag - to send e-mails
activeflg = 1

# Create ice charts zip file
def createshpzip( iceyr, icemh, icedy):

    # Generate zip file in temporary directory
    zipfn = ("%s/ice%4d%02d%02d.zip" % (TMPDIR,iceyr,icemh,icedy))
    icezip = zipfile.ZipFile(zipfn,'w',zipfile.ZIP_DEFLATED)

    # Shapefile component names
    shproot = ("%s/%4d/%02d/ice%4d%02d%02d" % (SHPARCHDIR,iceyr,icemh,iceyr,icemh,icedy))
    shpfn = ("%s.shp" % (shproot))
    dbffn = ("%s.dbf" % (shproot))
    shxfn = ("%s.shx" % (shproot))
    prjfn = ("%s.prj" % (shproot))

    # Add Shapefile components
    icezip.write(shpfn)
    icezip.write(dbffn)
    icezip.write(shxfn)
    icezip.write(prjfn)

    # Close zipfile
    icezip.close()

    status = 1
    return [zipfn,status]


# Create telex points zip file
def createtelexzip( iceyr, icemh, icedy):

    # Generate zip file in temporary directory
    telexzipfn = ("%s/ice_%4d%02d%02d_telexpts.zip" % (TMPDIR,iceyr,icemh,icedy))
    telexzip = zipfile.ZipFile(telexzipfn,'w',zipfile.ZIP_DEFLATED)

    # Shapefile component names
    shproot = ("%s/Telexpts/Outputs/ice_%4d%02d%02d_telexpts" % (BASEDIR,iceyr,icemh,icedy))
    shpfn = ("%s.shp" % (shproot))
    dbffn = ("%s.dbf" % (shproot))
    shxfn = ("%s.shx" % (shproot))
    prjfn = ("%s.prj" % (shproot))

    # Add Shapefile components
    telexzip.write(shpfn)
    telexzip.write(dbffn)
    telexzip.write(shxfn)
    telexzip.write(prjfn)

    # Close zipfile
    telexzip.close()

    status = 1
    return [telexzipfn,status]


# Output the start time
tmpt=time.gmtime()
tout=( '*** auto_disseminate.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/auto_disseminate.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Define date of product
if len(sys.argv) >= 2:
    datestr = sys.argv[1]
    iceyr = int(datestr[0:4])
    icemh = int(datestr[4:6])
    icedy = int(datestr[6:8])
else:
    now = datetime.now()
    iceyr = now.year
    icemh = now.month
    icedy = now.day
icedt = datetime( iceyr, icemh, icedy, 15, 0, 0)
dtstr = icedt.strftime('%Y%m%d')
dow = icedt.isoweekday()
# print icedt, dtstr, dow
if len(sys.argv) == 3:
    forceflg = int(sys.argv[2])
else:
    forceflg = 0

# Connect to database
dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')

# Loop through regions
regions = [ 'arctic', 'antarctic' ]
for regionstr in regions:

    # Check status in database
    proc_flg = 0
    if NEWSYS == 1:
        sqltxt = ("SELECT id,finish_flg,outputs_flg,disseminate_flg,archive_flg,telexpts_flg FROM production WHERE icechart_date = \'%4d-%02d-%02d\' AND region = \'%s\';" \
            % (iceyr,icemh,icedy,regionstr))
        # print sqltxt
        queryres = dbcon.query( sqltxt )
        # print queryres
        nrec = queryres.ntuples()
        if nrec == 0:
            print "auto_disseminate: No database record for this date. Stopping."
        elif nrec == 1:
            icechart_id = queryres.getresult()[0][0]
            finish_flg = queryres.getresult()[0][1]
            outputs_flg = queryres.getresult()[0][2]
            dissem_flg = queryres.getresult()[0][3]
            archive_flg = queryres.getresult()[0][4]
            telexpts_flg = queryres.getresult()[0][5]
        else:
            print "auto_disseminate: Too many database records for this date. Stopping."
        if finish_flg <= 0:
            print ("auto_disseminate: Ice chart for %s not finished on this date. Stopping." % regionstr)
        else:
            if (dow <= 5 and (outputs_flg <= 0 or archive_flg <= 0)):    #  or telexpts_flg <= 0
                print ("auto_disseminate: No output graphics or Shapefile for %s on this date. Stopping." % regionstr)
            elif (dow >= 6 and outputs_flg <= 0):
                print ("auto_disseminate: No output graphics or Shapefile for %s on this date. Stopping." % regionstr)
            else:
                if dissem_flg == 1 and forceflg == 0:
                    print ("auto_disseminate: Ice chart already sent for %s today. Stopping." % regionstr)
                    proc_flg = 0
                else:
                    proc_flg = 1
    # print regionstr, proc_flg
    
    # Flag for ice chart Shapefile creation
    createshp = 0
    
    # Flag for telex points Shapefile creation
    createtelex = 0
    
    # Only process if proc_flg = 1 or NEWSYS = 0
    if proc_flg == 1 or NEWSYS == 0:
    
        # Get list of customers
        today = datetime.today()
        sqltxt = ("SELECT id,name,language,salutation,area,style,format,email,enddate,dow FROM customer WHERE startdate <= \'%s\' AND enddate >= \'%s\' AND region = \'%s\' AND lastchart < \'%s\' ORDER BY id;" \
            % (dtstr,dtstr,regionstr,today.strftime('%Y-%m-%d')))
        # print sqltxt
        queryres = dbcon.query( sqltxt )
        ncust = queryres.ntuples()
        # print queryres
    
        # Connect to e-mail SMTP server
        # Outbox = smtplib.SMTP("smtp5.met.no")
        try:
            Outbox = smtplib.SMTP("smtp4.met.no")
        except:
            try:
                Outbox = smtplib.SMTP("smtp5.met.no")
            except:
                print 'No SMTP server available.'
    
        COMMASPACE = ', '
    
        # Loop through customer records and send e-mails
        custidlist = []
        for i in range(ncust):
    
            # Extract parameters from database query result
            record = queryres.getresult()[i]
            custid = int(record[0])
            custidlist.append( custid )
            custname = record[1]
            custlang = record[2]
            custsalut = record[3]
            custarea = record[4].split(',')
            custstyle = record[5].split(',')
            custformat = record[6].split(',')
            custemail = record[7].split(',')
            custend = record[8]
            custdow = record[9]
    
            # Only process customer if day-of-week is valid (Day-of-week in database is ISO standard, Monday = 1 and Sunday = 7)
            if custdow == 'all' or custdow.find(("%d" % dow)) > -1:
    
                # Output status header
                print ("Customer: %d   Name: %s   End-date: %s" % (custid,custname,custend))
    
                # List of outputs and their file size
                outlist = []
                fsizelist = []
                shortfn = []
                for area in custarea:
    
                    if area != 'telex':
                        # Map graphics and data
                        fname1 = ("%s/%d/%s/%s_%s" % (OUTDIR,iceyr,dtstr,area,dtstr))
                        for style in custstyle:
                            if style != 'colour':
                                fname2 = ("%s_%s" % (fname1,style))
                            else:
                                fname2 = fname1
                            for format in custformat:
                                if format == 'PNG' or format == 'JPG' or format == 'PDF':
                                    fname = ("%s.%s" % (fname2,format.lower()))
                                    # print fname
                                    outlist.append( fname )
                                    statinfo = os.stat(fname)
                                    fsizelist.append( int(statinfo.st_size/1024.0)+1 )
                                    fnbits = fname.split('/')
                                    shortfn.append( fnbits[-1] )
                                if format == 'SHP':
                                    if createshp == 0:
                                        [zipfn, createshp] = createshpzip( iceyr, icemh, icedy )
                                    outlist.append( zipfn )
                                    statinfo = os.stat(zipfn)
                                    fsizelist.append( int(statinfo.st_size/1024.0)+1 )
                                    fnbits = zipfn.split('/')
                                    shortfn.append( fnbits[-1] )                           
    
                    else:
                        # Telex points
                        for style in custstyle:
                            fname1 = ("%s/Telexpts/Outputs/ice_%s_telexpts" % (BASEDIR,dtstr))
                            for format in custformat:
                                if format == 'TXT':
                                    fname = ("%s.%s" % (fname1,format.lower()))
                                    outlist.append( fname )
                                    statinfo = os.stat(fname)
                                    fsizelist.append( int(statinfo.st_size/1024.0)+1 )
                                    fnbits = fname.split('/')
                                    shortfn.append( fnbits[-1] )
                                if format == 'SHP':
                                    if createtelex == 0:
                                        [telexzipfn, createtelex] = createtelexzip( iceyr, icemh, icedy )
                                    outlist.append( telexzipfn )
                                    statinfo = os.stat(telexzipfn)
                                    fsizelist.append( int(statinfo.st_size/1024.0)+1 )
                                    fnbits = telexzipfn.split('/')
                                    shortfn.append( fnbits[-1] )                           
                                if format == 'PNG' or format == 'JPG' or format == 'PDF':
                                    fname = ("%s.%s" % (fname1,format.lower()))
                                    # print fname
                                    outlist.append( fname )
                                    statinfo = os.stat(fname)
                                    fsizelist.append( int(statinfo.st_size/1024.0)+1 )
                                    fnbits = fname.split('/')
                                    shortfn.append( fnbits[-1] )
    
                nout = len(outlist)
                # print nout
                # print outlist
                # print fsizelist
                # print shortfn
    
                # E-mail addresses
                MyAddress = 'istjenesten@met.no'
                TargetAddress = custemail
                # ccAddress = [ MyAddress ]
                ccAddress = []
    
                # Create the container (outer) email message.
                msg = MIMEMultipart()
                if custarea[0] != 'telex':
                    msg['Subject'] = ("Ice chart %4d-%02d-%02d" % (iceyr,icemh,icedy))
                else:
                    msg['Subject'] = ("Telex Points for  %4d-%02d-%02d" % (iceyr,icemh,icedy))
                msg['From'] = MyAddress
                msg['To'] = COMMASPACE.join(TargetAddress)
                # CC to Istjenesten initially, remove when we are happy system is working
                # msg['Cc'] = MyAddress
                msg.preamble = "This message has been sent by the automatic product dissemination software of the Norwegian Ice Service."
                msg.preamble = ("%s Please contact %s if you have any questions.\r\n\r\n" \
                    % (msg.preamble,MyAddress))
    
                # Create the text part of the message
                text = ""
                if nout > 0:
                    if custarea[0] != 'telex':
                        text = ("%sPlease find attached the ice chart(s) for %4d-%02d-%02d.\r\n\r\n" \
                            % (text,iceyr,icemh,icedy))
                    else:
                        text = ("%sPlease find attached the telex points for %4d-%02d-%02d.\r\n\r\n" \
                            % (text,iceyr,icemh,icedy))
                text = ("%sThis message has been sent by the automatic product dissemination software of the Norwegian Ice Service." \
                    % text)
                text = ("%s Please contact %s if you have any questions.\r\n\r\n" \
                    % (text,MyAddress))
                if nout > 0:
                    text = ("%sThis message has as attachments the following files:\r\n" % text)
                    for i in range(nout):
                        text = ("%s    %30s [%d Kb]\r\n" \
                            % (text,shortfn[i],fsizelist[i]))
                else:
                    text = ("%sNo files are available for %4d-%02d-%02d.\r\n" \
                        % (text,iceyr,icemh,icedy))
                if custend == '2100-01-01':
                    text = ("%s\r\nThis service is set to run indefinitely.\r\n" % text)
                else:
                    text = ("%s\r\nThis service is set to run until %s.\r\n" % (text,custend))
                # print text
                part1 = MIMEText(text, 'plain')
    
                # Create the HTML version of the text
                html = """\
                <html>
                  <head></head>
                  <body>"""
                if nout > 0:
                    html = ("%s    <p>Please find attached the ice chart(s) for %4d-%02d-%02d.</p>\n" \
                        % (html,iceyr,icemh,icedy))
                html = ("%s    <p>This message has been sent by the automatic product dissemination software of the Norwegian Ice Service." \
                    % (html))
                html = ("%s Please contact %s if you have any questions.</p>\n" \
                    % (html,MyAddress))
                if nout > 0:
                    html = ("%s    <p>This message has as attachments the following files:<br>\n" \
                        % html)
                    for i in range(nout):
                        html = ("%s    <p>   %s [%d Kb]<br>\n" \
                            % (html,outlist[i],fsizelist[i]))
                else:
                    html = ("%s    <p>No files are available for %4d-%02d-%02d.<br>\n" \
                        % (html,iceyr,icemh,icedy))
     
                if custend == '2100-01-01':
                    html = ("%s    </p>\n    <p>This service is set to run indefinitely.</p>\n" % html)
                else:
                    html = ("%s    </p>\n    <p>This service is set to run until %s.</p>\n" % (html,custend))
    
                html = html+"""\
                  </body>
                </html>
                """
                # print html
                part2 = MIMEText(text, 'html')
    
                # Attach parts into message container
                msg.attach(part1)
                # msg.attach(part2)
    
                # Add the files if available
                if nout > 0:
                    for j in range(nout):
                        print ("  %s" % shortfn[j])
                        # Open the file in binary mode. Let the MIMEImage class automatically guess the specific image type.
                        imgfile = open(outlist[j], 'rb')
                        if shortfn[j][-3:] == 'pdf' or shortfn[j][-3:] == 'zip' or shortfn[j][-3:] == 'ped' or shortfn[j][-3:] == 'txt':
                            img = MIMEApplication(imgfile.read())
                        else:
                            img = MIMEImage(imgfile.read())
                        imgfile.close()
                        img.add_header('Content-Disposition', 'attachment; filename="%s"' % shortfn[j])
                        msg.attach(img)
    
                # Dump the message to screen to see what we are sending
                # print msg
    
                # Send mail via the SMTP server
                if activeflg == 1:
                    Outbox.sendmail(MyAddress,TargetAddress+ccAddress,msg.as_string())
    
                # On new system, set the last chart date for the customer record
                sqltxt = ("UPDATE customer SET (lastchart) = (\'%s\') WHERE id = %d;" % (icedt.strftime('%Y-%m-%d'),custid))
                print sqltxt
                if NEWSYS == 1 and activeflg == 1:
                    dbcon.query( sqltxt )
    
                print "\n"
    
                # Clear the message from memory
                del msg
    
        # Quit SMTP connection
        Outbox.quit()
    
        # On new system, enter database status
        if NEWSYS == 1:
            # Database status update
            timenow = datetime.now()
            outdtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
            sqltxt = ("UPDATE production SET (disseminate_flg,disseminate_dt) = (1,\'%s\') WHERE id = %d;"
                % (outdtstr,icechart_id))
            print sqltxt
            if activeflg == 1:
                dbcon.query( sqltxt )

# On new system, close connection
if NEWSYS == 1:
    # Close datebase connection
    dbcon.close()

# If we created a Shapefile zip archive, delete it.
if createshp == 1:
    os.remove(zipfn)
if createtelex == 1:
    os.remove(telexzipfn)

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** auto_disseminate.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

