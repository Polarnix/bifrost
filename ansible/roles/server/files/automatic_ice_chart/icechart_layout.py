#!/usr/bin/python

# Name:          icechart_layout.py
# Purpose:       Define ice chart elements layout for output graphics.
#                Includes options for  external ice services charts (Antarctic: NIC and AARI).
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, subprocess
import string, time
from datetime import datetime

import numpy as N

import pg

# from osgeo import gdal
# from osgeo import ogr
# from osgeo import osr
import osgeo.gdal as gdal
import osgeo.osr as osr
import osgeo.ogr as ogr
from osgeo.gdalconst import *
from pyproj import Proj
import mapscript

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import re

import icechart_plotting
from icechart_utils import thous, get_ice_class, get_region_codes, getproj
from file_utils import testoutdir

BASEDIR='/home/bifrostadmin'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Include/Automatic_Icechart" % BASEDIR)
ANCDIR=("%s/Ancillary/Automatic_Icechart" % BASEDIR)
OUTDIR=("/home/bifrostadmin/Outputs")

# Set the projection string
PROJSTR = '+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84'


def scalebar( draw, seglen, nseg, scale, xpos, ypos ):
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",33)
    #    Scale bar
    # seglen = 50000.0     # Segment length in metres
    #nseg = 5             # Number of segments
    # scale = mapxsz / (xmax-xmin) 
    segxsz = seglen * scale
    segysz = 40
    # print scale, segxsz
    for j in range(nseg):
        coords = [ (xpos+(j*segxsz),ypos), (xpos+((j+1)*segxsz),ypos+segysz) ]
        if j % 2 == 0:
            draw.rectangle( coords, outline=(0,0,0), fill=(0,0,0) )
        else:
            draw.rectangle( coords, outline=(0,0,0), fill=(255,255,255) )

    for j in range(nseg + 1):
        labelkm = ((j * seglen) / 1000.0)
        if j < nseg:
            coords = [ (xpos+(j*segxsz),ypos), \
                       (xpos+((j+1)*segxsz),ypos), \
                       (xpos+((j+1)*segxsz),ypos+segysz), \
                       (xpos+(j*segxsz),ypos+segysz), \
                       (xpos+(j*segxsz),ypos-20) ]        
            draw.line( coords, fill=(0,0,0), width=3 )
            labeltxt = ("%d" % labelkm)
        else:
            coords = [ (xpos+(j*segxsz),ypos+segysz), \
                       (xpos+(j*segxsz),ypos-20) ]        
            draw.line( coords, fill=(0,0,0), width=3 )
            labeltxt = ("%d km" % labelkm)
        img_txt = Image.new('L', font3.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        # xpos = ice_classes[j][2] - int(xsz / 2)
        # ypos = ypos + ysz + 5
        draw.text((xpos+(j*segxsz)-int(xsz/2), ypos-(20+ysz)),labeltxt,(0,0,0),font=font3)


def hatching( page, rect_coord, map_coord, ice_class ):
    # Size of box
    rectxsz = rect_coord[1][0] - rect_coord[0][0]
    rectysz = rect_coord[1][1] - rect_coord[0][1]
    xmin = map_coord[0]
    ymin = map_coord[1]
    xmax = map_coord[2]
    ymax = map_coord[3]

    # Setup image in Mapscript for legend box
    format = mapscript.outputFormatObj('GD/PNG', 'PNG' )
    format.setOption( 'imagemode', 'MS_IMAGEMODE_PC256' )
    format.setMimetype( 'image/png' )
    image = mapscript.imageObj(rectxsz, rectysz, format)
    white = mapscript.colorObj()
    white.setRGB( 255, 255, 255 )

    # Define map object to go onto image
    legendmap = mapscript.mapObj()
    legendmap.name = "Legend Item"
    legendmap.setSize( rectxsz, rectysz )
    legendmap.units = mapscript.MS_METERS
    legendmap.setExtent( xmin, ymin, xmax, ymax )
    legendmap.setProjection( PROJSTR )
    legendmap.selectOutputFormat( 'PNG' )
    legendmap.imagecolor = white

    # Symbols
    symbol_set = icechart_plotting.define_symbols( legendmap )

    # Define legend polygon layer
    legendpoly = mapscript.layerObj( legendmap )
    legendpoly.name = 'Legend Polygon'
    legendpoly.type = mapscript.MS_LAYER_POLYGON
    legendpoly.setProjection( PROJSTR )
    # Define class and style
    legendclass =  mapscript.classObj( legendpoly )
    legendstyle = mapscript.styleObj( legendclass )

    # Define polygon
    outring = ogr.Geometry(ogr.wkbLinearRing)
    outring.AddPoint(xmin,ymax)
    outring.AddPoint(xmax,ymax)
    outring.AddPoint(xmax,ymin)
    outring.AddPoint(xmin,ymin)
    outring.CloseRings()
    polygeom = ogr.Geometry(ogr.wkbPolygon)
    polygeom.AddGeometry(outring)

    poly = mapscript.shapeObj().fromWKT( polygeom.ExportToWkt() )
    poly.initValues(1)
    # print i, typelist[j]
    poly.setValue(0, ice_class)
    # print polylist[i][0:10], poly.type
    legendpoly.addFeature( poly )

    legendpoly.status = mapscript.MS_ON

    # Define class for ice concentration
    legendclass.name = ice_class

    # Chart style for cross-hatching
    rgb = [ 25, 25, 61 ]
    legendstyle.color.setRGB( rgb[0], rgb[1], rgb[2])

    if ice_class == 'Open Water':
        legendstyle.symbol = symbol_set[0]
        legendstyle.size = 2
        legendstyle.width = 2
        legendstyle.gap = 4
    elif ice_class == 'Very Open Drift Ice':
        legendstyle.symbol = symbol_set[1]
        legendstyle.size = 15
        legendstyle.width = 2
        legendstyle.gap = 5
    elif ice_class == 'Open Drift Ice':
        legendstyle.symbol = symbol_set[2]
        legendstyle.size = 15
        legendstyle.width = 2
        legendstyle.gap = 15
    elif ice_class == 'Close Drift Ice':
        legendstyle.symbol = symbol_set[3]
        legendstyle.size = 15
        legendstyle.width = 2
        legendstyle.angle = 0
    elif ice_class == 'Very Close Drift Ice':
        legendstyle.symbol = symbol_set[4]
        legendstyle.size = 15
        legendstyle.width = 2
        legendstyle.angle = 90
        substyle = mapscript.styleObj( legendclass )
        substyle.color.setRGB( rgb[0], rgb[1], rgb[2])
        substyle.symbol = symbol_set[4]
        substyle.size = 15
        substyle.width = 2
        substyle.angle = 0
    elif ice_class == 'Fast Ice':
        legendstyle.symbol = symbol_set[5]
        legendstyle.size = 15
        legendstyle.width = 2
        legendstyle.angle = 45
    elif ice_class == 'Iceberg':
        legendstyle.color.setRGB( 255, 0, 0 )
        legendstyle.symbol = symbol_set[5]
        legendstyle.size = 10
        legendstyle.width = 3
        legendstyle.angle = 45

    # Set anti-aliasing if required
    aaflg = 1
    if aaflg == 1:
        legendstyle.antialias = True

    # Draw layer onto image
    legendpoly.draw( legendmap, image )

    # Output temporary PNG of the legend
    tmppngfname = ("%s/%s_tmp." % \
        (TMPDIR,ice_class)) + image.format.extension
    image.save(tmppngfname,legendmap)

    # Add legend graphic
    icpng = Image.open(tmppngfname,'r')

    # Set transparent areas in icpng to white
    icpng = icpng.convert('RGBA')
    data = N.array(icpng)
    red, green, blue, alpha = data.T # Temporarily unpack the bands for readability
    transparent_areas = N.nonzero(alpha == 0)
    # print transparent_areas
    # print data.shape
    data[transparent_areas[1],transparent_areas[0],:] = [255, 255, 255, 255]
    icpng2 = Image.fromarray(data)

    page.paste(icpng2, (rect_coord[0][0],rect_coord[0][1]) )

    # Remove temporary graphic
    os.remove( tmppngfname )


def nicberg_legend( page, rect_coord, map_coord, regionstr ):

    # Get map projections
    PROJSTR = getproj()

    # Size of box
    rectxsz = rect_coord[1][0] - rect_coord[0][0]
    rectysz = rect_coord[1][1] - rect_coord[0][1]
    xmin = map_coord[0]
    ymin = map_coord[1]
    xmax = map_coord[2]
    ymax = map_coord[3]

    # Setup image in Mapscript for legend box
    format = mapscript.outputFormatObj('AGG/PNG', 'PNG24' )
    format.setOption( 'imagemode', 'MS_IMAGEMODE_RGBA' )
    format.setMimetype( 'image/png' )
    image = mapscript.imageObj(rectxsz, rectysz, format)
    white = mapscript.colorObj()
    white.setRGB( 255, 255, 255 )

    # Define map object to go onto image
    legendmap = mapscript.mapObj()
    legendmap.name = "Legend Item"
    legendmap.setSize( rectxsz, rectysz )
    legendmap.units = mapscript.MS_METERS
    legendmap.setExtent( xmin, ymin, xmax, ymax )
    legendmap.setProjection( PROJSTR[regionstr] )
    legendmap.selectOutputFormat( 'PNG' )
    legendmap.imagecolor = white

    # Define symbol
    berg_symb_fn = ("%s/Ice_Symbols/41_Tabular_Iceberg_Very_Large.png" % INCDIR)
    berg_symb = mapscript.imageObj(berg_symb_fn, 'GD/PNG')
    symbol = mapscript.symbolObj('berg_from_img',berg_symb_fn)
    symbol.name = 'berg_from_img'
    symbol.type = mapscript.MS_SYMBOL_PIXMAP
    symbol_index = legendmap.symbolset.appendSymbol(symbol)

    # Define legend point layer
    legend_pt = mapscript.layerObj( legendmap )
    legend_pt.name = 'Legend Polygon'
    legend_pt.type = mapscript.MS_LAYER_POINT
    legend_pt.setProjection( PROJSTR[regionstr] )
    # Define class and style
    legendclass = mapscript.classObj( legend_pt )
    legendclass.name = 'NIC Iceberg Symbol'
    legendstyle = mapscript.styleObj( legendclass )
    legendstyle.symbol = symbol_index
    legendstyle.size = 50

    # Define point
    point = ogr.Geometry(ogr.wkbPoint)
    point.AddPoint(xmin+(0.25*(xmax+xmin)),0.5*(ymin+ymax))
    map_pt = mapscript.shapeObj().fromWKT( point.ExportToWkt() )
    legend_pt.addFeature( map_pt )

    legend_pt.status = mapscript.MS_ON

    # Set anti-aliasing if required
    aaflg = 1
    if aaflg == 1:
        legendstyle.antialias = True

    # Draw layer onto image
    legend_pt.draw( legendmap, image )

    # Output temporary PNG of the legend
    tmppngfname = ("%s/nicberg_tmp." % \
        (TMPDIR)) + image.format.extension
    image.save(tmppngfname,legendmap)

    # Add legend graphic
    icpng = Image.open(tmppngfname,'r')

    # Set transparent areas in icpng to white
    icpng = icpng.convert('RGBA')
    data = N.array(icpng)
    red, green, blue, alpha = data.T # Temporarily unpack the bands for readability
    transparent_areas = N.nonzero(alpha == 0)
    # print transparent_areas
    # print data.shape
    data[transparent_areas[1],transparent_areas[0],:] = [255, 255, 255, 255]
    icpng2 = Image.fromarray(data)

    page.paste(icpng2, (rect_coord[0][0],rect_coord[0][1]) )

    # Remove temporary graphic
    os.remove( tmppngfname )


def legend_landscape( regionstr, draw, page, cornerx, cornery, mapcornerx, mapcornery, \
    mappwidth, boxflg, chart_style, mapscale, sstflg, sstlblflg, servstr ):

    # Get projection information
    PROJSTR = getproj()

    xmin = mapcornerx[0]
    xmax = mapcornerx[1]
    ymin = mapcornery[2]
    ymax = mapcornery[0]
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",36)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",24)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",20)
    font5 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",32)
    font6 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",24)

    # Bounding box
    if boxflg == 1:
        coords = [ (cornerx[0],cornery[0]), (cornerx[2],cornery[2]) ]       
        draw.rectangle( coords, outline=(0,0,0), fill=(255,255,255) )
        coords = [ (cornerx[0],cornery[0]), (cornerx[1],cornery[1]), (cornerx[2],cornery[2]), \
                   (cornerx[3],cornery[3]), (cornerx[0],cornery[0]) ]        
        draw.line( coords, fill=(0,0,0), width=3 )

    # Add coastline text
    if regionstr == 'arctic':
        labeltxt = "Coastline Data: GSHHS version 2.3.5 (http://www.soest.hawaii.edu/wessel/gshhs/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20
        ypos = cornery[0] - (ysz + 20)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos
    elif regionstr == 'antarctic':
        labeltxt = "Antarctic Digtial Database 7.0 (http://www.add.scar.org/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 170
        ypos = cornery[0] - (ysz + 20)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos
        labeltxt = "Coastline Data:"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20
        ypos = ycount - (ysz + 5)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        labeltxt = "GSHHS version 2.3.5 (http://www.soest.hawaii.edu/wessel/gshhs/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 170
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos

    # Add map corners text
    p1 = Proj(PROJSTR[regionstr])
    cornertxt = [ 'UL =', 'UR =', 'LR = ', 'LL =' ] 
    cornerlon = [ xmin, xmax, xmax, xmin ]
    cornerlat = [ ymax, ymax, ymin, ymin ]
    for j in range(4):
        cornerlon[j], cornerlat[j] = p1( cornerlon[j], cornerlat[j], inverse=True )
        # print cornerlon[j], cornerlat[j]
    cornerlist = []
    for j in range(4):
        if cornerlon[j] < 0:
            lonhem = 'W'
        else:
            lonhem = 'E'
        rawlon = N.abs( cornerlon[j] )
        londeg = int( rawlon )
        lonmin = int( (rawlon - londeg) * 60.0 )
        lonsec = (rawlon - (londeg + (float(lonmin) / 60.0) )) * 3600.0
        if cornerlat[j] < 0:
            lathem = 'S'
        else:
            lathem = 'N'
        rawlat = N.abs( cornerlat[j] )
        latdeg = int( rawlat )
        latmin = int( (rawlat - latdeg) * 60.0 )
        latsec = (rawlat - (latdeg + (float(latmin) / 60.0) )) * 3600.0
        # print cornerlon[j], londeg, lonmin, lonsec, lonhem
        # print cornerlat[j], latdeg, latmin, latsec, lathem
        labeltxt = (u"  %s %d\xB0'%d\'%6.3f\x22%s,%d\xB0'%d\'%6.3f\x22%s" % (cornertxt[j], \
            latdeg,latmin,latsec,lathem, londeg,lonmin,lonsec,lonhem))
        cornerlist.append(labeltxt)
    # print cornerlist
    for j in range(3,-1,-1):
        img_txt = Image.new('L', font4.getsize(cornerlist[j]))
        [xsz,ysz] = img_txt.size
        if j % 2 == 0:
            xpos = cornerx[0] + 20
            ycount = ypos
        else:
            xpos = cornerx[0] + 420
            ypos = ycount - (ysz + 5)
        draw.text((xpos, ypos),cornerlist[j],(0,0,0),font=font4)
        ycount = ypos
    labeltxt = "Map Corners: "
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    ycount = ypos

    # Add projection and scale text
    # print (pwidth - (2.0 * pmargin))
    # print xmax, xmin, ((xmax-xmin)*1000.0)
    scalestr = thous( int( (xmax-xmin) / mappwidth ) )
    labeltxt = (u"Scale: %s" % scalestr)
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 620
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    if regionstr == 'arctic':
        labeltxt = (u"Projection: Polar Stereographic, True Scale at 90\xB0N, WGS84")
    elif regionstr == 'antarctic':
        labeltxt = (u"Projection: Polar Stereographic, True Scale at 90\xB0S, WGS84")
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 20
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    ycount = ypos + 10

    # Legend symbol size
    rectxsz = 180
    rectysz = 60

    # Add Copernicus CMEMS logo here
    logopngfname = ("%s/Copernicus_Logo.png" % (INCDIR))
    logopng = Image.open(logopngfname,'r')
    logoxsz, logoysz = logopng.size
    logoratio = float(logoxsz) / float(logoysz)
    newxsz = 100
    newysz = int(newxsz / logoratio)
    logopng = logopng.resize((newxsz,newysz),Image.ANTIALIAS)
    xpos = cornerx[0] + 850
    ypos = ycount + 20
    page.paste(logopng, (xpos,ypos))

    # Add Sentinel-1 and Radarsat-2 coverage
    linexsz = rectxsz / 2
    xpos = cornerx[0] + 850 + 100 + 20
    ypos = ycount + 60
    coords = [ (xpos,ypos), (xpos+linexsz,ypos) ]
    draw.line( coords, fill=(0,0,0), width=10 )
    labeltxt = 'Sentinel-1'
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = xpos + linexsz + 20
    ypos = ypos - ysz
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    ypos = ypos + ysz
    labeltxt = 'Radarsat-2'
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    xpos = xpos + xsz

    # Add SST coverage
    if sstflg == 1:
        xpos = xpos + 20
        ypos = ycount + 60
        coords = [ (xpos,ypos), (xpos+linexsz,ypos) ]
        draw.line( coords, fill=(255,62,3), width=5 )
        if sstlblflg == 1:
            labeltxt = '0'
            img_txt = Image.new('L', font5.getsize(labeltxt))
            [lab_xsz,lab_ysz] = img_txt.size
            lab_xpos = xpos + ( linexsz / 2) - (lab_xsz / 2)
            lab_ypos = ypos - (lab_ysz / 2)
            draw.text((lab_xpos, lab_ypos),labeltxt,(0,0,0),font=font5)
        labeltxt = 'Sea Surface'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = xpos + linexsz + 20
        ypos = ypos - ysz
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
        ypos = ypos + ysz
        labeltxt = 'Temperature'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)

    # Add ice concentration classes
    ice_classes = get_ice_class(regionstr)
    if regionstr == 'arctic':
        nrows = 2
    elif regionstr == 'antarctic':
        nrows = 3
    yorig = ycount
    xstep = 0
    for j in range(len(ice_classes)):
        xpos = cornerx[0] + 20 + xstep
        ypos = yorig - ((nrows-(j%nrows)) * (rectysz + 20))

        coords = [ (xpos,ypos), (xpos+rectxsz,ypos+rectysz) ]       
        if chart_style == 'colour':
            if ice_classes[j][0] == 'Iceberg':
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                hatching( page, coords, map_coord, ice_classes[j][0] )
            elif ice_classes[j][0] == 'NIC Iceberg':
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                nicberg_legend( page, coords, map_coord, regionstr )
            else:
                draw.rectangle( coords, outline=(0,0,0), fill=ice_classes[j][2] )
        else:
            if ice_classes[j][0] == 'Ice Shelf':
                draw.rectangle( coords, outline=(0,0,0), fill=ice_classes[j][2] )
            elif ice_classes[j][0] == 'NIC Iceberg':
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                nicberg_legend( page, coords, map_coord, regionstr )
            else:
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                hatching( page, coords, map_coord, ice_classes[j][0] )
        if ice_classes[j][0] != 'NIC Iceberg':
            coords = [ (xpos,ypos),  \
                       (xpos+rectxsz,ypos), \
                       (xpos+rectxsz,ypos+rectysz), \
                       (xpos,ypos+rectysz), \
                       (xpos,ypos) ]
            draw.line( coords, fill=(0,0,0), width=3 )

        # 1/10th label in box
        labeltxt = ice_classes[j][1]
        if ice_classes[j][0] == 'Iceberg' or ice_classes[j][0] == 'NIC Iceberg':
            img_txt = Image.new('L', font6.getsize(labeltxt))
        else:
            img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20 + xstep + int(rectxsz/2) - int(xsz / 2)
        ypos = ypos + 15
        if ice_classes[j][0] == 'Iceberg':
            draw.text((xpos, ypos),labeltxt,(0,0,0),font=font6)
        elif ice_classes[j][0] == 'NIC Iceberg':
            draw.text((xpos+40, ypos),labeltxt,(0,0,0),font=font6)
        else:
            draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)

        # Ice Class name
        labeltxt = ice_classes[j][0]
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20 + xstep + rectxsz + 20
        ypos = ypos - 5
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)

        # Update ycount
        ycount = ycount - (rectysz + 20)
        # Update xstep
        if (j % nrows) == (nrows-1):
            xstep = xstep + 560
    ycount = yorig - (nrows * (rectysz + 20))

    # Ice Class name
    labeltxt = "Ice Categories"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 20
    ypos = ycount - (ysz + 15)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)


def legend_portrait( regionstr, draw, page, cornerx, cornery, mapcornerx, mapcornery, \
    mappwidth, boxflg, chart_style, mapscale, sstflg, sstlblflg, servstr ):

    # Get projection information
    PROJSTR = getproj()

    xmin = mapcornerx[0]
    xmax = mapcornerx[1]
    ymin = mapcornery[2]
    ymax = mapcornery[0]
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",36)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",24)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",20)
    font5 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",32)
    font6 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",24)

    # Bounding box
    if boxflg == 1:
        coords = [ (cornerx[0],cornery[0]), (cornerx[2],cornery[2]) ]       
        draw.rectangle( coords, outline=(0,0,0), fill=(255,255,255) )
        coords = [ (cornerx[0],cornery[0]), (cornerx[1],cornery[1]), (cornerx[2],cornery[2]), \
                   (cornerx[3],cornery[3]), (cornerx[0],cornery[0]) ]        
        draw.line( coords, fill=(0,0,0), width=3 )

    # Add coastline text
    if regionstr == 'arctic':
        labeltxt = "Coastline Data: GSHHS version 2.3.5 (http://www.soest.hawaii.edu/wessel/gshhs/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20
        ypos = cornery[0] - (ysz + 20)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos
    elif regionstr == 'antarctic':
        labeltxt = "Antarctic Digtial Database 7.0 (http://www.add.scar.org/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 170
        ypos = cornery[0] - (ysz + 20)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos
        labeltxt = "Coastline Data:"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20
        ypos = ycount - (ysz + 5)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        labeltxt = "GSHHS version 2.3.5 (http://www.soest.hawaii.edu/wessel/gshhs/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 170
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos

    # Add map corners text
    p1 = Proj(PROJSTR[regionstr])
    cornertxt = [ 'UL =', 'UR =', 'LR = ', 'LL =' ] 
    cornerlon = [ xmin, xmax, xmax, xmin ]
    cornerlat = [ ymax, ymax, ymin, ymin ]
    for j in range(4):
        cornerlon[j], cornerlat[j] = p1( cornerlon[j], cornerlat[j], inverse=True )
        # print cornerlon[j], cornerlat[j]
    cornerlist = []
    for j in range(4):
        if cornerlon[j] < 0:
            lonhem = 'W'
        else:
            lonhem = 'E'
        rawlon = N.abs( cornerlon[j] )
        londeg = int( rawlon )
        lonmin = int( (rawlon - londeg) * 60.0 )
        lonsec = (rawlon - (londeg + (float(lonmin) / 60.0) )) * 3600.0
        if cornerlat[j] < 0:
            lathem = 'S'
        else:
            lathem = 'N'
        rawlat = N.abs( cornerlat[j] )
        latdeg = int( rawlat )
        latmin = int( (rawlat - latdeg) * 60.0 )
        latsec = (rawlat - (latdeg + (float(latmin) / 60.0) )) * 3600.0
        # print cornerlon[j], londeg, lonmin, lonsec, lonhem
        # print cornerlat[j], latdeg, latmin, latsec, lathem
        labeltxt = (u"  %s %d\xB0'%d\'%6.3f\x22%s,%d\xB0'%d\'%6.3f\x22%s" % (cornertxt[j], \
            latdeg,latmin,latsec,lathem, londeg,lonmin,lonsec,lonhem))
        cornerlist.append(labeltxt)
    # print cornerlist
    for j in range(3,-1,-1):
        img_txt = Image.new('L', font4.getsize(cornerlist[j]))
        [xsz,ysz] = img_txt.size
        if j % 2 == 0:
            xpos = cornerx[0] + 20
            ycount = ypos
        else:
            xpos = cornerx[0] + 420
            ypos = ycount - (ysz + 5)
        draw.text((xpos, ypos),cornerlist[j],(0,0,0),font=font4)
        ycount = ypos
    labeltxt = "Map Corners: "
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    ycount = ypos

    # Add projection and scale text
    # print (pwidth - (2.0 * pmargin))
    # print xmax, xmin, ((xmax-xmin)*1000.0)
    scalestr = thous( int( (xmax-xmin) / mappwidth ) )
    labeltxt = (u"Scale: %s" % scalestr)
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 620
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    if regionstr == 'arctic':
        labeltxt = (u"Projection: Polar Stereographic, True Scale at 90\xB0N, WGS84")
    elif regionstr == 'antarctic':
        labeltxt = (u"Projection: Polar Stereographic, True Scale at 90\xB0S, WGS84")
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 20
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    ycount = ypos + 10

    # Legend symbol size
    rectxsz = 180
    rectysz = 60

    # Add Copernicus logo here
    logopngfname = ("%s/Copernicus_Logo.png" % (INCDIR))
    logopng = Image.open(logopngfname,'r')
    logoxsz, logoysz = logopng.size
    logoratio = float(logoxsz) / float(logoysz)
    newxsz = 100
    newysz = int(newxsz / logoratio)
    logopng = logopng.resize((newxsz,newysz),Image.ANTIALIAS)
    xpos = cornerx[0] + 850
    ypos = ycount + 20
    page.paste(logopng, (xpos,ypos))

    # Add Sentinel-1 and Radarsat-2 coverage
    linexsz = rectxsz / 2
    xpos = cornerx[0] + 850 + 100 + 20
    ypos = ycount + 60
    coords = [ (xpos,ypos), (xpos+linexsz,ypos) ]
    draw.line( coords, fill=(0,0,0), width=10 )
    labeltxt = 'Sentinel-1'
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = xpos + linexsz + 20
    ypos = ypos - ysz
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    ypos = ypos + ysz
    labeltxt = 'Radarsat-2'
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    xpos = xpos + xsz

    # Add SST coverage
    if sstflg == 1:
        xpos = xpos + 20
        ypos = ycount + 60
        coords = [ (xpos,ypos), (xpos+linexsz,ypos) ]
        draw.line( coords, fill=(255,62,3), width=5 )
        if sstlblflg == 1:
            labeltxt = '0'
            img_txt = Image.new('L', font5.getsize(labeltxt))
            [lab_xsz,lab_ysz] = img_txt.size
            lab_xpos = xpos + ( linexsz / 2) - (lab_xsz / 2)
            lab_ypos = ypos - (lab_ysz / 2)
            draw.text((lab_xpos, lab_ypos),labeltxt,(0,0,0),font=font5)
        labeltxt = 'Sea Surface'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = xpos + linexsz + 20
        ypos = ypos - ysz
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
        ypos = ypos + ysz
        labeltxt = 'Temperature'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)

    # Add ice concentration classes
    ice_classes = get_ice_class(regionstr)
    if regionstr == 'arctic':
        nrows = 2
    elif regionstr == 'antarctic':
        nrows = 3
    yorig = ycount
    xstep = 0
    for j in range(len(ice_classes)):
        xpos = cornerx[0] + 20 + xstep
        ypos = yorig - ((nrows-(j%nrows)) * (rectysz + 20))

        coords = [ (xpos,ypos), (xpos+rectxsz,ypos+rectysz) ]       
        if chart_style == 'colour':
            if ice_classes[j][0] == 'Iceberg':
                pass
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                hatching( page, coords, map_coord, ice_classes[j][0] )
            elif ice_classes[j][0] == 'NIC Iceberg':
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                nicberg_legend( page, coords, map_coord, regionstr )
            else:
                draw.rectangle( coords, outline=(0,0,0), fill=ice_classes[j][2] )
        else:
            if ice_classes[j][0] == 'Ice Shelf':
                draw.rectangle( coords, outline=(0,0,0), fill=ice_classes[j][2] )
            elif ice_classes[j][0] == 'NIC Iceberg':
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                nicberg_legend( page, coords, map_coord, regionstr )
            else:
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                hatching( page, coords, map_coord, ice_classes[j][0] )
        if ice_classes[j][0] != 'NIC Iceberg':
            coords = [ (xpos,ypos),  \
                       (xpos+rectxsz,ypos), \
                       (xpos+rectxsz,ypos+rectysz), \
                       (xpos,ypos+rectysz), \
                       (xpos,ypos) ]
            draw.line( coords, fill=(0,0,0), width=3 )

        # 1/10th label in box
        labeltxt = ice_classes[j][1]
        if ice_classes[j][0] == 'Iceberg' or ice_classes[j][0] == 'NIC Iceberg':
            img_txt = Image.new('L', font6.getsize(labeltxt))
        else:
            img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20 + xstep + int(rectxsz/2) - int(xsz / 2)
        ypos = ypos + 15
        if ice_classes[j][0] == 'Iceberg':
            draw.text((xpos, ypos),labeltxt,(0,0,0),font=font6)
        elif ice_classes[j][0] == 'NIC Iceberg':
            draw.text((xpos+40, ypos),labeltxt,(0,0,0),font=font6)
        else:
            draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)

        # Ice Class name
        labeltxt = ice_classes[j][0]
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20 + xstep + rectxsz + 20
        ypos = ypos - 5
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)

        # Update ycount
        ycount = ycount - (rectysz + 20)
        # Update xstep
        if (j % nrows) == (nrows-1):
            xstep = xstep + 560
    ycount = yorig - (nrows * (rectysz + 20))

    # Ice Class name
    labeltxt = "Ice Categories"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 20
    ypos = ycount - (ysz + 15)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)


def header_landscape( regionstr, draw, page, imginfo, cornerx, cornery ):
    font1 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",42)
    font1aa = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-BoldOblique.ttf",42)
    font1a = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Oblique.ttf",36)
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",32)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",28)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed-Bold.ttf",36)

    # Get values out of imginfo
    area_name = imginfo[0]
    day = imginfo[1]
    month = imginfo[2]
    year = imginfo[3]
    coverage = imginfo[4]
    coast = imginfo[5]
    # print imginfo

    # Draw rectangular box at side of map
    coords = [ (cornerx[0],cornery[0]), (cornerx[2],cornery[2]) ]       
    draw.rectangle( coords, outline=(0,0,0), fill=(255,255,255) )
    coords = [ (cornerx[0],cornery[0]), (cornerx[1],cornery[1]), (cornerx[2],cornery[2]), \
               (cornerx[3],cornery[3]), (cornerx[0],cornery[0]) ]        
    draw.line( coords, fill=(0,0,0), width=3 )

    # Add met.no logo
    logopngfname = ("%s/institute_logo.png" % (INCDIR))
    logopng = Image.open(logopngfname,'r')
    logoxsz, logoysz = logopng.size
    logoratio = float(logoxsz) / float(logoysz)
    newxsz = 500
    newysz = int(newxsz / logoratio)
    logopng = logopng.resize((newxsz,newysz),Image.ANTIALIAS)
    page.paste(logopng, (cornerx[0]+2,cornery[2]+10))
    ycount = cornery[2]+newysz

    # Add "Ice Service"
    labeltxt = "Ice Service"
    img_txt = Image.new('L', font1aa.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = (cornerx[0]+2)+int((newxsz/2)-(xsz/2))
    ypos = ycount + 10
    draw.text((xpos, ypos),labeltxt,(0,144,168),font=font1aa)

    # Add title text
    month_names = [ 'January', 'February', 'March', 'April', 'May', 'June', \
                    'July', 'August', 'September', 'October', 'November', 'December' ]
    monthstr = month_names[month-1]
    if day % 10 == 1:
        if day != 11:
            daysuffix = 'st'
        else:
            daysuffix = 'th'
    elif day % 10 == 2:
        if day != 12:
            daysuffix = 'nd'
        else:
            daysuffix = 'th'
    elif day % 10 == 3:
        if day != 13:
            daysuffix = 'rd'
        else:
            daysuffix = 'th'
    else:
        daysuffix = 'th'
    # Title line 1 "Date"
    labeltxt = ("%d%s %s %d" % (day,daysuffix,monthstr,year))
    img_txt = Image.new('L', font1.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz + 10)
    ypos = cornery[2] + 40
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font1)
    ycount = ypos + ysz
    # Title line 2 "Time"
    labeltxt = ("Valid 15:00 UTC")
    img_txt = Image.new('L', font1a.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz + 10)
    ypos = ycount + 10
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font1a)
    ycount = ycount + 10 + ysz

    # If Antarctic, add collabaorative product details
    if regionstr == 'antarctic':
        # Add NIC logo
        logopngfname = ("%s/NIC_Logo.png" % (INCDIR))
        logopng = Image.open(logopngfname,'r')
        logopng = logopng.resize((120,120),Image.ANTIALIAS)
        xpos = cornerx[2] - (120+80+10)
        ypos = ycount + 30
        page.paste(logopng, (xpos,ypos))

        # Add AARI logo
        logopngfname = ("%s/AARI_Logo.png" % (INCDIR))
        logopng = Image.open(logopngfname,'r')
        logopng = logopng.resize((125,120),Image.ANTIALIAS)
        xpos = xpos - (125+30)
        page.paste(logopng, (xpos,ypos))

        # Add MET Norway logo
        logopngfname = ("%s/METNorway_Logo.png" % (INCDIR))
        logopng = Image.open(logopngfname,'r')
        logopng = logopng.resize((75,120),Image.ANTIALIAS)
        xpos = xpos - (75+30)
        page.paste(logopng, (xpos,ypos))

        ycount = ypos + 120

        # Add collaborative product text
        labeltxt = ("Antarctic Collaborative Product")
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[2] - (xsz + 10)
        ypos = ycount + 10
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ycount + ysz

    # Add address text
    labeltxt = "Forecasting Division for Northern Norway"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 50
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    ycount = ypos + ysz

    labeltxt = u"N-9293 Troms\xF8, Norway"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 5
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    ycount = ypos + ysz

    # Add contact text
    labeltxt = "Tel: +47 77 62 14 62   Fax: +47 77 62 13 01   E-mail: istjenesten@met.no"
    img_txt = Image.new('L', font3.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 5
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font3)
    ycount = ycount + 5 + ysz


def header_portrait( regionstr, draw, page, imginfo, cornerx, cornery ):
    font1 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",42)
    font1aa = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-BoldOblique.ttf",42)
    font1a = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Oblique.ttf",36)
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",32)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",28)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed-Bold.ttf",36)

    # Get values out of imginfo
    area_name = imginfo[0]
    day = imginfo[1]
    month = imginfo[2]
    year = imginfo[3]
    coverage = imginfo[4]
    coast = imginfo[5]
    # print imginfo

    # Draw rectangular box at side of map
    coords = [ (cornerx[0],cornery[0]), (cornerx[2],cornery[2]) ]       
    draw.rectangle( coords, outline=(0,0,0), fill=(255,255,255) )
    coords = [ (cornerx[0],cornery[0]), (cornerx[1],cornery[1]), (cornerx[2],cornery[2]), \
               (cornerx[3],cornery[3]), (cornerx[0],cornery[0]) ]        
    draw.line( coords, fill=(0,0,0), width=3 )

    # Add met.no logo
    logopngfname = ("%s/institute_logo.png" % (INCDIR))
    logopng = Image.open(logopngfname,'r')
    logoxsz, logoysz = logopng.size
    logoratio = float(logoxsz) / float(logoysz)
    newxsz = 500
    newysz = int(newxsz / logoratio)
    logopng = logopng.resize((newxsz,newysz),Image.ANTIALIAS)
    page.paste(logopng, (cornerx[0]+2,cornery[2]+10))
    ycount = cornery[2]+newysz

    # Add "Ice Service"
    labeltxt = "Ice Service"
    img_txt = Image.new('L', font1aa.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = (cornerx[0]+2)+int((newxsz/2)-(xsz/2))
    ypos = ycount + 10
    draw.text((xpos, ypos),labeltxt,(0,144,168),font=font1aa)

    # If Antarctic, add collaborative product details
    if regionstr == 'antarctic':
        # Add MET Norway logo
        logopngfname = ("%s/METNorway_Logo.png" % (INCDIR))
        logopng = Image.open(logopngfname,'r')
        logopng = logopng.resize((75,120),Image.ANTIALIAS)
        xpos = cornerx[0] + 2 + 500 + 60
        ypos = cornery[2] + 20
        page.paste(logopng, (xpos,ypos))

        # Add AARI logo
        logopngfname = ("%s/AARI_Logo.png" % (INCDIR))
        logopng = Image.open(logopngfname,'r')
        logopng = logopng.resize((125,120),Image.ANTIALIAS)
        xpos = xpos + (75+30)
        page.paste(logopng, (xpos,ypos))

        # Add NIC logo
        logopngfname = ("%s/NIC_Logo.png" % (INCDIR))
        logopng = Image.open(logopngfname,'r')
        logopng = logopng.resize((120,120),Image.ANTIALIAS)
        xpos = xpos + (125+30)
        page.paste(logopng, (xpos,ypos))

        ycount = ypos + 120

        # Add collaborative product text
        labeltxt = ("Antarctic Collaborative")
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 2 + 500 + 60 + (380/2) - (xsz/2)
        ypos = ycount + 10
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ycount + ysz
        labeltxt = ("Product")
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 2 + 500 + 60 + (380/2) - (xsz/2)
        ypos = ycount + 10
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ycount + ysz

    # Add title text
    month_names = [ 'January', 'February', 'March', 'April', 'May', 'June', \
                    'July', 'August', 'September', 'October', 'November', 'December' ]
    monthstr = month_names[month-1]
    if day % 10 == 1:
        if day != 11:
            daysuffix = 'st'
        else:
            daysuffix = 'th'
    elif day % 10 == 2:
        if day != 12:
            daysuffix = 'nd'
        else:
            daysuffix = 'th'
    elif day % 10 == 3:
        if day != 13:
            daysuffix = 'rd'
        else:
            daysuffix = 'th'
    else:
        daysuffix = 'th'
    # Title line 1 "Date"
    labeltxt = ("%d%s %s %d" % (day,daysuffix,monthstr,year))
    img_txt = Image.new('L', font1.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz + 10)
    ypos = cornery[2] + 20
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font1)
    ycount = ypos + ysz
    # Title line 2 "Time"
    labeltxt = ("Valid 15:00 UTC")
    img_txt = Image.new('L', font1a.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz + 10)
    ypos = ycount + 10
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font1a)
    ycount = ycount + 10 + ysz

    # Add address text
    # labeltxt = u"Norwegian Ice Service, Forecasting Division for Northern Norway, N-9293 Troms\xF8, Norway"

    labeltxt = "Forecasting Division for Northern Norway, N-9293 Troms\xF8, Norway"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 40
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    ycount = ypos + ysz

    # Add contact text
    labeltxt = "Tel: +47 77 62 14 62   Fax: +47 77 62 13 01   E-mail: istjenesten@met.no"
    img_txt = Image.new('L', font3.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 5
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font3)
    ycount = ycount + 5 + ysz


# Main ice chart layout routine
def layout_png( regionstr, imginfo, papersize, orientation, paperdpi, legendloc, sbarloc, polyinput, typelist, \
    rs2poly, rs2dt, sstflg, sstfn, sstlevels, cvrullr, chart_style ):

    # Get values out of imginfo
    area_name = imginfo[0]
    day = imginfo[1]
    month = imginfo[2]
    year = imginfo[3]
    coverage = imginfo[4]
    coast = imginfo[5]
    # print imginfo

    # Extract polygons from polylist
    if regionstr == 'arctic':
        polylist = polyinput
    elif regionstr == 'antarctic':
        polylist = polyinput[0]
        shelfwkt = polyinput[1]
        bergwkts = polyinput[2]
        bergnames = polyinput[3]
        nicwkts = polyinput[4]
        nicbergs = polyinput[5]

    # Define constants
    rootfname=("NIS_%s" % regionstr)

    # Paper measurements
    if papersize == 'a4':
        pwidth = 210.0
        pheight = 297.0
        dpi = 300.0
        pmargin = 12.7
    elif papersize == 'a3':
        pwidth = 297.0
        pheight = 420.0
        dpi = 300.0
        pmargin = 25.4
    else:
        # A4 page measurements
        pwidth = 210.0
        pheight = 297.0
        dpi = 300.0
        pmargin = 12.7

    # Paper orientation
    if orientation == 'landscape':
        tmpval = pwidth
        pwidth = pheight
        pheight = tmpval

    # Map limits
    xmin = cvrullr[0]
    ymin = cvrullr[3]
    xmax = cvrullr[2]
    ymax = cvrullr[1]
    # print xmin, ymin, xmax, ymax
    width = xmax - xmin
    height = ymax - ymin

    # Map graphic measurements
    dx = float(cvrullr[2] - cvrullr[0])
    dy = float(cvrullr[1] - cvrullr[3])
    mapratio = dx / dy
    if orientation == 'landscape':
        mapysz = int(N.trunc(((pheight - (2.0 * pmargin)) / 25.4) * dpi))
        mapxsz = int(mapysz * mapratio)
    else:
        mapxsz = int(N.trunc(((pwidth - (2.0 * pmargin)) / 25.4) * dpi))
        mapysz = int(mapxsz / mapratio)
        # Adjust for header
        scaley = (ymax - ymin) / mapysz
        # ymax = ymax + (260 * scaley)
        mapysz = int(N.trunc(((pheight - (2.0 * pmargin)) / 25.4) * dpi))
        ymin = ymax - (mapysz * scaley)
        # height = ymax - ymin
    # print mapxsz, mapysz, mapratio

    # Map resolution and scale
    xres = width / float(mapxsz)
    yres = height / float(mapysz)
    scale = float(mapxsz) / (float(xmax)-float(xmin))

    #    Setup ice chart image in Mapscript
    format = mapscript.outputFormatObj('AGG/PNG', 'PNG24' )
    format.setOption( 'imagemode', 'MS_IMAGEMODE_RGBA' )
    format.setMimetype( 'image/png' )
    format.transparent = mapscript.MS_OFF
    # print mapxsz, mapysz
    # print format
    image = mapscript.imageObj(mapxsz, mapysz, format)
    # print 'here0'
    white = mapscript.colorObj()
    white.setRGB( 255, 255, 255 )
    # image.imagecolor = white
    # print 'here1'

    PROJSTR = getproj()

    # Define Ice Chart map to go onto image
    icemap = mapscript.mapObj()
    icemap.name = "Ice Chart"
    icemap.setSize( mapxsz, mapysz )
    icemap.units = mapscript.MS_METERS
    icemap.setExtent( xmin, ymin, xmax, ymax )
    icemap.setProjection( PROJSTR[regionstr] )
    icemap.selectOutputFormat( 'PNG' )
    icemap.imagecolor = white
    # icemap.transparent = mapscript.MS_OFF
    # print 'here2'

    assert icemap.fontset.numfonts == 0
    icemap.fontset.fonts.set('DejaVuSans', "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf")
    icemap.fontset.fonts.set('DejaVuSans-Bold', "/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf")

    # Define region codes
    regcode = get_region_codes()

    #    Draw polygons
    icechart_plotting.plot_icechart_polygons( regionstr, polylist, typelist, \
        1, 1, 1, chart_style, dpi, icemap, image )
    #    Draw SST contour(s)
    sstlblflg = 1
    if sstflg == 1:
        icechart_plotting.plot_sst_contours( regcode[regionstr], area_name, sstfn, sstlevels, 1, sstlblflg, dpi, icemap, image )
    #    Draw graticules
    icechart_plotting.plot_graticules( dpi, icemap, image )
    #    Draw landmask (GSHHS)
    icechart_plotting.plot_landmask_gshhs( regcode[regionstr], coast, 1, 1, dpi, icemap, image )

    #    Additional Antartic layers
    if regionstr == 'antarctic':
        # Draw ice shelves - Antarctic only
        icechart_plotting.plot_iceshelves( regionstr, shelfwkt, 1, 1, dpi, icemap, image )

        # Draw landmask (ADD) - Antarctic only
        icechart_plotting.plot_landmask_add( coast, coverage, 1, 1, dpi, icemap, image )

        # Draw iceberg polygons - Antarctic only
        icechart_plotting.plot_icebergs( regionstr, bergwkts, bergnames, chart_style, 1, 1, 1, dpi, icemap, image ) 

        # Draw NIC iceberg positions - Antarctic only
        icechart_plotting.plot_nicbergs( regionstr, scale, nicwkts, nicbergs, chart_style, 1, 1, dpi, icemap, image ) 

    #    Draw satellite coverage outline
    # for i in range(len(rs2poly)):
    #     # coverwkt = rs2poly[i].ExportToWkt()
    if len(rs2poly) > 0:
        icechart_plotting.plot_coverage_as_single( regionstr, rs2poly, dpi, icemap, image )

    # Output temporary PNG of the map area
    tmppngfname = ("%s/%s_%4d%02d%02d_tmp." % \
        (TMPDIR,area_name,year,month,day)) + image.format.extension
    image.save(tmppngfname,icemap)

    # Create final image using PIL

    # Open new image (A4, 300dpi)
    page_mode = 'RGBA'
    # page_size = (2480,3508)
    page_width = int( N.round((pwidth/25.4)*dpi) )
    page_height = int( N.round((pheight/25.4)*dpi) )
    page_margin = int( N.round((pmargin/25.4)*dpi) )
    # print page_width, page_height
    page_size = ( page_width,page_height )
    page_color = (255,255,255)
    page = Image.new(page_mode, page_size, page_color)

    # PIL settings
    font1 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",58)
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",42)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",33)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",33)

    draw = ImageDraw.Draw(page)

    # Add ice chart map
    icpng = Image.open(tmppngfname,'r')

    # Set transparent areas in icpng to white
    icpng = icpng.convert('RGBA')
    data = N.array(icpng)
    red, green, blue, alpha = data.T # Temporarily unpack the bands for readability
    transparent_areas = N.nonzero(alpha == 0)
    # print transparent_areas
    # print data.shape
    data[transparent_areas[1],transparent_areas[0],:] = [255, 255, 255, 255]
    icpng2 = Image.fromarray(data)

    # Add ice chart graphic to annotated final graphic
    if orientation == 'landscape':
        page.paste(icpng2, (page_width-(mapxsz+page_margin),page_margin))
    else:
        page.paste(icpng2, (page_width-(mapxsz+page_margin),page_margin))

    #    Neat box
    # coords = [ (150,3357), (2330,3357), (2330,150), \
    #     (150,150), (150,3357) ]        
    coords = [ (page_width-(mapxsz+page_margin), page_margin+mapysz), \
               (page_width-page_margin,          page_margin+mapysz), \
               (page_width-page_margin,          page_margin), \
               (page_width-(mapxsz+page_margin), page_margin), \
               (page_width-(mapxsz+page_margin), page_margin+mapysz) ]
    draw.line( coords, fill=(0,0,0), width=3 )
    # print page_width, page_height, page_margin
    # print mapxsz, mapysz

    #    Scale bar
    # xpos = 190
    # ypos = 3327 - (470 + 75)
    barsize = sbarloc[0]
    locstr = sbarloc[1]
    nsbar = 5
    sbwidth = barsize * nsbar * scale
    if locstr[1:2] == 'l':
        xpos = page_width-(mapxsz+page_margin)+40
    else:
        xpos = page_width-(page_margin+100+sbwidth)
    if orientation == 'landscape':
        if locstr[0:1] == 'l':
            if regionstr == 'arctic':  
                ypos = page_height-(page_margin+(380 + 75))
            elif regionstr == 'antarctic':
                ypos = page_height-(page_margin+(500 + 75))
        else:
            if regionstr == 'arctic':
                ypos = page_margin+(380 + 75)
            elif regionstr == 'antarctic':
                ypos = page_margin+(500 + 75)
    else:
        if locstr[0:1] == 'l':
            if regionstr == 'arctic':
                ypos = page_height-(page_margin+(380 + 75))
            elif regionstr == 'antarctic':
                ypos = page_height-(page_margin+(500 + 75))
        else:
            if regionstr == 'arctic':
                ypos = page_margin+(260 + 75)
            elif regionstr == 'antarctic':
                ypos = page_margin+(500 + 75)
    scalebar( draw, barsize, nsbar, scale, xpos, ypos )
    # print xpos,ypos

    #    Header
    if orientation == 'landscape':
        cornerx = [ page_margin+10, page_margin+1110, \
            page_margin+1110, page_margin+10 ]
        if regionstr == 'arctic':
            cornery = [ page_margin+380, page_margin+380, \
                page_margin+10, page_margin+10 ]
        elif regionstr == 'antarctic':
            cornery = [ page_margin+530, page_margin+530, \
                page_margin+10, page_margin+10 ]
        header_landscape( regionstr, draw, page, imginfo, cornerx, cornery )
    else:
        cornerx = [ page_margin+10, page_width-(page_margin+10), \
            page_width-(page_margin+10), page_margin+10 ]
        if regionstr == 'arctic':
            cornery = [ page_margin+260, page_margin+260, \
                page_margin+10, page_margin+10 ]
        elif regionstr == 'antarctic':
            cornery = [ page_margin+260, page_margin+260, \
                page_margin+10, page_margin+10 ]
        header_portrait( regionstr, draw, page, imginfo, cornerx, cornery )

    # Legend area

    #    Legend
    mapcornerx = [ xmin, xmax, xmax, xmin ]
    mapcornery = [ ymax, ymax, ymin, ymin ]
    mappwidth = ((mapxsz / dpi) * 2.54) / 100.0
    # print mappwidth
    if orientation == 'landscape':
        if legendloc == 'r':
            cornerx = [ page_width-(page_margin+1710), page_width-(page_margin+10), \
                page_width-(page_margin+10), page_width-(page_margin+1710) ]
        else:
            cornerx = [ page_margin+10, page_margin+1710, \
                page_margin+1710, page_margin+10 ]
        if regionstr == 'arctic':
            cornery = [ page_height-(page_margin+10), page_height-(page_margin+10), \
                page_height-(page_margin+380), page_height-(page_margin+380) ]
        elif regionstr == 'antarctic':
            cornery = [ page_height-(page_margin+10), page_height-(page_margin+10), \
                page_height-(page_margin+500), page_height-(page_margin+500) ]
        legend_landscape( regionstr, draw, page, cornerx, cornery, mapcornerx, mapcornery, \
            mappwidth, 1, chart_style, scale, sstflg, sstlblflg, 'NO' ) 
    else:
        cornerx = [ page_margin+10, page_width-(page_margin+10), \
            page_width-(page_margin+10), page_margin+10 ]
        if regionstr == 'arctic':
            cornery = [ page_height-(page_margin+10), page_height-(page_margin+10), \
                page_height-(page_margin+380), page_height-(page_margin+380) ]
        elif regionstr == 'antarctic':
            cornery = [ page_height-(page_margin+10), page_height-(page_margin+10), \
                page_height-(page_margin+500), page_height-(page_margin+500) ]
        legend_portrait( regionstr, draw, page, cornerx, cornery, mapcornerx, mapcornery, \
            mappwidth, 1, chart_style, scale, sstflg, sstlblflg, 'NO' ) 

    # Save as output PNG
    outputdir = ("%s/%4d/%4d%02d%02d" % (OUTDIR,year,year,month,day))
    testoutdir(outputdir)
    if chart_style == 'colour':
        outfname =  ("%s/%s_%4d%02d%02d.png" % \
            (outputdir,area_name,year,month,day))
    else:
        outfname =  ("%s/%s_%4d%02d%02d_old.png" % \
            (outputdir,area_name,year,month,day))
    page.save(outfname,'PNG')

    # Remove temporary PNG
    os.remove( tmppngfname )

    return outfname


# Main ice chart layout routine
def layout_external_png( regionstr, imginfo, papersize, orientation, paperdpi, legendloc, sbarloc, polyinput, typelist, \
    sstflg, sstfn, sstlevels, cvrullr, chart_style, servstr ):

    # Dictionary for ice services
    servdict = { 'RU': 'aari', \
                 'US': 'nic' }

    # Get values out of imginfo
    area_name = imginfo[0]
    day = imginfo[1]
    month = imginfo[2]
    year = imginfo[3]
    coverage = imginfo[4]
    coast = imginfo[5]
    # print imginfo

    # Extract polygons from polylist
    if regionstr == 'arctic':
        polylist = polyinput
    elif regionstr == 'antarctic':
        polylist = polyinput[0]
        nicwkts = polyinput[1]
        nicbergs = polyinput[2]

    # Define constants
    rootfname=("NIS_%s" % regionstr)

    # Paper measurements
    if papersize == 'a4':
        pwidth = 210.0
        pheight = 297.0
        dpi = 300.0
        pmargin = 12.7
    elif papersize == 'a3':
        pwidth = 297.0
        pheight = 420.0
        dpi = 300.0
        pmargin = 25.4
    else:
        # A4 page measurements
        pwidth = 210.0
        pheight = 297.0
        dpi = 300.0
        pmargin = 12.7

    # Paper orientation
    if orientation == 'landscape':
        tmpval = pwidth
        pwidth = pheight
        pheight = tmpval

    # Map limits
    xmin = cvrullr[0]
    ymin = cvrullr[3]
    xmax = cvrullr[2]
    ymax = cvrullr[1]
    # print xmin, ymin, xmax, ymax
    width = xmax - xmin
    height = ymax - ymin

    # Map graphic measurements
    dx = float(cvrullr[2] - cvrullr[0])
    dy = float(cvrullr[1] - cvrullr[3])
    mapratio = dx / dy
    if orientation == 'landscape':
        mapysz = int(N.trunc(((pheight - (2.0 * pmargin)) / 25.4) * dpi))
        mapxsz = int(mapysz * mapratio)
    else:
        mapxsz = int(N.trunc(((pwidth - (2.0 * pmargin)) / 25.4) * dpi))
        mapysz = int(mapxsz / mapratio)
        # Adjust for header
        scaley = (ymax - ymin) / mapysz
        # ymax = ymax + (260 * scaley)
        mapysz = int(N.trunc(((pheight - (2.0 * pmargin)) / 25.4) * dpi))
        ymin = ymax - (mapysz * scaley)
        # height = ymax - ymin
    # print mapxsz, mapysz, mapratio

    # Map resolution and scale
    xres = width / float(mapxsz)
    yres = height / float(mapysz)
    scale = float(mapxsz) / (float(xmax)-float(xmin))

    #    Setup ice chart image in Mapscript
    format = mapscript.outputFormatObj('AGG/PNG', 'PNG24' )
    format.setOption( 'imagemode', 'MS_IMAGEMODE_RGBA' )
    format.setMimetype( 'image/png' )
    format.transparent = mapscript.MS_OFF
    # print mapxsz, mapysz
    # print format
    image = mapscript.imageObj(mapxsz, mapysz, format)
    # print 'here0'
    white = mapscript.colorObj()
    white.setRGB( 255, 255, 255 )
    # image.imagecolor = white
    # print 'here1'

    PROJSTR = getproj()

    # Define Ice Chart map to go onto image
    icemap = mapscript.mapObj()
    icemap.name = "Ice Chart"
    icemap.setSize( mapxsz, mapysz )
    icemap.units = mapscript.MS_METERS
    icemap.setExtent( xmin, ymin, xmax, ymax )
    icemap.setProjection( PROJSTR[regionstr] )
    icemap.selectOutputFormat( 'PNG' )
    icemap.imagecolor = white
    # icemap.transparent = mapscript.MS_OFF
    # print 'here2'

    assert icemap.fontset.numfonts == 0
    icemap.fontset.fonts.set('DejaVuSans', "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf")
    icemap.fontset.fonts.set('DejaVuSans-Bold', "/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf")

    # Define region codes
    regcode = get_region_codes()

    #    Draw polygons
    icechart_plotting.plot_icechart_polygons( regionstr, polylist, typelist, \
        1, 1, 1, chart_style, dpi, icemap, image )
    #    Draw SST contour(s)
    sstlblflg = 1
    if sstflg == 1:
        icechart_plotting.plot_sst_contours( regcode[regionstr], area_name, sstfn, sstlevels, 1, sstlblflg, dpi, icemap, image )
    #    Draw graticules
    icechart_plotting.plot_graticules( dpi, icemap, image )
    #    Draw landmask (GSHHS)
    icechart_plotting.plot_landmask_gshhs( regcode[regionstr], coast, 1, 1, dpi, icemap, image )

    #    Additional Antartic layers
    if regionstr == 'antarctic':
        # Draw landmask (ADD) - Antarctic only
        print coast
        icechart_plotting.plot_landmask_add( coast, coverage, 1, 1, dpi, icemap, image )

        # Draw NIC iceberg positions - Antarctic only
        icechart_plotting.plot_nicbergs( regionstr, scale, nicwkts, nicbergs, chart_style, 1, 1, dpi, icemap, image ) 

    # Output temporary PNG of the map area
    tmppngfname = ("%s/%s_%4d%02d%02d_%s_tmp." % \
        (TMPDIR,area_name,year,month,day,servdict[servstr])) + image.format.extension
    image.save(tmppngfname,icemap)

    # Create final image using PIL

    # Open new image (A4, 300dpi)
    page_mode = 'RGBA'
    # page_size = (2480,3508)
    page_width = int( N.round((pwidth/25.4)*dpi) )
    page_height = int( N.round((pheight/25.4)*dpi) )
    page_margin = int( N.round((pmargin/25.4)*dpi) )
    # print page_width, page_height
    page_size = ( page_width,page_height )
    page_color = (255,255,255)
    page = Image.new(page_mode, page_size, page_color)

    # PIL settings
    font1 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",58)
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",42)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",33)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",33)

    draw = ImageDraw.Draw(page)

    # Add ice chart map
    icpng = Image.open(tmppngfname,'r')

    # Set transparent areas in icpng to white
    icpng = icpng.convert('RGBA')
    data = N.array(icpng)
    red, green, blue, alpha = data.T # Temporarily unpack the bands for readability
    transparent_areas = N.nonzero(alpha == 0)
    # print transparent_areas
    # print data.shape
    data[transparent_areas[1],transparent_areas[0],:] = [255, 255, 255, 255]
    icpng2 = Image.fromarray(data)

    # Add ice chart graphic to annotated final graphic
    if orientation == 'landscape':
        page.paste(icpng2, (page_width-(mapxsz+page_margin),page_margin))
    else:
        page.paste(icpng2, (page_width-(mapxsz+page_margin),page_margin))

    #    Neat box
    # coords = [ (150,3357), (2330,3357), (2330,150), \
    #     (150,150), (150,3357) ]        
    coords = [ (page_width-(mapxsz+page_margin), page_margin+mapysz), \
               (page_width-page_margin,          page_margin+mapysz), \
               (page_width-page_margin,          page_margin), \
               (page_width-(mapxsz+page_margin), page_margin), \
               (page_width-(mapxsz+page_margin), page_margin+mapysz) ]
    draw.line( coords, fill=(0,0,0), width=3 )
    # print page_width, page_height, page_margin
    # print mapxsz, mapysz

    #    Scale bar
    # xpos = 190
    # ypos = 3327 - (470 + 75)
    barsize = sbarloc[0]
    locstr = sbarloc[1]
    nsbar = 5
    sbwidth = barsize * nsbar * scale
    if locstr[1:2] == 'l':
        xpos = page_width-(mapxsz+page_margin)+40
    else:
        xpos = page_width-(page_margin+100+sbwidth)
    if orientation == 'landscape':
        if locstr[0:1] == 'l':
            if regionstr == 'arctic':  
                ypos = page_height-(page_margin+(380 + 75))
            elif regionstr == 'antarctic':
                ypos = page_height-(page_margin+(500 + 75))
        else:
            if regionstr == 'arctic':
                ypos = page_margin+(380 + 75)
            elif regionstr == 'antarctic':
                ypos = page_margin+(500 + 75)
    else:
        if locstr[0:1] == 'l':
            if regionstr == 'arctic':
                ypos = page_height-(page_margin+(380 + 75))
            elif regionstr == 'antarctic':
                ypos = page_height-(page_margin+(500 + 75))
        else:
            if regionstr == 'arctic':
                ypos = page_margin+(260 + 75)
            elif regionstr == 'antarctic':
                ypos = page_margin+(500 + 75)
    scalebar( draw, barsize, nsbar, scale, xpos, ypos )
    # print xpos,ypos

    #    Header
    if orientation == 'landscape':
        cornerx = [ page_margin+10, page_margin+1110, \
            page_margin+1110, page_margin+10 ]
        if regionstr == 'arctic':
            cornery = [ page_margin+380, page_margin+380, \
                page_margin+10, page_margin+10 ]
        elif regionstr == 'antarctic':
            cornery = [ page_margin+530, page_margin+530, \
                page_margin+10, page_margin+10 ]
        header_external_landscape( regionstr, draw, page, imginfo, cornerx, cornery, servstr )
    else:
        cornerx = [ page_margin+10, page_width-(page_margin+10), \
            page_width-(page_margin+10), page_margin+10 ]
        if regionstr == 'arctic':
            cornery = [ page_margin+260, page_margin+260, \
                page_margin+10, page_margin+10 ]
        elif regionstr == 'antarctic':
            cornery = [ page_margin+260, page_margin+260, \
                page_margin+10, page_margin+10 ]
        header_external_portrait( regionstr, draw, page, imginfo, cornerx, cornery, servstr )

    # Legend area

    #    Legend
    mapcornerx = [ xmin, xmax, xmax, xmin ]
    mapcornery = [ ymax, ymax, ymin, ymin ]
    mappwidth = ((mapxsz / dpi) * 2.54) / 100.0
    # print mappwidth
    if orientation == 'landscape':
        if legendloc == 'r':
            cornerx = [ page_width-(page_margin+1710), page_width-(page_margin+10), \
                page_width-(page_margin+10), page_width-(page_margin+1710) ]
        else:
            cornerx = [ page_margin+10, page_margin+1710, \
                page_margin+1710, page_margin+10 ]
        if regionstr == 'arctic':
            cornery = [ page_height-(page_margin+10), page_height-(page_margin+10), \
                page_height-(page_margin+380), page_height-(page_margin+380) ]
        elif regionstr == 'antarctic':
            cornery = [ page_height-(page_margin+10), page_height-(page_margin+10), \
                page_height-(page_margin+500), page_height-(page_margin+500) ]
        legend_external_landscape( regionstr, draw, page, cornerx, cornery, mapcornerx, mapcornery, \
            mappwidth, 1, chart_style, scale, sstflg, sstlblflg, servstr ) 
    else:
        cornerx = [ page_margin+10, page_width-(page_margin+10), \
            page_width-(page_margin+10), page_margin+10 ]
        if regionstr == 'arctic':
            cornery = [ page_height-(page_margin+10), page_height-(page_margin+10), \
                page_height-(page_margin+380), page_height-(page_margin+380) ]
        elif regionstr == 'antarctic':
            cornery = [ page_height-(page_margin+10), page_height-(page_margin+10), \
                page_height-(page_margin+500), page_height-(page_margin+500) ]
        legend_external_portrait( regionstr, draw, page, cornerx, cornery, mapcornerx, mapcornery, \
            mappwidth, 1, chart_style, scale, sstflg, sstlblflg, servstr ) 

    # Save as output PNG
    outputdir = ("%s/%4d/%4d%02d%02d" % (OUTDIR,year,year,month,day))
    testoutdir(outputdir)
    if chart_style == 'colour':
        outfname =  ("%s/%s_%4d%02d%02d_%s.png" % \
            (outputdir,area_name,year,month,day,servdict[servstr]))
    else:
        outfname =  ("%s/%s_%4d%02d%02d_%s_old.png" % \
            (outputdir,area_name,year,month,day,servdict[servstr]))
    page.save(outfname,'PNG')

    # Remove temporary PNG
    os.remove( tmppngfname )

    return outfname


def legend_external_landscape( regionstr, draw, page, cornerx, cornery, mapcornerx, mapcornery, \
    mappwidth, boxflg, chart_style, mapscale, sstflg, sstlblflg, servstr ):

    # Get projection information
    PROJSTR = getproj()

    xmin = mapcornerx[0]
    xmax = mapcornerx[1]
    ymin = mapcornery[2]
    ymax = mapcornery[0]
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",36)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",24)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",20)
    font5 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",32)
    font6 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",24)

    # Bounding box
    if boxflg == 1:
        coords = [ (cornerx[0],cornery[0]), (cornerx[2],cornery[2]) ]       
        draw.rectangle( coords, outline=(0,0,0), fill=(255,255,255) )
        coords = [ (cornerx[0],cornery[0]), (cornerx[1],cornery[1]), (cornerx[2],cornery[2]), \
                   (cornerx[3],cornery[3]), (cornerx[0],cornery[0]) ]        
        draw.line( coords, fill=(0,0,0), width=3 )

    # Add coastline text
    if regionstr == 'arctic':
        labeltxt = "Coastline Data: GSHHS version 2.3.5 (http://www.soest.hawaii.edu/wessel/gshhs/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20
        ypos = cornery[0] - (ysz + 20)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos
    elif regionstr == 'antarctic':
        labeltxt = "Antarctic Digtial Database 7.0 (http://www.add.scar.org/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 170
        ypos = cornery[0] - (ysz + 20)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos
        labeltxt = "Coastline Data:"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20
        ypos = ycount - (ysz + 5)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        labeltxt = "GSHHS version 2.3.5 (http://www.soest.hawaii.edu/wessel/gshhs/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 170
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos

    # Add map corners text
    p1 = Proj(PROJSTR[regionstr])
    cornertxt = [ 'UL =', 'UR =', 'LR = ', 'LL =' ] 
    cornerlon = [ xmin, xmax, xmax, xmin ]
    cornerlat = [ ymax, ymax, ymin, ymin ]
    for j in range(4):
        cornerlon[j], cornerlat[j] = p1( cornerlon[j], cornerlat[j], inverse=True )
        # print cornerlon[j], cornerlat[j]
    cornerlist = []
    for j in range(4):
        if cornerlon[j] < 0:
            lonhem = 'W'
        else:
            lonhem = 'E'
        rawlon = N.abs( cornerlon[j] )
        londeg = int( rawlon )
        lonmin = int( (rawlon - londeg) * 60.0 )
        lonsec = (rawlon - (londeg + (float(lonmin) / 60.0) )) * 3600.0
        if cornerlat[j] < 0:
            lathem = 'S'
        else:
            lathem = 'N'
        rawlat = N.abs( cornerlat[j] )
        latdeg = int( rawlat )
        latmin = int( (rawlat - latdeg) * 60.0 )
        latsec = (rawlat - (latdeg + (float(latmin) / 60.0) )) * 3600.0
        # print cornerlon[j], londeg, lonmin, lonsec, lonhem
        # print cornerlat[j], latdeg, latmin, latsec, lathem
        labeltxt = (u"  %s %d\xB0'%d\'%6.3f\x22%s,%d\xB0'%d\'%6.3f\x22%s" % (cornertxt[j], \
            latdeg,latmin,latsec,lathem, londeg,lonmin,lonsec,lonhem))
        cornerlist.append(labeltxt)
    # print cornerlist
    for j in range(3,-1,-1):
        img_txt = Image.new('L', font4.getsize(cornerlist[j]))
        [xsz,ysz] = img_txt.size
        if j % 2 == 0:
            xpos = cornerx[0] + 20
            ycount = ypos
        else:
            xpos = cornerx[0] + 420
            ypos = ycount - (ysz + 5)
        draw.text((xpos, ypos),cornerlist[j],(0,0,0),font=font4)
        ycount = ypos
    labeltxt = "Map Corners: "
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    ycount = ypos

    # Add projection and scale text
    # print (pwidth - (2.0 * pmargin))
    # print xmax, xmin, ((xmax-xmin)*1000.0)
    scalestr = thous( int( (xmax-xmin) / mappwidth ) )
    labeltxt = (u"Scale: %s" % scalestr)
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 620
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    if regionstr == 'arctic':
        labeltxt = (u"Projection: Polar Stereographic, True Scale at 90\xB0N, WGS84")
    elif regionstr == 'antarctic':
        labeltxt = (u"Projection: Polar Stereographic, True Scale at 90\xB0S, WGS84")
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 20
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    ycount = ypos + 10

    # Legend symbol size
    rectxsz = 180
    rectysz = 60

    # Add Copernicus logo here
    if servstr == 'NO' or sstflg == 1:
        logopngfname = ("%s/Copernicus_Logo.png" % (INCDIR))
        logopng = Image.open(logopngfname,'r')
        logoxsz, logoysz = logopng.size
        logoratio = float(logoxsz) / float(logoysz)
        newxsz = 100
        newysz = int(newxsz / logoratio)
        logopng = logopng.resize((newxsz,newysz),Image.ANTIALIAS)
        xpos = cornerx[0] + 850
        ypos = ycount + 20
        page.paste(logopng, (xpos,ypos))

    # Add Sentinel-1 and Radarsat-2 coverage
    linexsz = rectxsz / 2
    if servstr == 'NO':
        xpos = cornerx[0] + 850 + 100 + 20
        ypos = ycount + 60
        coords = [ (xpos,ypos), (xpos+linexsz,ypos) ]
        draw.line( coords, fill=(0,0,0), width=10 )
        labeltxt = 'Sentinel-1'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = xpos + linexsz + 20
        ypos = ypos - ysz
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
        ypos = ypos + ysz
        labeltxt = 'Radarsat-2'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
        xpos = xpos + xsz

    # Add SST coverage
    if sstflg == 1:
        if servstr != 'NO':
            xpos = cornerx[0] + 850 + 100 + 20
        else:
            xpos = xpos + 20
        ypos = ycount + 60
        coords = [ (xpos,ypos), (xpos+linexsz,ypos) ]
        draw.line( coords, fill=(255,62,3), width=5 )
        if sstlblflg == 1:
            labeltxt = '0'
            img_txt = Image.new('L', font5.getsize(labeltxt))
            [lab_xsz,lab_ysz] = img_txt.size
            lab_xpos = xpos + ( linexsz / 2) - (lab_xsz / 2)
            lab_ypos = ypos - (lab_ysz / 2)
            draw.text((lab_xpos, lab_ypos),labeltxt,(0,0,0),font=font5)
        labeltxt = 'Sea Surface'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = xpos + linexsz + 20
        ypos = ypos - ysz
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
        ypos = ypos + ysz
        labeltxt = 'Temperature'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)

    # Add ice concentration classes
    ice_classes = get_ice_class(regionstr)
    if regionstr == 'arctic':
        nrows = 2
    elif regionstr == 'antarctic':
        nrows = 3
    yorig = ycount
    xstep = 0
    for j in range(len(ice_classes)):
        xpos = cornerx[0] + 20 + xstep
        ypos = yorig - ((nrows-(j%nrows)) * (rectysz + 20))

        coords = [ (xpos,ypos), (xpos+rectxsz,ypos+rectysz) ]       
        if chart_style == 'colour':
            if ice_classes[j][0] == 'Iceberg':
                if servstr == 'NO':
                    map_coord = [ 0, \
                                  0, \
                                  rectxsz / mapscale,
                                  rectysz / mapscale ]
                    hatching( page, coords, map_coord, ice_classes[j][0] )
            elif ice_classes[j][0] == 'NIC Iceberg':
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                nicberg_legend( page, coords, map_coord, regionstr )
            else:
                draw.rectangle( coords, outline=(0,0,0), fill=ice_classes[j][2] )
        else:
            if ice_classes[j][0] == 'Ice Shelf':
                draw.rectangle( coords, outline=(0,0,0), fill=ice_classes[j][2] )
            elif ice_classes[j][0] == 'NIC Iceberg':
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                nicberg_legend( page, coords, map_coord, regionstr )
            else:
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                hatching( page, coords, map_coord, ice_classes[j][0] )
        if ice_classes[j][0] != 'NIC Iceberg':
            if servstr == 'NO':
                coords = [ (xpos,ypos),  \
                           (xpos+rectxsz,ypos), \
                           (xpos+rectxsz,ypos+rectysz), \
                           (xpos,ypos+rectysz), \
                           (xpos,ypos) ]
                draw.line( coords, fill=(0,0,0), width=3 )

        # 1/10th label in box
        labeltxt = ice_classes[j][1]
        if ice_classes[j][0] == 'Iceberg' or ice_classes[j][0] == 'NIC Iceberg':
            img_txt = Image.new('L', font6.getsize(labeltxt))
        else:
            img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20 + xstep + int(rectxsz/2) - int(xsz / 2)
        ypos = ypos + 15
        if ice_classes[j][0] == 'Iceberg':
            if servstr == 'NO':
                draw.text((xpos, ypos),labeltxt,(0,0,0),font=font6)
        elif ice_classes[j][0] == 'NIC Iceberg':
            draw.text((xpos+40, ypos),labeltxt,(0,0,0),font=font6)
        else:
            draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)

        # Ice Class name
        if servstr == 'NO' or ice_classes[j][0] != 'Iceberg':
            labeltxt = ice_classes[j][0]
            img_txt = Image.new('L', font2.getsize(labeltxt))
            [xsz,ysz] = img_txt.size
            xpos = cornerx[0] + 20 + xstep + rectxsz + 20
            ypos = ypos - 5
            draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)

        # Update ycount
        ycount = ycount - (rectysz + 20)
        # Update xstep
        if (j % nrows) == (nrows-1):
            xstep = xstep + 560
    ycount = yorig - (nrows * (rectysz + 20))

    # Ice Class name
    labeltxt = "Ice Categories"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 20
    ypos = ycount - (ysz + 15)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)


def legend_external_portrait( regionstr, draw, page, cornerx, cornery, mapcornerx, mapcornery, \
    mappwidth, boxflg, chart_style, mapscale, sstflg, sstlblflg, servstr ):

    # Get projection information
    PROJSTR = getproj()

    xmin = mapcornerx[0]
    xmax = mapcornerx[1]
    ymin = mapcornery[2]
    ymax = mapcornery[0]
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",36)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",24)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",20)
    font5 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",32)
    font6 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",24)

    # Bounding box
    if boxflg == 1:
        coords = [ (cornerx[0],cornery[0]), (cornerx[2],cornery[2]) ]       
        draw.rectangle( coords, outline=(0,0,0), fill=(255,255,255) )
        coords = [ (cornerx[0],cornery[0]), (cornerx[1],cornery[1]), (cornerx[2],cornery[2]), \
                   (cornerx[3],cornery[3]), (cornerx[0],cornery[0]) ]        
        draw.line( coords, fill=(0,0,0), width=3 )

    # Add coastline text
    if regionstr == 'arctic':
        labeltxt = "Coastline Data: GSHHS version 2.3.5 (http://www.soest.hawaii.edu/wessel/gshhs/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20
        ypos = cornery[0] - (ysz + 20)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos
    elif regionstr == 'antarctic':
        labeltxt = "Antarctic Digtial Database 7.0 (http://www.add.scar.org/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 170
        ypos = cornery[0] - (ysz + 20)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos
        labeltxt = "Coastline Data:"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20
        ypos = ycount - (ysz + 5)
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        labeltxt = "GSHHS version 2.3.5 (http://www.soest.hawaii.edu/wessel/gshhs/)"
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 170
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ypos

    # Add map corners text
    p1 = Proj(PROJSTR[regionstr])
    cornertxt = [ 'UL =', 'UR =', 'LR = ', 'LL =' ] 
    cornerlon = [ xmin, xmax, xmax, xmin ]
    cornerlat = [ ymax, ymax, ymin, ymin ]
    for j in range(4):
        cornerlon[j], cornerlat[j] = p1( cornerlon[j], cornerlat[j], inverse=True )
        # print cornerlon[j], cornerlat[j]
    cornerlist = []
    for j in range(4):
        if cornerlon[j] < 0:
            lonhem = 'W'
        else:
            lonhem = 'E'
        rawlon = N.abs( cornerlon[j] )
        londeg = int( rawlon )
        lonmin = int( (rawlon - londeg) * 60.0 )
        lonsec = (rawlon - (londeg + (float(lonmin) / 60.0) )) * 3600.0
        if cornerlat[j] < 0:
            lathem = 'S'
        else:
            lathem = 'N'
        rawlat = N.abs( cornerlat[j] )
        latdeg = int( rawlat )
        latmin = int( (rawlat - latdeg) * 60.0 )
        latsec = (rawlat - (latdeg + (float(latmin) / 60.0) )) * 3600.0
        # print cornerlon[j], londeg, lonmin, lonsec, lonhem
        # print cornerlat[j], latdeg, latmin, latsec, lathem
        labeltxt = (u"  %s %d\xB0'%d\'%6.3f\x22%s,%d\xB0'%d\'%6.3f\x22%s" % (cornertxt[j], \
            latdeg,latmin,latsec,lathem, londeg,lonmin,lonsec,lonhem))
        cornerlist.append(labeltxt)
    # print cornerlist
    for j in range(3,-1,-1):
        img_txt = Image.new('L', font4.getsize(cornerlist[j]))
        [xsz,ysz] = img_txt.size
        if j % 2 == 0:
            xpos = cornerx[0] + 20
            ycount = ypos
        else:
            xpos = cornerx[0] + 420
            ypos = ycount - (ysz + 5)
        draw.text((xpos, ypos),cornerlist[j],(0,0,0),font=font4)
        ycount = ypos
    labeltxt = "Map Corners: "
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    ycount = ypos

    # Add projection and scale text
    # print (pwidth - (2.0 * pmargin))
    # print xmax, xmin, ((xmax-xmin)*1000.0)
    scalestr = thous( int( (xmax-xmin) / mappwidth ) )
    labeltxt = (u"Scale: %s" % scalestr)
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 620
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    if regionstr == 'arctic':
        labeltxt = (u"Projection: Polar Stereographic, True Scale at 90\xB0N, WGS84")
    elif regionstr == 'antarctic':
        labeltxt = (u"Projection: Polar Stereographic, True Scale at 90\xB0S, WGS84")
    img_txt = Image.new('L', font4.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 20
    ypos = ycount - (ysz + 5)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
    ycount = ypos + 10

    # Legend symbol size
    rectxsz = 180
    rectysz = 60

    # Add Coperncius logo here
    if servstr == 'NO' or sstflg == 1:
        logopngfname = ("%s/Copernicus_Logo.png" % (INCDIR))
        logopng = Image.open(logopngfname,'r')
        logoxsz, logoysz = logopng.size
        logoratio = float(logoxsz) / float(logoysz)
        newxsz = 100
        newysz = int(newxsz / logoratio)
        logopng = logopng.resize((newxsz,newysz),Image.ANTIALIAS)
        xpos = cornerx[0] + 850
        ypos = ycount + 20
        page.paste(logopng, (xpos,ypos))

    # Add Sentinel-1 and Radarsat-2 coverage
    linexsz = rectxsz / 2
    if servstr == 'NO':
        xpos = cornerx[0] + 850 + 100 + 20
        ypos = ycount + 60
        coords = [ (xpos,ypos), (xpos+linexsz,ypos) ]
        draw.line( coords, fill=(0,0,0), width=10 )
        labeltxt = 'Sentinel-1'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = xpos + linexsz + 20
        ypos = ypos - ysz
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
        ypos = ypos + ysz
        labeltxt = 'Radarsat-2'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
        xpos = xpos + xsz

    # Add SST coverage
    if sstflg == 1:
        xpos = xpos + 20
        ypos = ycount + 60
        coords = [ (xpos,ypos), (xpos+linexsz,ypos) ]
        draw.line( coords, fill=(255,62,3), width=5 )
        if sstlblflg == 1:
            labeltxt = '0'
            img_txt = Image.new('L', font5.getsize(labeltxt))
            [lab_xsz,lab_ysz] = img_txt.size
            lab_xpos = xpos + ( linexsz / 2) - (lab_xsz / 2)
            lab_ypos = ypos - (lab_ysz / 2)
            draw.text((lab_xpos, lab_ypos),labeltxt,(0,0,0),font=font5)
        labeltxt = 'Sea Surface'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = xpos + linexsz + 20
        ypos = ypos - ysz
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
        ypos = ypos + ysz
        labeltxt = 'Temperature'
        img_txt = Image.new('L', font2.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)

    # Add ice concentration classes
    ice_classes = get_ice_class(regionstr)
    if regionstr == 'arctic':
        nrows = 2
    elif regionstr == 'antarctic':
        nrows = 3
    yorig = ycount
    xstep = 0
    for j in range(len(ice_classes)):
        xpos = cornerx[0] + 20 + xstep
        ypos = yorig - ((nrows-(j%nrows)) * (rectysz + 20))

        coords = [ (xpos,ypos), (xpos+rectxsz,ypos+rectysz) ]       
        if chart_style == 'colour':
            if ice_classes[j][0] == 'Iceberg':
                if servstr == 'NO':
                    map_coord = [ 0, \
                                  0, \
                                  rectxsz / mapscale,
                                  rectysz / mapscale ]
                    hatching( page, coords, map_coord, ice_classes[j][0] )
            elif ice_classes[j][0] == 'NIC Iceberg':
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                nicberg_legend( page, coords, map_coord, regionstr )
            else:
                draw.rectangle( coords, outline=(0,0,0), fill=ice_classes[j][2] )
        else:
            if ice_classes[j][0] == 'Ice Shelf':
                draw.rectangle( coords, outline=(0,0,0), fill=ice_classes[j][2] )
            elif ice_classes[j][0] == 'NIC Iceberg':
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                nicberg_legend( page, coords, map_coord, regionstr )
            else:
                map_coord = [ 0, \
                              0, \
                              rectxsz / mapscale,
                              rectysz / mapscale ]
                hatching( page, coords, map_coord, ice_classes[j][0] )
        if ice_classes[j][0] != 'NIC Iceberg':
            if servstr == 'NO' or ice_classes[j][0] != 'Iceberg':
                coords = [ (xpos,ypos),  \
                           (xpos+rectxsz,ypos), \
                           (xpos+rectxsz,ypos+rectysz), \
                           (xpos,ypos+rectysz), \
                           (xpos,ypos) ]
                draw.line( coords, fill=(0,0,0), width=3 )

        # 1/10th label in box
        labeltxt = ice_classes[j][1]
        if ice_classes[j][0] == 'Iceberg' or ice_classes[j][0] == 'NIC Iceberg':
            img_txt = Image.new('L', font6.getsize(labeltxt))
        else:
            img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 20 + xstep + int(rectxsz/2) - int(xsz / 2)
        ypos = ypos + 15
        if ice_classes[j][0] == 'Iceberg':
            if servstr == 'NO':
                draw.text((xpos, ypos),labeltxt,(0,0,0),font=font6)
        elif ice_classes[j][0] == 'NIC Iceberg':
            draw.text((xpos+40, ypos),labeltxt,(0,0,0),font=font6)
        else:
            draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)

        # Ice Class name
        if servstr == 'NO' or ice_classes[j][0] != 'Iceberg':
            labeltxt = ice_classes[j][0]
            img_txt = Image.new('L', font2.getsize(labeltxt))
            [xsz,ysz] = img_txt.size
            xpos = cornerx[0] + 20 + xstep + rectxsz + 20
            ypos = ypos - 5
            draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)

        # Update ycount
        ycount = ycount - (rectysz + 20)
        # Update xstep
        if (j % nrows) == (nrows-1):
            xstep = xstep + 560
    ycount = yorig - (nrows * (rectysz + 20))

    # Ice Class name
    labeltxt = "Ice Categories"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[0] + 20
    ypos = ycount - (ysz + 15)
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)


def header_external_landscape( regionstr, draw, page, imginfo, cornerx, cornery, servstr ):
    font1 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",42)
    font1aa = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-BoldOblique.ttf",42)
    font1a = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Oblique.ttf",36)
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",32)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",28)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed-Bold.ttf",36)

    # Get values out of imginfo
    area_name = imginfo[0]
    day = imginfo[1]
    month = imginfo[2]
    year = imginfo[3]
    coverage = imginfo[4]
    coast = imginfo[5]
    # print imginfo

    # Draw rectangular box at side of map
    coords = [ (cornerx[0],cornery[0]), (cornerx[2],cornery[2]) ]       
    draw.rectangle( coords, outline=(0,0,0), fill=(255,255,255) )
    coords = [ (cornerx[0],cornery[0]), (cornerx[1],cornery[1]), (cornerx[2],cornery[2]), \
               (cornerx[3],cornery[3]), (cornerx[0],cornery[0]) ]        
    draw.line( coords, fill=(0,0,0), width=3 )

    # Add met.no logo
    logopngfname = ("%s/institute_logo.png" % (INCDIR))
    logopng = Image.open(logopngfname,'r')
    logoxsz, logoysz = logopng.size
    logoratio = float(logoxsz) / float(logoysz)
    newxsz = 500
    newysz = int(newxsz / logoratio)
    logopng = logopng.resize((newxsz,newysz),Image.ANTIALIAS)
    page.paste(logopng, (cornerx[0]+2,cornery[2]+10))
    ycount = cornery[2]+newysz

    # Add "Ice Service"
    labeltxt = "Ice Service"
    img_txt = Image.new('L', font1aa.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = (cornerx[0]+2)+int((newxsz/2)-(xsz/2))
    ypos = ycount + 10
    draw.text((xpos, ypos),labeltxt,(0,144,168),font=font1aa)

    # Add title text
    month_names = [ 'January', 'February', 'March', 'April', 'May', 'June', \
                    'July', 'August', 'September', 'October', 'November', 'December' ]
    monthstr = month_names[month-1]
    if day % 10 == 1:
        if day != 11:
            daysuffix = 'st'
        else:
            daysuffix = 'th'
    elif day % 10 == 2:
        if day != 12:
            daysuffix = 'nd'
        else:
            daysuffix = 'th'
    elif day % 10 == 3:
        if day != 13:
            daysuffix = 'rd'
        else:
            daysuffix = 'th'
    else:
        daysuffix = 'th'
    # Title line 1 "Date"
    labeltxt = ("%d%s %s %d" % (day,daysuffix,monthstr,year))
    img_txt = Image.new('L', font1.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz + 10)
    ypos = cornery[2] + 40
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font1)
    ycount = ypos + ysz
    # Title line 2 "Time"
    labeltxt = ("Valid 15:00 UTC")
    img_txt = Image.new('L', font1a.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz + 10)
    ypos = ycount + 10
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font1a)
    ycount = ycount + 10 + ysz

    # If Antarctic, add collaborative product details
    # Logo order is right to left
    logo_order = [ 'nic', 'aari', 'nis' ]
    if servstr == 'RU':
        logo_order = [ 'nis', 'nic', 'aari' ]
    elif servstr == 'US':
        logo_order = [ 'nis', 'aari', 'nic' ]
    if regionstr == 'antarctic':
        xpos = cornerx[2] - (80+10)
        ypos = ycount + 30
        for j in range(len(logo_order)):

            # Add NIC logo
            if logo_order[j] == 'nic':
                logopngfname = ("%s/NIC_Logo.png" % (INCDIR))
                logopng = Image.open(logopngfname,'r')
                logopng = logopng.resize((120,120),Image.ANTIALIAS)
                xpos = xpos - (120+30)
                page.paste(logopng, (xpos,ypos))

            # Add AARI logo
            if logo_order[j] == 'aari':
                logopngfname = ("%s/AARI_Logo.png" % (INCDIR))
                logopng = Image.open(logopngfname,'r')
                logopng = logopng.resize((125,120),Image.ANTIALIAS)
                xpos = xpos - (125+30)
                page.paste(logopng, (xpos,ypos))

            # Add MET Norway logo
            if logo_order[j] == 'nis':
                logopngfname = ("%s/METNorway_Logo.png" % (INCDIR))
                logopng = Image.open(logopngfname,'r')
                logopng = logopng.resize((75,120),Image.ANTIALIAS)
                xpos = xpos - (75+30)
                page.paste(logopng, (xpos,ypos))

        ycount = ypos + 120

        # Add collaborative product text
        labeltxt = ("Antarctic Collaborative Product")
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[2] - (xsz + 10)
        ypos = ycount + 10
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ycount + ysz

    # Add address text
    labeltxt = "Forecasting Division for Northern Norway"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 50
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    ycount = ypos + ysz

    labeltxt = u"N-9293 Troms\xF8, Norway"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 5
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    ycount = ypos + ysz

    # Add contact text
    labeltxt = "Tel: +47 77 62 14 62   Fax: +47 77 62 13 01   E-mail: istjenesten@met.no"
    img_txt = Image.new('L', font3.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 5
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font3)
    ycount = ycount + 5 + ysz


def header_external_portrait( regionstr, draw, page, imginfo, cornerx, cornery, servstr ):
    font1 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf",42)
    font1aa = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-BoldOblique.ttf",42)
    font1a = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Oblique.ttf",36)
    font2 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",32)
    font3 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed.ttf",28)
    font4 = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSansCondensed-Bold.ttf",36)

    # Get values out of imginfo
    area_name = imginfo[0]
    day = imginfo[1]
    month = imginfo[2]
    year = imginfo[3]
    coverage = imginfo[4]
    coast = imginfo[5]
    # print imginfo

    # Draw rectangular box at side of map
    coords = [ (cornerx[0],cornery[0]), (cornerx[2],cornery[2]) ]       
    draw.rectangle( coords, outline=(0,0,0), fill=(255,255,255) )
    coords = [ (cornerx[0],cornery[0]), (cornerx[1],cornery[1]), (cornerx[2],cornery[2]), \
               (cornerx[3],cornery[3]), (cornerx[0],cornery[0]) ]        
    draw.line( coords, fill=(0,0,0), width=3 )

    # Add met.no logo
    logopngfname = ("%s/institute_logo.png" % (INCDIR))
    logopng = Image.open(logopngfname,'r')
    logoxsz, logoysz = logopng.size
    logoratio = float(logoxsz) / float(logoysz)
    newxsz = 500
    newysz = int(newxsz / logoratio)
    logopng = logopng.resize((newxsz,newysz),Image.ANTIALIAS)
    page.paste(logopng, (cornerx[0]+2,cornery[2]+10))
    ycount = cornery[2]+newysz

    # Add "Ice Service"
    labeltxt = "Ice Service"
    img_txt = Image.new('L', font1aa.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = (cornerx[0]+2)+int((newxsz/2)-(xsz/2))
    ypos = ycount + 10
    draw.text((xpos, ypos),labeltxt,(0,144,168),font=font1aa)

    # If Antarctic, add collaborative product details
    # Logo order is left to right
    logo_order = [ 'nis', 'aari', 'nic' ]
    if servstr == 'RU':
        logo_order = [ 'aari', 'nic', 'nis' ]
    elif servstr == 'US':
        logo_order = [ 'nic', 'aari', 'nis' ]
    if regionstr == 'antarctic':
        xpos = cornerx[0] + 2 + 500 + 60
        ypos = cornery[2] + 20
        for j in range(len(logo_order)):

            # Add NIC logo
            if logo_order[j] == 'nic':
                logopngfname = ("%s/NIC_Logo.png" % (INCDIR))
                logopng = Image.open(logopngfname,'r')
                logopng = logopng.resize((120,120),Image.ANTIALIAS)
                page.paste(logopng, (xpos,ypos))
                xpos = xpos + (120+30)

            # Add AARI logo
            if logo_order[j] == 'aari':
                logopngfname = ("%s/AARI_Logo.png" % (INCDIR))
                logopng = Image.open(logopngfname,'r')
                logopng = logopng.resize((125,120),Image.ANTIALIAS)
                page.paste(logopng, (xpos,ypos))
                xpos = xpos + (125+30)

            # Add MET Norway logo
            if logo_order[j] == 'nis':
                logopngfname = ("%s/METNorway_Logo.png" % (INCDIR))
                logopng = Image.open(logopngfname,'r')
                logopng = logopng.resize((75,120),Image.ANTIALIAS)
                page.paste(logopng, (xpos,ypos))
                xpos = xpos + (75+30)

        ycount = ypos + 120

        # Add collaborative product text
        labeltxt = ("Antarctic Collaborative")
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 2 + 500 + 60 + (380/2) - (xsz/2)
        ypos = ycount + 10
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ycount + ysz
        labeltxt = ("Product")
        img_txt = Image.new('L', font4.getsize(labeltxt))
        [xsz,ysz] = img_txt.size
        xpos = cornerx[0] + 2 + 500 + 60 + (380/2) - (xsz/2)
        ypos = ycount + 10
        draw.text((xpos, ypos),labeltxt,(0,0,0),font=font4)
        ycount = ycount + ysz

    # Add title text
    month_names = [ 'January', 'February', 'March', 'April', 'May', 'June', \
                    'July', 'August', 'September', 'October', 'November', 'December' ]
    monthstr = month_names[month-1]
    if day % 10 == 1:
        if day != 11:
            daysuffix = 'st'
        else:
            daysuffix = 'th'
    elif day % 10 == 2:
        if day != 12:
            daysuffix = 'nd'
        else:
            daysuffix = 'th'
    elif day % 10 == 3:
        if day != 13:
            daysuffix = 'rd'
        else:
            daysuffix = 'th'
    else:
        daysuffix = 'th'
    # Title line 1 "Date"
    labeltxt = ("%d%s %s %d" % (day,daysuffix,monthstr,year))
    img_txt = Image.new('L', font1.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz + 10)
    ypos = cornery[2] + 20
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font1)
    ycount = ypos + ysz
    # Title line 2 "Time"
    labeltxt = ("Valid 15:00 UTC")
    img_txt = Image.new('L', font1a.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz + 10)
    ypos = ycount + 10
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font1a)
    ycount = ycount + 10 + ysz

    # Add address text
    # labeltxt = u"Norwegian Ice Service, Forecasting Division for Northern Norway, N-9293 Troms\xF8, Norway"

    labeltxt = "Forecasting Division for Northern Norway, N-9293 Troms\xF8, Norway"
    img_txt = Image.new('L', font2.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 40
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font2)
    ycount = ypos + ysz

    # Add contact text
    labeltxt = "Tel: +47 77 62 14 62   Fax: +47 77 62 13 01   E-mail: istjenesten@met.no"
    img_txt = Image.new('L', font3.getsize(labeltxt))
    [xsz,ysz] = img_txt.size
    xpos = cornerx[2] - (xsz+10)
    ypos = ycount + 5
    draw.text((xpos, ypos),labeltxt,(0,0,0),font=font3)
    ycount = ycount + 5 + ysz

