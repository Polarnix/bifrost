#!/usr/bin/python

# Name:          auto_archive.py
# Purpose:       Automatically copy files to archive on Istjenesten server.
# Usage:         No command line options = Use current date and do not overwrite
#                python auto_archive [yyyymmdd] [force_flg]
#                  yyyymmdd = Date to upload
#                  force_flg = 1 to overwrite existing files on server
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, stat, shutil
import string, time
from datetime import datetime

import pg

from create_old_shp import create_old_shp

BASEDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Python/include" % BASEDIR)
ANCDIR=("%s/Python/Ancillary" % BASEDIR)
OUTDIR=("%s/Outputs" % BASEDIR)

ARCHBASE='/vol/istjenesten/archive'
SHPARCHDIR=("%s/iceShapeArchive" % ARCHBASE)
PRDARCHDIR=("%s/isarkiv" % ARCHBASE)

# New system flag
NEWSYS = 1

# Active flag - to copy files to archive
activeflg = 1

# Output the start time
tmpt=time.gmtime()
tout=( '*** auto_archive.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/auto_archive.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Define date of ice chart
if len(sys.argv) >= 2:
    datestr = sys.argv[1]
    iceyr = int(datestr[0:4])
    icemh = int(datestr[4:6])
    icedy = int(datestr[6:8])
else:
    now = datetime.now()
    iceyr = now.year
    icemh = now.month
    icedy = now.day
icedt = datetime( iceyr, icemh, icedy, 15, 0, 0)
dtstr = icedt.strftime('%Y%m%d')
# print icedt, dtstr
force_flg = 0
if len(sys.argv) == 3:
    force_flg =  int(sys.argv[2])

# Loop through ice chart regions
regions = [ 'arctic', 'antarctic' ]
for regionstr in regions:

    # Database checking on new system
    proc_flg = 0
    if NEWSYS == 1:
    
        # Connect to database
        dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')
    
        # Check status in database
        sqltxt = ("SELECT id,finish_flg,outputs_flg,archive_flg FROM production WHERE icechart_date = \'%4d-%02d-%02d\' AND region = \'%s\';" \
            % (iceyr,icemh,icedy,regionstr))
        queryres = dbcon.query( sqltxt )
        nrec = queryres.ntuples()
        if nrec == 0:
            print ("auto_archive: No database record for %s this date. Stopping." % regionstr)
        elif nrec == 1:
            icechart_id = queryres.getresult()[0][0]
            finish_flg = queryres.getresult()[0][1]
            outputs_flg = queryres.getresult()[0][2]
            archive_flg = queryres.getresult()[0][3]
        else:
            print ("auto_archive: Too many database records for %s this date. Stopping." % regionstr)
        if finish_flg <= 0:
            print ("auto_archive: Ice chart for %s not finished on this date. Stopping." % regionstr)
        else:
            if outputs_flg <= 0:
                print ("auto_archive: No output graphics for %s on this date. Stopping." % regionstr)
            else:
                if archive_flg == 1 and force_flg != 1:
                    print ("auto_archive: Ice chart already archived for %s this date. Stopping." % regionstr)
                    proc_flg = 0
                else:
                    proc_flg = 1
    
    # Only process if proc_flg = 1 or NEWSYS = 0
    if proc_flg == 1 or NEWSYS == 0:
        # 1. Copy PNG product files to PRDARCHDIR/YYYY/YYYYMMDD
        # Get list of PNG images
        # Set ice chart area data
        if regionstr == 'arctic':
            ic_names = [ 'general', 'baltic', 'fram_strait', 'barents', 'denmark_strait', 'svalbard', 'oslofjord' ]
        elif regionstr == 'antarctic':
            ic_names = [ 'antarctic', 'peninsula', 'weddell_east', 'bransfield_strait', 'adelaide_island' ]
        # print ic_names
        ic_types = [ '', '_old' ]
        ic_formats = [ 'png' ]
        # Loop through ice charts and add to list of PNG files.
        pnglist = []
        for chart_name in ic_names:
            for chart_type in ic_types:
                for chart_format in ic_formats:
                    # Upload binary file
                    localfn = ("%s/%d/%s/%s_%s%s.%s" % \
                        (OUTDIR,iceyr,dtstr,chart_name,dtstr,chart_type,chart_format))
                    # print localfn
                    pnglist.append( localfn )
    
        # Try to change directory
        # Year level
        targdir = ("%s/%d" % (PRDARCHDIR,iceyr))
        # print targdir
        try:
            os.chdir(targdir)
        except:
            if activeflg == 1:
                # NB: Need to create directory, then set the mode
                os.mkdir(targdir)
                os.chmod(targdir,stat.S_IRWXU+stat.S_IRWXG+stat.S_IRWXO)
        # Ice chart level
        targdir = ("%s/%d/%4d%02d%02d" % (PRDARCHDIR,iceyr,iceyr,icemh,icedy))
        print targdir
        try:
            os.chdir(targdir)
        except:
            if activeflg == 1:
                # NB: Need to create directory, then set the mode
                os.mkdir(targdir)
                os.chmod(targdir,stat.S_IRWXU+stat.S_IRWXG+stat.S_IRWXO)
    
        # Copy files
        for fname in pnglist:
            fnbits = fname.split('/')
            pngfn = fnbits[-1]
            archfn = ("%s/%s" % (targdir,pngfn))
            # print pngfn
            if activeflg == 1:
                shutil.copy( fname, archfn )
    
    
        # 2. Copy Shapefile to SHPARCHDIR/YYYY/MM (only for Arctic)
        if regionstr == 'arctic':

            # Create temporary Shapefile in old format
            oldshpfn = ("%s/chart_ice.shp" % TMPDIR)
            create_old_shp( oldshpfn, icedt )
   
            # Try to change directory
            # Year level
            targdir = ("%s/%d" % (SHPARCHDIR,iceyr))
            # print targdir
            try:
                os.chdir(targdir)
            except:
                if activeflg == 1:
                    # NB: Need to create directory, then set the mode
                    os.mkdir(targdir)
                    os.chmod(targdir,stat.S_IRWXU+stat.S_IRWXG+stat.S_IRWXO)
            # Month level
            targdir = ("%s/%d/%02d" % (SHPARCHDIR,iceyr,icemh))
            # print targdir
            try:
                os.chdir(targdir)
            except:
                if activeflg == 1:
                    # NB: Need to create directory, then set the mode
                    os.mkdir(targdir)
                    os.chmod(targdir,stat.S_IRWXU+stat.S_IRWXG+stat.S_IRWXO)
    
            # Copy Shapefile
            shp_bits = [ 'shp', 'shx', 'dbf', 'prj' ]
            for suffix in shp_bits:
                localfn = ("%s/chart_ice.%s" % (TMPDIR,suffix))
                archfn = ("%s/ice%4d%02d%02d.%s" % (targdir,iceyr,icemh,icedy,suffix))
                # print localfn, archfn
                if activeflg == 1:
                    shutil.copy( localfn, archfn )
            # Delete temporary Shapefile
            for fname in shp_bits:
                os.remove( ("%s/chart_ice.%s" % (TMPDIR,fname)) )
                pass
    
    
        # On new system, enter database status and close connection
        if NEWSYS == 1:
            # Database status update
            timenow = datetime.now()
            dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
            sqltxt = ("UPDATE production SET (archive_flg,archive_dt) = (1,\'%s\') WHERE id = %d;"
                % (dtstr,icechart_id))
            print sqltxt
            dbcon.query( sqltxt )
    
        # Close datebase connection
if NEWSYS == 1:
    dbcon.close()

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** auto_archive.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

