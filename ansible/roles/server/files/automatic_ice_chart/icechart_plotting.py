#!/usr/bin/python

# Name:          icechart_plotting.py
# Purpose:       Graphics plotting routines for ice chart products.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import sys, os
import StringIO, cStringIO

import numpy as N
from pylab import *

import osgeo.ogr as ogr
import osgeo.osr as osr

import mapscript

from file_utils import getproj4
from icechart_utils import getproj

# Directories
BASEDIR='/home/bifrostadmin'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Include/Automatic_Icechart" % BASEDIR)

# Database connection string
PG_ICECHARTS_CONNECTION_STRING = "dbname=Icecharts host=localhost user=bifrostadmin password=bifrostadmin"
PG_OCEAN_CONNECTION_STRING = "dbname=Ocean host=localhost user=bifrostadmin password=bifrostadmin"

# Map projection
PROJSTR = '+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84'

# Draw longitude/latitude lines and points without using a Shapefile
def plot_graticules( dpi, mapobj, image):

    # Define a circle symbol
    circle = mapscript.symbolObj( 'circle' )
    circle.name = 'circle'
    circle.type = mapscript.MS_SYMBOL_ELLIPSE
    circle.filled = mapscript.MS_TRUE
    line = mapscript.lineObj()
    p1 = mapscript.pointObj( 1, 1 )
    line.add( p1 )
    circle.setPoints( line )
    sset = mapobj.symbolset
    sym = sset.appendSymbol( circle )

    # Define lat-lon points layer
    llpoints = mapscript.layerObj( mapobj )
    llpoints.name = 'LL Points'
    llpoints.type = mapscript.MS_LAYER_POINT
    llpoints.setProjection( '+proj=longlat +ellps=WGS84' )

    points = mapscript.shapeObj( mapscript.MS_SHAPE_POINT )
    for lat in range(-83,84):
        if lat % 5 != 0:
            for lon in range(-179,180):
                if lon % 5 != 0:
                    line = mapscript.lineObj()
                    p1 = mapscript.pointObj( lon, lat )
                    line.add( p1 )
                    points.add( line )

    llpoints.addFeature( points )
    llpoints.status = mapscript.MS_ON

    # Define class for lat-lon points
    ptsclass = mapscript.classObj( llpoints )
    ptsclass.name = 'LL Points'
    ptsstyle = mapscript.styleObj( ptsclass )
    ptsstyle.outlinecolor.setRGB(0, 0, 255)
    ptsstyle.color.setRGB(0, 0, 255)
    ptsstyle.size = int( (3.0 / 300.0 ) * dpi )
    ptsstyle.symbol = sym
    ptsstyle.antialias = True

    # Define lat-lon graticules layer
    llgrats = mapscript.layerObj( mapobj )
    llgrats.name = 'LL Lines'
    llgrats.type = mapscript.MS_LAYER_LINE
    llgrats.setProjection( '+proj=longlat +ellps=WGS84' )

    lines = mapscript.shapeObj( mapscript.MS_SHAPE_LINE )
    linestp = 1.0
    # Latitude lines
    for lat in range(-85,86,5):
        line = mapscript.lineObj()
        nseg = int(360 / linestp) + 1
        for i in range(nseg):
            lon = -180.0 + (i * linestp)
            p1 = mapscript.pointObj( lon, lat )
            line.add( p1 )
        lines.add( line )
    # Longitude lines
    for lon in range(-180,180,5):
        line = mapscript.lineObj()
        if lon % 10 == 0:
            upper = 85
        else:
            upper = 83
        nseg = int((upper * 2) / linestp) + 1
        for i in range(nseg):
            lat = -upper + (i * linestp)
            p1 = mapscript.pointObj( lon, lat )
            line.add( p1 )
        lines.add( line )

    llgrats.addFeature( lines )
    llgrats.status = mapscript.MS_ON

    # Define class for lat-lon lines
    gratsclass = mapscript.classObj( llgrats )
    gratsclass.name = 'LL Lines'
    gratsstyle = mapscript.styleObj( gratsclass )
    gratsstyle.outlinecolor.setRGB(0, 0, 255)
    gratsstyle.width = int( ( 3.0 / 300.0 ) * dpi )
    gratsstyle.antialias = True

    # Draw layers onto image
    llpoints.draw( mapobj, image )
    llgrats.draw( mapobj, image )


# Draw landmask layer
def plot_landmask( dpi, mapobj, image ):

    # Define landmask layer
    landmask = mapscript.layerObj( mapobj )
    landmask.name = "Land Mask"
    landmask.type = mapscript.MS_LAYER_POLYGON
    landmask.status = mapscript.MS_ON
    longlatproj4str = '+proj=longlat +ellps=WGS84'
    landmask.setProjection( longlatproj4str )
    landmask.connectiontype = mapscript.MS_POSTGIS
    landmask.connection = PG_OCEAN_CONNECTION_STRING
    landmask.addProcessing("CLOSE_CONNECTION=DEFER")
    source_query = "the_geom FROM country USING srid=4326 USING UNIQUE gid"
    landmask.data = source_query

    # Define Land Mask class
    class_land = mapscript.classObj( landmask )
    class_land.name = "Land Mask"
    style_land = mapscript.styleObj(class_land)
    style_land.outlinecolor.setRGB(0, 0, 0)
    style_land.width = int( ( 3.0 / 300.0 ) * dpi )
    style_land.color.setRGB(196, 196, 196)
    style_land.antialias = False

    # Draw layer onto image
    landmask.draw( mapobj, image )


# Draw landmask layer (GSHHS)
def plot_landmask_gshhs( regcode, resstr, edgeflg, aaflg, dpi, mapobj, image ):

    # Define landmask layer
    landmask = mapscript.layerObj( mapobj )
    landmask.name = "Land Mask"
    landmask.type = mapscript.MS_LAYER_POLYGON
    landmask.status = mapscript.MS_ON
    longlatproj4str = '+proj=longlat +ellps=WGS84'
    landmask.setProjection( longlatproj4str )
    landmask.connectiontype = mapscript.MS_POSTGIS
    landmask.connection = PG_OCEAN_CONNECTION_STRING
    landmask.addProcessing("CLOSE_CONNECTION=DEFER")
    source_query = ("geom FROM gshhs_%s USING srid=4326 USING UNIQUE gid" % resstr)
    # print source_query
    landmask.data = source_query

    # Define Land Mask class
    class_land = mapscript.classObj( landmask )
    class_land.name = "Land Mask"
    style_land = mapscript.styleObj(class_land)
    if edgeflg == 1:
        style_land.outlinecolor.setRGB(0, 0, 0)
    else:
        style_land.outlinecolor.setRGB(196, 196, 196)
    style_land.width = int( ( 3.0 / 300.0 ) * dpi )
    style_land.color.setRGB(196, 196, 196)
    if aaflg == 1:
        style_land.antialias = True
    else:
        style_land.antialias = False

    # Draw layer onto image
    landmask.draw( mapobj, image )


# Draw Antarctic Digital Database (ADD) landmask layer (Antarctic only)
def plot_landmask_add( resstr, border, edgeflg, aaflg, dpi, mapobj, image ):

    # Adjust resstr since we only have, l, i, h
    if resstr == 'c':
        resstr = 'l'
    if resstr == 'f':
        resstr = 'h'
    # print border.ExportToWkt()

    add_proj4 = '+proj=stere +lat_0=-90 +lat_ts=-71 +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs'

    # Adjust border polygon to EPSG3031
    src_srs = osr.SpatialReference()
    src_srs.ImportFromProj4('+proj=stere +lat_0=90s +lon_0=0e +lat_ts=90s +ellps=WGS84 +datum=WGS84')
    add_srs = osr.SpatialReference()
    add_srs.ImportFromProj4(add_proj4)
    to_add = osr.CoordinateTransformation( src_srs, add_srs )
    # border = ogr.CreateGeometryFromWkt( border_wkt )
    border.Transform( to_add )
    # print border.ExportToWkt()
    bordergeom=("ST_GeometryFromText(\'%s\',3031)" % border.ExportToWkt())

    # Define add_ant_coast layer
    add_ant_coast = mapscript.layerObj( mapobj )
    add_ant_coast.name = "Land Mask (Antarctic)"
    add_ant_coast.type = mapscript.MS_LAYER_POLYGON
    add_ant_coast.status = mapscript.MS_ON
    longlatproj4str = add_proj4
    add_ant_coast.setProjection( longlatproj4str )
    add_ant_coast.connectiontype = mapscript.MS_POSTGIS
    add_ant_coast.connection = PG_OCEAN_CONNECTION_STRING
    add_ant_coast.addProcessing("CLOSE_CONNECTION=DEFER")
    source_query = ("geom FROM add_ant_coastline_%s USING srid=3031 USING UNIQUE gid WHERE ST_Intersects(geom,%s)" \
        % (resstr,bordergeom))
    # print source_query
    add_ant_coast.data = source_query

    # Define Land Mask class
    class_land = mapscript.classObj( add_ant_coast )
    class_land.name = "Land Mask"
    style_land = mapscript.styleObj(class_land)
    if edgeflg == 1:
        style_land.outlinecolor.setRGB(0, 0, 0)
    else:
        style_land.outlinecolor.setRGB(196, 196, 196)
    style_land.width = int( ( 3.0 / 300.0 ) * dpi )
    style_land.color.setRGB(196, 196, 196)
    if aaflg == 1:
        style_land.antialias = True
    else:
        style_land.antialias = False

    # Draw layer onto image
    add_ant_coast.draw( mapobj, image )

    # Define add_subant_coast layer
    add_subant_coast = mapscript.layerObj( mapobj )
    add_subant_coast.name = "Land Mask (Antarctic)"
    add_subant_coast.type = mapscript.MS_LAYER_POLYGON
    add_subant_coast.status = mapscript.MS_ON
    longlatproj4str = add_proj4
    add_subant_coast.setProjection( longlatproj4str )
    add_subant_coast.connectiontype = mapscript.MS_POSTGIS
    add_subant_coast.connection = PG_OCEAN_CONNECTION_STRING
    add_subant_coast.addProcessing("CLOSE_CONNECTION=DEFER")
    source_query = ("geom FROM add_subant_coastline_%s USING srid=3031 USING UNIQUE gid WHERE ST_Intersects(geom,%s)" \
        % (resstr,bordergeom))
    # print source_query
    add_subant_coast.data = source_query

    # Define Land Mask class
    class_land = mapscript.classObj( add_subant_coast )
    class_land.name = "Land Mask"
    style_land = mapscript.styleObj(class_land)
    if edgeflg == 1:
        style_land.outlinecolor.setRGB(0, 0, 0)
    else:
        style_land.outlinecolor.setRGB(196, 196, 196)
    style_land.width = int( ( 3.0 / 300.0 ) * dpi )
    style_land.color.setRGB(196, 196, 196)
    if aaflg == 1:
        style_land.antialias = True
    else:
        style_land.antialias = False

    # Draw layer onto image
    add_subant_coast.draw( mapobj, image )


# Draw ice chart using lists of polygons and ice types
def plot_icechart_polygons( regionstr, polylist, typelist, edgeflg, owflg, aaflg, \
    chart_style, dpi, mapobj, image ):
    # print edgeflg, owflg

    # Get map projection information
    PROJSTR = getproj()

    ice_types = [ [ 'Open Water',           [150, 200, 255] ], \
                  [ 'Very Open Drift Ice',  [140, 255, 160] ], \
                  [ 'Open Drift Ice',       [255, 255,   0] ], \
                  [ 'Close Drift Ice',      [255, 125,   7] ], \
                  [ 'Very Close Drift Ice', [255,   0,   0] ], \
                  [ 'Fast Ice',             [150, 150, 150] ] ]
    ntypes = len(ice_types)

    # Define symbols
    if chart_style != 'colour':
        symbol_set = define_symbols( mapobj )

    icepoly = []
    iceclass = []
    icestyle = []
    start = 1
    if owflg == 1:
        start = 0
    for i in range(ntypes):
        # for i in range(1):
        # print ice_types[i][0]
        icepoly.append( mapscript.layerObj( mapobj ) )
        iceclass.append( mapscript.classObj( icepoly[i] ) )
        icestyle.append( mapscript.styleObj( iceclass[i] ) )

        if i >= start:
            # Define ice polygon layer
            icepoly[i].name = 'Ice Chart Polygons'
            icepoly[i].type = mapscript.MS_LAYER_POLYGON
            icepoly[i].setProjection( PROJSTR[regionstr] )

            # Add polygons
            npoly = len( polylist )
            # print npoly
            for j in range( npoly ):
                # print ("\'%s\'" % typelist[j])
                if typelist[j] == ice_types[i][0]:
                    # print i
                    # print polylist[i]
                    poly = mapscript.shapeObj().fromWKT( polylist[j] )
                    poly.initValues(1)
                    # print i, typelist[j]
                    poly.setValue(0, typelist[j])
                    # print polylist[i][0:10], poly.type
                    icepoly[i].addFeature( poly )

            icepoly[i].status = mapscript.MS_ON

            # Define class for ice concentration
            iceclass[i].name = ice_types[i][0]
            if chart_style == 'colour':
                rgb = ice_types[i][1]
                # print rgb
                icestyle[i].color.setRGB( rgb[0], rgb[1], rgb[2])
                if edgeflg == 1:
                    icestyle[i].outlinecolor.setRGB(0, 0, 0)
                    icestyle[i].width = int( ( 2.0 / 300.0 ) * dpi )
                else:
                    icestyle[i].outlinecolor.setRGB( rgb[0], rgb[1], rgb[2])
                    icestyle[i].width = 0
            else:
                # Chart style for cross-hatching
                rgb = [ 25, 25, 61 ]
                # rgb = [ 255, 255, 255 ]
                icestyle[i].color.setRGB( rgb[0], rgb[1], rgb[2])
                # icestyle[i].outlinecolor.setRGB(0, 0, 0)

                if i == 0:
                    icestyle[i].symbol = symbol_set[i]
                    icestyle[i].size = 2
                    icestyle[i].width = 2
                    icestyle[i].gap = 4
                elif i == 1:
                    icestyle[i].symbol = symbol_set[i]
                    icestyle[i].size = 15
                    icestyle[i].width = 2
                    icestyle[i].gap = 5
                elif i == 2:
                    icestyle[i].symbol = symbol_set[i]
                    icestyle[i].size = 15
                    icestyle[i].width = 2
                    icestyle[i].gap = 15
                elif i == 3:
                    icestyle[i].symbol = symbol_set[i]
                    icestyle[i].size = 15
                    icestyle[i].width = 2
                    icestyle[i].angle = 0
                elif i == 4:
                    icestyle[i].symbol = symbol_set[i]
                    icestyle[i].size = 15
                    icestyle[i].width = 2
                    icestyle[i].angle = 90
                    substyle = mapscript.styleObj( iceclass[i] )
                    substyle.color.setRGB( rgb[0], rgb[1], rgb[2])
                    substyle.symbol = symbol_set[i]
                    substyle.size = 15
                    substyle.width = 2
                    substyle.angle = 0
                elif i == 5:
                    icestyle[i].symbol = symbol_set[i]
                    icestyle[i].size = 15
                    icestyle[i].width = 2
                    icestyle[i].angle = 45

            # Set anti-aliasing if required
            if aaflg == 1:
                icestyle[i].antialias = True
                if chart_style != 'colour' and i == 4:
                    substyle.antialias = True
            else:
                icestyle[i].antialias = False
                if chart_style != 'colour' and i == 4:
                    substyle.antialias = False

            # Draw layer onto image
            icepoly[i].draw( mapobj, image )

    # Draw outline polygons
    if chart_style != 'colour' and edgeflg == 1:
        iceoutline = mapscript.layerObj( mapobj )

        iceoutline.name = 'Ice Chart Polygon Outlines'
        iceoutline.type = mapscript.MS_LAYER_POLYGON
        iceoutline.setProjection( PROJSTR[regionstr] )
        outlineclass = mapscript.classObj( iceoutline )
        outlinestyle = mapscript.styleObj( outlineclass )

        # Add polygons
        npoly = len( polylist )
        # print npoly
        for j in range( npoly ):
            # print ("\'%s\'" % typelist[j])
            # print i
            # print polylist[i]
            poly = mapscript.shapeObj().fromWKT( polylist[j] )
            poly.initValues(1)
            # print i, typelist[j]
            poly.setValue(0, typelist[j])
            # print polylist[i][0:10], poly.type
            iceoutline.addFeature( poly )

        iceoutline.status = mapscript.MS_ON

        outlineclass.name = "Ice Polygon Outline"
        # outlinestyle.color.setRGB( 0, 0, 0 )
        outlinestyle.outlinecolor.setRGB(0, 0, 0)
        outlinestyle.width = int( ( 2.0 / 300.0 ) * dpi )
        outlinestyle.filled = False

        # Draw layer onto image
        iceoutline.draw( mapobj, image )



# Draw satellite coverage box
def plot_coverage( coverwkt, dpi, mapobj, image ):

    # Define satellite coverage polygon layer
    coverlyr = mapscript.layerObj( mapobj )
    coverlyr.name = 'Satellite Coverage'
    coverlyr.type = mapscript.MS_LAYER_POLYGON
    coverlyr.setProjection( '+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84' )

    # Add polygon
    # print coverwkt
    poly = mapscript.shapeObj().fromWKT( coverwkt )
    # print poly.type
    coverlyr.addFeature( poly )

    coverlyr.status = mapscript.MS_ON

    # Define class for satellite coverage polygon
    cvrclass = mapscript.classObj( coverlyr )
    cvrclass.name = "Satellite Coverage"
    cvrstyle = mapscript.styleObj( cvrclass )
    # cvrstyle.color.setRGB(0, 0, 0)
    cvrstyle.outlinecolor.setRGB(0, 0, 0)
    cvrstyle.width = int( ( 10.0 / 300.0 ) * dpi )
    cvrstyle.antialias = True

    # Draw layer onto image
    coverlyr.draw( mapobj, image )

# Draw satellite coverage box (as background)
def plot_coverage_background( coverwkt, mapobj, image ):

    # Define satellite coverage polygon layer
    coverlyr = mapscript.layerObj( mapobj )
    coverlyr.name = 'Satellite Coverage'
    coverlyr.type = mapscript.MS_LAYER_POLYGON
    coverlyr.setProjection( '+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84' )

    # Add polygon
    # print coverwkt
    poly = mapscript.shapeObj().fromWKT( coverwkt )
    # print poly.type
    coverlyr.addFeature( poly )

    coverlyr.status = mapscript.MS_ON

    # Define class for satellite coverage polygon
    cvrclass = mapscript.classObj( coverlyr )
    cvrclass.name = "Satellite Coverage"
    cvrstyle = mapscript.styleObj( cvrclass )
    cvrstyle.color.setRGB(0, 0, 0)
    cvrstyle.outlinecolor.setRGB(0, 0, 0)
    cvrstyle.width = 1
    cvrstyle.antialias = False

    # Draw layer onto image
    coverlyr.draw( mapobj, image )


# Draw Area Of Interest (AOI) box
def plot_aoi( shpfname, colour, linewidth, aaflg, dpi, mapobj, image ):

    # Define AOI layer
    aoi = mapscript.layerObj( mapobj )
    aoi.name = "AOI"
    aoi.type = mapscript.MS_LAYER_POLYGON
    aoi.status = mapscript.MS_ON
    longlatproj4str = '+proj=longlat +ellps=WGS84'
    aoi.setProjection( longlatproj4str )
    aoi.data = shpfname

    # Define AOI class
    class_aoi = mapscript.classObj( aoi )
    class_aoi.name = "AOI"
    style_aoi = mapscript.styleObj(class_aoi)
    style_aoi.outlinecolor.setRGB(colour[0], colour[1], colour[2])
    style_aoi.width = int( ( linewidth / 300.0 ) * dpi )
    # style_aoi.color.setRGB(196, 196, 196) Don't set a colour fill
    if aaflg == 1:
        style_aoi.antialias = True
    else:
        style_aoi.antialias = False

    # Draw layer onto image
    aoi.draw( mapobj, image )


# Draw satellite coverage box
def plot_coverage_with_date( coverwkt, coverdt, dpi, mapobj, image ):

    # Define satellite coverage polygon layer
    coverlyr = mapscript.layerObj( mapobj )
    coverlyr.name = 'Satellite Coverage'
    coverlyr.type = mapscript.MS_LAYER_POLYGON
    coverlyr.setProjection( PROJSTR )

    # Add polygons
    npoly = len(coverwkt)
    for i in range(npoly):
        poly = mapscript.shapeObj().fromWKT( coverwkt[i] )
        poly.text = coverdt[i]
        coverlyr.addFeature( poly )

    # Define class for satellite coverage polygon
    cvrclass = mapscript.classObj( coverlyr )
    cvrclass.name = "Satellite Coverage"
    cvrstyle = mapscript.styleObj( cvrclass )
    # cvrstyle.color.setRGB(0, 0, 0)
    cvrstyle.outlinecolor.setRGB(0, 0, 0)
    cvrstyle.width = int( ( 10.0 / 300.0 ) * dpi )
    cvrstyle.antialias = True

    cvrclass.label.type = mapscript.MS_TRUETYPE
    cvrclass.label.antialias = mapscript.MS_TRUE
    cvrclass.label.position = mapscript.MS_CC
    cvrclass.label.font = 'DejaVuSans'
    cvrclass.label.color.setRGB(0,0,0)
    cvrclass.label.size =  24
    cvrclass.label.partials = mapscript.MS_TRUE
    cvrclass.label.shadwsizex = 0
    cvrclass.label.shadwsizey = 0
    # print dir(cvrclass.label)
    # sys.exit()

    coverlyr.status = mapscript.MS_ON

    # Draw layer onto image
    coverlyr.draw( mapobj, image )

    # Draw the label cache (otherwise we do not see labels!) :-)
    mapobj.drawLabelCache(image)


# Draw satellite coverage box
def plot_coverage_as_single( regionstr, coverwkt, dpi, mapobj, image ):

    # Get map projection information
    PROJSTR = getproj()

    # Define satellite coverage polygon layer
    coverlyr = mapscript.layerObj( mapobj )
    coverlyr.name = 'Satellite Coverage'
    coverlyr.type = mapscript.MS_LAYER_POLYGON
    coverlyr.setProjection( PROJSTR[regionstr] )

    # Add polygons
    npoly = len(coverwkt)
    firstflg = 1
    for i in range(npoly):
        if firstflg == 1:
            polygeom = ogr.CreateGeometryFromWkt( coverwkt[i] )
            firstflg = 0
        else:
            tmppoly = ogr.CreateGeometryFromWkt( coverwkt[i] )
            polygeom = polygeom.Union( tmppoly)
    newcoverwkt = polygeom.ExportToWkt()
    poly = mapscript.shapeObj().fromWKT( newcoverwkt )
    coverlyr.addFeature( poly )

    # Define class for satellite coverage polygon
    cvrclass = mapscript.classObj( coverlyr )
    cvrclass.name = "Satellite Coverage"
    cvrstyle = mapscript.styleObj( cvrclass )
    # cvrstyle.color.setRGB(0, 0, 0)
    cvrstyle.outlinecolor.setRGB(0, 0, 0)
    cvrstyle.width = int( ( 10.0 / 300.0 ) * dpi )
    cvrstyle.antialias = True

    coverlyr.status = mapscript.MS_ON

    # Draw layer onto image
    coverlyr.draw( mapobj, image )


# Plot SST contours
def plot_sst_contours( regcode, sstarea, sstfn, sstlevels, aaflg, lblflg, dpi, mapobj, image ):

    # Construct Shapefile name
    bits = sstfn.split('/')
    rootfn = bits[-1][0:-4]
    shpfname = ("%s/%s.shp" % (TMPDIR,rootfn))

    # Create a filter string
    nlevels = len(sstlevels)
    if nlevels >= 1:
        filtstr = ("([SST] = %.1f)" % sstlevels[0])
        if nlevels > 1:
            for i in range(1,nlevels):
                filtstr = ("%s OR ([SST] = %.1f)" % (filtstr,sstlevels[i]))
            filtstr = ("(%s)" % filtstr)
    # print filtstr

    # Get proj4 map projection string for the Shapefile
    proj4str = getproj4( shpfname )

    # Define SST layer
    sst = mapscript.layerObj( mapobj )
    sst.name = "SST"
    sst.type = mapscript.MS_LAYER_LINE
    sst.status = mapscript.MS_ON
    sst.setProjection( proj4str )
    if nlevels >= 1:
        sst.setFilter( filtstr )
    sst.data = shpfname

    # Define SST class
    class_sst = mapscript.classObj( sst )
    class_sst.name = "SST"
    style_sst = mapscript.styleObj(class_sst)
    # style_sst.outlinecolor.setRGB(255, 0, 0)
    style_sst.width = int( ( 5.0 / 300.0 ) * dpi )
    style_sst.color.setRGB(255, 62, 3)
    if aaflg == 1:
        style_sst.antialias = True
    else:
        style_sst.antialias = False

    # Draw layer onto image
    sst.draw( mapobj, image )

    # Plot SST values if required
    if lblflg == 1:
        sst_labels( regcode, sstarea, sstfn, sstlevels, aaflg, dpi, mapobj, image )


# Plot SST labels
def sst_labels( regcode, sstarea, sstfn, sstlevels, aaflg, dpi, mapobj, image ):

    # Load the Shapefile driver
    shpdrv = ogr.GetDriverByName('ESRI Shapefile')

    # Load SST transect lines
    transfn = ("%s/sst_transects_%s.shp" % (INCDIR,regcode))
    sst_trans_area_list = []
    sst_trans_area_name = []
    sst_trans_area_geom = []
    src_ds = shpdrv.Open(transfn,0)
    inlay = src_ds.GetLayer()
    in_srs = inlay.GetSpatialRef()
    # trans_trans = osr.CoordinateTransformation(in_srs,trg_spatialReference)
    ntrans = inlay.GetFeatureCount()
    for i in range(ntrans):
        # Get input feature
        infeat = inlay.GetFeature(i)
        trans_id = infeat.GetField('id')
        trans_name = infeat.GetField('name')
        trans_geom = infeat.GetGeometryRef()
        # trans_geom.Transform(trans_trans)
        if trans_name == sstarea:
            sst_trans_area_list.append( trans_id )
            sst_trans_area_name.append( trans_name )
            sst_trans_area_geom.append( trans_geom.Clone() )
        infeat.Destroy()
    # Close file
    src_ds.Destroy()
    # print sst_trans_area_list
    # print sst_trans_area_name
    # print sst_trans_area_geom
    ntrans = len(sst_trans_area_list)

    # Construct Shapefile name
    bits = sstfn.split('/')
    rootfn = bits[-1][0:-4]
    shpfname = ("%s/%s.shp" % (TMPDIR,rootfn))
    # print shpfname
    src_ds = shpdrv.Open(shpfname,0)
    inlay = src_ds.GetLayer()
    in_srs = inlay.GetSpatialRef()

    # Set up lists
    sstlbl = []
    sstlblx = []
    sstlbly = []
    sstlblang = []

    # Number of features
    nfeat = inlay.GetFeatureCount()
    # Loop through SST levels
    for level in sstlevels:
        # Loop through features
        for i in range(nfeat):
            infeat = inlay.GetFeature(i)
            sstval = infeat.GetField('SST')

            # Only process if SST value is the same as the level
            if sstval == level:
                # Loop through transect lines and see if we have intersections
                sstline = infeat.GetGeometryRef()
                for j in range(ntrans):
                    if sstline.Intersects( sst_trans_area_geom[j] ):
                        # print level
                        # print sstline.ExportToWkt()
                        # print sst_trans_area_geom[j].ExportToWkt()
                        crossover = sstline.Intersection( sst_trans_area_geom[j] )
                        if crossover.GetGeometryType() == 1:
                            # 1 = POINT
                            px = crossover.GetX()
                            py = crossover.GetY()
                            # print px, py
                            angle = get_sst_angle( px, py, sstline )
                            sstlbl.append(level)
                            sstlblx.append(px)
                            sstlbly.append(py)
                            sstlblang.append(angle)
                        elif crossover.GetGeometryType() == 4:
                            # 4 = MULTIPOINT
                            np = crossover.GetGeometryCount()
                            for k in range(np):
                                subpoint = crossover.GetGeometryRef(k)
                                px = subpoint.GetX()
                                py = subpoint.GetY()
                                # print px, py
                                angle = get_sst_angle( px, py, sstline )
                                sstlbl.append(level)
                                sstlblx.append(px)
                                sstlbly.append(py)
                                sstlblang.append(angle)
                        else:
                            print 'Geometry of unknown type', crossover.GetGeometryType()
                            print crossover.ExportToWkt()
                            sys.exit()
                        # print level, j, crossover.ExportToWkt(), crossover.GetGeometryType()

            # Destroy the input feature
            infeat.Destroy()

    # print sstlbl
    # print sstlblang

    # Define satellite coverage polygon layer
    sstlbllyr = mapscript.layerObj( mapobj )
    sstlbllyr.name = 'SST Labels'
    sstlbllyr.type = mapscript.MS_LAYER_ANNOTATION
    sstlbllyr.setProjection( in_srs.ExportToProj4() )

    # Add polygons
    npoint = len(sstlbl)
    for i in range(npoint):
        pointgeom = ogr.Geometry(ogr.wkbPoint)
        pointgeom.AddPoint( sstlblx[i], sstlbly[i] )
        point = mapscript.shapeObj().fromWKT( pointgeom.ExportToWkt() )
        point.text = ("%d" % sstlbl[i])
        sstlbllyr.addFeature( point )
        pointgeom.Destroy()

    # Define class for satellite coverage polygon
    cvrclass = mapscript.classObj( sstlbllyr )
    cvrclass.name = "SST Label"
    cvrstyle = mapscript.styleObj( cvrclass )
    cvrstyle.color.setRGB(0, 0, 0)
    # cvrstyle.outlinecolor.setRGB(0, 0, 0)
    # cvrstyle.width = int( ( 10.0 / 300.0 ) * dpi )
    # cvrstyle.symbol = mapscript.MS_SYMBOL_ELLIPSE
    cvrstyle.size = 0.0
    # cvrstyle.angle = sstlblang

    cvrstyle.antialias = True
    # print dir(cvrstyle)
    # sys.exit()

    # SYMBOL
    # NAME 'circle'
    # TYPE ELLIPSE
    # FILLED TRUE
    # POINTS 1 1 END
    # END

    cvrclass.label.type = mapscript.MS_TRUETYPE
    cvrclass.label.antialias = mapscript.MS_TRUE
    cvrclass.label.position = mapscript.MS_CC
    cvrclass.label.font = 'DejaVuSans'
    cvrclass.label.color.setRGB(0,0,0)
    cvrclass.label.size =  24
    # cvrclass.label.angle = sstlblang
    cvrclass.label.partials = mapscript.MS_TRUE
    cvrclass.label.shadwsizex = 0
    cvrclass.label.shadwsizey = 0
    # print dir(cvrclass.label)
    # sys.exit()

    sstlbllyr.status = mapscript.MS_ON

    # Draw layer onto image
    sstlbllyr.draw( mapobj, image )

    # Draw the label cache (otherwise we do not see labels!) :-)
    mapobj.drawLabelCache(image)


# Get angle of SST line
def get_sst_angle( px, py, linegeom ):

    # Extract coordinates from SST line and calculate distance to crossover
    xlist = []
    ylist = []
    dist = []
    np = linegeom.GetPointCount()
    for i in range(np):
        lx = linegeom.GetX(i)
        ly = linegeom.GetY(i)
        dx = px - lx
        dy = py - ly
        dist.append( N.sqrt( (dx*dx) + (dy*dy) ) )
        xlist.append( lx )
        ylist.append( ly )

    # Find the SST line point at  minimum distance
    dist= N.array( dist, dtype=N.float32 )
    idx = N.nonzero( dist==N.min(dist) )[0][0]
    # print idx
    # print px, py
    # print xlist[idx], ylist[idx]
    # print dist[idx]

    # Calculate the angle using pythagoros
    dx = xlist[idx] - px
    dy = ylist[idx] - py
    # print dx, dy
    angle = N.degrees( N.arcsin( dx / dist[idx] ) )
    # print angle
    return angle


# Define symbol set
def define_symbols( mapobj ):
    ow_symbol = mapscript.symbolObj("ow")
    ow_symbol.type = mapscript.MS_SYMBOL_ELLIPSE
    # ow_symbol.type = mapscript.MS_SYMBOL_VECTOR
    ow_symbol.filled = False
    # p1 = mapscript.pointObj(0,0)
    # p2 = mapscript.pointObj(10,5)
    # l1 = mapscript.lineObj()
    # s1 = l1.add( p1 )
    # s2 = l1.add( p2 )
    # ow_np = ow_symbol.setPoints( l1 )
    ow_idx = mapobj.symbolset.appendSymbol(ow_symbol)

    vodi_symbol = mapscript.symbolObj("vodi")
    vodi_symbol.type = mapscript.MS_SYMBOL_ELLIPSE
    vodi_symbol.filled = False
    vodi_idx = mapobj.symbolset.appendSymbol(vodi_symbol)

    odi_symbol = mapscript.symbolObj("odi")
    odi_symbol.type = mapscript.MS_SYMBOL_VECTOR
    odi_symbol.filled = False
    p1 = mapscript.pointObj(0,0)
    p2 = mapscript.pointObj(0,10)
    l1 = mapscript.lineObj()
    s1 = l1.add( p1 )
    s2 = l1.add( p2 )
    odi_np = odi_symbol.setPoints( l1 )
    odi_idx = mapobj.symbolset.appendSymbol(odi_symbol)

    cdi_symbol = mapscript.symbolObj("cdi")
    cdi_symbol.type = mapscript.MS_SYMBOL_HATCH
    cdi_idx = mapobj.symbolset.appendSymbol(cdi_symbol)

    vcdi_symbol = mapscript.symbolObj("vcdi")
    vcdi_symbol.type = mapscript.MS_SYMBOL_HATCH
    vcdi_idx = mapobj.symbolset.appendSymbol(vcdi_symbol)

    fi_symbol = mapscript.symbolObj("fi")
    fi_symbol.type = mapscript.MS_SYMBOL_HATCH
    fi_idx = mapobj.symbolset.appendSymbol(fi_symbol)

    symbol_set = [ ow_idx, vodi_idx, odi_idx, cdi_idx, vcdi_idx, fi_idx ]

    return symbol_set


# Draw ice shelves (Antarctic only)
def plot_iceshelves( regionstr, shelfwkt, edgeflg, aaflg, dpi, mapobj, image ):

    # Get map projections
    PROJSTR = getproj()

    # Define ice shelves polygon layer
    shelflyr = mapscript.layerObj( mapobj )
    shelflyr.name = 'Ice Shelves'
    shelflyr.type = mapscript.MS_LAYER_POLYGON
    shelflyr.setProjection( PROJSTR[regionstr] )

    # Add polygons
    poly = mapscript.shapeObj().fromWKT( shelfwkt )
    shelflyr.addFeature( poly )

    # Define class for ice shelves polygon
    shelfclass = mapscript.classObj( shelflyr )
    shelfclass.name = "Ice Shelves"
    shelfstyle = mapscript.styleObj( shelfclass )
    shelfstyle.color.setRGB( 210, 210, 210 )
    if edgeflg == 1:
        shelfstyle.outlinecolor.setRGB(0, 0, 0)
        shelfstyle.width = int( ( 2.0 / 300.0 ) * dpi )
    else:
        shelfstyle.outlinecolor.setRGB( 210, 210, 210 )
        shelfstyle.width = 0
    if aaflg == 1:
        shelfstyle.antialias = True
    else:
        shelfstyle.antialias = False

    shelflyr.status = mapscript.MS_ON

    # Draw layer onto image
    shelflyr.draw( mapobj, image )


# Draw icebergs (Antarctic only)
def plot_icebergs( regionstr, bergwkts, bergnames, chart_style, lblflg, edgeflg, aaflg, dpi, mapobj, image ):

    # Get map projections
    PROJSTR = getproj()

    # Blank iceberg areas
    if chart_style == 'hatched':
        bergblank = mapscript.layerObj( mapobj )
        bergblank.name = 'Iceberg Polygon Blank'
        bergblank.type = mapscript.MS_LAYER_POLYGON
        bergblank.setProjection( PROJSTR[regionstr] )
        blankclass = mapscript.classObj( bergblank )
        blankstyle = mapscript.styleObj( blankclass )

        # Add polygons
        npoly = len(bergwkts)
        for i in range(npoly):
            poly = mapscript.shapeObj().fromWKT( bergwkts[i] )
            bergblank.addFeature( poly )
        bergblank.status = mapscript.MS_ON

        blankclass.name = "Iceberg Polygon Blank"
        blankstyle.color.setRGB( 255, 255, 255)
        blankstyle.width = int( ( 2.0 / 300.0 ) * dpi )
        blankstyle.filled = True

        # Draw layer onto image
        bergblank.draw( mapobj, image )

    # Define icebergs polygon layer
    berglyr = mapscript.layerObj( mapobj )
    berglyr.name = 'Ice Shelves'
    berglyr.type = mapscript.MS_LAYER_POLYGON
    berglyr.setProjection( PROJSTR[regionstr] )

    # Add polygons
    npoly = len(bergwkts)
    for i in range(npoly):
        poly = mapscript.shapeObj().fromWKT( bergwkts[i] )
        berglyr.addFeature( poly )

    # Define symbol for icebergs
    berg_symbol = mapscript.symbolObj("berg")
    berg_symbol.type = mapscript.MS_SYMBOL_HATCH
    berg_idx = mapobj.symbolset.appendSymbol(berg_symbol)

    # Define class for icebergs polygons
    bergclass = mapscript.classObj( berglyr )
    bergclass.name = "Ice Shelves"
    bergstyle = mapscript.styleObj( bergclass )
    if chart_style == 'colour':
        bergstyle.color.setRGB( 255, 255, 255 )
        bergstyle.filled = True
    else:
        bergstyle.color.setRGB( 255, 0, 0 )   # 255,0,0 ?
        bergstyle.filled = False
    bergstyle.outlinecolor.setRGB(255, 0, 0)
    bergstyle.width = int( ( 2.0 / 300.0 ) * dpi )
    if aaflg == 1:
        bergstyle.antialias = True
    else:
        bergstyle.antialias = False
    bergstyle.symbol = berg_idx
    bergstyle.size = 5
    bergstyle.width = 3
    bergstyle.angle = 45

    berglyr.status = mapscript.MS_ON

    # Draw layer onto image
    berglyr.draw( mapobj, image )

    # Draw outline polygons
    if edgeflg == 1:
        bergoutline = mapscript.layerObj( mapobj )

        bergoutline.name = 'Iceberg Polygon Outlines'
        bergoutline.type = mapscript.MS_LAYER_POLYGON
        bergoutline.setProjection( PROJSTR[regionstr] )
        outlineclass = mapscript.classObj( bergoutline )
        outlinestyle = mapscript.styleObj( outlineclass )

        # Add polygons
        npoly = len(bergwkts)
        for i in range(npoly):
            poly = mapscript.shapeObj().fromWKT( bergwkts[i] )
            bergoutline.addFeature( poly )
        bergoutline.status = mapscript.MS_ON

        outlineclass.name = "Iceberg Polygon Outline"
        outlinestyle.outlinecolor.setRGB(0, 0, 0)
        outlinestyle.width = int( ( 2.0 / 300.0 ) * dpi )
        outlinestyle.filled = False

        # Draw layer onto image
        bergoutline.draw( mapobj, image )

    # Plot iceberg names if required
    if lblflg == 1:
        iceberg_labels( regionstr, bergwkts, bergnames, aaflg, dpi, mapobj, image )


# Plot iceberg labels
def iceberg_labels( regionstr, bergwkts, bergnames, aaflg, dpi, mapobj, image ):

    # Get map projections
    PROJSTR = getproj()

    # Set up lists
    berglblx = []
    berglbly = []

    # Set label x+y offsets
    xoff = 0.0
    yoff = 0.0

    # Loop through iceberg polygons and get centre position for label
    npoly = len(bergwkts)
    for i in range(npoly):
        berggeom = ogr.CreateGeometryFromWkt( bergwkts[i] )
        centre = berggeom.Centroid()
        berglblx.append( centre.GetX()+xoff )
        berglbly.append( centre.GetY()+yoff )
        berggeom.Destroy()

    # Define iceberg labels layer
    berglbllyr = mapscript.layerObj( mapobj )
    berglbllyr.name = 'Iceberg Labels'
    berglbllyr.type = mapscript.MS_LAYER_ANNOTATION
    berglbllyr.setProjection( PROJSTR[regionstr] )

    # Add labels
    for i in range(npoly):
        if bergnames[i].upper() != 'UNKNOWN' and bergnames[i].upper() != 'NONE':
            pointgeom = ogr.Geometry(ogr.wkbPoint)
            pointgeom.AddPoint( berglblx[i], berglbly[i] )
            point = mapscript.shapeObj().fromWKT( pointgeom.ExportToWkt() )
            point.text = ("%s" % bergnames[i])
            berglbllyr.addFeature( point )
            pointgeom.Destroy()

    # Define class for iceberg label points
    brgptclass = mapscript.classObj( berglbllyr )
    brgptclass.name = "Iceberg Label"
    brgptstyle = mapscript.styleObj( brgptclass )
    brgptstyle.color.setRGB(0, 0, 0)
    brgptstyle.size = 0.0
    brgptstyle.antialias = True

    brgptclass.label.type = mapscript.MS_TRUETYPE
    brgptclass.label.antialias = mapscript.MS_TRUE
    brgptclass.label.position = mapscript.MS_CC
    brgptclass.label.font = 'DejaVuSans-Bold'
    brgptclass.label.color.setRGB(0,0,0)
    brgptclass.label.size =  24
    brgptclass.label.partials = mapscript.MS_TRUE
    brgptclass.label.shadwsizex = 0
    brgptclass.label.shadwsizey = 0

    berglbllyr.status = mapscript.MS_ON

    # Draw layer onto image
    berglbllyr.draw( mapobj, image )

    # Draw the label cache (otherwise we do not see labels!) :-)
    mapobj.drawLabelCache(image)


# Draw icebergs (Antarctic only)
def plot_nicbergs( regionstr, scale, bergwkts, bergnames, chart_style, lblflg, aaflg, dpi, mapobj, image ):

    # Get map projections
    PROJSTR = getproj()

    # Define symbol
    berg_symb_fn = ("%s/Ice_Symbols/41_Tabular_Iceberg_Very_Large.png" % INCDIR)
    berg_symb = mapscript.imageObj(berg_symb_fn, 'GD/PNG')
    symbol = mapscript.symbolObj('berg_from_img',berg_symb_fn)
    symbol.name = 'berg_from_img'
    symbol.type = mapscript.MS_SYMBOL_PIXMAP
    # symbol.imagepath = TMPDIR
    # format = mapscript.outputFormatObj('GD/PNG')
    # bergimg = symbol.getImage(format)
    # symbol_fn = ("%s/sym-%s.%s" % (TMPDIR,symbol.name,bergimg.format.extension))
    # bergimg.save(symbol_fn)
    symbol_index = mapobj.symbolset.appendSymbol(symbol)

    # Add points
    line = mapscript.lineObj()
    npoint = len(bergwkts)
    for i in range(npoint):
        point = ogr.CreateGeometryFromWkt( bergwkts[i] )
        pt = mapscript.pointObj( point.GetX(), point.GetY() )
        line.add( pt )
    shape = mapscript.shapeObj(mapscript.MS_SHAPE_POINT)
    shape.add( line )
    shape.setBounds()

    # Add NIC iceberg symbols
    nicberg = mapscript.layerObj( mapobj )
    nicberg.addFeature(shape)
    nicberg.setProjection( PROJSTR[regionstr] )
    nicberg.name = 'NIC Icebergs Symbols'
    nicberg.type = mapscript.MS_LAYER_POINT
    nicberg.connectiontype = mapscript.MS_INLINE
    nicberg.status = mapscript.MS_ON
    nicberg.transparency = mapscript.MS_GD_ALPHA

    nicclass = mapscript.classObj( nicberg )
    nicclass.name = 'NIC Iceberg Symbol'
    nicstyle = mapscript.styleObj( nicclass )
    nicstyle.symbol = symbol_index

    nicstyle.size = 50

    # Draw layer onto image
    nicberg.draw( mapobj, image )

    # Remove temporary symbol graphic
    # os.remove(symbol_fn)

    # Plot iceberg names if required
    if lblflg == 1:
        nicberg_labels( regionstr, scale, bergwkts, bergnames, aaflg, dpi, mapobj, image )


# Plot iceberg labels
def nicberg_labels( regionstr, scale, bergwkts, bergnames, aaflg, dpi, mapobj, image ):

    # Get map projections
    PROJSTR = getproj()

    # Set up lists
    berglblx = []
    berglbly = []

    # Set label x+y offsets
    xoff = (1.0/scale) * 25.0
    yoff = 0.0

    # Loop through iceberg polygons and get centre position for label
    npoly = len(bergwkts)
    for i in range(npoly):
        berggeom = ogr.CreateGeometryFromWkt( bergwkts[i] )
        centre = berggeom.Centroid()
        berglblx.append( centre.GetX()+xoff )
        berglbly.append( centre.GetY()+yoff )
        berggeom.Destroy()

    # Define iceberg labels layer
    berglbllyr = mapscript.layerObj( mapobj )
    berglbllyr.name = 'Iceberg Labels'
    berglbllyr.type = mapscript.MS_LAYER_ANNOTATION
    berglbllyr.setProjection( PROJSTR[regionstr] )

    # Add labels
    for i in range(npoly):
        if bergnames[i].upper != 'UNKNOWN' and bergnames[i].upper != 'NONE':
            pointgeom = ogr.Geometry(ogr.wkbPoint)
            pointgeom.AddPoint( berglblx[i], berglbly[i] )
            point = mapscript.shapeObj().fromWKT( pointgeom.ExportToWkt() )
            point.text = ("%s" % bergnames[i])
            berglbllyr.addFeature( point )
            pointgeom.Destroy()

    # Define class for iceberg label points
    brgptclass = mapscript.classObj( berglbllyr )
    brgptclass.name = "Iceberg Label"
    brgptstyle = mapscript.styleObj( brgptclass )
    brgptstyle.color.setRGB(0, 0, 0)
    brgptstyle.size = 0.0
    brgptstyle.antialias = True

    brgptclass.label.type = mapscript.MS_TRUETYPE
    brgptclass.label.antialias = mapscript.MS_TRUE
    brgptclass.label.position = mapscript.MS_CR
    brgptclass.label.font = 'DejaVuSans-Bold'
    brgptclass.label.color.setRGB(0,0,0)
    brgptclass.label.size =  24
    brgptclass.label.partials = mapscript.MS_TRUE
    brgptclass.label.shadwsizex = 0
    brgptclass.label.shadwsizey = 0

    berglbllyr.status = mapscript.MS_ON

    # Draw layer onto image
    berglbllyr.draw( mapobj, image )

    # Draw the label cache (otherwise we do not see labels!) :-)
    mapobj.drawLabelCache(image)

