#!/usr/bin/python

# Name:          auto_metline.py
# Purpose:       Automatically create METAREA-XIX report and send to Istjenesten, MET Norway
#                consultants and AARI.
# Usage:         No command line options = Use current date and do not overwrite
#                python auto_metline [yyyymmdd] [force_flg]
#                  yyyymmdd = Date to upload
#                  force_flg = 1 to overwrite existing files on server
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import string, time
from datetime import datetime
from ftplib import FTP
import zipfile

import numpy as N

import pg

from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo.gdalconst import *

# Email handling libraries
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

# General directories
BASEDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Python/include" % BASEDIR)
ANCDIR=("%s/Python/Ancillary" % BASEDIR)
OUTDIR=("%s/Outputs" % BASEDIR)

# Set the projection string (Polar Stereographic)
PROJSTR = '+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84'

# New system flag
NEWSYS = 1

# Active flag (send to AARI)
activeflg = 1

# E-mail flag (send to Istjenesten)
emailflg = 1

# METAREA's to process
areas = [ 19, 1 ]

# Output the start time
tmpt=time.gmtime()
tout=( '*** auto_metline.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/auto_metline.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Define date of ice chart
if len(sys.argv) >= 2:
    datestr = sys.argv[1]
    iceyr = int(datestr[0:4])
    icemh = int(datestr[4:6])
    icedy = int(datestr[6:8])
else:
    now = datetime.now()
    iceyr = now.year
    icemh = now.month
    icedy = now.day
icedt = datetime( iceyr, icemh, icedy, 15, 0, 0)
# print icedt
force_flg = 0
if len(sys.argv) == 3:
    force_flg = int(sys.argv[2])

# Database checking on new system
proc_flg = 0
if NEWSYS == 1:

    # Connect to database
    dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')

    # Check status in database
    sqltxt = ("SELECT id,metline_flg,metdiss_flg FROM production WHERE icechart_date = \'%4d-%02d-%02d\' AND region = \'arctic\';" \
        % (iceyr,icemh,icedy))
    queryres = dbcon.query( sqltxt )
    nrec = queryres.ntuples()
    if nrec == 0:
        print "auto_metline: No database record for this date. Stopping."
    elif nrec == 1:
        icechart_id = queryres.getresult()[0][0]
        metline_flg = queryres.getresult()[0][1]
        metdiss_flg = queryres.getresult()[0][2]
    else:
        print "auto_metline: Too many database records for this date. Stopping."
    if metline_flg <= 0:
        print "auto_metline: No METAREA line data for this date. Stopping."
    else:
        if metdiss_flg == 1 and force_flg != 1:
            print "auto_metline: METAREA line already sent and uploaded to AARI today. Stopping."
            proc_flg = 0
        else:
            proc_flg = 1

# Only process if proc_flg = 1 or NEWSYS = 0
if proc_flg == 1 or NEWSYS == 0:

    # Set spatial references, and transform from longitude/latitude to Polar Stereographic
    srs_spatialReference = osr.SpatialReference()
    srs_spatialReference.ImportFromProj4('+proj=longlat +ellps=WGS84')
    trg_spatialReference = osr.SpatialReference()
    trg_spatialReference.ImportFromProj4( PROJSTR )
    to_target = osr.CoordinateTransformation(srs_spatialReference,trg_spatialReference)
    to_ll = osr.CoordinateTransformation(trg_spatialReference,srs_spatialReference)
    
    # Connect to the database containing ice chart information
    con1 = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')
    
    # Get list of ice chart polygons from database
    querystr = ("SELECT AsText(line) FROM metline WHERE datetime = \'%s\';" \
        %  icedt )
    queryres = con1.query( querystr )
    # print queryres
    lineres = queryres.getresult()
    nline = len(lineres)

    # If no polygons, abort
    if nline == 0:
        print "No line data on this date.  Stopping."
        sys.exit()

    # Get the linestring and create a geometry
    linewkt = lineres[0][0]
    # print linewkt
    line_geom = ogr.CreateGeometryFromWkt( linewkt )

    # Extract points from line
    edgex = []
    edgey = []
    if line_geom is not None and line_geom.GetGeometryType() == ogr.wkbLineString:
        np = line_geom.GetPointCount()
        for i in range(np):
            # print "%10.5f, %10.5f" % ( line_geom.GetX(i), line_geom.GetY(i) )
            edgex = edgex + [ line_geom.GetX(i) ]
            edgey = edgey + [ line_geom.GetY(i) ]
    else:
        print "no line geometry\n"
        sys.exit()

    # Define Shapefile as the format using OGR
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # Go through the METAREAs
    for metarea in areas:

        # Open output file
        foutname = ('%s/%d/%s/ice%02d_%s_23.txt' % (OUTDIR,iceyr,icedt.strftime('%Y%m%d'), \
            metarea,icedt.strftime("%Y%m%d")) )
        # print foutname
        fout = open(foutname, 'w')

        # Header information
        fout.write(("ICE BULLETIN FOR METAREA %d ISSUED BY THE NORWEGIAN ICE SERVICE\n" % metarea))
        fout.write("(ISTJENESTEN@MET.NO) AT 23 UTC %d %s\n" % ( icedt.day,string.upper(icedt.strftime("%b")) ))

        # Open METAREA polygons file
        metareashp=('%s/metarea-%02d.shp' % (INCDIR,metarea))
        ds = ogr.Open( metareashp )
        if ds is None:
            print "Open failed.\n"
            sys.exit( 1 )

        # Open data layer
        lyrname = metareashp.split("/")[-1]
        lyrname = lyrname[:-4]
        # print lyrname
        lyr = ds.GetLayerByName( lyrname )
        lyr.ResetReading()

        # Get field definitions
        feat_defn = lyr.GetLayerDefn()
        # print 'Fields ', feat_defn.GetFieldCount()
        fieldnames = []
        for i in range(feat_defn.GetFieldCount()):
            field_defn = feat_defn.GetFieldDefn(i)
            # print field_defn.GetName(), field_defn.GetType()
            fieldnames = fieldnames + [ field_defn.GetName() ]
        # print fieldnames

        idlist = []
        namelist = []
        polywkblist = []
        npoly = 0
        polyxlist = []
        polyylist = []
        for feat in lyr:

            idlist = idlist + [ feat.GetField(0) ]    
            namelist = namelist + [ feat.GetField(2) ]

            geom = feat.GetGeometryRef()
            # print geom
            polywkblist = polywkblist + [ geom ]
            if geom is not None and geom.GetGeometryType() == ogr.wkbPolygon:
                tmpx = []
                tmpy = []
                ring = geom.GetGeometryRef(0)
                np = ring.GetPointCount()
                # print np
                for i in range(np):
                    # print "%10.5f, %10.5f" % ( geom.GetX(i), geom.GetY(i) )
                    lon, lat, z = ring.GetPoint(i)
                    tmpx = tmpx + [ lon ]
                    tmpy = tmpy + [ lat ]
                # print tmpx
                # print tmpy
                polyxlist = polyxlist + [ tmpx ]
                polyylist = polyylist + [ tmpy ]
                npoly = npoly + 1
            else:
                print "no polygon geometry\n"
                sys.exit()

        # Close METAREA Shapefile
        ds = None

        # print idlist
        # print namelist
        # print polyxlist
        # print polyylist
        # print npoly

        # sortedlist = sorted(idlist)
        # print sortedlist

        # Loop through polygons
        for i in sorted(idlist):

            # Get position of polygon in list (sorting by ID)
            idx = -9999
            for j in range(npoly):
                if idlist[j] == i:
                    idx = j
            # print idx, idlist[idx], namelist[idx]

            # Create polygon geometry
            ring = ogr.Geometry(ogr.wkbLinearRing)
            polyx = polyxlist[idx]
            polyy = polyylist[idx]
            for j in range(len(polyx)):
                # print polyx[j], polyy[j]
                ring.AddPoint(polyx[j], polyy[j])
            polygon = ogr.Geometry(ogr.wkbPolygon)
            polygon.AddGeometry(ring)

            # Loop through points in line, and see if they fall within polygon
            within = 0
            firstflg = 1
            segmentn = 0
            listn = 0
            segstr = []
            xlist = []
            ylist = []
            for j in range(len(edgex)):
                point = ogr.Geometry(ogr.wkbPoint)
                point.SetPoint_2D(0,edgex[j], edgey[j])
                within = int(point.Within(polygon))
                # print point, point.Within(polygon), firstflg, within
                if firstflg == 1:
                    firstflg = 0
                    if within == 1:
                        xlist = xlist + [ edgex[j] ]
                        ylist = ylist + [ edgey[j] ]
                        segmentn = segmentn + 1
                        segstr = segstr + [ listn ]
                        listn = listn + 1
                else:
                    line = ogr.Geometry(ogr.wkbLineString)
                    line.AddPoint(edgex[j-1], edgey[j-1])
                    line.AddPoint(edgex[j], edgey[j])
                    if oldwithin == 0 and within == 1:
                        # print "intersect entering"
                        intersect = line.Intersection(polygon)
                        # print line
                        # print intersect
                        # print intersect.GetX(0), intersect.GetY(0)
                        xlist = xlist + [ intersect.GetX(0) ]
                        ylist = ylist + [ intersect.GetY(0) ]
                        xlist = xlist + [ intersect.GetX(1) ]
                        ylist = ylist + [ intersect.GetY(1) ]
                        segmentn = segmentn + 1
                        segstr = segstr + [ listn ]
                        listn = listn + 2
                    elif oldwithin == 1 and within == 1:
                        # print "within"
                        xlist = xlist + [ edgex[j] ]
                        ylist = ylist + [ edgey[j] ]
                        listn = listn + 1
                    elif oldwithin == 1 and within == 0:
                        # print "intersect exiting"
                        intersect = line.Intersection(polygon)
                        # print line
                        # print intersect
                        xlist = xlist + [ intersect.GetX(1) ]
                        ylist = ylist + [ intersect.GetY(1) ]
                        listn = listn + 1
                    line.Destroy()
                point.Destroy()


                oldwithin = within

            # if segmentn > 0:
            #     print 'Segments = ', segmentn
            #     print segstr
            #     print listn
 
            polygon.Destroy()

            # print ' '
            # print xlist
            # print ylist

            # If point in sub-area, print report
            if len(xlist) > 0:

                # print ('Ice edge within METAREA-XIX Sub-Area \"%s\",' % namelist[idx].upper())
                # print 'as issued by the Norwegian Ice Service (istjenesten@met.no), is'
                # print 'north of the line delineated by the following coordinates:'
                fout.write(("\n%s\n" % namelist[idx].upper()))

                first = 1
                for segment in range(segmentn):
                    if first == 1:
                        coordline = 'ICE N OF '
                        first = 0
                        limit = 3
                    else:
                        coordline = ''
                        limit = 4
                    nlin = 0
                    start = segstr[segment]
                    if (segment+1) >= segmentn:
                        end = len(xlist)
                    else:
                        end = segstr[segment+1]
                    # print start, end
                    for j in range(start,end):
                        lat = ylist[j]
                        latdeg = abs(int(lat))
                        latmin = int((lat % 1) * 60)
                        # print lat, latdeg, latmin
                        if lat < 0:
                            latstr = ('%02d%02dS' % (latdeg,latmin))
                        else:
                            latstr = ('%02d%02dN' % (latdeg,latmin))
                        lon = xlist[j]
                        londeg = abs(int(lon))
                        lonmin = int((lon % 1) * 60)
                        if lon < 0:
                            lonstr = ('%03d%02dW' % (londeg,lonmin))
                        else:
                            lonstr = ('%03d%02dE' % (londeg,lonmin))
                        coordline = coordline + latstr + ' ' + lonstr
                        if j < (end-1):
                            coordline = coordline + ', '
                        nlin = nlin + 1
                        if nlin == limit:
                            fout.write(("%s\n" % coordline))
                            coordline = ''
                            nlin = 0
                    if nlin > 0:
                        fout.write(("%s\n" % coordline))
                    if segment < (segmentn-1):
                        fout.write("ICE EDGE EXITS AREA AND RE-ENTERS AT\n")

        fout.write("\nICE EDGE NOT FOR NAVIGATIONAL PURPOSES\n")
        fout.close()

        # E-mail report to Istjenesten for METAREA-XIX
        if emailflg == 1 and metarea == 19:

            # E-mail addresses
            MyAddress = 'istjenesten@met.no'
            TargetAddress = [ 'istjenesten@met.no', 'vnn.konsulenter@met.no' ]

            COMMASPACE = ', '

            # Create the container (outer) email message.
            msg = MIMEMultipart()
            msg['Subject'] = ("METAREA-XIX report for  %4d-%02d-%02d" % (iceyr,icemh,icedy))
            msg['From'] = MyAddress
            msg['To'] = COMMASPACE.join(TargetAddress)
            # CC to Istjenesten initially, remove when we are happy system is working
            # msg['Cc'] = MyAddress
            msg.preamble = "This message has been sent by the automatic product dissemination software of the Norwegian Ice Service."
            msg.preamble = ("%s Please contact %s if you have any questions.\r\n\r\n" \
                % (msg.preamble,MyAddress))

            # Create the text part of the message
            text = ""
            text = ("%sPlease find attached the METAREA-XIX report for %4d-%02d-%02d.\r\n\r\n" \
                % (text,iceyr,icemh,icedy))
            # print text
            part1 = MIMEText(text, 'plain')

            # Attach parts into message container
            msg.attach(part1)

            # Add the report file
            # Open the file in binary mode. Let the MIMEImage class automatically guess the specific image type.
            fnbits = foutname.split('/')
            shortfn = fnbits[-1]
            print shortfn
            repfile = open(foutname, 'rb')
            rep = MIMEText(repfile.read())
            repfile.close()
            rep.add_header('Content-Disposition', 'attachment; filename="%s"' % shortfn)
            msg.attach(rep)

            # Send mail via the SMTP server
            try:
                Outbox = smtplib.SMTP("smtp4.met.no")
                Outbox.sendmail(MyAddress,TargetAddress,msg.as_string())
                Outbox.quit()
            except:
                try:
                    Outbox = smtplib.SMTP("smtp5.met.no")
                    Outbox.sendmail(MyAddress,TargetAddress,msg.as_string())
                    Outbox.quit()
                except:
                    print 'No SMTP server available.'


        # Send results to AARI GMDSS site
        if metarea == 1 or metarea == 19:

            # Create a temporary Shapefile
            shpfname = ("%s/metarea_line.shp" % TMPDIR)
            if os.path.exists( shpfname ):
                driver.DeleteDataSource( shpfname )
            line_ds = driver.CreateDataSource( shpfname )
            line_lay = line_ds.CreateLayer( 'datestr', srs=srs_spatialReference, geom_type=ogr.wkbLineString )
            idfld = ogr.FieldDefn( 'id', ogr.OFTInteger )
            line_lay.CreateField( idfld )
            featdefn = line_lay.GetLayerDefn()

            # Create output feature
            ext_feat = ogr.Feature( featdefn )
            ext_feat.SetField( 'id', 0 )
            ext_feat.SetGeometry( line_geom )
            line_lay.CreateFeature( ext_feat )

            # Close dataset and destroy feature
            line_ds.Destroy()
            ext_feat.Destroy()

            # Create a zip file containing the Shapefile
            zipfname = ('%s/%d/%s/ice%02d_%s_23.zip' % \
                (OUTDIR,iceyr,icedt.strftime("%Y%m%d"),metarea,icedt.strftime("%Y%m%d")) )
            dbffname = shpfname[:-4]+'.dbf'
            shxfname = shpfname[:-4]+'.shx'
            prjfname = shpfname[:-4]+'.prj'
            # print zipfname
            edgezip = zipfile.ZipFile(zipfname,'w',zipfile.ZIP_DEFLATED)
            edgezip.write(dbffname)
            edgezip.write(shpfname)
            edgezip.write(shxfname)
            edgezip.write(prjfname)
            edgezip.close()

            # Delete the temporary Shapefile
            driver.DeleteDataSource( shpfname )

            # Transfer files
            if activeflg == 1:
                username = ('metarea%02d_ice' % metarea)
                password = ('metarea%02dgmdss' % metarea)
                # print username, password
                try:
                    aari = FTP(host='gmdss.aari.ru',user=username,passwd=password)
                    status = aari.cwd(('data/bull/%02d' % metarea))
                    targdir=icedt.strftime("%Y%m%d")
                    entries = aari.nlst()
                    present = 0
                    for fname in entries:
                        if targdir == fname:
                            present = 1
                    # print present
                    if present == 0:
                        status = aari.mkd(targdir)
                        # print status
                    status = aari.cwd(targdir)
                    # Send the text file
                    fin = open(foutname,'r')
                    storname = ('ice%02d_%s_23.txt' % (metarea,icedt.strftime("%Y%m%d")) )
                    cmd = ("STOR %s" % storname)
                    # print cmd
                    status = aari.storlines(cmd,fin)
                    fin.close()
                    # Send the Shapefile file zipped
                    fin = open(zipfname,'r')
                    storname = ('ice%02d_%s_23.zip' % (metarea,icedt.strftime("%Y%m%d")) )
                    cmd = ("STOR %s" % storname)
                    # print cmd
                    status = aari.storbinary(cmd,fin)
                    fin.close()
                    aari.quit()
                except:
                    print 'Problem with FTP transfer to AARI'

                    # E-mail addresses
                    MyAddress = 'istjenesten@met.no'
                    TargetAddress = [ 'istjenesten@met.no' ]

                    COMMASPACE = ', '

                    # Create the container (outer) email message.
                    msg = MIMEMultipart()
                    msg['Subject'] = ("Problem sending METAREA-XIX report for  %4d-%02d-%02d to AARI" % (iceyr,icemh,icedy))
                    msg['From'] = MyAddress
                    msg['To'] = COMMASPACE.join(TargetAddress)
                    # CC to Istjenesten initially, remove when we are happy system is working
                    # msg['Cc'] = MyAddress
                    msg.preamble = "Unable to send METAREA-XIX report to AARI."
                    msg.preamble = ("%s Please check AARI FTP server status and try uploading manually.\r\n\r\n" \
                        % (msg.preamble,MyAddress))

                    # Create the text part of the message
                    text = ""
                    # print text
                    part1 = MIMEText(text, 'plain')

                    # Attach parts into message container
                    msg.attach(part1)

                    # Send mail via the SMTP server
                    try:
                        Outbox = smtplib.SMTP("smtp4.met.no")
                        Outbox.sendmail(MyAddress,TargetAddress,msg.as_string())
                        Outbox.quit()
                    except:
                        try:
                            Outbox = smtplib.SMTP("smtp5.met.no")
                            Outbox.sendmail(MyAddress,TargetAddress,msg.as_string())
                            Outbox.quit()
                        except:
                            print 'No SMTP server available.'



    # On new system, enter database status and close connection
    if NEWSYS == 1:
        # Database status update
        timenow = datetime.now()
        dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
        sqltxt = ("UPDATE production SET (metdiss_flg,metdiss_dt) = (1,\'%s\') WHERE id = %d;"
            % (dtstr,icechart_id))
        print sqltxt
        dbcon.query( sqltxt )
    
        # Close datebase connection
        dbcon.close()

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** auto_metline.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

