#!/usr/bin/python

# Name:          auto_pvupload.py
# Purpose:       Automatically uploads product files to Polar View server.
#                NB: Need redevelopment for generic web server.
# Usage:         No command line options = Use current date and do not overwrite
#                  python auto_pvupload [yyyymmdd] [force_flg]
#                    yyyymmdd = Date to upload
#                    force_flg = 1 to overwrite existing files on server
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import string, time
from datetime import datetime

import pg

from ftplib import FTP

import PIL
from PIL import Image, ImageChops

# Standard directories
BASEDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Python/include" % BASEDIR)
ANCDIR=("%s/Python/Ancillary" % BASEDIR)
OUTDIR=("%s/Outputs" % BASEDIR)

# Web server upload options
WEBSERVIP='web server IP address'
WEBSERVUSER='username'
WEBSERVPASSWD='password'
WEBSERVDIR='Directory for file storage on web server'

# New system flag
NEWSYS = 1

# Active flag - to send files to Web Site and FTP
# Set flag = 2 to just upload to the archive
activeflg = 1


# Autocrop function for PIL
def trim(im):
    bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
    diff = ImageChops.difference(im, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        return im.crop(bbox)


# Output the start time
tmpt=time.gmtime()
tout=( '*** auto_pvupload.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/auto_pvupload.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Define date of ice chart
if len(sys.argv) >= 2:
    datestr = sys.argv[1]
    iceyr = int(datestr[0:4])
    icemh = int(datestr[4:6])
    icedy = int(datestr[6:8])
else:
    now = datetime.now()
    iceyr = now.year
    icemh = now.month
    icedy = now.day
icedt = datetime( iceyr, icemh, icedy, 15, 0, 0)
dtstr = icedt.strftime('%Y%m%d')
# print icedt, dtstr
force_flg = 0
if len(sys.argv) == 3:
    force_flg =  int(sys.argv[2])

# Database checking on new system
proc_flg = 0
if NEWSYS == 1:

    # Connect to database
    dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')

    # Check status in database
    sqltxt = ("SELECT id,finish_flg,outputs_flg,pvupload_flg,region FROM production WHERE icechart_date = \'%4d-%02d-%02d\';" \
        % (iceyr,icemh,icedy))
    # print sqltxt
    queryres = dbcon.query( sqltxt )
    nrec = queryres.ntuples()
    if nrec == 0:
        print "auto_pvupload: No database record for this date. Stopping."
    elif nrec >= 1:
        icechart_id = []
        finish_flg = []
        outputs_flg = []
        pvupload_flg = []
        regions = []
        proc_flg = []
        for i in range(nrec):
            icechart_id.append( queryres.getresult()[i][0] )
            finish_flg.append( queryres.getresult()[i][1] )
            outputs_flg.append( queryres.getresult()[i][2] )
            pvupload_flg.append( queryres.getresult()[i][3] )
            regions.append( (queryres.getresult()[i][4]).strip() )

            if finish_flg[i] <= 0:
                print ("auto_pvupload: Ice chart for %s not finished on this date. Stopping." % regions[i])
                proc_flg.append(0)
            else:
                if outputs_flg[i] <= 0:
                    print ("auto_pvupload: No output graphics for %s on this date. Stopping." % regions[i])
                    proc_flg.append(0)
                else:
                    if pvupload_flg[i] == 1 and force_flg != 1:
                        print ("auto_pvupload: Ice chart for %s already uploaded to Polar View server today. Stopping." % regions[i])
                        proc_flg.append(0)
                    else:
                        proc_flg.append(1)

# Only process if proc_flg = 1 or NEWSYS = 0
for i in range(len(proc_flg)):
    # Update dtstr
    dtstr = icedt.strftime('%Y%m%d')

    # Only process if proc_flg = 1 or NEWSYS = 0
    if proc_flg[i] == 1 or NEWSYS == 0:
    
        # Set ice chart area data
        ic_names = [ 'general', 'baltic', 'fram_strait', 'barents', 'denmark_strait', 'svalbard', 'oslofjord' ]
        ic_dirs = [ 'regs', 'regs', 'regs', 'regs', 'regs', 'highres', 'highres' ]
        # print ic_names
        # Names for Antarctic
        ic_ant_names = [ 'antarctic', 'peninsula', 'weddell_east', 'bransfield_strait', 'adelaide_island' ]
        ic_ant_dirs = [ 'antarctic', 'antarctic', 'antarctic', 'antarctic', 'antarctic' ]
        ic_ant_orientations = [ 'l', 'p', 'p', 'p', 'p' ]
    
        # Open FTP connection
        ftp = FTP (WEBSERVIP,WEBSERVUSER,WEBSERVPASSWD)

        # Get list of existing files
        # print 'here1', regions[i]
        if regions[i] == 'arctic':
            ftpdir = ("/var/www/%s" % ic_dirs[0])
            ftp.cwd( ftpdir )
            flist1 = ftp.nlst()
            dlist1 = []
            for j in range(len(flist1)):
                dlist1.append( ic_dirs[0] )
            ftpdir = ("/var/www/%s" % ic_dirs[5])
            ftp.cwd( ftpdir )
            flist2 = ftp.nlst()
            dlist2 = []
            for j in range(len(flist2)):
                dlist2.append( ic_dirs[5] )
            flist = flist1 + flist2
            dlist = dlist1 + dlist2
            # print flist
        elif regions[i] == 'antarctic':
            ftpdir = ("/var/www/%s" % ic_ant_dirs[0])
            ftp.cwd( ftpdir )
            flist = ftp.nlst()
            dlist = []
            for j in range(len(flist)):
                dlist.append( ic_ant_dirs[0] )
        pnglist = []
        pngdirlist = []
        for fname,fdir in zip(flist,dlist):
            if fname.find('.png') > -1 and fname != 'f24.png':
                pnglist.append( fname )
                pngdirlist.append( fdir )
        # print pnglist
        # print len(pnglist)
    
        # Loop through ice charts and upload to web site
        if regions[i] == 'arctic':
            for chart_name,chart_dir in zip(ic_names,ic_dirs):
                ftpdir = ("/var/www/%s" % chart_dir)
                # print chart_name, ftpdir
                # Change to relevant directory
                ftp.cwd( ftpdir )
                # Upload binary file
                localfn = ("%s/%d/%s/%s_%s.png" % (OUTDIR,iceyr,dtstr,chart_name,dtstr))
                # print localfn
                ftpcmd = ("STOR %s_%s.png" % (chart_name,dtstr))
                if activeflg == 1:
                    status = ftp.storbinary( ftpcmd, open( localfn, 'rb' ) )
        elif regions[i] == 'antarctic':
            for chart_name,chart_dir in zip(ic_ant_names,ic_ant_dirs):
                ftpdir = ("/var/www/%s" % chart_dir)
                # print chart_name, ftpdir
                # Change to relevant directory
                ftp.cwd( ftpdir )
                # Upload binary file
                localfn = ("%s/%d/%s/%s_%s.png" % (OUTDIR,iceyr,dtstr,chart_name,dtstr))
                # print localfn
                ftpcmd = ("STOR %s_%s.png" % (chart_name,dtstr))
                if activeflg == 1:
                    status = ftp.storbinary( ftpcmd, open( localfn, 'rb' ) )
    
        # yr.no uses static link to /var/www/regs/c_map1.jpg
        # Copy general_yyyymmdd.jpg to this to cover (and general_yyyymmdd.png to latest_icechart.png)
        if regions[i] == 'arctic':
            ftpdir = "/var/www/regs"
            # print ftpdir
            ftp.cwd( ftpdir )
            localfn = ("%s/%d/%s/general_%s.jpg" % (OUTDIR,iceyr,dtstr,dtstr))
            ftpcmd = "STOR c_map1.jpg"
            # print localfn
            # print ftpcmd
            if activeflg == 1:
                status = ftp.storbinary( ftpcmd, open( localfn, 'rb' ) )
            localfn = ("%s/%d/%s/general_%s.png" % (OUTDIR,iceyr,dtstr,dtstr))
            ftpcmd = "STOR latest_icechart.png"
            if activeflg == 1:
                status = ftp.storbinary( ftpcmd, open( localfn, 'rb' ) )

        # Arctic web pages
        if regions[i] == 'arctic':
    
            # Update main.html
            templfn = ("%s/pv_main_template.html" % INCDIR)
            mainfn = ("%s/pv_main_tmp.html" % TMPDIR)
            template = open( templfn, 'r' )
            main = open( mainfn, 'w' )
            for tline in template:
                if tline.find('YYYYMMDD') > -1:
                    tline = tline.replace( 'YYYYMMDD', dtstr )
                    # print tline
                main.write( tline )
            template.close()
            main.close()
            # Upload to server
            ftp.cwd( "/var/www" )
            if activeflg == 1:
                status = ftp.storbinary( "STOR main.html", open( mainfn, 'rb' ) )
            os.remove( mainfn )
    
            # Update clickmap.htm
            templfn = ("%s/pv_clickmap_template.htm" % INCDIR)
            clickmapfn = ("%s/pv_clickmap_tmp.htm" % TMPDIR)
            template = open( templfn, 'r' )
            clickmap = open( clickmapfn, 'w' )
            for tline in template:
                if tline.find('YYYYMMDD') > -1:
                    tline = tline.replace( 'YYYYMMDD', dtstr )
                    # print tline
                clickmap.write( tline )
            template.close()
            clickmap.close()
            # Upload to ftp
            ftp.cwd( "/var/www" )
            if activeflg == 1:
                status = ftp.storbinary( "STOR clickmap.htm", open( clickmapfn, 'rb' ) )
            os.remove( clickmapfn )

        elif regions[i] == 'antarctic':
    
            # Update main.html
            templfn = ("%s/pv_Antarctic_template.html" % INCDIR)
            antarcfn = ("%s/pv_Antarctic_tmp.html" % TMPDIR)
            template = open( templfn, 'r' )
            main = open( antarcfn, 'w' )
            for tline in template:
                if tline.find('YYYYMMDD') > -1:
                    tline = tline.replace( 'YYYYMMDD', dtstr )
                    # print tline
                main.write( tline )
            template.close()
            main.close()
            # Upload to server
            ftp.cwd( "/var/www" )
            if activeflg == 1:
                status = ftp.storbinary( "STOR Antarctic.html", open( antarcfn, 'rb' ) )
            os.remove( antarcfn )

        # Update Antarctic thumbnails
        if regions[i] == 'antarctic':
            for ic_name,ic_orient in zip(ic_ant_names,ic_ant_orientations):
                inppng = ("%s/%d/%s/%s_%s.png" % (OUTDIR,iceyr,dtstr,ic_name,dtstr))
                outjpg = ("%s/newstyle_%s.jpg" % (TMPDIR,ic_name))
                # print inppng
                # print outjpg
                if ic_orient == 'l':
                    basewidth = 217
                else:
                    basewidth = 153
                img = Image.open(inppng)
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                # print hsize
                img = trim(img)
                img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                img.save(outjpg)
                # Upload to server
                ftp.cwd( "/var/www/antarctic" )
                if activeflg == 1:
                    status = ftp.storbinary( ("STOR newstyle_%s.jpg" % ic_name), open( outjpg, 'rb' ) )
                    status = ftp.sendcmd( ("site chmod 664 newstyle_%s.jpg" % ic_name) )
                # Delete JPG file
                # os.remove( outjpg )
            # sys.exit()
    
        # Delete old ice charts
        count = 0
        for fname,fdir in zip(pnglist,pngdirlist):
            ftpdir = ("/var/www/%s" % fdir)
            # Change to relevant directory
            ftp.cwd( ftpdir )
            # if count % 100 == 0:
            #     print count, fdir, fname
            if activeflg == 1:
                status = ftp.delete( fname )
            count = count + 1
    
        # Copy files to the archive (general_YYYYMMDD.png, svalbard_YYYYMMDD.png)
        if regions[i] == 'arctic':
            dtstr = icedt.strftime('%Y%m%d')
            ftpdir = "/var/www/archive"
            ftp.cwd( ftpdir )
            # Check if year directory exists
            yearstr = ("%d" % iceyr)
            if yearstr in ftp.nlst():
                pass
            else:
                if activeflg >= 1:
                    ftp.mkd( yearstr )
            ftpdir = ("%s/%s" % (ftpdir,yearstr))
            ftp.cwd( ftpdir )
            # Check if date directory exists
            if dtstr in ftp.nlst():
                pass
            else:
                print "Creating directory"
                if activeflg >= 1:
                    ftp.mkd( dtstr )
            ftpdir = ("%s/%s" % (ftpdir,dtstr))
            ftp.cwd( ftpdir )
            # Upload general_YYYYMMDD.png
            localfn = ("%s/%d/%s/general_%s.png" % (OUTDIR,iceyr,dtstr,dtstr))
            ftpcmd = ("STOR general_%s.png" % dtstr)
            if activeflg >= 1:
                status = ftp.storbinary( ftpcmd, open( localfn, 'rb' ) )
            # Upload svalbard_YYYYMMDD.png
            localfn = ("%s/%d/%s/svalbard_%s.png" % (OUTDIR,iceyr,dtstr,dtstr))
            ftpcmd = ("STOR svalbard_%s.png" % dtstr)
            if activeflg >= 1:
                status = ftp.storbinary( ftpcmd, open( localfn, 'rb' ) )
    
        # Close FTP connection
        ftp.close()
    
        # On new system, enter database status
        # print 'here2', regions[i]
        if NEWSYS == 1:
            # Database status update
            timenow = datetime.now()
            tstamp = timenow.strftime('%Y-%m-%d %H:%M:%S')
            sqltxt = ("UPDATE production SET (pvupload_flg,pvupload_dt) = (1,\'%s\') WHERE id = %d;"
                % (tstamp,icechart_id[i]))
            print sqltxt
            if activeflg > 0:
                dbcon.query( sqltxt )
        
# Close datebase connection
if NEWSYS == 1:
    dbcon.close()

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** auto_pvupload.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

