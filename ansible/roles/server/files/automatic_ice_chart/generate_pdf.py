#!/usr/bin/python

# Name:          generate_pdf.py
# Purpose:       Use the reportlab library to make a PDF file.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

from reportlab.lib.pagesizes import A4, landscape, portrait, cm, inch
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm

def generate_pdf( pdffn, orientation, dpi, pngfn ):
    if orientation == 'landscape':
        c = canvas.Canvas( pdffn, pagesize=landscape(A4) )
        c.drawImage( pngfn, 0, 0, 29.7*cm, 21.0*cm)
    else:
        c = canvas.Canvas( pdffn, pagesize=portrait(A4) )
        c.drawImage( pngfn, 0, 0, 21.0*cm, 29.7*cm)
    # c.showPage()
    c.save()
