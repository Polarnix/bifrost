#!/usr/bin/python

# Name:          auto_print.py
# Purpose:       Automatically print general and svalbard products.
# Usage:         No command line options = Use current date and do not overwrite
#                python auto_print [yyyymmdd] [force_flg]
#                  yyyymmdd = Date to upload
#                  force_flg = 1 to overwrite existing files on server
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import string, time
from datetime import datetime

import pg

from ftplib import FTP

import cups

# Standard directories
BASEDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Python/include" % BASEDIR)
ANCDIR=("%s/Python/Ancillary" % BASEDIR)
OUTDIR=("%s/Outputs" % BASEDIR)
TELEXDIR=("%s/Telexpts/Outputs" % BASEDIR)

# New system flag
NEWSYS = 1

# Active flag - to send files to printer
activeflg = 1

# Number of copies
COPIES = 3

# Printer to use
PRINTER = 'vnn2etg_06_F_A4'
# PRINTER = 'vnn2etg_06_FD_A4'
# PRINTER = 'vnn2etg_03_F'    # Alternative colour printer - just prints garbage!

# Output the start time
tmpt=time.gmtime()
tout=( '*** auto_print.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/auto_print.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Define date of ice chart
if len(sys.argv) >= 2:
    datestr = sys.argv[1]
    iceyr = int(datestr[0:4])
    icemh = int(datestr[4:6])
    icedy = int(datestr[6:8])
else:
    now = datetime.now()
    iceyr = now.year
    icemh = now.month
    icedy = now.day
icedt = datetime( iceyr, icemh, icedy, 15, 0, 0)
dtstr = icedt.strftime('%Y%m%d')
# print icedt, dtstr
force_flg = 0
if len(sys.argv) == 3:
    force_flg =  int(sys.argv[2])

# Database checking on new system
proc_flg = 0
if NEWSYS == 1:

    # Connect to database
    dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')

    # Check status in database
    sqltxt = ("SELECT id,finish_flg,outputs_flg,print_flg,telexpts_flg FROM production WHERE icechart_date = \'%4d-%02d-%02d\' AND region = \'arctic\';" \
        % (iceyr,icemh,icedy))
    # print sqltxt
    queryres = dbcon.query( sqltxt )
    nrec = queryres.ntuples()
    if nrec == 0:
        print "auto_print: No database record for this date. Stopping."
    elif nrec == 1:
        icechart_id = queryres.getresult()[0][0]
        finish_flg = queryres.getresult()[0][1]
        outputs_flg = queryres.getresult()[0][2]
        print_flg = queryres.getresult()[0][3]
        telexpts_flg = queryres.getresult()[0][3]
    else:
        print "auto_print: Too many database records for this date. Stopping."
    if finish_flg <= 0:
        print "auto_print: Ice chart not finished on this date. Stopping."
    else:
        if outputs_flg <= 0:   #  and telexpts_flg <= 0
            print "auto_print: No output graphics on this date. Stopping."
        else:
            if print_flg == 1 and force_flg != 1:
                print "auto_print: Ice chart already printed today. Stopping."
                proc_flg = 0
            else:
                proc_flg = 1
# print proc_flg

# Only process if proc_flg = 1 or NEWSYS = 0
if proc_flg == 1 or NEWSYS == 0:

    # Set ice chart area data
    ic_names = [ 'general', 'svalbard' ]

    # Open connections to CUPS printer
    if activeflg == 1:
        conn = cups.Connection()
    options = {'media':'a4','orientation-requested':'3','fit-to-page':'True'}

    # Loop through areas
    for areaname in ic_names:
        # print areaname

        # Construct filename for printing
        pdffn = ("%s/%d/%s/%s_%4d%02d%02d.pdf" % (OUTDIR,iceyr,icedt.strftime('%Y%m%d'),areaname,iceyr,icemh,icedy))
        # print pdffn

        # Loop through number of copies
        for i in range(COPIES):

            # Create job in CUPS queue
            jobdesc = ("ice_chart_%s_%4d%02d%02d" % (areaname,iceyr,icemh,icedy))

            # Show status
            if activeflg == 1:
                jobid = conn.printFile(PRINTER,pdffn,jobdesc,options)
                print ("%20s %5d" % (areaname,jobid))

    # Print telex points diagnostic map
    try:
        pdffn = ("%s/ice_%4d%02d%02d_telexpts.pdf" % (TELEXDIR,iceyr,icemh,icedy))
        jobdesc = ("ice_%4d%02d%02d_telexpts" % (iceyr,icemh,icedy))
        if activeflg == 1:
            jobid = conn.printFile(PRINTER,pdffn,jobdesc,options)
            print ("%20s %5d" % (areaname,jobid))
    except:
        pass

    # On new system, enter database status and close connection
    if NEWSYS == 1:
        # Database status update
        timenow = datetime.now()
        dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
        sqltxt = ("UPDATE production SET (print_flg,print_dt) = (1,\'%s\') WHERE id = %d;"
            % (dtstr,icechart_id))
        print sqltxt
        dbcon.query( sqltxt )
    
        # Close datebase connection
        dbcon.close()

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** auto_print.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

