#!/bin/bash

# Name:          wkend.sh
# Purpose:       Shell script to run Python scripts generating ice chart output products (on weekends).
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Link to local copy of Mapscript 6.0.3
export PYTHONPATH=/disk1/Istjenesten/New_Production_System/Mapscript-6.0.3/lib/python2.7/site-packages:/disk1/Istjenesten/New_Production_System/PyDAV-0.21/lib/python2.7/site-packages:$PYTHONPATH

PYTHONDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart/Python'

# Generate product files

# Generate ice chart images
/usr/bin/python "${PYTHONDIR}/auto_icechart_wkend.py"    >> "${PYTHONDIR}/../Logs/auto_icechart_wkend.log"
# Generate grid files for MyOcean
# /usr/bin/python "${PYTHONDIR}/auto_grid.py"        >> "${PYTHONDIR}/../Logs/auto_grid.log"
# Generate SAF validation files
# /usr/bin/python "${PYTHONDIR}/auto_safvalid.py"    >> "${PYTHONDIR}/../Logs/auto_safvalid.log"
# Generate telex points
# /usr/bin/python "${PYTHONDIR}/auto_telexpts.py"    >> "${PYTHONDIR}/../Logs/auto_telexpts.log"
# Generate SIGRID3.3 files (NOT YET COMPLETE)"
# /usr/bin/python "${PYTHONDIR}/auto_sigrid3.py"     >> "${PYTHONDIR}/../Logs/auto_sigrid3.log"

# GICE model
# 1. Generate products from model output
# 2. Upload to ?

# Archive product files on Istjenesten server
# /usr/bin/python "${PYTHONDIR}/auto_archive.py"     >> "${PYTHONDIR}/../Logs/auto_archive.log"

# Distribute products

# Print copies of general and svalbard charts, and a telex points diagnostic
/usr/bin/python "${PYTHONDIR}/auto_print_wkend.py"       >> "${PYTHONDIR}/../Logs/auto_print_wkend.log"
# Send out products by e-mail
# /usr/bin/python "${PYTHONDIR}/auto_disseminate.py" >> "${PYTHONDIR}/../Logs/auto_disseminate.log"
# Send out METAREA-19 ice edge line to VNN consultants and AARI
# /usr/bin/python "${PYTHONDIR}/auto_metline.py"     >> "${PYTHONDIR}/../Logs/auto_metline.log"
# Upload files to iceservice.met.no
# /usr/bin/python "${PYTHONDIR}/auto_iceweb.py"      >> "${PYTHONDIR}/../Logs/auto_iceweb.log"
# Upload files to Polar View web server and adjust web pages
# /usr/bin/python "${PYTHONDIR}/auto_pvupload.py"    >> "${PYTHONDIR}/../Logs/auto_pvupload.log"
# Upload files to FTP server
# /usr/bin/python "${PYTHONDIR}/auto_ftpupload.py"   >> "${PYTHONDIR}/../Logs/auto_ftpupload.log"

# Routines for FoU

# Upload the MyOcean grid file
# /usr/bin/python "${PYTHONDIR}/auto_gridupload.py"  >> "${PYTHONDIR}/../Logs/auto_gridupload.log"
# Upload the ice chart Shapefile to the Oslo Routine server
# /usr/bin/python "${PYTHONDIR}/auto_routine.py"     >> "${PYTHONDIR}/../Logs/auto_routine.log"
# Upload the SAF validation files
# /usr/bin/python "${PYTHONDIR}/auto_safupload.py"   >> "${PYTHONDIR}/../Logs/auto_safupload.log"

exit
