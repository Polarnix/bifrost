#!/usr/bin/python

# Name:          antarctic_sigrid33.py
# Purpose:       Automatically generate Antarctic SIGRID-3.3 products, and upload to AARI FTP server.
# Usage:         No command line options = Use current date and do not overwrite
#                python antarctic_sigrid33 [-d yyyymmdd] [-s send_to_aari] [-f force_flg]
#                  yyyymmdd = Date to upload
#                  send_to_aari = 1 to upload the files to the AARI FTP
#                  force_flg = 1 to overwrite existing files on server
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import time
from datetime import date, datetime, timedelta
import argparse
import urllib2
import zipfile
from ftplib import FTP

import pg

import osgeo.ogr as ogr
import osgeo.osr as osr

from icechart_utils import thous, mkdate, getproj, icechart_areas, get_si3poly_defn
# from si33_ANT_xml import si33_ANT_xml
from file_utils import testoutdir

BASEDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Python/include" % BASEDIR)
ANCDIR=("%s/Python/Ancillary" % BASEDIR)
OUTDIR=("/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart/Outputs")

GMDSS_URL = 'http://ice.aari.aq/antice'
GMDSS_DIR = '/disk1/Istjenesten/New_Production_System/GMDSS/Antarctic'

# Set the projection strings (Polar Stereographic)
PROJSTR = getproj()

# New system flag
NEWSYS = 1

# Active flag - enables writing to database
active_flg = 0

# Output the start time
tmpt=time.gmtime()
tout=( '*** antarctic_sigrid33.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/antarctic_sigrid33.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Get today's date
today = time.localtime()
todaystr = time.strftime('%Y-%m-%d',today)
# print todaystr

# Get input parameters using argparse
parser = argparse.ArgumentParser()
parser.add_argument("-d","--date",help="Date for ice chart [yyyymmdd]",type=mkdate,default=today)
parser.add_argument("-s","--send",help="Send to AARI? [0/1]",type=int,default=0,choices=[0,1])
parser.add_argument("-f","--force",help="Force processing? [0/1]",type=int,default=0,choices=[0,1])
args = parser.parse_args()
iceyr = (args.date).tm_year
icemh = (args.date).tm_mon
icedy = (args.date).tm_mday
datestr = ("%4d%02d%02d" % (iceyr,icemh,icedy))
icedt = datetime( iceyr, icemh, icedy, 15, 0, 0 )
force_flg = args.force
send_flg = args.send

# Connect to database
dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')

# Database checking on new system
proc_flg = 0
if NEWSYS == 1:
        
    # Check status in production database
    sqltxt = "SELECT id,finish_flg,sigrid_flg,outputs_flg FROM production WHERE"
    sqltxt = ("%s icechart_date = \'%4d-%02d-%02d\'" % (sqltxt,iceyr,icemh,icedy))
    sqltxt = ("%s AND region = \'antarctic\';" % (sqltxt))
    # print sqltxt
    queryres = dbcon.query( sqltxt )
    nrec = queryres.ntuples()
    if nrec == 0:
        print ("antarctic_sigrid33: No database record for \'antarctic\' this date.")
    elif nrec == 1:
        icechart_id = queryres.getresult()[0][0]
        finish_flg = queryres.getresult()[0][1]
        sigrid_flg = queryres.getresult()[0][2]
        outputs_flg = queryres.getresult()[0][3]

        if finish_flg <= 0:
            print ("antarctic_sigrid33: \'antarctic\' ice chart not finished on this date.")
        else:
            if sigrid_flg == 1 and force_flg != 1:
                print ("antarctic_sigrid33: \'antarctic\' SIGRID-3.3 product already created today.")
                proc_flg = 0
            else:
                proc_flg = 1
            if outputs_flg == 0:
                print ("antarctic_sigrid33: \'antarctic\' output charts not created yet.")
                proc_flg = 0
    else:
        print ("antarctic_sigrid33: Too many \'antarctic\' database records for this date.")

    # Check SST status in database
    sst_flg = 0
    yesterday = icedt - timedelta(days=1)
    sqltxt = "SELECT ant_flg FROM sstproc WHERE"
    sqltxt = ("%s sstdate = \'%s\';" % (sqltxt,yesterday.strftime('%Y-%m-%d')))
    # print sqltxt
    queryres = dbcon.query( sqltxt )
    nrec = queryres.ntuples()
    if nrec == 0:
        print ("antarctic_sigrid33: No SST processing record for \'antarctic\' this date.")
    elif nrec == 1:
        sst_flg = queryres.getresult()[0][0]
        if sst_flg == 1:
            sstfn = ("%s/%d/%s/%s120000-UKMO-L4_GHRSST-SSTfnd-OSTIA-GLOB-v02.0-fv02.0.zip" \
                % (OUTDIR,yesterday.year, \
                yesterday.strftime('%Y%m%d'),yesterday.strftime('%Y%m%d')))
            # print sstfn
            if os.path.isfile(sstfn):
                procflg = 1
        else:
            print ("antarctic_sigrid33: SST data not yet available on this date.")
            procflg = 0

    
# Only process if proc_flg = 1 or NEWSYS = 0
if proc_flg == 1 or NEWSYS == 0:

    # Set ice chart area data
    [ ic_names, ic_old_names, ic_ullr, ic_coast, ic_orient, ic_legend, ic_scalebar ] = icechart_areas()
    narea = len(ic_names['antarctic'])

    # Set spatial references, and transform from longitude/latitude to Polar Stereographic
    srs_spatialReference = osr.SpatialReference()
    srs_spatialReference.ImportFromProj4('+proj=longlat +ellps=WGS84')
    trg_spatialReference = osr.SpatialReference()
    trg_spatialReference.ImportFromProj4( PROJSTR['antarctic'] )
    to_target = osr.CoordinateTransformation(srs_spatialReference,trg_spatialReference)
    to_ll = osr.CoordinateTransformation(trg_spatialReference,srs_spatialReference)

    # Define coverage polygon
    areano = 0
    cvrullr = (ic_ullr['antarctic'])[areano]
    # print cvrullr
    npoint = 10
    geomstr = ("POLYGON((%.2f %.2f," % (float(cvrullr[0]),float(cvrullr[1])))
    for j in range(1,npoint):
        xpos = cvrullr[0] + (j * ( (float(cvrullr[2]) - float(cvrullr[0])) / float(npoint) ) )
        # print xpos
        geomstr = ("%s %.2f %.2f," % (geomstr,xpos,float(cvrullr[1])))
    for j in range(npoint):
        ypos = cvrullr[1] - (j * ( (float(cvrullr[1]) - float(cvrullr[3])) / float(npoint) ) )
        # print ypos
        geomstr = ("%s %.2f %.2f," % (geomstr,float(cvrullr[2]),ypos))
    for j in range(npoint):
        xpos = cvrullr[2] - (j * ( (float(cvrullr[2]) - float(cvrullr[0])) / float(npoint) ) )
        # print xpos
        geomstr = ("%s %.2f %.2f," % (geomstr,xpos,float(cvrullr[3])))
    for j in range(npoint):
        ypos = cvrullr[3] + (j * ( (float(cvrullr[1]) - float(cvrullr[3])) / float(npoint) ) )
        # print ypos
        geomstr = ("%s %.2f %.2f," % (geomstr,float(cvrullr[0]),ypos))
    geomstr = ("%s %.2f %.2f))" % (geomstr,float(cvrullr[0]),float(cvrullr[1])))        
    coverage = ogr.CreateGeometryFromWkt( geomstr, trg_spatialReference )

    # Read ice shelf polygon data
    sqltxt = "SELECT satellite,datestr,dateend,ST_AsText( polygon ) FROM iceshelf"
    sqltxt = ("%s WHERE datetime = \'%s\' ORDER BY id;" % (sqltxt,icedt.strftime('%Y-%m-%d %H:%M:%S')))
    print sqltxt
    results = (dbcon.query( sqltxt )).getresult()
    iceshelf = []
    for record in results:
        # print record[0:3]
        satstr = record[0]
        if record[1] != 0:
            datestr = date( int(record[1]/10000), int((record[1]%10000)/100), record[1]%100 )
        else:
            datestr = -9999
        if record[2] != 0:
            dateend = date( int(record[2]/10000), int((record[2]%10000)/100), record[2]%100 )
        else:
            dateend = -9999
        polygeom = ogr.CreateGeometryFromWkt(record[3])
        polygeom.Transform( to_target )
        tmprec = [ satstr, datestr, dateend, polygeom.Clone() ]
        iceshelf.append( tmprec )
    nshelf = len(iceshelf)
    # print nshelf

    # Read icebergs polygon data
    sqltxt = "SELECT name,satellite,date,time,ST_AsText( polygon ) FROM icebergs"
    sqltxt = ("%s WHERE datetime = \'%s\' AND date != 0 ORDER BY id;" % (sqltxt,icedt.strftime('%Y-%m-%d %H:%M:%S')))
    print sqltxt
    results = (dbcon.query( sqltxt )).getresult()
    icebergs = []
    for record in results:
        # print record[0:4]
        namestr = record[0]
        satstr = record[1]
        bergdt = datetime( int(record[2]/10000), int((record[2]%10000)/100), record[2]%100, \
            int(record[3]/10000), int((record[3]%10000)/100), record[3]%100 )
        polygeom = ogr.CreateGeometryFromWkt(record[4])
        polygeom.Transform( to_target )
        tmprec = [ namestr, satstr, bergdt, polygeom.Clone() ]
        icebergs.append( tmprec )
    nbergs = len(icebergs)
    # print nbergs

    # Read icechart polygon data
    fieldlist = get_si3poly_defn()
    nfields = len(fieldlist)
    sqltxt = ("SELECT %s" % fieldlist[0][0])
    for i in range(1,nfields):
        sqltxt = ("%s,%s" % (sqltxt,fieldlist[i][0]))
    sqltxt = ("%s,ST_AsText(polygon),id" % sqltxt)
    sqltxt = ("%s FROM icecharts WHERE datetime = \'%s\' AND ST_Y(ST_Centroid(polygon)) < 0.0" \
        % (sqltxt,icedt.strftime('%Y-%m-%d %H:%M:%S')))
    sqltxt = ("%s AND service = \'NO\' ORDER BY id;" \
        % sqltxt)
    print sqltxt
    results = (dbcon.query( sqltxt )).getresult()
    icepoly = []
    idx = 0
    for record in results:
        fielddata = list(record[0:nfields])
        polygeom = ogr.CreateGeometryFromWkt(record[nfields])
        polygeom.Transform( to_target )
        # print polygeom.ExportToWkt()
        # print fielddata
        idval = record[nfields+1]
        # print idval
        # print idx, polygeom.GetGeometryName(), polygeom.Area()
        # if idval == 23679:
        #     print 'VCDI'
        #     print polygeom.ExportToWkt()
        #     sys.exit()

        # Clip polygons by ice shelves and icebergs
        polyflg = 0

        # 1. Ice shelves
        for i in range(nshelf):
            # print polygeom.Within( iceshelf[i][3] ), \
            #     iceshelf[0][3].Contains(polygeom), \
            #     polygeom.Intersects( iceshelf[i][3] )
            if polygeom.Within( iceshelf[i][3] ):
                polyflg = 1
                # print polygeom.ExportToWkt()
                # print fielddata
                # print idval
            elif polygeom.Intersects( iceshelf[i][3] ):
                tmppolygeom = polygeom.Clone()
                polygeom = tmppolygeom.Difference( iceshelf[i][3] )
                # print '  ', tmppolygeom.Area()/(1000.0*1000.0), polygeom.Area()/(1000.0*1000.0)
        # print idx, polygeom.GetGeometryName(), polygeom.Area()

        # 2. Icebergs
        for i in range(nbergs):
            # print polygeom.Within( icebergs[i][3] ), \
            #     icebergs[0][3].Contains(polygeom), \
            #     polygeom.Intersects( icebergs[i][3] )
            if polygeom.Within( icebergs[i][3] ):
                polyflg = 1
                # print polygeom.ExportToWkt()
                # print fielddata
                # print idval
            elif polygeom.Intersects( icebergs[i][3] ):
                tmppolygeom = polygeom.Clone()
                polygeom = tmppolygeom.Difference( icebergs[i][3] )
                # print '  ', tmppolygeom.Area()/(1000.0*1000.0), polygeom.Area()/(1000.0*1000.0)
        # print idx, polygeom.GetGeometryName(), polygeom.Area()

        # Check area is valid
        if polygeom.Area() < 1.0:
            polyflg = 2
        if polyflg == 0:
            polytype = polygeom.GetGeometryName()
            if polytype == 'POLYGON':
                fielddata[2] = polygeom.Area() / (1000.0*1000.0)
                fielddata[3] = polygeom.Boundary().Length() / 1000.0
                icepoly.append( [ fielddata, polygeom.Clone() ] )
            elif polytype == 'MULTIPOLYGON':
                nbits = polygeom.GetGeometryCount()
                # print nbits, fielddata[1]
                for i in range(nbits):
                    bitgeom = polygeom.GetGeometryRef(i)
                    if bitgeom.Area() >= 1.0:
                        # print bitgeom.GetGeometryName()
                        fielddata[2] = bitgeom.Area() / (1000.0*1000.0)
                        fielddata[3] = bitgeom.Boundary().Length() / 1000.0
                        icepoly.append( [ fielddata, bitgeom.Clone() ] )
            else:
                print ("antarctic_sigrid33: Unhandled geometry type \'%s\'" % polytype)
        idx = idx + 1

    npoly = len(icepoly)
    # print 'Ice chart polygons', len(results), npoly

    # Read NIC icebergs data
    #   Removes those outside area and already covered by icebergs
    sqltxt = ("SELECT DISTINCT iceberg FROM nicbergs")
    sqltxt = ("%s WHERE lastupdate > timestamp \'%s\' - interval \'14 days\' ORDER by iceberg;" \
        % (sqltxt,icedt))
    # print sqltxt
    niclist = dbcon.query( sqltxt ).getresult()
    nicberg_list = []
    for record in niclist:
        nicberg_id = record[0]
        # See if iceberg is already in polygons
        nicproc = 1
        for i in range(nbergs):
            if nicberg_id == icebergs[i][0]:
                nicproc = 0
        # If not present, get latest position and add to list
        if nicproc == 1:
            # Get latest position
            sqltxt = ("SELECT ST_AsText(location),lastupdate FROM nicbergs")
            sqltxt = ("%s WHERE iceberg = \'%s\' ORDER BY lastupdate DESC" % (sqltxt,nicberg_id))
            # print sqltxt
            nicwkt = dbcon.query( sqltxt ).getresult()[0][0]
            lastupdate = dbcon.query( sqltxt ).getresult()[0][1]
            # print lastupdate
            nicpoint = ogr.CreateGeometryFromWkt( nicwkt )
            nicpoint.Transform( to_target )
            # print nicpoint.ExportToWkt()
            # Add to list if within map coverage
            if coverage.Contains(nicpoint):
                nicberg_list.append( [ nicberg_id, nicpoint.Clone(), lastupdate ] )
    # print nicberg_list

    # Open Shapefile driver
    shpdrv = ogr.GetDriverByName('ESRI Shapefile')

    # Local directory for  outputs
    localdir = ("%s/%4d/%4d%02d%02d" % (OUTDIR,iceyr,iceyr,icemh,icedy))
    testoutdir(localdir)

    # Write SIGRID-3.3 Polygons shapefile
    shpfn = ("%s/nis_antarc_%4d%02d%02d_pl_a.shp" % (localdir,iceyr,icemh,icedy))
    if os.path.exists(shpfn):
        shpdrv.DeleteDataSource(shpfn)
    outds = shpdrv.CreateDataSource(shpfn)
    outlay = outds.CreateLayer('polygons',geom_type=ogr.wkbPolygon,srs=trg_spatialReference)
    for field in fieldlist:
        fieldname = field[2]
        fieldtype = field[1]
        fieldwidth = field[3]
        if fieldname != '--':
            if fieldtype == 'int' or fieldtype == 'string':
                fieldDefn = ogr.FieldDefn( fieldname, ogr.OFTString )
                fieldDefn.SetWidth( fieldwidth )
                outlay.CreateField(fieldDefn)
            elif fieldtype == 'float':
                fieldDefn = ogr.FieldDefn( fieldname, ogr.OFTReal )
                fieldDefn.SetWidth( int(fieldwidth) )
                fieldDefn.SetPrecision( int(int(fieldwidth*100.0)%100) )
                outlay.CreateField(fieldDefn)
    # Add ICECODE field for Norway ice class and other data
    fieldDefn = ogr.FieldDefn( 'ICECODE', ogr.OFTString )
    fieldDefn.SetWidth( 120 )
    outlay.CreateField(fieldDefn)
    # Get feature definition
    featureDefn = outlay.GetLayerDefn()

    # Write ice chart polygon features to file
    for i in range(npoly):
        # Get feature data
        fielddata = icepoly[i][0]
        polygeom = icepoly[i][1]
        # Create a new feature
        feature = ogr.Feature(featureDefn)
        # Add attriubutes and data
        for j in range(nfields):
            fieldname = fieldlist[j][2]
            fieldtype = fieldlist[j][1]
            fieldwidth = fieldlist[j][3]
            fieldval = '--'
            # Normal fields
            if fieldname != '--':
                if fieldtype == 'int':
                    if fielddata[j] == -9:
                        fielddata[j] = 99
                    fieldval = ("%02d" % fielddata[j])
                elif fieldtype == 'string':
                    fieldval = fielddata[j]
                elif fieldtype == 'float':
                    fieldval = fielddata[j]
                # print j, fieldlist[j][0], fieldval
                feature.SetField( fieldname, fieldval )
            # ICECODE 'norway_iceclass' field
            if fieldlist[j][0] == 'norway_iceclass':
                fieldval = fielddata[j]
                # print j, fieldlist[j][0], fieldval
                feature.SetField( 'ICECODE', fieldval )
        # Add geometry
        feature.SetGeometry( polygeom )
        # Create feature on the layer
        outlay.CreateFeature(feature)

        # Destroy the feature
        feature.Destroy()

    # Write ice shelf polygon features to file
    for i in range(nshelf):
        # Get feature data
        satstr = iceshelf[i][0]
        datestr = iceshelf[i][1]
        dateend = iceshelf[i][2]
        polygeom = iceshelf[i][3]
        # Contruct SIGRID-3.3 field data
        fielddata = [ '--', 'Ice Shelf', \
            polygeom.Area() / (1000.0*1000.0), \
            polygeom.Boundary().Length() / 1000.0, \
            'S', \
            '92', '92', '99', '99', \
            '99', '98', '99', '99', '99', \
            '08', '99', '99', '99', '99', \
            '99', '99' ]
        # print len(fielddata)

        # Create a new feature
        feature = ogr.Feature(featureDefn)
        # Add attributes and data
        for j in range(nfields):
            fieldname = fieldlist[j][2]
            fieldtype = fieldlist[j][1]
            fieldwidth = fieldlist[j][3]
            fieldval = '--'
            # Normal fields
            if fieldname != '--':
                if fieldtype == 'int':
                    fieldval = fielddata[j]
                elif fieldtype == 'string':
                    fieldval = fielddata[j]
                elif fieldtype == 'float':
                    fieldval = fielddata[j]
                # print j, fieldlist[j][0], fieldval
                feature.SetField( fieldname, fieldval )
            # ICECODE 'norway_iceclass' field
            if fieldlist[j][0] == 'norway_iceclass':
                # print fielddata[j], satstr, datestr, dateend
                fieldval = ("%s, Source:%s, Start_Date:%s, End_Date:%s" \
                    % (fielddata[j],satstr,datestr,dateend))
                # print fieldval
                # print len(fieldval)
                # print j, fieldlist[j][0], fieldval
                feature.SetField( 'ICECODE', fieldval )

        # Add geometry
        feature.SetGeometry( polygeom )
        # Create feature on the layer
        outlay.CreateFeature(feature)

        # Destroy the feature
        feature.Destroy()

    # Write icebergs polygon features to file
    for i in range(nbergs):
        # Get feature data  namestr, satstr, bergdt
        namestr = icebergs[i][0]
        satstr = icebergs[i][1]
        bergdt = icebergs[i][2]
        polygeom = icebergs[i][3]
        # Contruct SIGRID-3.3 field data
        fielddata = [ '--', 'Iceberg', \
            polygeom.Area() / (1000.0*1000.0), \
            polygeom.Boundary().Length() / 1000.0, \
            'S', \
            '92', '92', '99', '99', \
            '99', '98', '99', '99', '99', \
            '10', '99', '99', '99', '99', \
            '99', '99' ]
        # print len(fielddata)

        # Create a new feature
        feature = ogr.Feature(featureDefn)
        # Add attributes and data
        for j in range(nfields):
            fieldname = fieldlist[j][2]
            fieldtype = fieldlist[j][1]
            fieldwidth = fieldlist[j][3]
            fieldval = '--'
            # Normal fields
            if fieldname != '--':
                if fieldtype == 'int':
                    fieldval = fielddata[j]
                elif fieldtype == 'string':
                    fieldval = fielddata[j]
                elif fieldtype == 'float':
                    fieldval = fielddata[j]
                # print j, fieldlist[j][0], fieldval
                feature.SetField( fieldname, fieldval )
            # ICECODE 'norway_iceclass' field
            if fieldlist[j][0] == 'norway_iceclass':
                # print fielddata[j], satstr, datestr, dateend
                fieldval = ("%s, NIC_Code:%s, Source:%s, Date_Time:%s" \
                    % (fielddata[j],namestr,satstr,bergdt.strftime('%Y-%m-%d %H:%M:%S')))
                # print fieldval
                # print len(fieldval)
                # print j, fieldlist[j][0], fieldval
                feature.SetField( 'ICECODE', fieldval )

        # Add geometry
        feature.SetGeometry( polygeom )
        # Create feature on the layer
        outlay.CreateFeature(feature)

        # Destroy the feature
        feature.Destroy()

    # Close the output file
    outds.Destroy()

    # Write SIGRID-3.3 Points Shapefile
    shpfn = ("%s/nis_antarc_%4d%02d%02d_pt_a.shp" % (localdir,iceyr,icemh,icedy))
    if os.path.exists(shpfn):
        shpdrv.DeleteDataSource(shpfn)
    outds = shpdrv.CreateDataSource(shpfn)
    outlay = outds.CreateLayer('points',geom_type=ogr.wkbPoint,srs=trg_spatialReference)

    # Fields
    # Iceberg fields
    #   POINT_TYPE  str 3      'ICEBRG'
    #   ICESOD      str 2      '98'       Stage of dev
    #   ICEBSZ      str 2      '08'       Iceberg size
    #   ICEDDR      str 2      '99'       Iceberg drift direction
    #   IA_BFM      str 2      '02'       Iceberg form
    #   IA_BUH      int 2      -99        Max height of iceberg above waterline in metres
    #   IA_OBN      int 2      1          Number of ice objects
    #   ICEDSP      float 10   0.0        Iceberg drift spped in kts
    #   RECDAT      10-22      ''         Date and Time of Observation
    ptfields = [ [ 'POINT_TYPE', 'string', 6,  'ICEBRG' ], \
                 [ 'ICESOD',     'string', 2,  '98'     ], \
                 [ 'ICEBSZ',     'string', 2,  '08'     ], \
                 [ 'ICEDDR',     'string', 2,  '99'     ], \
                 [ 'IA_BFM',     'string', 2,  '02'     ], \
                 [ 'IA_BUH',     'int',    2,  -99      ], \
                 [ 'IA_OBN',     'int',    2,  1        ], \
                 [ 'ICEDSP',     'float',  10, -99.9    ], \
                 [ 'RECDAT',     'string', 10, ''       ], \
                 [ 'ICECODE',    'string', 80, ''       ] ]
    for field in ptfields:
        fieldname = field[0]
        fieldtype = field[1]
        fieldwidth = field[2]
        if fieldtype == 'string':
            fieldDefn = ogr.FieldDefn( fieldname, ogr.OFTString )
            fieldDefn.SetWidth( fieldwidth )
        elif fieldtype == 'int':
            fieldDefn = ogr.FieldDefn( fieldname, ogr.OFTInteger )
        elif fieldtype == 'float':
            fieldDefn = ogr.FieldDefn( fieldname, ogr.OFTReal )
        outlay.CreateField(fieldDefn)
    # Get feature definition
    featureDefn = outlay.GetLayerDefn()

    # Write icebergs polygon features to file
    for i in range(len(nicberg_list)):
        # Get feature data  namestr, satstr, bergdt
        namestr = nicberg_list[i][0]
        pointgeom = nicberg_list[i][1]
        lastupdate = nicberg_list[i][2]
        # Contruct SIGRID-3.3 field data
        fielddata = [ ptfields[0][3], \
            ptfields[1][3], \
            ptfields[2][3], \
            ptfields[3][3], \
            ptfields[4][3], \
            ptfields[5][3], \
            ptfields[6][3], \
            ptfields[7][3], \
            lastupdate, \
            namestr ]
        # print fielddata

        # Create a new feature
        feature = ogr.Feature(featureDefn)
        # Add attributes and data
        for j in range(len(ptfields)):
            fieldname = ptfields[j][0]
            fieldval = fielddata[j]
            feature.SetField( fieldname, fieldval )
        # Add geometry
        feature.SetGeometry( pointgeom )
        # Create feature on the layer
        outlay.CreateFeature(feature)

        # Destroy the feature
        feature.Destroy()

    # Close the output file
    outds.Destroy()

    # Write SIGRID-3.3 Metadata
    # minlon = limits[0]
    # maxlat = limits[1]
    # maxlon = limits[2]
    # inlat = limits[3]
    # minpsx = limits[4]
    # maxpsy = limits[5]
    # maxpsx = limits[6]
    # minpsy = limits[7]
    # xmlfn = si33_ANT_xml(iceyr,icemh,icedy,'antarctic','antarctic',limits,fieldname,fielddef):

    # Create zip-file package
    shptypes = ( 'pl', 'pt' )
    shpext = [ 'shp', 'shx', 'dbf', 'prj' ]
    zipfn = ("%s/nis_antarc_%4d%02d%02d_pl_a.zip" % (localdir,iceyr,icemh,icedy))
    si3zip = zipfile.ZipFile(zipfn,'w',zipfile.ZIP_DEFLATED)
    for shptype in shptypes:
        for ext in shpext:
            tmpfn = ("%s/nis_antarc_%4d%02d%02d_%s_a.%s" \
                % (localdir,iceyr,icemh,icedy,shptype,ext))
            arcfn = ("nis_antarc_%4d%02d%02d_%s_a.%s" \
                % (iceyr,icemh,icedy,shptype,ext))
            si3zip.write(tmpfn,arcfn)
    si3zip.close()

    # Send to AARI http://gmdss.aari.aq/antarc/ if required
    # Get listing of base directory
    dirlist = []
    aari_url = ("%s/%d/%02d" % (GMDSS_URL,iceyr,icemh))
    # print aari_url
    # NB: Need to create a directory here if it is a new month and does not exist!
    try:
        aari = urllib2.urlopen(aari_url).read().splitlines()
    except:
        print 'Problem opening connection to AARI.'
        uploadflg = 1
    else:
        uploadflg = 0
        for txtline in aari:
            bits = txtline.split('>')
            for i in range(len(bits)):
                # Get strings fitting the pattern '2???????_*'
                if bits[i][0:1] == '2' and bits[i][8:9] == '_':
                    dirstr = bits[i].split('/')[0]
                    # print dirstr
                    dirlist.append( dirstr )
        # print dirlist

    # Loop through directory names
    if uploadflg == 0:
        mkdir_flg = 1
        for dirstr in dirlist:
            # Extract date
            datestr = dirstr[0:8]
            year = int(datestr[0:4])
            month = int(datestr[4:6])
            day = int(datestr[6:8])
            uploaddt = datetime( year, month, day, 15, 0, 0 )
            # Extract provider
            provider = dirstr[9:]
            # print dirstr, uploaddt, provider
            if uploaddt == icedt and provider == 'nis':
                mkdir_flg = 0
        # print mkdir_flg

        # Open connection to AARI FTP
        username = 'aari_ftp_username'
        password = 'aari_ftp_password'
        # print username, password
        try:
            # aari_ftp = FTP(host='gmdss.aari.ru',user=username,passwd=password)
            aari_ftp = FTP(host='ice.aari.aq',user=username,passwd=password)
        except:
            print 'Problem with FTP connection to AARI'
        else:
            # Only continue if we have a valid connection
            status = aari_ftp.cwd(("antarc/antice/%d/%02d" % (iceyr,icemh)))
            # status = aari_ftp.cwd("data/antarc")

            # Make directory if required
            # print aari_ftp.pwd()
            remotedir = ("%4d%02d%02d_nis" % (iceyr,icemh,icedy))
            if mkdir_flg == 1:
                status = aari_ftp.mkd(remotedir)

            # 1. Shapefile data
            status = aari_ftp.cwd(remotedir)
            # Send the Shapefile file zipped
            print zipfn
            fin = open(zipfn,'rb')
            storname = ("nis_antarc_%4d%02d%02d_pl_a.zip" % (iceyr,icemh,icedy))
            cmd = ("STOR %s" % storname)
            # print cmd
            status = aari_ftp.storbinary(cmd,fin)
            fin.close()
            # Send the individual component files
            for shptype in shptypes:
                for ext in shpext:
                    tmpfn = ("%s/nis_antarc_%4d%02d%02d_%s_a.%s" \
                        % (localdir,iceyr,icemh,icedy,shptype,ext))
                    storfn = ("nis_antarc_%4d%02d%02d_%s_a.%s" \
                        % (iceyr,icemh,icedy,shptype,ext))
                    fin = open(tmpfn,'rb')
                    cmd = ("STOR %s" % storfn)
                    status = aari_ftp.storbinary(cmd,fin)
                    fin.close()

            # 2. SST data
            print localdir
            if sst_flg == 1:
                storfn = ("%s120000-UKMO-L4_GHRSST-SSTfnd-OSTIA-GLOB-v02.0-fv02.0.zip" \
                    % (yesterday.strftime('%Y%m%d')))
                fin = open(sstfn,'rb')
                cmd = ("STOR %s" % storfn)
                status = aari_ftp.storbinary(cmd,fin)
                fin.close()

            # 3. Outputs
            ic_types = [ '', '_old' ]
            ic_formats = [ 'png', 'jpg', 'pdf' ]
            for ic_area in ic_names['antarctic']:
                for ic_type in ic_types:
                    for ic_format in ic_formats:
                        icfn = ("%s/%s_%4d%02d%02d%s.%s" \
                            % (localdir,ic_area,iceyr,icemh,icedy,ic_type,ic_format))
                        print icfn
                        storfn = ("%s_%4d%02d%02d%s.%s" \
                            % (ic_area,iceyr,icemh,icedy,ic_type,ic_format))
                        # print storfn
                        fin = open(icfn,'rb')
                        cmd = ("STOR %s" % storfn)
                        status = aari_ftp.storbinary(cmd,fin)
                        fin.close()

            # Close FTP connection
            aari_ftp.quit()


# Close datebase connection
dbcon.close()

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** antarctic_sigrid33.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

