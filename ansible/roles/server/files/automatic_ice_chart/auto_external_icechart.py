#!/usr/bin/python

# Name:          auto_external_icechart.py
# Purpose:       Automatically generates PNG, JPG and PDF products for ice chart data coming 
#                from other ice services, for example NIC and AARI for the Antarctic.
# Usage:         No command line options = Use current date, do not overwrite and automatically 
#                determine regions
#                python auto_icechart [yyyymmdd] [force_flg]
#                  yyyymmdd = Date to upload
#                  force_flg = 1 to overwrite existing files on server
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import string
import time
import tarfile
from datetime import datetime, timedelta
from zipfile import ZipFile
import argparse

import numpy as N

import pg

from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo.gdalconst import *
from pyproj import Proj
import mapscript

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import re

from icechart_utils import thous, mkdate, getproj, icechart_areas
import icechart_layout
from reducejpeg import reducejpeg
from generate_pdf import generate_pdf

BASEDIR='/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Python/include" % BASEDIR)
ANCDIR=("%s/Python/Ancillary" % BASEDIR)
OUTDIR=("/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart/Outputs")
SSTDIR=("/disk1/Istjenesten/New_Production_System/Automatic_Ice_Chart/Outputs")

# Set the projection strings (Polar Stereographic)
PROJSTR = getproj()

# Set % threshold for RS2 coverage
RS2THRES = 0.0

# Set % threshold for Sentinel-1 coverage
S1THRES = 0.0

# New system flag
NEWSYS = 1

# Active flag - enables writing to database
active_flg = 0

# Output the start time
tmpt=time.gmtime()
tout=( '*** auto_icechart.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/auto_icechart.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Get today's date
today = time.localtime()
todaystr = time.strftime('%Y-%m-%d',today)
# print todaystr

# Get input parameters using argparse
parser = argparse.ArgumentParser()
parser.add_argument("-r","--region",help="Region for ice chart [arctic/antarctic]",type=str, \
    choices=['arctic','antarctic'],default=['arctic','antarctic'])
parser.add_argument("-d","--date",help="Date for ice chart [yyyymmdd]",type=mkdate,default=today)
parser.add_argument("-f","--force",help="Force processing [0/1]",type=int,default=0,choices=[0,1])
args = parser.parse_args()
if type(args.region) is list:
    regions = (args.region)
else:
    regions = [ args.region ]
iceyr = (args.date).tm_year
icemh = (args.date).tm_mon
icedy = (args.date).tm_mday
datestr = ("%4d%02d%02d" % (iceyr,icemh,icedy))
icedt = datetime( iceyr, icemh, icedy, 0, 0, 0 )
force_flg = args.force

# Dictionary to convert SIGRID3 CT and FA values to Norway Ice Class
# select distinct(si3ct) as ct from icecharts where service != 'NO' and ST_Y(ST_Centroid(polygon)) < -30.0 order by ct;
# select distinct(si3fa) as fa from icecharts where service != 'NO' and ST_Y(ST_Centroid(polygon)) < -30.0 order by fa;
si3ct_dict = { '-9': 'Ice Free', \
               ' 0': 'Ice Free', \
               ' 1': 'Open Water', \
               ' 2': 'Open Water', \
               '12': 'Very Open Drift Ice', \
               '13': 'Very Open Drift Ice', \
               '20': 'Very Open Drift Ice', \
               '23': 'Very Open Drift Ice', \
               '24': 'Very Open Drift Ice', \
               '30': 'Very Open Drift Ice', \
               '40': 'Open Drift Ice', \
               '45': 'Open Drift Ice', \
               '46': 'Open Drift Ice', \
               '50': 'Open Drift Ice', \
               '56': 'Open Drift Ice', \
               '57': 'Open Drift Ice', \
               '60': 'Open Drift Ice', \
               '68': 'Open Drift Ice', \
               '70': 'Close Drift Ice', \
               '78': 'Close Drift Ice', \
               '80': 'Close Drift Ice', \
               '81': 'Very Close Drift Ice', \
               '89': 'Close Drift Ice', \
               '90': 'Very Close Drift Ice', \
               '91': 'Very Close Drift Ice', \
               '92': 'Very Close Drift Ice' }
si3fa_dict = { '-9': 'Drift Ice', \
               ' 0': 'Ice Free', \
               ' 2': 'Drift Ice', \
               ' 3': 'Drift Ice', \
               ' 4': 'Drift Ice', \
               ' 5': 'Drift Ice', \
               ' 8': 'Fast Ice', \
               ' 9': 'Drift Ice', \
               '10': 'Drift Ice', \
               '99': 'Drift Ice' }

# Connect to database
dbcon = pg.connect(dbname='icecharts', host='istjenesten', user='istjenesten', passwd='svalbard')
# Connect to the database containing satellite processing information (Sentinel-1)
con2 = pg.connect(dbname='Satellite', host='istjenesten', user='istjenesten', passwd='svalbard')

# Main loop, through regions
for regionstr in regions:

    # Database checking on new system
    proc_flg = 0
    if NEWSYS == 1:
        
        # Check status in database
        sqltxt = "SELECT id,outputs_flg,service FROM external WHERE"
        sqltxt = ("%s icechart_date = \'%4d-%02d-%02d\'" % (sqltxt,iceyr,icemh,icedy))
        sqltxt = ("%s AND region = \'%s\';" % (sqltxt,regionstr))
        # print sqltxt
        queryres = dbcon.query( sqltxt )
        nrec = queryres.ntuples()
        records = queryres.getresult()
        # print records
        # print nrec
        if nrec == 0:
            print ("auto_icechart: No database record for \'%s\' this date." % regionstr)
        elif nrec >= 1:
            icechart_id = []
            outputs_flg = []
            servstr = []
            proc_flg = []
            for i in range(nrec):
                tmprec = records[i]
                tmpid = tmprec[0]
                tmpout = tmprec[1]
                tmpserv = tmprec[2]
                # print tmprec

                if tmpout == 1 and force_flg != 1:
                    print ("auto_icechart: %s \'%s\' ice chart products already created today." \
                        % (servdict[tmpserv].upper(),regionstr))
                    proc_flg.append( 0 )
                else:
                    proc_flg.append( 1)

                icechart_id.append( tmpid )
                outputs_flg.append( tmpout )
                servstr.append( tmpserv )

        # print icechart_id, outputs_flg, servstr, proc_flg

    # Loop through database records
    for chartno in range(nrec):
    
        # Only process if proc_flg = 1 or NEWSYS = 0
        if proc_flg[chartno] == 1 or NEWSYS == 0:
        
            # Set ice chart area data
            [ ic_names, ic_old_names, ic_ullr, ic_coast, ic_orient, ic_legend, ic_scalebar ] = icechart_areas()
            narea = len(ic_names[regionstr])
            
            # Set spatial references, and transform from longitude/latitude to Polar Stereographic
            srs_spatialReference = osr.SpatialReference()
            srs_spatialReference.ImportFromProj4('+proj=longlat +ellps=WGS84')
            trg_spatialReference = osr.SpatialReference()
            trg_spatialReference.ImportFromProj4( PROJSTR[regionstr] )
            to_target = osr.CoordinateTransformation(srs_spatialReference,trg_spatialReference)
            to_ll = osr.CoordinateTransformation(trg_spatialReference,srs_spatialReference)
           
            # Get list of ice chart polygons from database
            querystr = "SELECT AsText(polygon),si3ct,si3fa FROM icecharts WHERE"
            querystr = ("%s datetime = \'%s\'" %  (querystr,icedt) )
            if regionstr == 'arctic':
                querystr = ("%s AND ST_Y(ST_Centroid(polygon)) > 0.0 AND service = \'%s\';" \
                    %  (querystr,servstr[chartno]) )
            elif regionstr == 'antarctic':
                querystr = ("%s AND ST_Y(ST_Centroid(polygon)) < 0.0 AND service = \'%s\';" \
                    %  (querystr,servstr[chartno]) )
            # print querystr
            queryres = dbcon.query( querystr )
            iceres = queryres.getresult()
            npoly = len(iceres)
            # print npoly
        
            # If no polygons, abort
            if npoly == 0:
                print ("No \'%s\' polygons on this date." % regionstr)
                proc_flg = -1
    
        # Only process if proc_flg = 1 or NEWSYS = 0
        if proc_flg[chartno] == 1 or NEWSYS == 0:
    
            # If Antarctic, cross-compare latest NIC iceberg data with iceberg polygons
            if regionstr == 'antarctic':
                querystr = ("SELECT DISTINCT iceberg FROM nicbergs")
                querystr = ("%s WHERE lastupdate >= timestamp \'%s\' - interval \'14 days\' ORDER by iceberg;" \
                    % (querystr,icedt))
                queryres = dbcon.query( querystr )
                niclist = queryres.getresult()
                nicberg_list = []
                nicwkt_list = []
                # print icedt
                for record in niclist:
                    nicberg_id = record[0]
                    # See if iceberg is already in polygons
                    nicproc = 1
                    # If not present, get latest position and add to list
                    if nicproc == 1:
                        # Get latest position
                        querystr = ("SELECT ST_AsText(location),lastupdate FROM nicbergs")
                        querystr = ("%s WHERE iceberg = \'%s\' AND lastupdate >= timestamp \'%s\' - interval \'14 days\' AND lastupdate <= timestamp \'%s\' + interval \'1 day\' ORDER BY lastupdate DESC" \
                            % (querystr,nicberg_id,icedt,icedt))
                        # print querystr
                        # print nicberg_id
                        # print dbcon.query( querystr ).getresult()
                        nicwkt = dbcon.query( querystr ).getresult()[0][0]
                        # Add to lists
                        nicberg_list.append( nicberg_id )
                        nicwkt_list.append( nicwkt )
            # print nicberg_list
            # print nicwkt_list
            
            # Get SST contours, if available
            # searchstr1 = ("%4d%02d%02d" % (iceyr,icemh,icedy))
            if regionstr == 'arctic':
                searchstr1 = icedt.strftime('%Y%m%d')
                searchstr2 = 'METNO_OI-ARC'
                sstindir = ("%s/%4d/%s" % (SSTDIR,icedt.year,searchstr1))
            elif  regionstr == 'antarctic':
                yesterday = icedt - timedelta(days=1)
                searchstr1 = yesterday.strftime('%Y%m%d')
                searchstr2 = 'OSTIA-GLOB'
                sstindir = ("%s/%4d/%s" % (SSTDIR,yesterday.year,searchstr1))
            # sstindir = ("%s/%4d/%4d%02d%02d" % (SSTDIR,iceyr,iceyr,icemh,icedy))
            # print sstindir
            filelist = os.listdir(sstindir)
            sstflg = 0
            sstfn = ''
            sstlevels = [ 0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0 ]
            for fname in filelist:
                if fname.find(searchstr1) > -1 and fname.find(searchstr2) > -1:
                    sstfn = ("%s/%s" % (sstindir,fname))
                    sstflg = 1
            # Unpack zip-file
            if sstflg == 1:
                zipf = ZipFile( sstfn, 'r' )
                zipf.extractall( TMPDIR )
                zipf.close()
                bits = sstfn.split('/')
                sstrootfn = bits[-1][0:-4]
            # print sstflg
            # print sstfn
            
            # print len(iceres)
            
            # Define chart styles and loop through them
            chart_styles = [ 'colour' ]
            for chart_style in chart_styles:
            
                # Loop through ice chart areas
                for areano in range(narea):
                
                    # Define coverage polygon
                    cvrullr = (ic_ullr[regionstr])[areano]
                    # print cvrullr
                    npoint = 10
                    geomstr = ("POLYGON((%.2f %.2f," % (float(cvrullr[0]),float(cvrullr[1])))
                    for j in range(1,npoint):
                        xpos = cvrullr[0] + (j * ( (float(cvrullr[2]) - float(cvrullr[0])) / float(npoint) ) )
                        # print xpos
                        geomstr = ("%s %.2f %.2f," % (geomstr,xpos,float(cvrullr[1])))
                    for j in range(npoint):
                        ypos = cvrullr[1] - (j * ( (float(cvrullr[1]) - float(cvrullr[3])) / float(npoint) ) )
                        # print ypos
                        geomstr = ("%s %.2f %.2f," % (geomstr,float(cvrullr[2]),ypos))
                    for j in range(npoint):
                        xpos = cvrullr[2] - (j * ( (float(cvrullr[2]) - float(cvrullr[0])) / float(npoint) ) )
                        # print xpos
                        geomstr = ("%s %.2f %.2f," % (geomstr,xpos,float(cvrullr[3])))
                    for j in range(npoint):
                        ypos = cvrullr[3] + (j * ( (float(cvrullr[1]) - float(cvrullr[3])) / float(npoint) ) )
                        # print ypos
                        geomstr = ("%s %.2f %.2f," % (geomstr,float(cvrullr[0]),ypos))
                    geomstr = ("%s %.2f %.2f))" % (geomstr,float(cvrullr[0]),float(cvrullr[1])))        
                    coverage_tmp = ogr.CreateGeometryFromWkt( geomstr, trg_spatialReference )
                    coverage = coverage_tmp.Buffer(50000.0)
                    del coverage_tmp
                
                    # Set status flag
                    procstat = 0
                
                    # Set up lists for polygons and ice types
                    tmppolylist = []
                    typelist = []
                    
                    for i in range(npoly):
                    
                        # Create an OGR geometry
                        # print dir(ogr)
                        geometry = ogr.CreateGeometryFromWkt( iceres[i][0], srs_spatialReference )
                        # Error check removed to pass some NIC and AARI polygons
                        # if geometry != None and geometry.IsValid():
                        if geometry != None:
                            geometry.Transform( to_target )
                            # print geometry
                  
                            si3ctstr = ("%2d" % iceres[i][1])
                            si3fastr = ("%2d" % iceres[i][2])
                            # print ("%s %s " % (ice_type,ice_form))

                            # Need to generate a Norway ice type from SIGRID3 CT value.
                            if si3fastr == ' 8':
                                ice_type = 'Fast Ice'
                            else:
                                ice_type = si3ct_dict[si3ctstr]
                            # print si3ctstr, si3fastr, ice_type
                    
                            # Select and clip those polygons within or intersecting coverage
                            polyarea = geometry.Area()
                            # print polyarea

                            if polyarea > 0:
                                if coverage.Contains( geometry ) == True:
                                    # print 'Contains'
                                    tmppolylist.append( geometry.ExportToWkt() )
                                    typelist.append( ice_type )
                                    # print geometry.GetGeometryName(), ice_type
                                elif coverage.Intersects( geometry ) == True:
                                    # print 'Intersects'
                                    newgeom = coverage.Intersection( geometry )
                                    if newgeom != None and newgeom.IsValid():
                                        if newgeom.GetGeometryName() == 'POLYGON':
                                            tmppolylist.append( newgeom.ExportToWkt() )
                                            typelist.append( ice_type )
                                            # print newgeom.GetGeometryName(), ice_type
                                        elif newgeom.GetGeometryName() == 'MULTIPOLYGON':
                                            nparts = newgeom.GetGeometryCount() 
                                            for j in range(nparts):
                                                subgeom = newgeom.GetGeometryRef(j)
                                                tmppolylist.append( subgeom.ExportToWkt() )
                                                typelist.append( ice_type )
                                                # print subgeom.GetGeometryName(), ice_type

                                else:
                                    # print 'Polygon outside of satellite coverage.'
                                    pass
                            else:
                                print ("   Warning: Polygon %d has invalid area, ignoring." % i)
                                sys.exit()
                                pass
                    
                    filtnpoly = len(tmppolylist)
                    #print npoly, filtnpoly
                    
                    print "Chart style: ", chart_style
                    
                    # Print number of features
                    print ("   Found %d polygons within %s map area." % (filtnpoly,(ic_names[regionstr])[areano]))
                    # print typelist
                    
                    # Filter list of NIC icebergs to only get those in coverage
                    if regionstr == 'antarctic':
                        nicberg_final = []
                        nicwkt_final = []
                        for nicberg,nicwkt in zip(nicberg_list,nicwkt_list):
                            pointgeom = ogr.CreateGeometryFromWkt(nicwkt)
                            pointgeom.Transform( to_target )
                            if coverage.Contains(pointgeom) == True:
                                nicberg_final.append( nicberg)
                                nicwkt_final.append( pointgeom.ExportToWkt() )
                        # print nicberg_final
                        # print nicwkt_final
                        print ("   %d NIC iceberg locations to be added." % (len(nicberg_final)))
    
                    # Polygon list is a list-of-lists for Anarctic
                    if regionstr == 'arctic':
                        polylist = tmppolylist
                    elif regionstr == 'antarctic':
                        polylist = [ tmppolylist, nicwkt_final, nicberg_final ]
            
                    # Create colour ice chart graphic
                    imginfo = [ (ic_names[regionstr])[areano], icedy, icemh, iceyr, coverage, \
                        (ic_coast[regionstr])[areano] ]
                    pngfname = icechart_layout.layout_external_png( regionstr, imginfo, 'a4', (ic_orient[regionstr])[areano], 300.0, \
                        (ic_legend[regionstr])[areano], (ic_scalebar[regionstr])[areano], polylist, typelist, \
                        sstflg, sstfn, sstlevels, cvrullr, chart_style, servstr[chartno] )
                    # print pngfname
    
                    # Convert to reduced size JPEG
                    jpegfname =  ("%s.jpg" % pngfname[:-4] )
                    # print jpegfname
                    targetsize = 100.0
                    # reducejpeg( pngfname, jpegfname, targetsize )
            
                    # Generate GeoTIFF and NetCDF
                    # imgres = 200.0
                    # [ gtfname, ncfname ] = icechart_netcdf( imginfo, \
                    #     imgres, polylist, typelist )
            
                    # Convert to PDF
                    pdffname =  ("%s.pdf" % pngfname[:-4] )
                    # generate_pdf( pdffname, (ic_orient[regionstr])[areano], 300.0, pngfname )
                    # sys.exit()
    
            
            # print ("   Status = %d\n" % procstat)
            
            # Delete temporary Shapefile components
            if sstflg == 1:
                filelist = os.listdir( TMPDIR )
                for fname in filelist:
                    if fname.find( sstrootfn ) > -1:
                        os.remove( ("%s/%s" % (TMPDIR,fname)) )
            
            # On new system, enter database status
            if NEWSYS == 1:
                # Database status update
                timenow = datetime.now()
                dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
                sqltxt = ("UPDATE external SET (outputs_flg,outputs_dt) = (1,\'%s\') WHERE id = %d;"
                    % (dtstr,icechart_id[chartno]))
                print sqltxt
                if active_flg == 1:
                    dbcon.query( sqltxt )

            # sys.exit()
    
# Close datebase connection
dbcon.close()

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** auto_icechart.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

