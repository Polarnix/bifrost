#!/usr/bin/python

# Name:          auto_icechart.py
# Purpose:       Automatically generates PNG, JPG and PDF products.
# Usage:         No command line options = Use current date, do not overwrite and automatically determine regions
#                python auto_icechart [yyyymmdd] [force_flg]
#                  yyyymmdd = Date to upload
#                  force_flg = 1 to overwrite existing files on server
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# export PYTHONPATH=/disk1/Istjenesten/New_Production_System/Mapscript-6.0.3/lib/python2.7/site-packages

import os, sys
import string
import time
import tarfile
from datetime import datetime, timedelta
from zipfile import ZipFile
import argparse

import numpy as N

import pg

from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo.gdalconst import *
from pyproj import Proj
import mapscript

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import re

from icechart_utils import thous, mkdate, getproj, icechart_areas
import icechart_layout
from reducejpeg import reducejpeg
from generate_pdf import generate_pdf

BASEDIR='/home/bifrostadmin'
TMPDIR=("%s/tmp" % BASEDIR)
INCDIR=("%s/Include/Automatic_Icechart" % BASEDIR)
ANCDIR=("%s/Ancillary/Automatic_Icechart" % BASEDIR)
OUTDIR=("/home/bifrostadmin/Outputs")
SSTDIR=("/home/bifrostadmin/Outputs")

# Set the projection strings (Polar Stereographic)
PROJSTR = getproj()

# Set % threshold for RS2 coverage
RS2THRES = 0.0

# Set % threshold for Sentinel-1 coverage
S1THRES = 0.0

# New system flag
NEWSYS = 1

# Active flag - enables writing to database
active_flg = 1

# Output the start time
tmpt=time.gmtime()
tout=( '*** auto_icechart.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file
lockfn= ANCDIR + '/auto_icechart.lock'
try:
    fin = open(lockfn, 'r')
    fin.close()
    raise NameError, 'Lock Exists!'
except IOError:
    # No lock file exists so create one
    # fout = open(lockfn, 'w')
    # fout.write(tout)
    # fout.close()
    pass
except NameError:
    # If we get to here then there is a lock file in existance so we should exit
    print 'Lock exists!'
    sys.exit()

# Get today's date
today = time.localtime()
todaystr = time.strftime('%Y-%m-%d',today)
# print todaystr

# Get input parameters using argparse
parser = argparse.ArgumentParser()
parser.add_argument("-r","--region",help="Region for ice chart [arctic/antarctic]",type=str, \
    choices=['arctic','antarctic'],default=['arctic','antarctic'])
parser.add_argument("-d","--date",help="Date for ice chart [yyyymmdd]",type=mkdate,default=today)
parser.add_argument("-f","--force",help="Force processing [0/1]",type=int,default=0,choices=[0,1])
args = parser.parse_args()
if type(args.region) is list:
    regions = (args.region)
else:
    regions = [ args.region ]
iceyr = (args.date).tm_year
icemh = (args.date).tm_mon
icedy = (args.date).tm_mday
datestr = ("%4d%02d%02d" % (iceyr,icemh,icedy))
icedt = datetime( iceyr, icemh, icedy, 15, 0, 0 )
force_flg = args.force

# Connect to database
dbcon = pg.connect(dbname='Icecharts', host='localhost', user='bifrostadmin', passwd='bifrostadmin')
# Connect to the database containing satellite processing information (Sentinel-1)
con2 = pg.connect(dbname='Satellite', host='localhost', user='bifrostadmin', passwd='bifrostadmin')

# Main loop, through regions
for regionstr in regions:

    # Database checking on new system
    proc_flg = 0
    if NEWSYS == 1:
        
        # Check status in database
        sqltxt = "SELECT id,finish_flg,outputs_flg FROM production WHERE"
        sqltxt = ("%s icechart_date = \'%4d-%02d-%02d\'" % (sqltxt,iceyr,icemh,icedy))
        sqltxt = ("%s AND region = \'%s\';" % (sqltxt,regionstr))
        # print sqltxt
        queryres = dbcon.query( sqltxt )
        nrec = queryres.ntuples()
        if nrec == 0:
            print ("auto_icechart: No database record for \'%s\' this date." % regionstr)
        elif nrec == 1:
            icechart_id = queryres.getresult()[0][0]
            finish_flg = queryres.getresult()[0][1]
            outputs_flg = queryres.getresult()[0][2]

            if finish_flg <= 0:
                print ("auto_icechart: \'%s\' ice chart not finished on this date." % regionstr)
            else:
                if outputs_flg == 1 and force_flg != 1:
                    print ("auto_icechart: \'%s\' ice chart products already created today." % regionstr)
                    proc_flg = 0
                else:
                    proc_flg = 1
        else:
            print ("auto_icechart: Too many \'%s\' database records for this date." % regionstr)

    
    # Only process if proc_flg = 1 or NEWSYS = 0
    if proc_flg == 1 or NEWSYS == 0:
    
        # Set ice chart area data
        [ ic_names, ic_old_names, ic_ullr, ic_coast, ic_orient, ic_legend, ic_scalebar ] = icechart_areas()
        narea = len(ic_names[regionstr])
        
        # Set spatial references, and transform from longitude/latitude to Polar Stereographic
        srs_spatialReference = osr.SpatialReference()
        srs_spatialReference.ImportFromProj4('+proj=longlat +ellps=WGS84')
        trg_spatialReference = osr.SpatialReference()
        trg_spatialReference.ImportFromProj4( PROJSTR[regionstr] )
        to_target = osr.CoordinateTransformation(srs_spatialReference,trg_spatialReference)
        to_ll = osr.CoordinateTransformation(trg_spatialReference,srs_spatialReference)
       
        # Get list of ice chart polygons from database
        querystr = "SELECT ST_AsText(polygon),norway_iceclass FROM icechart_polygons WHERE"
        querystr = ("%s datetime = \'%s\'" %  (querystr,icedt) )
        if regionstr == 'arctic':
            querystr = ("%s AND ST_Y(ST_Centroid(polygon)) > 0.0;" %  querystr )
        elif regionstr == 'antarctic':
            querystr = ("%s AND ST_Y(ST_Centroid(polygon)) < 0.0;" %  querystr )
        # print querystr
        queryres = dbcon.query( querystr )
        iceres = queryres.getresult()
        npoly = len(iceres)
        # print npoly
    
        # If no polygons, abort
        if npoly == 0:
            print ("No \'%s\' polygons on this date." % regionstr)
            proc_flg = -1

    # Only process if proc_flg = 1 or NEWSYS = 0
    if proc_flg == 1 or NEWSYS == 0:

        # If Antarctic, get list of ice shelf polygons from database
        if regionstr == 'antarctic':
            querystr = ("SELECT ST_AsText(polygon) FROM iceshelf WHERE")
            querystr = ("%s datetime = \'%s\';" % (querystr,icedt))
            queryres = dbcon.query( querystr )
            # print queryres
            shelfres = queryres.getresult()
            for i in range(len(shelfres)):
                # print shelfres[i][0]
                if i == 0:
                    shelfgeom = ogr.CreateGeometryFromWkt( shelfres[i][0] )
                    shelfgeom.Transform( to_target )
                    shelfgeom = shelfgeom.Buffer( 50.0 )
                else:
                    tmppoly = ogr.CreateGeometryFromWkt( shelfres[i][0] )
                    tmppoly.Transform( to_target )
                    tmppoly = tmppoly.Buffer( 50.0 )
                    shelfgeom = shelfgeom.Union( tmppoly )
            shelfwkt = shelfgeom.ExportToWkt()
            # print shelfwkt
            shelfgeom.Destroy()

        # If Antarctic, get list of iceberg polygons from database
        if regionstr == 'antarctic':
            querystr = ("SELECT ST_AsText(polygon),name FROM icebergs WHERE")
            querystr = ("%s datetime = \'%s\';" % (querystr,icedt))
            queryres = dbcon.query( querystr )
            # print queryres
            bergres = queryres.getresult()
            bergwktlist = []
            bergnamelist = []
            for i in range(len(bergres)):
                berggeom = ogr.CreateGeometryFromWkt( bergres[i][0] )
                berggeom.Transform( to_target )
                bergwkt =berggeom.ExportToWkt()
                bergwktlist.append( bergwkt )
                bergnamelist.append( bergres[i][1].upper() )
                berggeom.Destroy()
        # print bergwktlist
        # print bergnamelist

        # If Antarctic, cross-compare latest NIC iceberg data with iceberg polygons
        if regionstr == 'antarctic':
            querystr = ("SELECT DISTINCT iceberg FROM nic_icebergs")
            querystr = ("%s WHERE lastupdate > timestamp \'%s\' - interval \'14 days\' ORDER by iceberg;" \
                % (querystr,icedt))
            queryres = dbcon.query( querystr )
            niclist = queryres.getresult()
            nicberg_list = []
            nicwkt_list = []
            for record in niclist:
                nicberg_id = record[0]
                # See if iceberg is already in polygons
                nicproc = 1
                for i in range(len(bergnamelist)):
                    if nicberg_id == bergnamelist[i]:
                        nicproc = 0
                # If not present, get latest position and add to list
                if nicproc == 1:
                    # Get latest position
                    querystr = ("SELECT ST_AsText(location),lastupdate FROM nic_icebergs")
                    querystr = ("%s WHERE iceberg = \'%s\' ORDER BY lastupdate DESC" % (querystr,nicberg_id))
                    # print querystr
                    nicwkt = dbcon.query( querystr ).getresult()[0][0]
                    # Add to lists
                    nicberg_list.append( nicberg_id )
                    nicwkt_list.append( nicwkt )
        # print nicberg_list
        # print nicwkt_list
        
        # Get list of RS2 polygons from database
        # querystr = ("SELECT ST_AsText(coverage),datetime FROM rs2_files WHERE")
        # if regionstr == 'arctic':
        #     querystr = ("%s procflg = 1 AND datetime < \'%s\' AND" % (querystr,icedt))
        #     querystr = ("%s datetime > timestamp \'%s\' - interval \'15 hours\' AND ST_Y(ST_Centroid(coverage)) > 0.0;" % (querystr,icedt))
        # elif regionstr == 'antarctic':
        #     querystr = ("%s procflg = -1 AND datetime < \'%s\' AND" % (querystr,icedt))
        #     querystr = ("%s datetime > timestamp \'%s\' - interval \'48 hours\' AND ST_Y(ST_Centroid(coverage)) < 0.0;" % (querystr,icedt))
        # print querystr
        # queryres = con2.query( querystr )
        # print queryres
        # rs2res = queryres.getresult()
        rs2res = []
        # print len(rs2res)

        # Get list of Sentinel-1 polygons from database
        querystr = ("SELECT ST_AsText(coverage),datetime FROM s1_files WHERE")
        if regionstr == 'arctic':
            querystr = ("%s procflg = 1 AND datetime < \'%s\' AND" % (querystr,icedt))
            querystr = ("%s datetime > timestamp\'%s\' - interval \'15 hours\' AND ST_Y(ST_Centroid(coverage)) > 0.0;" % (querystr,icedt))
        elif regionstr == 'antarctic':
            querystr = ("%s procflg = -1 AND datetime < \'%s\' AND" % (querystr,icedt))
            querystr = ("%s datetime > timestamp\'%s\' - interval \'48 hours\' AND ST_Y(ST_Centroid(coverage)) < 0.0;" % (querystr,icedt))
        queryres = con2.query( querystr )
        # print queryres
        S1res = queryres.getresult()
        # print len(S1res)
        
        # Get SST contours, if available
        # searchstr1 = ("%4d%02d%02d" % (iceyr,icemh,icedy))
        if regionstr == 'arctic':
            searchstr1 = icedt.strftime('%Y%m%d')
            searchstr2 = 'METNO_OI-ARC'
            sstindir = ("%s/%4d/%s" % (SSTDIR,icedt.year,searchstr1))
        elif  regionstr == 'antarctic':
            yesterday = icedt - timedelta(days=1)
            searchstr1 = yesterday.strftime('%Y%m%d')
            searchstr2 = 'OSTIA-GLOB'
            sstindir = ("%s/%4d/%s" % (SSTDIR,yesterday.year,searchstr1))
        # sstindir = ("%s/%4d/%4d%02d%02d" % (SSTDIR,iceyr,iceyr,icemh,icedy))
        # print sstindir
        sstflg = 0
        sstfn = ''
        sstlevels = [ 0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0 ]
        try:
            filelist = os.listdir(sstindir)
        except:
            print 'Unable to access SST data.'
        else:
            for fname in filelist:
                if fname.find(searchstr1) > -1 and fname.find(searchstr2) > -1:
                    sstfn = ("%s/%s" % (sstindir,fname))
                    sstflg = 1
        # Unpack zip-file
        if sstflg == 1:
            zipf = ZipFile( sstfn, 'r' )
            zipf.extractall( TMPDIR )
            zipf.close()
            bits = sstfn.split('/')
            sstrootfn = bits[-1][0:-4]
        # print sstflg
        # print sstrootfn
        
        # print len(iceres), len(rs2res)
        
        # Define chart styles and loop through them
        chart_styles = [ 'colour', 'hatched' ]
        for chart_style in chart_styles:
        
            # Loop through ice chart areas
            for areano in range(narea):
            
                # Define coverage polygon
                cvrullr = (ic_ullr[regionstr])[areano]
                # print cvrullr
                npoint = 10
                geomstr = ("POLYGON((%.2f %.2f," % (float(cvrullr[0]),float(cvrullr[1])))
                for j in range(1,npoint):
                    xpos = cvrullr[0] + (j * ( (float(cvrullr[2]) - float(cvrullr[0])) / float(npoint) ) )
                    # print xpos
                    geomstr = ("%s %.2f %.2f," % (geomstr,xpos,float(cvrullr[1])))
                for j in range(npoint):
                    ypos = cvrullr[1] - (j * ( (float(cvrullr[1]) - float(cvrullr[3])) / float(npoint) ) )
                    # print ypos
                    geomstr = ("%s %.2f %.2f," % (geomstr,float(cvrullr[2]),ypos))
                for j in range(npoint):
                    xpos = cvrullr[2] - (j * ( (float(cvrullr[2]) - float(cvrullr[0])) / float(npoint) ) )
                    # print xpos
                    geomstr = ("%s %.2f %.2f," % (geomstr,xpos,float(cvrullr[3])))
                for j in range(npoint):
                    ypos = cvrullr[3] + (j * ( (float(cvrullr[1]) - float(cvrullr[3])) / float(npoint) ) )
                    # print ypos
                    geomstr = ("%s %.2f %.2f," % (geomstr,float(cvrullr[0]),ypos))
                geomstr = ("%s %.2f %.2f))" % (geomstr,float(cvrullr[0]),float(cvrullr[1])))        
                coverage_tmp = ogr.CreateGeometryFromWkt( geomstr, trg_spatialReference )
                coverage = coverage_tmp.Buffer(50000.0)
                del coverage_tmp
            
                # Set status flag
                procstat = 0
            
                # Set up lists for polygons and ice types
                tmppolylist = []
                typelist = []
                
                for i in range(npoly):
                
                    # Create an OGR geometry
                    # print dir(ogr)
                    geometry = ogr.CreateGeometryFromWkt( iceres[i][0], srs_spatialReference )
                    if geometry != None and geometry.IsValid():
                        geometry.Transform( to_target )
                        # print geometry
              
                        ice_type = iceres[i][1]
                        # print ice_type
                
                        # Select and clip those polygons within or intersecting coverage
                        polyarea = geometry.Area()
                        # print polyarea
                
                        if polyarea > 0:
                            if coverage.Contains( geometry ) == True:
                                # print 'Contains'
                                tmppolylist.append( geometry.ExportToWkt() )
                                typelist.append( ice_type )
                            elif coverage.Intersects( geometry ) == True:
                                # print 'Intersects'
                                newgeom = coverage.Intersection( geometry )
                                if newgeom != None and newgeom.IsValid():
                                    tmppolylist.append( newgeom.ExportToWkt() )
                                    typelist.append( ice_type )
                            else:
                                # print 'Polygon outside of satellite coverage.'
                                pass
                        else:
                            # print ("   Warning: Polygon %d has invalid area, ignoring." % i)
                            pass
                
                filtnpoly = len(tmppolylist)
                #print npoly, filtnpoly
                
                print "Chart style: ", chart_style
                
                # Print number of features
                print ("   Found %d polygons within %s map area." % (filtnpoly,(ic_names[regionstr])[areano]))
                # print typelist
                
                # Create empty list of SAR scene geometries and date/times
                sar_poly = []
                sar_dt = []

                # Filter list of RS2 scenes to get those with more than 10% overlap with map area
                numrs2 = len(rs2res)
                # print numrs2
                if len(rs2res) > 0:
        
                    for i in range(numrs2):
        
                        # Create an OGR geometry
                        # print dir(ogr)
                        geometry = ogr.CreateGeometryFromWkt( rs2res[i][0], srs_spatialReference )
                        if geometry != None and geometry.IsValid():
                            geometry.Transform( to_target )
                            # print geometry
        
                            # Select and clip those polygons within or intersecting coverage
                            polyarea = geometry.Area()
                            # print polyarea
        
                            if polyarea > 0:
                                if coverage.Contains( geometry ) == True:
                                    # print 'Contains'
                                    sar_poly.append( geometry.ExportToWkt() )
                                    sar_dt.append( rs2res[i][1] )
                                elif coverage.Intersects( geometry ) == True:
                                    newgeom = coverage.Intersection( geometry )
                                    intersect = (100.0 / polyarea) * newgeom.Area()
                                    # print ("Intersects %5.1f" % intersect)
                                    # print newgeom
                                    if newgeom != None and newgeom.IsValid() and intersect > RS2THRES:
                                        # sar_poly.append( newgeom.ExportToWkt() )
                                        sar_poly.append( geometry.ExportToWkt() )
                                        sar_dt.append( rs2res[i][1] )
                                else:
                                    # print 'Polygon outside of satellite coverage.'
                                    pass
                            else:
                                # print ("   Warning: Polygon %d has invalid area, ignoring." % i)
                                pass
        
                # Filter list of Sentinel-1 scenes to get those with more than 10% overlap with map area
                numS1 = len(S1res)
                # print numS1
                if len(S1res) > 0:
    
                    for i in range(numS1):
    
                        # Create an OGR geometry
                        # print dir(ogr)
                        geometry = ogr.CreateGeometryFromWkt( S1res[i][0], srs_spatialReference )
                        if geometry != None and geometry.IsValid():
                            geometry.Transform( to_target )
                            geometry = geometry.Buffer( 100.0 )
                            # print geometry
    
                            # Select and clip those polygons within or intersecting coverage
                            polyarea = geometry.Area()
                            # print polyarea
    
                            if polyarea > 0:
                                if coverage.Contains( geometry ) == True:
                                    # print 'Contains'
                                    sar_poly.append( geometry.ExportToWkt() )
                                    sar_dt.append( S1res[i][1] )
                                elif coverage.Intersects( geometry ) == True:
                                    newgeom = coverage.Intersection( geometry )
                                    intersect = (100.0 / polyarea) * newgeom.Area()
                                    # print ("Intersects %5.1f" % intersect)
                                    # print newgeom
                                    if newgeom != None and newgeom.IsValid() and intersect > S1THRES:
                                        # sar_poly.append( newgeom.ExportToWkt() )
                                        sar_poly.append( geometry.ExportToWkt() )
                                        sar_dt.append( S1res[i][1] )
                                else:
                                    # print 'Polygon outside of satellite coverage.'
                                    pass
                            else:
                                # print ("   Warning: Polygon %d has invalid area, ignoring." % i)
                                pass
    
                num_sar = len(sar_poly)
                print ("   %d SAR (RS2+S1) scenes have greater than %d%% coverage." % (num_sar,int(RS2THRES)))

                # Filter list of NIC icebergs to only get those in coverage
                if regionstr == 'antarctic':
                    nicberg_final = []
                    nicwkt_final = []
                    for nicberg,nicwkt in zip(nicberg_list,nicwkt_list):
                        pointgeom = ogr.CreateGeometryFromWkt(nicwkt)
                        pointgeom.Transform( to_target )
                        if coverage.Contains(pointgeom) == True:
                            nicberg_final.append( nicberg)
                            nicwkt_final.append( pointgeom.ExportToWkt() )
                    # print nicberg_final
                    # print nicwkt_final
                    print ("   %d NIC iceberg locations to be added." % (len(nicberg_final)))

                # Polygon list is a list-of-lists for Anarctic
                if regionstr == 'arctic':
                    polylist = tmppolylist
                elif regionstr == 'antarctic':
                    polylist = [ tmppolylist, shelfwkt, bergwktlist, bergnamelist, nicwkt_final, nicberg_final ]
        
                # Create colour ice chart graphic
                imginfo = [ (ic_names[regionstr])[areano], icedy, icemh, iceyr, coverage, \
                    (ic_coast[regionstr])[areano] ]
                pngfname = icechart_layout.layout_png( regionstr, imginfo, 'a4', (ic_orient[regionstr])[areano], 300.0, \
                    (ic_legend[regionstr])[areano], (ic_scalebar[regionstr])[areano], polylist, typelist, sar_poly, sar_dt, \
                    sstflg, sstfn, sstlevels, cvrullr, chart_style )
                # print pngfname

                # Convert to reduced size JPEG
                jpegfname =  ("%s.jpg" % pngfname[:-4] )
                # print jpegfname
                targetsize = 100.0
                reducejpeg( pngfname, jpegfname, targetsize )
        
                # Generate GeoTIFF and NetCDF
                # imgres = 200.0
                # [ gtfname, ncfname ] = icechart_netcdf( imginfo, \
                #     imgres, polylist, typelist )
        
                # Convert to PDF
                pdffname =  ("%s.pdf" % pngfname[:-4] )
                generate_pdf( pdffname, (ic_orient[regionstr])[areano], 300.0, pngfname )

        
        # print ("   Status = %d\n" % procstat)
        
        # Delete temporary Shapefile components
        if sstflg == 1:
            filelist = os.listdir( TMPDIR )
            for fname in filelist:
                if fname.find( sstrootfn ) > -1:
                    os.remove( ("%s/%s" % (TMPDIR,fname)) )
        
        # On new system, enter database status and close connection
        if NEWSYS == 1:
            # Database status update
            timenow = datetime.now()
            dtstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
            sqltxt = ("UPDATE production SET (outputs_flg,outputs_dt) = (1,\'%s\') WHERE id = %d;"
                % (dtstr,icechart_id))
            print sqltxt
            if active_flg == 1:
                dbcon.query( sqltxt )
    
# Close datebase connection
dbcon.close()

# Remove the lock file
# os.remove(lockfn)

# Output the end time
tmpt=time.gmtime()
tout=( '*** auto_icechart.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

