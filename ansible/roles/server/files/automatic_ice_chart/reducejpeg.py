#!/usr/bin/python

# Name:          reducejpeg.py
# Purpose:       Generate JPG-graphic to the specified target size in Kb.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys

from PIL import Image, ImageFile

def reducejpeg( infname, jpegfname, targetsize):

    ImageFile.MAXBLOCK = 2**30

    im = Image.open( infname )
    size = im.size
    # Reduce size of image
    fraction = 3
    width = size[0] / fraction
    height = size[1] / fraction
    im = im.resize( (width,height), Image.ANTIALIAS )

    # Loop through, reducing quality until we obtain target size
    quality = 95
    im.save( jpegfname, "JPEG", quality=quality, optimize=True, progressive=True )
    jpegsize = float( (os.stat( jpegfname )).st_size ) / 1024.0
    while jpegsize > targetsize and quality > 25:
        quality = quality - 5
        im.save( jpegfname, "JPEG", quality=quality, optimize=True, progressive=True )
        jpegsize = float( (os.stat( jpegfname )).st_size ) / 1024.0
    print ("JPEG quality = %d, File size = %6.1fKb" % (quality,jpegsize) )

