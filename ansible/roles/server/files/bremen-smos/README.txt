Directory for University of Hamburg SMOS processing scripts.

For data files, see :
Arctic    GeoTIFF - http://www.iup.uni-bremen.de:8084/smos/tif/20160426_hvnorth_rfi_l1c.tif
Arctic    NetCDF  - http://www.iup.uni-bremen.de:8084/smos/ncs/20160426_hvnorth_rfi_l1c.nc
Antarctic GeoTIFF - http://www.iup.uni-bremen.de:8084/smos/tif/20160426_hvsouth_rfi_l1c.tif
Antarctic NetCDF  - http://www.iup.uni-bremen.de:8084/smos/ncs/20160426_hvsouth_rfi_l1c.nc

https://icdc.zmaw.de/thredds/catalog/ftpthredds/smos_sea_ice_thickness/catalog.html
Data is available Antarctic, October to April and Antarctic, March to ~September.
