#!/usr/bin/python

# Name:          process_bremen_smos.py
# Purpose:       Routine to download University of Bremen (Georg Heygster) SMOS data files for Arctic
#                and Antarctic.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-13  - Fix datetime in mkdate 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, platform
import shutil
import subprocess
import time
from datetime import date, datetime, timedelta
import argparse
from urllib2 import urlopen, URLError, HTTPError

import pg


# Directory settings for Bifrost server
BASEDIR = '/home/bifrostsat/Data/SMOS'
DATADIR = ("%s/Raw" % BASEDIR)
ARCHIVEDIR = ("%s/Archive" % BASEDIR)
ANCDIR = '/home/bifrostsat/Ancillary/SMOS'
DROPBOX = "/home/bifrostsat/Dropbox"
TMPDIR = ("%s/tmp" % BASEDIR)
GDALHOME = '/usr'

# Map projections
ARCPROJ='+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84'
ANTPROJ='+proj=stere +lat_0=90s +lon_0=0e +lat_ts=90s +ellps=WGS84 +datum=WGS84'

# Month codes
short_mth = [ 'jan', 'feb', 'mar', 'apr', 'may', 'jun', \
              'jul', 'aug', 'sep', 'oct', 'nov', 'dec' ]

# Length of archive in days
BUFSIZE = 31


# Download SMOS from University of Bremen
def download_smos_data( region, dt ):

    # Region specific settings
    if region == 'arctic':
        prodcode = 'north'
        regname = 'Arctic'
    elif region == 'antarctic':
        prodcode = 'south'
        regname = 'Antarctic'
    else:
        print ("Unknown region \'%s\'." % region)
        sys.exit()

    # Construct filenames and URL
    # e.g. http://www.iup.uni-bremen.de:8084/smos/tif/20160426_hvnorth_rfi_l1c.tif
    rawname = ("%s_hv%s_rfi_l1c.tif" % (dt.strftime('%Y%m%d'),prodcode))
    url = ("http://www.iup.uni-bremen.de:8084/smos/tif/%s" \
        % rawname)
    localfn = ("%s/%s" % (DATADIR,rawname))

    # Try to download file
    print url
    try:
        fin = urlopen(url)
        # print "downloading " + url

        # Open our local file for writing
        with open(localfn, "wb") as local_file:
            local_file.write(fin.read())

        status = 1

    #handle errors
    except HTTPError, e:
        print "HTTP Error:", e.code, url
        status = -91
    except URLError, e:
        print "URL Error:", e.reason, url
        status = -92

    return [ status, localfn ]


# Reproject AMSR2 image
def reproject_smos( region, inpfn, dt ):

    # Construct output filename
    if region == 'arctic':
        outfn = ("%s/Arctic/GeoTIFF/%s_hvnorth_rfi_l1c_MET_ARC.tif" \
            % (BASEDIR,dt.strftime('%Y%m%d')))
        projstr = ARCPROJ
    elif region == 'antarctic':
        outfn = ("%s/Antarctic/GeoTIFF/%s_hvsouth_rfi_l1c_MET_ANT.tif" \
            % (BASEDIR,dt.strftime('%Y%m%d')))
        projstr = ANTPROJ
    else:
        print ("Unknown region \'%s\'." % region)
        sys.exit()

    # Call gdalwarp to reproject image
    cmd = ("%s/bin/gdalwarp -of GTiff -co \"COMPRESS=LZW\" -t_srs \"%s\"" \
        % (GDALHOME,projstr))
    cmd = ("%s -tr 3125 3125 -srcnodata 254 -dstnodata 254 -r near -overwrite -q %s %s"
        % (cmd,inpfn,outfn))
    # print cmd
    retcode = 0
    try:
        retcode = subprocess.call(cmd, shell=True)
        status = 1
        if retcode < 0:
            print >>sys.stderr, "Child was terminated by signal", -retcode
            status = -93
    except OSError, e:
        print >>sys.stderr, "Execution failed:", e
        status = -94

    return [ status, outfn ]


def mkdate(datestr):
    return datetime.strptime(datestr,'%Y%m%d')


if __name__ == '__main__':

    # Get today's date
    yesterday = datetime.now() - timedelta(days=1)
    yesterdaystr = yesterday.strftime('%Y-%m-%d')
    # print todaystr

    # Get user inputs using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument( "-r", "--region", help="Region for SMOS [arctic/antarctic/both]", \
        type=str.lower, choices=['arctic','antarctic','both'], default='both')
    parser.add_argument("-d","--date",help="Date for SMOS [yyyymmdd]",type=mkdate,default=yesterday)
    parser.add_argument("-o","--overwrite",help="Reset database and overwrite existing files.", \
        type=int, default=0, choices=[0,1])
    args = parser.parse_args()
    region = args.region
    year = (args.date).year
    month = (args.date).month
    day = (args.date).day
    datestr = ("%4d-%02d-%02d" % (year,month,day))
    dtstamp = datetime( year, month, day, 12, 0, 0 )
    over_flg = args.overwrite

    # Create a lock file for slow computers
    lockfn= ANCDIR + '/process_bremen_smos.lock'
    # Check for stale lock file
    if os.path.isfile(lockfn):
        stat = os.stat(lockfn)
        ctime = stat.st_mtime
        lock_dt = datetime.fromtimestamp(ctime)
        lock_age = (datetime.now() - lock_dt).total_seconds()
        if lock_age > 3600.0:
            os.remove(lockfn)

    # Now see if lock file still exists
    try:
        fin = open(lockfn, 'r')
        fin.close()
        raise NameError, 'Lock Exists!'
    except IOError:
        # No lock file exists so create one
        tout = ("process_bremen_smos.py %s" % datestr)
        fout = open(lockfn, 'w')
        fout.write(tout)
        fout.close()
        pass
    except NameError:
        # If we get to here then there is a lock file in existance so we should exit
        print 'Lock exists!'
        sys.exit()
    # print region

    # Connect to 'Satellite' database for updating the 'bremen_smos' table.
    con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')

    # See if files have already been processed
    newflg = 1
    sql = ("SELECT id,arc_flg,ant_flg FROM bremen_smos WHERE smosdate = \'%s\';" \
        % datestr)
    res = (con1.query(sql)).getresult()
    if len(res) > 0:
        smosid = int(res[0][0])
        arc_flg = int(res[0][1])
        ant_flg = int(res[0][2])
        newflg = 0
    else:
        arc_flg = 0
        ant_flg = 0

    # Get Arctic file and process
    arcstatus = 0
    if arc_flg == 0 or over_flg == 1:
        if region.find('arctic') > -1 or region.find('both') > -1:
            [ arcstatus, smosarcfn ] = download_smos_data( 'arctic', dtstamp )
            if arcstatus == 1:
                [ arcstatus, finalarcfn ] = reproject_smos( 'arctic', smosarcfn, dtstamp )
                # Move raw file to Archive
                [ path, rootfn ] = os.path.split( smosarcfn )
                shutil.move( smosarcfn, os.path.join(ARCHIVEDIR,rootfn) )  
            arc_flg = arcstatus

    # Get Antarctic file and process
    antstatus = 0
    if ant_flg == 0 or over_flg == 1:
        if region.find('antarctic') > -1 or region.find('both') > -1:
            [ antstatus, smosantfn ] = download_smos_data( 'antarctic', dtstamp )
            if antstatus == 1:
                [ antstatus, finalantfn ] = reproject_smos( 'antarctic', smosantfn, dtstamp ) 
                # Move raw file to Archive
                [ path, rootfn ] = os.path.split( smosantfn )
                shutil.move( smosantfn, os.path.join(ARCHIVEDIR,rootfn) )  
            ant_flg = antstatus

    # Current timestamp
    nowdt = datetime.now()
    nowstr = nowdt.strftime('%Y-%m-%d %H:%M:%S')

    # Construct query for database
    if newflg == 1:
        fieldstr = "id, smosdate, arc_flg, arc_dt, ant_flg, ant_dt"
        valstr = ("nextval(\'bremen_smos_serial\'), \'%s\', %d, \'%s\', %d, \'%s\'" \
            % (datestr,arcstatus,nowstr,antstatus,nowstr))
        sql = ("INSERT INTO bremen_smos (%s) VALUES (%s);" \
            % (fieldstr,valstr))
        print sql
        res = con1.query(sql)

    elif newflg == 0:
        update_count = 0
        sql = "UPDATE bremen_smos SET"
        divider = ""
        if arc_flg == 0 or over_flg == 1:
            if region.find('arctic') > -1 or region.find('both') > -1:
                sql = ("%s arc_flg = %d, arc_dt = \'%s\'" \
                    % (sql,arc_flg,nowstr))
                divider = ","
                update_count = update_count + 1
        if ant_flg == 0 or over_flg == 1:
            if region.find('antarctic') > -1 or region.find('both') > -1:
                sql = ("%s%s ant_flg = %d, ant_dt = \'%s\'" \
                        % (sql,divider,ant_flg,nowstr))
                update_count = update_count + 1
        sql = ("%s WHERE id = %d;" % (sql,smosid))

        if update_count > 0:        
            print sql
            res = con1.query(sql)
        else:
            # print 'No updates.'
            pass

    # Clean up files older than BUFSIZE days
    check_dirs = [ ("%s/Arctic/GeoTIFF" % BASEDIR), \
                   ("%s/Antarctic/GeoTIFF" % BASEDIR), \
                   DATADIR, ARCHIVEDIR ]
    target_dt = datetime.now() - timedelta(days=BUFSIZE)
    # print target_dt
    os.stat_float_times(True)
    for check_dir in check_dirs:
        fnlist = os.listdir(check_dir)
        for fname in fnlist:
            fullfn = os.path.join( check_dir, fname )
            stat = os.stat(fullfn)
            ctime = stat.st_mtime
            create_dt = datetime.fromtimestamp(ctime)
            if create_dt < target_dt:
                os.remove( fullfn )

    # Delete the lock file
    os.remove(lockfn)

