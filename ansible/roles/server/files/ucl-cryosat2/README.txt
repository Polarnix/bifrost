Directory for UCL Cryosat processing scripts.

For data files, see :
NetCDF  - http://www.cpom.ucl.ac.uk/csopr/sidata/thk_2.map.22042016_23042016.nc.gz
GeoTIFF - http://www.cpom.ucl.ac.uk/csopr/sidata/thk_2.map.22042016_23042016.tif

Arctic only, 5 km grid.  2, 14 and 28 day composites.

Documentation at http://www.cpom.ucl.ac.uk/csopr/docs/CPOM_CS2_Operational_Formats_v1.3.pdf
