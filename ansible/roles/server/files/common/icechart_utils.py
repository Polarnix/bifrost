#!/usr/bin/python

# Name:          icechart_utils.py
# Purpose:       Utilities for ice charting software.
#                  thous                - Generate a number string with thousands separated by comma (X,XXX,XXX)
#                  mkdate               - Date reformatting.
#                  getproj              - Provides a dictionary of map projections.
#                  icechart_areas       - Provides dictionaries of ice chart areas.
#                  icechart_areas_wkend - Provides dictionaries of ice chart areas at weekends
#                  get_aari_dict        - Provides a dictionary between AARI Shapefile parameters and NIS database parameters.
#                  get_nic_dict         - Provides a dictionary between NIC Shapefile parameters and NIS database parameters.
#                  get_nicberg_dict     - Provides a dictionary between NIC Iceberg Shapefile parameters and NIS database parameters.
#                  get_si3poly_defn     - Provides a list of database fields, field types and widths for use in SIGRID-3.3 polygon output.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import re
import time

from get_map_projections import get_map_projections

# Setup administration directories
BASEDIR = "/home/bifrostadmin"
INCDIR = ("%s/Include/Bifrost" % BASEDIR)
SETDIR = ("%s/Settings/Bifrost" % BASEDIR)


# Generate a number string with thousands separated by comma (X,XXX,XXX)
def thous(x, sep=',', dot='.'):
    num, _, frac = str(x).partition(dot)
    num = re.sub(r'(\d{3})(?=\d)', r'\1'+sep, num[::-1])[::-1]
    if frac:
        num += dot + frac
    return num

# Date reformatting
def mkdate(datestr):
    return time.strptime(datestr,'%Y%m%d')

# Provides a dictionary of map projections
def getproj():
    # Region specific settings, map projection and extent
    mapfn = ("%s/map_projections.txt" % SETDIR)
    map_projections = get_map_projections( mapfn )

    projdict = {'arctic'    : map_projections['SRS_ARCTIC_PROJ4'], \
                'antarctic' : map_projections['SRS_ANTARCTIC_PROJ4'] }
    return projdict

# Provides dictonaries of ice chart areas
def icechart_areas():

    # Arctic icechart data
    arc_names = [ 'general', 'baltic', 'fram_strait', 'barents', 'denmark_strait', 'svalbard', 'oslofjord' ]
    arc_old_names = [ 'c_map1', 'c_map2', 'c_map3', 'c_map6', 'c_map7', 'sarmap2', 'none' ]
    arc_ullr = [ [ -1881073,  -200000, 2621073, -3260000 ], \
                 [ -1124706, -2400000, 2229834, -4680000 ], \
                 [  -320270,  -865000,  864118, -1670000 ], \
                 [  -186475,  -450000, 2300004, -2140000 ], \
                 [ -2160000,	-1180000,  303130, -2854130 ], \
                 [    60000,	 -781540,  760000, -1640000 ], \
                 [   550000,    -3351517,  720000, -3560000 ] ]
    arc_coast = [ 'l', 'i', 'h', 'i', 'i', 'f', 'f' ]
    arc_orient = [ 'landscape', 'landscape', 'landscape', 'landscape', 'landscape', \
                   'portrait', 'portrait' ]
    arc_legend = [ 'l', 'r', 'l', 'l', 'l', 'l', 'l' ]
    arc_scalebar = [ [ 200000.0, 'ul' ], \
                     [ 200000.0, 'lr' ], \
                     [  50000.0, 'll' ], \
                     [ 100000.0, 'll' ], \
                     [ 100000.0, 'ul' ], \
                     [  50000.0, 'ur' ], \
                     [  10000.0, 'ur' ] ]

    # Antarctic ice chart data
    ant_names = [ 'antarctic', 'peninsula', 'weddell_east', 'bransfield_strait', 'adelaide_island' ]
    ant_old_names = [ 'c_map50', 'c_map51', 'c_map52', 'c_map53', 'c_map54' ]  # We don't actually have old names, just made up
    ant_ullr = [ [ -4296642.0, 3500000.0,  1000000.0, -100000.0  ], \
                 [ -3391881.0, 2579600.0, -1900000.0,  750000.0 ], \
                 [ -1000000.0, 2629600.0,   328798.0, 1000000.0 ], \
                 [ -2800000.0, 1800000.0, -2350000.0, 1248133.0 ], \
                 [ -2625536.3, 1333576.8, -2175536.3,  781709.8 ] ]
    ant_coast = [ 'l', 'i', 'i', 'i', 'i']
    ant_orient = [ 'landscape', 'portrait', 'portrait', 'portrait', 'portrait' ]
    ant_legend = [ 'r', 'l', 'l', 'l', 'l' ]
    ant_scalebar = [ [ 200000.0, 'lr' ], \
                     [ 100000.0, 'll' ], \
                     [ 100000.0, 'lr' ], \
                     [  50000.0, 'lr' ], \
                     [  50000.0, 'll' ] ]

    # Set up dictionaries
    ic_names = { 'arctic' : arc_names, 'antarctic' : ant_names }
    ic_old_names = { 'arctic' : arc_old_names, 'antarctic' : ant_old_names }
    ic_ullr = { 'arctic' : arc_ullr, 'antarctic' : ant_ullr }
    ic_coast = { 'arctic' : arc_coast, 'antarctic' : ant_coast }
    ic_orient = { 'arctic' : arc_orient, 'antarctic' : ant_orient }
    ic_legend = { 'arctic' : arc_legend, 'antarctic' : ant_legend }
    ic_scalebar = { 'arctic' : arc_scalebar, 'antarctic' : ant_scalebar }

    return [ ic_names, ic_old_names, ic_ullr, ic_coast, ic_orient, ic_legend, ic_scalebar ]


# Provides dictonaries of ice chart areas
def icechart_areas_wkend():

    # Arctic icechart data
    arc_names = [ 'svalbard' ]
    arc_old_names = [ 'sarmap2' ]
    arc_ullr = [ [    60000,	 -781540,  760000, -1640000 ] ]
    arc_coast = [ 'f']
    arc_orient = [ 'portrait' ]
    arc_legend = [ 'l' ]
    arc_scalebar = [ [  50000.0, 'ur' ] ]

    # Antarctic ice chart data
    ant_names = [ 'antarctic', 'peninsula', 'weddell_east', 'bransfield_strait', 'adelaide_island' ]
    ant_old_names = [ 'c_map50', 'c_map51', 'c_map52', 'c_map53', 'c_map54' ]  # We don't actually have old names, just made up
    ant_ullr = [ [ -4296642.0, 3500000.0,  1000000.0, -100000.0  ], \
                 [ -3391881.0, 2579600.0, -1900000.0,  750000.0 ], \
                 [ -1000000.0, 2629600.0,   328798.0, 1000000.0 ], \
                 [ -2800000.0, 1800000.0, -2350000.0, 1248133.0 ], \
                 [ -2625536.3, 1333576.8, -2175536.3,  781709.8 ] ]
    ant_coast = [ 'l', 'i', 'i', 'i', 'i']
    ant_orient = [ 'landscape', 'portrait', 'portrait', 'portrait', 'portrait' ]
    ant_legend = [ 'r', 'l', 'l', 'l', 'l' ]
    ant_scalebar = [ [ 200000.0, 'lr' ], \
                     [ 100000.0, 'll' ], \
                     [ 100000.0, 'lr' ], \
                     [  50000.0, 'lr' ], \
                     [  50000.0, 'll' ] ]

    # Set up dictionaries
    ic_names = { 'arctic' : arc_names, 'antarctic' : ant_names }
    ic_old_names = { 'arctic' : arc_old_names, 'antarctic' : ant_old_names }
    ic_ullr = { 'arctic' : arc_ullr, 'antarctic' : ant_ullr }
    ic_coast = { 'arctic' : arc_coast, 'antarctic' : ant_coast }
    ic_orient = { 'arctic' : arc_orient, 'antarctic' : ant_orient }
    ic_legend = { 'arctic' : arc_legend, 'antarctic' : ant_legend }
    ic_scalebar = { 'arctic' : arc_scalebar, 'antarctic' : ant_scalebar }

    return [ ic_names, ic_old_names, ic_ullr, ic_coast, ic_orient, ic_legend, ic_scalebar ]


# Ice concentration classes (Norway)
def get_ice_class(regionstr):
    if regionstr == 'arctic':
        ice_classes = [ [ 'Fast Ice',             '10/10th',    (150, 150, 150) ], \
                        [ 'Very Close Drift Ice', '9-10/10ths', (255,   0,   0) ], \
                        [ 'Close Drift Ice',      '7-9/10ths',  (255, 125,   7) ], \
                        [ 'Open Drift Ice',       '4-7/10ths',  (255, 255,   0) ], \
                        [ 'Very Open Drift Ice',  '1-4/10ths',  (140, 255, 160) ], \
                        [ 'Open Water',           '0-1/10ths',  (150, 200, 255) ] ]
    elif regionstr == 'antarctic':
        ice_classes = [ [ 'Fast Ice',             '10/10th',    (150, 150, 150) ], \
                        [ 'Very Close Drift Ice', '9-10/10ths', (255,   0,   0) ], \
                        [ 'Ice Shelf',            '',           (210, 210, 210) ], \
                        [ 'Close Drift Ice',      '7-9/10ths',  (255, 125,   7) ], \
                        [ 'Open Drift Ice',       '4-7/10ths',  (255, 255,   0) ], \
                        [ 'Iceberg',              'NIC ID',     (255, 255, 255) ], \
                        [ 'Very Open Drift Ice',  '1-4/10ths',  (140, 255, 160) ], \
                        [ 'Open Water',           '0-1/10ths',  (150, 200, 255) ], \
                        [ 'NIC Iceberg',          'NIC ID',     (255, 255, 255) ] ]
    return ice_classes


# Ice chart region dictionary
def get_region_codes():
    regcode = { 'arctic' : 'ARC', 'antarctic' : 'ANT' }
    return regcode


# Relationship between AARI ice chart parameters and NIS ice chart database parameters
# Columns are:
#   AARI Shapefile field code
#   NIS database field code
#   AARI Shapefile field type
#   AARI Shapefile field width and precision
def get_aari_dict():
    aari_dict = { 'AREA'      : [ 'si3area',            'real',   19.11 ], \
                  'PERIMETER' : [ 'si3perimeter',       'real',   19.11 ], \
                  'CT'        : [ 'si3ct',              'string', 2     ], \
                  'CA'        : [ 'si3ca',              'string', 2     ], \
                  'SA'        : [ 'si3sa',              'string', 2     ], \
                  'FA'        : [ 'si3fa',              'string', 2     ], \
                  'CB'        : [ 'si3cb',              'string', 2     ], \
                  'SB'        : [ 'si3sb',              'string', 2     ], \
                  'FB'        : [ 'si3fb',              'string', 2     ], \
                  'CC'        : [ 'si3cc',              'string', 2     ], \
                  'SC'        : [ 'si3sc',              'string', 2     ], \
                  'FC'        : [ 'si3fc',              'string', 2     ], \
                  'CN'        : [ 'si3cn',              'string', 2     ], \
                  'CD'        : [ 'si3cd',              'string', 2     ], \
                  'CF'        : [ [ 'si3fp', 'si3fs' ], 'string', 4     ], \
                  'POLY_TYPE' : [ 'si3poly_type',       'string', 1     ], \
                  'COLORCT'   : [ 'None',               'string', 2     ], \
                  'COLORCA'   : [ 'None',               'string', 2     ], \
                  'COLORSD'   : [ 'None',               'string', 2     ], \
                  'COLORSA'   : [ 'None',               'string', 2     ], \
                  'EGGSTR1'   : [ 'None',               'string', 4     ], \
                  'EGGSTR2'   : [ 'None',               'string', 14    ], \
                  'EGGSTR3'   : [ 'None',               'string', 6     ], \
                  'EGGSTR4'   : [ 'None',               'string', 2     ], \
                  'EGGSTR5'   : [ 'None',               'string', 2     ], \
                  'EGGSTR6'   : [ 'None',               'string', 11    ] }
    return aari_dict


# Relationship between NIC ice chart parameters and NIS ice chart database parameters
# Columns are:
#   NIC Shapefile field code
#   NIS database field code
#   NIC Shapefile field type
#   NIC Shapefile field width and precision
def get_nic_dict():
    nic_dict = { 'CT'         : [ 'si3ct',        'string', 2     ], \
                 'CA'         : [ 'si3ca',        'string', 2     ], \
                 'CB'         : [ 'si3cb',        'string', 2     ], \
                 'CC'         : [ 'si3cc',        'string', 2     ], \
                 'SO'         : [ 'si3so',        'string', 2     ], \
                 'SA'         : [ 'si3sa',        'string', 2     ], \
                 'SB'         : [ 'si3sb',        'string', 2     ], \
                 'SC'         : [ 'si3sc',        'string', 2     ], \
                 'SD'         : [ 'si3sd',        'string', 2     ], \
                 'FA'         : [ 'si3fa',        'string', 2     ], \
                 'FB'         : [ 'si3fb',        'string', 2     ], \
                 'FC'         : [ 'si3fc',        'string', 2     ], \
                 'FS'         : [ 'si3fs',        'string', 2     ], \
                 'FP'         : [ 'si3fp',        'string', 2     ], \
                 'ICECODE'    : [ 'None',         'string', 45    ], \
                 'POLY_TYPE'  : [ 'si3poly_type', 'string', 2     ], \
                 'Shape_Leng' : [ 'si3area',      'real',   19.11 ], \
                 'Shape_Area' : [ 'si3perimeter', 'real',   19.11 ], \
                 'COLORCT'    : [ 'None',         'string', 2     ], \
                 'COLORCA'    : [ 'None',         'string', 2     ], \
                 'COLORSD'    : [ 'None',         'string', 2     ], \
                 'COLORSA'    : [ 'None',         'string', 2     ], \
                 'EGGSTR1'    : [ 'None',         'string', 4     ], \
                 'EGGSTR2'    : [ 'None',         'string', 14    ], \
                 'EGGSTR3'    : [ 'None',         'string', 6     ], \
                 'EGGSTR4'    : [ 'None',         'string', 11    ], \
                 'EGGSTR5'    : [ 'None',         'string', 2     ], \
                 'EGGSTR6'    : [ 'None',         'string', 2     ] }

    return nic_dict


# Relationship between NIC iceberg data parameters and NIS database parameters
# Columns are:
#   NIC Shapefile field code
#   NIS database field code
#   NIS database field type
#   NIC Shapefile field type
#   NIC Shapefile field width and precision
def get_nicberg_dict():
    nicberg_dict = { 'Iceberg'    : [ 'iceberg',    'string', 'string', 80 ], \
                     'Length (NM' : [ 'length_nm',  'int',    'string', 80 ], \
                     'Width (NM)' : [ 'width_nm',   'int',    'string', 80 ], \
                     'Latitude'   : [ 'latitude',   'float',  'string', 80 ], \
                     'Longitude'  : [ 'longitude',  'float',  'string', 80 ], \
                     'Remarks'    : [ 'remarks',    'string', 'string', 80 ], \
                     'Last Updat' : [ 'lastupdate', 'date',   'string', 80 ] }
    return nicberg_dict


# List of database fields used for SIGRID-3.3 polygon output
# Columns are:
#   Field name in NIS database
#   Field type in NIS database
#   Target field name
#   Target field width
def get_si3poly_defn():
    fieldlist = [ [ 'analyst',         'string', '--',        2     ], \
                  [ 'norway_iceclass', 'string', '--',        20    ], \
                  [ 'si3area',         'float',  'AREA',      19.11 ], \
                  [ 'si3perimeter',    'float',  'PERIMETER', 19.11 ], \
                  [ 'si3poly_type',    'string', 'POLY_TYPE', 1     ], \
                  [ 'si3ct',           'int',    'CT',        2     ], \
                  [ 'si3ca',           'int',    'CA',        2     ], \
                  [ 'si3cb',           'int',    'CB',        2     ], \
                  [ 'si3cc',           'int',    'CC',        2     ], \
                  [ 'si3so',           'int',    'SO',        2     ], \
                  [ 'si3sa',           'int',    'SA',        2     ], \
                  [ 'si3sb',           'int',    'SB',        2     ], \
                  [ 'si3sc',           'int',    'SC',        2     ], \
                  [ 'si3sd',           'int',    'SD',        2     ], \
                  [ 'si3fa',           'int',    'FA',        2     ], \
                  [ 'si3fb',           'int',    'FB',        2     ], \
                  [ 'si3fc',           'int',    'FC',        2     ], \
                  [ 'si3fs',           'int',    'FS',        2     ], \
                  [ 'si3fp',           'int',    'FP',        2     ], \
                  [ 'si3cn',           'int',    'CN',        2     ], \
                  [ 'si3cd',           'int',    'CD',        2     ] ]
    return fieldlist

