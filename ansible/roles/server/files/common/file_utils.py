#!/usr/bin/python

# Name:          file_utils.py
# Purpose:       Various utilities for file handling.
#                  testoutdir  - Check for output directory and crate it if required.
#                  ftpdownload - Download a file over FTP with checking for co0nnection and timeouts.
#                  getproj4    - Get proj4 map projection string from a Shapefile
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
from ftplib import FTP
import threading

import osgeo.ogr as ogr

# Test for existence of output directory
# Create it if it does not exist
def testoutdir(dirname):
    dirbits = dirname.split('/')
    # print dirname
    # print dirbits
    targdir = ''
    for i in range(1,len(dirbits)):
        targdir = ("%s/%s" % (targdir,dirbits[i]))
        # print targdir

        # Try to change to directory, if it fails then create it
        try:
            os.chdir(targdir)
        except:
            # print targdir
            # print '  Make directory'
            os.mkdir(targdir)
        else:
            # print '  Exists'
            pass
    return


# Download a file from FTP
def ftpdownload( url, ftpdir, ftpfn, user, passwd, localfn ):
    # print ftpdir
    # print ftpfn
    # print localfn
    status = 0

    # Try to open FTP connection
    try:
        ftp = FTP( host=url, user=user, passwd=passwd, timeout=10 )
    except:
        print 'ftpdownload: Cannot open FTP connection.'
        status = -1

    # Change to remote file directory
    if status == 0:
        try:
            ftp.cwd( ftpdir )
        except:
            print 'ftpdownload: Cannot change to remote FTP directory.'
            status = -2

    # Open local file
    if status == 0:
        try:
            fout = open( localfn, 'wb' )
        except:
            print 'ftpdownload: Unable to open local file.'
            status = -3

    # Handle for FTP data being downloaded
    def handleDownload(block):
        fout.write(block)

    # Timeout function
    def timeout():
        ftp.abort()  # may not work according to docs
        ftp.close()
        fout.write(block)

    # Handle for FTP data being downloaded (can abort if no data received for 30 seconds
    def handleDownload(block):
        fout.write(block)
        if timerthread[0] is not None:
            timerthread[0].cancel()
        timerthread[0] = threading.Timer(30, timeout)
        timerthread[0].start()

    # Download file to temporary directory
    timerthread = [threading.Timer(30, timeout)]
    if status == 0:
        try:
            timerthread[0].start()
            ftp.retrbinary( ("RETR %s" % ftpfn), handleDownload )
            timerthread[0].cancel()
            fout.close()
        except:
            print 'ftpdownload: Problem downloading the file.'
            status = -4

    # Close ftp session
    if status == 0:
        ftp.close()

    # Terminate if statusis not zero
    if status != 0:
        print 'ftpdownload: Problems retrieving the file. Stopping.'
        sys.exit()

    return


# Get proj4 map projection string from a Shapefile
def getproj4( shpfn ):
    shpdrv = ogr.GetDriverByName('ESRI Shapefile')
    inds = shpdrv.Open(shpfn, 0)
    if inds is None:
        print ("getproj4: Could not open %s." % shpfn)
        sys.exit()
    inlay = inds.GetLayer()
    in_srs = inlay.GetSpatialRef()
    proj4str = in_srs.ExportToProj4()
    inds.Destroy()
    return proj4str

