#!/usr/bin/python

# Name:          process_hamburg_amsr2.py
# Purpose:       Routine to download University of Hamburg (Lars Kaleschke) 3.125 km AMSR2 data
#                files for Arctic and Antarctic.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, platform
import shutil
import subprocess
import time
from datetime import date, datetime, timedelta
import argparse
from urllib2 import urlopen, URLError, HTTPError
import gzip

import pg


# Directory settings for Bifrost server
BASEDIR = '/home/bifrostsat/Data/AMSR2'
DATADIR = ("%s/Raw" % BASEDIR)
ARCHIVEDIR = ("%s/Archive" % BASEDIR)
ANCDIR = '/home/bifrostsat/Ancillary/AMSR2'
DROPBOX = "/home/bifrostsat/Dropbox"
TMPDIR = ("%s/tmp" % BASEDIR)
GDALHOME = '/usr'

# Map projections
ARCPROJ='+proj=stere +lat_0=90n +lon_0=0e +lat_ts=90n +ellps=WGS84 +datum=WGS84'
ANTPROJ='+proj=stere +lat_0=90s +lon_0=0e +lat_ts=90s +ellps=WGS84 +datum=WGS84'

# Month codes
short_mth = [ 'jan', 'feb', 'mar', 'apr', 'may', 'jun', \
              'jul', 'aug', 'sep', 'oct', 'nov', 'dec' ]

# Length of archive in days
BUFSIZE = 31


# Download AMSR from University of Bremen
def download_amsr2_data( region, dt ):

    # Region specific settings
    if region == 'arctic':
        prodcode = 'Arc'
    elif region == 'antarctic':
        prodcode = 'Ant'
    else:
        print ("Unknown region \'%s\'." % region)
        sys.exit()

    # Target folder ftp://ftp-projects.cen.uni-hamburg.de/seaice/AMSR2/3.125km/

    # Arctic
    # ftp://ftp-projects.cen.uni-hamburg.de/seaice/AMSR2/3.125km/Arc_20160425_res3.125_pyres.nc.gz

    # Antarctic
    # ftp://ftp-projects.cen.uni-hamburg.de/seaice/AMSR2/3.125km/Ant_20160425_res3.125_pyres.nc.gz

    # Construct filenames and URL
    rawname = ("%s_%s_res3.125_pyres.nc.gz" % (prodcode,dt.strftime('%Y%m%d')))
    url = ("ftp://ftp-projects.cen.uni-hamburg.de/seaice/AMSR2/3.125km/%s" % rawname)
    localfn = ("%s/%s" % (DATADIR,rawname))

    # Try to download file
    print url
    try:
        fin = urlopen(url)
        # print "downloading " + url

        # Open our local file for writing
        with open(localfn, "wb") as local_file:
            local_file.write(fin.read())

        status = 1

    #handle errors
    except HTTPError, e:
        print "HTTP Error:", e.code, url
        status = -91
    except URLError, e:
        print "URL Error:", e.reason, url
        status = -92

    return [ status, localfn ]


# Reproject AMSR2 image
def reproject_amsr2( region, inpfn, dt ):

    # Region specific settings
    if region == 'arctic':
        inprojstr = '+proj=stere +lat_0=90 +lat_ts=70 +lon_0=-45 +k=1 +x_0=0 +y_0=0 +a=6378273 +b=6356889.449 +units=m +no_defs'
        ullrstr = '-3848514.90 5847676.86 3748104.34 -5348872.89'
        outprojstr = ARCPROJ
        outdir = ("%s/Arctic/GeoTIFF" % BASEDIR)
        regcode = 'ARC'
    elif region == 'antarctic':
        inprojstr = '+proj=stere +lat_0=-90 +lat_ts=-70 +lon_0=0 +k=1 +x_0=0 +y_0=0 +a=6378273 +b=6356889.449 +units=m +no_defs'
        ullrstr = '-3948365.49 4349861.26 3948804.76 -3948804.76'
        outprojstr = ANTPROJ
        outdir = ("%s/Antarctic/GeoTIFF" % BASEDIR)
        regcode = 'ANT'
    else:
        print ("Unknown region \'%s\'." % region)
        sys.exit()

    # UL -13500 -4146
    # UR 13500 -4146 3948804.76 -3948804.76
    # LL -4223 -3924 -3948365.49 4349861.26
    # LR 4223 -3924

    # Uncompress file
    [ inppath, rootfn ] = os.path.split(inpfn)
    # Remove the .gz
    rootfn = rootfn[:-3]
    ncfn = ("%s/%s" % (TMPDIR,rootfn))
    # print ncfn
    with gzip.open(inpfn, 'rb') as infile:
        with open(ncfn, 'w') as outfile:
            for line in infile:
                outfile.write(line)
                
    # Remove the .nc
    rootfn = rootfn[:-3]
    
    # Convert NetCDF to GeoTIFF
    tiffn = ("%s/%s.tif" % (TMPDIR,rootfn))
    cmd = ("%s/bin/gdal_translate -of GTiff -a_srs \"%s\" -a_ullr %s -q" \
        % (GDALHOME,inprojstr,ullrstr))
    cmd = ("%s NETCDF:\"%s\":sea_ice_concentration \"%s\" " % (cmd,ncfn,tiffn))
    # print cmd
    retcode = 0
    try:
        retcode = subprocess.call(cmd, shell=True)
        status = 1
        if retcode < 0:
            print >>sys.stderr, "Child was terminated by signal", -retcode
            status = -93
    except OSError, e:
        print >>sys.stderr, "Execution failed:", e
        status = -94

    # Warp GeoTIFF to our projection
    outfn = ("%s/%s_MET_%s.tif" % (outdir,rootfn,regcode))
    # print outfn
    cmd = ("%s/bin/gdalwarp -of GTiff -co \"COMPRESS=LZW\" -t_srs \"%s\"" \
        % (GDALHOME,outprojstr))
    cmd = ("%s -tr 1000 1000 -r bilinear -overwrite -q \"%s\" \"%s\"" \
        % (cmd,tiffn,outfn))
    # print cmd
    retcode = 0
    try:
        retcode = subprocess.call(cmd, shell=True)
        status = 1
        if retcode < 0:
            print >>sys.stderr, "Child was terminated by signal", -retcode
            status = -95
    except OSError, e:
        print >>sys.stderr, "Execution failed:", e
        status = -96
        
    # Remove temporary files
    os.remove( ncfn )
    os.remove( tiffn )

    return [ status, outfn ]


def mkdate(datestr):
    return datetime.strptime(datestr,'%Y%m%d')


if __name__ == '__main__':

    # Get today's date
    yesterday = datetime.now() - timedelta(days=1)
    yesterdaystr = yesterday.strftime('%Y-%m-%d')
    # print todaystr

    # Get user inputs using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument( "-r", "--region", help="Region for AMSR2 [arctic/antarctic/both]", \
        type=str.lower, choices=['arctic','antarctic','both'], default='both')
    parser.add_argument("-d","--date",help="Date for AMSR2 [yyyymmdd]",type=mkdate,default=yesterday)
    parser.add_argument("-o","--overwrite",help="Reset database and overwrite existing files.", \
        type=int, default=0, choices=[0,1])
    args = parser.parse_args()
    region = args.region
    year = (args.date).year
    month = (args.date).month
    day = (args.date).day
    datestr = ("%4d-%02d-%02d" % (year,month,day))
    dtstamp = datetime( year, month, day, 12, 0, 0 )
    over_flg = args.overwrite

    # Create a lock file for slow computers
    lockfn= ANCDIR + '/process_hamburg_amsr2.lock'
    # Check for stale lock file
    if os.path.isfile(lockfn):
        stat = os.stat(lockfn)
        ctime = stat.st_mtime
        lock_dt = datetime.fromtimestamp(ctime)
        lock_age = (datetime.now() - lock_dt).total_seconds()
        if lock_age > 3600.0:
            os.remove(lockfn)

    # Now see if lock file still exists
    try:
        fin = open(lockfn, 'r')
        fin.close()
        raise NameError, 'Lock Exists!'
    except IOError:
        # No lock file exists so create one
        tout = ("process_hamburg_amsr2.py %s" % datestr)
        fout = open(lockfn, 'w')
        fout.write(tout)
        fout.close()
        pass
    except NameError:
        # If we get to here then there is a lock file in existance so we should exit
        print 'Lock exists!'
        sys.exit()
    # print region

    # Connect to 'Satellite' database for updating the 'hamburg_amsr2' table.
    con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')

    # See if files have already been processed
    newflg = 1
    sql = ("SELECT id,arc_flg,ant_flg FROM hamburg_amsr2 WHERE amsr2date = \'%s\';" \
        % datestr)
    res = (con1.query(sql)).getresult()
    if len(res) > 0:
        amsr2id = int(res[0][0])
        arc_flg = int(res[0][1])
        ant_flg = int(res[0][2])
        newflg = 0
    else:
        arc_flg = 0
        ant_flg = 0

    # Get Arctic file and process
    arcstatus = 0
    if arc_flg == 0 or over_flg == 1:
        if region.find('arctic') > -1 or region.find('both') > -1:
            [ arcstatus, amsr2arcfn ] = download_amsr2_data( 'arctic', dtstamp )
            if arcstatus == 1:
                [ arcstatus, finalarcfn ] = reproject_amsr2( 'arctic', amsr2arcfn, dtstamp )
                # Move raw file to Archive
                [ path, rootfn ] = os.path.split( amsr2arcfn )
                shutil.move( amsr2arcfn, os.path.join(ARCHIVEDIR,rootfn) )  
            arc_flg = arcstatus

    # Get Antarctic file and process
    antstatus = 0
    if ant_flg == 0 or over_flg == 1:
        if region.find('antarctic') > -1 or region.find('both') > -1:
            [ antstatus, amsr2antfn ] = download_amsr2_data( 'antarctic', dtstamp )
            if antstatus == 1:
                [ antstatus, finalantfn ] = reproject_amsr2( 'antarctic', amsr2antfn, dtstamp ) 
                # Move raw file to Archive
                [ path, rootfn ] = os.path.split( amsr2antfn )
                shutil.move( amsr2antfn, os.path.join(ARCHIVEDIR,rootfn) )  
            ant_flg = antstatus

    # Current timestamp
    nowdt = datetime.now()
    nowstr = nowdt.strftime('%Y-%m-%d %H:%M:%S')

    # Construct query for database
    if newflg == 1:
        fieldstr = "id, amsr2date, arc_flg, arc_dt, ant_flg, ant_dt"
        valstr = ("nextval(\'hamburg_amsr2_serial\'), \'%s\', %d, \'%s\', %d, \'%s\'" \
            % (datestr,arcstatus,nowstr,antstatus,nowstr))
        sql = ("INSERT INTO hamburg_amsr2 (%s) VALUES (%s);" \
            % (fieldstr,valstr))
        print sql
        res = con1.query(sql)

    elif newflg == 0:
        update_count = 0
        sql = "UPDATE hamburg_amsr2 SET"
        divider = ""
        if arc_flg == 0 or over_flg == 1:
            if region.find('arctic') > -1 or region.find('both') > -1:
                sql = ("%s arc_flg = %d, arc_dt = \'%s\'" \
                    % (sql,arc_flg,nowstr))
                divider = ","
                update_count = update_count + 1
        if ant_flg == 0 or over_flg == 1:
            if region.find('antarctic') > -1 or region.find('both') > -1:
                sql = ("%s%s ant_flg = %d, ant_dt = \'%s\'" \
                        % (sql,divider,ant_flg,nowstr))
                update_count = update_count + 1
        sql = ("%s WHERE id = %d;" % (sql,amsr2id))

        if update_count > 0:        
            print sql
            res = con1.query(sql)
        else:
            # print 'No updates.'
            pass

    # Clean up files older than BUFSIZE days
    check_dirs = [ ("%s/Arctic/GeoTIFF" % BASEDIR), \
                   ("%s/Antarctic/GeoTIFF" % BASEDIR), \
                   DATADIR, ARCHIVEDIR ]
    target_dt = datetime.now() - timedelta(days=BUFSIZE)
    # print target_dt
    os.stat_float_times(True)
    for check_dir in check_dirs:
        fnlist = os.listdir(check_dir)
        for fname in fnlist:
            fullfn = os.path.join( check_dir, fname )
            stat = os.stat(fullfn)
            ctime = stat.st_mtime
            create_dt = datetime.fromtimestamp(ctime)
            if create_dt < target_dt:
                os.remove( fullfn )

    # Delete the lock file
    os.remove(lockfn)


