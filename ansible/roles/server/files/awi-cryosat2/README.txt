
Documentation at http://www.meereisportal.de/fileadmin/user_upload/redakteur/MeereisBeobachtung/Beobachtungsergebnisse_aus_Satellitenmessungen/CryoSat-2_Meereisprodukt/AWI_CryoSat-2_Documentation_20130624.pdf

Data is Arctic-only, monthly averages. Includes freeboard, thickness and snow depth.

NetCDF - http://data.seaiceportal.de/data/cryosat2/n/2016/cs2awi_nh_201601.nc