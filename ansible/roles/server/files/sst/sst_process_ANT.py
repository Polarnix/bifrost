#!/usr/bin/python

# Name:          sst_process_ANT.py
# Purpose:       Download MyOcean Antarctic SST NetCDF files from NCOF UK and generate contour lines.
#                This uses the OSTIA Global SST product SST_GLO_SST_L4_NRT_OBSERVATIONS_010_001
#                Files for yesterday are available around 06:00 UTC every day
#                NB: Use export HDF5_DISABLE_VERSION_CHECK=3 to disable warnings about wrong HDF5
#                    library version
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, platform
from datetime import datetime, date, timedelta
import time
import bz2
from ftplib import FTP
import zipfile
import argparse

from subprocess import call
import shutil

from netCDF4 import Dataset

import numpy as N

import osgeo.gdal as gdal
from osgeo.gdalconst import *
import osgeo.ogr as ogr
import osgeo.osr as osr

import pg

from file_utils import testoutdir, ftpdownload
from get_cmems_credentials import get_cmems_credentials

BASEDIR = '/home/bifrostadmin/Data/SST'
TMPDIR = ("%s/tmp" % BASEDIR)
SETDIR = '/home/bifrostadmin/Settings'
SSTINDIR = ("%s/Inputs" % BASEDIR)
SSTOUTDIR = '/home/bifrostadmin/Outputs'

# Database flag, set to 1 to allow creation and updating of database entries
dbflg = 1

# Date reformatting
def mkdate(datestr):
    year = int(datestr[0:4])
    month = int(datestr[4:6])
    day = int(datestr[6:8])
    return date(year,month,day)

# Try to get CMEMS credentials for FTP server
cmems_fn = ("%s/cmems_credentials.txt" % SETDIR)
credentials = get_cmems_credentials( cmems_fn )

# Get yesterday's date
yesterday = date.today() - timedelta(days=1)
yesterdaystr = yesterday.strftime('%Y-%m-%d')
# print todaystr

# Get the date for processing
parser = argparse.ArgumentParser()
parser.add_argument("-d","--date",help="Date for SST processing [yyyymmdd]",type=mkdate,default=yesterday)
args = parser.parse_args()
sstyr = (args.date).year
sstmh = (args.date).month
sstdy = (args.date).day
datestr = ("%4d%02d%02d" % (sstyr,sstmh,sstdy))
sstdate = date( sstyr, sstmh, sstdy )
# print sstdate
jdays = (sstdate - date( sstyr-1, 12, 31 )).days
# print jdays

# Set computer name
hoststr = platform.node()

# Connect to ice charts database
dbcon = pg.connect(dbname='Ocean', host='localhost', user='bifrostadmin', passwd='bifrostadmin')

# Check processing status
sqltxt = ("SELECT id,ant_flg,ant_dt,ant_computer FROM sst_processing WHERE sstdate = \'%4d-%02d-%02d\';" \
    % (sstyr,sstmh,sstdy))
# print sqltxt
checkres = dbcon.query(sqltxt)
result = checkres.getresult()
update_flg = len(result)
# If we have a result, the check to see if processing is already done.
if update_flg > 0:
    idval = result[0][0]
    ant_flg = result[0][1]
    ant_dt = result[0][2]
    ant_computer = result[0][3]
    # arctic_flg will be greater than zero if processing has been done
    if ant_flg > 0:
        outtxt = ("sst_process_ANT: Processing already done for %4d-%02d-%02d." % (sstyr,sstmh,sstdy))
        outtxt = ("%s  Done at %s on %s." % (outtxt,ant_dt,ant_computer))
        print outtxt
        sys.exit()

# Download file from ISAC CNR (Italy) ftp server (from 2015-12-15)
user = credentials['CMEMS_USER']
passwd = credentials['CMEMS_PASSWD']
url = credentials['CMEMS_HOST']
ftpdir = ("Core/SST_GLO_SST_L4_NRT_OBSERVATIONS_010_001/METOFFICE-GLO-SST-L4-NRT-OBS-SST-V2/%4d/%02d" % (sstyr,sstmh))
ftpfn = ("%4d%02d%02d120000-UKMO-L4_GHRSST-SSTfnd-OSTIA-GLOB-v02.0-fv02.0.nc" \
    % (sstyr,sstmh,sstdy))
localfn = ("%s/%s" % (TMPDIR,ftpfn))
status = ftpdownload( url, ftpdir, ftpfn, user, passwd, localfn )

# Uncompress NetCDF (not needed with OSTIA NetCDF data)
# ncfn = ("%s/%s" % (TMPDIR,ftpfn[0:-4]))
# with open(ncfn,"wb") as tmp:
#     shutil.copyfileobj(bz2.BZ2File(localfn), tmp)
ncfn = localfn

# Translate NetCDF to GeoTIFF using GDAL
# NB: gdalinfo HDF5:"../SST/Input/20141111120000-UKMO-L4_GHRSST-SSTfnd-OSTIA-GLOB-v02.0-fv02.0.nc"://analysed_sst
# Above gdalinfo works
# ncfile = Dataset(ncfn,'r')
# print ncfile.variables
srcfn = ("HDF5:\"%s\"://analysed_sst" % ncfn)
tiffn = ("%s.tif" % ncfn[0:-3])
# print srcfn
# print tiffn
src_ds = gdal.Open( srcfn )
driver = gdal.GetDriverByName( 'Gtiff' )
band = src_ds.GetRasterBand(1)
sstdata = N.flipud(band.ReadAsArray())[2400:3500,:]  # 3600
[ny,nx] = sstdata.shape
# print ny,nx
band = None

# dst_ds = driver.CreateCopy( tiffn, src_ds, 0 )
dst_ds = driver.Create(tiffn, nx, ny, 1, GDT_Float32)
outband = dst_ds.GetRasterBand(1)
outband.WriteArray(sstdata,0,0)

# No GCPs in file, so have to manually set transfrom and spatial reference system
# transform = [ -180.0, 0.05, 0.0, -30.0, 0.0, -0.05 ]
# dst_ds.SetGeoTransform( transform )
llsrs = osr.SpatialReference()
llsrs.ImportFromEPSG( 4326 )
dst_ds.SetProjection( llsrs.ExportToWkt() )
gcps = []
for j in range(0,1101,100):
    lat = -30.0 - (float(j) * 0.05)
    for i in range(0,7201,100):
       lon = -180.0 + (float(i) * 0.05)
       ngcp = gdal.GCP()
       ngcp.GCPX = lon
       ngcp.GCPY = lat
       ngcp.GCPZ = 0.0
       ngcp.GCPPixel = i
       ngcp.GCPLine = j
       # ngcp.Info = ''
       # ngcp.Id = ''
       # print ngcp
       gcps.append(ngcp)
# print gcps
try:
    dst_ds.SetGCPs( gcps, llsrs.ExportToWkt() )
except:
    print 'Failed to set GCPs'
    sys.exit()
dst_ds = None
src_ds = None

# Warp GeoTIFF to Polar Stereographic
psfn = ("%s_pstereo.tif" % ncfn[0:-3])
# src_proj = '+proj=longlat +ellps=WGS84'
trg_proj = '+proj=stere +lat_0=90s +lon_0=0e +lat_ts=90s +ellps=WGS84'
cmd = "/usr/bin/gdalwarp -of GTiff -co \"COMPRESS=LZW\""
cmd = ("%s -t_srs \"%s\"" % (cmd,trg_proj))
cmd = ("%s -tr 5000 5000 -te -7000000 -7000000 7000000 7000000" % cmd)
cmd = ("%s -srcnodata -32768 -dstnodata -32768" % cmd)
cmd = ("%s -tps -r near -overwrite -q" % cmd)
cmd = ("%s -wo \"SAMPLE_GRID=YES\"" % cmd)
cmd = ("%s \"%s\"" % (cmd,tiffn))
cmd = ("%s \"%s\"" % (cmd,psfn))
# print cmd
try:
    retcode = call(cmd, shell=True)
    if retcode < 0:
        print >>sys.stderr, "Child was terminated by signal", -retcode
    else:
        # print >>sys.stderr, "Child returned", retcode
        pass
except OSError, e:
    print >>sys.stderr, "Execution failed:", e
# Remove original GeoTIFF file
os.remove(tiffn)

# Scale image to Celsius
src_ds = gdal.Open(psfn, GA_ReadOnly)
geotransform = src_ds.GetGeoTransform()
projection = src_ds.GetProjection()
band = src_ds.GetRasterBand(1)
cols = src_ds.RasterXSize
rows = src_ds.RasterYSize
sstdata = band.ReadAsArray()
# print sstdata.shape
band = None
src_ds = None
# Conversion to Celsius
sstdata = N.ma.masked_equal( sstdata.astype(N.float32), -32768 )
sstdata = (sstdata - 273.15) * 0.01
# print N.min(sstdata), N.max(sstdata)
sstfn = ("%s_celsius.tif" % ncfn[0:-3])
trg_ds = driver.Create( sstfn, cols, rows, 1, GDT_Float32, ["COMPRESS=LZW"] )
outband = trg_ds.GetRasterBand(1)
outband.WriteArray(sstdata)
outband.SetNoDataValue(-32768)
outband.FlushCache()
trg_ds.SetGeoTransform(geotransform)
trg_ds.SetProjection(projection)
outband = None
trg_ds = None
# Remove original projected GeoTIFF file
os.remove(psfn)

# Get SST limits
minsst = int( N.ceil(N.min(sstdata)) )
maxsst = int( N.floor(N.max(sstdata)) )
# print minsst, maxsst
del sstdata

# Create contour Shapefile
shpfn = ("%s_tmp.shp" % ncfn[0:-3])
# Open input raster
src_ds = gdal.Open(sstfn, GA_ReadOnly)
geotransform = src_ds.GetGeoTransform()
projection = src_ds.GetProjection()
band = src_ds.GetRasterBand(1)
cols = src_ds.RasterXSize
rows = src_ds.RasterYSize
# Open output Shapefile
shpdrv = ogr.GetDriverByName('ESRI Shapefile')
if os.path.exists(shpfn):
    shpdrv.DeleteDataSource(shpfn)
trg_ds = shpdrv.CreateDataSource(shpfn)
shp_lyr = trg_ds.CreateLayer( ("SST_%4d%02d%02d" % (sstyr,sstmh,sstdy)) )
field_defn = ogr.FieldDefn('ID', ogr.OFTInteger)
shp_lyr.CreateField(field_defn)
field_defn = ogr.FieldDefn('SST', ogr.OFTReal)
shp_lyr.CreateField(field_defn)
# Generate contours
gdal.ContourGenerate( band, 1.0, float(minsst), [], 1, -32768.0, \
    shp_lyr, 0, 1 )
# Close Shapefile
trg_ds.Destroy()
# Write projection file
prjfn = ("%s_tmp.prj" % ncfn[0:-3])
prjfile = open(prjfn, 'w')
prjfile.write(projection)
prjfile.close()
# Remove the SST raster
os.remove(sstfn)

# Simplify contours and remove line segments with less than 15 points
simplefn = ("%s.shp" % ncfn[0:-3])
# Open input Shapefile
src_ds = shpdrv.Open(shpfn,0)
inlay = src_ds.GetLayer()
in_srs = inlay.GetSpatialRef()
# Open output Shapefile
if os.path.exists(simplefn):
    shpdrv.DeleteDataSource(simplefn)
trg_ds = shpdrv.CreateDataSource(simplefn,)
out_lyr = trg_ds.CreateLayer( ("SST_%4d%02d%02d" % (sstyr,sstmh,sstdy)), \
    geom_type=ogr.wkbLineString, srs=in_srs )
field_defn = ogr.FieldDefn('ID', ogr.OFTInteger)
out_lyr.CreateField(field_defn)
field_defn = ogr.FieldDefn('SST', ogr.OFTReal)
out_lyr.CreateField(field_defn)
featureDefn = out_lyr.GetLayerDefn()

# Simplify distance
dist = 1000.0

# Limit to size
plimit = 15

# Loop through input lines
nline = inlay.GetFeatureCount()
for i in range(nline):
    # Get input feature
    infeat = inlay.GetFeature(i)
    fid = infeat.GetField('ID')
    sst = infeat.GetField('SST')
    inline = infeat.GetGeometryRef()
    np = inline.GetPointCount()
    # Simplify
    newline = inline.Simplify( dist )
    newp = newline.GetPointCount()
    # Create an output feature
    if newp >= plimit:
        outfeat = ogr.Feature( featureDefn )
        outfeat.SetGeometry(newline)
        outfeat.SetField('ID', fid)
        outfeat.SetField('SST', sst)
        out_lyr.CreateFeature(outfeat)
        outfeat.Destroy()

        # Print summary
        # print ("%6d %5.1f %8d %8d" % (fid,sst,np,newp))

    # Destroy features
    infeat.Destroy()

# Close files
src_ds.Destroy()
trg_ds.Destroy()

# Write projection file
prjfn = ("%s.prj" % simplefn[0:-4])
prjfile = open(prjfn, 'w')
prjfile.write(in_srs.ExportToWkt())
prjfile.close()

# Remove temporary Shapefile
shpdrv.DeleteDataSource(shpfn)

# Get list of final files
filelist = os.listdir( TMPDIR )
shplist = []
searchstr = ("%4d%02d%02d120000-UKMO-L4_GHRSST-SSTfnd-OSTIA-GLOB" \
    % (sstyr,sstmh,sstdy))
for fname in sorted(filelist):
    if fname.find( searchstr ) > -1 and fname.find('.nc') == -1:
        shplist.append(fname)

# Create a zip-file for the Output folder
outputdir = ("%s/%4d/%4d%02d%02d" % (SSTOUTDIR,sstyr,sstyr,sstmh,sstdy))
testoutdir(outputdir)
zipfn = ("%s/%4d%02d%02d120000-UKMO-L4_GHRSST-SSTfnd-OSTIA-GLOB-v02.0-fv02.0.zip" \
    % (outputdir,sstyr,sstmh,sstdy))
zipf = zipfile.ZipFile( zipfn, 'w', zipfile.ZIP_DEFLATED )
for shpfn in shplist:
    fname = ("%s/%s" % (TMPDIR,shpfn))
    zipf.write(fname,shpfn)
zipf.close()

# Remove temporary Shapefile
shpdrv.DeleteDataSource(simplefn)

# Compress original input NetcDF file
bz2fn = ("%s/%s.bz2" % (SSTINDIR,ftpfn))
with open(ncfn, 'rb') as ncin:
    with bz2.BZ2File(bz2fn, 'wb', compresslevel=9) as bz2out:
        shutil.copyfileobj(ncin, bz2out)
# Delete original NetCDF input file
os.remove(ncfn)

# Update database processing status
timenow = datetime.now()
dtnowstr = timenow.strftime('%Y-%m-%d %H:%M:%S')
if update_flg == 0:
    # Create new database record
    sqltxt = ("INSERT INTO sst_processing (id,sstdate,ant_flg,ant_dt,ant_computer,arc_flg)")
    sqltxt = ("%s VALUES (nextval(\'sst_processing_serial\'),\'%4d-%02d-%02d\'" % (sqltxt,sstyr,sstmh,sstdy))
    sqltxt = ("%s,1,\'%s\',\'%s\',0);" % (sqltxt,dtnowstr,hoststr))
else:
    # Update existing record
    sqltxt = "UPDATE sst_processing SET"
    sqltxt = ("%s ant_flg = 1, ant_dt = \'%s\', ant_computer = \'%s\'" \
        % (sqltxt,dtnowstr,hoststr))
    sqltxt = ("%s WHERE id = %d;" % (sqltxt,idval))
if dbflg == 1:
    checkres = dbcon.query(sqltxt)
else:
    print sqltxt

