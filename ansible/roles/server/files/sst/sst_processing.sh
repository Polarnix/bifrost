#!/bin/bash

# Name:          sst_processing.sh
# Purpose:       Shell script to run Python scripts doing Arctic and Antarctic SST processing.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

PYTHON=/usr/bin/python
BASEDIR=/home/bifrostadmin

DTSTR=`/bin/date +"%Y-%m-%d %H:%M:%S"`
echo "sst_processing.sh: ${DTSTR}"

# Arctic
"${PYTHON}" "${BASEDIR}/Python/SST/sst_process_ARC.py"

# Antarctic
"${PYTHON}" "${BASEDIR}/Python/SST/sst_process_ANT.py"

exit
