<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="1.8.0-Lisboa" minimumScale="0" maximumScale="1e+08" minLabelScale="0" maxLabelScale="1e+08" hasScaleBasedVisibilityFlag="0" scaleBasedLabelVisibilityFlag="0">
  <transparencyLevelInt>255</transparencyLevelInt>
  <classificationattribute>country</classificationattribute>
  <uniquevalue>
    <classificationfield>country</classificationfield>
    <symbol>
      <lowervalue null="1"></lowervalue>
      <uppervalue></uppervalue>
      <label>default</label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="195" blue="254" green="87"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath>.</texturepath>
    </symbol>
    <symbol>
      <lowervalue>Argentina</lowervalue>
      <uppervalue>Argentina</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="156" blue="109" green="206"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath>.</texturepath>
    </symbol>
    <symbol>
      <lowervalue>Australia</lowervalue>
      <uppervalue>Australia</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="122" blue="159" green="196"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath>.</texturepath>
    </symbol>
    <symbol>
      <lowervalue>Chile</lowervalue>
      <uppervalue>Chile</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="236" blue="114" green="137"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath>.</texturepath>
    </symbol>
    <symbol>
      <lowervalue>New Zealand</lowervalue>
      <uppervalue>New Zealand</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="134" blue="209" green="162"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath>.</texturepath>
    </symbol>
    <symbol>
      <lowervalue>South Africa</lowervalue>
      <uppervalue>South Africa</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="244" blue="0" green="174"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath>.</texturepath>
    </symbol>
  </uniquevalue>
  <customproperties/>
  <displayfield>name</displayfield>
  <label>1</label>
  <labelfield>name</labelfield>
  <labelattributes>
    <label fieldname="name" text="Label"/>
    <family fieldname="" name="Liberation Sans Narrow"/>
    <size fieldname="" units="mu" value="200000"/>
    <bold fieldname="" on="0"/>
    <italic fieldname="" on="0"/>
    <underline fieldname="" on="0"/>
    <strikeout fieldname="" on="0"/>
    <color fieldname="" red="0" blue="0" green="0"/>
    <x fieldname=""/>
    <y fieldname=""/>
    <offset x="0" y="0" units="pt" yfieldname="" xfieldname=""/>
    <angle fieldname="" value="0" auto="0"/>
    <alignment fieldname="" value="center"/>
    <buffercolor fieldname="" red="255" blue="255" green="255"/>
    <buffersize fieldname="" units="pt" value="1"/>
    <bufferenabled fieldname="" on=""/>
    <multilineenabled fieldname="" on=""/>
    <selectedonly on=""/>
  </labelattributes>
  <edittypes>
    <edittype type="0" name="country"/>
    <edittype type="0" name="id"/>
    <edittype type="0" name="metarea"/>
    <edittype type="0" name="name"/>
  </edittypes>
  <editform>.</editform>
  <editforminit></editforminit>
  <annotationform>.</annotationform>
  <attributeactions/>
</qgis>
