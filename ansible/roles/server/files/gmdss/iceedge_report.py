#!/usr/bin/python

# Name:          iceedge_report.py
# Purpose:       Generates ice edge text for GMDSS broadcast.
# Usage:         python iceedge.py iceedge.shp [metarea number]
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import sys
import string
from datetime import date
from ftplib import FTP
import zipfile

from osgeo import ogr

DATADIR='/vol/data/iceEdge'
STATICDIR='/home/istjenesten/software/METAREA-XIX/python/static'

# Get filenames from command line
lineshp=sys.argv[1]
if len(sys.argv) == 3:
    # Check validity of metarea number
    # print sys.argv[2]
    if int(sys.argv[2]) != 1 and int(sys.argv[2]) != 19:
        print "Only METAREA's 1 and 19 are valid!"
        sys.exit()
    areas = [int(sys.argv[2])]
else:
    areas = [ 19, 1 ]
# print areas

# Load Shapefile drivers
driverName = "ESRI Shapefile"
drv = ogr.GetDriverByName( driverName )
if drv is None:
    print "%s driver not available.\n" % driverName
    sys.exit( 1 )

# Open ice edge line
ds = ogr.Open( lineshp )
if ds is None:
    print "Open failed.\n"
    sys.exit( 1 )

# Open data layer
lyrname = lineshp.split("/")[-1]
lyrname = lyrname[:-4]
# print lyrname
lyr = ds.GetLayerByName( lyrname )
lyr.ResetReading()

# Read lines in data layer
edgex = []
edgey = []
for feat in lyr:
    geom = feat.GetGeometryRef()
    # print geom
    if geom is not None and geom.GetGeometryType() == ogr.wkbLineString:
        np = geom.GetPointCount()
        for i in range(np):
            # print "%10.5f, %10.5f" % ( geom.GetX(i), geom.GetY(i) )
            edgex = edgex + [ geom.GetX(i) ]
            edgey = edgey + [ geom.GetY(i) ]
    else:
        print "no line geometry\n"
        sys.exit()

# Close line Shapefile
ds = None

# print edgex
# print edgey

today = date.today()

# Go through the METAREAs
for metarea in areas:

    # Open output file
    foutname = ('%s/ice%02d_%s_23.txt' % (DATADIR,metarea,today.strftime("%Y%m%d")) )
    fout = open(foutname, 'w')

    # Header information
    fout.write(("ICE BULLETIN FOR METAREA %d ISSUED BY THE NORWEGIAN ICE SERVICE\n" % metarea))
    fout.write("(ISTJENESTEN@MET.NO) AT 23 UTC %d %s\n" % ( today.day,string.upper(today.strftime("%b")) ))

    # Open METAREA polygons file
    metareashp=('%s/metarea-%02d.shp' % (STATICDIR,metarea))
    ds = ogr.Open( metareashp )
    if ds is None:
        print "Open failed.\n"
        sys.exit( 1 )

    # Open data layer
    lyrname = metareashp.split("/")[-1]
    lyrname = lyrname[:-4]
    # print lyrname
    lyr = ds.GetLayerByName( lyrname )
    lyr.ResetReading()

    # Get field definitions
    feat_defn = lyr.GetLayerDefn()
    # print 'Fields ', feat_defn.GetFieldCount()
    fieldnames = []
    for i in range(feat_defn.GetFieldCount()):
        field_defn = feat_defn.GetFieldDefn(i)
        # print field_defn.GetName(), field_defn.GetType()
        fieldnames = fieldnames + [ field_defn.GetName() ]
    # print fieldnames

    idlist = []
    namelist = []
    polywkblist = []
    npoly = 0
    polyxlist = []
    polyylist = []
    for feat in lyr:

        idlist = idlist + [ feat.GetField(0) ]    
        namelist = namelist + [ feat.GetField(2) ]

        geom = feat.GetGeometryRef()
        # print geom
        polywkblist = polywkblist + [ geom ]
        if geom is not None and geom.GetGeometryType() == ogr.wkbPolygon:
            tmpx = []
            tmpy = []
            ring = geom.GetGeometryRef(0)
            np = ring.GetPointCount()
            # print np
            for i in range(np):
                # print "%10.5f, %10.5f" % ( geom.GetX(i), geom.GetY(i) )
                lon, lat, z = ring.GetPoint(i)
                tmpx = tmpx + [ lon ]
                tmpy = tmpy + [ lat ]
            # print tmpx
            # print tmpy
            polyxlist = polyxlist + [ tmpx ]
            polyylist = polyylist + [ tmpy ]
            npoly = npoly + 1
        else:
            print "no polygon geometry\n"
            sys.exit()

    # Close METAREA Shapefile
    ds = None

    # print idlist
    # print namelist
    # print polyxlist
    # print polyylist
    # print npoly

    # sortedlist = sorted(idlist)
    # print sortedlist

    # Loop through polygons
    for i in sorted(idlist):

        # Get position of polygon in list (sorting by ID)
        idx = -9999
        for j in range(npoly):
            if idlist[j] == i:
                idx = j
        # print idx, idlist[idx], namelist[idx]

        # Create polygon geometry
        ring = ogr.Geometry(ogr.wkbLinearRing)
        polyx = polyxlist[idx]
        polyy = polyylist[idx]
        for j in range(len(polyx)):
            # print polyx[j], polyy[j]
            ring.AddPoint(polyx[j], polyy[j])
        polygon = ogr.Geometry(ogr.wkbPolygon)
        polygon.AddGeometry(ring)

        # Loop through points in line, and see if they fall within polygon
        within = 0
        firstflg = 1
        segmentn = 0
        listn = 0
        segstr = []
        xlist = []
        ylist = []
        for j in range(len(edgex)):
            point = ogr.Geometry(ogr.wkbPoint)
            point.SetPoint_2D(0,edgex[j], edgey[j])
            within = int(point.Within(polygon))
            # print point, point.Within(polygon), firstflg, within
            if firstflg == 1:
                firstflg = 0
                if within == 1:
                    xlist = xlist + [ edgex[j] ]
                    ylist = ylist + [ edgey[j] ]
                    segmentn = segmentn + 1
                    segstr = segstr + [ listn ]
                    listn = listn + 1
            else:
                line = ogr.Geometry(ogr.wkbLineString)
                line.AddPoint(edgex[j-1], edgey[j-1])
                line.AddPoint(edgex[j], edgey[j])
                if oldwithin == 0 and within == 1:
                    # print "intersect entering"
                    intersect = line.Intersection(polygon)
                    # print line
                    # print intersect
                    # print intersect.GetX(0), intersect.GetY(0)
                    xlist = xlist + [ intersect.GetX(0) ]
                    ylist = ylist + [ intersect.GetY(0) ]
                    xlist = xlist + [ intersect.GetX(1) ]
                    ylist = ylist + [ intersect.GetY(1) ]
                    segmentn = segmentn + 1
                    segstr = segstr + [ listn ]
                    listn = listn + 2
                elif oldwithin == 1 and within == 1:
                    # print "within"
                    xlist = xlist + [ edgex[j] ]
                    ylist = ylist + [ edgey[j] ]
                    listn = listn + 1
                elif oldwithin == 1 and within == 0:
                    # print "intersect exiting"
                    intersect = line.Intersection(polygon)
                    # print line
                    # print intersect
                    xlist = xlist + [ intersect.GetX(1) ]
                    ylist = ylist + [ intersect.GetY(1) ]
                    listn = listn + 1
                line.Destroy()
            point.Destroy()


            oldwithin = within

        # if segmentn > 0:
        #     print 'Segments = ', segmentn
        #     print segstr
        #     print listn
 
        polygon.Destroy()

        # print ' '
        # print xlist
        # print ylist

        # If point in sub-area, print report
        if len(xlist) > 0:

            # print ('Ice edge within METAREA-XIX Sub-Area \"%s\",' % namelist[idx].upper())
            # print 'as issued by the Norwegian Ice Service (istjenesten@met.no), is'
            # print 'north of the line delineated by the following coordinates:'
            fout.write(("\n%s\n" % namelist[idx].upper()))

            first = 1
            for segment in range(segmentn):
                if first == 1:
                    coordline = 'ICE N OF '
                    first = 0
                    limit = 3
                else:
                    coordline = ''
                    limit = 4
                nlin = 0
                start = segstr[segment]
                if (segment+1) >= segmentn:
                    end = len(xlist)
                else:
                    end = segstr[segment+1]
                # print start, end
                for j in range(start,end):
                    lat = ylist[j]
                    latdeg = abs(int(lat))
                    latmin = int((lat % 1) * 60)
                    # print lat, latdeg, latmin
                    if lat < 0:
                        latstr = ('%02d%02dS' % (latdeg,latmin))
                    else:
                        latstr = ('%02d%02dN' % (latdeg,latmin))
                    lon = xlist[j]
                    londeg = abs(int(lon))
                    lonmin = int((lon % 1) * 60)
                    if lon < 0:
                        lonstr = ('%03d%02dW' % (londeg,lonmin))
                    else:
                        lonstr = ('%03d%02dE' % (londeg,lonmin))
                    coordline = coordline + latstr + ' ' + lonstr
                    if j < (end-1):
                        coordline = coordline + ', '
                    nlin = nlin + 1
                    if nlin == limit:
                        fout.write(("%s\n" % coordline))
                        coordline = ''
                        nlin = 0
                if nlin > 0:
                    fout.write(("%s\n" % coordline))
                if segment < (segmentn-1):
                    fout.write("ICE EDGE EXITS AREA AND RE-ENTERS AT\n")

    fout.write("\nICE EDGE NOT FOR NAVIGATIONAL PURPOSES\n")
    fout.close()

    # sys.exit()

    # Send results to AARI GMDSS site
    if metarea == 1 or metarea == 19:
        # Create a zip file containing the Shapefile
        zipfname = ('%s/ice%02d_%s_23.zip' % (DATADIR,metarea,today.strftime("%Y%m%d")) )
        shpfname = lineshp
        dbffname = shpfname[:-4]+'.dbf'
        shxfname = shpfname[:-4]+'.shx'
        # print zipfname
        edgezip = zipfile.ZipFile(zipfname,'w')
        edgezip.write(dbffname)
        edgezip.write(shpfname)
        edgezip.write(shxfname)
        edgezip.close()

        # Transfer files
        username = ('metarea%02d_ice' % metarea)
        password = ('metarea%02dgmdss' % metarea)
        # print username, password
        try:
            aari = FTP(host='gmdss.aari.ru',user=username,passwd=password)
        except:
            print 'Problem with FTP transfer to AARI'
        else:
            status = aari.cwd(('bull/%02d' % metarea))
            targdir=today.strftime("%Y%m%d")
            entries = aari.nlst()
            present = 0
            for fname in entries:
                if targdir == fname:
                    present = 1
            # print present
            if present == 0:
                status = aari.mkd(targdir)
                # print status
            status = aari.cwd(targdir)
            # Send the text file
            fin = open(foutname,'r')
            storname = ('ice%02d_%s_23.txt' % (metarea,today.strftime("%Y%m%d")) )
            cmd = ("STOR %s" % storname)
            # print cmd
            status = aari.storlines(cmd,fin)
            fin.close()
            # Send the Shapefile file zipped
            fin = open(zipfname,'r')
            storname = ('ice%02d_%s_23.zip' % (metarea,today.strftime("%Y%m%d")) )
            cmd = ("STOR %s" % storname)
            # print cmd
            status = aari.storbinary(cmd,fin)
            fin.close()
            aari.quit()
