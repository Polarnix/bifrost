METAREA-XIX (and METAREA-I) SOFTWARE
Nick Hughes, 2011-v-25

Generation of Ice Edge reports:

Use the script 'iceedge_report.py' in the python folder.  This takes an ice edge
file in Shapefile format, for example as created for the Bodo Radio report, and
outputs text in a suitable format for the METAREA.

python python/iceedge_report.py example_line/edge_example_v2.shp 19 > report.txt

This opens the Shapefile at example_line/edge_example_v2.shp, applies the
sub-areas from METAREA-XIX (19), and puts the text in a file called report.txt.

Features:
- If there is more than one ice edge line in the Shapefile, the software will join
these with a straight line.
- The software should notice that an ice edge leaves a sub-area, and comes back
in again.  This will be reported by groups of coordinates separated by the text
'Ice edge exits area and re-enters at'.

The report text should look like as follows:

Ice edge within METAREA-XIX Sub-Area "A3",
as issued by the Norwegian Ice Service (istjenesten@met.no), is
north of the line delineated by the following coordinates:
77.5N,  1.5E  78.1N,  3.1E  78.9N,  4.8E  79.5N,  6.0E  
79.6N,  9.5E  79.0N, 10.0E  
Ice edge exits area and re-enters at
78.5N, 10.0E  78.2N,  8.8E  77.8N,  8.7E  77.8N, 10.0E  
Ice edge not for navigational purposes.
