#!/bin/bash

# Name:          get_metadata.sh
# Purpose:       Shell script to download all MODIS archive metadata files.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

cd /home/bifrostsat/Data/MODIS

CURYR=`date +'%Y'`

# TERRA : 2000-present
for ((YEAR=2000; YEAR<=CURYR; YEAR++)); do
   echo TERRA $YEAR
   wget -x -nH ftp://ladsweb.nascom.nasa.gov/geoMeta/6/TERRA/$YEAR/*.txt
done

# AQUA  : 2002-present
for ((YEAR=2002; YEAR<=CURYR; YEAR++)); do
   echo AQUA $YEAR
   wget -x -nH ftp://ladsweb.nascom.nasa.gov/geoMeta/6/AQUA/$YEAR/*.txt
done

exit

