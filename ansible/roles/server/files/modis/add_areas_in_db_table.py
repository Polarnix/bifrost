#!/usr/bin/python

# Name:          add_areas_in_db_table.py
# Purpose:       Add analysis areas to MODIS processing database table.
#                Version for Archive processing on "biserver".
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import pg

from load_subset_areas import load_subset_areas
from modis_areas import modis_areas

BASEDIR = '/home/bifrostsat/Python/MODIS'
INCDIR = '/home/bifrostsat/Include/MODIS'

# Load subset information from Shapefile
shpfname = ("%s/modis_subsets_ll.shp" % INCDIR)
[nsubset,subset] = load_subset_areas( shpfname )

# Define column names and data types for 'modis_files' table
column_names = []
data_types = []

# Add area fields
for i in range(nsubset):
    coverstr = ("%s_cover" % subset[i].shortname)
    column_names.append( coverstr )
    data_types.append( 'real' )
    cloudstr = ("%s_cloud" % subset[i].shortname)
    column_names.append( cloudstr )
    data_types.append( 'real' )

# Connect to database
con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostadmin', passwd='bifrostadmin')

# Add new area data columns
idx = 0
for cname, dtype in zip( column_names, data_types ):
    # print cname, dtype
    querystr = ("ALTER TABLE modis_files ADD COLUMN %s %s;" % (cname,dtype))
    print querystr
    res = con1.query( querystr )
    idx = idx + 1
# print res
