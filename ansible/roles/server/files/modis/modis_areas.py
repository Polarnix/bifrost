#!/usr/bin/python

# Name:          modis_areas.py
# Purpose:       Class for defining MODIS area information.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os,sys

import osgeo.ogr as ogr
import osgeo.osr as osr

class modis_areas:

    def __init__( self, areaname, hemisphere, ps_rotate, sname, projection, polygeo ):
        self.name = areaname
        self.hem = hemisphere
        self.ps_rot = ps_rotate
        self.shortname = sname
        self.proj = projection
        self.geometry = polygeo

    # Calculate percentage overlap between are and another polygon geometry
    def calculate_overlap( self, projstr, wkt ):

        # Reconstruct the subset geometry
        subsetproj = osr.SpatialReference()
        subsetproj.ImportFromProj4( self.proj )
        subsetpoly = ogr.CreateGeometryFromWkt( self.geometry )
        # print self.geometry

        # Create a temporary geometry from MODIS coverage
        rawproj = osr.SpatialReference()
        rawproj.ImportFromProj4( projstr )
        projtrans = osr.CoordinateTransformation( rawproj, subsetproj )
        modiscover = ogr.CreateGeometryFromWkt( wkt )
        # print wkt
        modiscover.Transform( projtrans )

        # Calculate percentage area cover
        # if modiscover.Intersects( subsetpoly ):
        # For older version of GDAL
        if modiscover.Intersect( subsetpoly ) or subsetpoly.Within( modiscover):
            intersection = modiscover.Intersection( subsetpoly )
            totalarea = subsetpoly.GetArea()
            interarea = intersection.GetArea()
            overlap = (100.0 / totalarea) * interarea
            # print totalarea, interarea, overlap
            # print intersection
        else:
            overlap = 0.0

        return overlap

