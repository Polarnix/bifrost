#!/usr/bin/python

# Name:          check_data_storage.py
# Purpose:       Look at data directories and provide a total number of Gb stored in files
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys

def check_data_storage( path, ignorelist ):

    # Total size of storage
    total_size = 0

    # Walk through files and directories under path
    for root, dirs, files in os.walk(path, topdown=False):
        for name in files:
            # print(os.path.join(root, name))
            subdir = root.replace(path,'')
            ignoreflg = 0
            for ignoredir in ignorelist:
                if subdir.find(ignoredir) == 0:
                    ignoreflg = 1
            if ignoreflg == 0:
                # print subdir
                fn = os.path.join(root, name)
                size = (os.stat(fn)).st_size
                # print name, size
                total_size = total_size + size

    # Convert total size in bytes to Gb
    total_size = float(total_size) / (1024.0*1024.0*1024.0)

    return total_size




# Main program for running independently
if __name__ == '__main__':

    path = '/home/bifrostsat/Data/MODIS'
    ignorelist = [ '/NCEP', \
                   '/geoMeta/6', \
                   '/Arctic/Images', \
                   '/tmp' ]

    print check_data_storage( path, ignorelist )

