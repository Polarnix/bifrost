#!/usr/bin/python

# Name:          calculate_cover.py
# Purpose:       Calculate cloud coverage from cloud mask image.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import numpy as N

import osgeo.gdal as gdal
from osgeo.gdalconst import *


def calculate_cover( fname ):

    # Open cloud mask image file and load mask as an array
    cldmsk_ds = gdal.Open( fname, GA_ReadOnly )
    cldmsk_transform = cldmsk_ds.GetGeoTransform()
    cldmsk_b1 = cldmsk_ds.GetRasterBand(1)
    cldmsk = cldmsk_b1.ReadAsArray()
    cldmsk_ds = None

    # Get total area (km2)
    [ny,nx] = cldmsk.shape
    xres = N.abs( cldmsk_transform[1] )
    yres = N.abs( cldmsk_transform[5] )
    # print xres, yres, nx, ny
    total = nx * ny
    # print total
    # print N.min(cldmsk), N.max(cldmsk)

    # Get land area (km2)
    lndidx = N.nonzero( cldmsk == 255)
    # print lndidx[0]
    land = len(lndidx[0])
    # print land

    # Get clear sky area (km2)
    clridx = N.nonzero( cldmsk == 0)
    # print clridx[0]
    clear = len(clridx[0])
    # print clear

    # Calculate coverage percentage
    mult = 100.0 / (total - land)
    coverage = ((total - land) - clear) * mult
    # print coverage

    return coverage


