#!/bin/bash

# Name:          search_modis_meta.py
# Purpose:       Look for MODIS files covering a particular area within a date range
# Usage:         search_modis_meta.py satellite start_date end_date mode area threshold
#                  mode: D = day, N = Night, B = both, A = all
#                  e.g. python search_modis_meta.py aqua 20120101 20120131 D negreenlnd 100.0
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Look for MODIS files covering a particular area within a date range
# Usage: search_modis_meta.py satellite start_date end_date mode area threshold
#    mode: D = day, N = Night, B = both, A = all
# e.g. python search_modis_meta.py aqua 20120101 20120131 D negreenlnd 100.0
# Nick Hughes, 2015-vii-30

import os,sys
from os.path import join, getsize
from datetime import datetime, date, timedelta
import time
from ftplib import FTP
import argparse

import numpy as N

import osgeo.ogr as ogr
import osgeo.osr as osr

from load_subset_areas import load_subset_areas
from modis_areas import modis_areas
from modis_processing import cover_from_coords

BASEDIR = '/home/bifrostsat'
METADIR = '/home/bifrostsat/Data/MODIS/geoMeta/6'


# Search metadata files
def search_metadata( satellite, strdate, enddate, mode, area, threshold, delflg, lockfn ):

    # Check that end date is after start date
    if enddate < strdate:
        print "Start date cannot be before the end date."
        sys.exit()

    # Check satellite filename code
    satellite = satellite.upper()
    if satellite == 'AQUA':
        satcode = 'MYD'
    elif satellite == 'TERRA':
        satcode = 'MOD'
    else:
        print 'Unknown satellite.'
        sys.exit()
    
    # Set exceptions in OGR
    ogr.UseExceptions()
    
    # Load subset information from Shapefile
    shpfname = ("%s/Include/MODIS/modis_subsets_ll.shp" % BASEDIR)
    [nsubset,subset] = load_subset_areas( shpfname )
    
    # Find area definition
    areaflg = 0
    for i in range(nsubset):
        if area == subset[i].shortname:
            areaprojstr = subset[i].proj
            areawkt = subset[i].geometry
            areahem = subset[i].hem
            areaflg = 1
    # print area, areahem, areaflg
    if areaflg == 0:
        print ("Area %s not found. Stopping." % area)
        sys.exit()
    
    # Check threshold is sane
    if threshold < 0.0 or threshold > 100.0:
        print ("Threshold value of %f is not a percentage." % threshold)
        sys.exit()
    
    # Setup SRS for MODIS area
    areaproj = osr.SpatialReference()
    areaproj.ImportFromProj4( areaprojstr )
    
    # Setup SRS for longitude/latitude corodinates from metafile
    llproj = osr.SpatialReference()
    llproj.ImportFromEPSG( 4326 )
    
    # Coordinate transformation
    projtrans = osr.CoordinateTransformation( llproj, areaproj )
    
    # Setup area geometry object
    areageom = ogr.CreateGeometryFromWkt( areawkt )

    # Create lists for output data
    idlist = []
    dnlist = []
    dtlist = []
    hemlist = []
    coverlist = []
    
    # Loop through dates and determine which files cover the area
    curdate = strdate
    ftpopen = 0
    while curdate <= enddate:
        # print curdate
    
        # Construct metadata filename
        metafn = ("%s/%s/%d/%s03_%s.txt" % (METADIR,satellite,curdate.year,satcode,curdate))
        # print metafn

        # If delflg = 1, delete the file so that we are forced to download it again
        if delflg == 1 and os.path.isfile(metafn):
            os.remove(metafn)
    
        # Try to open file, if unsuccessful, then get it from ftp
        fstat = 0
        try:
            metafile = open( metafn, 'r' )
        except:
            # print 'Unable to open file'
            # Get file from ftp
            if ftpopen == 0:
                host = 'ladsweb.nascom.nasa.gov'
                username = 'anonymous'
                password = 'nick.hughes@met.no'
                ftpbasedir = 'allData/6'
                connectflg = 0
                retries = 9
                while connectflg == 0 and retries > 0:
                    try:
                        ftp = FTP( host, username, password )
                    except:
                        print 'Unable to make connection.'
                        retries = retries - 1
                        time.sleep(60)
                    else:
                        # print "Connected to ftp server."
                        ftpopen = 1
                        connectflg = 1
                if connectflg == 0:
                    print "Unable to make connection to FTP server, giving up for now."
                    if os.path.isfile( lockfn ):
                        os.remove( lockfn )
                    sys.exit()
    
            # Try to access directory for year on our server (create it if necessary)
            metapath = ("%s/%s/%d" % (METADIR,satellite,curdate.year))
            try:
                os.chdir( metapath  )
            except:
                os.makedirs( metapath )
    
            shortmetafn = ("%s03_%4d-%02d-%02d.txt" % (satcode,curdate.year,curdate.month,curdate.day))
            ftpdir = ("/geoMeta/6/%s/%4d" % (satellite,curdate.year) )
            dirflg = 1
            try:
                ftp.cwd( ftpdir )
            except:
                # print ("Unable to access FTP directory %s" % ftpdir)
                dirflg = 0
            # If we successfully access directory, try retrieving files
            if dirflg == 1:
                ftpcmd = ("RETR %s" % shortmetafn )
                # print ftpcmd
                try:
                    fmeta = open( metafn, 'wb' )
                    ftp.retrbinary( ftpcmd, fmeta.write )
                    fmeta.close()
                except:
                    # Remove failed attempt at getting the file
                    os.remove( metafn )
                    fstat = 0
                else:
                    metafile = open( metafn, 'r' )
                    fstat = 1
        else:
            fstat = 1
    
        # Read metadata file
        if fstat == 1:
            for tline in metafile:
                if tline[0:3] == satcode:
                    # print tline
                    metabits = tline.split(',')
                    GranuleID = metabits[0]
                    StartDateTime = metabits[1]
                    ArchiveSet = int( metabits[2] )
                    OrbitNumber = int( metabits[3] )
                    DayNightFlag = metabits[4]
                    EastBoundingCoord = float( metabits[5] )
                    NorthBoundingCoord = float( metabits[6] )
                    SouthBoundingCoord = float( metabits[7] )
                    WestBoundingCoord = float( metabits[8] )
                    GRingLongitude1 = float( metabits[9] )
                    GRingLongitude2 = float( metabits[10] )
                    GRingLongitude3 = float( metabits[11] )
                    GRingLongitude4 = float( metabits[12] )
                    GRingLatitude1 = float( metabits[13] )
                    GRingLatitude2 = float( metabits[14] )
                    GRingLatitude3 = float( metabits[15] )
                    GRingLatitude4 = float( metabits[16] )
    
                    # Create coverage
                    longlist = [ GRingLongitude1, GRingLongitude2, GRingLongitude3, GRingLongitude4, GRingLongitude1, ]
                    latlist = [ GRingLatitude1, GRingLatitude2, GRingLatitude3, GRingLatitude4, GRingLatitude1 ]
                    coverage = cover_from_coords( longlist, latlist )
                    coverwkt = coverage.ExportToWkt()
                    # print coverwkt
                    covergeom = ogr.CreateGeometryFromWkt( coverwkt )
    
                    # See if coverage is in Arctic (<50N) or Antarctic (>40S)
                    minlon, maxlon, minlat, maxlat = coverage.GetEnvelope()
                    # print minlat, maxlat
                    if N.max([minlat,maxlat]) > 50.0:
                        # print 'Arctic'
                        hem = 1
                    elif N.min([minlat,maxlat]) < -40.0:
                        # print 'Antarctic'
                        hem = -1
                    else:
                        # print 'Outside polar regions.'
                        hem = 0
    
                    # Only do more checks if image is within the right hemisphere for the area
                    if hem == areahem:
                        # Transform coordinates to area preojection (Polar Stereographic)
                        covergeom.Transform( projtrans )
    
                        # Calculate percentage overlap with area
                        overlap = 0.0
                        try:
                            # For versions of GDAl 2.0+
                            # if covergeom.Intersects( areageom ):
                            # For older versions of GDAL 1.7.0
                            if covergeom.Intersect( areageom ) or areageom.Within( covergeom):
                                try:
                                    intersection = covergeom.Intersection( areageom )
                                except:
                                    # print ("Problem with %s." % GranuleID)
                                    pass
                                else:
                                    if intersection != None and intersection.IsValid():
                                        totalarea = areageom.GetArea()
                                        interarea = intersection.GetArea()
                                        overlap = (100.0 / totalarea) * interarea
                        except:
                            # print ("Problem with %s." % GranuleID)
                            pass
    
                        # If overlap is equal or greater than threshold, output information
                        if mode == 'A' or mode == DayNightFlag:
                            if overlap >= threshold:
                                idlist.append( GranuleID )
                                dnlist.append( DayNightFlag )
                                dtlist.append( datetime.strptime(StartDateTime,'%Y-%m-%d %H:%M') )
                                hemlist.append( hem )
                                coverlist.append( overlap )
                                print ("%s %s %2d %6.2f" % (GranuleID, DayNightFlag, hem, overlap))
                    
            # Close metadata file
            metafile.close()
    
        # Close ftp connection (if open)
        if ftpopen == 1:
            ftp.quit()
            ftpopen = 0
    
        # Increment date
        curdate = curdate + timedelta(days=1)

    return [ idlist, dnlist, dtlist, hemlist, coverlist ]


# Make a date from string input
def mkdate(datestr):
    return datetime.strptime(datestr, '%Y%m%d').date()


# Main program wrapper
if __name__ == '__main__':

    # Load subset information from Shapefile
    shpfname = ("%s/Include/MODIS/modis_subsets_ll.shp" % BASEDIR)
    [nsubset,subset] = load_subset_areas( shpfname )
    shortnames = []
    for i in range(nsubset):
        shortnames.append( subset[i].shortname )

    # Get input arguments with argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("satellite", help="Name of MODIS satellite", choices={'aqua','terra'}, \
        type=str.lower)
    parser.add_argument("start_date", help="Start date (yyyymmdd)", type=mkdate)
    parser.add_argument("end_date", help="End date (yyyymmdd)", type=mkdate)
    parser.add_argument("type", help="Type of file (D=Day, N=Night, B=Both, A=All)", \
        choices={'D','N','B','A'}, type=str.upper)
    parser.add_argument("area", help="Processing area", choices=shortnames, type=str.lower)
    parser.add_argument("threshold", help="Threshold percentage for area covered", type=float)
    parser.add_argument("-d", "--delete", help="Delete metadata file (0=No,1=Yes)", choices={0,1}, \
        type=int, default=0)
    args = parser.parse_args()
    
    # Extract arguments from input
    satellite = (args.satellite).upper()
    start_date = args.start_date
    end_date = args.end_date
    mode = args.type
    area = args.area
    threshold = args.threshold
    delflg = args.delete

    # Search the metadata
    [ idlist, dnlist, dtlist, hemlist, coverlist ] = \
        search_metadata( satellite, start_date, end_date, mode, area, threshold, delflg, '' )

    # print idlist

