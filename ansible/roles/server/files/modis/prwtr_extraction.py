#!/usr/bin/python

# Name:          prwtr_extraction.py
# Purpose:       Get precipitable water vapour information from NCEP Reanalysis.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import datetime
import urllib2
from subprocess import call

import numpy as N
import numpy.ma as ma
# from Scientific.IO.NetCDF import NetCDFFile as Dataset
from netCDF4 import Dataset

import osgeo.gdal as gdal
import osgeo.osr as osr

BASEDIR = '/home/bifrostsat/Data/MODIS'
NCEPDIR = ("%s/NCEP" % BASEDIR)
TMPDIR = ("%s/tmp" % BASEDIR)

VAR = 'pr_wtr'


# Fetch NCEP Reanalysis precipitable water vapour file
def fetch_prwtr(year):

    ncfname = ("%s/pr_wtr.eatm.%4d.nc" % (NCEPDIR,year))
    url = ("ftp://ftp.cdc.noaa.gov/Datasets/ncep.reanalysis/surface/%s.eatm.%4d.nc" % (VAR,year))
    print 'Fetching', url
    req = urllib2.Request(url)
    aoerr = 0
    try:
        response = urllib2.urlopen(req)
    except urllib2.URLError, e:
        print '   Error retrieving file.'
        aoerr = 1
    else:
        # Save file to disk 
        ncout = open(ncfname,'w')
        ncout.write(response.read())
        response.close()
        ncout.close()

    return aoerr


# Extract dates and times from NetCDF, and check date is present
def check_datetime( checkdt, var_time ):

    # print checkdt
    var_dt = []
    # Original files were from hours from 0001-01-01 00:00:00
    # var_start = datetime.datetime( 1, 1, 1, 0, 0, 0 )
    var_start = datetime.datetime( 1800, 1, 1, 0, 0, 0 )
    i = 0
    idx = -9999
    for t in var_time:
        # Original files needed a 48-hour adjustment
        # tmpdt = var_start + datetime.timedelta(hours=(t-48))
        tmpdt = var_start + datetime.timedelta(hours=t)
        var_dt.append( tmpdt )
        # print i, t, tmpdt
        if tmpdt == checkdt:
            # Set index for array
            idx = i
            # print idx, t, tmpdt
        i = i + 1
    # print idx

    return [ idx, var_dt ]


# Extract precipitable water vapour values from NCEP Reanalysis files
def extract_prwtr( dtval ):

    # Flag to say if NetCDF file is open
    ncopen = 0
    count = 0
    while ncopen == 0 and count < 1:

        # Check that file for year exists, otherwise retrieve it from server
        ncfname = ("%s/pr_wtr.eatm.%4d.nc" % (NCEPDIR,dtval.year))
        # print ncfname
        try:
            ncfile = Dataset(ncfname,'r')
        except:
            # If we reach here, opening the file has failed so try to retrieve it with ftp
            aoerr = fetch_prwtr(dtval.year)
            if aoerr == 1:
                count = count + 1
        else:
            # Check to see if date is within file
            var_time = ncfile.variables['time'][:]
            [ idx, var_dt ] = check_datetime( dtval, var_time )
            if idx >= 0:
                # Sucessful open of ncfile and date present
                ncopen = 1
            else:
                print "    Date not present in NCEP Reanalysis pr_wtr data."
                ncfile.close()
                if dtval.year == (datetime.datetime.now()).year:
                    print "    Current year, so trying with downloading file again."
                    fetch_prwtr(dtval.year)
                    count = count + 1


    # If file open was successful, continue processing
    if ncopen == 1:

        # Get array longitude, latitude, and time limits
        # print ncfile.variables
        var_lon = ncfile.variables['lon'][:]
        var_lat = ncfile.variables['lat'][:]

        # Get Precipitable Water data and scale values
        var = ncfile.variables[VAR]
        # Old versions of these files used integer storage that required scaling, now floating point
        # var_array = ma.masked_equal(
        #     N.array( var[idx,:,:], dtype = N.float32),
        #     32767 )
        # var_array = (var_array * var.scale_factor) + var.add_offset
        var_array = ma.masked_less(
            N.array( var[idx,:,:], dtype = N.float32),
            -50.0 )
        [ nrow, ncol ] = var_array.shape
        # print nrow, ncol
        # print var_lon
        # print var_lat

        # Write as a GeoTIFF
        dst_fname = ("%s/%s_%4d%02d%02d_%02d0000.tif" % \
            (TMPDIR,VAR,dtval.year,dtval.month,dtval.day,dtval.hour))
        # print dst_fname
        driver = gdal.GetDriverByName( 'GTiff' )
        dst_ds = driver.Create( dst_fname, ncol, nrow, 1, gdal.GDT_Float32, \
            [ 'COMPRESS=LZW' ] )
        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(var_array,0,0)
        # UL x, w-e pixel resolution, rotation, UL y, rotation, n-s pixel resolution
        dst_ds.SetGeoTransform( [ var_lon[0]-1.25, 2.5, 0.0, var_lat[0]+1.25, 0.0, -2.5 ] )
        dst_srs = osr.SpatialReference()
        dst_srs.ImportFromEPSG( 4326 )
        dst_ds.SetProjection( dst_srs.ExportToWkt() )
        dst_ds = None

        # Close NetCDF file
        ncfile.close()

        status = 1
    else:
        status = -9999
        dst_fname = ''

    return [status,dst_fname]


# Interpolate precipitable water data values for time
def interpolate_prwtr( dtval ):

    # Calulate nearest NCEP time steps
    hour = int( N.trunc(dtval.hour / 6.0) * 6)
    dt1 = datetime.datetime( dtval.year, dtval.month, dtval.day, hour, 0, 0 )
    # print dt1
    dt2 = dt1 + datetime.timedelta( hours=6 )
    # print dt2

    # Create temporary files with data
    [status1,prwtr_fn1] = extract_prwtr( dt1 )
    [status2,prwtr_fn2] = extract_prwtr( dt2 )
    # print status1, status2

    # Only process if both status values are OK
    if status1 == 1 and status2 == 1:

        # Load data as arrays
        driver = gdal.GetDriverByName( 'GTiff' )
        driver.Register()
        # Dataset 1
        inds1 = gdal.Open( prwtr_fn1, gdal.GA_ReadOnly )
        band1 = inds1.GetRasterBand(1)
        data1 = band1.ReadAsArray().astype(N.float32)
        geotransform = inds1.GetGeoTransform()
        projection = inds1.GetProjection()
        band1 = None
        inds1 = None
        # Dataset 2
        inds2 = gdal.Open( prwtr_fn1, gdal.GA_ReadOnly )
        band2 = inds2.GetRasterBand(1)
        data2 = band2.ReadAsArray().astype(N.float32)
        band2 = None
        inds2 = None
        # print geotransform
        # print projection

        # Interpolate
        delta_t = (dtval - dt1).seconds / 3600.0
        delta_var = (data2 - data1) / 6.0
        outdata = data1 + (delta_t * delta_var)

        # Write as GeoTIFF file
        dst_fname = ("%s/%s_%4d%02d%02d_%02d%02d%02d.tif" % \
            (TMPDIR,VAR,dtval.year,dtval.month,dtval.day,dtval.hour,dtval.minute,dtval.second))
        # print dst_fname
        [ nrow, ncol ] = outdata.shape
        dst_ds = driver.Create( dst_fname, ncol, nrow, 1, gdal.GDT_Float32, \
            [ 'COMPRESS=LZW' ] )
        outband = dst_ds.GetRasterBand(1)
        outband.WriteArray(outdata,0,0)
        dst_ds.SetGeoTransform( geotransform )
        dst_ds.SetProjection( projection )
        dst_ds = None

        status = 1

    else:

        # Something went wrong, set status to bad
        status = -9999
        dst_fname = ''

    # Remove the two component data files
    if status1 == 1:
        os.remove( prwtr_fn1 )
    if status2 == 1:
        os.remove( prwtr_fn2 )

    return [status,dst_fname]


# Reproject precipitable water vapour image
def reproject_prwtr( projsrc_fn, data_fn ):

    # /disk1/nicholsh/IGS2014/MODIS/Arctic/GeoTIFF/modis_negreenlnd_20130930_1310_terra_ch2.tif
    status = 0

    # Open projected file and get map projection and geotransform information
    try:
        inds = gdal.Open( projsrc_fn, gdal.GA_ReadOnly )
    except:
        print 'Unable to open file'
        status = -9999
    else:
        ncol = inds.RasterXSize
        nrow = inds.RasterYSize
        # print ncol, nrow
        geotransform = inds.GetGeoTransform()
        projection = inds.GetProjection()
        inds = None
        status = 1

    # Try to open data_fn and set a status error if this fails
    if os.path.isfile( data_fn ):
        status = 1
    else:
        status = -9998
        output_fn = None

    # Only process more if status is 1
    if status == 1:

        ulx = geotransform[0]
        uly = geotransform[3]
        pixW = geotransform[1]
        pixH = geotransform[5]
        lrx = ulx + ( ncol * pixW )
        lry = uly + ( nrow * pixH )
        # print ulx, uly
        # print lrx, lry
        # Expect -699900,  -749699
        #            100  -1700099
        srs = osr.SpatialReference()
        srs.ImportFromWkt( projection )
        projstr = srs.ExportToProj4()
        # print projstr

        # Filename extraction
        # print data_fn
        fnbits = data_fn.split('/')
        # print fnbits
        for i in range(len(fnbits)-1):
            if i == 0:
                path = ("%s" % fnbits[i])
            else:
                path = ("%s/%s" % (path,fnbits[i]))
        # print path
        rootfn = fnbits[-1][:-4]
        # print rootfn

        # Output filename
        output_fn = ("%s/%s_projected.tif" % (path,rootfn))
        # print output_fn
        # Delete file if it exists
        if os.path.isfile( output_fn ):
            os.remove( output_fn )

        # Create command to reproject using gdalwarp        
        cmd = ("/usr/bin/gdalwarp -of \"GTiff\" -t_srs \"%s\"" % projstr)
        cmd = ("%s -te %f %f %f %f -tr %f %f" % (cmd,ulx,lry,lrx,uly,pixW,pixH))
        cmd = ("%s -r bilinear -q %s %s" % (cmd,data_fn,output_fn))
        # print cmd
        try:
            retcode = call( cmd, shell=True )
            if retcode < 0:
                print >>sys.stderr, "Child was terminated by signal", -retcode
            else:
                # print >>sys.stderr, "Child returned", retcode
                status = 2
        except OSError, e:
            print >>sys.stderr, "Execution failed:", e

    return [status,output_fn]


# Load precipitable water vapour as an array
def load_prwtr( data_fn ):

    # Load data as array
    driver = gdal.GetDriverByName( 'GTiff' )
    driver.Register()
    inds = gdal.Open( data_fn, gdal.GA_ReadOnly )
    band = inds.GetRasterBand(1)
    data = band.ReadAsArray().astype(N.float32)
    band = None
    inds = None

    return data
