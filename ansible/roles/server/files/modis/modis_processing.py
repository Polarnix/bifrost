#!/usr/bin/python

# Name:          modis_processing.py
# Purpose:       Routines for processing MODIS data files..
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, socket, subprocess
import datetime
import tarfile
from ftplib import FTP
import glob

import numpy as N

import osgeo.gdal as gdal
import osgeo.ogr as ogr
import osgeo.osr as osr
from osgeo.gdalconst import *

from modis_areas import modis_areas

PROGDIR = '/home/bifrostsat/Python/MODIS'
BASEDIR = '/home/bifrostsat/Data/MODIS'
TMPDIR = ("%s/tmp" % BASEDIR)

# Set raw longitude/latitude projection
rawprojstr = '+proj=longlat +ellps=WGS84'
rawproj = osr.SpatialReference()
rawproj.ImportFromProj4( rawprojstr )

# 22 reflectance channels (13 and 14 have dual calibrations)
# 16 emissive channels
bandnames = [ '1', '2', '3', '4', '5', '6', '7', \
    '8', '9', '10', '11', '12', '13lo', '13hi', \
    '14lo', '14hi', '15', '16', '17', '18', '19', '26', \
    '20', '21', '22', '23', '24', '25', '27', '28', \
    '29', '30', '31', '32', '33', '34', '35', '36' ]

# Band limits from http://modis.gsfc.nasa.gov/about/specifications.php
lower = N.array( [ 620, 841, 459, 545, 1230, 1628, 2105, \
    405, 438, 483, 526, 546, 662, 662, \
    673, 673, 743, 862, 890, 931, 915, 1.36, \
    3.66, 3.929, 3.929, 4.02, 4.433, 4.482, 6.535, 7.175, \
    8.4, 9.58, 10.78, 11.77, 13.185, 13.485, 13.785, 14.085 ], \
    dtype=N.float32 )
upper = N.array( [ 670, 876, 479, 565, 1250, 1652, 2155, \
    420, 448, 493, 536, 556, 672, 672, \
    683, 683, 753, 877, 920, 941, 965, 1.39, \
    3.84, 3.989, 3.989, 4.08, 4.498, 4.549, 6.895, 7.475, \
    8.7, 9.88, 11.28, 12.27, 13.485, 13.785, 14.085, 14.385 ], \
    dtype=N.float32 )
# print lower[0:21]
# print lower[21:39]
# print lower.shape, upper.shape
lower[0:21] = lower[0:21] * N.power(10.0,-9)
lower[21:39] = lower[21:39] * N.power(10.0,-6)
# print lower
upper[0:21] = upper[0:21] * N.power(10.0,-9)
upper[21:39] = upper[21:39] * N.power(10.0,-6)
lambdas = 0.5 * (lower + upper)



# Subroutine to load area data from file
def load_subset_areas( shpfname ):

    # Load subset area data
    subset = []
    # shpfname = ("%s/include/modis_subsets_ll.shp" % PROGDIR)
    ds = ogr.Open( shpfname )
    lyr = ds.GetLayerByName( 'modis_subsets_ll' )
    lyr.ResetReading()
    for feat in lyr:
        feat_defn = lyr.GetLayerDefn()
        for i in range(feat_defn.GetFieldCount()):
            field_defn = feat_defn.GetFieldDefn(i)
            # print dir(field_defn)
            # print field_defn.name, feat.GetField(i)
            if field_defn.name == 'name':
                areaname = feat.GetField(i)
            elif field_defn.name == 'hem':
                hemisphere = feat.GetField(i)
            elif field_defn.name == 'PS_rotatio':
                rotation = feat.GetField(i)
            elif field_defn.name == 'shortname':
                shortname = feat.GetField(i)
        # print areaname, hemisphere, rotation, shortname

        # Projection string according to hemisphere and rotation
        if rotation >= 0:
            rotstr = ("%de" % rotation)
        else:
            if rotation == -999:
                rotstr = 'void'
            else:
                rotstr = ("%dw" % rotation)
        if hemisphere == 1:
            if rotation != -999:
                projstr = ("+proj=stere +lat_0=90n +lon_0=%s +lat_ts=90n +ellps=WGS84" % rotstr)
            else:
                projstr = ("+proj=merc +lon_0=0 +k=1 +datum=WGS84 +units=m +no_defs")
        elif hemisphere == -1:
            if rotation != -999:
                projstr = ("+proj=stere +lat_0=90s +lon_0=%s +lat_ts=90s +ellps=WGS84" % rotstr)
            else:
                projstr = ("+proj=merc +lon_0=0 +k=1 +datum=WGS84 +units=m +no_defs" % rotstr)
        else:
            print 'Unknown hemisphere.'
            sys.exit()
        # print projstr
        projection = osr.SpatialReference()
        projection.ImportFromProj4( projstr )

        # Set coordinate transformation
        projtrans = osr.CoordinateTransformation( rawproj, projection )

        # Get the geometry
        geometry = feat.GetGeometryRef()
        # print geometry
        geometry.Transform( projtrans )
        # print geometry
        newwkt = geometry.ExportToWkt()
        # print newwkt

        areaval = modis_areas( areaname, hemisphere, rotation, shortname, \
            projstr, newwkt )
        subset.append( areaval )

    ds = None

    # print subset
    nsubset = len(subset)
    # print nsubset

    return [nsubset, subset]


def modis_cover( filename ):

    # Get coordimates from metadata file
    fmeta = open( filename, 'r' )
    longflg = 0
    latflg = 0
    seqflg = 0
    nlong = -9999
    nlat = -9999
    nseq = -9999
    for tline in fmeta:
        # print tline
        # Find start of metadata objects
        if tline.find( 'OBJECT                 = GRINGPOINTLONGITUDE' ) != -1:
            longflg = 1
        elif tline.find( 'OBJECT                 = GRINGPOINTLATITUDE' ) != -1:
            latflg = 1
        elif tline.find( 'OBJECT                 = GRINGPOINTSEQUENCENO' ) != -1:
            seqflg = 1

        # Get number of points in coordinate list
        if longflg == 1 and tline.find( 'NUM_VAL' ) != -1:
            bits = tline.split()
            # print bits
            nlong = int( bits[-1] )
            # print nlong
        elif latflg == 1 and tline.find( 'NUM_VAL' ) != -1:
            bits = tline.split()
            # print bits
            nlat = int( bits[-1] )
            # print nlat
        elif seqflg == 1 and tline.find( 'NUM_VAL' ) != -1:
            bits = tline.split()
            # print bits
            nseq = int( bits[-1] )
            # print nseq

        # Get values in coordinate list
        if longflg == 1 and tline.find( 'VALUE' ) != -1:
            bits = tline.split()
            # print bits
            values = []
            for i in range(nlong,0,-1):
                valstr = bits[len(bits)-i]
                valstr = valstr.translate( None, '(),' )
                values.append( float(valstr) )
                # print i, bits[len(bits)-i], valstr
            # print values
            longitude = N.array( values, dtype=N.float64 )
            # print longitude
        elif latflg == 1 and tline.find( 'VALUE' ) != -1:
            bits = tline.split()
            # print bits
            values = []
            for i in range(nlat,0,-1):
                valstr = bits[len(bits)-i]
                valstr = valstr.translate( None, '(),' )
                values.append( float(valstr) )
                # print i, bits[len(bits)-i], valstr
            # print values
            latitude = N.array( values, dtype=N.float64 )
            # print latitude
        elif seqflg == 1 and tline.find( 'VALUE' ) != -1:
            bits = tline.split()
            # print bits
            values = []
            for i in range(nseq,0,-1):
                valstr = bits[len(bits)-i]
                valstr = valstr.translate( None, '(),' )
                values.append( float(valstr) )
                # print i, bits[len(bits)-i], valstr
            sequence = N.array( values, dtype=N.uint8 )
            # print sequence

        if longflg == 1 and tline.find( 'END_OBJECT' ) != -1:
            longflg = 0
        elif latflg == 1 and tline.find( 'END_OBJECT' ) != -1:
            latflg = 0
        elif seqflg == 1 and tline.find( 'END_OBJECT' ) != -1:
            seqflg = 0

        totalflags = longflg + latflg + seqflg
        if totalflags > 1:
            print 'Too many objects.'
            sys.exit()

    fmeta.close()

    # Sort coordinates according to sequence
    longlist = []
    latlist = []
    for i in range(nseq):
        for j in range(nseq):
            if sequence[j] == i + 1:
                idx = sequence[i] - 1
                # print idx, longitude[idx], latitude[idx]
                longlist.append( longitude[idx] )
                latlist.append( latitude[idx] )
    longlist.append( longlist[0] )
    latlist.append( latlist[0] )
    longlist = N.array( longlist, dtype=N.float64 )
    latlist = N.array( latlist, dtype=N.float64 )
    # print longlist
    # print latlist

    # Construct polygon geometry, adding extra points along sides for reprojection
    Eradius = 6366710   # Earth radius in metres
    wkt = 'POLYGON(('
    for i in range(nseq):
        # print i, longlist[i], latlist[i]
        lon1 = N.radians( longlist[i] )
        lat1 = N.radians( latlist[i] )
        lon2 = N.radians( longlist[i+1] )
        lat2 = N.radians( latlist[i+1] )
        d = 2.0 * N.arcsin( N.sqrt( \
            N.power( N.sin((lat1-lat2)/2.0), 2.0) + \
            N.cos(lat1) * N.cos(lat2) * \
            N.power( N.sin((lon1-lon2)/2.0), 2.0) ) )
        # print d*Eradius
        npts = 10
        for j in range(npts):
            f = (1.0 / float(npts)) * float(j)
            # print '  ', j, f
            A = N.sin((1-f)*d) / N.sin(d)
            B = N.sin(f*d) / N.sin(d)
            x = (A * N.cos(lat1) * N.cos(lon1)) +  (B * N.cos(lat2) * N.cos(lon2))
            y = (A * N.cos(lat1) * N.sin(lon1)) +  (B * N.cos(lat2) * N.sin(lon2))
            z = (A * N.sin(lat1))               +  (B * N.sin(lat2))
            lat = N.degrees( N.arctan2( z, N.sqrt( (x*x) + (y*y) ) ) )
            lon = N.degrees( N.arctan2( y, x) )
            # print '  ', j, f, lon, lat
            wkt = ("%s%.10f %.10f," % (wkt,lon,lat) )
    wkt = ("%s%.10f %.10f))" % (wkt,longlist[nseq],latlist[nseq]) )
    # print wkt

    # Create OGR geometry
    coverage = ogr.CreateGeometryFromWkt( wkt )
    # print coverage

    return coverage


def cover_from_coords( longlist, latlist ):
    nseq = len(longlist) - 1

    # Construct polygon geometry, adding extra points along sides for reprojection
    Eradius = 6366710   # Earth radius in metres
    wkt = 'POLYGON(('
    for i in range(nseq):
        # print i, longlist[i], latlist[i]
        lon1 = N.radians( longlist[i] )
        lat1 = N.radians( latlist[i] )
        lon2 = N.radians( longlist[i+1] )
        lat2 = N.radians( latlist[i+1] )
        d = 2.0 * N.arcsin( N.sqrt( \
            N.power( N.sin((lat1-lat2)/2.0), 2.0) + \
            N.cos(lat1) * N.cos(lat2) * \
            N.power( N.sin((lon1-lon2)/2.0), 2.0) ) )
        # print d*Eradius
        npts = 10
        for j in range(npts):
            f = (1.0 / float(npts)) * float(j)
            # print '  ', j, f
            A = N.sin((1-f)*d) / N.sin(d)
            B = N.sin(f*d) / N.sin(d)
            x = (A * N.cos(lat1) * N.cos(lon1)) +  (B * N.cos(lat2) * N.cos(lon2))
            y = (A * N.cos(lat1) * N.sin(lon1)) +  (B * N.cos(lat2) * N.sin(lon2))
            z = (A * N.sin(lat1))               +  (B * N.sin(lat2))
            lat = N.degrees( N.arctan2( z, N.sqrt( (x*x) + (y*y) ) ) )
            lon = N.degrees( N.arctan2( y, x) )
            # print '  ', j, f, lon, lat
            wkt = ("%s%.10f %.10f," % (wkt,lon,lat) )
    wkt = ("%s%.10f %.10f))" % (wkt,longlist[nseq],latlist[nseq]) )
    # print wkt

    # Create OGR geometry
    coverage = ogr.CreateGeometryFromWkt( wkt )
    # print coverage

    return coverage


def get_modisfile( ftp, socket, ftpbasedir, ftype, satcode, \
    year, jday, hour, minute, tmpdir ):

    if ftype == 'GEO':
        ftpdir = ("/%s/%s03/Recent" % (ftpbasedir,satcode) )
        searchstr = ("%s03.A%4d%03d.%02d%02d" % \
            (satcode,year,jday,hour,minute) )
    else:
        ftpdir = ("/%s/%s02%s/Recent" % (ftpbasedir,satcode,ftype) )
        searchstr = ("%s02%s.A%4d%03d.%02d%02d" % \
            (satcode,ftype,year,jday,hour,minute) )
    # print ftpdir
    # print searchstr
    ftplist = ftp.nlst( ftpdir )

    # Find full filename to retrieve
    target = ''
    count = 0
    for fname in ftplist:
        if fname[-4:] == '.hdf' and fname.find( searchstr ) != -1:
            target = fname
            count = count + 1
    # print count

    # Get file from ftp server and put in temporary directory
    if count > 0:
        # print target
        modisfname = ("%s/%s" % (tmpdir,target.split('/')[-1]) )
        ftpcmd = ("RETR %s" % target )
        # print modisfname

        try:
            fmodis = open( modisfname, 'wb' )
            ftp.cwd( ftpdir )
            size = ftp.size(target)   # Get size of file
            mbsize = float(size) / 1024.0 / 1024.0
            print ("%50s : %8.2f Mb" % (target.split('/')[-1],mbsize))
            ftp.retrbinary( ftpcmd, fmodis.write )
            fmodis.close()
        except socket.timeout:
            print "There was a timeout"
            flg = -1
        except socket.error:
            print "There was some other socket error"
            status = -2
        else:
            flg = 1
    else:
        flg = 0
        modisfname = ''
        size = -1

    # Delete part-file of we have status -1
    if flg == -1 or flg == -2:
        os.remove( modisfname )

    # print flg
    return [flg,modisfname,size]


def get_archivefile( ftp, socket, ftpbasedir, ftype, satcode, \
    year, jday, hour, minute, tmpdir ):

    # Retrieval URL's are like ftp://ladsftp.nascom.nasa.gov/allData/5/MYD021KM/2004/214/MYD021KM.A2004214.0130.005.2009213000629.hdf

    if ftype == 'GEO':
        ftpdir = ("/%s/%s03/%4d/%03d" % (ftpbasedir,satcode,year,jday) )
        searchstr = ("%s03.A%4d%03d.%02d%02d" % \
            (satcode,year,jday,hour,minute) )
    elif ftype == 'CLOUD':
        ftpdir = ("/%s/%s35_L2/%4d/%03d" % (ftpbasedir,satcode,year,jday) )
        searchstr = ("%s35_L2.A%4d%03d.%02d%02d" % \
            (satcode,year,jday,hour,minute) )        
    else:
        ftpdir = ("/%s/%s02%s/%4d/%03d" % (ftpbasedir,satcode,ftype,year,jday) )
        searchstr = ("%s02%s.A%4d%03d.%02d%02d" % \
            (satcode,ftype,year,jday,hour,minute) )
    # print 'searchstr', searchstr
    ftplist = ftp.nlst( ftpdir )
    # print ftplist

    # Find full filename to retrieve
    target = ''
    count = 0
    for fname in ftplist:
        # print fname
        if fname[-4:] == '.hdf' and fname.find( searchstr ) != -1:
            target = fname
            count = count + 1
    # print count
    # print target

    # Get file from ftp server and put in temporary directory
    if count > 0:
        # print target
        modisfname = ("%s/%s" % (tmpdir,target.split('/')[-1]) )
        ftpcmd = ("RETR %s" % target )

        try:
            fmodis = open( modisfname, 'wb' )
            ftp.cwd( ftpdir )
            size = ftp.size(target)   # Get size of file
            mbsize = float(size) / 1024.0 / 1024.0
            print ("%50s : %8.2f Mb" % (target.split('/')[-1],mbsize))
            ftp.retrbinary( ftpcmd, fmodis.write )
            fmodis.close()
        except socket.timeout:
            print "There was a timeout"
            flg = -1
        except socket.error:
            print "There was some other socket error"
            status = -2
        else:
            flg = 1
    else:
        flg = 2
        modisfname = ''
        size = -1

    # Delete part-file of we have status -1
    if flg == -1 or flg == -2:
        os.remove( modisfname )

    # print flg
    return [flg,modisfname,size]


def get_coverage_limits(coverage):

    # Get coordinate limits of subset, making sure they are divisble by 800 metres
    xmin, xmax, ymin, ymax = coverage.GetEnvelope()
    xmin = int(xmin)
    xmax = int(xmax)
    # print xmin, xmax
    if (xmax-xmin) % 800 != 0:
        # print (xmax-xmin) / 800, (xmax-xmin) % 800
        nx = int((xmax-xmin) / 800) + 1
        xmax = xmin + (nx * 800)
        # print nx, xmax
    else:
        nx = int((xmax-xmin) / 800)
    ymin = int(ymin)
    ymax = int(ymax)
    # print ymin, ymax
    if (ymax-ymin) % 800 != 0:
        # print (ymax-ymin) / 800, (ymax-ymin) % 800
        ny = int((ymax-ymin) / 800) + 1
        ymax = ymin + (ny * 800)
        # print nx, ymax
    else:
        ny = int((ymax-ymin) / 800)
    # print coverage
    # print xmin, xmax
    # print ymin, ymax
    return [xmin, xmax, nx, ymin, ymax, ny]


def MRTSwath_process( satellite, dt, filedt, subset, itype ):
    # print satellite, filedt

    # Set filenames
    dtstr = dt.strftime('%Y%m%d_%H%M')
    if satellite == 'MOD':
        satname = 'terra'
    elif satellite == 'MYD':
        satname = 'aqua'
    else:
        print 'Unknown satellite: ', satellite
        sys.exit()
    # geofname = ("%s/%s03.%s.005.NRT.hdf" % (TMPDIR,satellite,filedt) )
    # print ("%s/%s03.%s.006.*.hdf" % (TMPDIR,satellite,filedt))
    geofname = glob.glob( ("%s/%s03.%s.006.*.hdf" % (TMPDIR,satellite,filedt)) )[0]
    cloudfname = glob.glob( ("%s/%s35_L2.%s.006.*.hdf" % (TMPDIR,satellite,filedt)) )[0]
    # print geofname
    if itype == 'GEO' or itype == 'MSK':
        hdffname = geofname
    elif itype == 'CLOUD':
        hdffname = cloudfname
    else:
        # hdffname = ("%s/%s02%s.%s.005.NRT.hdf" % (TMPDIR,satellite,itype,filedt) )
        hdffname = glob.glob( ("%s/%s02%s.%s.006.*.hdf" % (TMPDIR,satellite,itype,filedt)) )[0]
        # print hdffname
    if subset.hem == 1:
        lat0 = 90
    elif subset.hem == -1:
        lat0 = -90
    else:
        print 'Unknown hemisphere: ', subset.hem
        sys.exit()
    # prmfname = ("%s/%s.%s.%s.%s.prm" % (TMPDIR,subset.shortname,satellite,itype,filedt) )
    prmfname = ("%s/modis_%s_%s_%s_%s.prm" % (TMPDIR,subset.shortname,dtstr,satname,itype.lower()) )
    outfname = ("%s/modis_%s_%s_%s.tif" % (TMPDIR,subset.shortname,dtstr,satname) )
    txtfname = ("%s/modis_%s_%s_%s_%s.txt" % (TMPDIR,subset.shortname,dtstr,satname,itype.lower()) )
    metafname = ("%s/modis_%s_%s_%s_%s_1B_meta.txt" % (TMPDIR,subset.shortname,dtstr,satname,itype.lower()) )
    # logfname = ("%s/%s.%s.%s.log" % (TMPDIR,satellite,itype,filedt) )
    logfname = ("%s/modis_%s_%s_%s_%s.log" % (TMPDIR,subset.shortname,dtstr,satname,itype.lower()) )
    # print geofname
    # print hdffname
    # print prmfname
    # print outfname

    # Set map projection and limits
    projection = osr.SpatialReference()
    projection.ImportFromProj4( subset.proj )
    # Set coordinate transformation
    projtrans = osr.CoordinateTransformation( rawproj, projection )
    coverage = ogr.CreateGeometryFromWkt( subset.geometry )
    # coverage.Transform( projtrans )

    # Get coordinate limits of subset, making sure they are divisble by 800 metres
    # xmin, xmax, ymin, ymax = coverage.GetEnvelope()
    # xmin = int(xmin)
    # xmax = int(xmax)
    # # print xmin, xmax
    # if (xmax-xmin) % 800 != 0:
    #     # print (xmax-xmin) / 800, (xmax-xmin) % 800
    #     nx = int((xmax-xmin) / 800) + 1
    #     xmax = xmin + (nx * 800)
    #     # print nx, xmax
    # ymin = int(ymin)
    # ymax = int(ymax)
    # # print ymin, ymax
    # if (ymax-ymin) % 800 != 0:
    #     # print (ymax-ymin) / 800, (ymax-ymin) % 800
    #     ny = int((ymax-ymin) / 800) + 1
    #     ymax = ymin + (ny * 800)
    #     # print nx, ymax
    # # print coverage
    # # print xmin, xmax
    # # print ymin, ymax
    [ xmin, xmax, nx, ymin, ymax, ny ] = get_coverage_limits(coverage)

    kernel = 'BI'   # Normally CC
    if itype == 'QKM':
        imgres = 200
        outputs = 'EV_250_RefSB, 1, 1; EV_250_RefSB_Uncert_Indexes, 1, 1'
    elif itype == 'HKM':
        imgres = 400
        outputs = 'EV_500_RefSB, 1, 1, 1, 1, 1; EV_500_RefSB_Uncert_Indexes, 1, 1, 1, 1, 1; EV_250_Aggr500_RefSB, 1, 1; EV_250_Aggr500_RefSB_Uncert_Indexes, 1, 1; EV_250_Aggr500_RefSB_Samples_Used, 1, 1'
    elif itype == '1KM':
        imgres = 800
        outputs = 'EV_1KM_RefSB, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1; EV_1KM_RefSB_Uncert_Indexes, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1; EV_1KM_Emissive, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1; EV_1KM_Emissive_Uncert_Indexes, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1; EV_250_Aggr1km_RefSB, 1, 1; EV_250_Aggr1km_RefSB_Uncert_Indexes, 1, 1; EV_250_Aggr1km_RefSB_Samples_Used, 1, 1; EV_500_Aggr1km_RefSB, 1, 1, 1, 1, 1; EV_500_Aggr1km_RefSB_Uncert_Indexes, 1, 1, 1, 1, 1; EV_500_Aggr1km_RefSB_Samples_Used, 1, 1, 1, 1, 1; EV_Band26; EV_Band26_Uncert_Indexes'
    elif itype == 'GEO':
        imgres = 200
        outputs = 'Height; SensorZenith; SensorAzimuth; Range; SolarZenith; SolarAzimuth; gflags'
    elif itype == 'MSK':
        imgres = 200
        outputs = 'Land/SeaMask'
        kernel = 'NN'
    elif itype == 'CLOUD':
        imgres = 800
        outputs = 'Cloud_Mask; Scan_Start_Time'
        kernel = 'NN'
    else:
        print 'Unknown image type:', itype
        sys.exit()

    # Write .prm settings file for MRTSwath
    prmout = open( prmfname, 'w' )
    prmout.write( ("\nINPUT_FILENAME = %s\n\n" % hdffname) )
    prmout.write( ("GEOLOCATION_FILENAME = %s\n\n" % geofname) )
    prmout.write( ("INPUT_SDS_NAME = %s\n\n" % outputs) )
    prmout.write( ("OUTPUT_SPATIAL_SUBSET_TYPE = PROJ_COORDS\n") )
    prmout.write( ("OUTPUT_SPACE_UPPER_LEFT_CORNER (X Y) = %d.0 %d.0\n" % (xmin,ymax)) )
    prmout.write( ("OUTPUT_SPACE_LOWER_RIGHT_CORNER (X Y) = %d.0 %d.0\n\n" % (xmax,ymin)) )
    prmout.write( ("OUTPUT_FILENAME = %s\n" % outfname) )
    prmout.write( ("OUTPUT_FILE_FORMAT = GEOTIFF_FMT\n\n") )
    prmout.write( ("KERNEL_TYPE (CC/BI/NN) = %s\n\n" % kernel) )
    if subset.ps_rot != -999:
        prmout.write( ("OUTPUT_PROJECTION_NUMBER = PS\n\n") )
        prmout.write( ("OUTPUT_PROJECTION_PARAMETER = 0.0 0.0 0.0 0.0 %d.0 %d.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0\n\n" % (subset.ps_rot,lat0)) )
    else:
        prmout.write( ("OUTPUT_PROJECTION_NUMBER = MERCAT\n\n") )
        prmout.write( ("OUTPUT_PROJECTION_PARAMETER = 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0\n\n") )
    prmout.write( ("OUTPUT_PROJECTION_SPHERE = 8\n\n") )
    prmout.write( ("OUTPUT_PIXEL_SIZE = %d.0\n" % imgres) )
    prmout.close()

    # Call MRTSwath to generate GeoTIFF images
    cmd = "export MRTSWATH_HOME=\"/usr/local/MRTSwath\""
    cmd = ("%s; export PATH=\"$PATH:/usr/local/MRTSwath/bin\"" % cmd)
    cmd = ("%s; export MRTSWATH_DATA_DIR=\"/usr/local/MRTSwath/data\"" % cmd)
    cmd = ( "%s; /usr/local/MRTSwath/bin/swath2grid -pf=%s > %s" % (cmd,prmfname,logfname))
    # print cmd
    try:
        retcode = subprocess.call(cmd,shell=True)
        if retcode < 0:
           print >>sys.stderr, "Child was terminated by signal", -retcode
        # else:
        #     print >>sys.stderr, "Child returned", retcode
    except OSError, e:
        print >>sys.stderr, "Execution failed:", e

    # Use hdf4-tools 'ncdump-hdf' and 'hdp' to extract metadata
    if itype == 'QKM' or itype == 'HKM' or itype == '1KM':
        cmd = ( "/usr/bin/ncdump-hdf -h %s > %s" % (hdffname,txtfname))
        # print cmd
        try:
            hdf1ret = subprocess.call(cmd, shell=True)
            if hdf1ret < 0:
                print >>sys.stderr, "Child was terminated by signal", -hdf1ret
            # else:
            #     print >>sys.stderr, "Child returned", hdf1ret
        except OSError, e:
            print >>sys.stderr, "Execution failed:", e
        cmd = ( "/usr/bin/hdp dumpvd -n 'Level 1B Swath Metadata' %s > %s" % (hdffname,metafname))
        # print cmd
        try:
            hdf2ret = subprocess.call(cmd, shell=True)
            if hdf2ret < 0:
                print >>sys.stderr, "Child was terminated by signal", -hdf2ret
            # else:
            #     print >>sys.stderr, "Child returned", hdf1ret
        except OSError, e:
            print >>sys.stderr, "Execution failed:", e

    return retcode


def scale_image_8bit( data, nulldata, nstd ):

    idx1 = N.nonzero( data != nulldata )
    idx2 = N.nonzero( data == nulldata )
    avg = N.average( data[idx1] )
    std = N.std( data[idx1] )
    # print data[idx1].min(), data[idx1].max(), avg, std

    new1min = avg - (nstd * std)
    if new1min < 0:
        new1min = 0.0
    new1max = avg + (nstd * std)
    # print new1min, new1max
    data[idx1] = (data[idx1] - new1min) * (250 / (new1max - new1min) )
    idx3 = N.nonzero( data > 250 )
    data[idx3] = 250
    data[idx2] = 255
    idx4 = N.nonzero( data < 0 )
    data[idx4] = 0
    data = N.array( data, dtype=N.uint8 )

    return data


# Function to enlarge a 2D array
def enlarge(a, x=2, y=None):
    """Enlarges 2D image array a using simple pixel repetition in both dimensions.
    Enlarges by factor x horizontally and factor y vertically.
    If y is left as None, uses factor x for both dimensions."""
    a = N.asarray(a)
    assert a.ndim == 2
    if y == None:
        y = x
    for factor in (x, y):
        assert factor.__class__ == int
        assert factor > 0
    return a.repeat(y, axis=0).repeat(x, axis=1)


# Generate colour composite from 3 channels of MODIS data
def modis_composite( inp1fname, inp2fname, inp3fname, outfname ):
    print outfname

    nulldata = 65535
    nstd = 3.0

    driver = gdal.GetDriverByName( 'GTiff' )

    filelist = [ inp1fname, inp2fname, inp3fname ]
    band = 1
    for fname in filelist:
        # print fname
        inds = gdal.Open( fname )
        imgnx = inds.RasterXSize
        imgny = inds.RasterYSize
        # print imgnx, imgny
        imgproj = inds.GetProjectionRef()
        imgtrans = inds.GetGeoTransform()
        # print imgtrans
        xorigin = imgtrans[0]
        yorigin = imgtrans[3]
        imgres = imgtrans[1]
        # Get data and scale it to 8-bit
        img = inds.GetRasterBand(1).ReadAsArray()
        img = scale_image_8bit( img, nulldata, nstd )
        idx = N.nonzero( img != 255 )
        # print img[idx].min(), img[idx].max(), N.average( img[idx] ), N.std( img[idx] )
        inds = None

        if imgres == 400.0:
            img = enlarge(img, x=2, y=2)
            imgres = 200.0
            imgtrans = (xorigin,imgres,0.0,yorigin,0.0,-imgres)
            imgnx = img.shape[1]
            imgny = img.shape[0]
        elif imgres == 800.0:
            img = enlarge(img, x=4, y=4)
            imgres = 200.0
            imgtrans = (xorigin,imgres,0.0,yorigin,0.0,-imgres)
            imgnx = img.shape[1]
            imgny = img.shape[0]
        # print imgtrans
        # print img.shape

        # If the first band, create the new image file
        if band == 1:
            # print imgnx, imgny
            outds = driver.Create( outfname, imgnx, imgny, 3, gdal.GDT_Byte, \
                [ 'COMPRESS=JPEG', 'JPEG_QUALITY=75', 'PHOTOMETRIC=YCBCR' ] )
            #     [ 'COMPRESS=JPEG', 'JPEG_QUALITY=75' ] )
            outds.SetGeoTransform( imgtrans )
            outds.SetProjection( imgproj )

        outds.GetRasterBand(band).WriteArray( img )
        outds.GetRasterBand(band).SetNoDataValue( 255 )
        band = band + 1

    outds = None

    retcode = 0

    return retcode


# Generate single-channel greyscale image from MODIS data
def modis_single( inpfname, outfname ):
    print outfname

    nulldata = 65535
    nstd = 3.0

    driver = gdal.GetDriverByName( 'GTiff' )

    inds = gdal.Open( inpfname )
    imgnx = inds.RasterXSize
    imgny = inds.RasterYSize
    imgproj = inds.GetProjectionRef()
    imgtrans = inds.GetGeoTransform()
    # print imgtrans
    xorigin = imgtrans[0]
    yorigin = imgtrans[3]
    imgres = imgtrans[1]
    # Get data and scale it to 8-bit
    img = inds.GetRasterBand(1).ReadAsArray()
    img = scale_image_8bit( img, nulldata, nstd )
    idx = N.nonzero( img != 255 )
    # print img[idx].min(), img[idx].max(), N.average( img[idx] ), N.std( img[idx] )
    inds = None

    # Create the new image file
    outds = driver.Create( outfname, imgnx, imgny, 1, gdal.GDT_Byte, \
        [ 'COMPRESS=JPEG', 'JPEG_QUALITY=75' ] )
    outds.SetGeoTransform( imgtrans )
    outds.SetProjection( imgproj )
    outds.GetRasterBand(1).WriteArray( img )
    outds.GetRasterBand(1).SetNoDataValue( 255 )
    outds = None

    retcode = 0

    return retcode


# Generate GeoTIFF data
def generate_geotiff( qkmflg, hkmflg, km1flg, cldflg, satellite, dt, filedt, subset, coverage ):

    dtstr = dt.strftime('%Y%m%d_%H%M')
    if satellite == 'MOD':
        satname = 'terra'
    elif satellite == 'MYD':
        satname = 'aqua'
    else:
        print 'Unknown satellite: ', satellite
        sys.exit()
    if subset.hem == 1:
        areaname = 'Arctic'
    elif subset.hem == -1:
        areaname = 'Antarctic'
    else:
        print 'Unknown hemisphere:', hem
        sys.exit()

    # Process QKM
    if qkmflg == 1:
        qkm_status = MRTSwath_process( satellite, dt, filedt, subset, 'QKM' )

    # Process HKM
    if hkmflg == 1:
        hkm_status = MRTSwath_process( satellite, dt, filedt, subset, 'HKM' )

    # Process 1KM
    if km1flg == 1:
        km1_status = MRTSwath_process( satellite, dt, filedt, subset, '1KM' )
        
    # Process CLOUD
    if cldflg == 1:
        cld_status = MRTSwath_process( satellite, dt, filedt, subset, 'CLOUD' )    

    # Process GEO
    geo_status = MRTSwath_process( satellite, dt, filedt, subset, 'GEO' )

    # Process MSK
    msk_status = MRTSwath_process( satellite, dt, filedt, subset, 'MSK' )

    # Package results into tar.bz2 file
    outfname = ("%s/%s/Processed/modis_%s_%s_%s.tar.bz2" % \
        (BASEDIR,areaname,subset.shortname,dtstr,satname))
    tarout = tarfile.open( outfname, 'w:bz2' )
    searchstr = ("modis_%s_%s_%s" % (subset.shortname,dtstr,satname) )
    filelist = os.listdir( TMPDIR )
    for fname in sorted( filelist ):
        if fname.find( searchstr ) != -1:
            tarout.add( ("%s/%s" % (TMPDIR,fname)), arcname=fname )
    tarout.close()

    print ("   %30s %6.2f" % (subset.name,coverage) )

    # Generate output GeoTIFFs
    if km1flg == 1:

        # Ch.31 IR output (800m)
        inp1fname = ("%s/modis_%s_%s_%s_EV_1KM_Emissive_b10.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        ch31fname = ("%s/%s/GeoTIFF/modis_%s_%s_%s_ch31.tif" % \
            (BASEDIR,areaname,subset.shortname,dtstr,satname) )
        retcode = modis_single( inp1fname, ch31fname )

    if qkmflg == 1:

        # Ch.2 NIR output (200m)
        inp1fname = ("%s/modis_%s_%s_%s_EV_250_RefSB_b1.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        ch2fname = ("%s/%s/GeoTIFF/modis_%s_%s_%s_ch2.tif" % \
            (BASEDIR,areaname,subset.shortname,dtstr,satname) )
        retcode = modis_single( inp1fname, ch2fname )

    if qkmflg == 1 and hkmflg == 1:

        # Visual 1-4-3 (200m)
        inp1fname = ("%s/modis_%s_%s_%s_EV_250_RefSB_b0.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        inp2fname = ("%s/modis_%s_%s_%s_EV_500_RefSB_b1.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        inp3fname = ("%s/modis_%s_%s_%s_EV_500_RefSB_b0.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        visfname = ("%s/%s/GeoTIFF/modis_%s_%s_%s_ch1-4-3.tif" % \
            (BASEDIR,areaname,subset.shortname,dtstr,satname) )
        retcode = modis_composite(inp1fname,inp2fname,inp3fname,visfname)

        # False Colour (3-6-7 (400m)
        inp1fname = ("%s/modis_%s_%s_%s_EV_500_RefSB_b0.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        inp2fname = ("%s/modis_%s_%s_%s_EV_500_RefSB_b3.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        inp3fname = ("%s/modis_%s_%s_%s_EV_500_RefSB_b4.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        visfname = ("%s/%s/GeoTIFF/modis_%s_%s_%s_ch3-6-7.tif" % \
            (BASEDIR,areaname,subset.shortname,dtstr,satname) )
        retcode = modis_composite(inp1fname,inp2fname,inp3fname,visfname)

    if qkmflg == 1 and hkmflg == 1 and km1flg == 1:

        # False Colour 31-2-3 (200m)
        inp1fname = ("%s/modis_%s_%s_%s_EV_1KM_Emissive_b10.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        inp2fname = ("%s/modis_%s_%s_%s_EV_250_RefSB_b1.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        inp3fname = ("%s/modis_%s_%s_%s_EV_500_RefSB_b0.tif" % \
            (TMPDIR,subset.shortname,dtstr,satname) )
        visfname = ("%s/%s/GeoTIFF/modis_%s_%s_%s_ch31-2-3.tif" % \
            (BASEDIR,areaname,subset.shortname,dtstr,satname) )
        retcode = modis_composite(inp1fname,inp2fname,inp3fname,visfname)

    # Delete temporary files
    searchstr1 = ("modis_%s_%s_%s" % (subset.shortname,dtstr,satname) )
    filelist = os.listdir( TMPDIR )
    for fname in filelist:
        if fname.find( searchstr1 ) != -1:
            os.remove( ("%s/%s" % (TMPDIR,fname)) )


# Produce calibrated radiance values from MODIS data
# Bands are numbered 1-36
# refflg = 0 (Radiance), refflg = 1 (Reflectance)
# expflg = 1 to enlarge files
def modis_calibration( rootfname, band, refflg, expflg ):

    # bandres = [ 200, 200, 400, 400, 400, 400, 400, \
    #             800, 800, 800, \
    #             800, 800, 800, 800, 800, 800, 800, 800, 800, 800, \
    #             800, 800, 800, 800, 800, 800, 800, 800, 800, 800, \
    #             800, 800, 800, 800, 800, 800 ]
    bandres = [ '250', '250', '500', '500', '500', '500', '500', \
                '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', \
                '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', \
                '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM', '1KM' ]

    bandtypes = [ 'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', \
        'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', \
        'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', 'RefSB', \
        'Emissive', 'Emissive', 'Emissive', 'Emissive', 'Emissive', 'Emissive', 'Emissive', 'Emissive', \
        'Emissive', 'Emissive', 'Emissive', 'Emissive', 'Emissive', 'Emissive', 'Emissive', 'Emissive' ]

    bandnums = [ 0, 1, 0, 1, 2, 3, 4, \
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, \
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ]

    # print len(bandres), len(bandnames), len(bandtypes), len(bandnums)

    ifx = -1
    i = 0
    for bandname in bandnames:
        if bandname == band:
            idx = i
        i = i + 1

    # If we find the bandname, do processing
    output = 0
    if idx > -1:

        bres = bandres[idx]
        btype = bandtypes[idx]
        bnum = bandnums[idx]

        # Construct image data file name
        if band != '26':
            inpfname = ("%s/%s_EV_%s_%s_b%d.tif" % (TMPDIR,rootfname,bres,btype,bnum))
        else:
            inpfname = ("%s/%s_EV_Band26.tif" % (TMPDIR,rootfname))
        # print inpfname

        # Load image data
        inp_ds = gdal.Open( inpfname, GA_ReadOnly )
        inp_b1 = inp_ds.GetRasterBand(1)
        inp = inp_ds.ReadAsArray()
        inp_ds = None

        # Get calibration values
        #    EV_1KM_Emissive:band_names
        #    EV_1KM_Emissive:radiance_scales
        #    EV_1KM_Emissive:radiance_offsets
        if bres == '1KM':
            metafname = ("%s/%s_1km.txt" % (TMPDIR,rootfname))
        elif bres == '500':
            metafname = ("%s/%s_hkm.txt" % (TMPDIR,rootfname))
        elif bres == '250':
            metafname = ("%s/%s_qkm.txt" % (TMPDIR,rootfname))
        metafin = open( metafname, 'r' )
        for tline in metafin:
            srcstr = ("EV_%s_%s:band_names" % (bres,btype))
            if tline.find(srcstr) != -1:
                # print tline
                bandstr = tline.split("\"")[1]
                # print bandstr
                bits = bandstr.split(",")
                names = []
                for bit in bits:
                    names.append( bit )
            # Get reflectance scales and offsets instead of radiance as required
            if refflg == 1:
                srcstr = ("EV_%s_%s:reflectance_scales" % (bres,btype)) 
            else:
                srcstr = ("EV_%s_%s:radiance_scales" % (bres,btype)) 
            if tline.find(srcstr) != -1:
                # print tline
                scalestr = (((tline.split("=")[1]).replace(' ','')).replace(';','')).replace('f','')
                # print scalestr
                bits = scalestr.split(",")
                scales = []
                for bit in bits:
                    scales.append( float(bit) )
            if refflg == 1:
                srcstr = ("EV_%s_%s:reflectance_offsets" % (bres,btype))
            else:
                srcstr = ("EV_%s_%s:radiance_offsets" % (bres,btype))
            if tline.find(srcstr) != -1:
                # print tline
                offsetstr = (((tline.split("=")[1]).replace(' ','')).replace(';','')).replace('f','')
                # print offsetstr
                bits = offsetstr.split(",")
                offsets = []
                for bit in bits:
                    offsets.append( float(bit) )
        metafin.close()
        # print names
        # print scales
        # print offsets

        # Calibrate values
        # See http://mcst.gsfc.nasa.gov/forums/how-can-i-extract-temperature-l1b-data-product
        idx = -1
        i = 0
        for chn in names:
            if chn == band:
                idx = i
            i = i + 1
        # print idx
        if idx > -1:
            inp = scales[idx] * (inp - offsets[idx])
        # print inp.min(), inp.max()

        # Rescale if QKM or HKM
        if expflg == 1:
            if bres == '500':
                output = enlarge( inp, x=2, y=2 )
            elif bres == '1KM':
                output = enlarge( inp, x=4, y=4 )
            else:
                output = inp
        else:
            output = inp
        # print output.shape

    return output

def modis_calcbt( radval, band ):

    # print bandnames
    # print lambdas

    # Determine the band index
    ifx = -1
    i = 0
    for bandname in bandnames:
        if bandname == band:
            idx = i
        i = i + 1

    h = 6.62606957*N.power(10.0,-34.0) # Plank's constant (joules/second)
    c = 299792458                      # speed of light in vacuum (m/s)
    k = 1.3806488*N.power(10.0,-23.0)  # Boltzmann gas constant (joules/Kelvin)
    lmbda = lambdas[idx]   # band or detector center wavelength (m)
    # print lmbda*100
    hc = h * c
    # print hc
    bit3 = (radval*1000) * (1000^2)
    bt = hc / ( k * lmbda * N.log( \
        ( (2*h*(c*c)*N.power(lmbda,-5.0)) / bit3 ) + 1.0 \
        ) )
    # print h*c
    # print 'v1 NASA ', bt.min(), bt.max()

    return bt
