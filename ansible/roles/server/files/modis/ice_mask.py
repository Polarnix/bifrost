#!/usr/bin/python

# Name:          ice_mask.py
# Purpose:       Generate a sea ice mask used in cloud mask tests.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import datetime

from subprocess import call
from ftplib import FTP
import gzip, shutil

import numpy as N
from pylab import *
# from Scientific.IO.NetCDF import *
from netCDF4 import Dataset

import osgeo.gdal as gdal
from osgeo.gdalconst import *
import osgeo.osr as osr

import pg

import mapscript

BASEDIR = '/home/bifrostsat/Data/MODIS'
INCDIR='/home/bifrostsat/Include/MODIS'
TMPDIR = ("%s/tmp" % BASEDIR)
PROGDIR = '/home/bifrostsat/Python/MODIS'

# Draw landmask layer
def plot_landmask( mapobj, image ):

    # Define landmask layer
    landmask = mapscript.layerObj( mapobj )
    landmask.name = "Land Mask"
    landmask.type = mapscript.MS_LAYER_POLYGON
    landmask.status = mapscript.MS_ON
    longlatproj4str = '+proj=longlat +ellps=WGS84'
    landmask.setProjection( longlatproj4str )
    landmask.connectiontype = mapscript.MS_POSTGIS
    landmask.connection = 'user=bifrostadmin password=bifrostadmin dbname=Ocean host=localhost'
    landmask.addProcessing("CLOSE_CONNECTION=DEFER")
    landmask.data = "geom FROM gshhs_h USING srid=4326"
    landmask.antialias = mapscript.MS_FALSE

    # Define Land Mask class
    class_land = mapscript.classObj( landmask )
    class_land.name = "Land Mask"
    style_land = mapscript.styleObj(class_land)
    style_land.outlinecolor.setRGB(255, 255, 255)
    style_land.color.setRGB(255, 255, 255)
    style_land.antialias = mapscript.MS_FALSE

    # Draw layer onto image
    landmask.draw( mapobj, image )

def filter_table(hemname):
    # Get hemisphere code
    if hemname == 'Arctic':
        hemcode = 'nh'
    elif hemname == 'Antarctic':
        hemcode = 'sh'
    else:
        print ("  Unknown hemisphere \'%s\' in ice_mask.py: filter_table" % hemname)
        sys.exit()

    # Load typical recent OSI SAF ice concentrations
    # Day 1
    ncfname1 = ("%s/ice_conc_%s_polstere-100_multi_201203121200.nc" % (INCDIR,hemcode))
    fin1 = Dataset(ncfname1, 'r')
    var1 = fin1.variables['ice_conc']
    # ice_conc1 = N.array(var1.getValue())
    ice_conc1 = N.array(var1[:])
    fin1.close()
    ice_conc1 = ice_conc1.squeeze()
    # print ice_conc1.shape
    # Day 2
    ncfname2 = ("%s/ice_conc_%s_polstere-100_multi_201203131200.nc" % (INCDIR,hemcode))
    fin2 = Dataset(ncfname2, 'r')
    var2 = fin2.variables['ice_conc']
    # ice_conc2 = N.array(var2.getValue())
    ice_conc2 = N.array(var2[:])
    fin2.close()
    ice_conc2 = ice_conc2.squeeze()
    # Day 3
    ncfname3 = ("%s/ice_conc_%s_polstere-100_multi_201203141200.nc" % (INCDIR,hemcode))
    fin3 = Dataset(ncfname3, 'r')
    var3 = fin3.variables['ice_conc']
    # ice_conc3 = N.array(var3.getValue())
    ice_conc3 = N.array(var3[:])
    fin3.close()
    ice_conc3 = ice_conc3.squeeze()
    # Day 4
    ncfname4 = ("%s/ice_conc_%s_polstere-100_multi_201202021200.nc" % (INCDIR,hemcode))
    fin4 = Dataset(ncfname4, 'r')
    var4 = fin4.variables['ice_conc']
    # ice_conc4 = N.array(var4.getValue())
    ice_conc4 = N.array(var4[:])
    fin4.close()
    ice_conc4 = ice_conc4.squeeze()
    # print ice_conc4.shape

    # Initial filters table
    filters = [ [ 'NP',             -32767 ],
                [ 'Coastline',      -19900 ],
                [ 'Landmask',        -9900 ] ]

    # Loop through filters and create coordinate lists
    for i in range(len(filters)):
        idx = N.nonzero( N.all( [ ice_conc1 == filters[i][1], ice_conc2 == filters[i][1], \
            ice_conc3 == filters[i][1], ice_conc4 == filters[i][1] ], axis = 0 )  )
        idx_x = idx[0]
        idx_y = idx[1]
        # print idx_x.shape, idx_y.shape
        nidx = idx_x.shape[0]

        # North Pole hole
        if i == 0:
            idx_xf = []
            idx_yf = []
            for j in range(nidx):
                if idx_x[j] > 550 and idx_x[j] < 620:
                    if idx_y[j] > 350 and idx_y[j] < 410:
                        # print j, idx_x[j], idx_y[j]
                        idx_xf.append(idx_x[j])
                        idx_yf.append(idx_y[j])
            filters[i][1] = (N.array(idx_xf,dtype=int16),N.array(idx_yf,dtype=int16))

        else:
            filters[i][1] = idx

    return filters


# Draw OSI SAF layer as concentration (%)
def plot_osisaf( year, month, day, projstr, limits, res, rootfname, hemname ):
    # Get hemisphere code
    if hemname == 'Arctic':
        hemcode = 'nh'
    elif hemname == 'Antarctic':
        hemcode = 'sh'
    else:
        print ("  Unknown hemisphere \'%s\' in ice_mask.py: plot_osisaf" % hemname)
        sys.exit()

    # Set date and folder for checking
    month_date = datetime.date( year, month, day )
    reprocessed_date = datetime.date( 2009, 11, 1 )
    if month_date < reprocessed_date:
        ftpdir = ("reprocessed/ice/conc/v1p1/%4d/%02d/" % (year,month))
        ncfilter = ("ice_conc_%s_polstere-100_reproc_" % hemcode)
        nclen = 50
        dmstr = 32
        dmend = 40
    else:
        # ftpdir = 'prod_netcdf/ice/conc/'
        # ncfilter = 'ice_conc_nh_'
        # clen = 27
        # dmstr = 12
        # dmend = 20
        ftpdir = ("archive/ice/conc/%4d/%02d/" % (year,month))
        ncfilter = ("ice_conc_%s_polstere-100_multi_" % hemcode)
        nclen = 46
        dmstr = 31
        dmend = 39
        
    #          1         2         3         4
    # 12345678901234567890123456789012345678901234567890
    # ice_conc_nh_polstere-100_multi_201202021200.nc

    # Open FTP session
    osisaf_url = 'osisaf.met.no'
    ftp = FTP( osisaf_url )
    ftp.login()
    ftp.cwd( ftpdir )
    
    # Get list of files and filter out Arctic, Polar Stereographic
    teststr = ("%4d%02d%02d" % (year,month,day))
    filelist = ftp.nlst()
    nclist = []
    for fname in filelist:
        fdatestr = fname[dmstr:dmend]
        if fname.find( ncfilter ) != -1 and len(fname) == nclen and fdatestr == teststr:
            nclist.append( fname )
            # print fname, fname[dmstr:dmend]
    nclist = sorted(nclist)
    # print nclist

    # Handle data being downloaded
    def handleDownload(block):
        fout.write(block)

    # Download NetCDF file to temporary directory
    print '\nDownloading files:'
    for fname in nclist:
        print '   ', fname
        localfname = ("%s/%s" % (TMPDIR,fname))
        fout = open( localfname, 'wb' )
        ftp.retrbinary( ("RETR %s" % fname), handleDownload )
        fout.close()

    # Close ftp session
    ftp.close()

    # Process data file
    # narea = len(areas)
    ice_classes = [ "OW", "VODI", "ODI", "CDI", "VCDI+FI" ]
    nclass = len(ice_classes)
    nday = 0
    # total_area = N.zeros( (narea, nclass), dtype=float32 )

    # Check to see if OSI SAF data exists
    if len(nclist) > 0:
        fname = nclist[0]

        # ice_areas = N.zeros( (narea, nclass), dtype=float32 )
        localfname = ("%s/%s" % (TMPDIR,fname))

        # Reprocessed dataset files are compressed, so uncompress
        if month_date < reprocessed_date:
            netcdfname = localfname[:-3]
            with open(netcdfname, "wb") as tmp:
                shutil.copyfileobj(gzip.open(localfname), tmp)
            fin = Dataset(netcdfname, 'r')
            var = fin.variables['ice_conc']
        else:
            fin = Dataset(localfname, 'r')
            # var = fin.variables['ice_concentration']
            var = fin.variables['ice_conc']
        # ice_conc = N.array(var.getValue())
        ice_conc = N.array(var[:])
        fin.close()
        ice_conc = ice_conc.squeeze()
        # print ice_conc.shape, ice_conc.min(), ice_conc.max()
        if month_date < reprocessed_date:
            os.remove(netcdfname)

        # Apply filters to reprocessed dataset, due to recent poor OSI SAF processing
        # if month_date < reprocessed_date:
        filters = filter_table(hemname)
        for i in range(len(filters)):
            ice_conc[filters[i][1]] = -9999

        # Define colour table
        colT = gdal.ColorTable()
        colT.SetColorEntry(   0, (255,255,255,255) )
        colT.SetColorEntry( 255, (  0,  0,125,255) )
        # Open Water (0-1) (150,200,255)
        for i in range(1,11):
            # colT.SetColorEntry( i, (150,200,255,255) )
            colT.SetColorEntry( i, (255,255,255,255) )
        # Very Open Drift Ice (1-4) (140,255,160)
        for i in range(11,41):
            colT.SetColorEntry( i, (140,255,160,255) )
        # Open Drift Ice (4-7) (255,255,  0)
        for i in range(41,71):
            colT.SetColorEntry( i, (255,255,  0,255) )
        # Close Drift Ice (7-9) (255,125,  7)
        for i in range(71,91):
            colT.SetColorEntry( i, (255,125,  7,255))
        # Very Close Drift Ice (7-10) (255,   0,  0)
        for i in range(91,101):
            colT.SetColorEntry( i, (255,  0,  0,255))

        # Generate output graphic
        dst_filename = ("%s/osisaf_%4d%02d%02d_tmp.tif" % (TMPDIR,year,month,day))
        if hemname == 'Arctic':
            safprojstr = "+proj=stere +a=6378273 +b=6356889.44891 +lat_0=90 +lat_ts=70 +lon_0=-45"
            ulx = -3855005.0
            uly = 5844997.0
        elif hemname == 'Antarctic':
            safprojstr = "+proj=stere +a=6378273 +b=6356889.44891 +lat_0=-90 +lat_ts=-70 +lon_0=0"
            ulx = -3955000.0
            uly = 4355000.0
        safres = 10000.0

        tmpic = ice_conc / 100.0
        # print tmpic.min(), tmpic.max()
        tmpic[N.nonzero(tmpic<0)] = 255
        tmpic = N.array( tmpic, dtype=N.uint8 )
        # print tmpic.min(), tmpic.max()
        [ny,nx] = tmpic.shape
        # print nx,ny

        format = "GTiff"
        driver = gdal.GetDriverByName( format )
        metadata = driver.GetMetadata()
        dst_ds = driver.Create( dst_filename, nx, ny, 1, gdal.GDT_Byte )
        dst_ds.SetGeoTransform( [ ulx, safres, 0, uly, 0, -safres ] )
        srs = osr.SpatialReference()
        srs.ImportFromProj4(safprojstr)
        dst_ds.SetProjection( srs.ExportToWkt() )
        dst_ds.GetRasterBand(1).WriteArray( tmpic )
        dst_ds.GetRasterBand(1).SetRasterColorTable(colT)
        dst_ds = None

        # Use gdalwarp to create a high resolution image file for generating the anomaly plot
        warped_fname = ("%s/%s/Processed/%s_iceconc.tif" % (BASEDIR,hemname,rootfname))
        cmd = "/usr/bin/gdalwarp -of GTiff -co \"COMPRESS=LZW\""
        cmd = ("%s -t_srs \"%s\"" % (cmd,projstr))
        cmd = ("%s -te %.1f %.1f %.1f %.1f -tr %.1f %.1f" % \
            (cmd,limits[0],limits[3],limits[2],limits[1],res[0],res[1]))
        cmd = ("%s -r near -overwrite -q" % cmd)
        cmd = ("%s \"%s\"" % (cmd,dst_filename))
        cmd = ("%s \"%s\"" % (cmd,warped_fname))
        # print cmd
        try:
            retcode = call(cmd, shell=True)
            if retcode < 0:
                print >>sys.stderr, "Child was terminated by signal", -retcode
            else:
                # print >>sys.stderr, "Child returned", retcode
                pass
        except OSError, e:
            print >>sys.stderr, "Execution failed:", e

        # Clean up, remove temporary files
        os.remove( dst_filename )
        os.remove( localfname )

    else:
        # If we get to here, then no OSI SAF data available
        warped_fname = 'None'

    return warped_fname


# Draw ice chart layer as concentration (%)
def plot_icechart_as_concentration( mapobj, image, icedt ):

    # Define layer with region
    ice_chart = mapscript.layerObj( mapobj )
    ice_chart.name = "Ice Chart"
    ice_chart.type = mapscript.MS_LAYER_POLYGON
    ice_chart.status = mapscript.MS_ON
    longlatproj4str = '+proj=longlat +ellps=WGS84'
    ice_chart.setProjection( longlatproj4str )
    ice_chart.antialias = mapscript.MS_FALSE

    ice_chart.connectiontype = mapscript.MS_POSTGIS
    ice_chart.connection = 'user=bifrostadmin password=bifrostadmin dbname=icecharts host=localhost'
    ice_chart.addProcessing("CLOSE_CONNECTION=DEFER")
    ice_chart.data = "polygon FROM icechart_polygons USING srid=4326 USING UNIQUE id"

    ow_filter = ("id > -1 AND datetime = \'%s\'" % icedt)
    status = ice_chart.setFilter( ow_filter )

    ice_chart.classitem = 'norway_iceclass'

    # Define "Open Water" class
    class_ow = mapscript.classObj( ice_chart )
    class_ow.name = "Open Water"
    class_ow.setExpression( "\'Open Water\'" )
    style_ow = mapscript.styleObj(class_ow)
    style_ow.outlinecolor.setRGB(5, 5, 5)
    style_ow.color.setRGB(5, 5, 5)
    style_ow.antialias = mapscript.MS_FALSE

    # Define "Very Open Drift Ice" class
    class_vodi = mapscript.classObj( ice_chart )
    class_vodi.name = "Very Open Drift Ice"
    class_vodi.setExpression( "\'Very Open Drift Ice\'" )
    style_vodi = mapscript.styleObj(class_vodi)
    style_vodi.outlinecolor.setRGB(25, 25, 25)
    style_vodi.color.setRGB(25, 25, 25)
    style_vodi.antialias = mapscript.MS_FALSE

    # Define "Open Drift Ice" class
    class_odi = mapscript.classObj( ice_chart )
    class_odi.name = "Open Drift Ice"
    class_odi.setExpression( "\'Open Drift Ice\'" )
    style_odi = mapscript.styleObj(class_odi)
    style_odi.outlinecolor.setRGB(55, 55, 55)
    style_odi.color.setRGB(55, 55, 55)
    style_odi.antialias = mapscript.MS_FALSE

    # Define "Close Drift Ice" class
    class_cdi = mapscript.classObj( ice_chart )
    class_cdi.name = "Close Drift Ice"
    class_cdi.setExpression( "\'Close Drift Ice\'" )
    style_cdi = mapscript.styleObj(class_cdi)
    style_cdi.outlinecolor.setRGB(80, 80, 80)
    style_cdi.color.setRGB(80, 80, 80)
    style_cdi.antialias = mapscript.MS_FALSE

    # Define "Very Close Drift Ice" class
    class_vcdi = mapscript.classObj( ice_chart )
    class_vcdi.name = "Very Close Drift Ice"
    class_vcdi.setExpression( "\'Very Close Drift Ice\'" )
    style_vcdi = mapscript.styleObj(class_vcdi)
    style_vcdi.outlinecolor.setRGB(95, 95, 95)
    style_vcdi.color.setRGB(95, 95, 95)
    style_vcdi.antialias = mapscript.MS_FALSE

    # Define "Fast Ice" class
    class_fi = mapscript.classObj( ice_chart )
    class_fi.name = "Fast Ice"
    class_fi.setExpression( "\'Fast Ice\'" )
    style_fi = mapscript.styleObj(class_fi)
    style_fi.outlinecolor.setRGB(100, 100, 100)
    style_fi.color.setRGB(100, 100, 100)
    style_fi.antialias = mapscript.MS_FALSE

    # Draw layer onto image
    ice_chart.draw( mapobj, image )


# Construct ice mask for MODIS subsets
def ice_mask( rootfname, hemname, nxsz, nysz, projection, geotransform ):

    # Extract date and time from rootfname
    bits = rootfname.split('_')
    datestr = bits[2]
    year = int(datestr[0:4])
    month = int(datestr[4:6])
    day = int(datestr[6:8])
    timestr = bits[3]
    hour = int(timestr[0:2])
    minute = int(timestr[2:4])

    # Construct limits from geotransform
    ullr = [ geotransform[0], \
             geotransform[3], \
             geotransform[0] + ( nxsz * geotransform[1] ), \
             geotransform[3] + ( nysz * geotransform[5] ) ]
    # print ullr

    # Set projection as a Proj4 string
    srs = osr.SpatialReference()
    srs.ImportFromWkt( projection )
    projstr = srs.ExportToProj4()

    # Ice chart data
    # Check database for ice chart on this day
    con2 = pg.connect(dbname='Icecharts', host='localhost', user='bifrostadmin', passwd='bifrostadmin')
    strwin = datetime.datetime( year, month, day, hour, minute, 0) - \
        datetime.timedelta( hours = 6 )
    endwin = datetime.datetime( year, month, day, hour, minute, 0) + \
        datetime.timedelta( hours = 6 )
    if hemname == 'Arctic':
        querytxt = ("SELECT COUNT(*),max(datetime) from icechart_polygons WHERE datetime >= \'%s\' AND datetime <= \'%s\' AND ST_Y(ST_Centroid(polygon)) > 0.0;" % \
            ( strwin.__str__(), endwin.__str__() ) )
    elif hemname == 'Antarctic':
        querytxt = ("SELECT COUNT(*),max(datetime) from icechart_polygons WHERE datetime >= \'%s\' AND datetime <= \'%s\' AND ST_Y(ST_Centroid(polygon)) < 0.0;" % \
            ( strwin.__str__(), endwin.__str__() ) )
    else:
        print ("  Unknown hemisphere \'%s\' in ice_mask.py" % hemname)
        sys.exit()
    # print querytxt
    queryresult = con2.query( querytxt )
    npoly = queryresult.getresult()[0][0]
    icedt = queryresult.getresult()[0][1]
    # print npoly, icedt
    con2.close()

    # Plot the ice concentration data
    if npoly > 0:
        # Use Mapscript to generate ice raster
        format = mapscript.outputFormatObj('GDAL/GTiff', 'gtiff' )
        format.imagemode = mapscript.MS_IMAGEMODE_RGB
        format.setMimetype( 'image/tiff' )
        format.setOption( 'interlace', 'off' )
        format.setOption( 'compress', 'lzw' )
        image = mapscript.imageObj(nxsz, nysz, format)
        # Define Ice Chart map to go onto image
        iceconcmap = mapscript.mapObj()
        iceconcmap.name = "Ice Chart Concentrations"
        iceconcmap.setSize( nxsz, nysz )
        iceconcmap.units = mapscript.MS_METERS
        iceconcmap.setExtent( ullr[0], ullr[3], ullr[2], ullr[1] )
        iceconcmap.setProjection( projstr )
        iceconcmap.selectOutputFormat( 'gtiff' )
        black = mapscript.colorObj()
        black.setRGB( 0, 0, 0 )
        iceconcmap.imagecolor = black
        # print npoly

        # Draw ice chart (concentration) layer on image
        plot_icechart_as_concentration( iceconcmap, image, icedt )

        # Draw land mask on image
        plot_landmask( iceconcmap, image )
        # Save image to disk
        tiffname = ("%s/%s/Processed/%s_iceconc." % (BASEDIR,hemname,rootfname)) + image.format.extension
        image.save(tiffname,iceconcmap)

    else:
        # Fallback to using OSI SAF data
        print 'No ice chart on this date. Falling back to OSI SAF data.'
        resx = (ullr[2] - ullr[0]) / float(nxsz)
        resy = (ullr[1] - ullr[3]) / float(nysz)
        # print ullr
        # print resx, resy
        tiffname = plot_osisaf( year, month, day, projstr, \
            ullr, [ resx, resy ], rootfname, hemname )

    # Load image as array
    if tiffname != 'None':
        icemsk_ds = gdal.Open( tiffname, GA_ReadOnly )
        icemsk_b1 = icemsk_ds.GetRasterBand(1)
        icemsk = icemsk_b1.ReadAsArray()
        icemsk_ds = None
    else:
        icemsk = N.zeros( (nysz, nxsz), dtype=N.uint8 ) + 100

    # print icemsk.shape
    # print icemsk.min(), icemsk.max()
    # print N.unique(icemsk)

    # Remove temporary GeoTIFF
    # os.remove(tiffname)

    # Ice forecast data

    # OSI SAF data

    return icemsk
