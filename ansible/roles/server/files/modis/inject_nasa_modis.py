#!/usr/bin/python

# Name:          inject_nasa_modis.py
# Purpose:       Script to download a NASA MODIS archive file for a specific satellite, date and time.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, socket, subprocess
import time, datetime, smtplib
import time
from ftplib import FTP
import tarfile

import numpy as N

import osgeo.ogr as ogr
import osgeo.osr as osr

import pg

# from load_subset_areas import load_subset_areas
from modis_areas import modis_areas
from modis_processing import load_subset_areas, modis_cover, cover_from_coords, get_modisfile, get_archivefile

PROGDIR = '/home/bifrostsat/Python/MODIS'
INCDIR = '/home/bifrostsat/Include/MODIS'
ANCDIR= '/home/bifrostsat/Ancillary/MODIS'
BASEDIR = '/home/bifrostsat/Data/MODIS'
DATADIR = ("%s/Raw" % BASEDIR)
TMPDIR = ("%s/tmp" % BASEDIR)
ARCHIVE = ("%s/Archive" % BASEDIR)
METADIR = ("%s/geoMeta/6" % BASEDIR)

# Threshold for area that must be covered before image is processed
THRESHOLD = 5.0

# Active flag allows processing
active = 1    

# Main data download and injection routine
def inject_nasa_modis(  satellite, getdt, threshold, process, lockfn ):

    # Unpack date/time
    getyr = getdt.year
    getmh = getdt.month
    getdy = getdt.day
    gethr = getdt.hour
    getmn = getdt.minute

    # Date and time strings
    datestr = getdt.strftime('%Y%m%d')
    timestr = getdt.strftime('%H%M')
    
    # Controller e-mail address
    # cntrladdr='nick.hughes@met.no'
    # HeaderText='From: '+cntrladdr+'\r\n'
    # HeaderText+='To: '+cntrladdr+'\r\n'
    
    # Month decoding
    mths = { 'Jan' : 1, 'Feb' : 2, 'Mar' : 3, 'Apr' :  4, 'May' :  5, 'Jun' :  6,
             'Jul' : 7, 'Aug' : 8, 'Sep' : 9, 'Oct' : 10, 'Nov' : 11, 'Dec' : 12  }
    
    # Timeout values
    timeout = 20
    socket.setdefaulttimeout( timeout )
    
    # Set raw longitude/latitude projection
    rawprojstr = '+proj=longlat +ellps=WGS84'
    rawproj = osr.SpatialReference()
    rawproj.ImportFromProj4( rawprojstr )
    
    # Load subset information from Shapefile
    shpfname = ("%s/modis_subsets_ll.shp" % INCDIR)
    [nsubset,subset] = load_subset_areas( shpfname )
    
    # Satellites
    satcodes = [ 'MOD', 'MYD' ]
    
    # Define current date/time
    currdt = datetime.datetime.utcnow()
    # print currdt
    
    # Connect to 'icecharts' database for determining file status from 'modis_files' table
    con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')
    
    # Create a tarfile name for checking
    tarfname = ("modis_%4d%02d%02d_%02d%02d_%s.tar" % (getyr,getmh,getdy,gethr,getmn,satellite) )
    
    # Check database here
    querytxt = ("SELECT id FROM modis_files WHERE filename = \'%s\';" % tarfname)
    # print querytxt
    queryres = con1.query(querytxt)
    try:
        fileid = queryres.getresult()[0][0]
    except:
        # Now query for next available id
        maxfileidres = con1.query('SELECT MAX(id) FROM modis_files;')
        maxfileid = maxfileidres.getresult()[0][0]
        if not maxfileid:
            maxfileid = 0
        nextfileid = maxfileid + 1
        present = 0
    else:
        nextfileid = fileid
        present = 1
    
    # If file is present but we do not set the process override flag, quit the program
    # print present, nextfileid, process
    if present == 1 and process == 0:
        print "Processing already done, but override not set. Quitting program."
        sys.exit()
    # 63541
    
    # Set up connection to NASA LAADS FTP server
    host = 'ladsweb.nascom.nasa.gov'
    username = 'anonymous'
    password = 'nick.hughes@met.no'
    ftpbasedir = 'allData/6'
    connectflg = 0
    retries = 9
    while connectflg == 0 and retries > 0:
        try:
            ftp = FTP( host, username, password )
        except:
            print ("Unable to make connection to FTP server, %d attempts left." % retries)
            retries = retries - 1
            time.sleep(60)
        else:
            connectflg = 1
    if connectflg == 0:
        print "Unable to make connection to FTP server, giving up for now."
        os.remove( lockfn )
        sys.exit()
    
    # Switch to Binary mode
    ftp.sendcmd("TYPE i")

    # Switch to passive mode
    # ftp.sendcmd("PASV")
    
    # Loop through metadata files and get MODIS swath coverage polygon
    fcount = 0
    
    # Try to open metdata file from cache on our server
    # If file is not present, download from ftp
    if satellite == 'terra':
        satcode = 'MOD'
    elif satellite == 'aqua':
        satcode = 'MYD'
    else:
        print ("Unknown satellite: %s" % satcode)
        sys.exit()
    shortmetafn = ("%s03_%4d-%02d-%02d.txt" % (satcode,getyr,getmh,getdy))
    metapath = ("%s/%s/%d" % (METADIR,satellite.upper(),getyr))
    metafname = ("%s/%s" % (metapath,shortmetafn))
    # print metafname
    try:
        metafile = open( metafname, 'r' )
    except:
        # Try to access directory for year on our server
        try:
            os.chdir( metapath  )
        except:
            os.mkdir( metapath )
        # Get the metadata file from ftp
        # Get file from ftp server and put in temporary directory
        ftpdir = ("/geoMeta/6/%s/%4d" % (satellite.upper(),getyr) )
        ftp.cwd( ftpdir )
        ftpcmd = ("RETR %s" % shortmetafn )
        # print ftpcmd
        fmeta = open( metafname, 'wb' )
        ftp.retrbinary( ftpcmd, fmeta.write )
        fmeta.close()
        metafile = open( metafname, 'r' )
    # Read metadata file and obtain data for the date and time of image
    for tline in metafile:
        if tline[0:3] == satcode:
            metabits = tline.split(',')
            granule_name = metabits[0]
            gbits = granule_name.split('.')
            gdtstr = gbits[2]
            if gdtstr == timestr:
                # print tline
                GranuleID = metabits[0]
                StartDateTime = metabits[1]
                ArchiveSet = int( metabits[2] )
                OrbitNumber = int( metabits[3] )
                DayNightFlag = metabits[4]
                EastBoundingCoord = float( metabits[5] )
                NorthBoundingCoord = float( metabits[6] )
                SouthBoundingCoord = float( metabits[7] )
                WestBoundingCoord = float( metabits[8] )
                GRingLongitude1 = float( metabits[9] )
                GRingLongitude2 = float( metabits[10] )
                GRingLongitude3 = float( metabits[11] )
                GRingLongitude4 = float( metabits[12] )
                GRingLatitude1 = float( metabits[13] )
                GRingLatitude2 = float( metabits[14] )
                GRingLatitude3 = float( metabits[15] )
                GRingLatitude4 = float( metabits[16] )
    metafile.close()
    
    # Get Julian day
    jday = int( GranuleID[11:14] )
    
    # print year, jday, hour, minute
    dt = datetime.datetime( getyr-1, 12, 31, 0, 0, 0 ) + \
        datetime.timedelta( days=jday, hours=gethr, minutes=getmn )
    # print dt
    month = dt.month
    day = dt.day
    
    # Create coverage
    longlist = [ GRingLongitude1, GRingLongitude2, GRingLongitude3, GRingLongitude4, GRingLongitude1, ]
    latlist = [ GRingLatitude1, GRingLatitude2, GRingLatitude3, GRingLatitude4, GRingLatitude1 ]
    coverage = cover_from_coords( longlist, latlist )
    coverwkt = coverage.ExportToWkt()
    
    # See if coverage is in Arctic (<50N) or Antarctic (>40S)
    minlon, maxlon, minlat, maxlat = coverage.GetEnvelope()
    # print minlat, maxlat
    if N.max([minlat,maxlat]) > 50.0:
        print 'Arctic'
        hem = 1
    elif N.min([minlat,maxlat]) < -40.0:
        print 'Antarctic'
        hem = -1
    else:
        print 'Outside polar regions.'
        hem = 0
    
    # Check subset coverage
    maxoverlap = 0.0
    coverlist = []
    # print coverage
    for j in range(nsubset):
    
        if hem == subset[j].hem:
            # Convert coverage polygon to the same Polar Stereographic projection as
            # the subset
            overlap = subset[j].calculate_overlap( rawprojstr, coverwkt )
            if overlap > 0:
                print ("%30s %6.2f" % (subset[j].name, overlap) )
            # print intersection
            if overlap > maxoverlap:
                maxoverlap = overlap
        else:
            overlap = 0.0
    
        coverlist.append( overlap )
    
    # Initialise flags
    geoflg = 0
    qkmflg = 0
    hkmflg = 0
    km1flg = 0
    cloudflg = 0
    
    # If a subset coverage is above the threshold value, download the data
    if maxoverlap >= THRESHOLD:
    
        # Download data files
        if active == 1:
            tmpt = time.gmtime()
            stime = time.mktime(tmpt)
            seconds = -9999
    
            print "\nRetrieving files..."
            ntries = 3

            # Get QKM file
            trycount = 0
            while trycount < ntries and qkmflg <= 0:
                try:
                    [qkmflg,qkmfname,qkmsize] = get_archivefile( ftp, socket, ftpbasedir, 'QKM', \
                        satcode, getyr, jday, gethr, getmn, TMPDIR )
                except:
                    trycount = trycount + 1
                    print ("  qkm download failed %d time." % trycount)
            if qkmflg == 0:
                print "FTP server problem, stopping for now."
                os.remove( lockfn )
                sys.exit()
            elif qkmflg == 2:
                qkmflg = 0
            
            # Get HKM file
            trycount = 0
            while trycount < ntries and hkmflg <= 0:
                try:
                    [hkmflg,hkmfname,hkmsize] = get_archivefile( ftp, socket, ftpbasedir, 'HKM', \
                        satcode, getyr, jday, gethr, getmn, TMPDIR )
                except:
                    trycount = trycount + 1
                    print ("  hkm download failed %d time." % trycount)
            if hkmflg == 0:
                print "FTP server problem, stopping for now."
                os.remove( lockfn )
                sys.exit()
            elif hkmflg == 2:
                hkmflg = 0
    
            # Get 1KM file
            trycount = 0
            while trycount < ntries and km1flg <= 0:
                try:
                    [km1flg,km1fname,km1size] = get_archivefile( ftp, socket, ftpbasedir, '1KM', \
                        satcode, getyr, jday, gethr, getmn, TMPDIR )
                except:
                    trycount = trycount + 1
                    print ("  1km download failed %d time." % trycount)
            if km1flg == 0:
                print "FTP server problem, stopping for now."
                os.remove( lockfn )
                sys.exit()
            elif km1flg == 2:
                km1flg = 0
    
            totalflg = qkmflg + hkmflg + km1flg
            if totalflg > 0:
                # Get CLOUD file, and if successful, set flag to 1
                trycount = 0
                while trycount < ntries and cloudflg == 0:
                    try:
                        [cloudflg,cloudfname,cloudsize] = get_archivefile( ftp, socket, ftpbasedir, 'CLOUD', \
                            satcode, getyr, jday, gethr, getmn, TMPDIR )
                    except:
                        trycount = trycount + 1
                        print ("  cloud download failed %d time." % trycount)
                if cloudflg == 0:
                    print "FTP server problem, stopping for now."
                    os.remove( lockfn )
                    sys.exit()
                elif cloudflg == 2:
                    cloudflg = 0
                
                # Get GEO file, and if successful, set flag to 1
                trycount = 0
                while trycount < ntries and geoflg == 0:
                    try:
                        [geoflg,geofname,geosize] = get_archivefile( ftp, socket, ftpbasedir, 'GEO', \
                            satcode, getyr, jday, gethr, getmn, TMPDIR )
                    except:
                        trycount = trycount + 1
                        print ("  geo download failed %d time." % trycount)
                if geoflg == 0:
                    print "FTP server problem, stopping for now."
                    os.remove( lockfn )
                    sys.exit()
                elif geoflg == 2:
                    geoflg = 0
    
            print "Download Flags:", geoflg, qkmflg, hkmflg, km1flg, cloudflg
    
            if geoflg == 1 or qkmflg == 1 or hkmflg == 1 or km1flg == 1 or cloudflg == 1:
                tmpt = time.gmtime()
                etime = time.mktime(tmpt)
                seconds = etime-stime
                totalsize = (geosize+qkmsize+hkmsize+km1size) / 1024.0 / 1024.0
                print ("   Downloaded %7.1f Mb in %d seconds" % (totalsize,seconds) )
    
            if geoflg == -1 or qkmflg == -1 or hkmflg == -1 or km1flg == -1 or cloudflg == -1:
                fstat = 0
                MessageText='inject_nasa_modis reports a timeout when downloading '
                if geoflg == -1:
                    fstat = 1
                    MessageText = ("%s %s" % (MessageText,geofname) )
                if qkmflg == -1:
                    fstat = 1
                    MessageText = ("%s %s" % (MessageText,qkmfname) )
                if hkmflg == -1:
                    fstat = 1
                    MessageText = ("%s %s" % (MessageText,hkmfname) )
                if km1flg == -1:
                    fstat = 1
                    MessageText = ("%s %s" % (MessageText,km1fname) )
                if cloudflg == -1:
                    fstat = 1
                    MessageText = ("%s %s" % (MessageText,cloudfname) )
    
                if fstat == 1:
                    # Set to -1 if there is specifically a timeout
                    seconds = -1                            
                    # Let someone know about this
                    # HeaderText+='Subject: check_nasa_modis timeout\r\n\r\n'
                    # email=smtplib.SMTP('smtp.troms.dnmi.no')
                    # email.sendmail(cntrladdr,cntrladdr,HeaderText+MessageText)
                    # del email
    
        geoflg = 1
        geoflg = 1
        # If all flags are 1, create tarfile and update database
        totalflg = qkmflg + hkmflg + km1flg + cloudflg
        # print "cloudflg =", cloudflg
        # if totalflg == 3:
        if totalflg > 0:
            fcount = fcount  + 1
            # Create tar file
            tarfname = ("%s/modis_%4d%02d%02d_%02d%02d_%s.tar" % \
                (DATADIR,getyr,getmh,getdy,gethr,getmn,satellite) )
            print tarfname
            tarout = tarfile.open( tarfname, 'w' )
            if geoflg == 1:
                bits = geofname.split('/')
                rootfname = bits[-1]
                tarout.add( geofname, arcname=rootfname )
            if qkmflg == 1:
                bits = qkmfname.split('/')
                rootfname = bits[-1]
                tarout.add( qkmfname, arcname=rootfname )
            if hkmflg == 1:
                bits = hkmfname.split('/')
                rootfname = bits[-1]
                tarout.add( hkmfname, arcname=rootfname )
            if km1flg == 1:
                bits = km1fname.split('/')
                rootfname = bits[-1]
                tarout.add( km1fname, arcname=rootfname )
            if cloudflg == 1:
                bits = cloudfname.split('/')
                rootfname = bits[-1]
                tarout.add( cloudfname, arcname=rootfname )            
            tarout.close()
    
            # Remove temporary files and calculate fullsize
            fullsize = 0
            if geoflg == 1:
                os.remove( geofname )
                fullsize = fullsize + geosize
            if qkmflg == 1:
                os.remove( qkmfname )
                fullsize = fullsize + qkmsize
            if hkmflg == 1:
                os.remove( hkmfname )
                fullsize = fullsize + hkmsize
            if km1flg == 1:
                os.remove( km1fname )
                fullsize = fullsize + km1size
            if cloudflg == 1:
                os.remove( cloudfname )
                fullsize = fullsize + cloudsize
    
            # Update database
            tarfname = ("modis_%4d%02d%02d_%02d%02d_%s.tar" % \
                (getyr,getmh,getdy,gethr,getmn,satellite) )
            dtstr = ("%4d-%02d-%02d %02d:%02d:00" % (getyr,getmh,getdy,gethr,getmn) )
            srvdtstr = dtstr
            tmpt=time.gmtime()
            nowdtstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)
            # print dtstr, srvdtstr
            # print coverlist
            # print THRESHOLD
            # print fullsize
            # print tarfname
            # print coverwkt
            if present == 0:
                insertstr1 = "INSERT INTO modis_files" + \
                    "(id,satellite,datetime,serverdt,areaflg," + \
                    "threshold,filename,size,qkm,hkm,km1,cld,coverage," + \
                    "processdt,procflg,clouddt,cldflg," \
                    "deletedt,delflg,delprocessdt,delprocflg,archivedt,archflg"
                insertstr2 = ("VALUES(nextval(\'modis_serial\')," + \
                    "\'%s\',\'%s\',\'%s\',%d,%f,\'%s\',%d,%d,%d,%d,%d,ST_GeomFromText(\'%s\', 4326)" \
                    % (satcode,dtstr,srvdtstr,hem,THRESHOLD,tarfname, \
                    fullsize,qkmflg,hkmflg,km1flg,cloudflg,coverwkt) )
                insertstr2b = ("\'%s\',0,\'%s\',0,\'%s\',0,\'%s\',0,\'%s\',0" \
                    % (nowdtstr,nowdtstr,nowdtstr,nowdtstr,nowdtstr) )
                insertstr2 = ("%s,%s" % (insertstr2,insertstr2b) )
                for j in range(nsubset):
                    insertstr1 = ("%s,%s_cover" % (insertstr1,subset[j].shortname) )
                    insertstr2 = ("%s,%f" % (insertstr2,coverlist[j]) )
                insertstr = ("%s) %s);" % (insertstr1,insertstr2) )
            else:
                insertstr = "UPDATE modis_files SET"
                insertstr = ("%s serverdt = \'%s\'" % (insertstr,srvdtstr))
                insertstr = ("%s, threshold = %f" % (insertstr,THRESHOLD))
                insertstr = ("%s, filename = \'%s\'" % (insertstr,tarfname))
                insertstr = ("%s, size = %d" % (insertstr,fullsize))
                insertstr = ("%s, qkm = %d, hkm = %d, km1 = %d, cld = %d" % (insertstr,qkmflg,hkmflg,km1flg,cloudflg))
                insertstr = ("%s, coverage = ST_GeomFromText(\'%s\', 4326)" % (insertstr,coverwkt))
                insertstr = ("%s, processdt = \'%s\', procflg = 0" % (insertstr,nowdtstr))
                insertstr = ("%s, clouddt = \'%s\', cldflg = 0" % (insertstr,nowdtstr))
                insertstr = ("%s, deletedt = \'%s\', delflg = 0" % (insertstr,nowdtstr))
                insertstr = ("%s, delprocessdt = \'%s\', delprocflg = 0" % (insertstr,nowdtstr))
                insertstr = ("%s, archivedt = \'%s\', archflg = 0" % (insertstr,nowdtstr))
                for j in range(nsubset):
                    insertstr = ("%s, %s_cover = %f" % (insertstr,subset[j].shortname,coverlist[j]) )
                insertstr = ("%s WHERE id = %d;" % (insertstr, nextfileid))
            # print "id", nextfileid
            # print insertstr
            queryres = con1.query(insertstr)

            # Get the file ID number
            sqltxt = ("SELECT id FROM modis_files WHERE datetime = \'%s\' AND satellite = \'%s\';" \
                % (dtstr,satcode))
            queryres = con1.query(sqltxt)
            fid = queryres.getresult()[0][0]
            # print fid
    
        else:
            # If we get here, then we have a GEO file but no data so just skip this time around
            print '   No data files yet, skipping this time.'
            fid = -999
            pass
    
    
    # print ("%d files covered subsets" % fcount)
    ftp.quit()
    # print ' '

    return [ fid, tarfname, hem, qkmflg, hkmflg, km1flg, cloudflg, coverlist ]
    

# Main callable routine
if __name__ == '__main__':

    # Get user inputs
    if len(sys.argv) == 5:
        satellite = (sys.argv[1]).lower()
        datestr = sys.argv[2]
        timestr = sys.argv[3]
        process = int(sys.argv[4])
        # print satellite, datestr, timestr, process
        getyr = int( datestr[0:4] )
        getmh = int( datestr[4:6] )
        getdy = int( datestr[6:8] )
        gethr = int( timestr[0:2] )
        getmn = int( timestr[2:4] )
        # print getyr, getmh, getdy, gethr, getmn
        getdt = datetime.datetime( getyr, getmh, getdy, gethr, getmn, 0 )
    else:
        print "Usage: inject_nasa_modis.py satname yyyymmdd hhmm procflg"
        print "   procflg = 0 = Normal, procflg = 1 = Reset existing database entry."
        sys.exit()
    
    # Daily geometa data files are in /geoMeta/5/AQUA/yyyy and /geoMeta/5/TERRA/yyyy
    # Replace the 5 with 6 for version 6 processing
    # Retrieval URL's are like ftp://ladsftp.nascom.nasa.gov/allData/5/MYD021KM/2004/214/MYD021KM.A2004214.0130.005.2009213000629.hdf
    
    # Output the start time
    tmpt=time.gmtime()
    tout=( '*** inject_nasa_modis.py - ' + time.asctime(tmpt) + ' ***\n' )
    print tout
    
    # Create a lock file for slow computers
    lockfn= ("%s/inject_nasa_modis.lock" % ANCDIR)
    if os.path.isfile(lockfn) == True:
        print 'Lock exists!'
        sys.exit()
    else:
        # No lock file exists so create one
        fout = open(lockfn, 'w')
        fout.write(tout)
        fout.close()

    # Call injection routine
    [ nextfileid, tarfname, hem, qkmflg, hkmflg, km1flg, cldflg, coverlist ] \
        = inject_nasa_modis( satellite, getdt, THRESHOLD, process, lockfn )

    # Remove the lock file
    os.remove(lockfn)
