#!/usr/bin/python

# Name:          proces_modis.py
# Purpose:       Script to check NASA MODIS NRT archive and download files.
#                Interacts with PostgreSQL+PostGIS 'icecharts' database and 'modis_files'
#                table.  Requires MRTSwath software from NASA
#                Optional command-line argument(s) limit processing to a particular subarea(s).
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, socket, subprocess
import time, datetime
import tarfile

import numpy as N

import multiprocessing as mp

import osgeo.ogr as ogr
import osgeo.osr as osr

import pg

# from modis_processing import load_subset_areas, MRTSwath_process, modis_composite
# from modis_processing import generate_geotiff
from modis_processing import *
from modis_areas import modis_areas

PROGDIR = '/home/bifrostsat/Python/MODIS'
ANCDIR= '/home/bifrostsat/Ancillary/MODIS'
INCDIR= '/home/bifrostsat/Include/MODIS'
BASEDIR = '/home/bifrostsat/Data/MODIS'
DATADIR = ("%s/Raw" % BASEDIR)
TMPDIR = ("%s/tmp" % BASEDIR)
ARCHIVE = ("%s/Archive" % BASEDIR)

THRESHOLD = 5.0
NCPU = 1

active = 1


# Wrapper for generate_geotiff for use with multiprocessing
def wrapper( arg ):
    # print arg[6].name
    generate_geotiff( arg[0], arg[1], arg[2], arg[3], \
        arg[4], arg[5], arg[6], arg[7], arg[8] )


# Routine to process MODIS files
def process_modis( con1, fid, tarfname, satellite, dtstr, hem, qkmflg, hkmflg, km1flg, cldflg, \
    arealist, coverlist ):

    # Number of areas
    arean = len(arealist)

    # Load subset information from Shapefile
    shpfname = ("%s/modis_subsets_ll.shp" % INCDIR)
    [nsubset,subset] = load_subset_areas( shpfname )
    # print nsubset
    # print subset

    # Set raw longitude/latitude projection
    rawprojstr = '+proj=longlat +ellps=WGS84'
    rawproj = osr.SpatialReference()
    rawproj.ImportFromProj4( rawprojstr )
    
    # Uncompress tarfile in temporary directory
    if active == 1:
        # print tarfname
        fname = ("%s/%s" % (DATADIR,tarfname) )
        tarin = tarfile.open( fname, 'r' )
        tarin.extractall( path=TMPDIR )
        tarin.close()
    
    # Assemble date/time string
    # print dtstr
    year = int(dtstr[0:4])
    month = int(dtstr[5:7])
    day = int(dtstr[8:10])
    hour = int(dtstr[11:13])
    minute = int(dtstr[14:16])
    # print year, month, day, hour, minute
    dt = datetime.datetime( year, month, day, hour, minute, 0 )
    jday = dt - datetime.datetime( year-1, 12, 31, hour, minute, 0 )
    # print dt, jday.days
    filedt = ("A%4d%03d.%02d%02d" % (year,jday.days,hour,minute) )
    # print filedt

    dtstr = dt.strftime('%Y%m%d_%H%M')
    if satellite == 'MOD':
        satname = 'terra'
    elif satellite == 'MYD':
        satname = 'aqua'
    else:
        print 'Unknown satellite: ', satellite
        sys.exit()
    if hem == 1:
        areaname = 'Arctic'
    elif hem == -1:
        areaname = 'Antarctic'
    else:
        print 'Unknown hemisphere:', hem
        sys.exit()

    print ("\n%s qkm=%d hkm=%d 1km=%d" % (tarfname,qkmflg,hkmflg,km1flg) )

    if NCPU > 1:

        # Multi-processor code
        pool = mp.Pool(processes=NCPU)              # start 4 worker processes
        result = []
        args = []
        for i in range(arean):
            if coverlist[i] >= THRESHOLD:
                args.append( (qkmflg, hkmflg, km1flg, cldflg, satellite, \
                    dt, filedt, arealist[i], coverlist[i]) )
        jobres = pool.map(wrapper, args )

    else:
        # Loop through subsets and see which need processing
        for i in range(arean):
            # print coverlist[i]
            if coverlist[i] >= THRESHOLD:
                # print ("   %30s %6.2f" % (arealist[i].name,coverlist[i]) )

                # p_gengtiff = Process( generate_geotiff
                generate_geotiff( qkmflg, hkmflg, km1flg, cldflg, satellite, \
                    dt, filedt, arealist[i], coverlist[i] )
    procstatus = 1

    # Delete HDF files
    searchstr2 = filedt
    filelist = os.listdir( TMPDIR )
    for fname in filelist:
        if fname.find( searchstr2 ) != -1:
            os.remove( ("%s/%s" % (TMPDIR,fname)) )

    # Update database                
    # If we get here then we must have some form of procstatus value
    # Update database entry with new values
    tmpt=time.gmtime()
    procdtstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)	
    updatefstr = ("UPDATE modis_files SET processdt = \'%s\', " % procdtstr) + \
        ("procflg = %d " % procstatus) + \
        ("WHERE id = %d;" % fid)
    print updatefstr
    queryres = con1.query(updatefstr)
    # print queryres

    return procstatus
    

# Main callable routine
if __name__ == '__main__':
    
    # Output the start time
    tmpt=time.gmtime()
    tout=( '\n*** process_modis.py - ' + time.asctime(tmpt) + ' ***' )
    print tout
    
    # Create a lock file for slow computers
    lockfn= ("%s/process_modis.lock" % ANCDIR)
    if os.path.isfile(lockfn) == True:
        print 'Lock exists!'
        sys.exit()
    else:
        # No lock file exists so create one
        fout = open(lockfn, 'w')
        fout.write(tout)
        fout.close()

    # Load subset information from Shapefile
    shpfname = ("%s/modis_subsets_ll.shp" % INCDIR)
    [nsubset,subset] = load_subset_areas( shpfname )
    # print nsubset
    # print subset
    
    # Get command-line arguments (if supplied)
    # print len(sys.argv)
    arean = 0
    arealist = []
    if len(sys.argv) > 1:
        for i in range(1,len(sys.argv)):
            for j in range(nsubset):
                if sys.argv[i] == subset[j].shortname:
                    arealist.append(subset[j])
                    arean = arean + 1
    else:
        arealist = subset
        arean = nsubset
    # print arealist[0].geometry
    # print arean
    
    # Interrogate 'Satellite' database table 'modis_files' to see what files are
    # available for processing.
    con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')
    querytxt = "SELECT id,filename,satellite,datetime,areaflg,qkm,hkm,km1,cld"
    for i in range(arean):
        querytxt = ("%s,%s_cover" % (querytxt,arealist[i].shortname) )
    querytxt = ("%s FROM modis_files WHERE procflg = 0 AND size > 0 ORDER BY datetime DESC;" % querytxt )
    print  querytxt
    queryresult = con1.query( querytxt )
    proclist = queryresult.getresult()
    # print proclist
    
    # Loop through items and process
    for itemno in range(len(proclist)):
        item = proclist[itemno]
    
        # Extract parameters from database query result
        fid = item[0]
        tarfname = item[1]
        satellite = item[2]
        dtstr = item[3]
        hem = item[4]
        qkmflg = item[5]
        hkmflg = item[6]
        km1flg = item[7]
        cldflg = item[8]
        coverlist = []
        for i in range(arean):
            coverlist.append( item[9+i] )

        # Call main processing routine
        procstatus = process_modis( con1, fid, tarfname, satellite, dtstr, hem, qkmflg, hkmflg, km1flg, cldflg, \
            arealist, coverlist )
    
    # Remove the lock file
    os.remove(lockfn)
