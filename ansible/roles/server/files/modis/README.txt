# Running MODIS processing

1. Use get_metadata.sh script to download archive metadata for years 2000 to present.

2. Use get_ncep_pw.sh script to download NCEP Reanalysis precipitable water vapour data for years 2000 to present.

3. search_modis_meta.py - Add new metadata files, and output list of scenes where coverage of an area is
   greater than a threshold percentage.
   e.g. python search_modis_meta.py aqua 20150729 20150729 A svalbard 100

4. Copy metadata file information into catalogue using inject_nasa_modis.py, and download files
   e.g. python inject_nasa_modis.py aqua 20150729 1130 0

5. Use process_modis.py to process files.  Use command line option to only process certain areas.
   e.g. python process_modis.py svalbard

6. Cloud_mask.py to calculate cloud mask and percentage cover
   e.g. python cloud_mask.py svalbard
   Needs NCEP Reanalysis pr_wtr for tri-channel test, but this is about 3 days delayed (See step 2).
   
7. Archive raw file
   e.g. python archive_modis.py
   
8. Remove oldest processed files
   e.g. python modis_cleanup.py