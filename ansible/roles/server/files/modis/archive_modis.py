#!/usr/bin/python

# Name:          add_areas_in_db_table.py
# Purpose:       Move MODIS raw (.tar) files to archive once they have been processed.
#                Interacts with PostgreSQL+PostGIS 'Satellite' database and 'modisfiles'
#                table.
#                  delstatus codes:
#                        0 = Not processed
#                    -9001 = Unable to compress raw file
#                    -9002 = Unable to move compressed raw file to archive
#                        1 = Successful archiving of files.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys, subprocess
import string, time

import numpy

import pg

# System dependent settings
BASEDIR = '/home/bifrostsat/Data/MODIS'
DATADIR = ("%s/Raw" % BASEDIR)
ARCHIVEDIR = ("%s/Archive" % BASEDIR)
ANCDIR = '/home/bifrostsat/Ancillary/MODIS'

# Active flag allows debugging
activeflg = 1

# Output the start time
tmpt=time.gmtime()
tout=( '*** archive_modis.py - ' + time.asctime(tmpt) + ' ***\n' )
print tout

# Create a lock file for slow computers
lockfn= ("%s/archive_modis.lock" % ANCDIR)
if os.path.isfile(lockfn) == True:
    print 'Lock exists!'
    sys.exit()
else:
    # No lock file exists so create one
    fout = open(lockfn, 'w')
    fout.write(tout)
    fout.close()

# Connect to database table containing MODIS file information
con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')
    
# Interrogate 'Satellite' database table 'modisfiles' to determine what files need archiving.
sqltxt = "SELECT id,filename,procflg FROM modis_files WHERE procflg > -9000 AND procflg != 0 AND archflg = 0;"
queryresult = con1.query(sqltxt)
proclist = queryresult.getresult()
# print proclist

for item in proclist:
    fid = item[0]
    rootfn = item[1]
    procflg = item[2]
    print "Processing ", rootfn
    archstatus = -9999

    # Move the raw file to the archive directory
    oldfname = ("%s/%s" % (DATADIR,rootfn))
    newfname = ("%s/%s" % (ARCHIVEDIR,rootfn))
    if activeflg == 1:
        try:
            os.rename(oldfname,newfname)
            archstatus = 1
        except:
            archstatus = -9002
            print "  Failed to move file."
    else:
        print ("  %s" % oldfname)
        print ("  %s" % newfname)

    # If we get here then we must have some form of archstatus value
    # Update database entry with new values
    tmpt=time.gmtime()
    archdtstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)	
    updatefstr = ("UPDATE modis_files SET archivedt = \'%s\', " % archdtstr) + \
        ("archflg = %d " % archstatus) + \
        ("WHERE id = %d;" % fid)
    print updatefstr
    if activeflg == 1:
        queryres = con1.query(updatefstr)
        # print queryres

# Remove the lock file
os.remove(lockfn)

