#!/usr/bin/python

# Name:          load_subset_areas.py
# Purpose:       Loads MODIS processing subset areas from Shapefile.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import datetime

import numpy as N

import osgeo.ogr as ogr
import osgeo.osr as osr

from modis_areas import modis_areas


# Set raw longitude/latitude projection
rawprojstr = '+proj=longlat +ellps=WGS84'
rawproj = osr.SpatialReference()
rawproj.ImportFromProj4( rawprojstr )

# Subroutine to load area data from file
def load_subset_areas( shpfname ):
    # print shpfname

    # Load subset area data
    subset = []
    ds = ogr.Open( shpfname )
    lyr = ds.GetLayerByName( 'modis_subsets_ll' )
    lyr.ResetReading()
    for feat in lyr:
        feat_defn = lyr.GetLayerDefn()
        for i in range(feat_defn.GetFieldCount()):
            field_defn = feat_defn.GetFieldDefn(i)
            # print dir(field_defn)
            # print field_defn.name, feat.GetField(i)
            if field_defn.name == 'name':
                areaname = feat.GetField(i)
            elif field_defn.name == 'hem':
                hemisphere = feat.GetField(i)
            elif field_defn.name == 'PS_rotatio':
                rotation = feat.GetField(i)
            elif field_defn.name == 'shortname':
                shortname = feat.GetField(i)
        # print areaname, hemisphere, rotation, shortname

        # Projection string according to hemisphere and rotation
        if rotation >= 0:
            rotstr = ("%de" % rotation)
        else:
            rotstr = ("%dw" % rotation)
        if hemisphere == 1:
            projstr = ("+proj=stere +lat_0=90n +lon_0=%s +lat_ts=90n +ellps=WGS84" % rotstr)
        elif hemisphere == -1:
            projstr = ("+proj=stere +lat_0=90s +lon_0=%s +lat_ts=90s +ellps=WGS84" % rotstr)
        else:
            print 'Unknown hemisphere.'
            sys.exit()
        # print projstr
        projection = osr.SpatialReference()
        projection.ImportFromProj4( projstr )

        # Set coordinate transformation
        projtrans = osr.CoordinateTransformation( rawproj, projection )

        # Get the geometry
        geometry = feat.GetGeometryRef()
        # print geometry
        geometry.Transform( projtrans )
        # print geometry
        newwkt = geometry.ExportToWkt()
        # print newwkt

        areaval = modis_areas( areaname, hemisphere, rotation, shortname, \
            projstr, newwkt )
        subset.append( areaval )

    ds = None

    # print subset
    nsubset = len(subset)
    # print nsubset

    return [nsubset, subset]
