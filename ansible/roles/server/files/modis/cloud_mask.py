#!/usr/bin/python

# Name:          clooud_mask.py
# Purpose:       Script to generate cloud mask from processed MODIS files.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import time, datetime
import tarfile
import subprocess
import argparse

import numpy as N

import osgeo.gdal as gdal
from osgeo.gdalconst import *

import pg

from modis_processing import load_subset_areas, modis_calibration, modis_calcbt
from ice_mask import ice_mask
import cloud_tests
import calculate_cover

PROGDIR = '/home/bifrostsat/Python/MODIS'
ANCDIR = '/home/bifrostsat/Ancillary/MODIS'
INCDIR = '/home/bifrostsat/Include/MODIS'
BASEDIR = '/home/bifrostsat/Data/MODIS'
DATADIR = ("%s/Raw" % BASEDIR)
TMPDIR = ("%s/tmp" % BASEDIR)
ARCHIVE = ("%s/Archive" % BASEDIR)
SCRIPTDIR = '/home/bifrostsat/Scripts/MODIS'

THRESHOLD = 5.0
NCPU = 1

active = 1
debug = 0

def enlarge(a, x=2, y=None):
    """Enlarges 2D image array a using simple pixel repetition in both dimensions.
    Enlarges by factor x horizontally and factor y vertically.
    If y is left as None, uses factor x for both dimensions."""
    a = N.asarray(a)
    assert a.ndim == 2
    if y == None:
        y = x
    for factor in (x, y):
        assert factor.__class__ == int
        assert factor > 0
    return a.repeat(y, axis=0).repeat(x, axis=1)


# Routine to generate cloud mask
def cloud_mask( con1, fid, tarfname, satellite, dtstr, hem, qkmflg, hkmflg, km1flg, \
    arealist, coverlist ):

    # Number of areas
    arean = len(arealist)


    if hem == 1:
        hemname = 'Arctic'
    elif hem == -1:
        hemname = 'Antarctic'
    else:
        print 'Unknown hemisphere value', hem
        sys.exit()
    if satellite == 'MOD':
        satname = 'terra'
    elif satellite == 'MYD':
        satname = 'aqua'
    else:
        print 'Unknown satellite', satellite
        sys.exit()

    valstr = ""

    # Loop through subsets and see which need processing
    for i in range(arean):
        if coverlist[i] >= THRESHOLD:

            # Assemble date/time string
            dtstr = tarfname[6:19]
            # print dtstr
            year = int(dtstr[0:4])
            month = int(dtstr[4:6])
            day = int(dtstr[6:8])
            hour = int(dtstr[9:11])
            minute = int(dtstr[11:13])
            # print year, month, day, hour, minute
            dt = datetime.datetime( year, month, day, hour, minute, 0 )
            jday = dt - datetime.datetime( year-1, 12, 31, hour, minute, 0 )
            # print dt, jday.days

            # Only do more processing if active
            if active == 1:
                # Construct the root filename
                rootfn = ("modis_%s_%s_%s" % (arealist[i].shortname,dtstr,satname))

                # Uncompress processed tarfile in temporary directory
                fname = ("%s/%s/Processed/%s.tar.bz2" % \
                    (BASEDIR,hemname,rootfn) )
                print ("\n%s" % fname)
                tarin = tarfile.open( fname, 'r:bz2' )
                tarin.extractall( path=TMPDIR )
                tarin.close()

                # Load land/sea mask
                lsmskfname = ("%s/%s_LandSeaMask.tif" % (TMPDIR,rootfn))
                lsmsk_ds = gdal.Open( lsmskfname, GA_ReadOnly )
                cols = lsmsk_ds.RasterXSize
                rows = lsmsk_ds.RasterYSize
                geotransform = lsmsk_ds.GetGeoTransform()
                originX = geotransform[0]
                originY = geotransform[3]
                pixelWidth = geotransform[1]
                pixelHeight = geotransform[5]
                projection = lsmsk_ds.GetProjection()
                lsmsk_b1 = lsmsk_ds.GetRasterBand(1)
                lsmsk = lsmsk_ds.ReadAsArray()
                lsmsk_ds = None

                # print rows, cols
                # print lsmsk.shape
                # print geotransform
                # print projection

                # Generate ice mask, from (in order of preference):
                #    Ice chart data
                #    Ice forecast data
                #    OSI SAF ice concentration
                # Ice mask should include GSHHS land mask
                icemsk = ice_mask( rootfn, hemname, cols, rows, projection, geotransform )

                # Load channel data
                ch22 = modis_calibration( rootfn, '22', 0, 1 )
                ch26ref = modis_calibration( rootfn, '26', 1, 1 )
                ch27 = modis_calibration( rootfn, '27', 0, 1 )
                ch31 = modis_calibration( rootfn, '31', 0, 1 )

                if qkmflg == 1:
                    ch1ref = modis_calibration( rootfn, '1', 1, 1 )
                    ch2ref = modis_calibration( rootfn, '2', 1, 1 )
                if hkmflg == 1:
                    ch5ref = modis_calibration( rootfn, '5', 1, 1 )
                ch19ref = modis_calibration( rootfn, '19', 1, 1 )

                ch29 = modis_calibration( rootfn, '29', 0, 1 )
                ch32 = modis_calibration( rootfn, '32', 0, 1 )

                ch28 = modis_calibration( rootfn, '28', 0, 1 )
                ch36 = modis_calibration( rootfn, '36', 0, 1 )

                # Calculate brightness temperature
                bt22 = modis_calcbt( ch22, '22' )
                bt27 = modis_calcbt( ch27, '27' )
                bt31 = modis_calcbt( ch31, '31' )

                bt29 = modis_calcbt( ch29, '29' )
                bt32 = modis_calcbt( ch32, '32' )

                bt28 = modis_calcbt( ch27, '28' )
                bt36 = modis_calcbt( ch36, '36' )

                # Create new cloud mask array
                cloud = N.zeros( (rows,cols), dtype=N.uint8 )
                # print cloud.shape

                # Cloud mask components as per ATBD
                basic = N.zeros( (rows,cols), dtype=N.uint8 )
                additional1 = N.zeros( (rows,cols), dtype=N.uint16 )
                additional2 = N.zeros( (rows,cols), dtype=N.uint8 )
                cloud250 = N.zeros( (rows,cols), dtype=N.uint16 )

                # Set processing path flags

                # Set the Day/Night flag (uses Solar Zenith)
                szenfname = ("%s/%s_SolarZenith.tif" % (TMPDIR,rootfn))
                szen_ds = gdal.Open( szenfname, GA_ReadOnly )
                szen_b1 = szen_ds.GetRasterBand(1)
                szen = szen_ds.ReadAsArray()
                szen_ds = None
                # NEXT LINE IS IN RADIANS, PERHAPS IT SHOULD BE LEFT AS DEGREES?
                # szen = N.radians( szen * 0.01 )
                szen = szen * 0.01
                dayidx = N.nonzero(szen<85)
                basic[dayidx] = basic[dayidx] + 8
                szen = N.radians( szen )

                # Report on percentage of night
                totalpix = rows * cols
                percent_night = 100.0 - ((100.0 / totalpix) * len(dayidx[0]))

                # Set the Sun Glint flag (using Viewing Zenith
                vzenfname = ("%s/%s_SensorZenith.tif" % (TMPDIR,rootfn))
                vzen_ds = gdal.Open( vzenfname, GA_ReadOnly )
                vzen_b1 = vzen_ds.GetRasterBand(1)
                vzen = vzen_ds.ReadAsArray()
                vzen_ds = None
                vzen = N.radians( vzen * 0.01 )
                vazifname = ("%s/%s_SensorAzimuth.tif" % (TMPDIR,rootfn))
                vazi_ds = gdal.Open( vazifname, GA_ReadOnly )
                vazi_b1 = vazi_ds.GetRasterBand(1)
                vazi = vazi_ds.ReadAsArray()
                vazi_ds = None
                vazi = N.radians( vazi * 0.01 )
                rsa = (N.sin( vzen ) * N.sin( szen ) * N.cos( vazi )) + \
                    (N.cos( vzen ) * N.cos( szen ))
                rsa = N.degrees( rsa )
                # print rsa.min(), rsa.max()
                basic[N.nonzero(rsa>36)] = basic[N.nonzero(rsa>36)] + 16

                # Set the Snow/Ice Background flag
                basic[N.nonzero(icemsk==0)] == basic[N.nonzero(icemsk==0)] + 32

                # Set the Land/Water flag
                basic[N.nonzero(lsmsk==1)] == basic[N.nonzero(lsmsk==1)] + 128 + 64

                # Set execution flag
                basic[N.nonzero(lsmsk==1)] = basic[N.nonzero(lsmsk==1)] + 1

                print ("\nCloud Tests (%5.1f%% night):" % percent_night)

                # Simple threshold test
                [simple, simple_conf] = cloud_tests.simple( bt31, icemsk )
                idx = N.nonzero(simple>0)
                cloud[idx] = 200
                additional1[idx] = additional1[idx] + 32

                # BT_11 - BT_12 and BT_8.6 - BT_11 (p.28)
                # Needs total precipitable water vapour (PW) value?
                # Open water only
                [trichn] = cloud_tests.trichannel( bt29, bt31, bt32, icemsk, dt )
                if trichn != None:
                    idx = N.nonzero(trichn>0)
                    cloud[idx] = 195
                    additional1[idx] = additional1[idx] + 1024


                # Select test basd on whether it is day or night
                if percent_night < 70:

                    # BT_11 - BT_3.9 Test (p.32) (see Table 6 Ackerman et al)
                    # Stripey problem in earlier images
                    # NB: This test is challenging at night-time in high latitudes!
                    [cloud11, cloud11_conf, cloud11_area, cloud11_diff] = cloud_tests.bt11_bt3_9( bt31, bt22, \
                        lsmsk, icemsk, dayidx )
                    idx = N.nonzero(cloud11>0)
                    idx2 = N.nonzero(cloud11_conf==0)
                    # Disable to test winter images
                    cloud[idx] = 190
                    cloud[idx2] = 0
                    additional1[idx] = additional1[idx] + 2048
                    additional1[idx2] = additional1[idx2] - 2048 
                    # For debug, output area covered by surface types
                    if debug == 1:
                        surfacefn = ("%s/%s/Processed/%s_surfaces.tif" \
                            % (BASEDIR,hemname,rootfn))
                        format = "GTiff"
                        driver = gdal.GetDriverByName( format )
                        surface_ds = driver.Create( surfacefn, cols, rows, 1, gdal.GDT_Byte, \
                            [ 'COMPRESS=LZW' ] )
                        surface_ds.SetGeoTransform( geotransform )
                        surface_ds.SetProjection( projection )
                        surface_ds.GetRasterBand(1).WriteArray( cloud11_area )
                        surface_ds = None

                else:
                    # Improved polar night-time tests, after
                    # Liu, Y., Key, J.R., Frey, R.A., Ackerman, S.A., and Menzel, W.P. (2004).
                    # Nighttime polar cloud detection with MODIS.
                    # Remote sensing of environment, 92(2), 181-194.
                    [ polar_cloud, cloud11_procarea, cloud11_diff, cloud11_thres, cloud11_diff2, cloud11_thres2 ] \
                        = cloud_tests.liu_polar_night( bt22, bt28, bt31, bt36, lsmsk, icemsk, dayidx, hemname )
                    idx = N.nonzero(polar_cloud>0)
                    cloud[idx] = 191
                    additional1[idx] = additional1[idx] + 512

                    if debug == 1:
                        surfacefn = ("%s/%s/Processed/%s_procarea.tif" \
                            % (BASEDIR,hemname,rootfn))
                        format = "GTiff"
                        driver = gdal.GetDriverByName( format )
                        surface_ds = driver.Create( surfacefn, cols, rows, 1, gdal.GDT_Byte, \
                            [ 'COMPRESS=LZW' ] )
                        surface_ds.SetGeoTransform( geotransform )
                        surface_ds.SetProjection( projection )
                        surface_ds.GetRasterBand(1).WriteArray( cloud11_procarea )
                        surface_ds = None

                        polarcldfn = ("%s/%s/Processed/%s_polar_cloud.tif" \
                            % (BASEDIR,hemname,rootfn))
                        format = "GTiff"
                        driver = gdal.GetDriverByName( format )
                        polarcld_ds = driver.Create( polarcldfn, cols, rows, 1, gdal.GDT_Byte, \
                            [ 'COMPRESS=LZW' ] )
                        polarcld_ds.SetGeoTransform( geotransform )
                        polarcld_ds.SetProjection( projection )
                        polarcld_ds.GetRasterBand(1).WriteArray( polar_cloud )
                        polarcld_ds = None

                        difffn = ("%s/%s/Processed/%s_bt31.tif" \
                            % (BASEDIR,hemname,rootfn))
                        format = "GTiff"
                        driver = gdal.GetDriverByName( format )
                        diff_ds = driver.Create( difffn, cols, rows, 1, gdal.GDT_Float32, \
                            [ 'COMPRESS=LZW' ] )
                        diff_ds.SetGeoTransform( geotransform )
                        diff_ds.SetProjection( projection )
                        diff_ds.GetRasterBand(1).WriteArray( bt31 )
                        diff_ds = None

                        difffn = ("%s/%s/Processed/%s_threshold_1.tif" \
                            % (BASEDIR,hemname,rootfn))
                        format = "GTiff"
                        driver = gdal.GetDriverByName( format )
                        diff_ds = driver.Create( difffn, cols, rows, 1, gdal.GDT_Float32, \
                            [ 'COMPRESS=LZW' ] )
                        diff_ds.SetGeoTransform( geotransform )
                        diff_ds.SetProjection( projection )
                        diff_ds.GetRasterBand(1).WriteArray( cloud11_thres )
                        diff_ds = None

                        difffn = ("%s/%s/Processed/%s_difference_1.tif" \
                            % (BASEDIR,hemname,rootfn))
                        format = "GTiff"
                        driver = gdal.GetDriverByName( format )
                        diff_ds = driver.Create( difffn, cols, rows, 1, gdal.GDT_Float32, \
                            [ 'COMPRESS=LZW' ] )
                        diff_ds.SetGeoTransform( geotransform )
                        diff_ds.SetProjection( projection )
                        diff_ds.GetRasterBand(1).WriteArray( cloud11_diff )
                        diff_ds = None

                        difffn = ("%s/%s/Processed/%s_threshold_2.tif" \
                            % (BASEDIR,hemname,rootfn))
                        format = "GTiff"
                        driver = gdal.GetDriverByName( format )
                        diff_ds = driver.Create( difffn, cols, rows, 1, gdal.GDT_Float32, \
                            [ 'COMPRESS=LZW' ] )
                        diff_ds.SetGeoTransform( geotransform )
                        diff_ds.SetProjection( projection )
                        diff_ds.GetRasterBand(1).WriteArray( cloud11_thres2 )
                        diff_ds = None

                        difffn = ("%s/%s/Processed/%s_difference_2.tif" \
                            % (BASEDIR,hemname,rootfn))
                        format = "GTiff"
                        driver = gdal.GetDriverByName( format )
                        diff_ds = driver.Create( difffn, cols, rows, 1, gdal.GDT_Float32, \
                            [ 'COMPRESS=LZW' ] )
                        diff_ds.SetGeoTransform( geotransform )
                        diff_ds.SetProjection( projection )
                        diff_ds.GetRasterBand(1).WriteArray( cloud11_diff2 )
                        diff_ds = None

                # BT_3.7 - BT_12 (p.34)
                # Land only

                # BT_7.3 - BT_11 (p.34)
                # Land only

                # High cloud test (p.35)
                [hicloud,hicloud_conf] = cloud_tests.highcloud( bt27, lsmsk )
                idx = N.nonzero(hicloud>0)
                cloud[idx] = 180
                additional1[idx] = additional1[idx] + 128

                # NIR Band 26 test (Cirrus detection) (p.38)
                [nir26,cirrus_nir26,nir26_conf] = cloud_tests.nir_band26( ch26ref, lsmsk )
                idx = N.nonzero(nir26>0)
                cloud[idx] = 170
                additional1[idx] = additional1[idx] + 256
                cirrusidx = N.nonzero(cirrus_nir26>0)
                cloud[cirrusidx] = 165
                additional1[cirrusidx] = additional1[cirrusidx] + 2

                # Save NIR Band 26 as a GeoTIFF
                cloudfname = ("%s/%s_nir26.tif" % (TMPDIR,rootfn))
                format = "GTiff"
                driver = gdal.GetDriverByName( format )
                cloud_ds = driver.Create( cloudfname, cols, rows, 1, gdal.GDT_Float32 )
                cloud_ds.SetGeoTransform( geotransform )
                cloud_ds.SetProjection( projection )
                cloud_ds.GetRasterBand(1).WriteArray( ch26ref )
                cloud_ds = None

                # IR Thin Cirrus test (p.40)

                # Cloud shadow detection (p.40)
                if qkmflg == 1 and hkmflg == 1:
                    shadowmsk = cloud_tests.shadow( ch1ref, ch2ref, ch5ref, ch19ref, lsmsk )
                    idx = N.nonzero(shadowmsk>0)
                    # cloud[idx] = 50
                    additional1[idx] = additional1[idx] + 4

                # Visible reflectance test (p.41)
                if qkmflg == 1:
                    [viscloud,viscloud_conf] = cloud_tests.visible_reflectance( ch2ref, \
                        icemsk, dayidx )
                    idx = N.nonzero(viscloud>0)
                    cloud[idx] = 150
                    additional1[idx] = additional1[idx] + 4096

                # Reflectance ratio test (p.43)
                if qkmflg == 1:
                    [refratio,refratio_conf] = cloud_tests.reflectance_ratio( ch1ref, ch2ref, \
                        icemsk, dayidx )
                    idx = N.nonzero(refratio>0)
                    cloud[idx] = 140
                    additional1[idx] = additional1[idx] + 8196

                # Night Ocean spatial variability test (p.45)

                # print '\n', N.unique( additional1 )

                # Apply land mask to cloud mask as we are only interested in sea areas
                cloud[N.nonzero(lsmsk==1)] = 255

                # Output cloud mask values for debugging
                # print N.unique(cloud)

                # Save cloud mask as a GeoTIFF
                cloudfname = ("%s/%s/Processed/%s_cloud.tif" \
                    % (BASEDIR,hemname,rootfn))
                format = "GTiff"
                driver = gdal.GetDriverByName( format )
                cloud_ds = driver.Create( cloudfname, cols, rows, 1, gdal.GDT_Byte, \
                    [ 'COMPRESS=LZW' ] )
                cloud_ds.SetGeoTransform( geotransform )
                cloud_ds.SetProjection( projection )
                cloud_ds.GetRasterBand(1).WriteArray( additional1 )
                cloud_ds = None

                # Create combination masked images
                cmd = ("%s/pseudocolor.sh %s %s ch2 2> %s/pseudocolor.log" % \
                    (SCRIPTDIR,hemname,rootfn,TMPDIR))
                # print cmd
                p = subprocess.Popen(cmd, shell=True)
                sts = os.waitpid(p.pid, 0)
                cmd = ("%s/pseudocolor.sh %s %s ch31 2> %s/pseudocolor.log" % \
                    (SCRIPTDIR,hemname,rootfn,TMPDIR))
                # print cmd
                p = subprocess.Popen(cmd, shell=True)
                sts = os.waitpid(p.pid, 0)

                # Clean up, remove unpacked data files
                # searchstr1 = ("modis_%s_%s_%s" % (subset.shortname,dtstr,satname) )
                filelist = os.listdir( TMPDIR )
                for fname in filelist:
                    if fname.find( rootfn ) != -1:
                        os.remove( ("%s/%s" % (TMPDIR,fname)) )

                # Calculate cloud cover
                cldcov = calculate_cover.calculate_cover( cloudfname )

                # Add information to the values string
                valstr = ("%s, %s_cloud = %f" % (valstr,arealist[i].shortname,cldcov))

                # Remove pseudocolor.log file
                os.remove( ("%s/pseudocolor.log" % TMPDIR) )

    # If we get here, then successful so processing status should be 1
    procstatus = 1

    # Update database with values
    tmpt=time.gmtime()
    procdtstr = time.strftime("%Y-%m-%d %H:%M:%S", tmpt)	
    updatefstr = ("UPDATE modis_files SET clouddt = \'%s\', " % procdtstr) + \
        ("cldflg = %d" % procstatus) + \
        ("%s " % valstr) + \
        ("WHERE id = %d;" % fid)
    print updatefstr, '\n'
    queryres = con1.query(updatefstr)
    # print queryres


# Main callable routine
if __name__ == '__main__':

    # Output the start time
    tmpt=time.gmtime()
    tout=( '\n*** cloud_mask.py - ' + time.asctime(tmpt) + ' ***' )
    print tout

    # Create a lock file for slow computers
    lockfn= ("%s/cloud_mask.lock" % ANCDIR)
    if os.path.isfile(lockfn) == True:
        print 'Lock exists!'
        sys.exit()
    else:
        # No lock file exists so create one
        fout = open(lockfn, 'w')
        fout.write(tout)
        fout.close()

    # Load subset information from Shapefile
    shpfname = ("%s/modis_subsets_ll.shp" % INCDIR)
    [nsubset,subset] = load_subset_areas( shpfname )
    # print nsubset
    # print subset

    # Get command-line arguments (if supplied)
    # print len(sys.argv)
    arean = 0
    arealist = []
    if len(sys.argv) > 1:
        for i in range(1,len(sys.argv)):
            for j in range(nsubset):
                if sys.argv[i] == subset[j].shortname:
                    arealist.append(subset[j])
                    arean = arean + 1
    else:
        arealist = subset
        arean = nsubset
    # print arealist
    # print arean

    # Interrogate 'Satellite' database table 'modis_files' to see what files are
    # available for processing.
    con1 = pg.connect(dbname='Satellite', host='localhost', user='bifrostsat', passwd='bifrostsat')
    querytxt = "SELECT id,filename,satellite,datetime,areaflg,qkm,hkm,km1"
    for i in range(arean):
        querytxt = ("%s,%s_cover" % (querytxt,arealist[i].shortname) )
    querytxt = ("%s FROM modis_files WHERE cldflg = 0 AND procflg != 0 ORDER BY datetime DESC;" % querytxt )
    # querytxt = ("%s FROM modis_files WHERE cldflg = 0 AND procflg != 0 AND date_part('year',datetime) < 2006 ORDER BY datetime DESC;" % querytxt )
    # querytxt = ("%s FROM modis_files WHERE cldflg = 0 AND procflg != 0 AND id = 478 ORDER BY datetime DESC;" % querytxt )
    # print querytxt
    queryresult = con1.query( querytxt )
    proclist = queryresult.getresult()
    # print proclist

    # Loop through items and process
    for itemno in range(len(proclist)):
        item = proclist[itemno]

        # Extract parameters from database query result
        fid = item[0]
        tarfname = item[1]
        satellite = item[2]
        dtstr = item[3]
        hem = item[4]
        qkmflg = item[5]
        hkmflg = item[6]
        km1flg = item[7]
        coverlist = []
        for i in range(arean):
            coverlist.append( item[8+i] )

        # Call main processing routine
        procstatus = cloud_mask( con1, fid, tarfname, satellite, dtstr, hem, qkmflg, hkmflg, km1flg, \
            arealist, coverlist )

    # Remove the lock file
    os.remove(lockfn)
