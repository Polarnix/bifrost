#!/usr/bin/python

import os, sys

import numpy as N
import scipy.interpolate as inter

import prwtr_extraction

BASEDIR = '/home/bifrostsat/Data/MODIS'
TMPDIR = ("%s/tmp" % BASEDIR)

# Simple threshold test on ch31 (BT11) (p.27)
def simple( bt31, icemsk ):
    print '  Simple BT Threshold Test'

    # Get array size
    [rows,cols] = bt31.shape

    # Output cloud mask
    cloudmsk = N.zeros( (rows,cols), dtype=N.uint8 )

    area = N.zeros( (rows,cols), dtype=N.uint8 )
    # print area.shape
    # print icemsk.shape
    owidx = N.nonzero(icemsk==0)
    # print owidx
    area[N.nonzero(bt31<270)] = 1
    area[owidx] = area[owidx] + 1
    cloudidx = N.nonzero(area==2)

    cloudmsk[cloudidx] = 1

    # Calculate confidence
    confidence = N.zeros( (rows,cols), dtype=N.float32 )
    confidence[owidx] = (bt31[owidx] - 267.0) / 6.0
    confidence[N.nonzero(confidence<0)] = 0.0
    confidence[N.nonzero(confidence>1)] = 1.0
    # print confidence.min(), confidence.max()
    # print N.histogram( confidence, bins=10 )

    return [cloudmsk, confidence]


# BT_11 - BT_3.9 Test (p.32) (see Table 6 Ackerman et al)
# Stripey problem in earlier images
# NB: This test is challenging at night-time in high latitudes!
def bt11_bt3_9( bt31, bt22, lsmsk, icemsk, dayidx ):
    print '  BT11 - BT3_9 Test'

    # Get array size
    [rows,cols] = bt31.shape

    # Output cloud mask
    cloudmsk = N.zeros( (rows,cols), dtype=N.uint8 )

    owidx = N.nonzero(icemsk==0)

    # Difference between BT11 and BT3.9
    diff = bt31 - bt22

    # Areas covered by different day/night water/ice combinations
    area = N.zeros( (rows,cols), dtype=N.uint8 )
    area[N.nonzero(lsmsk!=1)] = 1
    area[owidx] = area[owidx] + 2
    area[dayidx] = area[dayidx] + 4
    # print N.unique(area)
    # print len(N.nonzero(area==4)[0])

    # Confidence values
    confidence = N.zeros( (rows,cols), dtype=N.float32 )

    # Day Ocean
    dodiff = N.zeros( (rows,cols), dtype=N.uint8 )
    dodiff[N.nonzero(area==7)] = 1
    diffidx = N.nonzero(diff<-8)
    dodiff[diffidx] = dodiff[diffidx] + 1
    doidx = N.nonzero(dodiff==2)
    cloudmsk[doidx] = 1
    doidx = N.nonzero(area==7)
    confidence[doidx] = (diff[doidx] - (-10)) / 4.0

    # Night Ocean
    nodiff = N.zeros( (rows,cols), dtype=N.uint8 )
    nodiff[N.nonzero(area==3)] = 1
    diffidx = N.nonzero(diff<=1.0)
    nodiff[diffidx] = nodiff[diffidx] + 1
    noidx = N.nonzero(nodiff==2)
    cloudmsk[noidx] = cloudmsk[noidx] + 1
    noidx = N.nonzero(area==3)
    confidence[noidx] = (diff[noidx] - (-10)) / 4.0

    # Day Snow/Ice
    didiff = N.zeros( (rows,cols), dtype=N.uint8 )
    didiff[N.nonzero(area==5)] = 1
    diffidx = N.nonzero(diff<-7)
    didiff[diffidx] = didiff[diffidx] + 1
    diidx = N.nonzero(didiff==2)
    cloudmsk[diidx] = cloudmsk[diidx] + 1
    diidx = N.nonzero(area==5)
    confidence[diidx] = (diff[diidx] - (-10)) / 4.0

    # Night Snow/Ice
    nsithres = 0.5     # +0.6 K in literature
    nidiff = N.zeros( (rows,cols), dtype=N.uint8 )
    nidiff[N.nonzero(area==1)] = 1
    # diffidx = N.nonzero(diff<nsithres)
    diffidx = N.nonzero(diff>=nsithres)
    nidiff[diffidx] = nidiff[diffidx] + 1
    niidx = N.nonzero(nidiff==2)
    cloudmsk[niidx] = cloudmsk[niidx] + 1
    niidx = N.nonzero(area==1)
    confidence[niidx] = (diff[niidx] - (-10)) / 4.0

    # print N.unique(cloudmsk)

    # Final adjustment of confidence values
    confidence[N.nonzero(confidence<0)] = 0.0
    confidence[N.nonzero(confidence>1)] = 1.0
    # print confidence.min(), confidence.max()
    # print N.histogram( confidence, bins=10 )

    return [cloudmsk, confidence, area, diff]


# High cloud test (p.35)
def highcloud( bt27, lsmsk ):
    print '  High Cloud Test'

    # Get array size
    [rows,cols] = bt27.shape

    # Output cloud mask
    cloudmsk = N.zeros( (rows,cols), dtype=N.uint8 )

    area = N.zeros( (rows,cols), dtype=N.uint8 )
    # print area.shape
    # print icemsk.shape
    seaidx = N.nonzero(lsmsk!=1)
    # print owidx
    area[N.nonzero(bt27<220)] = 1
    area[seaidx] = area[seaidx] + 1
    cloudidx = N.nonzero(area==2)

    cloudmsk[cloudidx] = 1
    # print N.unique(cloudmsk)

    # Calculate confidence
    confidence = N.zeros( (rows,cols), dtype=N.float32 )
    confidence[seaidx] = (bt27[seaidx] - 215.0) / 10.0
    confidence[N.nonzero(confidence<0)] = 0.0
    confidence[N.nonzero(confidence>1)] = 1.0
    # print confidence.min(), confidence.max()
    # print N.histogram( confidence, bins=10 )

    return [cloudmsk, confidence]


# NIR Band 26 test (Cirrus detection) (p.38)
def nir_band26( ch26, lsmsk ):
    print '  NIR Band 26 Test'

    threshold = 0.035
    lower = 0.03
    upper = 0.04
    rng = upper - lower

    # Get array size
    [rows,cols] = ch26.shape

    # Output cloud mask
    cloudmsk = N.zeros( (rows,cols), dtype=N.uint8 )
    cirrusmsk = N.zeros( (rows,cols), dtype=N.uint8 )

    area = N.zeros( (rows,cols), dtype=N.uint8 )
    # print area.shape
    # print icemsk.shape
    seaidx = N.nonzero(lsmsk!=1)
    # print owidx
    area[N.nonzero(ch26>threshold)] = 1
    area[seaidx] = area[seaidx] + 1
    cloudidx = N.nonzero(area==2)

    cloudmsk[cloudidx] = 1
    # print N.unique(cloudmsk)

    # Cirrus area
    cirrusmsk = N.zeros( (rows,cols), dtype=N.uint8 )
    cirrusmsk[cloudidx] = 1
    cirrusmsk[N.nonzero(ch26>upper)] = 0

    # Calculate confidence
    confidence = N.zeros( (rows,cols), dtype=N.float32 )
    confidence[seaidx] = (ch26[seaidx] - lower) / rng
    confidence[N.nonzero(confidence<0)] = 0.0
    confidence[N.nonzero(confidence>1)] = 1.0
    confidence[seaidx] = 1.0 - confidence[seaidx]
    # print confidence.min(), confidence.max()
    # print N.histogram( confidence, bins=10 )

    return [cloudmsk, cirrusmsk, confidence]


# Cloud shadow detection (p.40)
def shadow( ch1ref, ch2ref, ch5ref, ch19ref, lsmsk ):
    print '  Cloud Shadow Test'

    # Get array size
    [rows,cols] = ch1ref.shape

    # Output cloud mask
    shadowmsk = N.zeros( (rows,cols), dtype=N.uint8 )

    area = N.zeros( (rows,cols), dtype=N.uint8 )
    ch19idx = N.nonzero(ch19ref<0.07)
    area[ch19idx] = area[ch19idx] + 1
    ch2ch1ratio = ch2ref / ch1ref
    ch1_2idx = N.nonzero(ch2ch1ratio>0.3)
    area[ch1_2idx] = area[ch1_2idx] + 1
    ch5idx = N.nonzero(ch5ref<0.2)
    area[ch5idx] = area[ch5idx] + 1
    shadowidx = N.nonzero(area==3)

    shadowmsk[shadowidx] = 1

    return shadowmsk


# Visible reflectance test (p.41)
#    NB Dependent on sun glint
def visible_reflectance( ch2ref, icemsk, dayidx ):
    print '  Visible Reflectance'

    # Get array size
    [rows,cols] = ch2ref.shape

    # Output cloud mask
    cloudmsk = N.zeros( (rows,cols), dtype=N.uint8 )

    area = N.zeros( (rows,cols), dtype=N.uint8 )
    # print area.shape
    # print icemsk.shape
    owidx = N.nonzero(icemsk==0)
    # print owidx
    area[N.nonzero(ch2ref>0.055)] = 1
    area[owidx] = area[owidx] + 1
    area[dayidx] = area[dayidx] + 1
    cloudidx = N.nonzero(area==3)

    cloudmsk[cloudidx] = 1
    # print N.unique(cloudmsk)

    # Calculate confidence
    confidence = N.zeros( (rows,cols), dtype=N.float32 )
    confidence[owidx] = (ch2ref[owidx] - 0.045) / 0.02
    confidence[N.nonzero(confidence<0)] = 0.0
    confidence[N.nonzero(confidence>1)] = 1.0
    confidence[owidx] = 1.0 - confidence[owidx]
    # print confidence.min(), confidence.max()
    # print N.histogram( confidence, bins=10 )

    return [cloudmsk, confidence]


# Reflectance ratio test (p.43)
def reflectance_ratio( ch1ref, ch2ref, icemsk, dayidx ):
    print '  Reflectance Ratio'

    # Get array size
    [rows,cols] = ch1ref.shape

    # Output cloud mask
    cloudmsk = N.zeros( (rows,cols), dtype=N.uint8 )

    area = N.zeros( (rows,cols), dtype=N.uint8 )
    # print area.shape
    # print icemsk.shape
    owidx = N.nonzero(icemsk==0)
    # print owidx
    refratio = ch2ref / ch1ref
    area[N.nonzero(refratio>1.9)] = 1
    area[owidx] = area[owidx] + 1
    area[dayidx] = area[dayidx] + 1
    cloudidx = N.nonzero(area==3)

    cloudmsk[cloudidx] = 1
    # print N.unique(cloudmsk)

    # Calculate confidence
    confidence = N.zeros( (rows,cols), dtype=N.float32 )
    confidence[owidx] = (ch2ref[owidx] - 1.85) / 0.1
    confidence[N.nonzero(confidence<0)] = 0.0
    confidence[N.nonzero(confidence>1)] = 1.0
    confidence[owidx] = 1.0 - confidence[owidx]
    # print confidence.min(), confidence.max()
    # print N.histogram( confidence, bins=10 )

    return [cloudmsk, confidence]


# BT_11 - BT_12 and BT_8.6 - BT_11 (p.28)
def trichannel( bt29, bt31, bt32, icemsk, dt ):
    print '  Tri-Channel Test'

    diff8_6 = bt29 - bt31
    # print diff8_6.min(), diff8_6.max()
    diff12 = bt31 - bt32
    # print diff12.min(), diff12.max()

    # Get Precipitable Water Vapour (values in kg/m2)
    # projsrc_fn = '/disk1/nicholsh/IGS2014/MODIS/Arctic/GeoTIFF/modis_negreenlnd_20130930_1310_terra_ch2.tif'
    # projsrc_fn = ("/disk1/nicholsh/IGS2014/MODIS/tmp/modis_negreenlnd_%4d%02d%02d_%02d%02d_terra_EV_1KM_Emissive_b8.tif" % \
    #     (dt.year,dt.month,dt.day,dt.hour,dt.minute))
    # print dt
    flist = os.listdir(TMPDIR)
    for fname in flist:
        if fname.find( dt.strftime('%Y%m%d_%H%M') ) > -1 and fname.find('LandSeaMask.tif') > -1:
            projsrc_fn = ("%s/%s" % (TMPDIR,fname))
    # print projsrc_fn
    [status,tmp_fn] = prwtr_extraction.interpolate_prwtr( dt )

    # Only process further if interpolate_prwtr has been successful
    if status == 1:
        [status,prwtr_fn] = prwtr_extraction.reproject_prwtr( projsrc_fn, tmp_fn )
        # print status, prwtr_fn

    if status == 2:
        prwtr = prwtr_extraction.load_prwtr( prwtr_fn )

        # Get array size
        [rows,cols] = prwtr.shape

        # Calculate threshold arrays for clear-sky conditions
        t8m11 = -3.19797 - 1.64805 * N.log( prwtr )
        t11m12 = -0.456924 + ( 0.488198 * prwtr )

        # Output cloud mask
        cloudmsk = N.zeros( (rows,cols), dtype=N.uint8 )

        area = N.zeros( (rows,cols), dtype=N.uint8 )

        # print diff8_6.shape
        # print t8m11.shape
        owidx = N.nonzero(icemsk==0)
        area[owidx] = area[owidx] + 1
        t8idx = N.nonzero(diff8_6>t8m11)
        area[t8idx] = area[t8idx] + 1
        t11idx = N.nonzero(diff12>t11m12)
        area[t11idx] = area[t11idx] + 1
        cloudidx = N.nonzero(area==3)
        cloudmsk[cloudidx] = 1
        # print N.min(area), N.max(area)

        # Calculate confidence
        confidence = N.zeros( (rows,cols), dtype=N.float32 )
        # print confidence.min(), confidence.max()
        # print N.histogram( confidence, bins=10 )

        # Remove prwtr files
        os.remove( tmp_fn )
        os.remove( prwtr_fn )

    else:
        print "    Unable to complete Tri-Channel text."
        cloudmsk = None

    return [cloudmsk]


# Improved polar night-time tests, after
# Liu, Y., Key, J.R., Frey, R.A., Ackerman, S.A., and Menzel, W.P. (2004).
# Nighttime polar cloud detection with MODIS.
# Remote sensing of environment, 92(2), 181-194.
# See p.80 of ATBD
# Original values for BT7.2-BT11, not those reported in the ATBD
def liu_polar_night( bt22, bt28, bt31, bt36, lsmsk, icemsk, dayidx, hemname ):
    # bt27,  bt32,
    # Flag to use new threshold values in ABTD, rather than Liu et al 2004 originals
    newflg = 2
    if newflg == 0:
        print '  Nighttime Polar Cloud Tests (Liu et al 2004)'
    elif newflg == 1:
        print '  Nighttime Polar Cloud Tests (Liu et al 2004, ATBD adjusted)'
    elif newflg == 2:
        print '  Nighttime Polar Cloud Tests (Liu et al 2004, Hughes adjusted)'

    # Get array size
    [rows,cols] = bt31.shape

    # Output cloud mask
    cloudmsk = N.zeros( (rows,cols), dtype=N.uint8 )

    # Difference between BT7.2 and BT11
    diff1 = bt28 - bt31

    # Difference between BT11 and BT3.9
    diff2 = bt31 - bt22

    # Difference between BT14.2 and BT11
    diff3 = bt36 - bt31

    # Areas covered by different day/night land/water combinations
    area = N.zeros( (rows,cols), dtype=N.uint8 )
    area[N.nonzero(lsmsk!=1)] = 1
    area[dayidx] = area[dayidx] + 2
    # print N.unique(area)
    # print len(N.nonzero(area==4)[0])

    # BT11 threshold values for BT7.2-BT11 tests
    threshold_bt28bt31 = N.zeros( (rows,cols), dtype=N.float32 )
    # print N.min(bt31), N.average(bt31), N.max(bt31)
    procarea1 = N.zeros( (rows,cols), dtype=N.uint8 )
    procarea1[N.nonzero(area==1)] = 1

    if newflg == 1:

        # New, thresholds as linear functions
        idx = N.nonzero( bt31 <= 245.0 )
        procarea1[idx] = 4
        idx = N.nonzero( bt31 <= 220.0 )
        threshold_bt28bt31[idx] = 2.0
        procarea1[idx] = 2
        idx = N.nonzero( bt31 >= 245.0 )
        procarea1[idx] = 8
        idx = N.nonzero( bt31 >= 255.0 )
        procarea1[idx] = 16
        idx = N.nonzero( bt31 >= 265.0 )
        threshold_bt28bt31[idx] = -21.0
        procarea1[idx] = 32

        idx = N.nonzero(procarea1==4)
        scalar = (-4.5 - 2.0) / (245.0 - 220.0)
        threshold_bt28bt31[idx] = 2.0 + ((bt31[idx] - 220.0) * scalar)
        idx = N.nonzero(procarea1==8)
        scalar = (-17.5 - -4.5) / (255.0 - 245.0)
        threshold_bt28bt31[idx] = -4.5 + ((bt31[idx] - 245.0) * scalar)
        idx = N.nonzero(procarea1==16)
        scalar = (-21 - -17.5) / (265.0 - 255.0)
        threshold_bt28bt31[idx] = -17.5 + ((bt31[idx] - 255.0) * scalar)

    elif newflg == 0:

        # Original values from Liu et al.  They also note not to apply the filter when BT11 is
        # greater than 250 K
        idx = N.nonzero( bt31 <= 245.0 )
        procarea1[idx] = 4
        idx = N.nonzero( bt31 <= 220.0 )
        threshold_bt28bt31[idx] = 3.0
        procarea1[idx] = 2
        idx = N.nonzero( bt31 >= 245.0 )
        procarea1[idx] = 8
        idx = N.nonzero( bt31 > 250.0 )
        procarea1[idx] = 0
        threshold_bt28bt31[idx] = -5.0

        idx = N.nonzero(procarea1==4)
        scalar = (-2.0 - 3.0) / (245.0 - 220.0)
        threshold_bt28bt31[idx] = 3.0 + ((bt31[idx] - 220.0) * scalar)
        idx = N.nonzero(procarea1==8)
        scalar = (-5.0 - -2.0) / (250.0 - 245.0)
        threshold_bt28bt31[idx] = -2.0 + ((bt31[idx] - 245.0) * scalar)

    if newflg == 2:

        # New, thresholds as spline function based on Svalbard images
        bt_x   = N.array( [ 220.0, 235.0, 239.0, 242.0, 245.0, 247.0, 250.0, 253.0, 260.0, 270.0 ], dtype=N.float32 )
        diff_y = N.array( [   2.0, -15.0, -22.0, -25.0, -18.0, -18.0, -25.0, -32.0, -38.0, -40.0 ], dtype=N.float32 )
        curve = inter.InterpolatedUnivariateSpline( bt_x, diff_y )
        # xx = N.arange(220.0,270.0,2.0)
        # new_y = curve(xx)
        # for x,y in zip(xx,new_y):
        #     print x, y
        idx = N.nonzero(procarea1==1)
        threshold_bt28bt31[idx] = curve(bt31[idx])

    # Do BT7.2-BT11 test
    dodiff = N.zeros( (rows,cols), dtype=N.uint8 )
    dodiff[N.nonzero(area==1)] = 1
    diffidx = N.nonzero(icemsk>0)
    dodiff[diffidx] = dodiff[diffidx] + 1    
    diffidx = N.nonzero(procarea1>0)
    dodiff[diffidx] = dodiff[diffidx] + 1    
    diffidx = N.nonzero(diff1<threshold_bt28bt31)
    dodiff[diffidx] = dodiff[diffidx] + 1
    # print N.unique(dodiff)
    doidx = N.nonzero(dodiff==4)
    cloudmsk[doidx] = 1

    totalpix = rows * cols
    cloudpix = len(doidx[0])
    percent_cloud = (100.0/totalpix) * cloudpix
    # print ("    Percent cloud = %5.1f" % percent_cloud)

    # Clear test for BT7.2-BT11
    dodiff = N.zeros( (rows,cols), dtype=N.uint8 )
    dodiff[N.nonzero(area==1)] = 1
    diffidx = N.nonzero(diff1>5.0)
    dodiff[diffidx] = dodiff[diffidx] + 1
    doidx = N.nonzero(dodiff==2)
    cloudmsk[doidx] = 0

    totalpix = rows * cols
    cloudpix = len(doidx[0])
    percent_cloud = (100.0/totalpix) * cloudpix
    # print ("    Percent cloud = %5.1f" % percent_cloud)

    # Threshold areas for BT11-BT3.9 tests
    procarea2 = N.zeros( (rows,cols), dtype=N.uint8 )
    threshold_bt31bt22 = N.zeros( (rows,cols), dtype=N.float32 )
    procarea2[N.nonzero(area==1)] = 1

    idx = N.nonzero( bt31 <= 235.0 )
    procarea2[idx] = procarea2[idx] + 2
    threshold_bt31bt22[idx] = -0.9

    idx = N.nonzero( bt31 >= 265.0 )
    procarea2[idx] = procarea2[idx] + 4
    threshold_bt31bt22[idx] = 0.5

    idx = N.nonzero(procarea2==1)
    scalar = (0.5 - -0.9) / (265.0 - 235.0)
    procarea2[idx] = procarea2[idx] + 8
    threshold_bt31bt22[idx] = -0.9 + ((bt31[idx] - 235.0) * scalar)

    # Do BT11-BT3.9 test
    dodiff = N.zeros( (rows,cols), dtype=N.uint8 )
    dodiff[N.nonzero(area==1)] = 1
    diffidx = N.nonzero(diff2>threshold_bt31bt22)
    dodiff[diffidx] = dodiff[diffidx] + 1
    doidx = N.nonzero(dodiff==2)
    cloudmsk[doidx] = cloudmsk[doidx] + 2

    if hemname == 'Antarctic':

        # Do BT14.2-BT11 test
        dodiff = N.zeros( (rows,cols), dtype=N.uint8 )
        dodiff[N.nonzero(area==0)] = 1
        diffidx = N.nonzero(diff3<-3.0)
        dodiff[diffidx] = dodiff[diffidx] + 1
        doidx = N.nonzero(dodiff==2)
        cloudmsk[doidx] = cloudmsk[doidx] + 4

    # print N.unique(cloudmsk)
    totalpix = rows * cols
    cloudpix = len(doidx[0])
    percent_cloud = (100.0/totalpix) * cloudpix
    # print ("    Percent cloud = %5.1f" % percent_cloud)

    return [ cloudmsk, procarea1, diff1, threshold_bt28bt31, diff2, threshold_bt31bt22  ]

