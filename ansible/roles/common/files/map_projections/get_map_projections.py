#!/usr/bin/python

# Name:          get_map_projections.py
# Purpose:       Get Arctic and Antarctic map projections from files.
#                Needs a plain text file containing the following 4 lines:
#                  SRS_ARCTIC_FN=
#                  SRS_ARCTIC_ULLR=
#                  SRS_ANTARCTIC_FN=
#                  SRS_ANTARCTIC_ULLR=#
#                The routine then gets the map projection information from the files.
# Author(s):     Nick Hughes
# Created:       2017-ix-4
# Modifications: 2017-ix-?  - 
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.884048
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Get map projections from files
# Nick Hughes, 2016-iv-25

# Needs a plain text file containing the following 4 lines:
#   SRS_ARCTIC_FN=
#   SRS_ARCTIC_ULLR=
#   SRS_ANTARCTIC_FN=
#   SRS_ANTARCTIC_ULLR=
#
# Routine then gets the map projection information from the files.

import os, sys
from datetime import datetime

import osgeo.osr as osr

def get_map_projections( mapproj_fn ):

    # Empty dictionary for values
    map_projections = {}

    # Try to load file data, otherwise give an error and halt
    try:
        infile = open( mapproj_fn, 'r' )
    except:
        print "Bifrost map projections are required!"
        print ("Unable to load from file %s" % mapproj_fn)
        sys.exit()
    else:
        for txtln in infile:
            bits = (txtln.strip()).split('=')
            keytxt = bits[0]
            keyval = bits[1]
            map_projections[keytxt] = keyval
        infile.close()

    # Try to load further server information from the SRS_ARCTIC_FN and
    # SRS_ANTARCTIC_FN filenames provided.
    regions = [ 'ARCTIC', 'ANTARCTIC' ]
    for region in regions:
        keystr = ("SRS_%s_FN" % region)

        try:
            srswktfile = open( map_projections[keystr], 'r' )
        except:
            print ("Bifrost map projection file (%s) is not available." % map_projections[keystr])
            sys.exit()
        else:
            srswkt = srswktfile.readline()
            srswktfile.close()

            # Try to create an OSR SRS from WKT
            srs = osr.SpatialReference()
            srs.ImportFromWkt( srswkt )
            srsproj4 = srs.ExportToProj4()
        
            # Add information to credentials
            keystr = ("SRS_%s_WKT" % region)
            map_projections[keystr] = srswkt
            keystr = ("SRS_%s_PROJ4" % region)
            map_projections[keystr] = srsproj4

    return map_projections


# Main routine for testing
if __name__ == '__main__':
    
    inpfn = '/home/bifrostanalyst/Settings/Bifrost/map_projections.txt'
    map_projections = get_map_projections( inpfn )
    print map_projections
    
