![bifrost_logo.png](/resources/bifrost_logo.png){width=200}

Bifrost is the ice charting system of the [Norwegian Ice Service](http://www.met.no/).  This repository contains the [Ansible](https://www.ansible.com/) scripts that create  a working copy of the system as a server-client pair of [Vagrant](https://www.vagrantup.com/) boxes, running under [VirtualBox](https://www.virtualbox.org/).

# Installation and Running #

Due to the large size of the Vagrant box file used for Bifrost, the repository is split into 2 parts. You can find the Vagrant box at the [Bifrost Lubuntu Xenial repository](https://bitbucket.org/Polarnix/bifrost-lubuntu-xenial) and that repository should be downloaded when installing the system.  That repository all contains the full [Installation and Running instructions](https://bitbucket.org/Polarnix/bifrost-lubuntu-xenial/src/c4712905f63f18f9c8dcec15059338c012f0509d/doc/InstallationAndRunning.md).

# Satellite Data Processing #

The server part of Bifrost is capable of pre-processing a range of different satellite data products for use in ice charting.  Current capabilities include Sentinel-1, Sentinel-2, MODIS, AMSR2, ASCAT, CryoSat-2, and SMOS.  See the [Satellite Data Processing Guide](/doc/SatelliteDataProcessing.md) for more information.

The client part of Bifrost has capabilities for searching for Sentinel-1 and Sentinel-2 data.

# Ice Charting with Bifrost #

The procedure for ice charting with Bifrost is explained in the [Ice Charting Guide](/doc/IceCharting.md).

# Automatic Output of Products #

On completion of an ice chart, Bifrost has a range of automated routines for production of output graphics and disseminating these to users.  The current capabilites are described in the [Automatic Outputs Guide](/doc/AutomaticProduction.md)

# Acknowledgements #

As well as making use of [Lubuntu Xenial](https://help.ubuntu.com/community/Lubuntu/GetLubuntu/LTS), Bifrost also uses the following software which has been included in the installation files:

- [MapServer 6.0.3](http://mapserver.org/download.html)
- The MODIS Reprojection Tool, [MRTSwath](https://lpdaac.usgs.gov/tools/modis_reprojection_tool_swath) by the NASA Earth Science Data and Information Systems (ESDIS) Project.
- [prepair](https://github.com/tudelft3d/prepair) by H. Ledoux, K. Arroyo Ohori and M. Meijers at the Delft University of Technology.


### Note ###

The scripts here are copies of those used on the operational ice charting system of the Norwegian Ice Service. On this version of Bifrost they are very much work in progress and there may be some bugs or things that do not work as expected.  The intention with Bifrost is to develop these into icons that can be integrated into the QGIS menu system.

**Citing this software**  
This software has DOI: 10.5281/zenodo.884048 on Zenodo.  
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.884048.svg)](https://doi.org/10.5281/zenodo.884048)

